package de.tavendo.autobahn.websocket.types;

public class ConnectionResponse {

    public final String protocol;

    public ConnectionResponse(String protocol) {
        this.protocol = protocol;
    }
}
