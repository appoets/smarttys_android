package org.appspot.apprtc;

public class WebrtcConstants {
    public static String OWN_TURN_SERVER = "";//change in strings.xml too key ="pref_room_server_url_own_server"
    public static String APPRTC_SERVER = "http://www.smarttysapp.com:8080";


    public static boolean isTurnServerEnabled = true;//change in strings.xml too key ="is_turn_server_enabled"

    public static String getBaseUrl() {

        if (isTurnServerEnabled)
            return OWN_TURN_SERVER;

        return APPRTC_SERVER;
    }
}
