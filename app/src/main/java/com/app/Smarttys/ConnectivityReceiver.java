package com.app.Smarttys;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.app.Smarttys.annotations.BroadcastReceiverActions;
import com.app.Smarttys.core.CoreController;


/*

public class ConnectivityReceiver extends BroadcastReceiver {
    public static ConnectivityReceiverListener connectivityReceiverListener;

    public ConnectivityReceiver() {
        super();
    }
    @Override
    public void onReceive(Context context, Intent intent) {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if (connectivityReceiverListener != null) {
            connectivityReceiverListener.onNetworkConnectionChanged(isConnected);
        }
    }
    public static boolean isConnected() {
        ConnectivityManager
                cm = (ConnectivityManager) CoreController.getInstance().getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }
    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }
}
*/


/*@BroadcastReceiverActions({"android.intent.action.SCREEN_ON", "android.intent.action.SCREEN_OFF",
        "android.intent.action.DREAMING_STARTED", "android.intent.action.DREAMING_STOPPED",
        "android.intent.action.ACTION_POWER_DISCONNECTED", "android.intent.action.ACTION_POWER_CONNECTED",
        "android.net.conn.CONNECTIVITY_CHANGE"})*/

@BroadcastReceiverActions({"android.intent.action.ACTION_POWER_DISCONNECTED", "android.net.conn.CONNECTIVITY_CHANGE", "android.intent.action.ACTION_POWER_CONNECTED"
})
public class ConnectivityReceiver extends BroadcastReceiver {
    public static ConnectivityReceiverListener connectivityReceiverListener;
    boolean isConnected;

    public ConnectivityReceiver() {
        super();
    }

    public static boolean isConnected() {
        ConnectivityManager
                cm = (ConnectivityManager) CoreController.getInstance().getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onReceive(Context context, Intent intent) {


    /*    ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
*/
        Log.e("ConnectivityReceiver", "intent" + intent);

        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {

                //   Toast.makeText(context, "Internet Connected" + intent.getAction(), Toast.LENGTH_LONG).show();
                //    isConnected=networkInfo.isConnectedOrConnecting();
                isConnected = isConnected();
                //       Log.d("Network", "Internet YAY");
                if (connectivityReceiverListener != null) {
                    connectivityReceiverListener.onNetworkConnectionChanged(isConnected);
                    //   connectivityReceiverListener.onNetworkConnectionChanged( context,  intent);
                }
            } else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED) {
                //         Log.d("Network", "No internet :(");

                //   isConnected=networkInfo.isConnectedOrConnecting();
                isConnected = isConnected();

                if (connectivityReceiverListener != null) {
                    connectivityReceiverListener.onNetworkConnectionChanged(isConnected);
                    //   connectivityReceiverListener.onNetworkConnectionChanged( context,  intent);
                }
                //   Toast.makeText(context, "No internet " + intent.getAction(), Toast.LENGTH_LONG).show();
            }
        }

      /*  if (connectivityReceiverListener != null) {
            connectivityReceiverListener.onNetworkConnectionChanged(isConnected);
         //   connectivityReceiverListener.onNetworkConnectionChanged( context,  intent);
        }*/
      /*  if (intent.getAction().equals("android.intent.action.DREAMING_STOPPED")) {
            if (connectivityReceiverListener != null) {
                connectivityReceiverListener.onNetworkConnectionBundle(intent);
                //   connectivityReceiverListener.onNetworkConnectionChanged( context,  intent);
            }
        }*/

    }

    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);


        //    void onNetworkConnectionChanged(Context context, Intent intent);

    }
}
