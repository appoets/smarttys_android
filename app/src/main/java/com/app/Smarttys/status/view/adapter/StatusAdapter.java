package com.app.Smarttys.status.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.WhatsappStatusCircle;
import com.app.Smarttys.app.widget.CircleImageView;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.status.controller.StatusUtil;
import com.app.Smarttys.status.controller.interfaces.OnListClickListener;
import com.app.Smarttys.status.model.StatusModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user134 on 3/27/2018.
 */

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.MyViewHolder> implements Filterable {

    private static final String TAG = "StatusAdapter";
    private Context mContext;
    private OnListClickListener onListClickListener;
    private List<StatusModel> mData = new ArrayList<>();
    private List<StatusModel> mDisplayedValues = new ArrayList<>();
    private boolean isMutedList;
    private String type;

    public StatusAdapter(Context context, List<StatusModel> data, String type, boolean isMutedList, OnListClickListener onListClickListener) {
        mContext = context;
        mData = data;
        mDisplayedValues = data;
        this.isMutedList = isMutedList;
        this.type = type;
        this.onListClickListener = onListClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.status_list_row_home, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        if (mDisplayedValues != null && mDisplayedValues.get(position) != null) {
            final StatusModel statusModel = mDisplayedValues.get(position);
            final String thumbnailImage = statusModel.getThumbNail();
            MyLog.d(TAG, "onBindViewHolder: " + position);
            try {
                if (statusModel.isVideo()) {
                    if (thumbnailImage != null) {
                        String thumbnailImageResult = thumbnailImage.replace("data:image/jpeg;base64,", "");
                        Bitmap bitmap = StatusUtil.decodeBitmapFromBase64(thumbnailImageResult, 100, 100, false);
                        if (bitmap != null) {
                            holder.ivProfileImage.setImageBitmap(bitmap);
                        }
                    }
                }

                if (statusModel.isTextstatus()) {
                    holder.text_status_caption.setVisibility(View.VISIBLE);
                    //holder.ivProfileImage.setLayoutParams (new ConstraintLayout.LayoutParams(200, 200));
                    ShapeDrawable badge = new ShapeDrawable(new OvalShape());
                    badge.setIntrinsicWidth(60);
                    badge.setIntrinsicHeight(60);
                    badge.getPaint().setColor(Color.parseColor(statusModel.getTextstatus_color_code()));
                    holder.ivProfileImage.setImageDrawable(badge);
                    holder.text_status_caption.setText(statusModel.getTextstatus_caption());
                } else {
                    holder.text_status_caption.setVisibility(View.GONE);
                    if (AppUtils.isNetworkAvailable(mContext)) {
                        final String serverImage = statusModel.getServerPath();
                        // Picasso.with(mContext).load(serverImage).fit().into(holder.ivProfileImage );
                        if (serverImage != null)
                            AppUtils.loadglideimage(mContext, AppUtils.getGlideURL(serverImage, mContext), holder.ivProfileImage);
                    } else {
                         /* Picasso.with(mContext).load(statusModel.getServerPath()).networkPolicy(NetworkPolicy.OFFLINE)
                                  .fit()
                                  .into(holder.ivProfileImage);*/
                        AppUtils.loadglideimage(mContext, AppUtils.getGlideURL(statusModel.getServerPath(), mContext), holder.ivProfileImage);
                    }
                }
            } catch (Exception e) {
                MyLog.e(TAG, "run: ", e);
            }

            if (statusModel.getUserId() != null) {
                String name = StatusUtil.getNameById(mContext, statusModel.getUserId());
                if (name != null && !name.isEmpty())
                    holder.tvProfileName.setText(name);
                else {
                    String nameFromDB = StatusUtil.getNameById(mContext, statusModel.getUserId());
                    if (nameFromDB != null && !nameFromDB.isEmpty()) {
                        holder.tvProfileName.setText(nameFromDB);
                    } else {
                        if (statusModel.getMsisdn() != null && !statusModel.getMsisdn().isEmpty())
                            holder.tvProfileName.setText(statusModel.getMsisdn());
                    }
                }
            }
            if (statusModel.getTimeUploaded() != 0L) {
                //   holder.tvTime.setText(AppUtils.getStatusTime(mContext, statusModel.getTimeUploaded()));
                MyLog.e("tvTime", "tvTime" + statusModel.getTimeUploaded());
                holder.tvTime.setText(new String(AppUtils.getStatusTime(mContext, statusModel.getTimeUploaded(), false)));
            }

            holder.rootView.setOnClickListener(v -> onListClickListener.onItemClick(holder.getAdapterPosition(), mDisplayedValues.get(holder.getAdapterPosition()), false));
            holder.rootView.setOnLongClickListener(v -> {
                onListClickListener.onItemClick(holder.getAdapterPosition(), mDisplayedValues.get(holder.getAdapterPosition()), true);
                return true;
            });
            int totalCount = CoreController.getStatusDB(mContext).getStatusCount(statusModel.getUserId());
            int unViewedStatusCount = CoreController.getStatusDB(mContext).getUnViewedStatusCount(statusModel.getUserId());

            holder.whatsappStatusCircle.setTotal(totalCount);
            holder.whatsappStatusCircle.setViewed(totalCount - unViewedStatusCount);
            holder.whatsappStatusCircle.invalidate();

            if (isMutedList) {
                holder.tvProfileName.setAlpha(0.5f);
                holder.tvTime.setAlpha(0.5f);
                holder.ivProfileImage.setAlpha(0.5f);
                holder.whatsappStatusCircle.setAlpha(0.5f);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mDisplayedValues == null)
            return 0;
        MyLog.d(TAG, "getItemCount: " + mDisplayedValues.size());
        return mDisplayedValues.size();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mDisplayedValues = (ArrayList<StatusModel>) results.values; // has the filtered values
                if (mDisplayedValues != null) {
//                    Toast.makeText(context, "No Contacts Matching Your Query...", Toast.LENGTH_SHORT).show();
                    MyLog.d(TAG, "publishResults: " + constraint + " type: " + type + " size: " + mDisplayedValues.size());
                }
                if (onListClickListener != null) {
                    onListClickListener.onItemClick(results.count, type, false);
                }
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<StatusModel> FilteredArrList = new ArrayList<>();
                if (mData == null) {
                    mData = new ArrayList<>(mDisplayedValues); // saves the original data in mData
                }
                if (constraint == null || constraint.length() == 0) {
                    // set the Original result to return
                    results.count = mData.size();
                    results.values = mData;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mData.size(); i++) {
                        String contactName = mData.get(i).getName();
                        String contactNo = mData.get(i).getMsisdn();
                        if ((contactName != null && contactName.toLowerCase().contains(constraint)) ||
                                (contactNo != null && contactNo.toLowerCase().contains(constraint))) {
                            FilteredArrList.add(mData.get(i));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private WhatsappStatusCircle whatsappStatusCircle;
        private CircleImageView ivProfileImage;
        private TextView tvProfileName, tvTime;
        private View rootView;
        private TextView text_status_caption;

        public MyViewHolder(View itemView) {
            super(itemView);
            whatsappStatusCircle = itemView.findViewById(R.id.whatsapp_status_circle);
            ivProfileImage = itemView.findViewById(R.id.iv_profile_image);
            tvProfileName = itemView.findViewById(R.id.tv_name);
            tvTime = itemView.findViewById(R.id.tv_time);
            rootView = itemView.findViewById(R.id.root_view);
            text_status_caption = itemView.findViewById(R.id.text_status_caption);
        }
    }
}