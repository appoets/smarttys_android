package com.app.Smarttys.status.model;

import java.io.Serializable;

/**
 * Created by user134 on 3/29/2018.
 */

public class MyStatusModel implements Serializable {
    private int count;
    private StatusModel statusModel;


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


    public StatusModel getStatusModel() {
        return statusModel;
    }

    public void setStatusModel(StatusModel statusModel) {
        this.statusModel = statusModel;
    }
}
