package com.app.Smarttys.status.controller;

import android.content.Context;
import android.widget.Toast;

import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.MessageAck;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.message.TextMessage;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.status.model.StatusDB;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by user134 on 4/9/2018.
 */

public class StatusSocketEvents {
    private static final String TAG = "StatusSocketEvents";

    public static synchronized void sendAckToServer(Context context, String to, String docId, String id, String status) {
        MyLog.d(TAG, "sendAckToServer: ");
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_STATUS_ACK);
        MessageAck ack = (MessageAck) MessageFactory.getMessage(MessageFactory.message_ack, (context));

        messageEvent.setMessageObject((JSONObject) ack.getStatusMessageObject(to, docId,
                status, id));
        EventBus.getDefault().post(messageEvent);
    }


    public static synchronized void muteUnMuteStatus(Context context, String to, String docId, boolean isMuted, boolean isUnMuted) {

        //String convId = mCurrentUserId + "-" + to;
        String status = "0";
        if (isMuted) {
            CoreController.getStatusDB(context).insertMutedUser(to);
            status = "1";
        }
        if (isUnMuted) {
            CoreController.getStatusDB(context).updateMuteStatus(to, StatusDB.UN_MUTED);
            status = "0";
        }
        try {
            SessionManager sessionManager = SessionManager.getInstance(context);
            String mCurrentUserId = sessionManager.getCurrentUserID();
            String convId = CoreController.getStatusDB(context).getConvId(docId);
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            object.put("status", status);
            object.put("convId", convId);
            object.put("to", to);
            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_STATUS_MUTE);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (Exception e) {
            MyLog.e(TAG, "muteStatus: ", e);
        }
    }


    public static void fetchAllStatus(Context context) {
        try {
            SessionManager sessionManager = SessionManager.getInstance(context);
            String mCurrentUserId = sessionManager.getCurrentUserID();
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_STATUS_FETCH_ALL);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (Exception e) {
            MyLog.e(TAG, "muteStatus: ", e);
        }
    }


    public static void setStatusPrivacy(Context context, String type, List<String> contactsIds, String contactIdsString) {
        try {
            String currentUserId = SessionManager.getInstance(context).getCurrentUserID();
            SharedPrefUtil.setStatusPrivacy(type, contactIdsString);
            JSONObject object = new JSONObject();
            object.put("from", currentUserId);
            object.put("privacy", type);
            if (contactsIds != null && contactsIds.size() > 0) {
                object.put("statusToID",
                        new JSONArray(contactsIds));
            }
            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_STATUS_PRIVACY);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
            Toast.makeText(context, "Your status privacy settings updated successfully", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            MyLog.e(TAG, "muteStatus: ", e);
        }
    }


    public static void sendStatusReply(Context context, String from, String to,
                                       String senderName,
                                       String receiverName,
                                       String recordId,
                                       boolean isVideo,
                                       String thumbnailData,
                                       String receiverMsisdn,
                                       String imagePath,
                                       String data, String docId) {

        String ReplySender = senderName;
        String replytype = "1";
        Session session = new Session(context);
        if (!data.equalsIgnoreCase("")) {
            if (session.getarchivecount() != 0) {
                if (session.getarchive(from + "-" + to))
                    session.removearchive(from + "-" + to);
            }

            SendMessageEvent messageEvent = new SendMessageEvent();
            TextMessage message = (TextMessage) MessageFactory.getMessage(MessageFactory.text, context);
            JSONObject msgObj;

            messageEvent.setEventName(SocketManager.EVENT_REPLY_MESSAGE);
            try {
                msgObj = (JSONObject) message.getMessageObject(to, data, false);
                msgObj.put("recordId", recordId);
                msgObj.put("reply_type", "status");
                messageEvent.setMessageObject(msgObj);
            } catch (Exception ex) {
                MyLog.e(TAG, "sendStatusReply: ", ex);
            }


            MessageItemChat item = message.createMessageItem(true, data,
                    MessageFactory.DELIVERY_STATUS_NOT_SENT, to, senderName);
            item.setReplyType(replytype);

            if (isVideo) {
                item.setReplyMessage("");
                item.setreplyimagebase64(thumbnailData);
            } else {
                //item.setReplyMessage("Status");
                item.setreplyimagepath(imagePath);
                item.setreplyimagebase64(thumbnailData);
            }

            item.setReplySender(ReplySender);
            MessageDbController db = CoreController.getDBInstance(context);
            item.setSenderMsisdn(receiverMsisdn);
            item.setSenderName(senderName);
            item.setStatusReply(true);
            item.setStatusDocId(docId);
            String chatType = MessageFactory.CHAT_TYPE_SINGLE;
            db.updateChatMessage(item, chatType);
            EventBus.getDefault().post(messageEvent);
        }
    }


}
