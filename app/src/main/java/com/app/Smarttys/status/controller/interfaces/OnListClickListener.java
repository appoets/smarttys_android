package com.app.Smarttys.status.controller.interfaces;

/**
 * Created by user134 on 3/27/2018.
 */

public interface OnListClickListener {
    void onItemClick(int position, Object result, boolean isLongPress);
}
