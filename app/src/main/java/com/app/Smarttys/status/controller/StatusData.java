package com.app.Smarttys.status.controller;

import com.app.Smarttys.status.model.StatusModel;

import java.util.HashMap;

/**
 * Created by user134 on 4/2/2018.
 */

public class StatusData {
    private static final StatusData ourInstance = new StatusData();
    public HashMap<Integer, StatusModel> selectedGalleryItems;

    private StatusData() {
    }

    public static StatusData getInstance() {
        return ourInstance;
    }
}
