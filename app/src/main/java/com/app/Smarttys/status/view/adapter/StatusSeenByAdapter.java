package com.app.Smarttys.status.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.service.Constants;
import com.app.Smarttys.status.controller.StatusUtil;
import com.app.Smarttys.status.model.StatusListModel;

import java.util.List;

/**
 * Created by user134 on 3/29/2018.
 */

public class StatusSeenByAdapter extends RecyclerView.Adapter<StatusSeenByAdapter.MyViewHolder> {


    private static final String TAG = "StatusSeenByAdapter";
    private Context mContext;
    private List<StatusListModel> mData;

    public StatusSeenByAdapter(Context context, List<StatusListModel> data) {
        this.mContext = context;
        mData = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.status_list_row_seenby, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        if (mData != null && mData.get(position) != null) {

            final StatusListModel statusListModel = mData.get(position);

            holder.ivProfile.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
                        String imageTS = contactDB_sqlite.getDpUpdatedTime(statusListModel.getId());
                        if (imageTS == null || imageTS.isEmpty())
                            imageTS = "1";
                        String avatar = Constants.USER_PROFILE_URL + statusListModel.getId() + ".jpg?id=" + imageTS;
                        StatusUtil.setProfileImage(holder.ivProfile, mContext, avatar, statusListModel.getId());
                    } catch (Exception e) {
                        MyLog.e(TAG, "onBindViewHolder: ", e);
                    }
                }
            });


            holder.tvTime.setText(AppUtils.getStatusTime(mContext, statusListModel.getUpdatedTIme(), false));
            if (statusListModel.getName() != null)
                holder.tvName.setText(statusListModel.getName());
        }
    }

    @Override
    public int getItemCount() {
        if (mData == null)
            return 0;
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivProfile;
        private TextView tvName, tvTime;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivProfile = itemView.findViewById(R.id.iv_profile_image);
            tvName = itemView.findViewById(R.id.tv_name);
            tvTime = itemView.findViewById(R.id.tv_time);
        }
    }
}
