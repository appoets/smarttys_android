package com.app.Smarttys.status.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.Keys;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.status.controller.StatusData;
import com.app.Smarttys.status.controller.interfaces.OnGalleryItemClickListener;
import com.app.Smarttys.status.model.StatusModel;
import com.app.Smarttys.status.view.adapter.GalleryImagesAdapter;

import java.util.HashMap;

import static com.app.Smarttys.status.view.activity.StatusAddActivity.MEDIA_PICK_COMPLETED;
import static com.app.Smarttys.status.view.activity.StatusAddActivity.OPEN_MEDIA_PICKER;
import static com.app.Smarttys.status.view.activity.StatusAddActivity.STATUS_ADD;

/**
 * Created by user134 on 4/2/2018.
 */

public class StatusGalleryPickerActivity extends CoreActivity implements OnGalleryItemClickListener, View.OnClickListener {
    private static final String TAG = "StatusGalleryPicker";
    private static final int CAPTION_REQ_CODE = 2;
    private RecyclerView mRvGallery;
    private TextView tvSelectedCount;
    private HashMap<Integer, StatusModel> selectedGalleryItems = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_gallery_picker);

        mRvGallery = findViewById(R.id.rv_gallery_picker);
        tvSelectedCount = findViewById(R.id.tv_media_count);
        mRvGallery.setLayoutManager(AppUtils.getGridLayoutManger(this, 3));
        //mRvGallery.setItemViewCacheSize(500);
        mRvGallery.setDrawingCacheEnabled(true);
        mRvGallery.getItemAnimator().setChangeDuration(0);

//        mRvGallery.setHasFixedSize(true);


        findViewById(R.id.iv_back).setOnClickListener(this);
        findViewById(R.id.tv_ok).setOnClickListener(this);

        if (selectedGalleryItems != null) {
            int selectedItemsCount = selectedGalleryItems.size();
            if (selectedItemsCount > 0) {
                findViewById(R.id.tv_ok).setVisibility(View.VISIBLE);
            }

            tvSelectedCount.setText(String.valueOf(selectedItemsCount));
        }
    }

    private void setAdapter() {
        selectedGalleryItems = StatusData.getInstance().selectedGalleryItems;

        if (selectedGalleryItems != null) {
            tvSelectedCount.setText(String.valueOf(selectedGalleryItems.size()));
        }
        mRvGallery.postDelayed(new Runnable() {
            @Override
            public void run() {
                GalleryImagesAdapter adapter = new GalleryImagesAdapter(StatusGalleryPickerActivity.this,
                        StatusGalleryPickerActivity.this, null, true, selectedGalleryItems);
                adapter.setHasStableIds(true);
                mRvGallery.setAdapter(adapter);
            }
        }, 100);
    }

    @Override
    public void onItemClick(int totalCount, HashMap<Integer, StatusModel> selectedItems) {
        MyLog.d(TAG, "onItemClick: ");
        if (totalCount > 0) {
            findViewById(R.id.tv_ok).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.tv_ok).setVisibility(View.GONE);
        }
        tvSelectedCount.setText(String.valueOf(totalCount));
        selectedGalleryItems = selectedItems;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setAdapter();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MyLog.d(TAG, "onActivityResult: resultCode: " + resultCode);
        if (resultCode == RESULT_OK) {
            Intent galleryIntent = new Intent();
            galleryIntent.putExtra("status_uploaded", true);
            setResult(OPEN_MEDIA_PICKER, galleryIntent);
        } else if (resultCode == MEDIA_PICK_COMPLETED || resultCode == STATUS_ADD) {
            Intent galleryIntent = new Intent();
            galleryIntent.putExtra("status_uploaded", true);
            setResult(OPEN_MEDIA_PICKER, galleryIntent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        StatusData.getInstance().selectedGalleryItems = selectedGalleryItems;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.tv_ok:
                if (selectedGalleryItems != null && selectedGalleryItems.size() > 0) {
                    StatusData.getInstance().selectedGalleryItems = selectedGalleryItems;
                    Intent intent = new Intent(StatusGalleryPickerActivity.this, StatusCaptionActivity.class);
                    intent.putExtra(Keys.FROM_GALLERY_PICKER, true);
                    startActivityForResult(intent, CAPTION_REQ_CODE);
                }
                //setResult(StatusAddActivity.FINISH_ACTIVITY);
                break;

        }
    }
}
