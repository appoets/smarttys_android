package com.app.Smarttys.status.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.message.PictureMessage;
import com.app.Smarttys.core.message.VideoMessage;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;
import com.app.Smarttys.status.model.StatusDB;
import com.app.Smarttys.status.model.StatusModel;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by user134 on 4/14/2018.
 */

public class StatusUploadUtil {
    private static final StatusUploadUtil ourInstance = new StatusUploadUtil();
    private static final String TAG = "StatusUploadUtil";
    private FileUploadDownloadManager fileUploadDownloadMgnr;
    private List<StatusModel> pendingStatusList = new ArrayList<>();

    private StatusUploadUtil() {
    }

    public static StatusUploadUtil getInstance() {
        return ourInstance;
    }

    private void uploadImage(StatusModel statusModel, Context context) {
        String imgPath = statusModel.getLocalPath();
        String caption = statusModel.getCaption();
        String docId = statusModel.getDocId();
        MyLog.d(TAG, "StatusTest-->  uploadImage: " + docId);
        MyLog.d(TAG, "StatusTest--> uploadImage: " + imgPath);
        try {
            String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(imgPath);
            String serverFileName = SessionManager.getInstance(context).getCurrentUserID();

            PictureMessage message = new PictureMessage(context);
            JSONObject object = (JSONObject) message.createStatusImageObject(serverFileName, fileExtension, imgPath, caption, docId);
            MyLog.d(TAG, "StatusTest--> uploadImage: picture msg: " + object);

            if (fileUploadDownloadMgnr == null)
                fileUploadDownloadMgnr = new FileUploadDownloadManager(context);
            Log.d(TAG, "onClick: startFileUpload14");
            fileUploadDownloadMgnr.startFileUpload(EventBus.getDefault(), object);
        } catch (Exception e) {
            MyLog.e(TAG, "StatusTest--> uploadImage: ", e);
        }
    }

    private void uploadVideo(StatusModel statusModel, Context context) {
        String videoPath = statusModel.getLocalPath();
        String caption = statusModel.getCaption();
        String docId = statusModel.getDocId();
        String currentUserName = SessionManager.getInstance(context).getnameOfCurrentUser();
        String from = SessionManager.getInstance(context).getCurrentUserID();
        MyLog.d(TAG, "DOCID_TEST uploadVideo: docId: " + docId);
        if (videoPath != null) {
            MyLog.d(TAG, "uploadVideo: " + videoPath);
            VideoMessage message = (VideoMessage) MessageFactory.getMessage(MessageFactory.video, context);

            MessageItemChat item;
            message.getMessageObject("", videoPath, false);
            item = message.createMessageItem(true, videoPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                    "", currentUserName, caption);
            Bitmap thumbBmp = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MICRO_KIND);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            String thumbData = null;
            if (thumbBmp != null) {
                thumbBmp.compress(Bitmap.CompressFormat.PNG, 30, out);
                byte[] thumbArray = out.toByteArray();
                try {
                    out.close();
                } catch (Exception e) {
                    MyLog.e(TAG, "uploadVideo: ", e);
                }

                thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT);
            }
            if (thumbData != null) {
                item.setThumbnailData(thumbData);
            } else {
                item.setThumbnailData("");
            }

            String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(videoPath);
            //String videoName = item.getMessageId() + fileExtension;


            String serverFileName = SessionManager.getInstance(context).getCurrentUserID();
            JSONObject uploadObj = (JSONObject) message.createStatusVideoUploadObject(item.getMessageId(), docId,
                    serverFileName, fileExtension, videoPath, from, caption, MessageFactory.CHAT_TYPE_STATUS);

            if (fileUploadDownloadMgnr == null)
                fileUploadDownloadMgnr = new FileUploadDownloadManager(context);
            //   fileUploadDownloadMgnr.uploadFile(EventBus.getDefault(), uploadObj);
            fileUploadDownloadMgnr.startFileUpload(EventBus.getDefault(), uploadObj);
        }
    }


    public synchronized void checkAndUploadStatus(Context context) {
        MyLog.d(TAG, "StatusTest--> checkAndUploadStatus: ");
        try {
            pendingStatusList.clear();
            pendingStatusList.addAll(CoreController.getStatusDB(context).getMyUnUploadedStatus());
            List<StatusModel> failToUploadStatus = CoreController.getStatusDB(context).getMyFailToUploadStatus();
            if (failToUploadStatus != null && failToUploadStatus.size() > 0) {
                MyLog.d(TAG, "StatusTest--> checkAndUploadStatus failed files size in DB: " + failToUploadStatus.size());
                if (pendingStatusList.size() == 0) {
                    MyLog.d(TAG, "StatusTest--> checkAndUploadStatus: failed files count: " + failToUploadStatus.size());
                    uploadStatus(failToUploadStatus.get(0), context);
                }
            } else {
                MyLog.d(TAG, "StatusTest--> checkAndUploadStatus: no failed files in DB");
            }
            if (pendingStatusList != null && pendingStatusList.size() > 0) {
                MyLog.d(TAG, "StatusTest-->  checkAndUploadStatus: pending status count: " + pendingStatusList.size());
                uploadStatus(pendingStatusList.get(0), context);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "StatusTest--> checkAndUploadStatus: ", e);
        }
    }

    private void uploadStatus(final StatusModel statusModel, final Context context) {
        if (AppUtils.isNetworkAvailable(context)) {
            MyLog.d(TAG, "StatusTest-->  uploadStatus: " + statusModel.getDocId());
            SharedPrefUtil.setStatusRunning(true);
            SharedPrefUtil.setLastStatusStartTime(System.currentTimeMillis());

            if (statusModel.isImage()) {
                CoreController.getStatusDB(context).updateFileUploadingStatus(AppUtils.parseLong(statusModel.getId()), StatusDB.UPLOADING);
                uploadImage(statusModel, context);
            } else if (statusModel.isVideo()) {
                CoreController.getStatusDB(context).updateFileUploadingStatus(AppUtils.parseLong(statusModel.getId()), StatusDB.UPLOADING);
                uploadVideo(statusModel, context);
            } else if (statusModel.isTextstatus()) {
                //TextStatus is completed
                UploadTextStatus(statusModel, context);
            }
        }
    }

    private void UploadTextStatus(StatusModel statusModel, Context context) {

        Log.e("UploadTextStatus", "UploadTextStatus" + statusModel.toString());
        String Status_caption_text = statusModel.getTextstatus_caption();
        String Status_color_code = statusModel.getTextstatus_color_code();
        String Status_Font_Type = statusModel.getTextstatus_text_font();

        String imgPath = statusModel.getLocalPath();
        String caption = statusModel.getCaption();
        String docId = statusModel.getDocId();
        String themeCategory = statusModel.getThemeCategory();
        Log.d(TAG, "StatusTest-->  uploadImage: " + docId);
        Log.d(TAG, "StatusTest--> uploadImage: " + imgPath);
        try {
            //String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(imgPath);
            //String serverFileName = SessionManager.getInstance(context).getCurrentUserID();

            PictureMessage message = new PictureMessage(context);
            JSONObject object = (JSONObject) message.createStatusTextObject(Status_color_code, Status_Font_Type, Status_caption_text, docId, themeCategory);

            //  Log.e(TAG, "UploadTextStatus " + object);

/*            if (fileUploadDownloadMgnr == null)
                fileUploadDownloadMgnr = new FileUploadDownloadManager(context);
            fileUploadDownloadMgnr.startFileUpload(EventBus.getDefault(), object);*/
            TextStatusUpload(EventBus.getDefault(), object);
        } catch (Exception e) {
            Log.e(TAG, "StatusTest--> uploadImage: ", e);
        }
    }

    private void TextStatusUpload(EventBus eventBus, JSONObject object) {

        SendMessageEvent event = new SendMessageEvent();

        try {

            event.setEventName(SocketManager.EVENT_STATUS);
            Log.e("TextStatusUpload", "TextStatusUpload" + object);
            event.setMessageObject(object);
            eventBus.post(event);
            Log.e("CheckEventConnection", "Text Status Uploading..." + "" + object);

        } catch (Exception e) {
            Log.e(TAG, "", e);
        }

    }

}
