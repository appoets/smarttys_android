package com.app.Smarttys.status.controller;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.status.view.activity.StatusPrivacyActivity;

/**
 * Created by user134 on 4/4/2018.
 */

public class SharedPrefUtil {
    private static final String KEY_STATUS_UPLOAD_IN_PROGRESS = "status_in_progress";
    private static final String KEY_IS_FETCH_ALL_COMPLETED = "fetch_all_completed";
    private static final String KEY_STATUS_UPLOAD_TIMESTAMP = "status_upload_timestamp";
    private static final String KEY_STATUS_PRIVACY_TYPE = "status_privacy_type";
    private static final String KEY_STATUS_PRIVACY_IS_WAITING = "status_privacy_is_waiting";
    private static final String KEY_STATUS_PRIVACY_CONTACTS = "staus_privacy_contacts";
    private static final String TAG = SharedPrefUtil.class.getSimpleName();
    private static SharedPreferences mSharedPref;

    private SharedPrefUtil() {

    }

    public static void init(Context context) {
        if (mSharedPref == null)
            mSharedPref = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
    }

    public static String read(String key, String defValue) {
        return mSharedPref.getString(key, defValue);
    }

    public static void write(String key, String value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putString(key, value);
        prefsEditor.apply();
    }

    public static boolean read(String key, boolean defValue) {
        return mSharedPref.getBoolean(key, defValue);
    }

    public static long read(String key, long defValue) {
        return mSharedPref.getLong(key, defValue);
    }

    public static void write(String key, boolean value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.apply();
    }

    public static Integer read(String key, int defValue) {
        return mSharedPref.getInt(key, defValue);
    }

    public static void writeInt(String key, Integer value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putInt(key, value).apply();
    }


    public static void write(String key, long value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putLong(key, value).apply();
    }

    public static boolean isStatusInProgress() {
        return read(KEY_STATUS_UPLOAD_IN_PROGRESS, false);
    }

    public static boolean isFetchAllCompleted() {
        return read(KEY_IS_FETCH_ALL_COMPLETED, false);
    }


    public static void setStatusRunning(boolean status) {
        MyLog.d(TAG, "setStatusRunning: " + status);
        SharedPrefUtil.write(SharedPrefUtil.KEY_STATUS_UPLOAD_IN_PROGRESS, status);
    }

    public static void setFetchAllCompleted() {
        SharedPrefUtil.write(SharedPrefUtil.KEY_IS_FETCH_ALL_COMPLETED, true);
    }


    public static void setLastStatusStartTime(long timeStamp) {
        SharedPrefUtil.write(SharedPrefUtil.KEY_STATUS_UPLOAD_TIMESTAMP, timeStamp);
    }


    public static void setStatusPrivacy(String type, String contactsIds) {
        SharedPrefUtil.write(SharedPrefUtil.KEY_STATUS_PRIVACY_IS_WAITING, true);
        SharedPrefUtil.write(SharedPrefUtil.KEY_STATUS_PRIVACY_TYPE, type);
        SharedPrefUtil.write(SharedPrefUtil.KEY_STATUS_PRIVACY_CONTACTS, contactsIds);
    }

    public static String getSelectedContactIds() {
        return read(SharedPrefUtil.KEY_STATUS_PRIVACY_CONTACTS, "");
    }


    public static String getPrivacyType() {
        return read(SharedPrefUtil.KEY_STATUS_PRIVACY_TYPE, StatusPrivacyActivity.TYPE_MY_CONTACTS);
    }

}
