package com.app.Smarttys.status.view.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.dialog.CommonAlertDialog;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.Keys;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.status.controller.StatusSocketEvents;
import com.app.Smarttys.status.controller.interfaces.OnListClickListener;
import com.app.Smarttys.status.view.adapter.StatusPrivacyContactsAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class StatusPrivacyContactsActivity extends CoreActivity implements View.OnClickListener, OnListClickListener {
    private static final String TAG = StatusPrivacyContactsActivity.class.getSimpleName();
    private RecyclerView rvContactsList;
    private String type;
    private StatusPrivacyContactsAdapter adapter;
    private Toolbar searchToolbar, toolbar;
    private Menu searchMenu;
    private MenuItem itemSearch;
    private TextView tvToolbarSubTitle;
    private String subTitleContent = "";
    private FloatingActionButton fabIcon;
    private int totalSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_privacy_contacts);
        toolbar = findViewById(R.id.toolbar);
        searchToolbar = findViewById(R.id.search_tool_bar);
        setSupportActionBar(toolbar);
        MyLog.d(TAG, "onCreate: ");

        fabIcon = findViewById(R.id.fab_ok);
        fabIcon.setOnClickListener(this);
        findViewById(R.id.iv_select_all).setOnClickListener(this);
        findViewById(R.id.iv_search).setOnClickListener(this);
        findViewById(R.id.iv_back).setOnClickListener(this);
        TextView tvTitle = findViewById(R.id.toolbar_title);
        tvToolbarSubTitle = findViewById(R.id.tv_toolbar_subtitle);
        if (getIntent() != null) {
            type = getIntent().getStringExtra(Keys.TYPE);
            if (type.equals(StatusPrivacyActivity.TYPE_ONLY_SHARE_WITH)) {
                tvTitle.setText("Only share with...");
                subTitleContent = " contacts added";
            } else {
                tvTitle.setText("Hide status from...");
                subTitleContent = " contacts excluded";
            }
        }


        rvContactsList = findViewById(R.id.rv_contacts_list);
        rvContactsList.setLayoutManager(AppUtils.getDefaultLayoutManger(this));
        setSearchToolBar();
        setAdapter();


        setCount();
        if (totalSelected > 0)
            fabIcon.show();
        else
            fabIcon.hide();


    }

    private void setCount() {
        totalSelected = 0;

        for (SmarttyContactModel smarttyContactModel : adapter.getData()) {
            if (smarttyContactModel.isSelected())
                totalSelected++;
        }
        tvToolbarSubTitle.setText(String.valueOf(totalSelected) + subTitleContent);
    }

    @Override
    public void onBackPressed() {
        if (searchToolbar != null && searchToolbar.getVisibility() == View.VISIBLE) {
            hideSearchToolBar();
        } else if (totalSelected > 0) {
            CommonAlertDialog.showDialog(StatusPrivacyContactsActivity.this,
                    new CommonAlertDialog.DialogListener() {
                        @Override
                        public void onPositiveBtnClick(Object result) {
                            finish();
                            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                        }

                        @Override
                        public void onNegativeBtnClick() {

                        }
                    }, "", "Discard Changes", "DISCARD", "CANCEL", "");
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.status_privacy_contacts_home, menu);
        return true;
    }


    private void setAdapter() {
        if (adapter == null)
            adapter = new StatusPrivacyContactsAdapter(this, type, false, this);
        rvContactsList.setAdapter(adapter);
    }


    @Override
    protected void onResume() {
        super.onResume();

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event != null && event.getEventName() != null) {

            switch (event.getEventName()) {


                case SocketManager.EVENT_STATUS_ACK:


            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(StatusPrivacyContactsActivity.this);

    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(StatusPrivacyContactsActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.fab_ok:
                afterConfirm();
                break;

            case R.id.iv_select_all: {
                adapter.setSelectAll(!adapter.getSelectAll());
                setCount();
                if (adapter.getSelectAll()) {
                    fabIcon.show();
                }
            }
            break;


            case R.id.iv_search:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    circleReveal(R.id.search_tool_bar, true);
                } else {
                    toolbar.setVisibility(View.GONE);
                    searchToolbar.setVisibility(View.VISIBLE);
                }
                itemSearch.expandActionView();
                break;
        }
    }

    private void afterConfirm() {
        if (adapter != null) {
            List<SmarttyContactModel> data = adapter.getData();
            List<String> userIds = new ArrayList<>();
            List<SmarttyContactModel> selectedContacts = new ArrayList<>();
            if (data != null && data.size() > 0) {
                for (SmarttyContactModel contactModel : data) {
                    if (contactModel.isSelected()) {
                        selectedContacts.add(contactModel);
                        userIds.add(contactModel.get_id());
                    }
                }
            }

/*            //add old items also
            List<String> oldContacts = adapter.prevSelectedlist();
            if (oldContacts != null) {
                userIds.addAll(oldContacts);
            }*/

            if (userIds.size() > 0) {

                String commaSeparatedIds = TextUtils.join(",", userIds);
                MyLog.d(TAG, "afterConfirm userIds: " + commaSeparatedIds);
                switch (type) {
                    case StatusPrivacyActivity.TYPE_ONLY_SHARE_WITH:
                        //hit socket event
                        MyLog.d(TAG, "afterConfirm: TYPE_ONLY_SHARE_WITH " + selectedContacts.size());
                        StatusSocketEvents.setStatusPrivacy(this, "only_share", userIds, commaSeparatedIds);
                        finish();
                        break;

                    case StatusPrivacyActivity.TYPE_SHARE_ALL_EXCEPT:
                        MyLog.d(TAG, "afterConfirm: TYPE_SHARE_ALL_EXCEPT " + selectedContacts.size());
                        StatusSocketEvents.setStatusPrivacy(this, "my_contacts_except", userIds, commaSeparatedIds);
                        finish();
                        break;
                }

            } else {
                StatusSocketEvents.setStatusPrivacy(this, "my_contacts", null, "");
                finish();
            }

        }
    }


    public void setSearchToolBar() {

        if (searchToolbar != null) {
            searchToolbar.inflateMenu(R.menu.status_privacy_contacts_search);
            searchMenu = searchToolbar.getMenu();

            searchToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideSearchToolBar();
                }
            });

            itemSearch = searchMenu.findItem(R.id.action_filter_search);

            MenuItemCompat.setOnActionExpandListener(itemSearch, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    // Do something when collapsed
                    hideSearchToolBar();
                    return true;
                }

                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    // Do something when expanded
                    return true;
                }
            });

            initSearchView();


        } else
            MyLog.d("toolbar", "setSearchToolBar: NULL");
    }

    private void hideSearchToolBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            circleReveal(R.id.search_tool_bar, false);
        } else {
            searchToolbar.setVisibility(View.GONE);
            toolbar.setVisibility(View.VISIBLE);
        }
    }

    public void initSearchView() {
        final SearchView searchView =
                (SearchView) searchMenu.findItem(R.id.action_filter_search).getActionView();

        // Enable/Disable Submit button in the keyboard

        searchView.setSubmitButtonEnabled(false);

        // Change search close button image

        ImageView closeButton = searchView.findViewById(R.id.search_close_btn);

        closeButton.setImageResource(R.drawable.close);
        closeButton.setColorFilter(Color.argb(255, 255, 255, 255)); // White Tint
        // set the cursor

        AutoCompleteTextView searchTextView = searchView.findViewById(R.id.search_src_text);
        searchTextView.setHint("Search..");
        searchTextView.setHintTextColor(Color.WHITE);
        searchTextView.setTextColor(getResources().getColor(R.color.white));
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.search_cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
            MyLog.e(TAG, "initSearchView: ", e);
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                callSearch(query);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                callSearch(newText);
                return true;
            }
        });

    }

    private void callSearch(String query) {
        //Do searching
        Log.i("query", "" + query);
        adapter.getFilter().filter(query);
    }


    @Override
    public void onItemClick(int position, Object result, boolean isLongPress) {
        if (result != null) {
            totalSelected = AppUtils.parseInt(result.toString());
            if (totalSelected > 0) {
                fabIcon.show();
            }
            tvToolbarSubTitle.setText(String.valueOf(totalSelected) + subTitleContent);

        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void circleReveal(int viewID, final boolean isShow) {
        final View myView = findViewById(viewID);

        int width = myView.getWidth();

        //width-=(1 *getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material))-(getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material)/ 2);
        // width-=getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material);
        width -= 20;

        int cx = width;
        int cy = myView.getHeight() / 2;

        Animator anim;
        if (isShow)
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, (float) width);
        else
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, (float) width, 0);

        anim.setDuration((long) 500);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (!isShow) {
                    super.onAnimationEnd(animation);
                    myView.setVisibility(View.GONE);
                    toolbar.setVisibility(View.VISIBLE);
                }
            }
        });

        // make the view visible and start the animation
        if (isShow) {
            myView.setVisibility(View.VISIBLE);
            toolbar.setVisibility(View.GONE);
        }

        // start the animation
        anim.start();


    }

}
