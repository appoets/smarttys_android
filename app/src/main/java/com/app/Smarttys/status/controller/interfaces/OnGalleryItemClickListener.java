package com.app.Smarttys.status.controller.interfaces;

import com.app.Smarttys.status.model.StatusModel;

import java.util.HashMap;

/**
 * Created by user134 on 4/2/2018.
 */

public interface OnGalleryItemClickListener {

    void onItemClick(int totalCount, HashMap<Integer, StatusModel> selectedItems);
}
