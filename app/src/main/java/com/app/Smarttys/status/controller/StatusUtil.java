package com.app.Smarttys.status.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.ShareFromThirdPartyAppActivity;
import com.app.Smarttys.app.dialog.CommonAlertDialog;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.Keys;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.service.Constants;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.status.model.StatusHistoryModel;
import com.app.Smarttys.status.model.StatusModel;
import com.app.Smarttys.status.view.activity.StatusViewActivity;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user134 on 4/6/2018.
 */

public class StatusUtil {
    private static final String TAG = "StatusUtil";

    public static StatusModel getPojoFromJson(JSONObject objects) {
        if (objects == null) {
            return null;
        }
        StatusModel statusModel = new StatusModel();

        try {


            String from = getSafeJsonString("from", objects);
            statusModel.setUserId(from);
            if (from == null || from.isEmpty()) {
                return null;
            }
            String Status_type = getSafeJsonString("type", objects);

            String id = getSafeJsonString("msgId", objects);
            statusModel.setId(id);

            String docId = getSafeJsonString("doc_id", objects);
            statusModel.setDocId(docId);

            String caption = getSafeJsonString("payload", objects);
            statusModel.setCaption(caption);

            String fromAvatar = getSafeJsonString("From_avatar", objects);
            statusModel.setProfileURL(fromAvatar);

            long timeStamp = objects.getLong("timestamp");
            statusModel.setTimeUploaded(timeStamp);

            if (objects.has("filesize")) {
                Object aObj = objects.get("filesize");
                if (aObj instanceof String) {
                    //    long fileSize = objects.getLong("filesize");
                    statusModel.setFileSize(0);

                } else if (aObj instanceof Long) {
                    long fileSize = objects.getLong("filesize");

                    statusModel.setFileSize(fileSize);

                }

            }

            String mobileNumber = getSafeJsonString("From_msisdn", objects);
            statusModel.setMobileNumber(mobileNumber);

            String thumbNail = getSafeJsonString("thumbnail_data", objects);
            statusModel.setThumbNail(thumbNail);

            String recordId = getSafeJsonString("recordId", objects);
            statusModel.setRecordId(recordId);

            String serverPath = getSafeJsonString("thumbnail", objects);
            if (serverPath != null && serverPath.startsWith("./")) {
                serverPath = serverPath.replaceFirst("./", "");
                statusModel.setServerPath(Constants.SOCKET_IP + serverPath);
            }

         /*   if (serverPath != null && (serverPath.contains(".mp4") || serverPath.contains(".3gp")
                    || serverPath.contains(".avi")
                    || serverPath.contains(".flv")
            )) {
                statusModel.setVideo(true);
            } else {
                statusModel.setImage(true);
            }*/

            if (Status_type.equalsIgnoreCase("0")) {

                String Theme_Color = getSafeJsonString("theme_color", objects);
                String Theme_Font = getSafeJsonString("theme_font", objects);
                String text_caption = getSafeJsonString("payload", objects);

                statusModel.setTextstatus(true);
                statusModel.setImage(false);
                statusModel.setVideo(false);
                statusModel.setTextstatus_color_code(Theme_Color);
                statusModel.setTextstatus_text_font(Theme_Font);
                statusModel.setTextstatus_caption(text_caption);
                statusModel.setFileSize(0);


            } else if (Status_type.equalsIgnoreCase("1")) {

                long fileSize = objects.getLong("filesize");
                statusModel.setFileSize(fileSize);

                statusModel.setImage(true);
                statusModel.setTextstatus(false);
                statusModel.setVideo(false);

            } else if (Status_type.equalsIgnoreCase("2")) {

                long fileSize = objects.getLong("filesize");
                statusModel.setFileSize(fileSize);

                statusModel.setVideo(true);
                statusModel.setTextstatus(false);
                statusModel.setImage(false);
            }

            String msisdn = "";
            //live --> From_msisdn offline -> ContactMsisdn
            // if (isOffline)
            msisdn = getSafeJsonString("From_msisdn", objects);
            // else
            if (msisdn == null || msisdn.isEmpty())
                msisdn = getSafeJsonString("ContactMsisdn", objects);
            statusModel.setMsisdn(msisdn);


        } catch (Exception e) {
            MyLog.e(TAG, "getPojoFromJson: ", e);
        }

        return statusModel;
    }

    public static synchronized void myStatusSuccessResponse(Context context, ReceviceMessageEvent event) {
        try {
            Object[] array = event.getObjectsArray();
            if (array == null || array.length == 0)
                return;
            JSONObject objects = new JSONObject(array[0].toString());
            String docId = objects.getString("doc_id");
            MyLog.e(TAG, "objects### myStatusSuccessResponse: " + objects);

            MyLog.d(TAG, "DOCID_TEST myStatusSuccessResponse: " + docId);
            //StatusModel statusModel= getPojoFromJson(array,false);
            JSONObject dataObj = objects.getJSONObject("data");
            String recordId = dataObj.getString("recordId");
            MyLog.d(TAG, "myStatusSuccessResponse: record id " + recordId);
            MyLog.d(TAG, "myStatusSuccessResponse: docId  " + docId);
            if (docId != null && recordId != null)
                CoreController.getStatusDB(context).updateRecordId(recordId, docId);
        } catch (Exception e) {
            MyLog.e(TAG, "myStatusUpdate: ", e);
            MyLog.e(TAG, "objects###: ", e);

        }
    }


    public static void muteUnMuteResponse(Context context, ReceviceMessageEvent event) {
        try {
            Object[] array = event.getObjectsArray();
            if (array == null || array.length == 0)
                return;
            JSONObject objects = new JSONObject(array[0].toString());
            if (objects != null)
                Log.i(TAG, "muteUnMuteResponse: " + objects.toString());
        } catch (Exception e) {
            MyLog.e(TAG, "muteUnMuteResponse: ", e);
        }
    }

    public static Bitmap decodeBitmapFromBase64(String thumbnailData, int reqWidth, int reqHeight, boolean needResize) {

        try {
            if (thumbnailData.startsWith("data:image/jpeg;base64,")) {
                thumbnailData = thumbnailData.replace("data:image/jpeg;base64,", "");
            }

            byte[] decodedString = Base64.decode(thumbnailData, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            if (needResize) {
                Bitmap resizedBitmap = Bitmap.createScaledBitmap(decodedByte, reqWidth, reqHeight, true);
                return resizedBitmap;
            }
            return decodedByte;
        } catch (Exception e) {
            return null;
        }
    }

    private static String getSafeJsonString(String key, JSONObject jsonObject) {
        try {
            if (jsonObject.has(key) && jsonObject.getString(key) != null) {
                return jsonObject.getString(key);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "getSafeJsonString: ", e);
        }
        return "";
    }

    private static String getUserProfileById(Context context, String userId) {
        if (context == null || userId == null || userId.isEmpty())
            return null;
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
        return contactDB_sqlite.getSingleData(userId, ContactDB_Sqlite.AVATARIMAGEURL);
    }

    public static void statusAckResult(Context context, ReceviceMessageEvent event, String currentUserId) {
        try {
            String result = event.getObjectsArray()[0].toString();
            JSONObject object = new JSONObject(result);
            Log.e(TAG, "statusAckResult" + object);
            String from = "";
            //Check From keyword is null  or empty
            if (object.has("from")) {
                from = object.getString("from");
            }


            //update only for other users
            if (!from.equalsIgnoreCase(currentUserId) || from.equalsIgnoreCase(currentUserId)) {

                String docId = object.getString("doc_id");
                //String status = object.getString("status");
                //String timeStamp = object.getString("currenttime");
                Log.e("docId", "docId" + docId);
                String id = null;
                if (docId != null && docId.contains("--") || docId.contains("-")) {
                    if (docId.contains("--")) {
                        docId = docId.replaceFirst("--", "-");
                    }

                    String[] splitIds = docId.split("-");
                    id = splitIds[1];

                }
                /*else {
                    String[] splitIds = docId.split("-");
                    id = splitIds[1];
                }*/
                Log.e("id", "id" + id);
                //String msgId = docId + "-" + id;
                if (id != null) {


                    //cHECK STATUS TYPE IS 3
                    if (object.has("status")) {
                        String status = object.getString("status");
                        if (Integer.parseInt(status) == 3) {
                            CoreController.getStatusDB(context).updateStatusOfStatus(AppUtils.parseLong(id), MessageFactory.DELIVERY_STATUS_READ);
                        }
                    }
                }

                if (object.has("current_status")) {
                    JSONObject currentStatusObj = object.getJSONObject("current_status");
                    String convId = currentStatusObj.getString("convId");

                    CoreController.getStatusDB(context).updateConvId(docId, convId);
                    Object muted = currentStatusObj.get("muted");
                    String removed = currentStatusObj.getString("removed");

                    // muted user 1 - Muted 0 - UnMuted
                    if (muted != null && muted.equals(1)) {
                        CoreController.getStatusDB(context).insertMutedUser(from);
                    }
                }
            } else {
                MyLog.d(TAG, "statusAckResult: no need update. its current userid");
                String docId = object.getString("doc_id");
                if (object.has("current_status")) {
                    JSONObject currentStatusObj = object.getJSONObject("current_status");
                    String convId = currentStatusObj.getString("convId");

                    CoreController.getStatusDB(context).updateConvId(docId, convId);
                    Object muted = currentStatusObj.get("muted");
                    String removed = currentStatusObj.getString("removed");
                    if (muted != null && muted.equals(1)) {
                        CoreController.getStatusDB(context).insertMutedUser(from);
                    }

                }
            }
        } catch (JSONException ex) {
            MyLog.e(TAG, "statusAckResult: ", ex);
        } catch (Exception e) {
            MyLog.e(TAG, "statusAckResult: ", e);
        }
    }

    public static synchronized boolean statusDelete(StatusModel statusModel, Context context) {
        //record id available means --> its live status
        if (statusModel.getRecordId() != null && !statusModel.getRecordId().isEmpty()) {

            SendMessageEvent messageEvent = new SendMessageEvent();

            messageEvent.setEventName(SocketManager.EVENT_STATUS_DELETE);
            try {
                JSONObject deleteMsgObj = new JSONObject();
                deleteMsgObj.put("from", statusModel.getUserId());
                deleteMsgObj.put("recordId", statusModel.getRecordId());
                deleteMsgObj.put("status", "2");
                messageEvent.setMessageObject(deleteMsgObj);
                EventBus.getDefault().post(messageEvent);
            } catch (Exception e) {
                MyLog.e(TAG, "statusDelete: ", e);
            }
/*                if (statusModel.getDocId() != null) {
                    CoreController.getStatusDB(context).deleteStatus(statusModel.getDocId());
                }*/


            return false;

        }
        //record id not available means --> if not updated on server. It only available in local. so delete local only
        else {
            if (statusModel.getDocId() != null) {
                CoreController.getStatusDB(context).deleteStatus(statusModel.getDocId());
                return true;
            }
        }
        return false;
    }

    public static void clearPicassoCache(String filePath, Context context) {
        try {
            Picasso.with(context).invalidate(filePath);
        } catch (Exception e) {
            MyLog.e(TAG, "clearPicassoCache: ", e);
        }
    }

    public static synchronized boolean afterStatusDelete(ReceviceMessageEvent event, Context context) {
        try {
            String result = event.getObjectsArray()[0].toString();
            JSONObject object = new JSONObject(result);
            MyLog.e(TAG, "afterStatusDelete: " + object);
            if (object.has("doc_id")) {
                String doc_id = object.getString("doc_id");
                CoreController.getStatusDB(context).deleteStatus(doc_id);
            }
            return true;

        } catch (Exception e) {
            MyLog.e(TAG, "afterStatusDelete: ", e);
        }
        return false;
    }


    public static synchronized void afterMuteResponse(ReceviceMessageEvent event, Context context) {
        try {
            String result = event.getObjectsArray()[0].toString();
            JSONObject object = new JSONObject(result);
            String mutedUserId = object.getString("to");
            CoreController.getStatusDB(context).updateUserMuted(mutedUserId);
        } catch (Exception e) {
            MyLog.e(TAG, "afterMuteResponse: ", e);
        }
    }


    public static synchronized void afterFetchAllResponse(ReceviceMessageEvent event, Context context) {
        try {
            String result = event.getObjectsArray()[0].toString();
            JSONObject object = new JSONObject(result);

        } catch (Exception e) {
            MyLog.e(TAG, "afterMuteResponse: ", e);
        }
    }

    public static void setProfileImage(ImageView imageView, Context context, String profileUrl, String userId) {
        try {
            if (null == context)
                return;
            if (profileUrl != null && !profileUrl.isEmpty()) {
                AppUtils.loadImageSmooth(context, profileUrl, imageView, 100, R.drawable.placeholder_icon);

            } else {

//                profileUrl= StatusUtil.getUserProfileById(context,userId);


                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
                String imageTS = contactDB_sqlite.getDpUpdatedTime(userId);
                if (imageTS == null || imageTS.isEmpty())
                    imageTS = "1";
                profileUrl = Constants.USER_PROFILE_URL + userId + ".jpg?id=" + imageTS;
                if (!profileUrl.isEmpty()) {
                    MyLog.d(TAG, "onBindViewHolder profile from Db: " + profileUrl);
                    AppUtils.LoadImage(context, profileUrl, imageView, R.drawable.placeholder_icon);
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "setProfileImage: ", e);
        }
    }

    public static synchronized void statusAckByOthers(Context context, ReceviceMessageEvent event, String currentUserId) {
        try {
            MyLog.d(TAG, "statusAckByOthers: ");
            String result = event.getObjectsArray()[0].toString();
            JSONObject object = new JSONObject(result);
            String from = object.getString("from");
            String docId = object.getString("doc_id");
            String status = object.getString("status");
            MyLog.d(TAG, "DOCID_TEST statusAckByOthers docId: " + docId);
            //update only for other users
            if (!from.equalsIgnoreCase(currentUserId)) {
                MyLog.d(TAG, "statusAckByOthers: not current user " + from);

                String chatId = object.getString("doc_id");
                //   String status = object.getString("status");
                String timeStamp = object.getString("currenttime");

                Log.e("chatId", "chatId" + chatId);
                if (chatId != null) {

                    String[] splitIds = chatId.split("-");
                    String id = splitIds[1];

                    Log.e("id", "id" + id);
                    Log.e("status", "status" + status);


                    if (status != null && status.equals(MessageFactory.DELIVERY_STATUS_READ)) {
                        CoreController.getStatusDB(context).updateStatusOfStatus(AppUtils.parseLong(id), MessageFactory.DELIVERY_STATUS_READ);
                        StatusHistoryModel statusHistoryModel = new StatusHistoryModel();
                        statusHistoryModel.setUserId(from);
                        statusHistoryModel.setViewedAt(AppUtils.parseLong(timeStamp));
                        String name = StatusUtil.getNameById(context, from);
                        if (name != null)
                            statusHistoryModel.setName(name);
                        String profile = StatusUtil.getUserProfileById(context, from);
                        statusHistoryModel.setProfileUrl(AppUtils.getValidProfilePath(profile));

                        MyLog.d(TAG, "statusAckByOthers: timestamp: " + timeStamp);
                        //String localPath="";
                        //String caption="";
                        CoreController.getStatusDB(context).addStatusViewedHistory(docId, statusHistoryModel);
                    }
                }


            } else {
                MyLog.d(TAG, "statusAckResult: no need update. its current userid");
            }
        } catch (Exception e) {
            MyLog.e(TAG, "statusAckResult: ", e);
        }
    }


    public static synchronized void statusAckByOthersOffline(Context context, ReceviceMessageEvent event, String currentUserId) {
        try {
//            MyLog.d(TAG, "statusAckByOthers: ");
            String result = event.getObjectsArray()[0].toString();
            JSONObject object = new JSONObject(result);
            Log.e(TAG, "statusAckByOthers: " + object);

            String err = object.getString("err");

            if (Integer.parseInt(err) == 0) {
                JSONArray a = object.getJSONArray("statusDetails");
                if (a.length() > 0) {
                    for (int i = 0; i < a.length(); i++) {
                        JSONObject item = a.getJSONObject(i);
                        String mRecordId = item.getString("_id");
                        String doc_id = item.getString("doc_id");
                        String mMsgId = "";
                        if (doc_id != null) {

                            String[] splitIds = doc_id.split("-");
                            mMsgId = splitIds[1];

                            Log.e("mMsgId", "mMsgId" + mMsgId);
                        }
                        JSONArray items = item.getJSONArray("to");
                        if (items.length() > 0) {
                            for (int z = 0; z < items.length(); z++) {
                                JSONObject data = items.getJSONObject(z);
                                String status = data.getString("status");
                                //Check it is 3
                                if (Integer.parseInt(status) == 3) {
                                    //Update Db
                                    String time_to_deliever = data.getString("time_to_deliever");
                                    String convId = data.getString("convId");
                                    String time_to_seen = data.getString("time_to_seen");
                                    String id = data.getString("id");

                                    MyLog.d(TAG, "statusAckByOthers:  current user " + id);

                                    if (status != null && status.equals(MessageFactory.DELIVERY_STATUS_READ)) {

                                        //    CoreController.getStatusDB(context).updateStatusRecordId((mRecordId), MessageFactory.DELIVERY_STATUS_READ);
                                        CoreController.getStatusDB(context).updateStatusOfStatus(AppUtils.parseLong(mMsgId), MessageFactory.DELIVERY_STATUS_READ);


                                        //  CoreController.getStatusDB(context).updateStatusOfStatus(AppUtils.parseLong(id), MessageFactory.DELIVERY_STATUS_READ);

                                        StatusHistoryModel statusHistoryModel = new StatusHistoryModel();
                                        statusHistoryModel.setUserId(id);
                                        statusHistoryModel.setViewedAt(AppUtils.parseLong(time_to_seen));
                                        String name = StatusUtil.getNameById(context, id);
                                        if (name != null)
                                            statusHistoryModel.setName(name);
                                        String profile = StatusUtil.getUserProfileById(context, id);
                                        statusHistoryModel.setProfileUrl(AppUtils.getValidProfilePath(profile));

                                        MyLog.d(TAG, "statusAckByOthers: timestamp: " + time_to_seen);
                                        //String localPath="";
                                        //String caption="";
                                        //  CoreController.getStatusDB(context).addStatusViewedHistoryUser(mRecordId,statusHistoryModel);
                                        CoreController.getStatusDB(context).addStatusViewedHistory(doc_id, statusHistoryModel);

                                        List<StatusModel> myStatusList = CoreController.getStatusDB(context).getMyAllStatus();
                                        if (myStatusList != null && myStatusList.size() > 0) {
                                            Log.e(TAG, "myStatusList: myStatusList: " + myStatusList.size());
                                            Log.e(TAG, "myStatusList: myStatusList: " + myStatusList.toString());
                                        }
                                    }


                                   /* if (!from.equalsIgnoreCase(currentUserId)) {
                                        MyLog.d(TAG, "statusAckByOthers: not current user " + from);

                                    }else {
                                        MyLog.d(TAG, "statusAckByOthers: not current user " + from);
                                    }*/


                                }

                            }
                        }
                    }
                }
            }
         /*   String from = object.getString("from");
            String docId = object.getString("doc_id");
            String status = object.getString("status");
            MyLog.d(TAG, "DOCID_TEST statusAckByOthers docId: " + docId);*/
            //update only for other users

         /*   if (!from.equalsIgnoreCase(currentUserId)) {
                MyLog.d(TAG, "statusAckByOthers: not current user " + from);
*/
            /*    String chatId = object.getString("doc_id");
                //   String status = object.getString("status");
                String timeStamp = object.getString("currenttime");

                Log.e("chatId", "chatId" + chatId);
                if (chatId != null) {

                    String[] splitIds = chatId.split("-");
                    String id = splitIds[1];

                    Log.e("id", "id" + id);
                    Log.e("status", "status" + status);


                    if (status != null && status.equals(MessageFactory.DELIVERY_STATUS_READ)) {
                        CoreController.getStatusDB(context).updateStatusOfStatus(AppUtils.parseLong(id), MessageFactory.DELIVERY_STATUS_READ);
                        StatusHistoryModel statusHistoryModel = new StatusHistoryModel();
                        statusHistoryModel.setUserId(from);
                        statusHistoryModel.setViewedAt(AppUtils.parseLong(timeStamp));
                        String name = StatusUtil.getNameById(context, from);
                        if (name != null)
                            statusHistoryModel.setName(name);
                        String profile = StatusUtil.getUserProfileById(context, from);
                        statusHistoryModel.setProfileUrl(AppUtils.getValidProfilePath(profile));

                        MyLog.d(TAG, "statusAckByOthers: timestamp: " + timeStamp);
                        //String localPath="";
                        //String caption="";
                        CoreController.getStatusDB(context).addStatusViewedHistory(docId, statusHistoryModel);
                    }
                }
*/

         /*   } else {
                MyLog.d(TAG, "statusAckResult: no need update. its current userid");
            }*/
        } catch (Exception e) {
            MyLog.e(TAG, "statusAckResult: ", e);
        }
    }

    public static void shareImageOrVideo(String caption, Uri uri, boolean isImage, Context context) {
        try {
            Intent shareIntent = new Intent(context, ShareFromThirdPartyAppActivity.class);
            shareIntent.setAction(Intent.ACTION_SEND);
            if (caption != null)
                shareIntent.putExtra(Intent.EXTRA_TEXT, caption);
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
            if (isImage)
                shareIntent.setType("image/*");
            else
                shareIntent.setType("video/*");

            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(shareIntent);
        } catch (Exception e) {
            MyLog.e(TAG, "shareImageOrVideo: ", e);
        }
    }


    public static void shareMultipleStatus(ArrayList<StatusModel> data, Context context) {
        try {
            Intent shareIntent = new Intent(context, ShareFromThirdPartyAppActivity.class);
            shareIntent.setAction(Keys.ACTION_STATUS_MESSAGES);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Intent.EXTRA_TEXT, data);
            shareIntent.putExtras(bundle);
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            shareIntent.setType("image/*");
            context.startActivity(shareIntent);
        } catch (Exception e) {
            MyLog.e(TAG, "shareImageOrVideo: ", e);
        }
    }

    public static String getNameById(Context context, String userId) {
        try {
            if (context == null || userId == null || userId.isEmpty())
                return null;
            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
            return contactDB_sqlite.getSingleData(userId, ContactDB_Sqlite.FIRSTNAME);
        } catch (Exception e) {
            return null;
        }
    }

    public static long getVideoDuration(String videoPath, boolean isServerUrl) {
        long timeInMillis = 0;
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            if (isServerUrl)
                retriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                retriever.setDataSource(videoPath);
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            timeInMillis = Long.parseLong(time);
        } catch (Exception e) {
            MyLog.e(TAG, "getVideoDuration: ", e);
        }
        return timeInMillis;
    }

    public static synchronized void setImage(Context context, ImageView imageView, StatusModel statusModel) {
        if (statusModel != null) {
            String imageURL;
            //image status
            if (statusModel.isImage()) {
                imageURL = statusModel.getLocalPath();
                AppUtils.loadLocalImage(context, imageURL, imageView);
            }
            //video status
            else {
                //Uri videoUri = Uri.fromFile(new File(statusModel.getLocalPath()));
                Bitmap videoThumb = AppUtils.getThumbnailFromVideo(statusModel.getLocalPath());
                imageView.setImageBitmap(videoThumb);
            }
        }
    }

    public static void showMuteUnDialog(Context context, StatusModel selectedItemData, CommonAlertDialog.DialogListener dialogListener) {
        if (context == null || selectedItemData == null)
            return;
        String userId = selectedItemData.getUserId();
        String userName = StatusUtil.getNameById(context, userId);
        String mCurrentUserId = SessionManager.getInstance(context).getCurrentUserID();
        if (userName == null)
            userName = "";
        if (userName.isEmpty()) {
            userName = selectedItemData.getMsisdn();
        }
        if (userId != null && !userId.equals(mCurrentUserId)) {
            boolean isMutedUser = CoreController.getStatusDB(context).isMutedUser(userId);
            String dialogTitle = "", dialogMsg = "", okBtnText = "", cancelBtnText = "CANCEL";
            if (isMutedUser) {
                //unmute msg
                dialogMsg = context.getString(R.string.unmute_dialog_text1) + userName +
                        context.getString(R.string.unmute_dialog_text2);
                dialogTitle = "Unmute " + userName + " status updates?";
                okBtnText = "UNMUTE";
            } else {
                //mute msg
                dialogMsg = context.getString(R.string.mute_dialog_text1) + userName +
                        context.getString(R.string.mute_dialog_text2);
                dialogTitle = "Mute " + userName + " status updates?";
                okBtnText = "MUTE";
            }
            CommonAlertDialog.showDialog(context, dialogListener, dialogTitle, dialogMsg, okBtnText, cancelBtnText, selectedItemData);
        }
    }

    public static void muteUnmute(Context context, StatusModel selectedItemData) {
        String userId = selectedItemData.getUserId();
        if (userId != null) {
            boolean isMutedUser = CoreController.getStatusDB(context).isMutedUser(userId);
            //already muted user -> so unmute
            if (isMutedUser) {
                StatusSocketEvents.muteUnMuteStatus(context, selectedItemData.getUserId(), selectedItemData.getDocId(), false, true);
            }
            //normal user previously --> so mute
            else {
                StatusSocketEvents.muteUnMuteStatus(context, selectedItemData.getUserId(), selectedItemData.getDocId(), true, false);
            }
        }
    }

    public static void startStatusViewActivity(Activity context, StatusModel statusModel, boolean isMyStatus, boolean isMySingleStatus, boolean showPopup) {
        Intent intent = new Intent(context, StatusViewActivity.class);

        if (statusModel != null) {
            intent.putExtra(Keys.USER_ID, statusModel.getUserId());
            intent.putExtra(Keys.MY_STATUS, isMyStatus);
            intent.putExtra(Keys.MY_STATUS_SINGLE, isMySingleStatus);
            intent.putExtra(Keys.ID, statusModel.getId());
            long statusUploadedTime = statusModel.getTimeUploaded();
            String statusTime = AppUtils.getStatusTime(context, statusUploadedTime, false);
            intent.putExtra(Keys.STATUS_UPLOADED_TIME, statusTime);
            intent.putExtra(Keys.SHOW_POPUP, showPopup);
            context.startActivityForResult(intent, Constants.STATUS_DELETE_REQUEST);
        }
    }
}
