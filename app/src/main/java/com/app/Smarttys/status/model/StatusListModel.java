package com.app.Smarttys.status.model;

/**
 * Created by user134 on 3/27/2018.
 */

public class StatusListModel {
    private String id;
    private String profileUrl;
    private String name;
    private long updatedTIme;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getUpdatedTIme() {
        return updatedTIme;
    }

    public void setUpdatedTIme(long updatedTIme) {
        this.updatedTIme = updatedTIme;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }
}
