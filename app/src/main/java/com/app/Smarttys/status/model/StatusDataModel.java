package com.app.Smarttys.status.model;

import java.util.List;

/**
 * Created by user134 on 4/4/2018.
 */

public class StatusDataModel {

    private List<StatusModel> data;

    public List<StatusModel> getData() {
        return data;
    }

    public void setData(List<StatusModel> data) {
        this.data = data;
    }
}
