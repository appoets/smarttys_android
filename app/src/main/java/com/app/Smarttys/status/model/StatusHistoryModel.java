package com.app.Smarttys.status.model;

import java.io.Serializable;

/**
 * Created by user134 on 4/9/2018.
 */

public class StatusHistoryModel implements Serializable {
    private String userId;
    private long viewedAt;
    private String name;
    private String profileUrl;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getViewedAt() {
        return viewedAt;
    }

    public void setViewedAt(long viewedAt) {
        this.viewedAt = viewedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }
}
