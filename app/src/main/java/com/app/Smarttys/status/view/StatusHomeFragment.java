package com.app.Smarttys.status.view;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.NewHomeScreenActivty;
import com.app.Smarttys.app.dialog.CommonAlertDialog;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.Keys;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.WhatsappStatusCircle;
import com.app.Smarttys.app.widget.CircleImageView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.connectivity.NetworkChangeReceiver;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.service.Constants;
import com.app.Smarttys.core.socket.MessageService;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;
import com.app.Smarttys.status.controller.SharedPrefUtil;
import com.app.Smarttys.status.controller.StatusData;
import com.app.Smarttys.status.controller.StatusSocketEvents;
import com.app.Smarttys.status.controller.StatusUploadUtil;
import com.app.Smarttys.status.controller.StatusUtil;
import com.app.Smarttys.status.controller.interfaces.MyInternetListener;
import com.app.Smarttys.status.controller.interfaces.OnListClickListener;
import com.app.Smarttys.status.model.StatusModel;
import com.app.Smarttys.status.view.activity.MyStatusListActivity;
import com.app.Smarttys.status.view.activity.StatusAddActivity;
import com.app.Smarttys.status.view.activity.StatusPrivacyActivity;
import com.app.Smarttys.status.view.activity.StatusViewActivity;
import com.app.Smarttys.status.view.adapter.StatusAdapter;
import com.app.Smarttys.textstatus.CircleView.CircularStatusView;
import com.app.Smarttys.textstatus.TextStatusActivity;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by user134 on 3/27/2018.
 */

public class StatusHomeFragment extends Fragment implements OnListClickListener, View.OnClickListener, MyInternetListener, CommonAlertDialog.DialogListener {

    public final static String TYPE_RECENT = "type_recent";
    public final static String TYPE_VIEWED = "type_viewed";
    public final static String TYPE_MUTED = "type_muted";
    private static final String TAG = StatusHomeFragment.class.getSimpleName();
    private static final int STATUS_RUNTIME_PERMISSION_CODE = 10;
    private static final int SEARCH_VISIBLITY_DELAY = 200;
    private RecyclerView mRvViewed, mRvRecent, mRvMuteList;
    private String profileImage = "https://www.thefamouspeople.com/profiles/thumbs/sundar-pichai-1.jpg";
    private boolean isMyStatusAvailable = false;
    private String from, mCurrentUserId, currentUserName;
    private SessionManager sessionManager;
    private SearchView searchView;
    private ContactDB_Sqlite contactDB_sqlite;
    private FileUploadDownloadManager fileUploadDownloadMgnr;
    private ImageView ivStatusAddSmallIcon, ivMyStatusDetail;
    private CircleImageView ivMyStatus;
    private WhatsappStatusCircle whatsappStatusCircleView;
    private int myTotalStatusCount = 0;
    private TextView tvRecentUpdatesTitle, tvViewedUpdatesTitle, tvMuteListTitle;
    private boolean appOpen;
    private TextView tvMyStatusDesc;
    private Handler mHandler;
    private AutoCompleteTextView searchTextView;
    private boolean isNetworkFailed = false;
    private View myStatusContainer;
    private TextView tvSearchEmptyMsg;
    private StatusAdapter adapterRecent, adapterViewed, adapterMute;
    private NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();

    //Text Status
    private RelativeLayout status_text_layout;
    private de.hdodenhof.circleimageview.CircleImageView status_text_image;
    private TextView text_status_caption;
    private CircularStatusView circular_status_view;

    @Override
    public void onItemClick(int position, Object result, boolean isLongClick) {
        try {
            StatusModel selectedItemData = null;
            if (result instanceof StatusModel)
                selectedItemData = (StatusModel) result;
            //for item click
            if (selectedItemData != null) {
                if (isLongClick) {
                    //Long press only for --> only for other persons mute/unmute
                    StatusUtil.showMuteUnDialog(getActivity(), selectedItemData, this);
                } else {
                    startStatusViewActivity(selectedItemData.getId(), selectedItemData.getUserId(),
                            //     AppUtils.getStatusTime(getActivity(), selectedItemData.getTimeUploaded()), false);
                            AppUtils.getStatusTime(getActivity(), selectedItemData.getTimeUploaded(), false), false);

                }
            }
            //for search filter result --> we use same interface (this is not click)
            // here we take "position" as count
            else {
                String typeOfStatus = (String) result;
                switch (typeOfStatus) {
                    case TYPE_MUTED:
                        if (position == 0) {
                            tvMuteListTitle.setVisibility(View.GONE);
                            mRvMuteList.setVisibility(View.GONE);
                        } else {
                            tvMuteListTitle.setVisibility(View.VISIBLE);
                            mRvMuteList.setVisibility(View.VISIBLE);
                        }
                        break;
                    case TYPE_RECENT:
                        if (position == 0) {
                            tvRecentUpdatesTitle.setVisibility(View.GONE);
                            mRvRecent.setVisibility(View.GONE);
                        } else {
                            tvRecentUpdatesTitle.setVisibility(View.VISIBLE);
                            mRvRecent.setVisibility(View.VISIBLE);
                        }
                        break;
                    case TYPE_VIEWED:
                        if (position == 0) {
                            tvViewedUpdatesTitle.setVisibility(View.GONE);
                            mRvViewed.setVisibility(View.GONE);
                        } else {
                            tvViewedUpdatesTitle.setVisibility(View.VISIBLE);
                            mRvViewed.setVisibility(View.VISIBLE);
                        }
                        break;
                }
                mHandler.postDelayed(() -> {
                    if (mRvMuteList.getVisibility() != View.VISIBLE && mRvRecent.getVisibility() != View.VISIBLE
                            && mRvViewed.getVisibility() != View.VISIBLE) {
                        if (searchTextView != null) {
                            String searchText = searchTextView.getText().toString();
                            tvSearchEmptyMsg.setText("No search results found for '" + searchText + "'");
                        }
                        tvSearchEmptyMsg.setVisibility(View.VISIBLE);
                    } else {
                        tvSearchEmptyMsg.setVisibility(View.GONE);
                    }
                }, 300);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "onItemClick: ", e);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_status_home, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        MyLog.d(TAG, "onStart: performance");
        EventBus.getDefault().register(StatusHomeFragment.this);
        registerNetworkReceiver();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(StatusHomeFragment.this);
        unRegisterReceiver();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyLog.d(TAG, "onViewCreated: ");
        sessionManager = SessionManager.getInstance(getActivity());
        mHandler = new Handler();
        contactDB_sqlite = new ContactDB_Sqlite(getActivity());
        from = sessionManager.getCurrentUserID();
        currentUserName = sessionManager.getnameOfCurrentUser();
        mCurrentUserId = sessionManager.getCurrentUserID();

        MyLog.d(TAG, "onViewCreated: currentUserId: " + mCurrentUserId);
        fileUploadDownloadMgnr = new FileUploadDownloadManager(getActivity());
        //testing purpose
        //CoreController.getStatusDB(getActivity()).clearStatusTable();

        SharedPrefUtil.setStatusRunning(false);

        ivMyStatusDetail = view.findViewById(R.id.iv_my_status_details);
        view.findViewById(R.id.tv_my_status).setOnClickListener(this);
        view.findViewById(R.id.iv_add_status).setOnClickListener(this);
        tvMyStatusDesc = view.findViewById(R.id.tv_my_status_desc);
        tvMyStatusDesc.setOnClickListener(this);
        view.findViewById(R.id.fab_add_status).setOnClickListener(this);

        view.findViewById(R.id.fab_text_status).setOnClickListener(this);
        //Check app name is noble net
        if (getString(R.string.app_name).equalsIgnoreCase("Nobel Net")) {
            view.findViewById(R.id.fab_text_status).setVisibility(View.VISIBLE);
        } else {
            view.findViewById(R.id.fab_text_status).setVisibility(View.GONE);
        }
        status_text_layout = view.findViewById(R.id.status_text_layout);
        circular_status_view = view.findViewById(R.id.circular_status_view);
        status_text_image = view.findViewById(R.id.status_text_image);
        text_status_caption = view.findViewById(R.id.text_status_caption);
        circular_status_view = view.findViewById(R.id.circular_status_view);

        status_text_layout.setOnClickListener(this);

        ivMyStatusDetail.setOnClickListener(this);

        view.findViewById(R.id.my_status_click_area).setOnClickListener(this);
        tvSearchEmptyMsg = view.findViewById(R.id.search_empty_msg);
        myStatusContainer = view.findViewById(R.id.my_status_container);
        tvRecentUpdatesTitle = view.findViewById(R.id.tv_recent_updates);
        tvViewedUpdatesTitle = view.findViewById(R.id.tv_viewed_updates);
        tvMuteListTitle = view.findViewById(R.id.tv_muted_status);
        mRvRecent = view.findViewById(R.id.rv_status_recent);
        mRvViewed = view.findViewById(R.id.rv_status_viewed);
        mRvMuteList = view.findViewById(R.id.rv_status_mute);
        ivStatusAddSmallIcon = view.findViewById(R.id.iv_add_icon_small);
        whatsappStatusCircleView = view.findViewById(R.id.whatsapp_status_circle);
        ivMyStatus = view.findViewById(R.id.iv_add_status);
        mRvRecent.setLayoutManager(AppUtils.getDefaultLayoutManger(getActivity()));
        mRvViewed.setLayoutManager(AppUtils.getDefaultLayoutManger(getActivity()));
        mRvMuteList.setLayoutManager(AppUtils.getDefaultLayoutManger(getActivity()));
        ViewCompat.setNestedScrollingEnabled(mRvRecent, false);
        ViewCompat.setNestedScrollingEnabled(mRvViewed, false);
        ViewCompat.setNestedScrollingEnabled(mRvMuteList, false);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MyLog.d(TAG, "onCreateOptionsMenu: --Home fragment---");
        if (menu != null)
            menu.clear();
        getActivity().getMenuInflater().inflate(R.menu.fragment_status, menu);
//        MenuItem add_shortcut = menu.findItem(R.id.status_privacy);
        int result = CoreController.getStatusDB(getActivity()).deleteOldStatus(); // delete old status
        MyLog.d(TAG, "onCreateOptionsMenu: deleted result " + result);
        MenuItem searchItem = menu.findItem(R.id.search_icon);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        setSearchViewCollapseExpand(menu);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.setIconifiedByDefault(true);
                searchView.setIconified(true);
                searchView.setQuery("", false);
                searchView.clearFocus();
                setSearchResult(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setSearchResult(newText);
                return true;
            }
        });
        searchView.setOnCloseListener(() -> {
            tvSearchEmptyMsg.setVisibility(View.GONE);
            menu.findItem(R.id.status_privacy).setVisible(true);
            return false;
        });
        searchView.setOnSearchClickListener(v -> menu.findItem(R.id.status_privacy).setVisible(false));
        searchView.setIconifiedByDefault(true);
        searchView.setQuery("", false);
        searchView.clearFocus();
        searchView.setIconified(true);

        searchTextView = searchView.findViewById(R.id.search_src_text);
        searchTextView.setTextColor(Color.WHITE);

        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, 0);
        } catch (Exception e) {
            MyLog.e(TAG, "onCreateOptionsMenu: ", e);
        }
    }

    private void setSearchResult(String newText) {
        if (newText != null && newText.length() > 0) {
            MyLog.d(TAG, "StatusAdapter setSearchResult: " + newText);
            myStatusContainer.setVisibility(View.GONE);
            adapterRecent.getFilter().filter(newText);
            adapterViewed.getFilter().filter(newText);
            adapterMute.getFilter().filter(newText);
        } else {
            tvSearchEmptyMsg.setVisibility(View.GONE);
            myStatusContainer.setVisibility(View.VISIBLE);
            refreshStatus();
        }
    }

    private void setSearchViewCollapseExpand(Menu menu) {
        MenuItem searchItem = menu.findItem(R.id.chatsc_searchIcon);
        if (searchItem != null) {
            MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    NewHomeScreenActivty homeScreen = (NewHomeScreenActivty) getActivity();
                    homeScreen.getSupportActionBar().show();
                    return true;
                }

                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    NewHomeScreenActivty homeScreen = (NewHomeScreenActivty) getActivity();
                    homeScreen.getSupportActionBar().hide();
                    return true;
                }
            });
            MenuItemCompat.setActionView(searchItem, searchView);
        }
    }

    private void registerNetworkReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        getActivity().registerReceiver(networkChangeReceiver, intentFilter);
    }

    private void unRegisterReceiver() {
        getActivity().unregisterReceiver(networkChangeReceiver);
    }

    private List<StatusModel> getOtherStatus(String type) {
        LinkedList<StatusModel> result = new LinkedList<>();
        LinkedList<String> uniqueUsersList = CoreController.getStatusDB(getActivity()).getStatusUserIds();
        if (uniqueUsersList != null) {
            //remove current user id --> (we need only other status list for recent/viewed)
            uniqueUsersList.remove(mCurrentUserId);
        }
        //no others status available
        if (uniqueUsersList == null || uniqueUsersList.size() == 0) {
            switch (type) {
                case TYPE_RECENT:
                    tvRecentUpdatesTitle.setVisibility(View.GONE);
                    mRvRecent.setVisibility(View.GONE);
                    break;
                case TYPE_VIEWED:
                    tvViewedUpdatesTitle.setVisibility(View.GONE);
                    mRvViewed.setVisibility(View.GONE);
                    break;
                case TYPE_MUTED:
                    tvMuteListTitle.setVisibility(View.GONE);
                    mRvMuteList.setVisibility(View.GONE);
                    break;
            }
        }
        //status available
        else {
            switch (type) {
                case TYPE_RECENT:
                    tvRecentUpdatesTitle.setVisibility(View.VISIBLE);
                    mRvRecent.setVisibility(View.VISIBLE);
                    break;
                case TYPE_VIEWED:
                    tvViewedUpdatesTitle.setVisibility(View.VISIBLE);
                    mRvViewed.setVisibility(View.VISIBLE);
                    break;
                case TYPE_MUTED:
                    tvMuteListTitle.setVisibility(View.VISIBLE);
                    mRvMuteList.setVisibility(View.VISIBLE);
                    break;
            }
            for (String userId : uniqueUsersList) {
                StatusModel statusModel = CoreController.getStatusDB(getActivity()).getRecentStatus(userId);
                boolean isMutedUser = CoreController.getStatusDB(getActivity()).isMutedUser(statusModel.getUserId());
                int totalCount = CoreController.getStatusDB(getActivity()).getStatusCount(userId);
                int unViewedStatusCount = CoreController.getStatusDB(getActivity()).getUnViewedStatusCount(statusModel.getUserId());
                int viewedCount = totalCount - unViewedStatusCount;
                //status viewed
                if (type.equals(TYPE_VIEWED)) {
                    if ((totalCount == viewedCount) && !isMutedUser) {
                        result.add(statusModel);
                    }
                } else if (type.equals(TYPE_RECENT)) {
                    if ((totalCount > viewedCount) && !isMutedUser)
                        result.add(statusModel);
                } else if (type.equals(TYPE_MUTED)) {
                    if (isMutedUser) {
                        result.add(statusModel);
                    }
                }
            }
            switch (type) {
                case TYPE_RECENT:
                    if (result.size() == 0) {
                        tvRecentUpdatesTitle.setVisibility(View.GONE);
                        mRvRecent.setVisibility(View.GONE);
                    }
                    break;
                case TYPE_VIEWED:
                    if (result.size() == 0) {
                        tvViewedUpdatesTitle.setVisibility(View.GONE);
                        mRvViewed.setVisibility(View.GONE);
                    }
                    break;
                case TYPE_MUTED:
                    if (result.size() == 0) {
                        tvMuteListTitle.setVisibility(View.GONE);
                        mRvMuteList.setAdapter(null);
                        mRvMuteList.setVisibility(View.GONE);
                    }
                    break;
            }
        }
        return result;
    }

    private void setRecentAdapter() {
        //Check recent status
        List<StatusModel> mList = getOtherStatus(TYPE_RECENT);
        MyLog.e(TAG, "Recent" + mList.size());
        adapterRecent = new StatusAdapter(getActivity(), getOtherStatus(TYPE_RECENT), TYPE_RECENT, false, this);
        mRvRecent.setAdapter(adapterRecent);
        adapterRecent.notifyDataSetChanged();
    }

    private void setViewedAdapter() {
        List<StatusModel> mList = getOtherStatus(TYPE_RECENT);
        MyLog.e(TAG, "Viewed" + mList.size());
        adapterViewed = new StatusAdapter(getActivity(), getOtherStatus(TYPE_VIEWED), TYPE_VIEWED, false, this);
        mRvViewed.setAdapter(adapterViewed);
        adapterViewed.notifyDataSetChanged();
    }

    private void setMuteListAdapter() {
        List<StatusModel> mList = getOtherStatus(TYPE_RECENT);
        MyLog.e(TAG, "MuteList" + mList.size());
        adapterMute = new StatusAdapter(getActivity(), getOtherStatus(TYPE_MUTED), TYPE_MUTED, true, this);
        mRvMuteList.setAdapter(adapterMute);
        adapterMute.notifyDataSetChanged();
    }

    private void startStatusViewActivity(String id, String userId, String uploadedTime, boolean isMyStatus) {
        Intent intent = new Intent(getActivity(), StatusViewActivity.class);
        intent.putExtra(Keys.ID, id);
        intent.putExtra(Keys.USER_ID, userId);
        intent.putExtra(Keys.STATUS_UPLOADED_TIME, uploadedTime);
        intent.putExtra(Keys.MY_STATUS, isMyStatus);
        startActivity(intent);
    }

  /*  private void startMyStatusListActivity() {
        Intent intent = new Intent(getActivity(), MyStatusListActivity.class);
        context.startActivityForResult(intent,9);
        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish(); // close this activity and return to preview activity (if there is any)
        } else if (item.getItemId() == R.id.status_privacy) {
            startPrivacyActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    private void startPrivacyActivity() {
        Intent intent = new Intent(getContext(), StatusPrivacyActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_my_status:
                startNextPage();
                break;
            case R.id.iv_add_status:
                startNextPage();
                break;
            case R.id.tv_my_status_desc:
                startNextPage();
                break;
            case R.id.my_status_click_area:
                startNextPage();
                break;
            case R.id.iv_my_status_details:
                startMyStatusListActivity(getActivity());
                break;
            case R.id.fab_add_status:
                startStatusAddPage();
                break;
            case R.id.fab_text_status:
                Intent text_status = new Intent(getActivity(), TextStatusActivity.class);
                startActivity(text_status);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MyLog.e(TAG, "onActivityResult");
      /*  if (requestCode == 9) {
            if (resultCode == RESULT_OK) {
               CoreActiviy.showProgres(getContext());
            }
        }*/
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void startMyStatusListActivity(FragmentActivity activity) {
        Intent intent = new Intent(getActivity(), MyStatusListActivity.class);
        activity.startActivityForResult(intent, 9);
        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    private void startNextPage() {
        boolean isMyStatus = false;
        StatusModel statusModel = CoreController.getStatusDB(getActivity()).getRecentStatus(mCurrentUserId);
        if (statusModel != null) {
            if (statusModel.getUserId() != null && statusModel.getUserId().equals(mCurrentUserId))
                isMyStatus = true;
            String recentStatusTime = AppUtils.getStatusTime(getActivity(), statusModel.getTimeUploaded(), true);
            startStatusViewActivity(statusModel.getId(), statusModel.getUserId(), recentStatusTime, isMyStatus);
        } else {
            startStatusAddPage();
        }
    }

    private boolean checkMyStatusAvailable() {
        MyLog.d(TAG, "checkMyStatusAvailable: ");
        myTotalStatusCount = CoreController.getStatusDB(getActivity()).getStatusCount(mCurrentUserId);
        isMyStatusAvailable = myTotalStatusCount > 0;
        MyLog.d(TAG, "checkMyStatusAvailable: " + myTotalStatusCount);
        return isMyStatusAvailable;
    }

    private void startStatusAddPage() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkCameraPermission() || !checkWriteExternalStoragePermission()) {
                requestPermission();
            } else {
                Intent intent = new Intent(getActivity(), StatusAddActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        } else {
            //below lollipop
            Intent intent = new Intent(getActivity(), StatusAddActivity.class);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, STATUS_RUNTIME_PERMISSION_CODE);
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkCameraPermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onResume() {
        super.onResume();
        MyLog.d(TAG, "onResume: performance");
        appOpen = true;
        ImageSet();
        int result = CoreController.getStatusDB(getActivity()).deleteOldStatus(); // delete old status
        MyLog.d(TAG, "onResume deleted result: " + result);
        MyLog.d(TAG, "onResume: currentUserId: " + mCurrentUserId);
        mHandler.postDelayed(() -> ((CoreActivity) getActivity()).hidepDialog(), 1000);
        updateMyStatus();

        StatusData.getInstance().selectedGalleryItems = null;

        refreshStatus();

        MessageService.getStatusHistory(mCurrentUserId);
        NetworkChangeReceiver.setListener(this);
        if (!SharedPrefUtil.isFetchAllCompleted()) {
            StatusSocketEvents.fetchAllStatus(getActivity());
        }
        StatusUploadUtil.getInstance().checkAndUploadStatus(getActivity());
        MyLog.d(TAG, "onResume: 2");
    }

    private void ImageSet() {
        isMyStatusAvailable = checkMyStatusAvailable();
        if (!isMyStatusAvailable) {
            // String userProfile = SessionManager.getInstance(getActivity()).getUserProfilePic().replaceFirst("./", "");
            //if (!userProfile.isEmpty()) {
            //  String path = Constants.SOCKET_IP + userProfile;
            //AppUtils.loadImage(getActivity(), path, ivMyStatus, 100, R.drawable.personprofile);
                /*Picasso.with(getActivity()).load(path)
                        .error(R.drawable.personprofile)
                        .into(ivMyStatus);*/
            AppUtils.loadProfilePic(getActivity(), ivMyStatus);
        }
    }

   /* private void updateMyStatus() {
        MyLog.d(TAG, "updateMyStatus: ");
        try {
            isMyStatusAvailable = checkMyStatusAvailable();
            if (myTotalStatusCount > 0) {
                ivStatusAddSmallIcon.setVisibility(View.GONE);
                whatsappStatusCircleView.setVisibility(View.VISIBLE);
                whatsappStatusCircleView.setTotal(myTotalStatusCount);
                whatsappStatusCircleView.setViewed(0);
                ivMyStatusDetail.setVisibility(View.VISIBLE);
                StatusModel recentStatus = CoreController.getStatusDB(getActivity()).getRecentStatus(mCurrentUserId);
                tvMyStatusDesc.setText(new String(AppUtils.getStatusTime(getActivity(), recentStatus.getTimeUploaded(), true)));
                StatusUtil.setImage(getActivity(), ivMyStatus, recentStatus);
            } else {
               *//* String userProfile=SessionManager.getInstance(getActivity()).getUserProfilePic().replaceFirst("./","");
                if(!userProfile.isEmpty()) {
                    String path = Constants.SOCKET_IP + userProfile;
                    //AppUtils.loadImage(getActivity(),path,ivMyStatus,100,R.drawable.personprofile);
                    Picasso.with(getActivity()).load(path)
                            .error(R.drawable.personprofile)
                            .into(ivMyStatus);
                }*//*
                AppUtils.loadProfilePic(getActivity(),ivMyStatus);
                ivMyStatusDetail.setVisibility(View.GONE);
                ivStatusAddSmallIcon.setVisibility(View.VISIBLE);
                //  ivMyStatus.setImageResource(R.drawable.personprofile);
                whatsappStatusCircleView.setVisibility(View.GONE);
                tvMyStatusDesc.setText(getString(R.string.tap_to_add_status_update));
            }
        } catch (Exception e) {
            MyLog.e(TAG, "updateMyStatus: ", e);
        }
    }
*/

    private void updateMyStatus() {
        Log.d(TAG, "updateMyStatus: ");
        try {
            isMyStatusAvailable = checkMyStatusAvailable();
            if (myTotalStatusCount > 0) {
                ivStatusAddSmallIcon.setVisibility(View.GONE);
               /* whatsappStatusCircleView.setVisibility(View.VISIBLE);
                whatsappStatusCircleView.setTotal(myTotalStatusCount);
                whatsappStatusCircleView.setViewed(0);*/
                ivMyStatusDetail.setVisibility(View.VISIBLE);
                StatusModel recentStatus = CoreController.getStatusDB(getActivity()).getRecentStatus(mCurrentUserId);
                //  tvMyStatusDesc.setText(AppUtils.getStatusTime(getActivity(), recentStatus.getTimeUploaded()));
                tvMyStatusDesc.setText(AppUtils.getStatusTime(getActivity(), recentStatus.getTimeUploaded(), true));
                if (recentStatus.isTextstatus()) {
                    whatsappStatusCircleView.setVisibility(View.INVISIBLE);
                    ivMyStatus.setVisibility(View.INVISIBLE);
                    status_text_layout.setVisibility(View.VISIBLE);
                    circular_status_view.setVisibility(View.VISIBLE);
                    circular_status_view.setPortionsCount(myTotalStatusCount);
                    status_text_image.setLayoutParams(new RelativeLayout.LayoutParams(200, 200));
                    ShapeDrawable badge = new ShapeDrawable(new OvalShape());
                    badge.setIntrinsicWidth(200);
                    badge.setIntrinsicHeight(200);
                    badge.getPaint().setColor(Color.parseColor(recentStatus.getTextstatus_color_code()));
                    status_text_image.setImageDrawable(badge);
                    String Caption = recentStatus.getTextstatus_caption();
//                    text_status_caption.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/"+recentStatus.getTextstatus_text_font()));
                    text_status_caption.setText(recentStatus.getTextstatus_caption());
                } else {
                    status_text_layout.setVisibility(View.GONE);
                    ivMyStatus.setVisibility(View.VISIBLE);
                    whatsappStatusCircleView.setVisibility(View.VISIBLE);
                    whatsappStatusCircleView.setTotal(myTotalStatusCount);
                    whatsappStatusCircleView.setViewed(0);
                    text_status_caption.setText("");
                    StatusUtil.setImage(getActivity(), ivMyStatus, recentStatus);
                }
            } else {
                status_text_layout.setVisibility(View.GONE);
                ivMyStatus.setVisibility(View.VISIBLE);
                text_status_caption.setText("");
                String userProfile = SessionManager.getInstance(getActivity()).getUserProfilePic().replaceFirst("./", "");
                if (!userProfile.isEmpty()) {
                    String path = Constants.SOCKET_IP + userProfile;
                    //AppUtils.loadImage(getActivity(),path,ivMyStatus,100,R.drawable.personprofile);
                    Picasso.with(getActivity()).load(path)
                            .error(R.drawable.personprofile)
                            .into(ivMyStatus);
                }
                ivMyStatusDetail.setVisibility(View.GONE);
                ivStatusAddSmallIcon.setVisibility(View.VISIBLE);
                //  ivMyStatus.setImageResource(R.drawable.personprofile);
                whatsappStatusCircleView.setVisibility(View.GONE);
                tvMyStatusDesc.setText(getString(R.string.tap_to_add_status_update));
            }
        } catch (Exception e) {
            Log.e(TAG, "updateMyStatus: ", e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        appOpen = false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event != null && event.getEventName() != null) {
            switch (event.getEventName()) {
                case SocketManager.EVENT_STATUSDELETE:
                    // Log.e(TAG, "onMessageEvent: EVENT_STATUSDELETE ");
                    String result = event.getObjectsArray()[0].toString();
                    JSONObject object = null;
                    try {
                        object = new JSONObject(result);
                        String mErr = "";
                        Log.e(TAG, "EVENT_STATUSDELETE: " + object);
                        if (object.has("err")) {
                            mErr = object.getString("err");
                            if (Integer.valueOf(mErr) == 0) {

                                if (object.has("status")) {
                                    JSONArray array = object.optJSONArray("status");
                                    for (int i = 0; i < array.length(); i++) {
                                        try {
                                            JSONObject msgObj = array.getJSONObject(i);
                                            String doc_id = msgObj.getString("doc_id");
                                            CoreController.getStatusDB(getActivity()).deleteStatus(doc_id);
                                        } catch (Exception e) {

                                        }
                                    }
                                }
                                refreshStatus();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case SocketManager.EVENT_STATUS_ACK:
                    try {
                        MyLog.d(TAG, "onMessageEvent: EVENT_STATUS_ACK ");
                        int mutedOldCount = CoreController.getStatusDB(getActivity()).getMutedUsersCount();
                        MyLog.d(TAG, "onMessageEvent: " + mutedOldCount);
                        mCurrentUserId = sessionManager.getCurrentUserID();
                        StatusUtil.statusAckResult(getActivity(), event, mCurrentUserId);
                        int mutedNewCount = CoreController.getStatusDB(getActivity()).getMutedUsersCount();
                        MyLog.d(TAG, "onMessageEvent: EVENT_STATUS_ACK ");
                        MyLog.e(TAG, "onMessageEvent: EVENT_STATUS_ACK " + mutedNewCount + "mutedOldCount" + mutedOldCount);
                        //Issue in muted count
                        //  if (mutedNewCount > mutedOldCount) {
                        refreshStatus();
                        MyLog.e(TAG, "onMessageEvent: EVENT_STATUS_ACK " + "refreshStatus");
                        // }
                    } catch (Exception e) {
                        MyLog.e(TAG, "onMessageEvent: ", e);
                    }
                    break;
                case SocketManager.EVENT_STATUS_UPDATE:
                    MyLog.d(TAG, "onMessageEvent: EVENT_STATUS_UPDATE ");
                    StatusUtil.statusAckByOthers(getActivity(), event, mCurrentUserId);
                    //    Toast.makeText(getActivity(), "Status updated by other end", Toast.LENGTH_SHORT).show();
                    break;
                case SocketManager.EVENT_STATUS_RESPONSE:
                    try {
                        MyLog.d(TAG, "onMessageEvent: EVENT_STATUS_RESPONSE ");
                        //       Toast.makeText(getActivity(), "Status updated", Toast.LENGTH_SHORT).show();
                        SharedPrefUtil.setStatusRunning(false);
                        // StatusUtil.myStatusSuccessResponse(getActivity(),event);
                    } catch (Exception e) {
                        MyLog.e(TAG, "onMessageEvent: ", e);
                    }
                    break;
                case SocketManager.EVENT_STATUS_OFFLINE:
                    addNewStatusFromServer(event, true);
                    MyLog.d(TAG, "onMessageEvent: EVENT_STATUS_OFFLINE");
                    break;
                case SocketManager.EVENT_STATUS_DELETE:
                    MyLog.d(TAG, "onMessageEvent: EVENT_STATUS_DELETE");
                    boolean resultt = StatusUtil.afterStatusDelete(event, getActivity());
                    mHandler.postDelayed(() -> {
                        refreshStatus();
                        MyLog.e(TAG, "HIDE");
                        ((CoreActivity) getActivity()).hidepDialog();
                        updateMyStatus();
                    }, 2000);
                    break;
                case SocketManager.EVENT_STATUS_MUTE:
                    //      Toast.makeText(getActivity(), "Status mute", Toast.LENGTH_SHORT).show();
                    MyLog.d(TAG, "onMessageEvent: EVENT_STATUS_MUTE");
                    StatusUtil.afterMuteResponse(event, getActivity());
                    break;
                case SocketManager.EVENT_STATUS:
                    MyLog.e(TAG, "onMessageEvent: EVENT_STATUS");
                    addNewStatusFromServer(event, false);
                    break;
                case SocketManager.EVENT_STATUS_FETCH_ALL:
                    MyLog.d(TAG, "onMessageEvent: EVENT_STATUS_FETCH_ALL");
                    StatusUtil.afterFetchAllResponse(event, getActivity());
                    break;
            }
        }
    }

    private synchronized void addNewStatusFromServer(ReceviceMessageEvent event, boolean isOffline) {
        try {
            Object[] array = event.getObjectsArray();
            Log.e(TAG, "addNewStatusFromServer: " + array[0].toString());
            if (isOffline) {
                JSONObject objects = new JSONObject(array[0].toString());
                if (objects.has("result")) {
                    JSONArray resultArray = objects.getJSONArray("result");
                    if (resultArray != null && resultArray.length() > 0) {
                        for (int i = 0; i < resultArray.length(); i++) {
                            JSONObject statusObj = resultArray.getJSONObject(i);
                            StatusModel statusModel = StatusUtil.getPojoFromJson(statusObj);
                            saveStatus(statusModel);
                        }
                    }
                } else {
                    JSONObject objectsSingle = new JSONObject(array[0].toString());
                    StatusModel statusModel = StatusUtil.getPojoFromJson(objectsSingle);
                    saveStatus(statusModel);
                }
            } else {
                JSONObject objects = new JSONObject(array[0].toString());
                StatusModel statusModel = StatusUtil.getPojoFromJson(objects);
                saveStatus(statusModel);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "addNewStatusFromServer: ", e);
        }
    }

    private void saveStatus(StatusModel statusModel) {
        if (statusModel != null) {
            MyLog.d(TAG, "addNewStatusFromServer: " + statusModel.getId());
            if (statusModel.getUserId().equals(mCurrentUserId)) {
                Log.i(TAG, "addNewStatusFromServer: current user status");
                return;
            }
            //if my blocked contact
            if (contactDB_sqlite.getBlockedStatus(statusModel.getUserId(), false).equals("1")) {
                return;
            }
            //if me blocked by others
            if (contactDB_sqlite.getBlockedMineStatus(statusModel.getUserId(), false).equals("1")) {
                return;
            }
            //for local check (un uploaded items)
            int previousCountInDB = 0;
            //  int previousCountInDB=CoreController.getStatusDB(getActivity()).getStatusCountById(AppUtils.parseLong(statusModel.getId()));
            //for uploaded items

            int previousCountByDocId = CoreController.getStatusDB(getActivity()).getStatusCountByDocId(statusModel.getDocId());

            MyLog.e(TAG, "addNewStatusFromServer: " + statusModel.getDocId());

            MyLog.d(TAG, "addNewStatusFromServer: " + previousCountInDB);
            //for avoid duplicate insert
            if (previousCountByDocId == 0) {
                String to = statusModel.getUserId();
                String docId = statusModel.getUserId() + "-" + statusModel.getId();
                statusModel.setDocId(docId);
                CoreController.getStatusDB(getActivity()).insertNewStatus(Collections.singletonList(statusModel), mCurrentUserId);
                refreshStatus();
                StatusSocketEvents.sendAckToServer(getActivity(), to, docId, statusModel.getId(), MessageFactory.DELIVERY_STATUS_DELIVERED);
            }
        }
    }

    private void refreshStatus() {
        setRecentAdapter();
        setViewedAdapter();
        setMuteListAdapter();
    }

    @Override
    public void networkAvailable(boolean avialable) {
        MyLog.d(TAG, "networkAvailable: " + avialable);
        if (!avialable) {
            isNetworkFailed = true;
        }
        if (appOpen && avialable && isNetworkFailed) {
            MyLog.d(TAG, "networkAvailable: true & app open ");
            //checkAndUploadStatus("network available ");
        }
    }

    //long press mute/unmute dialog listener
    @Override
    public void onPositiveBtnClick(Object result) {
        try {
            StatusModel selectedItemData = (StatusModel) result;
            StatusUtil.muteUnmute(getActivity(), selectedItemData);
            refreshStatus();
        } catch (Exception e) {
            MyLog.e(TAG, "onPositiveBtnClick: ", e);
        }
    }

    @Override
    public void onNegativeBtnClick() {
        MyLog.d(TAG, "onNegativeBtnClick: cancel ");
    }

    private void getOfflineDeletedStatus() {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_STATUSDELETE);
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        Log.e(TAG, "setMenuVisibility" + visible);
        if (visible) {
            //Do your stuff here
            getOfflineDeletedStatus();
        }
        super.setMenuVisibility(visible);
    }
}