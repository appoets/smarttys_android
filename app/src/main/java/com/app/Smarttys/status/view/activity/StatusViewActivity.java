package com.app.Smarttys.status.view.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.dialog.CommonAlertDialog;
import com.app.Smarttys.app.dialog.CustomAlertDialog;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.Keys;
import com.app.Smarttys.app.utils.MyExoPlayer;
import com.app.Smarttys.app.utils.MyExoPlayerListener;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.message.AudioMessage;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.photoview.OnLoadProfile;
import com.app.Smarttys.photoview.OnMatrixChangedListener;
import com.app.Smarttys.photoview.OnPausePlayer;
import com.app.Smarttys.photoview.OnPhotoTapListener;
import com.app.Smarttys.photoview.OnResumePlayer;
import com.app.Smarttys.photoview.OnResumeStory;
import com.app.Smarttys.photoview.OnSingleFlingListener;
import com.app.Smarttys.photoview.OnStatusPress;
import com.app.Smarttys.status.controller.StatusSocketEvents;
import com.app.Smarttys.status.controller.StatusUtil;
import com.app.Smarttys.status.model.StatusHistoryModel;
import com.app.Smarttys.status.model.StatusListModel;
import com.app.Smarttys.status.model.StatusModel;
import com.app.Smarttys.status.view.adapter.StatusSeenByAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import jp.shts.android.storiesprogressview.StoriesProgressView;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static java.util.Arrays.asList;

/**
 * Created by user134 on 3/27/2018.
 */

public class StatusViewActivity extends CoreActivity implements StoriesProgressView.StoriesListener, View.OnClickListener, View.OnLongClickListener, CommonAlertDialog.DialogListener {
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    private static final String TAG = StatusViewActivity.class.getSimpleName();
    private static final int MIN_DISTANCE = 150;
    private static final long IMAGE_DURATION = 5000L;
    private static final int PAUSE_DELAY = 100;
    private static final int RESUME_DELAY = 90;
    public static Activity mActivity;
    public static long pressTime = 0L;
    public static int screenWidth;
    private static StoriesProgressView storiesProgressView;
    private static boolean isPlayerPaused = false;
    //  private static long pressTime = 0L;
    private static MyExoPlayer mMyExoPlayer;
    private static List<StatusModel> data = new ArrayList<>();
    //Zoom
    private static View statusReplyContainer;
    private final int AUDIO_RECORD_PERMISSION_REQUEST_CODE = 14;
    EmojIconActions emojIcon;
    Context context;
    private ImageView ivStatusImage;
    private ImageView ivEye;
    private float firstTouchPosition;
    private ProgressBar progressBar;
    private TextView tvViewCount;
    private String statusUploadedTime = "";
    private TextView tvCaption;
    private SimpleExoPlayerView mExoPlayerView;
    private int currentIndex = -1;
    private boolean isMyStatus = false, isMySingleStatus = false;
    private BottomSheetDialog mBottomSheetDialog;
    private TextView tvStatusTime;
    private String currentStatusId = "", currentUserId = "";
    private boolean showPopup = false;
    private RecyclerView rvSeenBy;
    private TextView tvBottomSheetViewCount;
    private Handler handler;
    private boolean audioRecordStarted = false;
    private MediaRecorder audioRecorder;
    private Chronometer myChronometer;
    private androidx.appcompat.widget.Toolbar toolbar;
    private TextView tvStatusReplyDesc;
    private ImageView ivStatusReplyDesc;
    private ImageView ivStatusReplyPreview;
    private ImageView ivStatusReplySmile;
    private ImageView ivStatusReplyKeyboard;
    private ImageView ivStatusReplyRecording;
    private EmojiconEditText edStatusReply;
    private SessionManager sessionManager;
    private AppBarLayout appbar;
    private TextView status_text;
    private int startIndex;
    private String audioRecordPath;
    private List<StatusHistoryModel> statusViewHistoryList = new ArrayList<>();
    private Matrix matrix = new Matrix();
    private Matrix savedMatrix = new Matrix();
    private int mode = NONE;
    private PointF mStartPoint = new PointF();
    private PointF mMiddlePoint = new PointF();
    private Point mBitmapMiddlePoint = new Point();
    private float oldDist = 1f;
    private float matrixValues[] = {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f};
    private float scale;
    private float oldEventX = 0;
    private float oldEventY = 0;
    private float oldStartPointX = 0;
    private float oldStartPointY = 0;
    private int mViewWidth = -1;
    private int mViewHeight = -1;
    private int mBitmapWidth = -1;
    private int mBitmapHeight = -1;
    private boolean mDraggable = false;
    private View.OnTouchListener audioRecordTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View pView, MotionEvent pEvent) {
            pView.onTouchEvent(pEvent);

            if (pEvent.getAction() == MotionEvent.ACTION_UP) {
                try {

                    if (audioRecordStarted) {
                        String sendAudioPath = audioRecordPath;
                        audioRecordPath = "";
                        ivStatusReplyRecording.setImageResource(R.drawable.record);
                        ivStatusReplySmile.setImageResource(R.drawable.smile);
                        myChronometer.setVisibility(View.GONE);
//                    audioRecorder.stop();
                        audioRecorder.release();
                        myChronometer.stop();

                        File file = new File(sendAudioPath);
                        if (file.exists()) {
                            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                            mmr.setDataSource(StatusViewActivity.this, Uri.parse(sendAudioPath));
                            String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                            durationStr = getTimeString(Long.parseLong(durationStr));
                            showAudioRecordSentAlert(sendAudioPath, durationStr);
                        }
                    }
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
                audioRecordStarted = false;
                ivStatusReplySmile.setEnabled(true);
            }
            return false;
        }
    };
    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            boolean isVideoClick = false;
            boolean mZoom = false;


            if (v.getId() == R.id.status_video_player_view) {
                isVideoClick = true;
            }


            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    firstTouchPosition = event.getX();
                    onStatusPress();
                    if (isVideoClick) {
                        pausePlayer();
                    }
                    return true;
                case MotionEvent.ACTION_UP:
                    float finalTouchPosition = event.getX();
                    float deltaX = finalTouchPosition - firstTouchPosition;

                    if (Math.abs(deltaX) > MIN_DISTANCE) {
                        resumeStories();
                        // Right to left swipe action
                        if (finalTouchPosition > firstTouchPosition) {
                            MyLog.d(TAG, "onTouch: right to left");
                            loadNextProfile(true);
                        }
                        // Left to Right swipe action
                        else {
                            MyLog.d(TAG, "onTouch: left to right");
                            loadNextProfile(false);
                        }

                    } else {
                        boolean isClickEvent = onTap(isVideoClick, finalTouchPosition, StatusViewActivity.this);
                        if (!isClickEvent) {
                            resumeStories();
                            if (isVideoClick) {
                                resumePlayer();
                            }
                        }
                    }
                    return true;
            }


            switch (event.getAction() & MotionEvent.ACTION_MASK) {

                case MotionEvent.ACTION_DOWN:
                    savedMatrix.set(matrix);
                    mStartPoint.set(event.getX(), event.getY());
                    mode = DRAG;
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    oldDist = spacing(event);
                    if (oldDist > 10f) {
                        savedMatrix.set(matrix);
                        midPoint(mMiddlePoint, event);
                        mode = ZOOM;
                    }
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    mode = NONE;
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mode == DRAG) {
                        drag(event);
                        mZoom = true;
                    } else if (mode == ZOOM) {
                        zoom(event);
                        mZoom = true;
                    }
                    break;

            }
            if (mZoom && !isVideoClick) {

                ImageView view = (ImageView) v;
                view.setImageMatrix(matrix);


            }
            MyLog.d(TAG, "onTouch: others ");
            return true;
        }
    };
    private Callback picassoImageCallBack = new Callback() {
        @Override
        public void onSuccess() {
            progressBar.setVisibility(View.GONE);
            storiesProgressView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    storiesProgressView.resume();
                }
            }, PAUSE_DELAY);

        }

        @Override
        public void onError() {
            //ASK --> What to do when error happen
            storiesProgressView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    storiesProgressView.resume();
                }
            }, PAUSE_DELAY);
            progressBar.setVisibility(View.GONE);
        }
    };

    public static boolean onTap(boolean isVideoClick, float touchPosition, StatusViewActivity mActivity) {
        boolean isTapped = false;
       /* if (screenWidth == 0) {
            getScreenWidth();
        }*/
        try {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            if (mActivity != null) {
                if (mActivity.getWindowManager() != null) {
                    mActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    screenWidth = displayMetrics.widthPixels;
                    if (statusReplyContainer.getVisibility() == View.VISIBLE) {
                        statusReplyContainer.setVisibility(View.GONE);

                        //hideReplyLayout();
//            hideKeyboard();
                        storiesProgressView.resume();
                        isTapped = true;
                    } else {
                        long now = System.currentTimeMillis();
                        long differnceTime = now - pressTime;
                        MyLog.d(TAG, "onTouch: differnceTime " + differnceTime);
                        //double tap --> skip to next
                        if (differnceTime < 500) {
                            if (storiesProgressView != null && data != null && data.size() > 0) {
                                int leftTouchMax = 0;
                                if (isVideoClick)
                                    if (mMyExoPlayer != null) {
                                        isPlayerPaused = true;
                                        mMyExoPlayer.pausePlayer();
                                    }

                                if (screenWidth > 0) {
                                    leftTouchMax = screenWidth * 35 / 100;
                                }
                                if (screenWidth > 0 && touchPosition < (leftTouchMax)) {
                                    MyLog.d(TAG, "onTap: reverse");
                                    storiesProgressView.resume();
                                    storiesProgressView.reverse();
                                } else {
                                    MyLog.d(TAG, "onTap: skip");
                                    storiesProgressView.skip();
                                }
                                //storiesProgressView.skip();
                            }
                            isTapped = true;
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
        return isTapped;
    }

    public static boolean onTap(boolean isVideoClick, float touchPosition) {
        boolean isTapped = false;
       /* if (screenWidth == 0) {
            getScreenWidth();
        }*/
        try {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            if (mActivity != null) {
                if (mActivity.getWindowManager() != null) {
                    mActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    screenWidth = displayMetrics.widthPixels;
                    if (statusReplyContainer.getVisibility() == View.VISIBLE) {
                        statusReplyContainer.setVisibility(View.GONE);

                        //hideReplyLayout();
//            hideKeyboard();
                        storiesProgressView.resume();
                        isTapped = true;
                    } else {
                        long now = System.currentTimeMillis();
                        long differnceTime = now - pressTime;
                        MyLog.d(TAG, "onTouch: differnceTime " + differnceTime);
                        //double tap --> skip to next
                        if (differnceTime < 500) {
                            if (storiesProgressView != null && data != null && data.size() > 0) {
                                int leftTouchMax = 0;
                                if (isVideoClick)
                                    if (mMyExoPlayer != null) {
                                        isPlayerPaused = true;
                                        mMyExoPlayer.pausePlayer();
                                    }

                                if (screenWidth > 0) {
                                    leftTouchMax = screenWidth * 35 / 100;
                                }
                                if (screenWidth > 0 && touchPosition < (leftTouchMax)) {
                                    MyLog.d(TAG, "onTap: reverse");
                                    storiesProgressView.resume();
                                    storiesProgressView.reverse();
                                } else {
                                    MyLog.d(TAG, "onTap: skip");
                                    storiesProgressView.skip();
                                }
                                //storiesProgressView.skip();
                            }
                            isTapped = true;
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
        return isTapped;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(StatusViewActivity.this);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_view);
        View rootView = findViewById(R.id.root_view);
        context = StatusViewActivity.this;
        mActivity = StatusViewActivity.this;
        sessionManager = SessionManager.getInstance(context);
        appbar = (AppBarLayout) findViewById(R.id.appbar);

        //------------------------Text Status--------------

        status_text = (TextView) findViewById(R.id.status_text);
        status_text.setText("");

        handler = new Handler();
        storiesProgressView = findViewById(R.id.status_progress_view);
        progressBar = findViewById(R.id.progress_bar);
        ivStatusImage = findViewById(R.id.iv_status_image);

        ivStatusReplyDesc = findViewById(R.id.iv_status_reply_desc_icon);
        tvViewCount = findViewById(R.id.tv_my_status_view_count);
        ivEye = findViewById(R.id.iv_my_status_eye);
        ImageView ivUserProfile = findViewById(R.id.iv_profile_icon);

        statusReplyContainer = findViewById(R.id.reply_layout);
        statusReplyContainer.setOnClickListener(this);
        findViewById(R.id.status_send_btn).setOnClickListener(this);


        ivStatusReplyPreview = findViewById(R.id.iv_status_reply_preview);
        ivStatusReplyRecording = findViewById(R.id.iv_status_reply_record);
        ImageView ivStatusReplySend = findViewById(R.id.iv_status_reply_send);
        ivStatusReplyRecording.setOnClickListener(this);
        ivStatusReplySend.setOnClickListener(this);


        ivStatusReplyRecording.setOnTouchListener(audioRecordTouchListener);
        ivStatusReplyRecording.setOnLongClickListener(StatusViewActivity.this);
        edStatusReply = findViewById(R.id.ed_status_reply_content);
        ivStatusReplyKeyboard = findViewById(R.id.iv_status_reply_keyboard);
        ivStatusReplySmile = findViewById(R.id.iv_status_reply_smile);
        TextView tvStatusReplyTitle = findViewById(R.id.status_reply_title);
        tvStatusReplyDesc = findViewById(R.id.status_reply_desc);
        myChronometer = findViewById(R.id.chronometer);

        mExoPlayerView = findViewById(R.id.status_video_player_view);
        TextView tvProfileName = findViewById(R.id.tv_profile_name);
        tvStatusTime = findViewById(R.id.tv_status_uploaded_time);
        tvCaption = findViewById(R.id.tv_status_caption);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        findViewById(R.id.iv_back).setOnClickListener(this);
        findViewById(R.id.tv_status_reply).setOnClickListener(this);
        ivStatusReplyKeyboard.setOnClickListener(this);
        ivStatusImage.setOnTouchListener(onTouchListener);
        //   ivStatusImage.setOnTouchListener(new MyScaleGestures(context));
        //  ivStatusImage.setOnTouchListener(new TouchImageView(context));
        // TouchImageView img = new TouchImageView(this);
        //  ivStatusImage.setOnTouchListener(new StandardGestures(context));

        mExoPlayerView.setOnTouchListener(onTouchListener);
        ivStatusReplySmile.setOnClickListener(this);
        emojIcon = new EmojIconActions(this, rootView, edStatusReply, ivStatusReplySmile);
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smile);
        emojIcon.setUseSystemEmoji(false);
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {

            }

            @Override
            public void onKeyboardClose() {

            }
        });

        edStatusReply.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //setSendIconVisiblity(s);
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        try {
            if (getIntent() != null) {
                isMyStatus = getIntent().getBooleanExtra(Keys.MY_STATUS, false);
                isMySingleStatus = getIntent().getBooleanExtra(Keys.MY_STATUS_SINGLE, false);
                showPopup = getIntent().getBooleanExtra(Keys.SHOW_POPUP, false);
                currentUserId = getIntent().getStringExtra(Keys.USER_ID);
                currentStatusId = getIntent().getStringExtra(Keys.ID);
                statusUploadedTime = getIntent().getStringExtra(Keys.STATUS_UPLOADED_TIME);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "onCreate: ", e);
        }

        int totalCount = CoreController.getStatusDB(this).getStatusCount(currentUserId);
        int unViewedStatusCount = CoreController.getStatusDB(this).getUnViewedStatusCount(currentUserId);
        if (unViewedStatusCount > 0)
            startIndex = totalCount - unViewedStatusCount;// ex: total 5 , unviewed=3 5-3=2
        if (isMyStatus) {
            //hide reply button
            findViewById(R.id.tv_status_reply).setVisibility(View.GONE);
            findViewById(R.id.iv_reply_arrow).setVisibility(View.GONE);
            findViewById(R.id.iv_menu_icon).setVisibility(View.GONE);
            tvProfileName.setText(getString(R.string.my_status));
            tvViewCount.setVisibility(View.VISIBLE);
            ivEye.setVisibility(View.VISIBLE);
            tvViewCount.setOnClickListener(this);
            ivEye.setOnClickListener(this);

        } else {
            String userName = StatusUtil.getNameById(this, currentUserId);
            tvProfileName.setText(userName);
            tvStatusReplyTitle.setText(userName + " . Status");
            findViewById(R.id.iv_menu_icon).setOnClickListener(this);

        }
        setBottomSheetDialog();
        setData();

        if (showPopup) {
            //show bottom sheet list
            ivEye.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ivEye.performClick();
                }
            }, PAUSE_DELAY);
        }
        StatusUtil.setProfileImage(ivUserProfile, this, "", currentUserId);


/*        ivStatusImage.setOnMatrixChangeListener(new MatrixChangeListener());
        ivStatusImage.setOnPhotoTapListener(new PhotoTapListener());
        ivStatusImage.setOnSingleFlingListener(new SingleFlingListener());
        ivStatusImage.setOnPauseTapListener(new PasuePlayerTapListener());
        ivStatusImage.setOnResumeTapListener(new ResumePlayerTapListener());
        ivStatusImage.setOnLoadProfileListener(new LoadProfileTapListener());
        ivStatusImage.setOnResumeStoryTapListener(new ResumeStoriesTapListener());
        ivStatusImage.setOnStatusTapListener(new StatusPressTapListener());*/
    }

    private void loadVideo(StatusModel statusModel) {
        MyLog.d(TAG, "loadVideo: ");
        mExoPlayerView.setVisibility(View.VISIBLE);
        ivStatusImage.setVisibility(View.INVISIBLE);
        mMyExoPlayer = new MyExoPlayer();
        progressBar.setVisibility(View.VISIBLE);
        ivStatusReplyDesc.setImageResource(R.drawable.video);
        tvStatusReplyDesc.setText(getString(R.string.video));
        StatusUtil.setImage(this, ivStatusReplyPreview, statusModel);

        pauseStoryView();
        mMyExoPlayer.loadPlayer(this, 1, mExoPlayerView, new MyExoPlayerListener() {
            @Override
            public void onError() {
                MyLog.d(TAG, "#####onError: ");
                if (progressBar != null) {
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCompleted() {
                MyLog.d(TAG, "#####onCompleted: ");
                storiesProgressView.skip();
            }

            @Override
            public void onStartPlaying() {
                MyLog.d(TAG, "#####onStartPlaying: ");

            }

            @Override
            public void onVideoChanged(boolean isImageAds, boolean isAds) {
                MyLog.d(TAG, "#####onVideoChanged: ");
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                MyLog.d(TAG, "#####onPlayerStateChanged: " + playbackState);
                if ((playbackState == PlaybackStateCompat.STATE_PLAYING)) {
                    MyLog.d(TAG, "onPlayerStateChanged: start playing");
                    progressBar.setVisibility(View.GONE);
                    if (!showPopup && !isPlayerPaused) {
                        storiesProgressView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                storiesProgressView.resume();
                            }
                        }, PAUSE_DELAY);
                    }
                }
            }
        });
        mExoPlayerView.setUseController(false);

        // mMyExoPlayer.setMaxBitRate(16);
        if (isMyStatus) {
            mMyExoPlayer.setMediaSource(mMyExoPlayer.getMediaSourceFromLocalFile(this, statusModel.getLocalPath()));
        } else {

            mMyExoPlayer.setMediaSource(mMyExoPlayer.getMediaSourceFromUrl(this, statusModel.getServerPath()));
        }
        play();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case AUDIO_RECORD_PERMISSION_REQUEST_CODE:
                try {
                    if (grantResults.length > 0) {
                        boolean StoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                        boolean RecordPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                        if (StoragePermission && RecordPermission) {
                            Toast.makeText(StatusViewActivity.this, "Now you can record and share audio", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "onRequestPermissionsResult: ", e);
                }
                break;
        }
    }

    private void sendAudioMessage(String filePath, String duration, int audioFrom) {

        MyLog.d("AudioFIleupload", filePath + "  ___    " + duration + "   ___   " + audioFrom);

        AudioMessage message = (AudioMessage) MessageFactory.getMessage(MessageFactory.audio, this);
        MessageItemChat item = null;
        boolean canSent = false;
/*        JSONObject uploadObj = (JSONObject) message.createAudioUploadObject(item.getMessageId(), docId,
                audioName, filePath, duration, receiverNameText.getText().toString(), audioFrom, chatType, false);


        uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);*/

    }

    private void showAudioRecordSentAlert(final String audioRecordPath, final String durationStr) {
        CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage("You want send this recorded audio?");
        dialog.setPositiveButtonText("Send");
        dialog.setNegativeButtonText("Cancel");
        dialog.setCancelable(false);

        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                sendAudioMessage(audioRecordPath, durationStr, MessageFactory.AUDIO_FROM_RECORD);
            }

            @Override
            public void onNegativeButtonClick() {

            }
        });
        dialog.show(getSupportFragmentManager(), "Record Alert");
    }

    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        int hours = (int) (millis / (1000 * 60 * 60));
        int minutes = (int) ((millis % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (int) (((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000);

        buf.append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }

    private void play() {
        if (mMyExoPlayer != null && mExoPlayerView.getVisibility() == View.VISIBLE)
            mMyExoPlayer.play();
    }

    private void setData() {
        tvStatusTime.setText(statusUploadedTime);

        data = CoreController.getStatusDB(this).getParticularPersonStatusList(currentUserId);


        if (isMySingleStatus) {
            data = asList(CoreController.getStatusDB(this).getParticularStatus(currentStatusId));
        } else {
            data = CoreController.getStatusDB(this).getParticularPersonStatusList(currentUserId);
        }

        setStories();
    }

    private void setStories() {
        if (data != null && data.size() > 0) {
            MyLog.d(TAG, "setData() data size: " + data.size());
            storiesProgressView.setStoriesCount(data.size());

            //list to array
            long[] durationArrray = new long[data.size()];
            for (int i = 0; i < data.size(); i++) {
                long duration;
                StatusModel statusModel = data.get(i);
                if (statusModel != null) {
                    if (statusModel.isImage()) {
                        duration = IMAGE_DURATION;
                    } else if (statusModel.isTextstatus()) {
                        duration = IMAGE_DURATION;
                    } else {
                        long durationOfVideo = 0L;
                        if (statusModel.getUserId().equals(currentUserId)) {
                            if (AppUtils.isEmpty(statusModel.getLocalPath())) {
                            }
                            durationOfVideo = StatusUtil.getVideoDuration(statusModel.getLocalPath(), false);
                        } else {
                            durationOfVideo = StatusUtil.getVideoDuration(statusModel.getServerPath(), true);
                        }
                        if (durationOfVideo > 0) {
                            duration = durationOfVideo;
                        } else {
                            duration = 30000L;//testing
                        }
                    }
                    durationArrray[i] = duration;
                }
            }

            storiesProgressView.setStoriesCountWithDurations(durationArrray);
            //storiesProgressView.setStoryDuration(IMAGE_DURATION);
            storiesProgressView.setStoriesListener(this);

            if (!isMySingleStatus && startIndex > 0) {
                storiesProgressView.startStories(startIndex);
                startIndex = startIndex - 1;
                currentIndex = startIndex;
            } else {
                storiesProgressView.startStories();
            }
            loadImageOrVideo(true);

        } else {
            storiesProgressView.setVisibility(View.GONE);
        }
    }

    private void loadImageOrVideo(boolean isNext) {

        if (isNext) {
            currentIndex++;
        } else {
            if ((currentIndex - 1) < 0)
                return;
            currentIndex--;
        }
        if (data != null) {
            try {
                final StatusModel statusModel = data.get(currentIndex);
                String idString = statusModel.getId();
                long id = AppUtils.parseLong(idString);
                MyLog.d(TAG, "loadImageOrVideo: DB id: " + id);
                statusViewHistoryList = statusModel.getStatusViewHistory();
                int count = statusModel.getStatusViewHistory().size();
                tvViewCount.setText(String.valueOf(count));
                String statusTime = AppUtils.getStatusTime(this, statusModel.getTimeUploaded(), isMyStatus);
                if (statusTime != null && !statusTime.isEmpty())
                    tvStatusTime.setText(statusTime);
                CoreController.getStatusDB(this).updateStatusOfStatus(id, "1");
                String status = CoreController.getStatusDB(this).getStatusOfStatus(id);
                Log.e(TAG, "status" + status);
                if (status == null || !status.equals(MessageFactory.DELIVERY_STATUS_READ)) {
                    final String to = statusModel.getUserId();
                    final String docId = statusModel.getUserId() + "-" + statusModel.getId();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            StatusSocketEvents.sendAckToServer(StatusViewActivity.this, to, docId, statusModel.getId(), MessageFactory.DELIVERY_STATUS_READ);
//UPDATE IN THE DATABSE TO HANDLE OFFLINE

                        }
                    });
                }
                if (statusModel.isVideo()) {
                    loadVideo(statusModel);
                } else if (statusModel.isTextstatus() || statusModel.isImage()) {
                    loadPicassoImage(statusModel);
                } /*else {
                    loadPicassoImage(statusModel);
                }*/

                if (statusModel.getCaption() != null && !statusModel.getCaption().trim().isEmpty()) {
                    //    Log.e(TAG,"statusModel.getCaption()"+statusModel.getCaption());
                    if (statusModel.isTextstatus()) {
                        //        Log.e(TAG,"statusModel.getCaption()"+statusModel.isTextstatus());
                        tvCaption.setVisibility(View.GONE);
                        status_text.setVisibility(View.VISIBLE);

                    } else if (statusModel.isVideo()) {
                        //        Log.e(TAG,"statusModel.getCaption()"+statusModel.isVideo());

                        tvCaption.setVisibility(View.GONE);

                    } else {
                        //      Log.e(TAG,"statusModel.IMAGE()"+statusModel.isVideo());

                        tvCaption.setVisibility(View.VISIBLE);
                        tvCaption.setText(statusModel.getCaption());
                    }
                } else {
                    status_text.setVisibility(View.GONE);
                    //  Log.e(TAG,"statusModel.getCaption() EMPTY");

                    tvCaption.setVisibility(View.GONE);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                finish();
            } catch (Exception e) {
                MyLog.e(TAG, "loadImageOrVideo: ", e);
            }
        }


    }

    private void loadNextProfile(boolean isPrevious) {
        if (isMyStatus) {
            MyLog.d(TAG, "loadNextProfile: no need to load");
            if (isMySingleStatus) {
                MyLog.d(TAG, "loadNextProfile: load next my status");
            }
        } else {
            String nextUserId = "";
            String loginUserId = SessionManager.getInstance(this).getCurrentUserID();
            LinkedList<String> uniqueUsersList = CoreController.getStatusDB(this).getStatusUserIds();
            List<String> mutedUsersList = CoreController.getStatusDB(this).getMutedUsersList();

            if (loginUserId != null) {
                //remove current user --> no need current user status, when swipe left/right
                uniqueUsersList.remove(loginUserId);
            }
            //remove muted users --> no need muted users status, when swipe left/right
            if (mutedUsersList != null && mutedUsersList.size() > 0) {
                for (String mutedUserId : mutedUsersList) {
                    uniqueUsersList.remove(mutedUserId);
                }
            }

            int totalSize = uniqueUsersList.size();
            int currentUserIndex = uniqueUsersList.indexOf(currentUserId);
            if (isPrevious) {
                //no more previous - so finish it
                if (currentUserIndex == 0) {
                    MyLog.d(TAG, "loadNextProfile:  no previous users");
                    finish();
                    return;
                }
                currentUserIndex = currentUserIndex - 1;
                try {
                    nextUserId = uniqueUsersList.get(currentUserIndex);
                } catch (Exception e) {
                    MyLog.e(TAG, "loadNextProfile: ", e);
                }
            }
            //next
            else {
                //no more next - so finish it
                if (currentUserIndex == (totalSize - 1)) {
                    MyLog.d(TAG, "loadNextProfile: no next users");
                    finish();
                    return;
                }
                currentUserIndex = currentUserIndex + 1;
                try {
                    nextUserId = uniqueUsersList.get(currentUserIndex);
                } catch (Exception e) {
                    MyLog.e(TAG, "loadNextProfile: ", e);
                }
            }

            if (nextUserId != null && !nextUserId.isEmpty()) {
                pausePlayer();
                Intent intent = new Intent(this, this.getClass());
                intent.putExtra(Keys.MY_STATUS, false);
                intent.putExtra(Keys.MY_STATUS_SINGLE, false);
                intent.putExtra(Keys.USER_ID, nextUserId);
                startActivity(intent);
                if (isPrevious)
                    overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                else
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            } else {
                finish();
            }
        }
    }

    private void pausePlayer() {
        if (mMyExoPlayer != null) {
            isPlayerPaused = true;
            mMyExoPlayer.pausePlayer();
        }
    }

    private void resumePlayer() {
        if (mMyExoPlayer != null && isPlayerPaused) {
            isPlayerPaused = false;
            mMyExoPlayer.resumePlayer();
        }
    }

    private void resumeStories() {
        try {
            if (data != null && data.size() > 0)
                storiesProgressView.resume();
        } catch (Exception e) {
            MyLog.d(TAG, "resumeStories: " + e.getMessage());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        pausePlayer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mActivity = StatusViewActivity.this;
        resumePlayer();
        getScreenWidth();
    }

    private void getScreenWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenWidth = displayMetrics.widthPixels;
    }

    @Override
    public void onStop() {
        super.onStop();
        mActivity = null;
        EventBus.getDefault().unregister(StatusViewActivity.this);
        pausePlayer();
    }

  /*  public static boolean onTap(boolean isVideoClick, float touchPosition) {
        boolean isTapped = false;
        if (screenWidth == 0) {
            getScreenWidth();
        }
        if (statusReplyContainer.getVisibility() == View.VISIBLE) {
            hideReplyLayout();
            hideKeyboard();
            storiesProgressView.resume();
            isTapped = true;
        } else {
            long now = System.currentTimeMillis();
            long differnceTime = now - pressTime;
            MyLog.d(TAG, "onTouch: differnceTime " + differnceTime);
            //double tap --> skip to next
            if (differnceTime < 500) {
                if (storiesProgressView != null && data != null && data.size() > 0) {
                    int leftTouchMax = 0;
                    if (isVideoClick)
                        pausePlayer();

                    if (screenWidth > 0) {
                        leftTouchMax = screenWidth * 35 / 100;
                    }
                    if (screenWidth > 0 && touchPosition < (leftTouchMax)) {
                        MyLog.d(TAG, "onTap: reverse");
                        storiesProgressView.resume();
                        storiesProgressView.reverse();
                    } else {
                        MyLog.d(TAG, "onTap: skip");
                        storiesProgressView.skip();
                    }
                    //storiesProgressView.skip();
                }
                isTapped = true;
            }
        }
        return isTapped;
    }

    private void hideReplyLayout() {
        statusReplyContainer.setVisibility(View.GONE);
    }

    @Override
    public void onNext() {
        try {
            pausePlayer();
            loadImageOrVideo(true);
        } catch (Exception e) {
            Log.e(TAG, "onNext: ", e);
        }
    }
*/

    private void onStatusPress() {
        pressTime = System.currentTimeMillis();
        try {
            storiesProgressView.pause();
        } catch (Exception e) {
            MyLog.e(TAG, "onStatusPress: ", e);
        }
    }

    @Override
    public void onNext() {
        try {
            pausePlayer();
            loadImageOrVideo(true);
        } catch (Exception e) {
            MyLog.e(TAG, "onNext: ", e);
        }
    }

    @Override
    public void onPrev() {
        try {
            loadImageOrVideo(false);
        } catch (Exception e) {
            MyLog.e(TAG, "onPrev: ", e);
        }
    }

    private void setBottomSheetDialog() {
        View bottomSheet = getLayoutInflater().inflate(R.layout.status_seen_by_popup, null);

        rvSeenBy = bottomSheet.findViewById(R.id.rv_status_viewed_members);
        tvBottomSheetViewCount = bottomSheet.findViewById(R.id.tv_viewed_by);
        bottomSheet.findViewById(R.id.iv_delete).setOnClickListener(this);
        bottomSheet.findViewById(R.id.iv_forward).setOnClickListener(this);
        rvSeenBy.setLayoutManager(AppUtils.getDefaultLayoutManger(this));


        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(bottomSheet);
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                MyLog.d(TAG, "onDismiss: ");
                if (storiesProgressView != null) {
                    showPopup = false;
                    storiesProgressView.resume();
                    resumePlayer();
                }
            }
        });
    }

    private List<StatusListModel> fetchSeenByMemebersData() {
        List<StatusListModel> data = new ArrayList<>();
        tvBottomSheetViewCount.setText(getString(R.string.viewed_by) + statusViewHistoryList.size());
        for (int i = 0; i < statusViewHistoryList.size(); i++) {
            StatusHistoryModel statusHistoryModel = statusViewHistoryList.get(i);
            StatusListModel statusListModel = new StatusListModel();
            statusListModel.setProfileUrl(statusHistoryModel.getProfileUrl());
            statusListModel.setName(statusHistoryModel.getName());
            statusListModel.setId(statusHistoryModel.getUserId());
            statusListModel.setUpdatedTIme(statusHistoryModel.getViewedAt());
            data.add(statusListModel);
        }
        return data;
    }

    private void loadPicassoImage(StatusModel statusModel) {
        MyLog.d(TAG, "loadPicassoImage: ");
        mExoPlayerView.setVisibility(View.GONE);
        isPlayerPaused = false;
        ivStatusImage.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        ivStatusReplyDesc.setImageResource(R.drawable.camera);
        tvStatusReplyDesc.setText(getString(R.string.photo));

        pauseStoryView();
//Set Text Status


        if (isMyStatus) {
            if (statusModel.isTextstatus()) {

                ivStatusImage.setBackground(null);
                ivStatusImage.setImageResource(android.R.color.transparent);

                appbar.setBackgroundColor(Color.parseColor(statusModel.getTextstatus_color_code()));

                progressBar.setVisibility(View.GONE);
                ivStatusImage.setBackgroundColor(Color.parseColor(statusModel.getTextstatus_color_code()));
                String Caption = statusModel.getTextstatus_caption();
                status_text.setText(statusModel.getTextstatus_caption());
                //  findViewById(R.id.ll_download).setVisibility(View.GONE);

                storiesProgressView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        storiesProgressView.resume();
                    }
                }, PAUSE_DELAY);


            } else {
                Picasso.with(this).load(new File(statusModel.getLocalPath())).into(ivStatusImage, picassoImageCallBack);
            }
            //  Picasso.with(this).load(new File(statusModel.getLocalPath())).into(ivStatusImage, picassoImageCallBack);
        } else {

            if (statusModel.isTextstatus()) {

                ivStatusImage.setBackground(null);
                ivStatusImage.setImageResource(android.R.color.transparent);
                // findViewById(R.id.ll_download).setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);

                ivStatusImage.setBackgroundColor(Color.parseColor(statusModel.getTextstatus_color_code()));
                String Caption = statusModel.getTextstatus_caption();
                status_text.setVisibility(View.VISIBLE);

                status_text.setText(statusModel.getTextstatus_caption());

                appbar.setBackgroundColor(Color.parseColor(statusModel.getTextstatus_color_code()));

                storiesProgressView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        storiesProgressView.resume();
                    }
                }, PAUSE_DELAY);


            } else {
                status_text.setVisibility(View.GONE);
            }


            // picasso.with(this).load(statusModel.getServerPath()).into(ivStatusImage, picassoImageCallBack);
            // picasso.with(this).load(statusModel.getServerPath()).into(ivStatusReplyPreview, picassoImageCallBack);
            if (!AppUtils.isEmpty(statusModel.getServerPath())) {
                Log.d(TAG, "loadPicassoImage: " + statusModel.getServerPath());

                Glide
                        .with(context)
                        .load(AppUtils.getGlideURL(statusModel.getServerPath(), this))
                        .asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontAnimate()
                        .error(R.mipmap.chat_attachment_profile_default_image_frame)
                        .into(new SimpleTarget<Bitmap>() {

                            @Override
                            public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                // TODO Auto-generated method stub
                                Log.d(TAG, "onResourceReady: ");
                                ivStatusImage.setImageBitmap(arg0);
                                progressBar.setVisibility(View.GONE);
                                storiesProgressView.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        storiesProgressView.resume();
                                    }
                                }, PAUSE_DELAY);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                Log.e(TAG, "onLoadFailed: ", e);
                                storiesProgressView.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        storiesProgressView.resume();
                                    }
                                }, PAUSE_DELAY);
                                progressBar.setVisibility(View.GONE);
                            }
                        });
            }

        }

    }

    @Override
    public void onComplete() {
        finish();
    }

    @Override
    protected void onDestroy() {
        storiesProgressView.destroy();
        super.onDestroy();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean isMutedUser = CoreController.getStatusDB(StatusViewActivity.this).isMutedUser(currentUserId);

        if (isMyStatus) {
            menu.findItem(R.id.menu_status_mute).setVisible(false);
            menu.findItem(R.id.menu_status_unmute).setVisible(false);
        } else {
            if (isMutedUser) {
                menu.findItem(R.id.menu_status_unmute).setVisible(true);
                menu.findItem(R.id.menu_status_mute).setVisible(false);
            } else {
                menu.findItem(R.id.menu_status_mute).setVisible(true);
                menu.findItem(R.id.menu_status_unmute).setVisible(false);
            }
        }
        return super.onPrepareOptionsMenu(menu);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //    super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.activity_status_view, menu);
        return true;
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        pauseStoryView();
        pausePlayer();
        return super.onMenuOpened(featureId, menu);

    }

    @Override
    public void onPanelClosed(int featureId, Menu menu) {
        super.onPanelClosed(featureId, menu);
        resumeStories();
        resumePlayer();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        MyLog.d(TAG, "onOptionsItemSelected: currentUserId: " + currentUserId);
        String docId = data.get(currentIndex).getDocId();

        switch (item.getItemId()) {

            case R.id.menu_status_unmute:
                pauseStoryView();
                pausePlayer();
                StatusUtil.showMuteUnDialog(StatusViewActivity.this, data.get(currentIndex), this);
                return true;


            case R.id.menu_status_mute:
                pauseStoryView();
                pausePlayer();
                StatusUtil.showMuteUnDialog(StatusViewActivity.this, data.get(currentIndex), this);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (statusReplyContainer != null && statusReplyContainer.getVisibility() == View.VISIBLE) {
            hideReplyLayout();
            resumeStories();
            resumePlayer();
        } else {
            super.onBackPressed();
        }
    }

    private void hideReplyLayout() {
        statusReplyContainer.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.tv_status_reply:
                showReply();
                break;

            case R.id.iv_status_reply_send:
                sendStatusReply();
                break;


            case R.id.iv_status_reply_smile:

                switchKeyBoard("smile");
                break;


            case R.id.iv_status_reply_keyboard:
                switchKeyBoard("keyboard");
                break;


            case R.id.status_send_btn:
                sendStatusReply();
                break;


            case R.id.iv_status_reply_record:
                if (!checkAudioRecordPermission()) {
                    requestAudioRecordPermission();
                } else {
                    recordAudio();
                }
                break;

            case R.id.iv_my_status_eye:
                setBottomSheetAdapterAndShow();
                break;

            case R.id.tv_my_status_view_count:
                setBottomSheetAdapterAndShow();
                break;


            case R.id.iv_menu_icon:
                if (toolbar != null) {
                    toolbar.showOverflowMenu();
                }
                break;

            case R.id.iv_delete:
                //      Toast.makeText(StatusViewActivity.this,"Delete status", Toast.LENGTH_SHORT).show();
                final StatusModel statusModel_ = data.get(currentIndex);
                CommonAlertDialog.showDialog(StatusViewActivity.this, new CommonAlertDialog.DialogListener() {
                            @Override
                            public void onPositiveBtnClick(Object result) {
                                if (AppUtils.isNetworkAvailable(getApplicationContext())) {
                                    boolean isDeleted = StatusUtil.statusDelete(statusModel_, StatusViewActivity.this);
                                    if (isMyStatus) {
                                        //  Toast.makeText(StatusViewActivity.this, "Status deleted", Toast.LENGTH_SHORT).show();
                                        if (!isDeleted) {
                                            Intent intent = new Intent();
                                            setResult(RESULT_OK, intent);
                                        }
                                        finish();

                                    }
                                } else {
                                    Toast.makeText(StatusViewActivity.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onNegativeBtnClick() {

                            }
                        }, "Delete this status update?", "It will also be deleted for everyone who received it.",
                        "DELETE", "CANCEL", "");

                break;

            case R.id.iv_forward:
                //    Toast.makeText(StatusViewActivity.this,"Forward status", Toast.LENGTH_SHORT).show();
                StatusModel statusModel = data.get(currentIndex);
                if (statusModel != null) {
                    boolean isImage = false;
                    if (statusModel.isImage())
                        isImage = true;
                    StatusUtil.shareImageOrVideo(statusModel.getCaption(),
                            Uri.fromFile(new File(statusModel.getLocalPath())),
                            isImage, getApplicationContext());
                }
                break;
        }
    }

    private void switchKeyBoard(String from) {

        switch (from) {
            case "smile":
                ivStatusReplySmile.setVisibility(View.GONE);
                //show emoji keyboard
                emojIcon.ShowEmojIcon();
                ivStatusReplyKeyboard.setVisibility(View.VISIBLE);
                break;

            case "keyboard":
                ivStatusReplyKeyboard.setVisibility(View.GONE);
                //show normal keyboard
                emojIcon.closeEmojIcon();
                ivStatusReplySmile.setVisibility(View.VISIBLE);

                break;
        }

    }

    private void recordAudio() {
        try {
            if (!audioRecordStarted) {
                audioRecordStarted = true;
                File audioDir = new File(MessageFactory.AUDIO_STORAGE_PATH);
                if (!audioDir.exists()) {
                    audioDir.mkdirs();
                }

                audioRecordPath = MessageFactory.AUDIO_STORAGE_PATH + MessageFactory.getMessageFileName(MessageFactory.audio,
                        Calendar.getInstance().getTimeInMillis() + "rc", ".mp3");
                File file = new File(audioRecordPath);
                try {
                    file.createNewFile();
                } catch (Exception e) {
                    MyLog.d(TAG, "startAudioRecord: default path not working");
                    audioRecordPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getAbsolutePath() +
                            MessageFactory.getMessageFileName(MessageFactory.audio,
                                    Calendar.getInstance().getTimeInMillis() + "rc", ".mp3");
                    file = new File(audioRecordPath);
                    try {
                        file.createNewFile();
                    } catch (Exception e1) {
                        MyLog.e(TAG, "startAudioRecord: ", e1);
                    }
                }
                audioRecorder = new MediaRecorder();
                audioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                audioRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                audioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                audioRecorder.setOutputFile(audioRecordPath);
                audioRecorder.prepare();
                audioRecorder.start();
                myChronometer.start();
                myChronometer.setBase(SystemClock.elapsedRealtime());
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            MyLog.e(TAG, "recordAudio: ", e);
        }
    }

    private void sendStatusReply() {
        hideKeyboard();
        if (edStatusReply != null && edStatusReply.getText() != null) {

            StatusModel statusModel = data.get(currentIndex);
            String replyMsg = edStatusReply.getText().toString();
            if (replyMsg == null || replyMsg.trim().isEmpty())
                return;
            SessionManager sessionManager = SessionManager.getInstance(this);
            String from = sessionManager.getCurrentUserID();
            String senderName = sessionManager.getnameOfCurrentUser();
            String to = statusModel.getUserId();
            String receiverName = StatusUtil.getNameById(this, to);
            String recordId = statusModel.getRecordId();
            //call reply event
            StatusSocketEvents.sendStatusReply(this, from, to, senderName,
                    receiverName, recordId, statusModel.isVideo(), statusModel.getThumbNail(),
                    statusModel.getMsisdn(), statusModel.getServerPath(), replyMsg, statusModel.getDocId());
            finish();
        }
    }

    private void setBottomSheetAdapterAndShow() {
        List<StatusListModel> data = fetchSeenByMemebersData();
        rvSeenBy.setAdapter(new StatusSeenByAdapter(this, data));
        storiesProgressView.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()) {
                    storiesProgressView.pause();
                    pausePlayer();
                    mBottomSheetDialog.show();
                }
            }
        }, PAUSE_DELAY);

    }

    private void showReply() {
//        ivStatusReplyRecording.setVisibility(View.VISIBLE);
        statusReplyContainer.setVisibility(View.VISIBLE);
        edStatusReply.requestFocus();
        showKeyboard();
        pauseStoryView();
        pausePlayer();
    }

    private void pauseStoryView() {
        try {
            storiesProgressView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    storiesProgressView.pause();
                }
            }, PAUSE_DELAY);
        } catch (Exception e) {
            MyLog.e(TAG, "showReply: ", e);
        }
    }

    private void requestAudioRecordPermission() {
        ActivityCompat.requestPermissions(StatusViewActivity.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, AUDIO_RECORD_PERMISSION_REQUEST_CODE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event != null && event.getEventName() != null) {
            MyLog.d(TAG, "onMessageEvent: " + event.getEventName());
            switch (event.getEventName()) {
                case SocketManager.EVENT_STATUS_ACK:
                    MyLog.d(TAG, "onMessageEvent: EVENT_STATUS_ACK ");
                    currentUserId = sessionManager.getCurrentUserID();
                    StatusUtil.statusAckResult(this, event, currentUserId);
                    break;

/*                case SocketManager.EVENT_STATUS_DELETE:
                    MyLog.d(TAG, "onMessageEvent: ");
                    StatusUtil.afterStatusDelete(event);
                    break;*/

            }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    @Override
    public void onPositiveBtnClick(Object result) {
        try {
            resumeStories();
            resumePlayer();
            StatusModel selectedItemData = (StatusModel) result;
            StatusUtil.muteUnmute(StatusViewActivity.this, selectedItemData);
        } catch (Exception e) {
            MyLog.e(TAG, "onPositiveBtnClick: ", e);
        }
    }

    @Override
    public void onNegativeBtnClick() {
        resumeStories();
        resumePlayer();
    }

    public void drag(MotionEvent event) {
        matrix.getValues(matrixValues);

        float left = matrixValues[2];
        float top = matrixValues[5];
        float bottom = (top + (matrixValues[0] * mBitmapHeight)) - mViewHeight;
        float right = (left + (matrixValues[0] * mBitmapWidth)) - mViewWidth;

        float eventX = event.getX();
        float eventY = event.getY();
        float spacingX = eventX - mStartPoint.x;
        float spacingY = eventY - mStartPoint.y;
        float newPositionLeft = (left < 0 ? spacingX : spacingX * -1) + left;
        float newPositionRight = (spacingX) + right;
        float newPositionTop = (top < 0 ? spacingY : spacingY * -1) + top;
        float newPositionBottom = (spacingY) + bottom;
        boolean x = true;
        boolean y = true;

        if (newPositionRight < 0.0f || newPositionLeft > 0.0f) {
            if (newPositionRight < 0.0f && newPositionLeft > 0.0f) {
                x = false;
            } else {
                eventX = oldEventX;
                mStartPoint.x = oldStartPointX;
            }
        }
        if (newPositionBottom < 0.0f || newPositionTop > 0.0f) {
            if (newPositionBottom < 0.0f && newPositionTop > 0.0f) {
                y = false;
            } else {
                eventY = oldEventY;
                mStartPoint.y = oldStartPointY;
            }
        }

        if (mDraggable) {
            matrix.set(savedMatrix);
            matrix.postTranslate(x ? eventX - mStartPoint.x : 0, y ? eventY - mStartPoint.y : 0);
            //  this.setImageMatrix(matrix);
            if (x) oldEventX = eventX;
            if (y) oldEventY = eventY;
            if (x) oldStartPointX = mStartPoint.x;
            if (y) oldStartPointY = mStartPoint.y;
        }

    }

    public void zoom(MotionEvent event) {
        matrix.getValues(matrixValues);

        float newDist = spacing(event);
        float bitmapWidth = matrixValues[0] * mBitmapWidth;
        float bimtapHeight = matrixValues[0] * mBitmapHeight;
        boolean in = newDist > oldDist;

        if (!in && matrixValues[0] < 1) {
            return;
        }
        mDraggable = bitmapWidth > mViewWidth || bimtapHeight > mViewHeight;

        float midX = (mViewWidth / 2);
        float midY = (mViewHeight / 2);

        matrix.set(savedMatrix);
        scale = newDist / oldDist;
        matrix.postScale(scale, scale, bitmapWidth > mViewWidth ? mMiddlePoint.x : midX, bimtapHeight > mViewHeight ? mMiddlePoint.y : midY);

        //  this.setImageMatrix(matrix);


    }

    /**
     * Determine the space between the first two fingers
     */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);

        return (float) Math.sqrt(x * x + y * y);
    }

    /**
     * Calculate the mid point of the first two fingers
     */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    private void showToast(CharSequence text) {

    }


    enum Mode {
        NONE,
        DRAG,
        ZOOM
    }

    public class StandardGestures implements View.OnTouchListener, GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener, ScaleGestureDetector.OnScaleGestureListener {
        private View view;
        private GestureDetector gesture;
        private ScaleGestureDetector gestureScale;
        private float scaleFactor = 1;
        private boolean inScale;

        public StandardGestures(Context c) {
            gesture = new GestureDetector(c, this);
            gestureScale = new ScaleGestureDetector(c, this);
        }

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            this.view = view;
            gesture.onTouchEvent(event);
            gestureScale.onTouchEvent(event);

            boolean isVideoClick = false;

            if (view.getId() == R.id.status_video_player_view) {
                isVideoClick = true;
            }

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    firstTouchPosition = event.getX();
                    onStatusPress();
                    if (isVideoClick) {
                        pausePlayer();
                    }
                    return true;
                case MotionEvent.ACTION_UP:
                    float finalTouchPosition = event.getX();
                    float deltaX = finalTouchPosition - firstTouchPosition;

                    if (Math.abs(deltaX) > MIN_DISTANCE) {
                        resumeStories();
                        // Right to left swipe action
                        if (finalTouchPosition > firstTouchPosition) {
                            MyLog.d(TAG, "onTouch: right to left");
                            loadNextProfile(true);
                        }
                        // Left to Right swipe action
                        else {
                            MyLog.d(TAG, "onTouch: left to right");
                            loadNextProfile(false);
                        }

                    } else {
                        boolean isClickEvent = onTap(isVideoClick, finalTouchPosition, StatusViewActivity.this);
                        if (!isClickEvent) {
                            resumeStories();
                            if (isVideoClick) {
                                resumePlayer();
                            }
                        }
                    }
                    return true;
            }

            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public void onShowPress(MotionEvent e) {

        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return false;
        }


        @Override
        public void onLongPress(MotionEvent e) {

        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent event) {
            view.setVisibility(View.GONE);
            return true;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent event) {
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent event) {
            return true;
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {

            scaleFactor *= detector.getScaleFactor();
            scaleFactor = scaleFactor < 1 ? 1 : scaleFactor; // prevent our image from becoming too small
            scaleFactor = (float) (int) (scaleFactor * 100) / 100; // Change precision to help with jitter when user just rests their fingers //
            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);
            //   onScroll(null, null, 0, 0); // call scroll to make sure our bounds are still ok //
            return true;
        }


        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            inScale = true;
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            inScale = false;
            //  onScroll(null, null, 0, 0); // call scroll to make sure our bounds are still ok //
        }


    }

    private class PasuePlayerTapListener implements OnPausePlayer {

        @Override
        public void onPauseTap() {
            //   showToast(String.format("Paused Player"));
            pausePlayer();
        }
    }

    private class ResumePlayerTapListener implements OnResumePlayer {


        @Override
        public void onResumePlayerTap() {
            //   showToast(String.format("Resume Player"));
            resumePlayer();
        }
    }

    private class ResumeStoriesTapListener implements OnResumeStory {

        @Override
        public void OnResumeStory() {
            //    showToast(String.format("Resume Stories"));
            resumeStories();
        }
    }

    private class StatusPressTapListener implements OnStatusPress {


        @Override
        public void OnStatusPress() {
            //  showToast(String.format("Status Press"));
            onStatusPress();
        }
    }

    private class LoadProfileTapListener implements OnLoadProfile {

        @Override
        public void OnLoadProfile(Boolean mLoad) {
            //  showToast(String.format("OnLoadProfile  Press") + mLoad);
            loadNextProfile(mLoad);
        }
    }

    private class PhotoTapListener implements OnPhotoTapListener {

        @Override
        public void onPhotoTap(ImageView view, float x, float y) {
            float xPercentage = x * 100f;
            float yPercentage = y * 100f;
        }
    }

    private class MatrixChangeListener implements OnMatrixChangedListener {

        @Override
        public void onMatrixChanged(RectF rect) {
            // mCurrMatrixTv.setText(rect.toString());
        }
    }

    private class SingleFlingListener implements OnSingleFlingListener {

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            //   Log.d("PhotoView", String.format(FLING_LOG_STRING, velocityX, velocityY));
            return true;
        }
    }
}
