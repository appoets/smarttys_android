package com.app.Smarttys.status.model;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user134 on 4/2/2018.
 */

public class StatusModel implements Serializable {

    private String id;
    private String docId;
    private String userId;
    private String localPath;
    private String serverPath;
    private boolean isImage;
    private boolean isVideo;
    private boolean isSelected;
    private String caption;
    private String status; //0-not-send 1-uploaded 2-viewed 3-deleted
    private long timeUploaded;
    private String profileURL;
    private String mobileNumber;
    private String thumbNail;
    private long fileSize;
    private String msisdn;
    private String recordId;
    private String convId;
    private int videoStartPosition;
    private int videoEndPosition;
    private boolean isTrimmed;
    private long timeSelected;
    private String name;


    //Text Status


    private boolean isTextstatus;
    private String Textstatus_caption = "";
    private String Textstatus_color_code = "";
    private String Textstatus_text_font = "";
    private String themeCategory = "";

    private String duration = "";
    private String Status_feelings = "";
    private List<StatusHistoryModel> statusViewHistory = new ArrayList<>();

    //----------------------------------Text Status----------------------------------------------------
    public String getThemeCategory() {
        return themeCategory;
    }

    public void setThemeCategory(String themeCategory) {
        this.themeCategory = themeCategory;
    }

    public boolean isTextstatus() {
        return isTextstatus;
    }

    public void setTextstatus(boolean textstatus) {
        isTextstatus = textstatus;
    }

    public String getTextstatus_caption() {
        return Textstatus_caption;
    }

//----------------------------------------Status Feelings----------------------------------

    public void setTextstatus_caption(String textstatus_caption) {
        Textstatus_caption = textstatus_caption;
    }

    public String getStatus_feelings() {
        return Status_feelings;
    }

    public void setStatus_feelings(String status_feelings) {
        Status_feelings = status_feelings;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getduration() {
        return duration;
    }

    public String getTextstatus_color_code() {
        return Textstatus_color_code;
    }

    public void setTextstatus_color_code(String textstatus_color_code) {
        Textstatus_color_code = textstatus_color_code;
    }

    public String getTextstatus_text_font() {
        return Textstatus_text_font;
    }

    public void setTextstatus_text_font(String textstatus_text_font) {
        Textstatus_text_font = textstatus_text_font;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public boolean isImage() {
        return isImage;
    }

    public void setImage(boolean image) {
        isImage = image;
    }

    public boolean isVideo() {
        return isVideo;
    }

    public void setVideo(boolean video) {
        isVideo = video;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getCaption() {
        if (caption == null)
            caption = "";
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public long getTimeUploaded() {
        return timeUploaded;
    }

    public void setTimeUploaded(long timeUploaded) {
        this.timeUploaded = timeUploaded;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        if (status == null || status.trim().isEmpty())
            status = "0"; // default status --> offline
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getServerPath() {
        if (serverPath != null && !serverPath.contains("?id"))
            return serverPath + "?id=0";
        else
            return serverPath;
    }

    public void setServerPath(String serverPath) {
        this.serverPath = serverPath;
    }

    public String getProfileURL() {

        return profileURL;
    }

    public void setProfileURL(String profileURL) {
        this.profileURL = profileURL;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getThumbNail() {
        return thumbNail;
    }

    public void setThumbNail(String thumbNail) {
        this.thumbNail = thumbNail;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }


    public List<StatusHistoryModel> getStatusViewHistory() {
        if (statusViewHistory == null)
            return new ArrayList<>();
        return statusViewHistory;
    }

    public void setStatusViewHistory(List<StatusHistoryModel> statusViewHistory) {
        this.statusViewHistory = statusViewHistory;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getConvId() {
        return convId;
    }

    public void setConvId(String convId) {
        this.convId = convId;
    }

    public int getVideoEndPosition() {
        return videoEndPosition;
    }

    public void setVideoEndPosition(int videoEndPosition) {
        this.videoEndPosition = videoEndPosition;
    }

    public int getVideoStartPosition() {
        return videoStartPosition;
    }

    public void setVideoStartPosition(int videoStartPosition) {
        this.videoStartPosition = videoStartPosition;
    }

    public boolean isTrimmed() {
        return isTrimmed;
    }

    public void setTrimmed(boolean trimmed) {
        isTrimmed = trimmed;
    }

    public long getTimeSelected() {
        return timeSelected;
    }

    public void setTimeSelected(long timeSelected) {
        this.timeSelected = timeSelected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NonNull
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
