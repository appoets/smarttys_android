package com.app.Smarttys.status.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.VideoRequestHandler;
import com.app.Smarttys.status.controller.StatusUtil;
import com.app.Smarttys.status.controller.interfaces.OnGalleryItemClickListener;
import com.app.Smarttys.status.controller.interfaces.SwipeCallBack;
import com.app.Smarttys.status.model.StatusModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by user134 on 3/27/2018.
 */

public class GalleryImagesAdapter extends RecyclerView.Adapter<GalleryImagesAdapter.MyViewHolder> {
    private static final String TAG = GalleryImagesAdapter.class.getSimpleName();
    private List<StatusModel> mData = new ArrayList<>();
    private Context mContext;
    private int totalSelected;
    private boolean isGalleryPage;
    private OnGalleryItemClickListener onItemClickListener;
    private Picasso picassoInstance;
    private SwipeCallBack swipeCallBack;
    private HashMap<Integer, StatusModel> selectedItems = new HashMap<>();
    private float x1 = 0, x2 = 0;
    private float y1 = 0, y2 = 0;
    private long t1 = 0;
    private long CLICK_DURATION = 300;

    public GalleryImagesAdapter(Context context, OnGalleryItemClickListener onItemClickListener,
                                SwipeCallBack swipeCallBack, boolean isGalleryPage, HashMap<Integer, StatusModel> selectedItems) {
        mData = AppUtils.getAllShownImagesPath(context);
        mContext = context;
        this.isGalleryPage = isGalleryPage;
        this.onItemClickListener = onItemClickListener;
        this.swipeCallBack = swipeCallBack;
        if (selectedItems != null) {
            this.selectedItems = selectedItems;
            totalSelected = this.selectedItems.size();
            setPrevSelectedItems();
        }
        VideoRequestHandler videoRequestHandler = new VideoRequestHandler();
        picassoInstance = new Picasso.Builder(context.getApplicationContext())
                .indicatorsEnabled(true)
                .addRequestHandler(videoRequestHandler)
                //.memoryCache(new LruCache(mContext))
                .build();
    }

    public void updateSelectedItems(HashMap<Integer, StatusModel> selectedItems) {
        reset();
        if (selectedItems != null && !selectedItems.isEmpty()) {
            this.selectedItems = selectedItems;
            totalSelected = this.selectedItems.size();
            setPrevSelectedItems();
        } else {
            this.selectedItems = new HashMap<>();
            totalSelected = 0;
            mData.clear();
        }
    }

    private void reset() {
        if (mData != null && mData.size() > 0)
            for (int i = 0; i < mData.size(); i++) {
                mData.get(i).setSelected(false);
            }
    }

    private void setPrevSelectedItems() {
        if (mData == null || selectedItems == null)
            return;

        Set<Integer> selectedIndexes = selectedItems.keySet();
        try {
            for (int index : selectedIndexes) {
                mData.get(index).setSelected(true);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "setPrevSelectedItems: ", e);
        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (isGalleryPage)
            view = LayoutInflater.from(mContext).inflate(R.layout.status_list_row_gallery_page, parent, false);
        else
            view = LayoutInflater.from(mContext).inflate(R.layout.status_list_row_gallery_horizontal, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.setIsRecyclable(false);
        if (mData != null && mData.get(position) != null) {




/*            holder.ivGalleryImage.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    int selectedPosition= 0;
                    try{
                        String positionString= holder.ivGalleryImage.getTag(R.string.position).toString();
                        selectedPosition= Integer.parseInt(positionString);

                    }
                    catch (Exception e){
                        Log.e(TAG, "onTouch: ",e );
                    }
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            MyLog.d(TAG, "onTouch: down");
                            x1 = event.getX();
                            y1 = event.getY();
                            t1 = System.currentTimeMillis();

                            return true;

                        case MotionEvent.ACTION_UP:
                            x2 = event.getX();
                            y2 = event.getY();
                            float yAxisDifference=  y1-y2;
                            MyLog.d(TAG, "onTouch: up yaxis differnce "+yAxisDifference);
                            long t2 = System.currentTimeMillis();
                            if ((yAxisDifference>10) ) {
                                //swipe top
                                if(swipeCallBack!=null)
                                    swipeCallBack.onSwipeTop();
                            }
                            //if (x1 == x2 && y1 == y2 && (t2 - t1) < CLICK_DURATION) {
                            else{
                                //click

                                clickItem(holder,selectedPosition);
                            }
                            return true;
                    }

                    return false;
                }
            });*/
            StatusModel statusModel = mData.get(position);
            final String path = statusModel.getLocalPath();
            if (statusModel.isImage()) {
                holder.ivVideoIcon.setVisibility(View.GONE);

                if (holder.ivGalleryImage.getTag() == null) {
                    File localFile = new File(path);
                    Glide.with(mContext)
                            .load(localFile).asBitmap()
                            .override(100, 130)
                            //.diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    holder.ivGalleryImage.setImageBitmap(resource);
                                    holder.ivGalleryImage.setTag("added");
                                }
                            });
                }
            } else {
                if (holder.ivGalleryImage.getTag() == null) {
                    holder.ivVideoIcon.setVisibility(View.VISIBLE);
                    holder.ivGalleryImage.setBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
                    //  new LoadVideoThumbnailTask(path,holder.ivGalleryImage).execute();
                    picassoInstance.load(VideoRequestHandler.SCHEME_VIDEO + ":" + path).
                            //    resize(100,130).
                                    into(holder.ivGalleryImage);
                }
            }
            if (statusModel.isSelected()) {
                holder.ivSelect.setVisibility(View.VISIBLE);
                holder.bg_select_video.setBackgroundResource(R.color.trans);
            } else {
                holder.ivSelect.setVisibility(View.GONE);
                holder.bg_select_video.setBackgroundResource(0);
            }
            holder.ivGalleryImage.setTag(R.string.position, "" + position);
            holder.ivGalleryImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickItem(holder, position);
                }
            });
        }
    }

    private void clickItem(MyViewHolder holder, int selectedPosition) {
        holder.ivSelect.setVisibility(View.GONE);
        holder.bg_select.setBackgroundResource(0);
        if (mData.get(selectedPosition).isSelected()) {
            totalSelected--;
            mData.get(selectedPosition).setSelected(false);
            if (isGalleryPage) {
                holder.ivSelect.setVisibility(View.GONE);
                holder.bg_select.setBackgroundResource(0);
                if (totalSelected != selectedItems.size()) {
                    selectedItems.remove(selectedPosition);
                }
            } else {
                holder.ivSelect.setVisibility(View.GONE);
                holder.bg_select.setBackgroundResource(0);
                selectedItems.remove(selectedPosition);
                //notifyItemChanged(selectedPosition);
            }
            if (onItemClickListener != null)
                onItemClickListener.onItemClick(totalSelected, selectedItems);

        } else {
            if (totalSelected > 29) {
                Toast.makeText(mContext, mContext.getString(R.string.max_upload_limit_msg), Toast.LENGTH_SHORT).show();
            } else {
                boolean isVideo = false, shallWeUpload = false;
                if (mData.get(selectedPosition).isVideo()) {
                    isVideo = true;
                    long videoDuration = StatusUtil.getVideoDuration(mData.get(selectedPosition).getLocalPath(), false);
                    MyLog.d(TAG, "onTouch: " + videoDuration);
                                                /*if(videoDuration<=30000){
                                                    shallWeUpload=true;
                                                }
                                                */
                    shallWeUpload = true;
                }
                if (isVideo) {
                    if (!shallWeUpload) {
                        Toast.makeText(mContext, "Please select less than 30 seconds video", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    shallWeUpload = true;
                }

                if (shallWeUpload) {
                    totalSelected++;
                    mData.get(selectedPosition).setSelected(true);
                    mData.get(selectedPosition).setTimeSelected(System.currentTimeMillis());
                    selectedItems.put(selectedPosition, mData.get(selectedPosition));
                                              /*  if(isGalleryPage)
                                                holder.ivSelect.setVisibility(View.VISIBLE);
                                                else
                                                notifyItemChanged(selectedPosition);*/
                    if (isGalleryPage) {
                        holder.ivSelect.setVisibility(View.VISIBLE);
                        holder.bg_select.setBackgroundResource(R.color.trans);
                    } else {
                        holder.ivSelect.setVisibility(View.VISIBLE);
                        holder.bg_select.setBackgroundResource(R.color.trans);
                    }
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(totalSelected, selectedItems);
                }

            }
        }
    }

    @Override
    public int getItemCount() {
        if (mData == null)
            return 0;

        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivGalleryImage, ivSelect, ivVideoIcon;
        private View rootView;
        private FrameLayout bg_select, bg_select_video;

        public MyViewHolder(View itemView) {
            super(itemView);
            rootView = itemView.findViewById(R.id.root_view);
            ivSelect = itemView.findViewById(R.id.iv_select);
            ivGalleryImage = itemView.findViewById(R.id.iv_gallery_image);
            ivVideoIcon = itemView.findViewById(R.id.iv_video_icon);
            bg_select = itemView.findViewById(R.id.bg_select);
            bg_select_video = itemView.findViewById(R.id.bg_select);
        }
    }


}