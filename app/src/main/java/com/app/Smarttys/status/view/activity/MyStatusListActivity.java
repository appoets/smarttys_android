package com.app.Smarttys.status.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.service.Constants;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.status.controller.StatusUtil;
import com.app.Smarttys.status.controller.interfaces.OnListClickListener;
import com.app.Smarttys.status.model.StatusModel;
import com.app.Smarttys.status.view.adapter.MyStatusListAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user134 on 3/29/2018.
 */

public class MyStatusListActivity extends CoreActivity implements View.OnClickListener, OnListClickListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = MyStatusListActivity.class.getSimpleName();
    private RecyclerView mRvMyStatus;
    private MyStatusListAdapter mAdapter;
    private String mCurrentUserId;
    private TextView tvTitle;
    private ImageView ivDelete, ivForward;
    private Context mContext;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_my_status_list);

        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        mContext = MyStatusListActivity.this;

        mCurrentUserId = SessionManager.getInstance(this).getCurrentUserID();
        findViewById(R.id.iv_back).setOnClickListener(this);
        mRvMyStatus = findViewById(R.id.rv_my_status_list);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        mRvMyStatus.setLayoutManager(AppUtils.getDefaultLayoutManger(this));
        //for all version support including pre-lollipop
        ViewCompat.setNestedScrollingEnabled(mRvMyStatus, false);
        tvTitle = findViewById(R.id.tv_title);
        ivDelete = findViewById(R.id.iv_delete);
        ivForward = findViewById(R.id.iv_forward);
        ivForward.setOnClickListener(this);
        ivDelete.setOnClickListener(this);

        getofflineStatusAck();

        //getOfflineDeletedStatus();
    }

    public void getofflineStatusAck() {
        SendMessageEvent groupMsgEvent = new SendMessageEvent();
        groupMsgEvent.setEventName(SocketManager.EVENT_GET_ACKSTATUS);
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            groupMsgEvent.setMessageObject(object);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        EventBus.getDefault().post(groupMsgEvent);
    }

    private void getOfflineDeletedStatus() {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_STATUSDELETE);
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void loadofflinestatus(String data) {
        try {
            JSONObject objects = new JSONObject(data);
            Log.e(TAG, "loadofflinestatus" + objects);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void setAdapter() {
        List<StatusModel> myStatusList = CoreController.getStatusDB(this).getMyAllStatus();
        if (myStatusList != null && myStatusList.size() > 0) {
            if (mAdapter != null) {
                List<StatusModel> oldData = mAdapter.getData();
            }
            mAdapter = new MyStatusListAdapter(this, myStatusList, this);
            mRvMyStatus.setAdapter(mAdapter);
            findViewById(R.id.tv_status_disapear_msg).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.tv_status_disapear_msg).setVisibility(View.GONE);
            mRvMyStatus.setAdapter(null);
            mRvMyStatus.setVisibility(View.GONE);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //when back icon click
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setAdapter();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.iv_delete:
                deleteStatus();
                break;
            case R.id.iv_forward:
                forwardStatus();
                break;
        }
    }

    private void deleteStatus() {
        if (AppUtils.isNetworkAvailable(this)) {
            if (mAdapter != null && mAdapter.getData().size() > 0) {
                boolean result = false;
                for (StatusModel statusModel : mAdapter.getData()) {
                    if (statusModel.isSelected()) {
                        result = StatusUtil.statusDelete(statusModel, this);
                    }
                }
/*                if(result)
                    Toast.makeText(this, "Status deleted", Toast.LENGTH_SHORT).show();*/
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        } else {
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }

    private void forwardStatus() {
        ArrayList<StatusModel> selectedItems = new ArrayList<>();
        if (mAdapter != null && mAdapter.getData().size() > 0) {
            for (StatusModel statusModel : mAdapter.getData()) {
                if (statusModel.isSelected()) {
                    selectedItems.add(statusModel);
                }
            }
            StatusUtil.shareMultipleStatus(selectedItems, this);
        }
    }

    private void hideIcons() {
        ivDelete.setVisibility(View.GONE);
        ivForward.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.my_status));
    }

    private void showIcons(int totalSelectedItems) {
        ivDelete.setVisibility(View.VISIBLE);
        ivForward.setVisibility(View.VISIBLE);
        tvTitle.setText(String.valueOf(totalSelectedItems));
    }

    @Override
    public void onBackPressed() {
        if (mAdapter != null && mAdapter.isLongPressed()) {
            hideIcons();
            mAdapter.resetLongPress();
            mAdapter.resetSelection();
            mAdapter.notifyDataSetChanged();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event != null && event.getEventName() != null) {
            MyLog.d(TAG, "onMessageEvent: " + event.getEventName());
            switch (event.getEventName()) {
                case SocketManager.EVENT_STATUS_UPDATE:
                    MyLog.d(TAG, "onMessageEvent: EVENT_STATUS_UPDATE ");
                    StatusUtil.statusAckByOthers(this, event, mCurrentUserId);
                    mRvMyStatus.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setAdapter();
                        }
                    }, 1000);
                    //     Toast.makeText(this, "Status updated by other end", Toast.LENGTH_SHORT).show();
                    break;
                case SocketManager.EVENT_STATUS_DELETE:
                    StatusUtil.afterStatusDelete(event, this);
                    mRvMyStatus.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            hidepDialog();
                            setAdapter();
                        }
                    }, 1000);
                    break;
                case SocketManager.EVENT_STATUSDELETE:
                    // Log.e(TAG, "onMessageEvent: EVENT_STATUSDELETE ");
                    String result = event.getObjectsArray()[0].toString();
                    JSONObject object = null;
                    try {
                        object = new JSONObject(result);
                        String mErr = "";
                        Log.e(TAG, "EVENT_STATUSDELETE: " + object);
                        if (object.has("err")) {
                            mErr = object.getString("err");
                            if (Integer.valueOf(mErr) == 0) {
                                if (object.has("status")) {
                                    JSONArray array = object.optJSONArray("status");
                                    for (int i = 0; i < array.length(); i++) {
                                        try {
                                            JSONObject msgObj = array.getJSONObject(i);
                                            String doc_id = msgObj.getString("doc_id");
                                            CoreController.getStatusDB(mContext).deleteStatus(doc_id);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                            mRvMyStatus.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    hidepDialog();
                                    setAdapter();
                                }
                            }, 2000);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case SocketManager.EVENT_GET_ACKSTATUS:
                    MyLog.d(TAG, "onMessageEvent: EVENT_GET_ACKSTATUS ");
                    //Update my status Ack by others for offline event
                    StatusUtil.statusAckByOthersOffline(this, event, mCurrentUserId);
                    mRvMyStatus.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Notify my adapter
                            // setAdapter();
                            notifyAdapter();
                        }
                    }, 1000);
                    //     StatusUtil.statusAckByOthers(this, event, mCurrentUserId);
                  /*  mRvMyStatus.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setAdapter();
                        }
                    }, 1000);*/
                    //     Toast.makeText(this, "Status updated by other end", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

    private void notifyAdapter() {
        List<StatusModel> myStatusList = CoreController.getStatusDB(this).getMyAllStatus();
        if (myStatusList != null && myStatusList.size() > 0) {
            mAdapter = new MyStatusListAdapter(this, myStatusList, this);
            mRvMyStatus.setAdapter(mAdapter);
            findViewById(R.id.tv_status_disapear_msg).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.tv_status_disapear_msg).setVisibility(View.GONE);
            mRvMyStatus.setAdapter(null);
            mRvMyStatus.setVisibility(View.GONE);
            finish();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(MyStatusListActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.STATUS_DELETE_REQUEST && resultCode == RESULT_OK) {
            showProgres();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(MyStatusListActivity.this);
    }

    @Override
    public void onItemClick(int position, Object result, boolean isLongPress) {
        if (isLongPress) {
            if (mAdapter.getData() != null && mAdapter.getData().size() > 0) {
                boolean isSelected = false;
                int totalSelection = 0;
                for (StatusModel statusModel : mAdapter.getData()) {
                    if (statusModel.isSelected()) {
                        totalSelection++;
                        if (!isSelected)
                            isSelected = true;
                    }
                }
                if (isSelected) {
                    showIcons(totalSelection);
                } else {
                    hideIcons();
                    mAdapter.resetLongPress();
                }
            } else {
                hideIcons();
            }
        } else {
            hideIcons();
        }
    }

    @Override
    public void onRefresh() {
        hideIcons();
        swipeRefreshLayout.setRefreshing(true);
        setAdapter();
        swipeRefreshLayout.setRefreshing(false);
    }
}