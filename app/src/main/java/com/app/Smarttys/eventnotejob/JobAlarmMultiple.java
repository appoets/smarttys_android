package com.app.Smarttys.eventnotejob;

import android.content.Context;

import androidx.annotation.NonNull;

import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.socket.MessageService;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.wang.avi.indicators.BallClipRotateIndicator;

/**
 * Created by user145 on 6/23/2018.
 */
public class JobAlarmMultiple extends Job {

    public static final String TAG = ">>>> JobAlarmMultiple";

    public static void scheduleJob() {
        new JobRequest.Builder(JobAlarmMultiple.TAG)
                .setExecutionWindow(2000, 2000)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setUpdateCurrent(true)
                .build()
                .schedule();

        new JobRequest.Builder(JobAlarmMultiple.TAG)
                .setExecutionWindow(10000, 10000)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setUpdateCurrent(true)
                .build()
                .schedule();

        new JobRequest.Builder(JobAlarmMultiple.TAG)
                .setExecutionWindow(60000, 60000)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setUpdateCurrent(true)
                .build()
                .schedule();


        new JobRequest.Builder(JobAlarmMultiple.TAG)
                .setExecutionWindow(120000, 120000)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    private static void cancelJob(int jobId) {
        JobManager.instance().cancel(jobId);
    }

    @Override
    protected boolean isRequirementDeviceIdleMet() {
        MyLog.d(TAG, "isRequirementDeviceIdleMet: ");
        return super.isRequirementDeviceIdleMet();
    }

    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        // run your job here
        MyLog.d(TAG, "onRunJob: ");
        final Context context = this.getContext();
        if (!AppUtils.isMyServiceRunning(context, MessageService.class)) {
            //     MyLog.e("isMyServiceRunning","isMyServiceRunning started");
            AppUtils.startService(context, BallClipRotateIndicator.class);
        } else {
            //    MyLog.e("isMyServiceRunning", "isMyServiceRunning running already");
        }
        return Result.SUCCESS;
    }
}
