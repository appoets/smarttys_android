package com.app.Smarttys.app.utils;

/**
 * Created by user145 on 3/12/2018.
 */
public class AppDataUtil {
    private static AppDataUtil ourInstance = new AppDataUtil();
    public long lastDialogShowTime = 0L;

    private AppDataUtil() {
    }

    public static AppDataUtil getInstance() {
        return ourInstance;
    }
}
