package com.app.Smarttys.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.UltimateSettingAdapter;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.app.widget.CircleImageView;
import com.app.Smarttys.core.ActivityLauncher;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.model.SmarttySettingsModel;

import java.util.ArrayList;

public class SmarttySettings extends CoreActivity implements View.OnClickListener {

    ArrayList<SmarttySettingsModel> dataList = new ArrayList<>();
    /* Display the settings page - options to do stuff like help, check profile etc */
    private ListView lv_Settings;
    private AvnNextLTProRegTextView Username, Status;
    private ImageView profileImage, backnavigate;
    private RelativeLayout headerlayout;
    /*private String[] values = { "Help", "Profile", "Account", "Chat setting",
        "Notification", "Contacts" };*/
    private String[] values = {"Account", /*"Chats", */"Notifications", "Email Settings"
            , "Contacts", "About & Help"};
    private int[] imagesIcon = {R.drawable.ic_account,
            /*R.drawable.ic_chat,*/ R.drawable.ic_notify, R.drawable.ic_mail, R.drawable.ic_contact,
            R.drawable.ic_help};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        setTitle("Settings");

        // Setting the menu String value to get them from the resource/strings
        values[0] = getString(R.string.Account);
        values[1] = getString(R.string.Notifications);
        values[2] = getString(R.string.email_settings);
        values[3] = getString(R.string.Contacts);
        values[4] = getString(R.string.about_and_help);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        backnavigate = findViewById(R.id.backnavigate);

        backnavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        for (int i = 0; i < values.length; i++) {
            boolean canAdd = false;
            if (i != 3) {
                canAdd = true;
            } else if (SessionManager.getInstance(this).getLockChatEnabled().equals("1")) {
                canAdd = true;
            }

            if (canAdd) {
                SmarttySettingsModel model = new SmarttySettingsModel();
                model.setTitle(values[i]);
                model.setResourceId(imagesIcon[i]);
                dataList.add(model);
            }
        }

        lv_Settings = findViewById(R.id.listViewSettings);
        LayoutInflater infalter = getLayoutInflater();
        ViewGroup header = (ViewGroup) infalter.inflate(R.layout.listviewheader, lv_Settings, false);
        lv_Settings.addHeaderView(header);
        lv_Settings.setAdapter(new UltimateSettingAdapter(SmarttySettings.this, dataList));


        Username = header.findViewById(R.id.username);
        Status = header.findViewById(R.id.userstatus);

        if (!SessionManager.getInstance(SmarttySettings.this).getcurrentUserstatus().isEmpty()) {
            Status.setText(SessionManager.getInstance(SmarttySettings.this).getcurrentUserstatus());
        }
        Username.setText(SessionManager.getInstance(SmarttySettings.this).getnameOfCurrentUser());
        String uname = Username.getText().toString();
        /*if (!uname.equals("")) {
            uname = uname.toLowerCase();
            uname = uname.substring(0, 1).toUpperCase() + uname.substring(1).toLowerCase();
        }*/
        Username.setText(uname);
        profileImage = (CircleImageView) header.findViewById(R.id.userprofile);
        headerlayout = header.findViewById(R.id.header);
        headerlayout.setOnClickListener(SmarttySettings.this);
        String pic = SessionManager.getInstance(SmarttySettings.this).getUserProfilePic();
        if (pic != null && !pic.isEmpty()) {

            /*Glide.with(Settings.this).load(pic).asBitmap()

                    .fitCenter().placeholder(R.mipmap.chat_attachment_profile_default_image_frame).
                    into(new BitmapImageViewTarget(profileImage) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            profileImage.setImageDrawable(circularBitmapDrawable);
                        }
                    });*/
/*            Picasso.with(this).load(AppUtils.getValidProfilePath(pic)).error(
                    R.drawable.ic_account_circle_white_64dp).into(profileImage);*/
            AppUtils.loadImage(SmarttySettings.this, AppUtils.getValidProfilePath(pic), profileImage, 150, R.drawable.ic_account_circle_white_64dp);
        }

        lv_Settings.setOnItemClickListener(new OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                if (!SessionManager.getInstance(SmarttySettings.this).getLockChatEnabled().equals("1")
                        && position > 3) {
                    position++;
                }


                switch (position) {
                    case 1:
                        ActivityLauncher.launchAccount(SmarttySettings.this);
                        break;
                    case 2:
                        //    ActivityLauncher.launchChatSetting(SmarttySettings.this);
                        ActivityLauncher.launchNotification(SmarttySettings.this);

                        break;
                    case 3:
                        Intent intent = new Intent(SmarttySettings.this, EmailSettings.class);
                        startActivity(intent);
//                        Intent intent = new Intent(SmarttySettings.this, EmailSettings.class);

                        break;

                    case 4:
  /*                      Intent intent = new Intent(SmarttySettings.this, EmailSettings.class);
                        startActivity(intent);
  */
                        ActivityLauncher.launchContactSettings(SmarttySettings.this);

                        break;

                    case 5:
//                        ActivityLauncher.launchContactSettings(SmarttySettings.this);
                        ActivityLauncher.launchAbouthelp(SmarttySettings.this);

                        break;
                    case 6:
//                        ActivityLauncher.launchAbouthelp(SmarttySettings.this);
                        break;
                }

            }

        });
        //  lv_Settings.getAdapter().getItem(2);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.header:
                ActivityLauncher.launchUserProfile(SmarttySettings.this);

                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Status.setText(SessionManager.getInstance(SmarttySettings.this).getcurrentUserstatus());
        Username.setText(SessionManager.getInstance(SmarttySettings.this).getnameOfCurrentUser());
        /*String uname = Username.getText().toString();
        if (!uname.equals("")) {
            uname = uname.toLowerCase();
            uname = uname.substring(0, 1).toUpperCase() + uname.substring(1).toLowerCase();
        }
        Username.setText(uname);*/

    }

}
