package com.app.Smarttys.app.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.Smarttys.R;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.SessionManager;

/**
 * Created by CAS63 on 11/22/2016.
 */
public class ChangeNumber_sub extends CoreActivity {
    ImageView backimg;
    EditText phoneNumber, phoneNumber2;
    TextView changenumber_actionbar_1, next_changenumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_number_sub);
        backimg = findViewById(R.id.backarrow_changenumber);
        getSupportActionBar().hide();

        changenumber_actionbar_1 = findViewById(R.id.changenumber_actionbar_1);
        next_changenumber = findViewById(R.id.next_changenumber);
        phoneNumber = findViewById(R.id.phoneNumber);
        phoneNumber2 = findViewById(R.id.phoneNumber2);

        phoneNumber.setText(SessionManager.getInstance(ChangeNumber_sub.this).getPhoneNumberOfCurrentUser());
        backimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
