package com.app.Smarttys.app.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;


/**
 * Created by CAS63 on 2/6/2017.
 */
public class VHStarredMessageSent extends RecyclerView.ViewHolder {


    public TextView fromname, toname, datelbl;

    public TextView senderName, message, time, timebelow, tvDateLbl, replaymessage, lblMsgFrom2, replaymessagemedio;

    public ImageView userprofile, singleTick, singleTickbelow, doubleTickGreenbelow, doubleTickBluebelow, doubleTickGreen, doubleTickBlue, clockbelow, clock, starredbelow, starred, cameraphoto, sentimage, audioimage, pdfimage, personimage;

    public RelativeLayout mainSent, relative_layout_message, replaylayout;

    public VHStarredMessageSent(View view) {
        super(view);
        userprofile = view.findViewById(R.id.userprofile);
        fromname = view.findViewById(R.id.fromname);
        toname = view.findViewById(R.id.toname);
        senderName = view.findViewById(R.id.lblMsgFrom);
        lblMsgFrom2 = view.findViewById(R.id.lblMsgFrom2);
        personimage = view.findViewById(R.id.personimage);
        pdfimage = view.findViewById(R.id.pdfimage);
        message = view.findViewById(R.id.txtMsg);

        time = view.findViewById(R.id.ts);
        timebelow = view.findViewById(R.id.ts_below);
        cameraphoto = view.findViewById(R.id.cameraphoto);
        sentimage = view.findViewById(R.id.sentimage);
        tvDateLbl = view.findViewById(R.id.tvDateLbl);
        audioimage = view.findViewById(R.id.audioimage);
        singleTick = view.findViewById(R.id.single_tick_green);
        singleTickbelow = view.findViewById(R.id.single_tick_green_below_below);
        doubleTickGreen = view.findViewById(R.id.double_tick_green);
        doubleTickGreenbelow = view.findViewById(R.id.double_tick_green_below);
        doubleTickBlue = view.findViewById(R.id.double_tick_blue);
        doubleTickBluebelow = view.findViewById(R.id.double_tick_blue_below);
        clock = view.findViewById(R.id.clock);
        clockbelow = view.findViewById(R.id.clock_below);
        mainSent = view.findViewById(R.id.mainSent);
        replaylayout = view.findViewById(R.id.replaylayout);
        starred = view.findViewById(R.id.starredindicator);
        starredbelow = view.findViewById(R.id.starredindicator_below);
        datelbl = view.findViewById(R.id.datelbl);
    }
}


