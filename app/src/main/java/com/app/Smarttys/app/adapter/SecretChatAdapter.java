package com.app.Smarttys.app.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.app.widget.CircleImageView;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.model.SmarttyContactModel;

import java.util.ArrayList;
import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;


/**
 *
 */
public class SecretChatAdapter extends RecyclerView.Adapter<SecretChatAdapter.ViewHolder> implements Filterable {
    private static final String TAG = "SecretChatAdapter";
    public List<SmarttyContactModel> mDisplayedValues;
    Session session;
    private Context context;
    private List<SmarttyContactModel> mOriginalValues;
    private UserInfoSession userInfoSession;
    private Getcontactname getcontactname;

    public SecretChatAdapter(Context context, List<SmarttyContactModel> data) {
        this.context = context;
        this.mDisplayedValues = data;
        this.mOriginalValues = data;
        session = new Session(context);
        userInfoSession = new UserInfoSession(context);
        getcontactname = new Getcontactname(context);
    }

    public SmarttyContactModel getItem(int position) {
        return mDisplayedValues.get(position);
    }

    public void updateInfo(List<SmarttyContactModel> aitem) {
        this.mDisplayedValues = aitem;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.secret_chat_contact_list_items, parent, false);

        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        SmarttyContactModel contact = mDisplayedValues.get(position);
        viewHolder.tvStatus.setTextSize(11);
        viewHolder.tvName.setText(contact.getFirstName());
        viewHolder.pos = position;

        try {
            String userId = contact.get_id();
            getcontactname.configProfilepic(viewHolder.ivUser, userId, true, true, R.mipmap.chat_attachment_profile_default_image_frame);

            if (!contact.getStatus().contentEquals("")) {
                getcontactname.setProfileStatusText(viewHolder.tvStatus, userId, contact.getStatus(), true);
            } else {
                viewHolder.tvStatus.setText(context.getResources().getString(R.string.status_not_available));
            }
        } catch (Exception e) {
            viewHolder.tvStatus.setText(context.getResources().getString(R.string.status_not_available));
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mDisplayedValues.size();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<SmarttyContactModel>) results.values; // has the filtered values

                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<SmarttyContactModel> FilteredArrList = new ArrayList<>();
                try {
                    if (mOriginalValues == null) {
                        mOriginalValues = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
                    }

                    if (constraint == null || constraint.length() == 0) {

                        // set the Original result to return
                        results.count = mOriginalValues.size();
                        results.values = mOriginalValues;
                    } else {
                        constraint = constraint.toString().toLowerCase();
                        for (int i = 0; i < mOriginalValues.size(); i++) {


                            String contactName = mOriginalValues.get(i).getFirstName();
                            String contactNo = mOriginalValues.get(i).getNumberInDevice();

                            if (contactName.toLowerCase().contains(constraint) || contactNo.toLowerCase().contains(constraint)) {
                                SmarttyContactModel mycontact = new SmarttyContactModel();
                                mycontact.setFirstName(mOriginalValues.get(i).getFirstName());

                                mycontact.set_id(mOriginalValues.get(i).get_id());
                                mycontact.setStatus(mOriginalValues.get(i).getStatus());
                                mycontact.setAvatarImageUrl(mOriginalValues.get(i).getAvatarImageUrl());
                                mycontact.setNumberInDevice(mOriginalValues.get(i).getNumberInDevice());

                                FilteredArrList.add(mycontact);
                            }


                        }
                        // set the Filtered result to return
                        results.count = FilteredArrList.size();
                        results.values = FilteredArrList;
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "performFiltering: ", e);
                }
                return results;
            }
        };
        return filter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public AvnNextLTProDemiTextView tvName;
        public AvnNextLTProRegTextView mobileText;
        public int pos;
        protected EmojiconTextView tvStatus;
        protected CircleImageView ivUser;
        Typeface face2 = CoreController.getInstance().getAvnNextLTProRegularTypeface();

        public ViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.userName_contacts);
            tvStatus = itemView.findViewById(R.id.status_contacts);
            ivUser = itemView.findViewById(R.id.userPhoto_contacts);
            mobileText = itemView.findViewById(R.id.mobileText);
            tvStatus.setTypeface(face2);
            tvStatus.setTextSize(11);
        }
        //protected Transformation transformation;
    }


}


