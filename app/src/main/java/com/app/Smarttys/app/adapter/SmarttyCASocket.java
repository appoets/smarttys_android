package com.app.Smarttys.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.widget.CircleImageView;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;

import java.util.ArrayList;
import java.util.List;


/**
 *
 */
public class SmarttyCASocket extends RecyclerView.Adapter<SmarttyCASocket.MyViewHolder> implements Filterable, FastScrollRecyclerView.SectionedAdapter {
    private static final String TAG = SmarttyCASocket.class.getSimpleName() + ">>";
    public List<SmarttyContactModel> mDisplayedValues;
    Session session;
    private Context context;
    private List<SmarttyContactModel> mOriginalValues;
    private Getcontactname getcontactname;
    private ChatListItemClickListener listener;

    public SmarttyCASocket(Context context, List<SmarttyContactModel> data) {
        MyLog.d(TAG, "SmarttyCASocket: ");
        this.context = context;
        this.mDisplayedValues = data;
        this.mOriginalValues = data;
        session = new Session(context);
        getcontactname = new Getcontactname(context);

    }

    public SmarttyContactModel getItem(int position) {
        return mDisplayedValues.get(position);
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        SmarttyContactModel contact = mDisplayedValues.get(position);
        return contact.getFirstName().substring(0, 1);
    }

    public void updateInfo(List<SmarttyContactModel> aitem) {
        this.mDisplayedValues = aitem;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyLog.d(TAG, "onCreateViewHolder: ");
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contacts_list_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder viewHolder, final int position) {
        MyLog.d(TAG, "onBindViewHolder: " + position);
        SmarttyContactModel contact = mDisplayedValues.get(position);
        viewHolder.tvStatus.setTextSize(13);
        viewHolder.mobileText.setText(contact.getType());
        viewHolder.tvName.setText(contact.getFirstName());
        viewHolder.pos = position;

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onItemClick(viewHolder.itemView, position);
                }
            }
        });

        try {
            String userId = contact.get_id();
            if (!contact.getStatus().contentEquals("")) {
                getcontactname.setProfileStatusText(viewHolder.tvStatus, userId, contact.getStatus(), false);
            } /*else {
                viewHolder.tvStatus.setText(context.getResources().getString(R.string.status_not_available));
            }*/
        } catch (Exception e) {
            viewHolder.tvStatus.setText(context.getResources().getString(R.string.status_available));
        }

        try {
            final String to = contact.get_id();
            //System.out.println("MissedAvatar"+""+contact.getAvatarImageUrl());
            //viewHolder.ivUser.setImageResource(0);


            viewHolder.ivUser.post(new Runnable() {
                @Override
                public void run() {
                    getcontactname.configProfilepic(viewHolder.ivUser, to, false, true, R.mipmap.chat_attachment_profile_default_image_frame);

                }
            });
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mDisplayedValues.size();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<SmarttyContactModel>) results.values; // has the filtered values
                if (mDisplayedValues.size() == 0) {
//                    Toast.makeText(context, "No Contacts Matching Your Query...", Toast.LENGTH_SHORT).show();
                }

                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<SmarttyContactModel> FilteredArrList = new ArrayList<>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {


                        String contactName = mOriginalValues.get(i).getFirstName();
                        String contactNo = mOriginalValues.get(i).getNumberInDevice();

                        if (contactName.toLowerCase().contains(constraint) || contactNo.toLowerCase().contains(constraint)) {
                            //createnewgroupSmarttySocketModel(FilteredArrList);
                            SmarttyContactModel mycontact = new SmarttyContactModel();
                            mycontact.setFirstName(mOriginalValues.get(i).getFirstName());

                            mycontact.set_id(mOriginalValues.get(i).get_id());
                            mycontact.setStatus(mOriginalValues.get(i).getStatus());
                            mycontact.setAvatarImageUrl(mOriginalValues.get(i).getAvatarImageUrl());
                            mycontact.setNumberInDevice(mOriginalValues.get(i).getNumberInDevice());
                            Log.e("SmarttyCASocket", "setNumberInDevice" + mOriginalValues.get(i).getNumberInDevice());

                            FilteredArrList.add(mycontact);
                        }


                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public void setChatListItemClickListener(ChatListItemClickListener listener) {
        this.listener = listener;
    }


    public interface ChatListItemClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView tick;
        public int pos;
        Typeface face2 = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        TextView tvName, tvStatus, mobileText;
        CircleImageView ivUser;

        public MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.userName_contacts);
            tvStatus = view.findViewById(R.id.status_contacts);
            tick = view.findViewById(R.id.tick);

            ivUser = view.findViewById(R.id.userPhoto_contacts);
            mobileText = view.findViewById(R.id.mobileText);
            tvName.setTextColor(Color.parseColor("#3f3f3f"));

            tvStatus.setTextColor(Color.parseColor("#808080"));
            tvStatus.setTypeface(face2);
            tvStatus.setTextSize(13);
        }
    }

}


