package com.app.Smarttys.app.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.core.ActivityLauncher;
import com.app.Smarttys.core.CoreActivity;

/**
 * Created by CAS63 on 11/18/2016.
 */
public class ChangeNumber extends CoreActivity {
    ImageView backimg;
    TextView text1, text2, text3;
    AvnNextLTProDemiTextView changenumber_actionbar_1, next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_number);

        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        backimg = findViewById(R.id.backarrow_changenumber);
        text1 = findViewById(R.id.changenumber_1sttext);
        text2 = findViewById(R.id.changenumber_2ndtext);
        text3 = findViewById(R.id.changenumber_3rdtext);
        getSupportActionBar().hide();

        backimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        next = findViewById(R.id.next_changenumber);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityLauncher.launchChangenumber2(ChangeNumber.this);
            }
        });

        changenumber_actionbar_1 = findViewById(R.id.changenumber_actionbar_1);

    }
}
