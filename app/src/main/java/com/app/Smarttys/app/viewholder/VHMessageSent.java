package com.app.Smarttys.app.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;


public class VHMessageSent extends RecyclerView.ViewHolder {


    public TextView time, ts_new, timebelow, tvDateLbl, tvSecretLbl, replaymessage, lblMsgFrom2, replaymessagemedio, ts_reply, ts_reply_above;
    public TextView message;
    public ImageView clock_new, clockbelow, clock, starredbelow, starred, star_new, cameraphoto, sentimage,
            starredindicator_reply, imageViewindicatior, clock_reply,
            starredindicator_reply_above, clock_reply_above;

    public ImageView
            doubleTickBlue, single_tick_green_below_reply, single_tick_green_above_reply;

    public ImageView ivSingleLineTick, ivTickBelow;
    public RelativeLayout replaylayout, layout_reply_tsalign, layout_reply_tsalign_above,
            message_sent_singleChar_layout, below_layout;

    public View selection_layout, relative_layout_message;

    public VHMessageSent(View view) {
        super(view);
        lblMsgFrom2 = view.findViewById(R.id.lblMsgFrom2);
        message = view.findViewById(R.id.txtMsg);
        replaymessage = view.findViewById(R.id.replaymessage);
        replaymessagemedio = view.findViewById(R.id.replaymessagemedio);
        time = view.findViewById(R.id.ts);
        ts_new = view.findViewById(R.id.ts_new);
        ts_reply = view.findViewById(R.id.ts_reply);
        ts_reply_above = view.findViewById(R.id.ts_reply_above);
        timebelow = view.findViewById(R.id.ts_below);
        cameraphoto = view.findViewById(R.id.cameraphoto);
        sentimage = view.findViewById(R.id.sentimage);
        tvDateLbl = view.findViewById(R.id.tvDateLbl);
        tvSecretLbl = view.findViewById(R.id.tvSecretLbl);
        ivSingleLineTick = view.findViewById(R.id.iv_singleline_tick);
        ivTickBelow = view.findViewById(R.id.tick_below);
        single_tick_green_below_reply = view.findViewById(R.id.single_tick_green_below_reply);
        single_tick_green_above_reply = view.findViewById(R.id.single_tick_green_above_reply);


        doubleTickBlue = view.findViewById(R.id.double_tick_blue);

        clock = view.findViewById(R.id.clock);
        clock_reply = view.findViewById(R.id.clock_reply);
        clock_reply_above = view.findViewById(R.id.clock_reply_above);
        clockbelow = view.findViewById(R.id.clock_below);
        clock_new = view.findViewById(R.id.clock_new);
        replaylayout = view.findViewById(R.id.replaylayout);
        layout_reply_tsalign = view.findViewById(R.id.layout_reply_tsalign);
        starred = view.findViewById(R.id.starredindicator);
        star_new = view.findViewById(R.id.star_new);
        starredbelow = view.findViewById(R.id.starredindicator_below);
        starredindicator_reply_above = view.findViewById(R.id.starredindicator_reply_above);
        starredindicator_reply = view.findViewById(R.id.starredindicator_reply);
        imageViewindicatior = view.findViewById(R.id.imageView);
        relative_layout_message = view.findViewById(R.id.relative_layout_message);
        layout_reply_tsalign_above = view.findViewById(R.id.layout_reply_tsalign_above);
        selection_layout = view.findViewById(R.id.selection_layout);
        message_sent_singleChar_layout = view.findViewById(R.id.message_sent_singleChar_layout);
        below_layout = view.findViewById(R.id.below_layout);
//        ivLocation = (ImageView) view.findViewById(R.id.ivLocation);

    }

}
