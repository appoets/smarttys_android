package com.app.Smarttys.app.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.CropingOptionAdapter;
import com.app.Smarttys.app.dialog.CustomMultiTextItemsDialog;
import com.app.Smarttys.app.utils.GroupInfoSession;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.app.widget.CircleImageView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.message.PictureMessage;
import com.app.Smarttys.core.model.CropingOption;
import com.app.Smarttys.core.model.GroupInfoPojo;
import com.app.Smarttys.core.model.MultiTextDialogPojo;
import com.app.Smarttys.core.model.PictureModel;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.smarttyhelperclass.SmarttyImageUtils;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;
import com.soundcloud.android.crop.Crop;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import id.zelory.compressor.Compressor;


/**
 *
 */
public class GroupCreation extends CoreActivity {
    private static final String TAG = "GroupCreation";
    private static final int GALLERY_REQUEST_CODE = 1;
    private static final int CAMERA_REQUEST_CODE = 2;
    ArrayList<String> myList = new ArrayList<>();
    EmojiconEditText Mygroupname;
    ImageView happyFace;
    String mCurrentUserId;
    AvnNextLTProRegTextView label;
    AvnNextLTProRegTextView count;
    AvnNextLTProDemiTextView actionbarlabel;
    Uri cameraImageUri;
    EmojIconActions emojIcon;
    ImageView backarrow_grouphead;
    TextWatcher watch = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence cs, int a, int b, int c) {
            // TODO Auto-generated method stub
            int countgroupname = 25 - cs.length();
            count.setText(String.valueOf(countgroupname));


        }
    };
    private ImageView creategroup;
    private String currentDateTimeString;
    private CircleImageView myProfilePic;
    private ProgressDialog progress;
    private Uri mImageCaptureUri;
    private Boolean imageChanged = false;
    private File outPutFile = null;
    private PictureModel pictureModel = null;
    private String tag_string_req = "string_req";
    private String pictureurl = null;
    private Activity mActivity;
    private GroupInfoSession groupInfoSession;
    private boolean isFirstEmojiClick = true;
    private String selectedGroupImagePath;
    private boolean mclick;

    public static Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap circuleBitmap = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(circuleBitmap);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return circuleBitmap;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.groupcreation);

        if (savedInstanceState != null) {
            cameraImageUri = Uri.parse(savedInstanceState.getString("ImageUri"));
        } else {
            cameraImageUri = Uri.parse("");
        }

        mActivity = GroupCreation.this;
        initProgress("Uploading Image....", false);

        label = findViewById(R.id.normaltext);
        actionbarlabel = findViewById(R.id.actionbar);
        getSupportActionBar().hide();
        count = findViewById(R.id.count);
        myList = (ArrayList<String>) getIntent().getSerializableExtra("mylist");
        MyLog.d("send array", "" + myList);

        initialise();
        actionbarlabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Mygroupname.addTextChangedListener(watch);
    }

    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GROUP)) {

            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String groupAction = object.getString("groupType");

                if (groupAction.equalsIgnoreCase(SocketManager.ACTION_NEW_GROUP)) {

                    String from = object.getString("from");
                    String currentUserId = SessionManager.getInstance(GroupCreation.this).getCurrentUserID();

                    if (from.equalsIgnoreCase(currentUserId)) {
                        String respMsg = object.getString("message");
                        String groupId = object.getString("groupId");
                        String members = object.getString("groupMembers");
                        String createdBy = object.getString("createdBy");
                        String profilePic = object.getString("profilePic");
                        String groupName = object.getString("groupName");
                        String ts = object.getString("timeStamp");
                        String admin = object.getString("admin");
                        String id = object.getString("id");

                        String docId = currentUserId.concat("-").concat(groupId).concat("-g");

                        GroupInfoSession groupInfoSession = new GroupInfoSession(GroupCreation.this);

                        GroupInfoPojo infoPojo = new GroupInfoPojo();
                        infoPojo.setGroupId(groupId);
                        infoPojo.setCreatedBy(createdBy);
                        infoPojo.setAvatarPath(profilePic);
                        infoPojo.setGroupName(groupName);
                        infoPojo.setGroupMembers(members);
                        infoPojo.setAdminMembers(admin);
                        infoPojo.setLiveGroup(true);
                        groupInfoSession.updateGroupInfo(docId, infoPojo);

                    }
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }


            if (progress != null)
                progress.dismiss();

            Intent intent = new Intent(GroupCreation.this, NewHomeScreenActivty.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_IMAGE_UPLOAD)) {
            Object[] array = event.getObjectsArray();
            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        hideProgressDialog();

                    }
                });
                JSONObject objects = new JSONObject(array[0].toString());
                String err = objects.getString("err");
                String message = objects.getString("message");

                if (err.equalsIgnoreCase("0")) {
                    String from = objects.getString("from");
                    String type = objects.getString("type");

                    if (from.equalsIgnoreCase(SessionManager.getInstance(GroupCreation.this).getCurrentUserID())
                            && type.equalsIgnoreCase("group")) {
                        pictureurl = objects.getString("file");
                        Log.e("EVENT_IMAGE_UPLOAD", "EVENT_IMAGE_UPLOAD" + "updateData");
                        Log.e("updateData", "imageChanged" + imageChanged);

                        //Issue calling multiple  times group creating double times
                        if (imageChanged) {
                            imageChanged = false;
                            updateData();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    hideProgressDialog();

                                }
                            });
                        }/* else {
                            updateData();
                            hideProgressDialog();
                        }*/


                    }

                }

            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
        }
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void initialise() {

        creategroup = findViewById(R.id.creategroup);
        myProfilePic = findViewById(R.id.group_icon);
        backarrow_grouphead = findViewById(R.id.backarrow_grouphead);
        outPutFile = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
        happyFace = findViewById(R.id.grouphappyFace);

        backarrow_grouphead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        myProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<MultiTextDialogPojo> labelsList = new ArrayList<>();
                MultiTextDialogPojo label = new MultiTextDialogPojo();
                label.setImageResource(R.drawable.blue_camera);
                label.setLabelText("Take Image From Camera");
                labelsList.add(label);

                label = new MultiTextDialogPojo();
                label.setImageResource(R.drawable.gallery);
                label.setLabelText("Add Image From Gallery");
                labelsList.add(label);

                CustomMultiTextItemsDialog dialog = new CustomMultiTextItemsDialog();
                dialog.setTitleText("Profile Picture");
                dialog.setNegativeButtonText("Cancel");
                dialog.setLabelsList(labelsList);

                dialog.setDialogItemClickListener(new CustomMultiTextItemsDialog.DialogItemClickListener() {
                    @Override
                    public void onDialogItemClick(int position) {
                        switch (position) {
                            case 0:
                                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                                    StrictMode.setVmPolicy(builder.build());
                                }
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                File cameraImageOutputFile = new File(
                                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                                        createCameraImageFileName());
                                cameraImageUri = Uri.fromFile(cameraImageOutputFile);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri);
                                intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                                startActivityForResult(intent, CAMERA_REQUEST_CODE);
                                break;
                            case 1:
                                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                                photoPickerIntent.setType("image/*");
                                startActivityForResult(photoPickerIntent, GALLERY_REQUEST_CODE);
                                break;
                        }
                    }
                });
                dialog.show(getSupportFragmentManager(), "Profile Pic");
            }
        });
        Mygroupname = findViewById(R.id.groupname);
        Typeface typeface = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        Mygroupname.setTypeface(typeface);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(Mygroupname, InputMethodManager.SHOW_IMPLICIT);

        final View rootView = findViewById(R.id.mainlayoutofgroup);
        emojIcon = new EmojIconActions(this, rootView, Mygroupname, happyFace);


        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);
        emojIcon.ShowEmojIcon();

        happyFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isFirstEmojiClick) {
                    emojIcon.ShowEmojIcon();
                    happyFace.performClick();
                    isFirstEmojiClick = false;
                } else {
                    emojIcon.ShowEmojIcon();
                }
            }
        });


        Mygroupname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  popup.dismiss();
                final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(Mygroupname, InputMethodManager.SHOW_IMPLICIT);

            }
        });

        creategroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (internetcheck()) {

                    if (!Mygroupname.getText().toString().trim().isEmpty()) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            // Call some material design APIs here
                            Toast.makeText(mActivity, "Creating group", Toast.LENGTH_SHORT).show();
                        } else {
                            // Implement this feature without material design
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    initProgress("Creating group", false);

                                    showProgressDialog(mActivity);

                                }
                            });
                        }

                        Log.e("imageChanged", "imageChanged" + imageChanged);
                        if (imageChanged) {

                            //uploadImage(profileImg);

                            uploadImageNew();

                        } else {
                            Log.e("creategroup", "uploadImageNew imageChanged" + imageChanged + "updateData");
                            updateData();
                        }
                    } else {
                        Toast.makeText(GroupCreation.this, "Please provide group name", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(GroupCreation.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }

        });
    }

    private Boolean internetcheck() {
        ConnectivityManager cm =
                (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void sendGroupCreateMessage() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df3 = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
        currentDateTimeString = df3.format(c.getTime());


        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
        Date date = null;
        try {
            date = df.parse(currentDateTimeString);
        } catch (ParseException e) {
            MyLog.e(TAG, "", e);
        }
        final long epoch = date.getTime();
        myList.add(SessionManager.getInstance(GroupCreation.this).getCurrentUserID());
        JSONArray jsArray = new JSONArray(myList);
        SendMessageEvent messageEvent = new SendMessageEvent();
        JSONObject obj = new JSONObject();
        try {
            obj.put("id", Calendar.getInstance().getTimeInMillis());
            obj.put("from", SessionManager.getInstance(GroupCreation.this).getCurrentUserID());
            obj.put("groupId", "" + epoch);
            obj.put("groupMembers", jsArray);
            obj.put("groupName", Mygroupname.getText().toString());
            obj.put("profilePic", pictureurl);
            obj.put("groupType", SocketManager.ACTION_NEW_GROUP);

            MyLog.d("group creation", obj.toString());

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        messageEvent.setEventName(SocketManager.EVENT_GROUP);
        messageEvent.setMessageObject(obj);
        EventBus.getDefault().post(messageEvent);

    }

    private String createCameraImageFileName() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return timeStamp + ".jpg";
    }

    private void uploadImage(Bitmap circleBmp) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                showProgressDialog(mActivity);

            }
        });
        //    showProgressDialog(mActivity);

        if (circleBmp != null) {
            try {
                File imgDir = new File(MessageFactory.PROFILE_IMAGE_PATH);
                if (!imgDir.exists()) {
                    imgDir.mkdirs();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        showProgressDialog(mActivity);

                    }
                });
                String profileImgPath = imgDir + "/" + Calendar.getInstance().getTimeInMillis() + "_pro.jpg";

                File file = new File(profileImgPath);
                if (file.exists()) {
                    file.delete();
                }
                file.createNewFile();

                OutputStream outStream = new FileOutputStream(file);
                circleBmp.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.flush();
                outStream.close();

                String serverFileName = SessionManager.getInstance(GroupCreation.this).getCurrentUserID()
                        + "-g-" + Calendar.getInstance().getTimeInMillis() + ".jpg";

                PictureMessage message = new PictureMessage(GroupCreation.this);
                JSONObject object = (JSONObject) message.createGroupProfileImageObject(serverFileName, profileImgPath);
                FileUploadDownloadManager fileUploadDownloadMgnr = new FileUploadDownloadManager(GroupCreation.this);
                Log.d(TAG, "onClick: startFileUpload7");
                fileUploadDownloadMgnr.startFileUpload(EventBus.getDefault(), object);
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
        }
    }

    private void uploadImageNew() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                showProgressDialog(mActivity);

            }
        });

        if (selectedGroupImagePath != null) {
            try {


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initProgress("Creating group", false);

                        showProgressDialog(mActivity);

                    }
                });

                File file = new File(selectedGroupImagePath);
                Bitmap compressedBitmap = Compressor.getDefault(this).compressToBitmap(file);

                uploadImage(compressedBitmap);

              /*  String serverFileName = SessionManager.getInstance(GroupCreation.this).getCurrentUserID()
                        + "-g-" + Calendar.getInstance().getTimeInMillis() + ".jpg";

                PictureMessage message = new PictureMessage(GroupCreation.this);
                JSONObject object = (JSONObject) message.createGroupProfileImageObject(serverFileName, uri.getPath());
                FileUploadDownloadManager fileUploadDownloadMgnr = new FileUploadDownloadManager(GroupCreation.this);
                fileUploadDownloadMgnr.startFileUpload(EventBus.getDefault(), object);*/
            } catch (Exception e) {
                MyLog.e(TAG, "" + e.getMessage(), e);

                MyLog.e(TAG, "", e);
                Bitmap profileImg = ((BitmapDrawable) myProfilePic.getDrawable()).getBitmap();
                uploadImage(profileImg);

            }
        } else {
            Log.e("uploadImageNew", "uploadImageNew" + "updateData");
            updateData();
        }
    }

    private void updateData() {
//Check it is trigging two times and elimate it

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df3 = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
        currentDateTimeString = df3.format(c.getTime());


        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
        Date date = null;
        try {
            date = df.parse(currentDateTimeString);
        } catch (ParseException e) {
            MyLog.e(TAG, "", e);
        }
        final long epoch = date.getTime();
        myList.add(SessionManager.getInstance(GroupCreation.this).getCurrentUserID());
        JSONArray jsArray = new JSONArray(myList);

        if (pictureurl == null) {

            pictureurl = "";
        }

        JSONObject obj = new JSONObject();
        try {
            obj.put("from", SessionManager.getInstance(GroupCreation.this).getCurrentUserID());
            obj.put("groupId", "" + epoch);
            obj.put("groupMembers", jsArray);
            obj.put("groupName", Mygroupname.getText().toString());
            obj.put("profilePic", pictureurl);
            obj.put("groupType", SocketManager.ACTION_NEW_GROUP);
            MyLog.e("updateData", "ACTION_NEW_GROUP" + obj);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_GROUP);
        messageEvent.setMessageObject(obj);
        EventBus.getDefault().post(messageEvent);
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("ImageUri", cameraImageUri.toString());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        MyLog.d("Casperon result code", "" + requestCode + "" + data);

        if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    Uri selectedImageUri = data.getData();
                    beginCrop(selectedImageUri);
                }
            } else {
                if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    Toast.makeText(this, "Sorry! Failed to capture image",
                            Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                beginCrop(cameraImageUri);

            } else {
                if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    Toast.makeText(this, "Sorry! Failed to capture image",
                            Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            Uri uri = Crop.getOutput(data);
            String filePath = uri.getPath();

            try {

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
                Bitmap alignedBitmap = SmarttyImageUtils.getAlignedBitmap(bitmap, filePath);
                myProfilePic.setImageBitmap(alignedBitmap);
                selectedGroupImagePath = filePath;
                imageChanged = true;
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            } catch (OutOfMemoryError e) {
                Toast.makeText(GroupCreation.this, "Out of memory!", Toast.LENGTH_LONG).show();
            }
        }


    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void CropingIMG() {

        final ArrayList<CropingOption> cropOptions = new ArrayList<CropingOption>();

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        List<ResolveInfo> list = getPackageManager().queryIntentActivities(
                intent, 0);
        int size = list.size();
        if (size == 0) {
            Toast.makeText(this, "Can't find image croping app",
                    Toast.LENGTH_SHORT).show();
            return;
        } else {
            intent.setData(mImageCaptureUri);
            intent.putExtra("outputX", 512);
            intent.putExtra("outputY", 512);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);

            // TODO: don't use return-data tag because it's not return large
            // image data and crash not given any message
            // intent.putExtra("return-data", true);

            // Create output file here
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outPutFile));

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName,
                        res.activityInfo.name));

                startActivityForResult(i, 3);
            } else {
                for (ResolveInfo res : list) {
                    final CropingOption co = new CropingOption();

                    co.title = getPackageManager().getApplicationLabel(
                            res.activityInfo.applicationInfo);
                    co.icon = getPackageManager().getApplicationIcon(
                            res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);
                    co.appIntent
                            .setComponent(new ComponentName(
                                    res.activityInfo.packageName,
                                    res.activityInfo.name));
                    cropOptions.add(co);
                }

                CropingOptionAdapter adapter = new CropingOptionAdapter(
                        getApplicationContext(), cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose Cropping App");
                builder.setCancelable(false);
                builder.setAdapter(adapter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                startActivityForResult(
                                        cropOptions.get(item).appIntent, 3);
                            }
                        });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        if (mImageCaptureUri != null) {
                            getContentResolver().delete(mImageCaptureUri, null,
                                    null);
                            mImageCaptureUri = null;
                        }
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }


  /*  private void profilepicupdation() {
        Log.d(TAG, "profilepicupdation:11");
        //  if (receiverAvatar != null) {

            groupInfoSession = new GroupInfoSession(this);
            mCurrentUserId = SessionManager.getInstance(this).getCurrentUserID();
            Log.d(TAG, "profilepicupdation: to " + ChatPageActivity.to);
            Log.d(TAG, "profilepicupdation: mCurrentUserId " + mCurrentUserId);
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(mCurrentUserId.concat("-").concat(ChatPageActivity.to).concat("-g"));
        ChatPageActivity.receiverAvatar = infoPojo.getAvatarPath();
        ChatPageActivity.receiverAvatar = AppUtils.getValidProfilePath(receiverAvatar);
            Log.d(TAG, "profilepicupdation: " + receiverAvatar);
            if (!AppUtils.isEmptyImage(ChatPageActivity.receiverAvatar)) {

                AppUtils.loadImage(this, receiverAvatar, user_profile_image, 100, R.drawable.avatar_group);

            } else {
                user_profile_image.setImageResource(R.drawable.avatar_group);
            }


    }*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideProgressDialog();
    }
}

