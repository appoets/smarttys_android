package com.app.Smarttys.app.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;

import com.app.Smarttys.R;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;

import org.greenrobot.eventbus.EventBus;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class AutoDownLoadUtils {
    private static final AutoDownLoadUtils ourInstance = new AutoDownLoadUtils();
    private static final String TAG = "AutoDownLoadUtils";

    private AutoDownLoadUtils() {
    }

    public static AutoDownLoadUtils getInstance() {
        return ourInstance;
    }

    public void checkAndAutoDownload(Context context, MessageItemChat item) {
        FileUploadDownloadManager uploadDownloadManager = new FileUploadDownloadManager(context);
        Session session = new Session(context);
        boolean isSecretMsg = item.isSecretChat();
        if (chkmobiledataon(context).equalsIgnoreCase("CONNECTED")) {
            String[] values = session.getmobiledataPrefsName().split(",");
            for (int i = 0; i < values.length; i++) {
                if (values[i].equalsIgnoreCase(context.getString(R.string.photo))) {
                    if (MessageFactory.picture == Integer.parseInt(item.getMessageType())) {
                        uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, isSecretMsg);
                    }
                }
                if (values[i].equalsIgnoreCase(context.getString(R.string.audio))) {
                    if (MessageFactory.audio == Integer.parseInt(item.getMessageType())) {
                        uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, isSecretMsg);
                    }
                }
                if (values[i].equalsIgnoreCase(context.getString(R.string.video))) {
                    if (MessageFactory.video == Integer.parseInt(item.getMessageType())) {
                        uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, isSecretMsg);
                    }
                }
                if (values[i].equalsIgnoreCase(context.getString(R.string.doc))) {
                    if (MessageFactory.document == Integer.parseInt(item.getMessageType())) {
                        uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, isSecretMsg);
                    }
                }
            }
        }
        Boolean dataroaming = isDataRoamingEnabled(context);
        if (dataroaming) {
            String[] values = session.getromingPrefsName().split(",");
            for (int i = 0; i < values.length; i++) {
                if (values[i].equalsIgnoreCase(context.getString(R.string.photo))) {
                    if (MessageFactory.picture == Integer.parseInt(item.getMessageType())) {
                        uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, isSecretMsg);
                    }
                }
                if (values[i].equalsIgnoreCase(context.getString(R.string.audio))) {
                    if (MessageFactory.audio == Integer.parseInt(item.getMessageType())) {
                        uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, isSecretMsg);
                    }
                }
                if (values[i].equalsIgnoreCase(context.getString(R.string.video))) {
                    if (MessageFactory.video == Integer.parseInt(item.getMessageType())) {
                        uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, isSecretMsg);
                    }
                }
                if (values[i].equalsIgnoreCase(context.getString(R.string.doc))) {
                    if (MessageFactory.document == Integer.parseInt(item.getMessageType())) {
                        uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, isSecretMsg);
                    }
                }
            }
        }
        if (isWifiEnabled(context)) {
            String[] values = session.getwifiPrefsName().split(",");
            for (int i = 0; i < values.length; i++) {
                if (values[i].equalsIgnoreCase(context.getString(R.string.photo))) {
                    if (MessageFactory.picture == Integer.parseInt(item.getMessageType())) {
                        uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, isSecretMsg);
                    }
                }
                if (values[i].equalsIgnoreCase(context.getString(R.string.audio))) {
                    if (MessageFactory.audio == Integer.parseInt(item.getMessageType())) {
                        uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, isSecretMsg);
                    }
                }
                if (values[i].equalsIgnoreCase(context.getString(R.string.video))) {
                    if (MessageFactory.video == Integer.parseInt(item.getMessageType())) {
                        uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, isSecretMsg);
                    }
                }
                if (values[i].equalsIgnoreCase(context.getString(R.string.doc))) {
                    if (MessageFactory.document == Integer.parseInt(item.getMessageType())) {
                        uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, isSecretMsg);
                    }
                }
            }
        }
    }

    public boolean isWifiEnabled(Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            return wifiManager.isWifiEnabled();
        } catch (Exception e) {
            return false;
        }
    }

    public String chkmobiledataon(Context context) {
        try {
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
            NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (mobileInfo != null)
                return mobileInfo.getState().toString();
        } catch (Exception e) {
            MyLog.e(TAG, "chkmobiledataon: ", e);
        }
        return "";
    }

    public Boolean isDataRoamingEnabled(Context context) {
        try {
            // return true or false if data roaming is enabled or not
            return Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.DATA_ROAMING) == 1;
        } catch (Settings.SettingNotFoundException e) {
            // return null if no such settings exist (DEVICE with no radio data ?)
            return false;
        }
    }

}
