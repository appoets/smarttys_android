package com.app.Smarttys.app.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;


/**
 *
 */
public class VHservermessage extends RecyclerView.ViewHolder {

    public TextView tvServerMsgLbl, tvSecretLbl, tvDateLbl;


    public VHservermessage(View view) {
        super(view);

        tvServerMsgLbl = view.findViewById(R.id.tvServerMsgLbl);
        tvSecretLbl = view.findViewById(R.id.tvSecretLbl);
        tvDateLbl = view.findViewById(R.id.tvDateLbl);

    }
}
