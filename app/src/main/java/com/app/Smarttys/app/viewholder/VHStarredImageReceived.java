package com.app.Smarttys.app.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;


/**
 * Created by CAS63 on 2/6/2017.
 */
public class VHStarredImageReceived extends RecyclerView.ViewHolder {
    public AvnNextLTProRegTextView captiontext;
    public TextView senderName, time, toname, fromname, datelbl, ts_abovecaption;
    public ImageView imageView, starredindicator_below, userprofile;
    public RelativeLayout caption, abovecaption_layout, rlTs;
    public ImageView starredindicator_above;

    public VHStarredImageReceived(View view) {
        super(view);
        senderName = view.findViewById(R.id.lblMsgFrom);
        imageView = view.findViewById(R.id.imgshow);
        userprofile = view.findViewById(R.id.userprofile);
        time = view.findViewById(R.id.ts);
        toname = view.findViewById(R.id.toname);
        fromname = view.findViewById(R.id.fromname);
        datelbl = view.findViewById(R.id.datelbl);
        starredindicator_below = view.findViewById(R.id.starredindicator_below);
        captiontext = view.findViewById(R.id.captiontext);
        caption = view.findViewById(R.id.caption);
        rlTs = view.findViewById(R.id.rlTs);

        starredindicator_above = view.findViewById(R.id.starredindicator_above);
        ts_abovecaption = view.findViewById(R.id.ts_abovecaption);
        abovecaption_layout = view.findViewById(R.id.abovecaption);

    }
}
