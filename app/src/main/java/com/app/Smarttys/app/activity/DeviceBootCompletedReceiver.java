package com.app.Smarttys.app.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.socket.MessageService;

/**
 * Created by CAS60 on 9/19/2017.
 */
public class DeviceBootCompletedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())
                && !MessageService.isStarted() && SessionManager.getInstance(context).getBackupRestored()) { // Only start service after login completed
            // Marshmallow+
           /* if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
                Intent restartServiceIntent = new Intent(context, MessageService.class);
                restartServiceIntent.setPackage(context.getPackageName());
                PendingIntent restartServicePendingIntent = PendingIntent.getService(context, 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
                AlarmManager alarmService = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                if (alarmService != null) {
                    alarmService.set(
                            AlarmManager.ELAPSED_REALTIME,
                            SystemClock.elapsedRealtime() + 500,
                            restartServicePendingIntent);
                }
            } else {
                //below Marshmallow{
                context.startService(new Intent(context, MessageService.class));
            }*/
        }
    }
}
