package com.app.Smarttys.app.calls;

import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.ConnectivityInfo;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.fcm.MyFirebaseMessagingService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import static android.Manifest.permission.RECORD_AUDIO;

/**
 * Created by CAS60 on 7/14/2017.
 */
public class IncomingCallActivity extends AppCompatActivity {
    public static final String EXTRA_DOC_ID = "DocId";
    public static final String EXTRA_FROM_USER_ID = "FromUserId";
    public static final String EXTRA_TO_USER_ID = "ToUserId";
    public static final String EXTRA_FROM_USER_MSISDN = "FromUserMsisdn";
    public static final String EXTRA_CALL_ROOM_ID = "RoomId";
    public static final String EXTRA_CALL_RECORD_ID = "recordId";
    public static final String EXTRA_CALL_TYPE = "CallType";
    public static final String EXTRA_CALL_TIME_STAMP = "CallTimeStamp";
    private static final String TAG = "IncomingCallActivity";
    public static boolean isStarted;
    private final int AUDIO_RECORD_PERMISSION_REQUEST_CODE = 1;
    ImageView category_single_view;
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    private int field = 0x00000020;
    //  private SensorManager mSensorManager;
    //   private Sensor mProximity;
    private boolean mAccept, mReject;
    private TextView tvName, tvCallLbl;
    private ImageButton ibAnswer, ibReject;
    private ImageView ivProfilePic;
    private String mCurrentUserId, mCallId, mRoomId, fromUserId, toUserId, fromUserMsisdn,
            profilePic, mCallTS, mReconnect;
    private boolean isVideoCall, resultAudioRecordPermission;
    private Ringtone ringtone;
    private Vibrator vibrator;
    private int screenWidth;
    private float rejectBtnStart, answerBtnEnd;
    private double maxWidthAnsBtn, minWidthRejectBtn, minEnableAns, minEnableReject;
    private Handler callTimeoutHandler, broadcastHandler;
    private Runnable callTimeoutRunnable, broadcastRunnable;
    //----------------------------------New Design------------------------
    private TextView call_status;
    private ImageView circle_profile_image;
    private TextView caller_name;
    private TextView caller_status;
    private RelativeLayout accept_layout;
    private RelativeLayout disconnect_layout;
    private String mRecordId = "";
    private Camera mCamera;
    private CameraPreview mCameraPreview;
    private int currentCameraId;
    private View.OnTouchListener answerButtonTouchListener = new View.OnTouchListener() {
        float mPrevX;

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            if (ConnectivityInfo.isInternetConnected(IncomingCallActivity.this)) {
                float currX;
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN: {
                        mPrevX = event.getX();
                        break;
                    }

                    case MotionEvent.ACTION_MOVE: {
                        if (ibReject.getVisibility() == View.VISIBLE) {
                            ibReject.setVisibility(View.INVISIBLE);
                        }

                        currX = event.getRawX();
                        int position = (int) (currX - mPrevX);

                        if (position > 0 && position < maxWidthAnsBtn && position < rejectBtnStart) {
                            MyLog.d("Incom_AnsDrag", position + "");
                            ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(view.getLayoutParams());
                            marginParams.setMargins(position, (int) view.getY(), 0, 0);
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(marginParams);
                            view.setLayoutParams(layoutParams);
                        }
                        break;
                    }

                    case MotionEvent.ACTION_CANCEL:
                        break;

                    case MotionEvent.ACTION_UP:
                        ibReject.setVisibility(View.VISIBLE);
                        float position = view.getX();

                        if (position >= minEnableAns) {
                            answerCall();
                        } else {
                            ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(view.getLayoutParams());
                            marginParams.setMargins(0, (int) view.getY(), 0, 0);
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(marginParams);
                            view.setLayoutParams(layoutParams);
                        }
                        break;
                }
            }

            return true;
        }
    };
    private View.OnTouchListener rejectButtonTouchListener = new View.OnTouchListener() {
        float mPrevX;

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            float currX;
            int action = event.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN: {
                    mPrevX = event.getX();
                    break;
                }

                case MotionEvent.ACTION_MOVE: {
                    if (ibAnswer.getVisibility() == View.VISIBLE) {
                        ibAnswer.setVisibility(View.INVISIBLE);
                    }

                    currX = event.getRawX();

                    int position = (int) (currX - mPrevX);

                    if (position > minWidthRejectBtn && position < screenWidth && position > answerBtnEnd) {
                        ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(view.getLayoutParams());
                        marginParams.setMargins(position, (int) view.getY(), 0, 0);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(marginParams);
                        view.setLayoutParams(layoutParams);
                    }
                    break;
                }

                case MotionEvent.ACTION_CANCEL:
                    break;

                case MotionEvent.ACTION_UP:
                    ibAnswer.setVisibility(View.VISIBLE);
                    float position = view.getX();

                    if (position <= minEnableReject) {
                        rejectCall();
                    } else {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        params.setMargins(10, 10, 10, 10);
                        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
                            params.addRule(RelativeLayout.ALIGN_PARENT_END);
                        }

                        view.setLayoutParams(params);
                    }
                    break;
            }

            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                + WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                +WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                +WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        setContentView(R.layout.activity_incoming_call);
        //  mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //   mProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        init();

        if (MyFirebaseMessagingService.ringtone != null && MyFirebaseMessagingService.ringtone.isPlaying()) {
            MyFirebaseMessagingService.ringtone.stop();
        }
        if (MyFirebaseMessagingService.vibrator != null) {
            MyFirebaseMessagingService.vibrator.cancel();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true);
            setTurnScreenOn(true);

            // If you want to display the keyguard to prompt the user to unlock the phone:
            /*KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
            keyguardManager.requestDismissKeyguard(this, null);*/
        }


        try {
            // Yeah, this is hidden field.
            field = PowerManager.class.getClass().getField("PROXIMITY_SCREEN_OFF_WAKE_LOCK").getInt(null);
        } catch (Throwable ignored) {
        }

        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(field, getLocalClassName());
        if (!wakeLock.isHeld()) {
            wakeLock.acquire();
        }

    }

    private Camera getCameraInstance() {
        Camera camera = null;
        try {
            currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
            camera = Camera.open(currentCameraId);
            FakeOutgoingcall_Activity.setCameraDisplayOrientation(this, currentCameraId, camera);
        } catch (Exception e) {
            // cannot get camera or does not exist
            Log.e(TAG, "getCameraInstance: ", e);
        }
        return camera;
    }

    private void init() {
        isStarted = true;
        EventBus.getDefault().register(this);
        category_single_view = findViewById(R.id.category_single_view);
        tvName = findViewById(R.id.tvName);
        tvCallLbl = findViewById(R.id.tvCallLbl);
        ibAnswer = findViewById(R.id.ibAnswer);
        ibReject = findViewById(R.id.ibReject);
        ivProfilePic = findViewById(R.id.ivProfilePic);

        call_status = findViewById(R.id.call_status);
        caller_name = findViewById(R.id.caller_name);
        caller_status = findViewById(R.id.caller_status);
        circle_profile_image = findViewById(R.id.circle_profile_image);

        accept_layout = findViewById(R.id.accept_layout);
        disconnect_layout = findViewById(R.id.disconnect_layout);


        mCurrentUserId = SessionManager.getInstance(IncomingCallActivity.this).getCurrentUserID();

        mCallId = getIntent().getStringExtra(EXTRA_DOC_ID);
        fromUserId = getIntent().getStringExtra(EXTRA_FROM_USER_ID);
        fromUserMsisdn = getIntent().getStringExtra(EXTRA_FROM_USER_MSISDN);
        toUserId = getIntent().getStringExtra(EXTRA_TO_USER_ID);
        mRoomId = getIntent().getStringExtra(EXTRA_CALL_ROOM_ID);
        mRecordId = getIntent().getStringExtra(EXTRA_CALL_RECORD_ID);
        isVideoCall = getIntent().getBooleanExtra(EXTRA_CALL_TYPE, false);
        mCallTS = getIntent().getStringExtra(EXTRA_CALL_TIME_STAMP);


        mReconnect = getIntent().getStringExtra("reconnecting");
        if (mReconnect != null) {
            if (mReconnect.equals("true")) {
                //   ReconnectanswerCall();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        CallMessage.openCallScreen(IncomingCallActivity.this, fromUserId, toUserId, mCallId,
                                mRoomId, profilePic, fromUserMsisdn, MessageFactory.CALL_IN_FREE + "",
                                isVideoCall, false, mCallTS, "");
                        finish();
                    }
                }, 1000);
            }
        }
        Getcontactname getcontactname = new Getcontactname(this);
        String name;
        if (fromUserId.equalsIgnoreCase(mCurrentUserId)) {
            name = getcontactname.getSendername(toUserId, fromUserMsisdn);
        } else {
            name = getcontactname.getSendername(fromUserId, fromUserMsisdn);
        }
        //tvName.setText(name);
        caller_name.setText(name);

        getcontactname.configProfilepic(circle_profile_image, fromUserId, false, false, R.drawable.avatar_contact);


        getcontactname.configProfilepic(ivProfilePic, fromUserId, false, false, R.drawable.avatar_contact);
        getcontactname.configProfilepic(category_single_view, fromUserId, false, false, R.drawable.avatar_contact);

        if (isVideoCall) {
            call_status.setText(getString(R.string.app_name) + " VIDEO CALL");
        } else {
            call_status.setText(getString(R.string.app_name) + " VOICE CALL");
        }

        playRingtone();
        if (!canRecordAudio()) {
            requestAudioRecordPermission();
        }

        // Device width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;

        maxWidthAnsBtn = screenWidth * 0.75;
        minWidthRejectBtn = screenWidth * 0.25;
        minEnableAns = screenWidth * 0.35;
        minEnableReject = screenWidth * 0.4;

//        ibAnswer.setOnTouchListener(answerButtonTouchListener);
//        ibReject.setOnTouchListener(rejectButtonTouchListener);

        //--------------------------------------New Code--------------------------------------

        disconnect_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mReject) {
                    mReject = true;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationManager mNotificationManager = (NotificationManager) IncomingCallActivity.this.getSystemService(NOTIFICATION_SERVICE);
                        String id = "my_channel_01";
                        mNotificationManager.deleteNotificationChannel(id);
                    }
                    rejectCall();
                }
            }
        });

        accept_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mAccept) {
                    mAccept = true;
                    answerCall();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                        if
                        NotificationManager mNotificationManager = (NotificationManager) IncomingCallActivity.this.getSystemService(NOTIFICATION_SERVICE);
                        String id = "my_channel_01";
                        mNotificationManager.deleteNotificationChannel(id);
                    }
                   /* new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            answerCall();
                        }
                    }, 4000);
*/

                }
            }
        });

        //--------------------------------------New Code--------------------------------------

        ibAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                answerCall();
            }
        });

        ibReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rejectCall();
            }
        });

        ibAnswer.post(new Runnable() {
            @Override
            public void run() {
                answerBtnEnd = ibAnswer.getX() + ibAnswer.getWidth() + 20;
            }
        });

        ibReject.post(new Runnable() {
            @Override
            public void run() {
                rejectBtnStart = ibReject.getX() - ibAnswer.getWidth() - 20;
            }
        });

        callTimeoutHandler = new Handler();
        callTimeoutRunnable = new Runnable() {
            @Override
            public void run() {
                if (IncomingCallActivity.isStarted) {
                    finish();
                }
            }
        };
        callTimeoutHandler.postDelayed(callTimeoutRunnable, CallsActivity.MISSED_CALL_TIMEOUT);
        initCameraPreview();
    }

    private void initCameraPreview() {
        FrameLayout preview = findViewById(R.id.camera_preview);
        if (isVideoCall) {
            mCamera = getCameraInstance();
            mCameraPreview = new CameraPreview(this, mCamera);
            preview.addView(mCameraPreview);
            View headerView = findViewById(R.id.header_layout);
            headerView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
        } else {
            preview.setVisibility(View.GONE);
        }
    }

    private void playRingtone() {
        AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);

        switch (am.getRingerMode()) {
            case AudioManager.RINGER_MODE_SILENT:

                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                long[] pattern = {0, 1000, 1500}; // start, vibrate duration, sleep duration
                vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, 0);
                break;
            case AudioManager.RINGER_MODE_NORMAL:
                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                    ringtone = RingtoneManager.getRingtone(IncomingCallActivity.this, notification);
                    ringtone.play();
                } catch (Exception e) {
                    MyLog.d("RingtoneException", e.toString());
                    MyLog.e(TAG, "", e);
                }
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {

        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CALL_STATUS)
                || event.getEventName().equalsIgnoreCase(SocketManager.EVENT_DISCONNECT_CALL)) {
            String data = event.getObjectsArray()[0].toString();
            try {
                JSONObject object = new JSONObject(data);
                String recordId = object.getString("recordId");
                String callStatus = object.getString("call_status");
                if (mRecordId.isEmpty())
                    mRecordId = recordId;
                // Close activity if sender tap end button before receiver pick call
                if (recordId.equalsIgnoreCase(mRecordId)) {
                    switch (callStatus) {
                        case MessageFactory.CALL_STATUS_END + "":
                        case MessageFactory.CALL_STATUS_REJECTED + "":

                        case MessageFactory.CALL_STATUS_RECEIVED + "":
                        case MessageFactory.CALL_STATUS_MISSED + "":
                            String from = object.getString("from");
                            if (!from.equalsIgnoreCase(mCurrentUserId)) {
                                MessageDbController db = CoreController.getDBInstance(this);
                                db.updateCallStatus(mCallId, MessageFactory.CALL_STATUS_REJECTED, "00:00");
                            }
                            finish();
                            break;


                    }
                }
                if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_DISCONNECT_CALL)) {
                    finish();
                }


            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CALL_RESPONSE)) {

            String data = event.getObjectsArray()[0].toString();
            MyLog.d("Call_response", data);
            calldisconnect(data);

        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onStop() {
        super.onStop();
        if (wakeLock.isHeld()) {
            wakeLock.release();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
        stopRingtone();
        isStarted = false;

        if (broadcastRunnable != null) {
            broadcastHandler.removeCallbacks(broadcastRunnable);
        }


    }

    private void stopRingtone() {
        if (ringtone != null && ringtone.isPlaying()) {
            ringtone.stop();
        }

        if (vibrator != null) {
            vibrator.cancel();
        }
    }

    private void calldisconnect(String data) {
        try {
            JSONObject object = new JSONObject(data);
            JSONObject dataObj = object.getJSONObject("data");
            mRecordId = dataObj.getString("recordId");
            /*String call_status=object.getString("call_status");
            if( call_status.equals(""+MessageFactory.CALL_STATUS_REJECTED) || call_status.equals(""+MessageFactory.CALL_STATUS_END) ){
                finish();
            }*/
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    public boolean canRecordAudio() {
        int recordPermission = ContextCompat.checkSelfPermission(this, RECORD_AUDIO);
        return recordPermission == PackageManager.PERMISSION_GRANTED;
    }

    private void requestAudioRecordPermission() {
        ActivityCompat.requestPermissions(this, new
                String[]{RECORD_AUDIO}, AUDIO_RECORD_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case AUDIO_RECORD_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    resultAudioRecordPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                }
                break;
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN) {
                    //TODO
                    stopRingtone();
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN) {
                    //TODO
                    stopRingtone();
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }

    private void ReconnectanswerCall() {
        if (canRecordAudio() || resultAudioRecordPermission) {
            String[] splitIds = mCallId.split("-");
            String id = splitIds[2];
            String callDocId = toUserId + "-" + fromUserId + "-" + id;
            String type = "" + MessageFactory.audio_call;
            if (isVideoCall)
                type = "" + MessageFactory.video_call;

            JSONObject object = CallMessage.getCallStatusObject(mCurrentUserId, fromUserId,
                    id, callDocId, mRecordId, MessageFactory.CALL_STATUS_ANSWERED, type);

            MessageDbController db = CoreController.getDBInstance(this);
            //  db.updateCallStatus(mCallId, MessageFactory.CALL_STATUS_ANSWERED, "00:00");
            Log.e(TAG, "ReconnectanswerCall" + mRecordId);
            SendMessageEvent event = new SendMessageEvent();

            event.setEventName(SocketManager.EVENT_CALL_STATUS);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    CallMessage.openCallScreen(IncomingCallActivity.this, fromUserId, toUserId, mCallId,
                            mRoomId, profilePic, fromUserMsisdn, MessageFactory.CALL_IN_FREE + "",
                            isVideoCall, false, mCallTS, "");
                    finish();
                }
            }, 1000);

            ibReject.setOnTouchListener(null);
            ibAnswer.setOnTouchListener(null);
        } else {
            requestAudioRecordPermission();
        }
        mAccept = false;
    }


    private void ReconnectCall() {
        if (canRecordAudio() || resultAudioRecordPermission) {
            String[] splitIds = mCallId.split("-");
            String id = splitIds[2];
            String callDocId = toUserId + "-" + fromUserId + "-" + id;
            String type = "" + MessageFactory.audio_call;
            if (isVideoCall)
                type = "" + MessageFactory.video_call;

            JSONObject object = CallMessage.getCallStatusObject(mCurrentUserId, fromUserId,
                    id, callDocId, mRecordId, MessageFactory.CALL_STATUS_ANSWERED, type);

            MessageDbController db = CoreController.getDBInstance(this);
            //  db.updateCallStatus(mCallId, MessageFactory.CALL_STATUS_ANSWERED, "00:00");
            Log.e(TAG, "ReconnectanswerCall" + mRecordId);
            SendMessageEvent event = new SendMessageEvent();

            event.setEventName(SocketManager.EVENT_CALL_STATUS);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    CallMessage.openCallScreen(IncomingCallActivity.this, fromUserId, toUserId, mCallId,
                            mRoomId, profilePic, fromUserMsisdn, MessageFactory.CALL_IN_FREE + "",
                            isVideoCall, false, mCallTS, "");
                    finish();
                }
            }, 1000);

            ibReject.setOnTouchListener(null);
            ibAnswer.setOnTouchListener(null);
        } else {
            requestAudioRecordPermission();
        }
        mAccept = false;
    }

    private void answerCall() {
        if (canRecordAudio() || resultAudioRecordPermission) {
            String[] splitIds = mCallId.split("-");
            String id = splitIds[2];
            String callDocId = toUserId + "-" + fromUserId + "-" + id;
            String type = "" + MessageFactory.audio_call;
            if (isVideoCall)
                type = "" + MessageFactory.video_call;

            JSONObject object = CallMessage.getCallStatusObject(mCurrentUserId, fromUserId,
                    id, callDocId, mRecordId, MessageFactory.CALL_STATUS_ANSWERED, type);
            Log.e("answerCall", "object" + object);
            MessageDbController db = CoreController.getDBInstance(this);
            db.updateCallStatus(mCallId, MessageFactory.CALL_STATUS_ANSWERED, "00:00");

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_CALL_STATUS);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    CallMessage.openCallScreen(IncomingCallActivity.this, fromUserId, toUserId, mCallId,
                            mRoomId, profilePic, fromUserMsisdn, MessageFactory.CALL_IN_FREE + "",
                            isVideoCall, false, mCallTS, "");
                    finish();
                }
            }, 1000);

            ibReject.setOnTouchListener(null);
            ibAnswer.setOnTouchListener(null);
        } else {
            requestAudioRecordPermission();
        }
        mAccept = false;
    }

    private void rejectCall() {
        String[] splitIds = mCallId.split("-");
        String id = splitIds[2];
        String callDocId = toUserId + "-" + fromUserId + "-" + id;
        int callStatus = MessageFactory.CALL_STATUS_MISSED;
        //   int callStatus = MessageFactory.CALL_STATUS_REJECTED;
        if (!ConnectivityInfo.isInternetConnected(IncomingCallActivity.this)) {
            callStatus = MessageFactory.CALL_STATUS_MISSED;
        }
        String type = "" + MessageFactory.audio_call;
        if (isVideoCall) {
            type = "" + MessageFactory.video_call;
        }
        JSONObject object = CallMessage.getCallStatusObject(mCurrentUserId, fromUserId, id, callDocId, mRecordId, callStatus, "" + type);
        MessageDbController db = CoreController.getDBInstance(this);
        MyLog.e("mCallId", "mCallId" + mCallId + "object" + object);
        db.updateCallStatus(mCallId, callStatus, "00:00");
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_CALL_STATUS);
        event.setMessageObject(object);
        EventBus.getDefault().post(event);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 1000);
        ibReject.setOnTouchListener(null);
        ibAnswer.setOnTouchListener(null);
        mReject = false;
    }

    private void sendIncomingCallBroadcast() {
        if (broadcastHandler == null) {
            broadcastHandler = new Handler();
            broadcastRunnable = new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent();
                    intent.setAction(getPackageName() + ".incoming_call");
                    intent.putExtras(getIntent().getExtras());
                    sendBroadcast(intent);

                    broadcastHandler.postDelayed(this, 5000);
                }
            };
            broadcastHandler.postDelayed(broadcastRunnable, 2500);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        sendIncomingCallBroadcast();
        //  mSensorManager.registerListener(this, mProximity, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    protected void onPause() {
        super.onPause();
        // mSensorManager.unregisterListener(this);

    }


}
