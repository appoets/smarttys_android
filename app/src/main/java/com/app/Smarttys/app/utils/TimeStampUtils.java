package com.app.Smarttys.app.utils;

import android.content.Context;

import com.app.Smarttys.core.SessionManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CAS60 on 5/12/2017.
 */
public class TimeStampUtils {

    private static final String TAG = "TimeStampUtils";

    public static String get12HrTimeFormat(Context context, String ts) {
        try {
            long givenTS = Long.parseLong(ts);
            long serverDiff = SessionManager.getInstance(context).getServerTimeDifference();
            long deviceTS = givenTS + serverDiff;
            Date dateObj = new Date(deviceTS);
            String timeStamp = new SimpleDateFormat("h:mm a", Locale.ENGLISH).format(dateObj);
            return timeStamp;
        } catch (NumberFormatException e) {
            MyLog.e(TAG, "", e);
            return "";
        }
    }

    public static Date getMessageTStoDate(Context context, String ts) {
        try {
            long givenTS = Long.parseLong(ts);
            long serverDiff = SessionManager.getInstance(context).getServerTimeDifference();
            long deviceTS = givenTS + serverDiff;

            DateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            Date dateTemp = new Date(deviceTS);
            return df.parse(df.format(dateTemp));
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
            return null;
        }
    }

    public static Date getDateFormat(long ts) {
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            Date dateTemp = new Date(ts);
            return df.parse(df.format(dateTemp));
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
            return null;
        }
    }

    public static String getDate(long ts) {
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            Date dateTemp = new Date(ts);
            return df.format(dateTemp);
        } catch (Exception e) {
            return "";
        }
    }

    public static Date getYesterdayDate(Date today) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        cal.add(Calendar.DATE, -1);
        Date yesterday = cal.getTime();
        return yesterday;
    }

    public static String getServerTimeStamp(Context context, long deviceTS) {
        long serverDiff = SessionManager.getInstance(context).getServerTimeDifference();
        long serverTS = deviceTS + serverDiff;
        return String.valueOf(serverTS);
    }

    public static Date addDay(Date date, int count) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, count);
        return cal.getTime();
    }

    public static Date addHour(Date date, int count) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, count);
        return cal.getTime();
    }

    public static Date addYear(Date date, int count) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, count);
        return cal.getTime();
    }
}
