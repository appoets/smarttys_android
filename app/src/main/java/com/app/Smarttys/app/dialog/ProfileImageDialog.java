package com.app.Smarttys.app.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.ChatPageActivity;
import com.app.Smarttys.app.activity.GroupInfo;
import com.app.Smarttys.app.activity.ImageZoom;
import com.app.Smarttys.app.activity.UserInfo;
import com.app.Smarttys.app.calls.CallMessage;
import com.app.Smarttys.app.calls.CallsActivity;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.ConnectivityInfo;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.model.MessageItemChat;

import org.appspot.apprtc.CallActivity;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


/**
 * Created by CAS60 on 2/23/2017.
 */
public class ProfileImageDialog extends DialogFragment implements View.OnClickListener {

    private static final String TAG = "ProfileImageDialog";
    private final int AUDIO_RECORD_PERMISSION_REQUEST_CODE = 14;
    private View view;
    private RelativeLayout rlParent;
    private EmojiconTextView tvName;
    private ImageView ivProfilePic;
    private ImageButton ibChat, ibInfo;
    private FrameLayout flChat, flInfo, flVoiceCall, flVideoCall;
    private LinearLayout llActions;
    private String receiverUserId, receiverName, receiverAvatar, receiverNumber, Uid;
    private boolean isGroupChat, fromSecretChat;
    private MessageItemChat msgItem;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        view = inflater.inflate(R.layout.dialog_profile_image, container, false);

        tvName = view.findViewById(R.id.tvName);


        rlParent = view.findViewById(R.id.rlParent);
        llActions = view.findViewById(R.id.llActions);
        ivProfilePic = view.findViewById(R.id.ivProfilePic);
        ivProfilePic.setOnClickListener(ProfileImageDialog.this);
        //ivProfilePic.setFitsSystemWindows(true);
        ibChat = view.findViewById(R.id.ibChat);
        ibChat.setOnClickListener(ProfileImageDialog.this);

        flChat = view.findViewById(R.id.flChat);
        flChat.setOnClickListener(ProfileImageDialog.this);

        flVideoCall = view.findViewById(R.id.fl_video_call);
        flVideoCall.setOnClickListener(ProfileImageDialog.this);

        flVoiceCall = view.findViewById(R.id.fl_voice_call);
        flVoiceCall.setOnClickListener(ProfileImageDialog.this);

        ibInfo = view.findViewById(R.id.ibInfo);
        ibInfo.setOnClickListener(ProfileImageDialog.this);

        flInfo = view.findViewById(R.id.flInfo);
        flInfo.setOnClickListener(ProfileImageDialog.this);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        ViewGroup.LayoutParams params = rlParent.getLayoutParams();
        params.height = (height / 100) * 40;
        params.width = (width / 100) * 80;
        rlParent.setLayoutParams(params);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        msgItem = (MessageItemChat) bundle.getSerializable("MessageItem");
        Uid = bundle.getString("userID");
        isGroupChat = bundle.getBoolean("GroupChat");
//        imageTS = bundle.getLong("imageTS");
        fromSecretChat = bundle.getBoolean("FromSecretChat", false);
//        SerializeBitmap bmp = (SerializeBitmap) bundle.getSerializable("ProfilePic");

        initData();

    }

    private void initData() {
        setBtnVisiblity();
        Typeface typeface = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        tvName.setTypeface(typeface);
        try {
            String msgId = msgItem.getMessageId();
            String[] splitId = msgId.split("-");
            receiverUserId = splitId[1];
        } catch (Exception ex) {
            receiverUserId = msgItem.getReceiverID();
            MyLog.e(TAG, "initData: ", ex);
        }
        receiverAvatar = msgItem.getAvatarImageUrl();
//        receiverAvatar = receiverAvatar.concat("?id=").concat(String.valueOf(Calendar.getInstance().getTimeInMillis()));
        receiverName = msgItem.getSenderName();
        receiverNumber = msgItem.getSenderMsisdn();
        if (isGroupChat) {
            if (receiverAvatar != null && !receiverAvatar.isEmpty()) {
                AppUtils.loadImage(getActivity(), AppUtils.getValidProfilePath(receiverAvatar), ivProfilePic, 0, R.mipmap.ic_group_icon);
            } else {
                ivProfilePic.setImageResource(R.mipmap.ic_group_icon);
                ivProfilePic.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
        } else {
            Getcontactname getcontactname = new Getcontactname(getActivity());
            getcontactname.configProfilepic(ivProfilePic, receiverUserId, false, fromSecretChat, R.mipmap.ic_single_user_icon);
        }

        tvName.setText(receiverName);
    }

    private void setBtnVisiblity() {
        if (isGroupChat) {
            //if(true){

            llActions.setWeightSum(2f);
            LinearLayout.LayoutParams lParams = (LinearLayout.LayoutParams) flChat.getLayoutParams(); //or create new LayoutParams...

            lParams.weight = 1f;
            LinearLayout.LayoutParams lParams2 = (LinearLayout.LayoutParams) flInfo.getLayoutParams(); //or create new LayoutParams...

            lParams2.weight = 1f;

            flChat.setLayoutParams(lParams);
            flInfo.setLayoutParams(lParams);
            flVoiceCall.setVisibility(View.GONE);
            flVideoCall.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ivProfilePic:
                if (receiverAvatar != null && !receiverAvatar.isEmpty()) {
                    Intent imgIntent = new Intent(getActivity(), ImageZoom.class);
                    imgIntent.putExtra("ProfilePath", receiverAvatar);
                    imgIntent.putExtra("Profilepicname", receiverName);
                    startActivity(imgIntent);
                    getDialog().dismiss();
                }
                break;

            case R.id.flChat:
                goChatViewScreen();
                getDialog().dismiss();
                break;

            case R.id.flInfo:
                goInfoScreen();
                getDialog().dismiss();
                break;

            case R.id.ibChat:
                goChatViewScreen();
                getDialog().dismiss();
                break;

            case R.id.ibInfo:
                goInfoScreen();
                getDialog().dismiss();
                break;

            case R.id.fl_video_call:
                performCall(true, getActivity());
                break;

            case R.id.fl_voice_call:
                performCall(false, getActivity());

                break;
        }

    }

    private void goInfoScreen() {
        Intent infoIntent;
        if (isGroupChat) {
            infoIntent = new Intent(getActivity(), GroupInfo.class);
            infoIntent.putExtra("GroupId", receiverUserId);
            infoIntent.putExtra("GroupName", receiverName);
        } else {
            infoIntent = new Intent(getActivity(), UserInfo.class);
            infoIntent.putExtra("UserId", receiverUserId);
            infoIntent.putExtra("UserName", receiverName);
            infoIntent.putExtra("UserNumber", receiverNumber);
            infoIntent.putExtra("FromSecretChat", fromSecretChat);
        }

        infoIntent.putExtra("UserAvatar", AppUtils.getValidProfilePath(receiverAvatar));
        startActivity(infoIntent);
    }

    private void goChatViewScreen() {

        Intent intent = new Intent(getActivity(), ChatPageActivity.class);
        String profileimage = msgItem.getAvatarImageUrl();
        intent.putExtra("receiverUid", msgItem.getNumberInDevice());
        intent.putExtra("receiverName", msgItem.getSenderName());
        intent.putExtra("documentId", receiverUserId);

        intent.putExtra("Username", msgItem.getSenderName());
        intent.putExtra("Image", msgItem.getAvatarImageUrl());
        intent.putExtra("msisdn", msgItem.getSenderMsisdn());
        intent.putExtra("type", 0);

        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public boolean checkAudioRecordPermission(Context context) {
        int result = ContextCompat.checkSelfPermission(context,
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(context,
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void performCall(boolean isVideoCall, Activity context) {
        String mCurrentUserId = SessionManager.getInstance(context).getCurrentUserID();
        if (ConnectivityInfo.isInternetConnected(context))
            if (checkAudioRecordPermission(context)) {
                CallMessage message = new CallMessage(context);
                boolean isOutgoingCall = true;

                String roomid = message.getroomid();
                String timestamp = message.getroomid();
                String callid = mCurrentUserId + "-" + receiverUserId + "-" + timestamp;

                if (!CallsActivity.isStarted) {
                    if (isOutgoingCall) {
                        CallsActivity.opponentUserId = receiverUserId;
                    }

                    PreferenceManager.setDefaultValues(context, org.appspot.apprtc.R.xml.preferences, false);
                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

                    String keyprefRoomServerUrl = context.getString(org.appspot.apprtc.R.string.pref_room_server_url_key);
                    String roomUrl = sharedPref.getString(
                            keyprefRoomServerUrl, context.getString(org.appspot.apprtc.R.string.pref_room_server_url_default));


                    int videoWidth = 0;
                    int videoHeight = 0;
                    String resolution = context.getString(org.appspot.apprtc.R.string.pref_resolution_default);
                    String[] dimensions = resolution.split("[ x]+");
                    if (dimensions.length == 2) {
                        try {
                            videoWidth = Integer.parseInt(dimensions[0]);
                            videoHeight = Integer.parseInt(dimensions[1]);
                        } catch (NumberFormatException e) {
                            videoWidth = 0;
                            videoHeight = 0;
                            MyLog.e("SmarttyCallError", "Wrong video resolution setting: " + resolution);
                        }
                    }
                    Uri uri = Uri.parse(roomUrl);
                    Intent intent = new Intent(context, CallsActivity.class);
                    intent.setData(uri);
                    intent.putExtra(CallsActivity.EXTRA_IS_OUTGOING_CALL, isOutgoingCall);
                    intent.putExtra(CallsActivity.EXTRA_DOC_ID, callid);
                    intent.putExtra(CallsActivity.EXTRA_FROM_USER_ID, mCurrentUserId);
                    intent.putExtra(CallsActivity.EXTRA_TO_USER_ID, receiverUserId);
                    intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, receiverNumber);
                    intent.putExtra(CallsActivity.EXTRA_OPPONENT_PROFILE_PIC, "");
                    intent.putExtra(CallsActivity.EXTRA_NAVIGATE_FROM, context.getClass().getSimpleName()); // For navigating from call activity
                    intent.putExtra(CallsActivity.EXTRA_CALL_CONNECT_STATUS, "0");
                    intent.putExtra(CallsActivity.EXTRA_CALL_TIME_STAMP, timestamp);

                    intent.putExtra(CallsActivity.EXTRA_ROOMID, roomid);
                    intent.putExtra(CallsActivity.EXTRA_LOOPBACK, false);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_CALL, isVideoCall);
                    intent.putExtra(CallsActivity.EXTRA_SCREENCAPTURE, false);
                    intent.putExtra(CallsActivity.EXTRA_CAMERA2, true);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_WIDTH, videoWidth);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_HEIGHT, videoHeight);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_FPS, 0);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_BITRATE, 0);
                    intent.putExtra(CallsActivity.EXTRA_VIDEOCODEC, context.getString(org.appspot.apprtc.R.string.pref_videocodec_default));
                    intent.putExtra(CallsActivity.EXTRA_HWCODEC_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_CAPTURETOTEXTURE_ENABLED, true);
                    intent.putExtra(CallsActivity.EXTRA_FLEXFEC_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_NOAUDIOPROCESSING_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_AECDUMP_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_OPENSLES_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AEC, false);
                    intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AGC, false);
                    intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_NS, false);
                    intent.putExtra(CallsActivity.EXTRA_ENABLE_LEVEL_CONTROL, false);
                    intent.putExtra(CallsActivity.EXTRA_AUDIO_BITRATE, 0);
                    intent.putExtra(CallsActivity.EXTRA_AUDIOCODEC, context.getString(org.appspot.apprtc.R.string.pref_audiocodec_default));
                    intent.putExtra(CallsActivity.EXTRA_DISPLAY_HUD, false);
                    intent.putExtra(CallsActivity.EXTRA_TRACING, false);
                    intent.putExtra(CallsActivity.EXTRA_CMDLINE, false);
                    intent.putExtra(CallsActivity.EXTRA_RUNTIME, 0);

                    intent.putExtra(CallActivity.EXTRA_DATA_CHANNEL_ENABLED, true);
                    intent.putExtra(CallActivity.EXTRA_ORDERED, true);
                    intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS_MS, -1);
                    intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS, -1);
                    intent.putExtra(CallActivity.EXTRA_PROTOCOL, context.getString(org.appspot.apprtc.R.string.pref_data_protocol_default));
                    intent.putExtra(CallActivity.EXTRA_NEGOTIATED, false);
                    intent.putExtra(CallActivity.EXTRA_ID, -1);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);

                    dismiss();
                }


            } else {
                requestAudioRecordPermission(context);
            }
    }

    private void requestAudioRecordPermission(Activity context) {
        ActivityCompat.requestPermissions(context, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, AUDIO_RECORD_PERMISSION_REQUEST_CODE);
    }

}
