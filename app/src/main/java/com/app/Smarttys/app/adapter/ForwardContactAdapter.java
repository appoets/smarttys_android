package com.app.Smarttys.app.adapter;

/**
 * Created by CAS63 on 4/20/2017.
 */

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.CircleImageView;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.bumptech.glide.Glide;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;

import java.util.ArrayList;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;


public class ForwardContactAdapter extends RecyclerView.Adapter<ForwardContactAdapter.MyViewHolder> implements Filterable, FastScrollRecyclerView.SectionedAdapter {

    private static final String TAG = "ForwardContactAdapter";
    public ArrayList<SmarttyContactModel> mDisplayedValues;
    Session session;
    Getcontactname getcontactname;
    private Context context;
    private ArrayList<SmarttyContactModel> mOriginalValues;
    private ChatListItemClickListener listener;
    private UserInfoSession userInfoSession;

    public ForwardContactAdapter(Context context, ArrayList<SmarttyContactModel> data) {
        this.context = context;
        this.mDisplayedValues = data;
        this.mOriginalValues = data;
        session = new Session(context);
        getcontactname = new Getcontactname(context);
        userInfoSession = new UserInfoSession(context);
    }

    public SmarttyContactModel getItem(int position) {
        return mDisplayedValues.get(position);
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        SmarttyContactModel contact = mDisplayedValues.get(position);
        return contact.getFirstName().substring(0, 1);
    }

    public void updateInfo(ArrayList<SmarttyContactModel> aitem) {
        this.mDisplayedValues = aitem;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.forward_contact_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int position) {
        SmarttyContactModel contact = mDisplayedValues.get(position);
        viewHolder.tvStatus.setTextSize(11);
        viewHolder.mobileText.setText("" + contact.getType());
        viewHolder.tvName.setText("" + contact.getFirstName());
        viewHolder.pos = position;

        if (contact.isSelected()) {
            viewHolder.tick.setVisibility(View.VISIBLE);
        } else {
            viewHolder.tick.setVisibility(View.GONE);
        }

        try {
            String userId = contact.get_id();
            if (!contact.getStatus().equals("")) {
                getcontactname.setProfileStatusText(viewHolder.tvStatus, userId, contact.getStatus(), false);
            } else {
                viewHolder.tvStatus.setText(context.getResources().getString(R.string.status_not_available));
            }
        } catch (Exception e) {
            viewHolder.tvStatus.setText(context.getResources().getString(R.string.status_available));
        }

        viewHolder.ivUser.setImageResource(R.drawable.personprofile);
        if (contact.isGroup()) {
            try {
                if (contact.getAvatarImageUrl() != null && !contact.getAvatarImageUrl().isEmpty()) {
                    AppUtils.loadImage(context, AppUtils.getValidGroupPath(contact.getAvatarImageUrl()), viewHolder.ivUser, 100, R.drawable.avatar_group);
                }
            } catch (Exception e) {
                Log.e(TAG, "onBindViewHolder: ", e);
            }
        } else {
            try {
                String to = contact.get_id();
                getcontactname.configProfilepic(viewHolder.ivUser, to, false, true, R.mipmap.chat_attachment_profile_default_image_frame);
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
                //   Picasso.with(context).load(R.drawable.personprofile).into(viewHolder.ivUser);
                Glide.with(context).load(R.drawable.personprofile)
                        .into(viewHolder.ivUser);
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mDisplayedValues.size();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<SmarttyContactModel>) results.values; // has the filtered values
                if (mDisplayedValues.size() == 0) {
//                    Toast.makeText(context, "No Contacts Matching Your Query...", Toast.LENGTH_SHORT).show();
                }

                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<SmarttyContactModel> FilteredArrList = new ArrayList<>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {


                        String contactName = mOriginalValues.get(i).getFirstName();
                        String contactNo = mOriginalValues.get(i).getNumberInDevice();

                        if (contactName.toLowerCase().contains(constraint) || contactNo.toLowerCase().contains(constraint)) {
                            FilteredArrList.add(mOriginalValues.get(i));
                        }


                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    private void createnewgroupSmarttySocketModel(ArrayList<SmarttyContactModel> arg) {
        SmarttyContactModel mycontact = new SmarttyContactModel();
        mycontact.setFirstName("New Group");
        // arg.add(mycontact);
        // mycontact.setAvatarImageUrl(R.drawable.chat);
    }

    public void setChatListItemClickListener(ChatListItemClickListener listener) {
        this.listener = listener;
    }

    public interface ChatListItemClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public AvnNextLTProDemiTextView tvName, mobileText;
        public ImageView tick;
        public int pos;
        protected EmojiconTextView tvStatus;
        protected CircleImageView ivUser;
        Typeface face2 = CoreController.getInstance().getAvnNextLTProRegularTypeface();

        public MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.userName_contacts);
            tvStatus = view.findViewById(R.id.status_contacts);
            tick = view.findViewById(R.id.tick);

            ivUser = view.findViewById(R.id.userPhoto_contacts);
            mobileText = view.findViewById(R.id.mobileText);
            tvName.setTextColor(Color.parseColor("#3f3f3f"));

            tvStatus.setTextColor(Color.parseColor("#808080"));
            tvStatus.setTypeface(face2);
            tvStatus.setTextSize(11);
        }
    }

}



