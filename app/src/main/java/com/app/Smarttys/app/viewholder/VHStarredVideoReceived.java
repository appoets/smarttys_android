package com.app.Smarttys.app.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;


/**
 * Created by CAS63 on 2/6/2017.
 */
public class VHStarredVideoReceived extends RecyclerView.ViewHolder {

    public TextView senderName, time, toname, fromname, datelbl, duration, duration_above, ts_abovecaption;
    public ImageView thumbnail, download, starredindicator_below, userprofile;
    public ImageView starredindicator_above;
    public RelativeLayout caption, videoabove_layout, video_belowlayout;
    public AvnNextLTProRegTextView captiontext;

    public VHStarredVideoReceived(View view) {
        super(view);
        senderName = view.findViewById(R.id.lblMsgFrom);
        userprofile = view.findViewById(R.id.userprofile);
        time = view.findViewById(R.id.ts);
        thumbnail = view.findViewById(R.id.vidshow);
        starredindicator_below = view.findViewById(R.id.starredindicator_below);
        toname = view.findViewById(R.id.toname);
        fromname = view.findViewById(R.id.fromname);
        datelbl = view.findViewById(R.id.datelbl);
        download = view.findViewById(R.id.download);
        duration = view.findViewById(R.id.duration);
        caption = view.findViewById(R.id.caption);
        captiontext = view.findViewById(R.id.captiontext);


        duration_above = view.findViewById(R.id.duration_above);
        starredindicator_above = view.findViewById(R.id.starredindicator_above);
        ts_abovecaption = view.findViewById(R.id.ts_abovecaption);
        videoabove_layout = view.findViewById(R.id.videoabove_layout);
        video_belowlayout = view.findViewById(R.id.video_belowlayout);
    }
}
