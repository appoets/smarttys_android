package com.app.Smarttys.app.activity;

import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.app.Smarttys.R;


public class HideActivity extends AppCompatActivity implements View.OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unhide_app);

        findViewById(R.id.tv_hide).setOnClickListener(this);
        findViewById(R.id.tv_unhide).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_hide:
                hideApp();
                break;

            case R.id.tv_unhide:
                unhideApp();
                break;
        }
    }

    private void unhideApp() {
        PackageManager p = getPackageManager();
        ComponentName componentName = new ComponentName(this, LoginAuthenticationActivity.class);
        p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
        Toast.makeText(this, "It will unhide in 20 seconds", Toast.LENGTH_SHORT).show();
        finish();
    }

    private void hideApp() {
        PackageManager p = getPackageManager();
        ComponentName componentName = new ComponentName(this, LoginAuthenticationActivity.class); // activity which is first time open in manifiest file which is declare as <category android:name="android.intent.category.LAUNCHER" />
        p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
        Toast.makeText(this, "It will hide in 20 seconds", Toast.LENGTH_SHORT).show();
        finish();
    }
}
