package com.app.Smarttys.app.utils;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.DigestException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by user134 on 6/8/2018.
 */

public class CryptLibDecryption {
    private static final String TAG = "CryptLibDecryption";
    private static final String CHARSET = "UTF-8";
    private static final String HASH_ALGORITHM = "SHA-256";

    public static String encrypt(String password, String message)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        SecretKey secret = generateKey(password);
        Cipher cipher = null;
        cipher = Cipher.getInstance("AES");
        //cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secret);
        byte[] cipherText = cipher.doFinal(message.getBytes(StandardCharsets.UTF_8));
        return Base64.encodeToString(cipherText, Base64.DEFAULT);
    }

    public static String decrypt(String secret, String cipherText) {
        try {
            //     cipherText=cipherText.replace("\n","");
            //Log.d(TAG, "decrypt: "+secret);
            //Log.d(TAG, "decrypt: "+cipherText);
            if (cipherText != null && cipherText.startsWith("{") && cipherText.endsWith("}")) {
                return cipherText;
            }
            byte[] cipherData = Base64.decode(cipherText, Base64.NO_WRAP);
            byte[] saltData = Arrays.copyOfRange(cipherData, 8, 16);

            MessageDigest md5 = MessageDigest.getInstance("MD5");
            final byte[][] keyAndIV = GenerateKeyAndIV(32, 16, 1, saltData, secret.getBytes(CHARSET), md5);
            SecretKeySpec key = new SecretKeySpec(keyAndIV[0], "AES");
            IvParameterSpec iv = new IvParameterSpec(keyAndIV[1]);

            byte[] encrypted = Arrays.copyOfRange(cipherData, 16, cipherData.length);

            try {
                Cipher aesCBC = Cipher.getInstance("AES/CBC/PKCS5Padding");
                aesCBC.init(Cipher.DECRYPT_MODE, key, iv);
                byte[] decryptedData = aesCBC.doFinal(encrypted);
                String decryptedText = new String(decryptedData, CHARSET);
                //Log.d(TAG, "decryptionWorking: decr result: " + decryptedText);
                return decryptedText;
            } catch (BadPaddingException e) {
/*                Cipher aesCBC = Cipher.getInstance("AES/CBC/NoPadding");
                aesCBC.init(Cipher.DECRYPT_MODE, key, iv);
                byte[] decryptedData = aesCBC.doFinal(encrypted);
                String decryptedText = new String(decryptedData, CHARSET);
                return decryptedText;*/
                return cipherText;
            }
        } catch (Exception e) {
            MyLog.e(TAG, "decryptionWorking: ", e);
        }
        return cipherText;
    }


    public static byte[][] GenerateKeyAndIV(int keyLength, int ivLength, int iterations, byte[] salt, byte[] password, MessageDigest md) {

        int digestLength = md.getDigestLength();
        int requiredLength = (keyLength + ivLength + digestLength - 1) / digestLength * digestLength;
        byte[] generatedData = new byte[requiredLength];
        int generatedLength = 0;

        try {
            md.reset();

            // Repeat process until sufficient data has been generated
            while (generatedLength < keyLength + ivLength) {

                // Digest data (last digest if available, password data, salt if available)
                if (generatedLength > 0)
                    md.update(generatedData, generatedLength - digestLength, digestLength);
                md.update(password);
                if (salt != null)
                    md.update(salt, 0, 8);
                md.digest(generatedData, generatedLength, digestLength);

                // additional rounds
                for (int i = 1; i < iterations; i++) {
                    md.update(generatedData, generatedLength, digestLength);
                    md.digest(generatedData, generatedLength, digestLength);
                }

                generatedLength += digestLength;
            }

            // Copy key and IV into separate byte arrays
            byte[][] result = new byte[2][];
            result[0] = Arrays.copyOfRange(generatedData, 0, keyLength);
            if (ivLength > 0)
                result[1] = Arrays.copyOfRange(generatedData, keyLength, keyLength + ivLength);

            return result;

        } catch (DigestException e) {
            throw new RuntimeException(e);

        } finally {
            // Clean out temporary data
            Arrays.fill(generatedData, (byte) 0);
        }
    }

    private static SecretKeySpec generateKey(final String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final MessageDigest digest = MessageDigest.getInstance(HASH_ALGORITHM);
        byte[] bytes = password.getBytes(StandardCharsets.UTF_8);
        digest.update(bytes, 0, bytes.length);
        byte[] key = digest.digest();

        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        return secretKeySpec;
    }

}
