package com.app.Smarttys.app.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.CircleImageView;
import com.app.Smarttys.core.model.BlockListPojo;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by CAS63 on 3/7/2017.
 */
public class BlockContactAdapter extends RecyclerView.Adapter<BlockContactAdapter.MyViewHolder> {

    Activity context;
    private ArrayList<BlockListPojo> blockListPojos = new ArrayList<>();


    public BlockContactAdapter(Activity context, ArrayList<BlockListPojo> blockListPojos) {
        super();
        this.context = context;
        this.blockListPojos = blockListPojos;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.block_contact_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BlockListPojo blockListPojo = blockListPojos.get(position);
        holder.uname.setText(blockListPojo.getName());
        holder.unumber.setText(blockListPojo.getNumber());
        String image = blockListPojo.getImagePath();

        if ((image != null && !image.equals(""))) {
            String path = AppUtils.getValidProfilePath(image);
        /*    Picasso.with(context).load(path).fit().error(
                    R.mipmap.chat_attachment_profile_default_image_frame)
                    .into(holder.imageview);
      */
            Glide.with(context).load(path).error(
                    R.mipmap.chat_attachment_profile_default_image_frame).fitCenter()
                    .into(holder.imageview);
        } else {
            //   Picasso.with(context).load(R.mipmap.chat_attachment_profile_default_image_frame).into(holder.imageview);
            Glide.with(context).load(R.mipmap.chat_attachment_profile_default_image_frame)
                    .into(holder.imageview);
        }

        if (blockListPojo.isSecretChat()) {
            holder.ivSecretIcon.setVisibility(View.VISIBLE);
        } else {
            holder.ivSecretIcon.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return this.blockListPojos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public AvnNextLTProDemiTextView uname, unumber;
        public ImageView ivSecretIcon;
        CircleImageView imageview;

        public MyViewHolder(View view) {
            super(view);
            uname = view.findViewById(R.id.BlockUserName);
            unumber = view.findViewById(R.id.blockUserNumber);
            imageview = view.findViewById(R.id.block_user_image);
            ivSecretIcon = view.findViewById(R.id.ivSecretIcon);
        }
    }


}
