package com.app.Smarttys.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.TimeStampUtils;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.ShortcutBadgeManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.MuteStatusPojo;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SecretChatListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable, FastScrollRecyclerView.SectionedAdapter {

    private static final String TAG = "SecretChatListAdapter";
    public List<MessageItemChat> mDisplayedValues;
    private List<MessageItemChat> mListData;
    private ShortcutBadgeManager shortcutBadgeManager;
    private ChatListItemClickListener listener;
    private String currentUserId;
    private Context mContext;
    private Session session;
    private UserInfoSession userInfoSession;
    private Getcontactname getcontactname;

    public SecretChatListAdapter(Context mContext, ArrayList<MessageItemChat> mListData) {
        this.mListData = mListData;
        this.mDisplayedValues = mListData;
        this.mListData = mListData;
        this.mContext = mContext;
        session = new Session(mContext);
        userInfoSession = new UserInfoSession(mContext);

        shortcutBadgeManager = new ShortcutBadgeManager(mContext);
        currentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        getcontactname = new Getcontactname(mContext);
    }

    public void updateInfo(List<MessageItemChat> aitem) {
        this.mDisplayedValues = aitem;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return this.mDisplayedValues.size();
    }


    @Override
    public int getItemViewType(int position) {
        return 1;
    }


    public MessageItemChat getItem(int position) {
        return mDisplayedValues.get(position);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());


        View v = inflater.inflate(R.layout.secret_chat_list_items, viewGroup, false);
        viewHolder = new ViewHolderChat(v);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {


        final ViewHolderChat vh2 = (ViewHolderChat) viewHolder;

        configureViewHolderChat(vh2, position);
        setItemClickListener(vh2, position);
    }

    private void setItemClickListener(final ViewHolderChat vh2, final int position) {
        if (listener != null) {
            vh2.rlChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(vh2.rlChat, position);
                }
            });

            vh2.rlChat.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    listener.onItemLongClick(vh2.itemView, position);
                    return false;
                }
            });

            vh2.storeImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(vh2.storeImage, position);
                }
            });

            vh2.storeImage.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    listener.onItemLongClick(vh2.itemView, position);
                    return false;
                }
            });
        }
    }


    private void configureViewHolderChat(ViewHolderChat vh, int position) {
        final MessageItemChat chat = mDisplayedValues.get(position);

        vh.ivMsgType.setVisibility(View.GONE);

        if (chat.getMessageType() != null) {
            vh.newMessage.setVisibility(View.GONE);
            vh.tvTyping.setVisibility(View.GONE);

            if (chat.getTypingAt() != 0) {
                vh.newMessage.setVisibility(View.GONE);
                vh.tvTyping.setVisibility(View.VISIBLE);

                if (chat.getMessageId().contains("-g-")) {
                    vh.tvTyping.setText(chat.getTypePerson().concat(" typing..."));
                } else {
                    vh.tvTyping.setText("typing...");
                }
            }

            if (chat.getMessageType().equals("" + MessageFactory.text)) {
                vh.newMessage.setTextColor(ContextCompat.getColor(mContext, R.color.chatlist_messagecolor));
                vh.newMessage.setText(chat.getTextMessage());
            } else if (chat.getMessageType().equals("" + MessageFactory.picture)) {
                vh.newMessage.setText("Image");
                vh.ivMsgType.setVisibility(View.GONE);
                vh.ivMsgType.setImageResource(R.drawable.camera);
            } else if (chat.getMessageType().equals("" + MessageFactory.contact)) {
                vh.newMessage.setText("Contact");
                vh.ivMsgType.setVisibility(View.GONE);
                vh.ivMsgType.setImageResource(R.drawable.contact);
            } else if (chat.getMessageType().equals("" + MessageFactory.video)) {
                vh.newMessage.setText("Video");
                vh.ivMsgType.setVisibility(View.GONE);
                vh.ivMsgType.setImageResource(R.drawable.video);
            } else if (chat.getMessageType().equals("" + MessageFactory.audio)) {
                vh.newMessage.setText("Audio");
                vh.ivMsgType.setVisibility(View.GONE);
                vh.ivMsgType.setImageResource(R.drawable.audio);
            } else if (chat.getMessageType().equals("" + MessageFactory.document)) {
                vh.newMessage.setText("Document");
                vh.ivMsgType.setVisibility(View.GONE);
                vh.ivMsgType.setImageResource(R.drawable.document);
            } else if (chat.getMessageType().equals("" + MessageFactory.web_link)) {
                vh.newMessage.setText("Weblink");
                vh.ivMsgType.setVisibility(View.GONE);
                vh.ivMsgType.setImageResource(R.drawable.link);
            }
        } else {
            vh.newMessage.setText("");
        }
        vh.storeName.setText(chat.getSenderName());

        try {
            configureDateLabel(vh.newMessageDate, position);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

        vh.newMessageCount.setVisibility(View.GONE);
       /* if (chat.hasNewMessage()) {
            if (chat.getNewMessageCount() != null && chat.getNewMessageCount().length() > 0) {
                vh.newMessageCount.setVisibility(View.VISIBLE);
                vh.newMessageCount.setText("" + chat.getNewMessageCount());
            }
        }*/
        String[] arrDocId = chat.getMessageId().split("-");
        String toUserId = arrDocId[1];
        String docId = currentUserId.concat("-").concat(arrDocId[1]).concat("-secret");
        String convId = userInfoSession.getChatConvId(docId);

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
        MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(currentUserId, toUserId, convId, true);

        if (muteData != null && muteData.getMuteStatus().equals("1")) {
            vh.mute_chatlist.setVisibility(View.VISIBLE);
        } else {
            vh.mute_chatlist.setVisibility(View.GONE);
        }

        /*if (session.getmark(toUserId)) {
            vh.newMessageCount.setVisibility(View.GONE);
            // vh.tick.setVisibility(View.GONE);

        } else {
            vh.newMessageCount.setVisibility(View.VISIBLE);
            //vh.tick.setVisibility(View.VISIBLE);
        }*/

        if (convId != null && !convId.equals("")) {
            int countMsg = shortcutBadgeManager.getSingleBadgeCount(convId);
            if (countMsg > 0) {
                vh.newMessageCount.setVisibility(View.VISIBLE);
                vh.newMessageCount.setBackgroundResource(R.drawable.ic_msg_count_secret);
                if (countMsg > 0) {
                    vh.newMessageCount.setText("" + countMsg);
                } else {
                    vh.newMessageCount.setText("");
                }
            } else {
                vh.newMessageCount.setVisibility(View.GONE);
            }
        } else {
            vh.newMessageCount.setVisibility(View.GONE);
        }

        getcontactname.configProfilepic(vh.storeImage, toUserId, false, true, R.mipmap.chat_attachment_profile_default_image_frame);

        if (chat.isSelected()) {
            vh.tick.setVisibility(View.VISIBLE);
        } else {
            vh.tick.setVisibility(View.GONE);
        }


    }

    private void configureDateLabel(TextView tvDateLbl, int position) {

        MessageItemChat item = mDisplayedValues.get(position);
        if (item.getTS() != null && !item.getTS().equals("")) {
            String currentItemTS = TimeStampUtils.getServerTimeStamp(mContext, Long.parseLong(item.getTS()));
            if (currentItemTS.equals("0")) {
                tvDateLbl.setText("");
            } else {
                Date currentItemDate = TimeStampUtils.getDateFormat(Long.parseLong(currentItemTS));
                if (currentItemDate != null) {
                    String mydate = TimeStampUtils.get12HrTimeFormat(mContext, item.getTS());
                    mydate = mydate.replace(".", "");
                    setDateText(tvDateLbl, currentItemDate, currentItemTS, mydate);
                } else {
                    tvDateLbl.setText("");
                }
            }
        } else {
            tvDateLbl.setText("");
        }
    }

    private void setDateText(TextView tvDateLbl, Date currentItemDate, String ts, String time) {
        Calendar calendar = Calendar.getInstance();
        Date today = TimeStampUtils.getDateFormat(calendar.getTimeInMillis());
        Date yesterday = TimeStampUtils.getYesterdayDate(today);

        if (currentItemDate.equals(today)) {
            tvDateLbl.setText(time);
        } else if (currentItemDate.equals(yesterday)) {
            tvDateLbl.setText("Yesterday");
        } else {
            DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
            String formatDate = df.format(currentItemDate);
            tvDateLbl.setText(formatDate);
        }
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<MessageItemChat>) results.values; // has the filtered values
                if (mDisplayedValues.size() == 0) {
                    // Toast.makeText(mContext, "No Contacts Matching Your Query...", Toast.LENGTH_SHORT).show();
                }

                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<MessageItemChat> FilteredArrList = new ArrayList<>();

                if (mListData == null) {
                    mListData = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mListData.size();
                    results.values = mListData;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mListData.size(); i++) {


                        String senderName = mListData.get(i).getSenderName();
                        if (senderName.toLowerCase().contains(constraint)) {
                            FilteredArrList.add(mListData.get(i));
                        }


                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public void setChatListItemClickListener(ChatListItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        return mListData.get(position).getSenderName().substring(0, 1);
    }

    public interface ChatListItemClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

}
