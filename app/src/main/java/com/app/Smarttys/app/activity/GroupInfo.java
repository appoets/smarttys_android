package com.app.Smarttys.app.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.GroupInfoAdapter;
import com.app.Smarttys.app.adapter.RItemAdapter;
import com.app.Smarttys.app.dialog.ChatLockPwdDialog;
import com.app.Smarttys.app.dialog.CustomAlertDialog;
import com.app.Smarttys.app.dialog.CustomMultiTextItemsDialog;
import com.app.Smarttys.app.dialog.MuteAlertDialog;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.ConnectivityInfo;
import com.app.Smarttys.app.utils.DocOpenUtils;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.GroupInfoSession;
import com.app.Smarttys.app.utils.MuteUnmute;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.app.widget.AvnNextLTProDemiButton;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.message.PictureMessage;
import com.app.Smarttys.core.model.ChatLockPojo;
import com.app.Smarttys.core.model.GroupInfoPojo;
import com.app.Smarttys.core.model.GroupMembersPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.MultiTextDialogPojo;
import com.app.Smarttys.core.model.MuteStatusPojo;
import com.app.Smarttys.core.model.MuteUserPojo;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.smarttyhelperclass.SmarttyRegularExp;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.kyleduo.switchbutton.SwitchButton;
import com.soundcloud.android.crop.Crop;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import id.zelory.compressor.Compressor;

/**
 * Created by CAS60 on 11/29/2016.
 */
public class GroupInfo extends CoreActivity implements View.OnClickListener, MuteAlertDialog.MuteAlertCloseListener, RItemAdapter.OnItemClickListener {

    private static final String TAG = "GroupInfo";
    private static CustomMultiTextItemsDialog dialog;
    private final int ADD_CONTACT = 21;
    private final int GALLERY_REQUEST_CODE = 1;
    private final int CAMERA_REQUEST_CODE = 2;
    private final int ADD_MEMBER_REQUEST_CODE = 3;
    private final int CHANGE_GROUP_NAME_REQUEST_CODE = 4;
    AvnNextLTProDemiTextView tvParticipantTitle;
    GroupMembersPojo mCurrentUserData;
    Getcontactname getcontactname;
    RelativeLayout medialayout;
    LinearLayout media_lineralayout;
    String uid, contactname, image, msisdn;
    Dialog listDialog;
    TextView mediacount;
    Boolean ismutechange = false;
    AvnNextLTProDemiTextView mute;
    Boolean isgroupemty = false;
    private AvnNextLTProDemiButton btnExitGroup, btnDeleteGroup;
    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private RecyclerView rvGroupMembers;
    private RecyclerView rvMedia = null;
    private SwitchButton swMute;
    private AvnNextLTProRegTextView tvMembersCount, tvAddMember, custom_notification, groupempty, tvGroupCreatedInfo;
    private ImageView ivGroupDp;
    private int valueposition;
    private SessionManager sessionManager;
    private GroupInfo mActivity;
    private Session session;
    private GroupInfoSession groupInfoSession;
    private GroupInfoAdapter adapter;
    private String mGroupId, mGroupName, groupUserIds, mCurrentUserId, mLocDbDocId, value;
    private List<GroupMembersPojo> savedMembersList, unsavedMembersList, allMembersList;
    private boolean isAdminUser, isLiveGroup;
    private long lastClickTime;
    private Menu groupMenu;
    private ArrayList<String> imgzoompath;
    private ArrayList<MessageItemChat> mChatData;
    private Uri cameraImageUri;
    private ArrayList<MessageItemChat> horizontalList;
    private HorizontalAdapter horizontalAdapter = null;
    private int membersCount;
    private UserInfoSession userInfoSession;
    private String savedNumber = "";
    private int savedNumberIndex = -1;
    private ProgressBar progressBar;
    private Context mContext;

    public static void dismissInfo() {
        try {
            if (dialog != null && dialog.isVisible())
                dialog.dismiss();
        } catch (Exception ex) {
            MyLog.e(TAG, "dismissInfo: ", ex);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyLog.d(TAG, "slowTest onCreate: ");
        setContentView(R.layout.activity_group_info);
        horizontalList = new ArrayList<MessageItemChat>();
        mContext = GroupInfo.this;
        imgzoompath = new ArrayList<String>();
        getcontactname = new Getcontactname(GroupInfo.this);
        mActivity = GroupInfo.this;
        if (savedInstanceState != null) {
            cameraImageUri = Uri.parse(savedInstanceState.getString("ImageUri"));
        } else {
            cameraImageUri = Uri.parse("");
        }

        initProgress(getString(R.string.loading_in), true);
        MyLog.d(TAG, "slowTest onCreate: 2");
        initView();
        MyLog.d(TAG, "slowTest onCreate: 2b");
        initData();
        exit_delete();
        MyLog.d(TAG, "slowTest onCreate: 3");
      /*  swMute.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (ConnectivityInfo.isInternetConnected(GroupInfo.this)) {
                    if (isChecked) {
                        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(GroupInfo.this);
                        MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(mCurrentUserId, null, mGroupId, false);
//Check  mute status is empty assign to 0

                        if(AppUtils.isEmpty(muteData.getMuteStatus())){

                            //      if(ismutecheckchange)

                            muteData.setMuteStatus("0");

                        }
                        if (muteData == null || muteData.getMuteStatus().equals("0")) {
                            MuteUserPojo muteUserPojo = new MuteUserPojo();
                            muteUserPojo.setReceiverId(mGroupId);
                            muteUserPojo.setSecretType("no");
                            muteUserPojo.setChatType(MessageFactory.CHAT_TYPE_GROUP);

                            ArrayList<MuteUserPojo> muteUserList = new ArrayList<>();
                            muteUserList.add(muteUserPojo);

                            Bundle putBundle = new Bundle();
                            putBundle.putSerializable("MuteUserList", muteUserList);

                            MuteAlertDialog dialog = new MuteAlertDialog();
                            dialog.setArguments(putBundle);
                            dialog.setCancelable(false);
                            dialog.setMuteAlertCloseListener(GroupInfo.this);
                            dialog.show(getSupportFragmentManager(), "Mute");
                        }
                    } else {
                        MuteUnmute.performUnMute(GroupInfo.this, EventBus.getDefault(), mGroupId,
                                MessageFactory.CHAT_TYPE_GROUP, "no");

                        ismutechange = true;

                        showProgressDialog();
                    }
                } else {
                    swMute.setChecked(!isChecked);
                    Toast.makeText(GroupInfo.this, "Check Your Network Connection", Toast.LENGTH_SHORT).show();
                }
            }

        });*/
        MyLog.d(TAG, "slowTest onCreate: 4");
    }

    private void initView() {
        progressBar = findViewById(R.id.pbHeaderProgress);
        progressBar.setVisibility(View.VISIBLE);
        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
        mediacount = findViewById(R.id.mediacount);
        medialayout = findViewById(R.id.medialayout);
        media_lineralayout = findViewById(R.id.media_lineralayout);
        rvGroupMembers = findViewById(R.id.rvGroupMembers);

        /*rvGroupMembers.addOnItemTouchListener(new RItemAdapter(GroupInfo.this,
                rvGroupMembers, GroupInfo.this));*/
      /*  rvMedia = findViewById(R.id.rvMedia);
        LinearLayoutManager mediaManager = new LinearLayoutManager(GroupInfo.this, LinearLayoutManager.HORIZONTAL, false);
        rvMedia.setLayoutManager(mediaManager);

        swMute = findViewById(R.id.swMute);
        mute = findViewById(R.id.mute);
*/
        session = new Session(GroupInfo.this);
        //   tvMembersCount = findViewById(R.id.tvMembersCount);
        //  tvParticipantTitle = findViewById(R.id.tvParticipantTitle);
        //  groupempty = findViewById(R.id.groupempty);
        //   tvAddMember = findViewById(R.id.tvAddMember);
        //   tvAddMember.setOnClickListener(GroupInfo.this);

        //  custom_notification = findViewById(R.id.custom_notification);

        //  btnExitGroup = findViewById(R.id.btnExitGroup);
        //  btnExitGroup.setOnClickListener(GroupInfo.this);
        //  btnDeleteGroup = findViewById(R.id.btnDeleteGroup);
        //  btnDeleteGroup.setOnClickListener(GroupInfo.this);
        ivGroupDp = findViewById(R.id.ivGroupDp);
        ivGroupDp.setOnClickListener(GroupInfo.this);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(RecyclerView.VERTICAL);
        rvGroupMembers.setLayoutManager(llm);
/*        rvGroupMembers.setHasFixedSize(true);
        rvGroupMembers.setNestedScrollingEnabled(false);*/


    }

    private void initData() {

        sessionManager = SessionManager.getInstance(GroupInfo.this);
        mCurrentUserId = sessionManager.getCurrentUserID();
        Bundle data = getIntent().getExtras();
        if (data != null) {
            mGroupId = data.getString("GroupId", "");
            mGroupName = data.getString("GroupName", "");
        } else {
            mGroupId = "";
            mGroupName = "";
        }

        mLocDbDocId = mCurrentUserId.concat("-").concat(mGroupId).concat("-g");
        mChatData = new ArrayList<>();
        loadFromDB();
        collapsingToolbarLayout.setTitle(mGroupName);
        Typeface typeface = CoreController.getInstance().getAvnNextLTProDemiTypeface();
        collapsingToolbarLayout.setCollapsedTitleTypeface(typeface);
        collapsingToolbarLayout.setExpandedTitleTypeface(typeface);
        membersCount = 0;

        allMembersList = new ArrayList<>();
        savedMembersList = new ArrayList<>();
        unsavedMembersList = new ArrayList<>();

        adapter = new GroupInfoAdapter(mActivity, mContext, allMembersList, getSupportFragmentManager(), this);
        rvGroupMembers.setAdapter(adapter);


        userInfoSession = new UserInfoSession(GroupInfo.this);
        groupInfoSession = new GroupInfoSession(GroupInfo.this);
        GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(mCurrentUserId.concat("-").concat(mGroupId).concat("-g"));
        if (infoPojo != null) {
            String memerlist = infoPojo.getGroupMembers();
            MyLog.d(TAG, "initData: emptygroup set");
            if (memerlist != null && memerlist.equalsIgnoreCase("")) {

                //   tvParticipantTitle.setVisibility(View.GONE);
                if (tvMembersCount != null) {
                    tvMembersCount.setVisibility(View.GONE);
                }
            }


            String[] useridsplitmemeber = memerlist.split(",");
            int countofmember = useridsplitmemeber.length;
            if (countofmember <= 0) {

                //  tvParticipantTitle.setVisibility(View.GONE);
            }
        }

        if (infoPojo != null) {
            isLiveGroup = infoPojo.isLiveGroup();
            if (infoPojo.getAvatarPath() != null) {
                String path = infoPojo.getAvatarPath();
                Glide
                        .with(this)
                        .load(AppUtils.getGlideURL(AppUtils.getValidGroupPath(path), mContext))
                        .asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontAnimate()
                        .into(new SimpleTarget<Bitmap>() {

                            @Override
                            public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                // TODO Auto-generated method stub
                                ivGroupDp.setImageBitmap(arg0);
                            }
                        });

                // AppUtils.loadImage(this,AppUtils.getValidGroupPath(path),ivGroupDp);
                //Picasso.with(GroupInfo.this).load(path).error(R.drawable.group_icon2).into(ivGroupDp);
            }
        }
        if (swMute != null) {
            mute_gone_exit();
        }

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(mCurrentUserId, null, mGroupId, false);

        if (muteData != null && muteData.getMuteStatus().equals("1")) {
            if (swMute != null) {
                swMute.setChecked(true);
            }
            //  swMute.setChecked(true);
        } else {
            if (swMute != null) {
                swMute.setChecked(false);
            }
            //  swMute.setChecked(false);

        }

        if (isLiveGroup) {

            getGroupDetails();

        } else if (!isLiveGroup) {

            progressBar.setVisibility(View.GONE);

            // getGroupDetails();

        }
    }

    private void updateAdapter() {
        if (adapter != null) {
            adapter.updateList(allMembersList);
            adapter.notifyItemChanged(savedNumberIndex);
        }
    }

    private void loadFromDB() {
        MessageDbController db = CoreController.getDBInstance(this);
        ArrayList<MessageItemChat> items = db.selectAllChatMessages(mLocDbDocId, MessageFactory.CHAT_TYPE_GROUP);
        mChatData.clear();
        mChatData.addAll(items);
        mediafile();
        if (horizontalList.size() > 0) {
            if (media_lineralayout != null)
                media_lineralayout.setVisibility(View.VISIBLE);
        }
    }

    protected void mediafile() {

        for (int i = 0; i < mChatData.size(); i++) {
            try {
                String type = mChatData.get(i).getMessageType();
                int mtype = Integer.parseInt(type);

                if (MessageFactory.picture == mtype) {
                    MessageItemChat msgItem = mChatData.get(i);
                    if (msgItem.getChatFileLocalPath() != null) {
                        String path = msgItem.getImagePath();
                        File file = new File(path);
                        if (file.exists()) {
                            horizontalList.add(msgItem);
                            imgzoompath.add(path);
                        }
                    } /*else if (msgItem.getChatFileLocalPath() != null) {
                    String path = msgItem.getChatFileLocalPath();
                    File file = new File(path);
                    if (file.exists()) {
                        Uri pathuri = Uri.fromFile(file);
                        horizontalList.add(msgItem);
                        imgzoompath.add(path);
                    }
                }*/

                }
                if (MessageFactory.video == mtype) {
                    MessageItemChat msgItem = mChatData.get(i);
               /*     if (msgItem.getVideoPath() != null) {
                        String path = msgItem.getVideoPath();
                        File file = new File(path);
                        if (file.exists()) {
                            Uri pathuri = Uri.fromFile(file);
                            horizontalList.add(msgItem);
                            imgzoompath.add(path);
                        }
                    } else */
                    if (msgItem.getChatFileLocalPath() != null) {
                        String path = msgItem.getChatFileLocalPath();
                        File file = new File(path);
                       /* File file = new File(path);
                        if (file.exists()) {
                            Uri pathuri = Uri.fromFile(file);
                            horizontalList.add(msgItem);
                            imgzoompath.add(path);
                        }*/
                        if (file.exists()) {
                            if (msgItem.getUploadStatus() == MessageFactory.UPLOAD_STATUS_COMPLETED) {
                                //Uri pathuri = Uri.fromFile(file);
                                horizontalList.add(msgItem);
                                imgzoompath.add(path);
                            }
                        }
                    }
                }
                if (MessageFactory.audio == mtype) {
                    MessageItemChat msgItem = mChatData.get(i);
                    if (msgItem.getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                        if (msgItem.getAudioPath() != null) {
                            String path = msgItem.getAudioPath();
                            File file = new File(path);
                            if (file.exists()) {
                                // Uri pathuri = Uri.fromFile(file);
                                horizontalList.add(msgItem);
                                imgzoompath.add(path);
                                //  uriList.add(pathuri);
                            }
                        } else if (msgItem.getChatFileLocalPath() != null) {
                            String path = msgItem.getChatFileLocalPath();
                            File file = new File(path);
                            if (file.exists()) {
                                //Uri pathuri = Uri.fromFile(file);
                                horizontalList.add(msgItem);
                                imgzoompath.add(path);
                                // uriList.add(pathuri);
                            }
                        }
                    }

                    //Add from audio attached

                    else if (msgItem.getaudiotype() == MessageFactory.AUDIO_FROM_ATTACHMENT) {
                        if (msgItem.getAudioPath() != null) {
                            String path = msgItem.getAudioPath();
                            File file = new File(path);
                            if (file.exists()) {
                                // Uri pathuri = Uri.fromFile(file);
                                horizontalList.add(msgItem);
                                imgzoompath.add(path);
                                //  uriList.add(pathuri);
                            }
                        } else if (msgItem.getChatFileLocalPath() != null) {
                            String path = msgItem.getChatFileLocalPath();
                            File file = new File(path);
                            if (file.exists()) {
                                // Uri pathuri = Uri.fromFile(file);
                                horizontalList.add(msgItem);
                                imgzoompath.add(path);
                                // uriList.add(pathuri);
                            }
                        }
                    }
                }

                if (MessageFactory.document == mtype) {
                    MessageItemChat msgItem = mChatData.get(i);
                    if (msgItem.getVideoPath() != null) {
                        String path = msgItem.getChatFileLocalPath();
                        File file = new File(path);
                        if (file.exists()) {
                            Uri pathuri = Uri.fromFile(file);
                            horizontalList.add(msgItem);
                            imgzoompath.add(path);
                        }
                    } else if (msgItem.getChatFileLocalPath() != null) {
                        String path = msgItem.getChatFileLocalPath();
                        File file = new File(path);
                        if (file.exists()) {
                            Uri pathuri = Uri.fromFile(file);
                            horizontalList.add(msgItem);
                            imgzoompath.add(path);
                        }
                    }
                }
            } catch (Exception e) {

            }
        }


        //   horizontalAdapter = new HorizontalAdapter(horizontalList);
       /* if (rvMedia != null) {
            LinearLayoutManager mediaManager = new LinearLayoutManager(GroupInfo.this, LinearLayoutManager.HORIZONTAL, false);
            rvMedia.setLayoutManager(mediaManager);

            rvMedia.setAdapter(horizontalAdapter);

        }*/
        int count = horizontalList.size();
        if (mediacount != null) {
            mediacount.setText((String.valueOf(count)));
        }
       /* if (rvMedia != null) {

            rvMedia.addOnItemTouchListener(new RItemAdapter(GroupInfo.this, rvMedia, new RItemAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.picture)) {
                        Intent intent = new Intent(getApplication(), ImageZoom.class);
                        intent.putExtra("from", "media");
                        intent.putExtra("image", imgzoompath.get(position));
                        startActivity(intent);
                    }
                    if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.video)) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(imgzoompath.get(position)));
                        intent.setDataAndType(Uri.parse(imgzoompath.get(position)), "video/*");
                        startActivity(intent);

                    }
                    if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.document)) {
                        DocOpenUtils.openDocument(horizontalList.get(position), GroupInfo.this);
                    } else if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.audio)) {
                        try {
                            File file = new File(imgzoompath.get(position));
                            boolean isExists = file.exists();
                            Log.d(TAG, "onItemClick: " + isExists);
                            Uri uri = Uri.fromFile(file);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            intent.setDataAndType(uri, "audio/*");
                            startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(GroupInfo.this, "No app installed to play this audio", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            MyLog.e(TAG, "onItemClick: ", e);
                        }

                    }
                }

                @Override
                public void onItemLongClick(View view, int position) {

                }
            }));
        }*/
        if (medialayout != null) {

            medialayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(GroupInfo.this, MediaAcitivity.class);
                    intent.putExtra("username", mGroupName);
                    intent.putExtra("docid", mLocDbDocId);
                    startActivity(intent);
                }
            });
            if (horizontalList.size() == 0) {
                media_lineralayout.setVisibility(View.GONE);
            }
        }
    }

    private void getGroupDetails() {
        MyLog.d(TAG, "slowTest getGroupDetails: ");
        if (groupInfoSession.getGroupMembers(mGroupId) != null) {
            final String groupMembersList = groupInfoSession.getGroupMembers(mGroupId);
            try {
                //loadGroupDetails(new JSONObject(groupMembersList));
                new LoadGroupAsyncTask(new JSONObject(groupMembersList)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } catch (Exception e) {
                MyLog.e(TAG, "getGroupDetails: ", e);
            }
/*
                rvGroupMembers.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                         progressBar.setVisibility(View.VISIBLE);

                        progressBar.setVisibility(View.GONE);
                        }catch (Exception e){
                            Log.e(TAG, "getGroupDetails: ", e);
                        }
                    }
                });
*/


        }
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_GROUP_DETAILS);

        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            object.put("convId", mGroupId);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("ImageUri", cameraImageUri.toString());
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnExitGroup:
                if (isLiveGroup) {
                    showExitGroupAlert();
                    exit_delete();
                } else {

                    Toast.makeText(GroupInfo.this, R.string.you_are_not_member, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnDeleteGroup:
                showDeleteGroupAlert();
                break;

            case R.id.tvAddMember:
                if (isLiveGroup) {
                    if (membersCount < 255) {
                        goAddMemberScreen();
                    } else {
                        Toast.makeText(GroupInfo.this, R.string.group_has_max_members, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(GroupInfo.this, R.string.you_are_not_member, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.ivGroupDp:
                String docId = mCurrentUserId.concat("-").concat(mGroupId).concat("-g");
                GroupInfoPojo data = groupInfoSession.getGroupInfo(docId);
                if (data != null && data.getAvatarPath() != null && !data.getAvatarPath().isEmpty()) {
                    Intent imgIntent = new Intent(GroupInfo.this, ImageZoom.class);
                    imgIntent.putExtra("ProfilePath", AppUtils.getValidGroupPath(data.getAvatarPath()));
                    startActivity(imgIntent);
                }
                break;

        }

    }

    public void Addmemeber() {
        if (isLiveGroup) {
            if (membersCount < 255) {
                goAddMemberScreen();
            } else {
                Toast.makeText(GroupInfo.this, R.string.group_has_max_members, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(GroupInfo.this, R.string.you_are_not_member, Toast.LENGTH_SHORT).show();
        }
    }

    private void showGroupDpAlert() {
        List<MultiTextDialogPojo> labelsList = new ArrayList<>();
        MultiTextDialogPojo label = new MultiTextDialogPojo();
        label.setImageResource(R.drawable.blue_camera);
        label.setLabelText("Take Image From Camera");
        labelsList.add(label);

        label = new MultiTextDialogPojo();
        label.setImageResource(R.drawable.gallery);
        label.setLabelText("Add Image From Gallery");
        labelsList.add(label);

        CustomMultiTextItemsDialog dialog = new CustomMultiTextItemsDialog();
        dialog.setTitleText("Profile Picture");
        dialog.setNegativeButtonText("Cancel");
        dialog.setLabelsList(labelsList);

        dialog.setDialogItemClickListener(new CustomMultiTextItemsDialog.DialogItemClickListener() {
            @Override
            public void onDialogItemClick(int position) {
                switch (position) {

                    case 0:
                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                            StrictMode.setVmPolicy(builder.build());
                        }
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File cameraImageOutputFile = new File(
                                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                                createCameraImageFileName());
                        cameraImageUri = Uri.fromFile(cameraImageOutputFile);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri);
                        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                        startActivityForResult(intent, CAMERA_REQUEST_CODE);

                        break;

                    case 1:

                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                        photoPickerIntent.setType("image/*");
                        startActivityForResult(photoPickerIntent, GALLERY_REQUEST_CODE);

                        break;

                }
            }
        });
        dialog.show(getSupportFragmentManager(), "Profile Pic");
    }

    private String createCameraImageFileName() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return timeStamp + ".jpg";
    }

    private void goAddMemberScreen() {
        Intent addMemberIntent = new Intent(GroupInfo.this, AddMemberToGroup.class);
        addMemberIntent.putExtra("GroupId", mGroupId);
        addMemberIntent.putExtra("GroupName", mGroupName);
        addMemberIntent.putExtra("GroupUserIds", groupUserIds);
        startActivityForResult(addMemberIntent, ADD_MEMBER_REQUEST_CODE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GROUP)) {
            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String groupAction = object.getString("groupType");

                if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EXIT_GROUP)) {
                    loadExitMessage(object);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_DELETE_GROUP_MEMBER)) {
                    loadDeleteMemberMessage(object);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_CHANGE_GROUP_DP)) {
                    loadGroupDpChangeMessage(object);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_MAKE_GROUP_ADMIN)) {
                    loadMakeAdminMessage(object);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_CHANGE_GROUP_NAME)) {
                    loadGroupDpChangeName(object);

                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GROUP_DETAILS)) {
            try {
                Object[] array = event.getObjectsArray();
                final JSONObject objects = new JSONObject(array[0].toString());
                if (groupInfoSession.getGroupMembers(mGroupId) == null ||
                        !groupInfoSession.getGroupMembers(mGroupId).equals(objects.toString())) {
                    new LoadGroupAsyncTask(objects).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    groupInfoSession.setGroupMembers(mGroupId, objects);
                }
            } catch (Exception e) {
                MyLog.e(TAG, "onMessageEvent: ", e);
            }

        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_IMAGE_UPLOAD)) {
            loadProfilePicMessage(event);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_PRIVACY_SETTINGS)) {
            loadPrivacySetting(event);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_MUTE)) {
            loadMuteMessage(event);
        }
    }

    private void loadMuteMessage(ReceviceMessageEvent event) {
        try {
            JSONObject object = new JSONObject(event.getObjectsArray()[0].toString());
            String from = object.getString("from");
            String convId = object.getString("convId");

            if (from.equalsIgnoreCase(mCurrentUserId) && convId.equalsIgnoreCase(mGroupId)) {
                String status = object.getString("status");
                if (status.equals("1")) {
                    swMute.setChecked(true);
                } else {
                    swMute.setChecked(false);
                }

                hideProgressDialog();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadPrivacySetting(ReceviceMessageEvent event) {
        adapter.notifyDataSetChanged();
    }

    private void loadMakeAdminMessage(JSONObject object) {

        getGroupDetails();

        try {
            String err = object.getString("err");

            if (err.equalsIgnoreCase("0")) {
                String msg;
                String groupid = object.getString("groupId");
                if (mGroupId.equalsIgnoreCase(groupid)) {
                    String msgId = object.getString("id");
                    String timeStamp = object.getString("timeStamp");
                    String toDocId = object.getString("toDocId");
                    String from = object.getString("from");
                    String admin = object.getString("admin");
                    String adminmsddn = object.getString("newadminmsisdn");
                    String adminuser = object.getString("adminuser");
                  /*  TextMessage message = (TextMessage) MessageFactory.getMessage(MessageFactory.text, this);
                    MessageItemChat item = message.createMessageItem(false, "", MessageFactory.DELIVERY_STATUS_READ,
                            mGroupId, mGroupName);*/
                    String docId = SessionManager.getInstance(GroupInfo.this).getCurrentUserID().concat("-")
                            .concat(mGroupId).concat("-g");
                   /* item.setSenderName(mGroupName);
                    item.setGroupName(mGroupName);
                    item.setMessageId(docId.concat("-").concat(msgId));*/
                    GroupInfoPojo data = groupInfoSession.getGroupInfo(docId);
                    data.setAdminMembers(admin);
                    groupInfoSession.updateGroupInfo(docId, data);
                    // db.updateChatMessage(docId, item);
                    sendGroupAckToServer();


                    Intent s = new Intent();
                    s.setAction("com.group.makeadmin");
                    s.putExtra("object", object.toString());
                    sendBroadcast(s);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadProfilePicMessage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            String err = objects.getString("err");
            String message = objects.getString("message");
            message = "Group Icon Changed Successfully";
            if (err.equalsIgnoreCase("0") && objects.getString("file") != null) {
                String type = objects.getString("type");

                if (type.equalsIgnoreCase("group")) {
                    String from = objects.getString("from");
                    String avatar = objects.getString("file");

                    if (from.equalsIgnoreCase(mCurrentUserId) && type.equalsIgnoreCase("group")) {

                        long ts = Calendar.getInstance().getTimeInMillis();
                        String msgId = mLocDbDocId + "-" + ts;

                        JSONObject object = new JSONObject();
                        try {
                            object.put("groupType", SocketManager.ACTION_CHANGE_GROUP_DP);
                            object.put("from", mCurrentUserId);
                            object.put("groupId", mGroupId);
                            object.put("avatar", avatar);
                            object.put("id", ts);
                            object.put("toDocId", msgId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        SendMessageEvent messageEvent = new SendMessageEvent();
                        messageEvent.setEventName(SocketManager.EVENT_GROUP);
                        messageEvent.setMessageObject(object);
                        EventBus.getDefault().post(messageEvent);


                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadGroupDpChangeName(JSONObject object) {
        try {
            if (object.getString("groupId").equalsIgnoreCase(mGroupId)) {
                mGroupName = object.getString("groupName");
                collapsingToolbarLayout.setTitle(mGroupName);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadGroupDpChangeMessage(JSONObject object) {
        try {
            String err = object.getString("err");
            if (err.equalsIgnoreCase("0")) {
                String from = object.getString("from");
                String message = object.getString("message");
                String groupId = object.getString("groupId");
                String avatar = object.getString("avatar");
                String groupName = object.getString("groupName");
                ChatPageActivity.receiverAvatar = avatar;
                if (groupId.equalsIgnoreCase(mGroupId)) {
                    String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");
                    // For replace exiting path
                    GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);
                    infoPojo.setAvatarPath(avatar);

                    //   AppUtils.loadImage(this,AppUtils.getValidGroupPath(avatar),ivGroupDp);

                    Glide
                            .with(this)
                            .load(AppUtils.getValidGroupPath((avatar)))
                            .asBitmap()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .dontAnimate()
                            .into(new SimpleTarget<Bitmap>() {

                                @Override
                                public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                    // TODO Auto-generated method stub
                                    ivGroupDp.setImageBitmap(arg0);
                                    Toast.makeText(GroupInfo.this, "Group Icon Changed Successfully!", Toast.LENGTH_SHORT).show();

                                }
                            });
                    hideProgressDialog();

                    Intent s = new Intent();
                    s.setAction("com.groupprofile.update");
                    s.putExtra("image", avatar);
                    s.putExtra("object", object.toString());
                    sendBroadcast(s);

                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void loadDeleteMemberMessage(JSONObject object) {
        getGroupDetails();


        try {
            String err = object.getString("err");

            if (err.equalsIgnoreCase("0")) {
                String msg = object.getString("message");
                String msgId = object.getString("id");
                String timeStamp = object.getString("timeStamp");
                String removeId = object.getString("removeId");
                String toDocId = object.getString("toDocId");
                String from = object.getString("from");
                if (removeId.equalsIgnoreCase(mCurrentUserId)) {
                    exit_delete();
                }

               /* TextMessage message = (TextMessage) MessageFactory.getMessage(MessageFactory.text, this);
                MessageItemChat item = message.createMessageItem(false, msg, MessageFactory.DELIVERY_STATUS_READ,
                        mGroupId, mGroupName);
                String docId = SessionManager.getInstance(GroupInfo.this).getCurrentUserID().concat("-")
                        .concat(mGroupId).concat("-g");
                item.setSenderName(mGroupName);
                item.setGroupName(mGroupName);
                item.setMessageId(docId.concat("-").concat(msgId));
                db.updateChatMessage(docId, item);
*/
                // sendGroupAckToServer();

                Intent s = new Intent();
                s.setAction("com.group.delete.members");
                s.putExtra("object", object.toString());
                sendBroadcast(s);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendGroupAckToServer() {
        SendMessageEvent event = new SendMessageEvent();
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            object.put("groupId", mGroupId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadExitMessage(JSONObject object) {
        try {
            String msg = object.getString("message");
            String groupId = object.getString("groupId");
            String timeStamp = object.getString("timeStamp");
            String from = object.getString("from");

            if (mGroupId.equalsIgnoreCase(groupId) && from.equalsIgnoreCase(mCurrentUserId)) {
                removeUser(groupId);

                // For finish ChatViewActivity and go ChatList page
                Intent exitIntent = new Intent();
                exitIntent.putExtra("exitFromGroup", true);
                setResult(RESULT_OK, exitIntent);
                finish();
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void removeUser(String groupId) {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_REMOVE_USER);
        JSONObject object = new JSONObject();
        try {
            object.put("_id", groupId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        messageEvent.setMessageObject(object);
        EventBus.getDefault().post(messageEvent);
    }

    private void loadGroupDetails(JSONObject objects) {

        try {
            MyLog.d(TAG, "slowTest loadGroupDetails: start" + objects);


            String displayName = objects.getString("DisplayName");

            // String groupCreatedAt = objects.getString("GroupcreatedAt");//time stamp
            String displayPic = objects.getString("GroupIcon");
            final String isAdmin = objects.getString("isAdmin");
            String groupId = objects.getString("_id");

            if (groupId.equalsIgnoreCase(mGroupId)) {
                groupUserIds = "";
                mCurrentUserData = null;

                if (displayName != null && !displayName.equals("")) {
                    mGroupName = displayName;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            collapsingToolbarLayout.setTitle(mGroupName);

                        }
                    });
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdmin.equalsIgnoreCase("1") && isLiveGroup) {
                            isAdminUser = true;
                            if (tvAddMember != null)
                                tvAddMember.setVisibility(View.VISIBLE);

                            if (groupMenu != null) {
                                MenuItem menuItem = groupMenu.findItem(R.id.addNewMember);
                                menuItem.setVisible(true);
                                menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                            }
                        } else {
                            if (tvAddMember != null) {
                                tvAddMember.setVisibility(View.GONE);
                            }
                        }
                    }
                });


                allMembersList.clear();
                savedMembersList.clear();
                unsavedMembersList.clear();
                String groupContactNames = "";

                JSONArray arrMembers = objects.getJSONArray("GroupUsers");
                membersCount = arrMembers.length();

                for (int i = 0; i < arrMembers.length(); i++) {
                    JSONObject userObj = arrMembers.getJSONObject(i);
                    String userId = userObj.getString("id");
                    String active = userObj.getString("active");
                    String isDeleted = userObj.getString("isDeleted");
                    String msisdn = userObj.getString("msisdn");
                    String phNumber = userObj.getString("PhNumber");
                    String name = userObj.getString("ContactName");
                    String status = userObj.getString("Status");
                    String userDp = userObj.getString("avatar");
                    String adminUser = userObj.getString("isAdmin");
                    String isPending = userObj.optString("isPending");
                    // String contactmsisdn = userObj.getString("ContactName");
                    //  String isExitsContact = userObj.getString("isExitsContact");
                    String originalName = "";
                    try {
                        if (userObj.has("Name"))
                            originalName = userObj.getString("Name");
                    } catch (Exception e) {
                        MyLog.e(TAG, "loadGroupDetails: ", e);
                        originalName = "";
                    }


                    String contactName = getcontactname.getSendername(userId, msisdn);

                    if (mCurrentUserId.equalsIgnoreCase(userId)) {
                        contactName = "You";
                        status = SessionManager.getInstance(this).getcurrentUserstatus();
                    }

                    groupContactNames = groupContactNames.concat(contactName);

                    GroupMembersPojo data = new GroupMembersPojo();
                    data.setUserId(userId);
                    data.setActive(active);
                    data.setIsDeleted(isDeleted);
                    data.setMsisdn(msisdn);
                    data.setPhNumber(phNumber);
                    data.setName(name);
                    data.setStatus(status);
                    data.setUserDp(userDp);
                    data.setIsAdminUser(adminUser);
                    data.setIsPending(isPending);
                    data.setContactName(contactName);
                    data.setUserOriginalName(originalName);
                    groupUserIds = groupUserIds.concat(userId);
                    if (userId.equalsIgnoreCase(mCurrentUserId)) {
                        mCurrentUserData = data;
                    } else {
                        if (msisdn.equalsIgnoreCase(contactName)) {
                            unsavedMembersList.add(data);
                        } else {
                            savedMembersList.add(data);
                        }
                    }

                    if ((arrMembers.length() - 1) > i) {
                        try {
                            groupUserIds = groupUserIds.concat(",");
                            groupContactNames = groupContactNames.concat(",");
                        } catch (Exception e) {
                        }

                    }
                }

                final String mDocId = mCurrentUserId.concat("-").concat(groupId).concat("-g");
                boolean hasGroupInfo = groupInfoSession.hasGroupInfo(mDocId);
                final GroupInfoPojo infoPojo;
                if (hasGroupInfo) {
                    infoPojo = groupInfoSession.getGroupInfo(mDocId);
                } else {
                    infoPojo = new GroupInfoPojo();
                }
                infoPojo.setGroupId(mGroupId);
                infoPojo.setGroupName(mGroupName);
                infoPojo.setGroupMembers(groupUserIds);
                infoPojo.setAvatarPath(displayPic);
                infoPojo.setIsAdminUser(isAdmin);

                //tvMembersCount.setText(membersCount + "/256");

                //   tvMembersCount.setText(membersCount + "");
                //  tvGroupCreatedInfo.setText("Created by "+ TimeStampUtils.getMessageTStoDate(this,groupCreatedAt));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (tvMembersCount != null) {
                            tvMembersCount.setText(membersCount + "");
                            tvMembersCount.setVisibility(View.VISIBLE);

                        }
                        notifyAdapter(mDocId, infoPojo, true);
                        progressBar.setVisibility(View.GONE);
                    }
                });

                MyLog.d(TAG, "slowTest loadGroupDetails: end");
            }
        } catch (Exception ex) {
            groupInfoSession.setGroupMembers(mGroupId, null);
            ex.printStackTrace();
        }

    }

    private void notifyAdapter(String docId, GroupInfoPojo infoPojo, boolean needUpdate) {
        Collections.sort(savedMembersList, Getcontactname.groupMemberAsc);
        Collections.sort(unsavedMembersList, Getcontactname.groupMemberAsc);
        allMembersList.addAll(savedMembersList);
        if (membersCount > 0)
            allMembersList.addAll(unsavedMembersList);

        if (mCurrentUserData != null) {
            allMembersList.add(mCurrentUserData);
        }

        if (needUpdate) {
            String groupContactNames = "";
            String groupUserIds = "";
            for (int i = 0; i < allMembersList.size(); i++) {
                if (i != 0) {
                    groupContactNames = groupContactNames + ", ";
                    groupUserIds = groupUserIds + ",";
                }
                groupContactNames = groupContactNames + allMembersList.get(i).getContactName();
                groupUserIds = groupUserIds + allMembersList.get(i).getUserId();
            }
            infoPojo.setGroupContactNames(groupContactNames);
            infoPojo.setGroupMembers(groupUserIds);
//            groupInfoSession.updateGroupInfo(docId, infoPojo);
        }


        if (allMembersList.size() > 5) {
          /*  new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {*/
            adapter.updateList(allMembersList);
            adapter.notifyDataSetChanged();
            //adapter = new GroupInfoAdapter(mActivity, GroupInfo.this, allMembersList, getSupportFragmentManager(), GroupInfo.this);
            //rvGroupMembers.setAdapter(adapter);
            //  largeList();
/*

                }
            }, 500);
*/


           /* findViewById(R.id.more_people).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    findViewById(R.id.more_people).setVisibility(View.GONE);
                    adapter = new GroupInfoAdapter(GroupInfo.this, allMembersList,getSupportFragmentManager(),GroupInfo.this);
                    rvGroupMembers.setAdapter(adapter);
                }
            });*/
        } else {
            adapter.updateList(allMembersList);
            adapter.notifyDataSetChanged();
        }
    }

    private void loadgroupdetail() {
        String docId = mCurrentUserId.concat("-").concat(mGroupId).concat("-g");

        boolean hasInfo = groupInfoSession.hasGroupInfo(docId);
        if (hasInfo) {
            GroupInfoPojo data = groupInfoSession.getGroupInfo(docId);
            String displayName = data.getGroupName();
            String displayPic = data.getAvatarPath();
            String isAdmin = data.getIsAdminUser();
            String groupId = data.getGroupId();
            isLiveGroup = data.isLiveGroup();
            MyLog.e("Check with server data", displayPic);

            if (groupId != null && groupId.equalsIgnoreCase(mGroupId)) {
                groupUserIds = "";

                if (displayName != null && !displayName.equals("")) {
                    mGroupName = displayName;
                    collapsingToolbarLayout.setTitle(mGroupName);

                }


                allMembersList.clear();
                savedMembersList.clear();
                unsavedMembersList.clear();

                String groupmembers = data.getGroupMembers();
                String adminuser = data.getAdminMembers();
                String adminmember[] = {};
                if (adminuser != null) {
                    adminmember = adminuser.split(",");
                }
                String array[] = groupmembers.split(",");
                if (groupmembers != null && !groupmembers.isEmpty()) {
                    membersCount = array.length;
                }

                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(GroupInfo.this);

                for (int i = 0; i < array.length; i++) {

                    String userId = null, msisdn = null, phNumber = null, status = null, userDp = null;

                    if (array[i].equalsIgnoreCase(mCurrentUserId)) {

                    } else {
                        SmarttyContactModel userInfo = contactDB_sqlite.getUserOpponenetDetails(array[i]);
                        if (userInfo != null) {
                            userId = userInfo.get_id();
                            msisdn = userInfo.getMsisdn();
                            phNumber = userInfo.getMsisdn();
                            status = userInfo.getStatus();
                            if (SmarttyRegularExp.isEncodedBase64String(status)) {
                                try {
                                    status = new String(Base64.decode(status, Base64.DEFAULT), StandardCharsets.UTF_8);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                            userDp = userInfo.getAvatarImageUrl();
                        }

                        String active = "";
                        String adminUser = "";
                        String contactName = "";
                        String isExitsContact = "";
                        String isDeleted = "";
                        for (int j = 0; j < adminmember.length; j++) {
                            if (array[i].equalsIgnoreCase(adminmember[j])) {
                                active = "";
                                adminUser = "1";
                                isExitsContact = "";
                                isDeleted = "";
                            }

                        }
                        contactName = getcontactname.getSendername(userId, phNumber);

                        if (mCurrentUserId.equalsIgnoreCase(userId)) {
                            contactName = "You";
                            status = SessionManager.getInstance(this).getcurrentUserstatus();
                        }

                        if (adminUser.equalsIgnoreCase("1") && isLiveGroup) {
                            isAdminUser = true;
                            if (mCurrentUserId.equalsIgnoreCase(array[i])) {
                                tvAddMember.setVisibility(View.VISIBLE);
                                if (groupMenu != null) {
                                    MenuItem menuItem = groupMenu.findItem(R.id.addNewMember);
                                    menuItem.setVisible(true);
                                    menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                                }
                            } else {
                                isAdminUser = false;
                                tvAddMember.setVisibility(View.GONE);
                            }

                        }
                        GroupMembersPojo value = new GroupMembersPojo();
                        value.setUserId(userId);
                        value.setActive(active);
                        value.setIsDeleted(isDeleted);
                        value.setMsisdn(msisdn);
                        value.setPhNumber(phNumber);
                        value.setName(contactName);
                        value.setStatus(status);
                        value.setUserDp(userDp);
                        value.setIsAdminUser(adminUser);
                        value.setContactName(contactName);
                        if (userId != null) {
                            groupUserIds = groupUserIds.concat(userId);
                        }

//                        if(userId.equalsIgnoreCase(mCurrentUserId)){
//                            groupUserIds = groupUserIds.concat(mCurrentUserId);
//                        }else{
//
//                        }
                        if (userId != null && userId.equalsIgnoreCase(mCurrentUserId)) {
                            mCurrentUserData = value;
                        } else {
                            if (msisdn != null) {
                                if (msisdn.equalsIgnoreCase(contactName)) {
                                    unsavedMembersList.add(value);
                                } else {
                                    savedMembersList.add(value);
                                }
                            }
                        }
                    }
                }
                //tvMembersCount.setText(membersCount + "/256");
                if (tvMembersCount != null) {
                    tvMembersCount.setText(membersCount + "");
                    tvMembersCount.setVisibility(View.VISIBLE);
                }
                //     tvMembersCount.setText(membersCount + "");
                notifyAdapter(docId, data, false);
            }

        }
    }

    private void performGroupExit() {
        long ts = Calendar.getInstance().getTimeInMillis();
        String msgId = mCurrentUserId + "-" + mGroupId + "-g-" + ts;

        try {
            JSONObject exitObject = new JSONObject();
            exitObject.put("groupType", SocketManager.ACTION_EXIT_GROUP);
            exitObject.put("from", mCurrentUserId);
            exitObject.put("groupId", mGroupId);
            exitObject.put("id", ts);
            exitObject.put("toDocId", msgId);

            SendMessageEvent exitGroupEvent = new SendMessageEvent();
            exitGroupEvent.setEventName(SocketManager.EVENT_GROUP);
            exitGroupEvent.setMessageObject(exitObject);
            EventBus.getDefault().post(exitGroupEvent);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void performGroupExit(String mGroupId) {
        long ts = Calendar.getInstance().getTimeInMillis();
        String msgId = mCurrentUserId + "-" + mGroupId + "-g-" + ts;

        try {
            JSONObject exitObject = new JSONObject();
            exitObject.put("groupType", SocketManager.ACTION_EXIT_GROUP);
            exitObject.put("from", mCurrentUserId);
            exitObject.put("groupId", mGroupId);
            exitObject.put("id", ts);
            exitObject.put("toDocId", msgId);

            SendMessageEvent exitGroupEvent = new SendMessageEvent();
            exitGroupEvent.setEventName(SocketManager.EVENT_GROUP);
            exitGroupEvent.setMessageObject(exitObject);
            EventBus.getDefault().post(exitGroupEvent);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void performDeleteGroup() {
        try {
            JSONObject object = new JSONObject();
            object.put("convId", mGroupId);
            object.put("from", mCurrentUserId);
            object.put("type", MessageFactory.CHAT_TYPE_GROUP);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_DELETE_CHAT);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String docId = mCurrentUserId.concat("-").concat(mGroupId).concat("-g");
        MessageDbController db = CoreController.getDBInstance(this);
        db.deleteChat(docId, MessageFactory.CHAT_TYPE_GROUP);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.group_info, menu);
        this.groupMenu = menu;
        groupMenu.findItem(R.id.addNewMember).setVisible(false);
        if (!isLiveGroup) {
            groupMenu.findItem(R.id.editGroupName).setVisible(false);
            groupMenu.findItem(R.id.changeGroupDp).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.editGroupName:
                if (isLiveGroup) {
                    Intent editNameIntent = new Intent(GroupInfo.this, ChangeGroupName.class);
                    editNameIntent.putExtra("GroupId", mGroupId);
                    editNameIntent.putExtra("GroupName", mGroupName);
                    startActivityForResult(editNameIntent, CHANGE_GROUP_NAME_REQUEST_CODE);
                } else {
                    Toast.makeText(GroupInfo.this, "You are not a member in this group", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.addNewMember:
                goAddMemberScreen();
                break;

            case R.id.changeGroupDp:

                if (isLiveGroup) {
                    showGroupDpAlert();
                } else {
                    Toast.makeText(GroupInfo.this, "You are not a member", Toast.LENGTH_SHORT).show();
                }

                break;

            case android.R.id.home:
              /*  if (session.getmute(mGroupId + "mute")) {
                    swMute.setChecked(false);
                } else {
                    swMute.setChecked(true);
                }*/
                changemute();
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private void changemute() {
        Intent muteintant = new Intent();
        muteintant.putExtra("ismutechange", ismutechange);
        muteintant.putExtra("isgroupempty", isgroupemty);
        setResult(RESULT_OK, muteintant);
    }

    private void showExitGroupAlert() {

        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setNegativeButtonText("Cancel");
        dialog.setPositiveButtonText("Exit");
        dialog.setMessage("Exit " + mGroupName + " group");
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                if (ConnectivityInfo.isInternetConnected(getApplication())) {
                    performGroupExit();
                    /*String docId = mCurrentUserId.concat("-").concat(mGroupId).concat("-g");
                    GroupInfoPojo data = groupInfoSession.getGroupInfo(docId);
                    String memerlist = data.getGroupMembers();
                    String[] useridsplitmemeber = memerlist.split(",");
                    int countofmember = useridsplitmemeber.length;
                    String adminuserid = data.getAdminMembers();
                    String[] useridsplit = adminuserid.split(",");
                    int countofadmin = useridsplit.length;
                    String isadminuser = data.getIsAdminUser();
                    if (countofadmin == 1 && isadminuser.equals("1") && countofmember > 1) {
                        performMakeAdminUser(useridsplitmemeber[0]);
                    }
                    if (memerlist.equals("") || countofmember <= 0) {
                        isgroupemty = true;
                    }*/
                } else {
                    Toast.makeText(GroupInfo.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getSupportFragmentManager(), "Exit group alert");
    }

/*    @Override
    public void onItemClick(View view, int position) {

    }*/

/*    @Override
    public void onItemLongClick(View view, int position) {
       *//* if (isAdminUser && !allMembersList.get(position).getUserId().equalsIgnoreCase(mCurrentUserId)
                && !allMembersList.get(position).getIsAdminUser().equals("1")) {
            showMakeAdminAlert(position);
        }*//*
    }*/


    private void onItemClick(int position) {
        Log.e(TAG, "onItemClick" + "onItemClick" + position);
        try {
            position = position - 4;
            Log.e(TAG, "onItemClick" + "onItemClick" + position);
            if (!allMembersList.get(position).getUserId().equalsIgnoreCase(mCurrentUserId)) {
                //showDeleteMemberAlert(position);
                valueposition = position;
                ShowListdialog(position);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "onItemClick: ", e);
        }
    }

    private void showMakeAdminAlert(final int position) {

        final String msg = "Make " + allMembersList.get(position).getContactName()
                + " as admin in " + mGroupName + " group";

        String isPending = allMembersList.get(position).getIsPending();
        if (isPending.equals("1")) {
            Toast.makeText(GroupInfo.this, "You can't make pending member as admin", Toast.LENGTH_SHORT).show();
            return;
        }

        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setNegativeButtonText("Cancel");
        dialog.setPositiveButtonText("Ok");
        dialog.setMessage(msg);
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                if (ConnectivityInfo.isInternetConnected(getApplication())) {
                    performMakeAdminUser(allMembersList.get(position).getUserId());
                } else {
                    Toast.makeText(GroupInfo.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getSupportFragmentManager(), "Make admin alert");
    }

    private void performMakeAdminUser(String adminUserId) {


        SendMessageEvent makeAdminEvent = new SendMessageEvent();
        makeAdminEvent.setEventName(SocketManager.EVENT_GROUP);

        try {
            String docId = mCurrentUserId.concat("-").concat(mGroupId).concat("-g");
            Calendar calendar = Calendar.getInstance();
            String timeStamp = String.valueOf(calendar.getTimeInMillis());

            JSONObject adminObj = new JSONObject();
            adminObj.put("groupType", SocketManager.ACTION_MAKE_GROUP_ADMIN);
            adminObj.put("from", mCurrentUserId);
            adminObj.put("groupId", mGroupId);
            adminObj.put("adminuser", adminUserId);
            adminObj.put("id", timeStamp);
            adminObj.put("toDocId", docId.concat("-").concat(timeStamp));

            makeAdminEvent.setMessageObject(adminObj);


            /*item.setSenderName(mGroupName);
            item.setGroupName(mGroupName);
            item.setMessageId(docId.concat("-").concat(timeStamp));*/
//            Log.e("<<<<<-------Doc Id--------->>>>>", docId);
//            db.updateChatMessage(message.getId(), item);
            EventBus.getDefault().post(makeAdminEvent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void showDeleteMemberAlert(final int position) {

        String msg = "";

        if (getResources().getBoolean(R.bool.is_arabic)) {
            msg = "هل تريد حذق " + allMembersList.get(position).getContactName()
                    + " من " + mGroupName;
        } else {
            msg = "Remove " + allMembersList.get(position).getContactName()
                    + " from " + mGroupName + " group";
        }

        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setNegativeButtonText(getString(R.string.cancel));
        dialog.setPositiveButtonText(getString(R.string.confirm));
        dialog.setMessage(msg);
        final String finalMsg = msg;
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                if (ConnectivityInfo.isInternetConnected(getApplication())) {
                    if (position > -1) {
                        performDeleteMember(finalMsg, allMembersList.get(position).getUserId());
                    }
                } else {
                    Toast.makeText(GroupInfo.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getSupportFragmentManager(), "Delete member alert");
    }

    /*private void ShowListdialog(final int position) {
        String[] val = {"Message" + "\t" + membersList.get(position).getContactName(), "View" + "\t" + membersList.get(position).getContactName(),
                "Remove" + "\t" + membersList.get(position).getContactName()};
        String[] val2 = {"Message" + "\t" + membersList.get(position).getContactName(), "View" + "\t" + membersList.get(position).getContactName()};

        listDialog = new Dialog(this);
        LayoutInflater li = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = li.inflate(R.layout.navigation_list, null, false);
        listDialog.setContentView(v);
        listDialog.setCancelable(true);


        ListView list1 = (ListView) listDialog.findViewById(R.id.listview);
        if (isAdminUser) {
            list1.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, val));
        } else {
            list1.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, val2));

        }


        listDialog.show();

        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                String name = membersList.get(valueposition).getName();
                contactname = membersList.get(valueposition).getContactName();
                uid = membersList.get(valueposition).getUserId();
                image = membersList.get(valueposition).getUserDp();
                msisdn = membersList.get(valueposition).getMsisdn();

                switch (position) {

                    case 0:
                        Intent intent = new Intent(getApplicationContext(), ChatPageActivity.class);

                        intent.putExtra("msisdn", "");
                        intent.putExtra("Username", contactname);
                        intent.putExtra("documentId", uid);
                        intent.putExtra("receiverUid", "");
                        intent.putExtra("Image", image);
                        intent.putExtra("type", 0);
                        intent.putExtra("receiverName", contactname);
                        intent.putExtra("msisdn", msisdn);
                        startActivity(intent);
                        listDialog.dismiss();
                        break;

                    case 1:
                        Intent infoIntent = new Intent(GroupInfo.this, UserInfo.class);
                        infoIntent.putExtra("UserId", uid);
                        infoIntent.putExtra("UserName", contactname);
                        infoIntent.putExtra("UserAvatar", image);
                        startActivity(infoIntent);
                        listDialog.dismiss();
                        break;

                    case 2:
                        showDeleteMemberAlert(valueposition);
                        listDialog.dismiss();
                        break;

                }
            }
        });
    }*/
    private void ShowListdialog(final int position) {
        List<MultiTextDialogPojo> labelsList = new ArrayList<>();
        MultiTextDialogPojo label = new MultiTextDialogPojo();
        // label.setImageResource(R.drawable.blue_camera);
        label.setLabelText(getString(R.string.message) + "\t" + allMembersList.get(position).getContactName());
        labelsList.add(label);

        label = new MultiTextDialogPojo();
        label.setLabelText(getString(R.string.view) + "\t" + allMembersList.get(position).getContactName());
        labelsList.add(label);

        if (isAdminUser) {

            label = new MultiTextDialogPojo();
            label.setLabelText(getString(R.string.remove) + "\t" + allMembersList.get(position).getContactName());
            labelsList.add(label);

        }
        if (isAdminUser && !allMembersList.get(position).getUserId().equalsIgnoreCase(mCurrentUserId)
                && !allMembersList.get(position).getIsAdminUser().equals("1")) {

            label = new MultiTextDialogPojo();
            label.setLabelText(getString(R.string.make_group_admin));
            labelsList.add(label);

        }
        //unsaved numbers
        String msisdn_ = allMembersList.get(position).getMsisdn();
        if (msisdn_.equals(allMembersList.get(position).getContactName())) {
            label = new MultiTextDialogPojo();
            label.setLabelText(getString(R.string.add_to_contacts));
            labelsList.add(label);
            label = new MultiTextDialogPojo();
            label.setLabelText(getString(R.string.add_to_existing_contact));
            labelsList.add(label);
        }


        dialog = new CustomMultiTextItemsDialog();

        dialog.setLabelsList(labelsList);

        dialog.setDialogItemClickListener(new CustomMultiTextItemsDialog.DialogItemClickListener() {
            @Override
            public void onDialogItemClick(int position) {

                String name = allMembersList.get(valueposition).getName();
                contactname = allMembersList.get(valueposition).getContactName();
                uid = allMembersList.get(valueposition).getUserId();
                image = allMembersList.get(valueposition).getUserDp();
                msisdn = allMembersList.get(valueposition).getMsisdn();

                switch (position) {

                    case 0:
                        ChatLockPojo lockPojo = getChatLockdetailfromDB(position);

                        if (sessionManager.getLockChatEnabled().equals("1") && lockPojo != null) {

                            String stat = lockPojo.getStatus();
                            String pwd = lockPojo.getPassword();

                            String documentid = mCurrentUserId + "-" + uid;
                            if (stat.equals("1")) {
                                openUnlockChatDialog(documentid, stat, pwd, contactname, image, msisdn);
                            } else {
                                navigateTochatviewpage();
                            }
                        } else {
                            navigateTochatviewpage();
                        }
                        break;

                    case 1:
                        Intent infoIntent = new Intent(GroupInfo.this, UserInfo.class);
                        if (contactname == null || contactname.isEmpty())
                            contactname = msisdn;
                        infoIntent.putExtra("UserId", uid);
                        infoIntent.putExtra("UserName", contactname);
                        infoIntent.putExtra("UserAvatar", image);
                        infoIntent.putExtra("UserNumber", msisdn);
                        infoIntent.putExtra("FromSecretChat", false);
                        startActivity(infoIntent);
                        break;

                    case 2:
                        if (isAdminUser)
                            showDeleteMemberAlert(valueposition);
                        else
                            addToContact(msisdn, valueposition);
                        break;

                    case 3:
                        if (isAdminUser)
                            // showMakeAdminAlert(valueposition);
                            showMakeAdminAlert(valueposition);

                        else
                            addToExistingContact(msisdn, valueposition);
                        break;
                    case 4:

/*                        if(isAdminUser){

                            addToExistingContact(msisdn,valueposition);

                        }else{

                        }*/
                        addToContact(msisdn, valueposition);

                        break;
                    case 5:
                        addToExistingContact(msisdn, valueposition);
                        break;

                }
            }
        });
        dialog.show(getSupportFragmentManager(), "Profile Pic");
    }

    public void addToContact(String msisdn, int index) {
        savedNumber = msisdn;
        savedNumberIndex = index;
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        //intent.putExtra(INTENT_KEY_FINISH_ACTIVITY_ON_SAVE_COMPLETED, true);
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, msisdn);
        startActivityForResult(intent, ADD_CONTACT);
    }

    public void media(final LinearLayout media_lineralayout, RelativeLayout medialayout, TextView mediacount) {
        this.media_lineralayout = media_lineralayout;
        this.medialayout = medialayout;
        this.mediacount = mediacount;

    }

    public void footerinit(final AvnNextLTProDemiButton btnDeleteGroup, AvnNextLTProDemiButton btnExitGroup) {
        this.btnDeleteGroup = btnDeleteGroup;
        this.btnExitGroup = btnExitGroup;
        btnExitGroup.setOnClickListener(GroupInfo.this);
        btnDeleteGroup.setOnClickListener(GroupInfo.this);


    }

    public void groupmemberinit(AvnNextLTProDemiTextView tvParticipantTitle, AvnNextLTProRegTextView groupempty, AvnNextLTProRegTextView tvMembersCount, AvnNextLTProRegTextView tvAddMember, LinearLayout media_lineralayout) {
        this.tvParticipantTitle = tvParticipantTitle;
        this.groupempty = groupempty;
        this.tvMembersCount = tvMembersCount;
        this.tvAddMember = tvAddMember;
        this.media_lineralayout = media_lineralayout;
        tvAddMember.setOnClickListener(GroupInfo.this);

        if (tvMembersCount != null) {
            tvMembersCount.setText(membersCount + "");
            tvMembersCount.setVisibility(View.VISIBLE);
        }
        if (isAdminUser) {
            tvAddMember.setVisibility(View.VISIBLE);
        }
    }

    public void rvMediainit(final RecyclerView rvMedia) {

        Log.e("onBindViewHolder", "rvMediainit");

        this.rvMedia = rvMedia;


        if (horizontalAdapter == null) {
            horizontalAdapter = new HorizontalAdapter(horizontalList);
            LinearLayoutManager mediaManager = new LinearLayoutManager(GroupInfo.this, LinearLayoutManager.HORIZONTAL, false);
            rvMedia.setLayoutManager(mediaManager);
            rvMedia.setAdapter(horizontalAdapter);
        }
        if (rvMedia != null) {
            //    LinearLayoutManager mediaManager = new LinearLayoutManager(GroupInfo.this, LinearLayoutManager.HORIZONTAL, false);
            //   rvMedia.setLayoutManager(mediaManager);
            //   rvMedia.setAdapter(horizontalAdapter);
            horizontalAdapter.notifyDataSetChanged();
        }


        if (rvMedia != null) {

            rvMedia.addOnItemTouchListener(new RItemAdapter(GroupInfo.this, rvMedia, new RItemAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    long currentMillis = System.currentTimeMillis();
                    long diffMillis = currentMillis - lastClickTime;
                    if (diffMillis > 1000) {
                        lastClickTime = currentMillis;
                        mediaClick(position);
                    }
                }

                @Override
                public void onItemLongClick(View view, int position) {

                }
            }));
        }

        if (medialayout != null) {
            Log.e("rvMediainit", "medialayout");
            medialayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(GroupInfo.this, MediaAcitivity.class);
                    intent.putExtra("username", mGroupName);
                    intent.putExtra("docid", mLocDbDocId);
                    startActivity(intent);
                }
            });
            if (horizontalList.size() == 0) {
                media_lineralayout.setVisibility(View.GONE);
            }
        }
      /*  mediafile();
        if (horizontalList.size() > 0) {
            if (media_lineralayout!=null)
                media_lineralayout.setVisibility(View.VISIBLE);
        }*/
    }

    private void mediaClick(int position) {
        Log.e("onItemClick", "position" + position);

        boolean mload = false;
        if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.picture)) {
            mload = true;
            Log.e("rvMediainit", "ImageZoom");
            if (mload) {
                Intent intent = new Intent(getApplication(), ImageZoom.class);
                intent.putExtra("from", "media");
                intent.putExtra("image", imgzoompath.get(position));
                startActivity(intent);

            }
        }
        if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.video)) {
            Log.e("rvMediainit", "video");
            mload = true;

            if (mload) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(imgzoompath.get(position)));
                intent.setDataAndType(Uri.parse(imgzoompath.get(position)), "video/*");
                startActivity(intent);
            }
        }
        if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.document)) {
            mload = true;

            if (mload) {
                DocOpenUtils.openDocument(horizontalList.get(position), GroupInfo.this);
            }
        } else if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.audio)) {
            try {
                Log.e("rvMediainit", "audio");
                mload = true;

                if (mload) {

                    File file = new File(imgzoompath.get(position));

                    boolean isExists = file.exists();
                    Log.d(TAG, "onItemClick: " + isExists);
                    Uri uri = Uri.fromFile(file);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.setDataAndType(uri, "audio/*");
                    startActivity(intent);
                }
            } catch (ActivityNotFoundException e) {
                Toast.makeText(GroupInfo.this, "No app installed to play this audio", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                MyLog.e(TAG, "onItemClick: ", e);
            }

        }
    }

    public void mutecheckbox(final SwitchButton swMute, AvnNextLTProDemiTextView mute) {
        this.swMute = swMute;
        this.mute = mute;
        mute_gone_exit();
        swMute.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (ConnectivityInfo.isInternetConnected(GroupInfo.this)) {
                    if (isChecked) {
                        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(GroupInfo.this);
                        MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(mCurrentUserId, null, mGroupId, false);
//Check  mute status is empty assign to 0

                        if (AppUtils.isEmpty(muteData.getMuteStatus())) {

                            //      if(ismutecheckchange)

                            muteData.setMuteStatus("0");

                        }
                        if (muteData == null || muteData.getMuteStatus().equals("0")) {
                            MuteUserPojo muteUserPojo = new MuteUserPojo();
                            muteUserPojo.setReceiverId(mGroupId);
                            muteUserPojo.setSecretType("no");
                            muteUserPojo.setChatType(MessageFactory.CHAT_TYPE_GROUP);

                            ArrayList<MuteUserPojo> muteUserList = new ArrayList<>();
                            muteUserList.add(muteUserPojo);

                            Bundle putBundle = new Bundle();
                            putBundle.putSerializable("MuteUserList", muteUserList);

                            MuteAlertDialog dialog = new MuteAlertDialog();
                            dialog.setArguments(putBundle);
                            dialog.setCancelable(false);
                            dialog.setMuteAlertCloseListener(GroupInfo.this);
                            dialog.show(getSupportFragmentManager(), "Mute");
                        }
                    } else {
                        MuteUnmute.performUnMute(GroupInfo.this, EventBus.getDefault(), mGroupId,
                                MessageFactory.CHAT_TYPE_GROUP, "no");

                        ismutechange = true;

                        showProgressDialog();
                    }
                } else {
                    swMute.setChecked(!isChecked);
                    Toast.makeText(GroupInfo.this, "Check Your Network Connection", Toast.LENGTH_SHORT).show();
                }
            }

        });
    }

    public void addToExistingContact(String msisdn, int index) {
        savedNumber = msisdn;
        savedNumberIndex = index;
        Intent intent = new Intent(Intent.ACTION_INSERT_OR_EDIT);
        //intent.putExtra(INTENT_KEY_FINISH_ACTIVITY_ON_SAVE_COMPLETED, true);
        //intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        intent.setType(ContactsContract.Contacts.CONTENT_ITEM_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, msisdn);
        startActivityForResult(intent, ADD_CONTACT);
    }

    public void openUnlockChatDialog(String documentid, String stat, String pwd, String contactname, String image, String msisdn) {

        String convId = userInfoSession.getChatConvId(documentid);
        ChatLockPwdDialog dialog = new ChatLockPwdDialog();
        dialog.setTextLabel1("Enter your Password");
        dialog.setEditTextdata("Enter new Password");
        dialog.setforgotpwdlabel("Forgot Password");
        dialog.setHeader("Unlock Chat");
        dialog.setButtonText("Unlock");
        Bundle bundle = new Bundle();
        bundle.putString("convID", convId);
        bundle.putString("status", "1");
        bundle.putString("pwd", pwd);
        bundle.putString("contactName", contactname);
        bundle.putString("avatar", image);
        bundle.putString("msisdn", msisdn);
        String uid = documentid.split("-")[1];
        bundle.putString("docid", uid);
        bundle.putString("page", "chatlist");
        bundle.putString("type", "single");
        bundle.putString("from", mCurrentUserId);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "chatunLock");
    }

    private void navigateTochatviewpage() {
        Intent intent = new Intent(getApplicationContext(), ChatPageActivity.class);
        if (contactname == null || contactname.isEmpty())
            contactname = msisdn;
        intent.putExtra("msisdn", "");
        intent.putExtra("Username", contactname);
        intent.putExtra("documentId", uid);
        intent.putExtra("receiverUid", "");
        intent.putExtra("Image", image);
        intent.putExtra("type", 0);
        intent.putExtra("receiverName", contactname);
        intent.putExtra("msisdn", msisdn);
        startActivity(intent);
    }

    private void performDeleteMember(String msg, String deleteUserId) {
        SendMessageEvent exitGroupEvent = new SendMessageEvent();
        exitGroupEvent.setEventName(SocketManager.EVENT_GROUP);

        try {
            String docId = mCurrentUserId.concat("-").concat(mGroupId).concat("-g");
            long ts = Calendar.getInstance().getTimeInMillis();

            JSONObject deleteObj = new JSONObject();
            deleteObj.put("groupType", SocketManager.ACTION_DELETE_GROUP_MEMBER);
            deleteObj.put("from", mCurrentUserId);
            deleteObj.put("groupId", mGroupId);
            deleteObj.put("removeId", deleteUserId);
            deleteObj.put("id", ts);
            deleteObj.put("toDocId", docId.concat("-").concat(ts + ""));

            exitGroupEvent.setMessageObject(deleteObj);
            EventBus.getDefault().post(exitGroupEvent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_MEMBER_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            boolean isMemberAdded = data.getBooleanExtra("MemberAdded", false);
            if (isMemberAdded) {
                getGroupDetails();
            }
        } else if (requestCode == CHANGE_GROUP_NAME_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            boolean isNameChanged = data.getBooleanExtra("NameChanged", false);
            if (isNameChanged) {
                String newGroupName = data.getStringExtra("newGroupName");
                Toast.makeText(GroupInfo.this, "Group name changed to " + newGroupName, Toast.LENGTH_SHORT).show();
                getGroupDetails();


            }
        } else if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    if (data != null) {
                        Uri selectedImageUri = data.getData();
                        if (ConnectivityInfo.isInternetConnected(getApplication())) {
                            beginCrop(selectedImageUri);
                        } else {
                            Toast.makeText(GroupInfo.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (OutOfMemoryError | NullPointerException e) {
                }
            } else {
                if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    Toast.makeText(this, "Sorry! Failed to capture image",
                            Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                /*if (data != null) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    Uri tempUri = getImageUri(getApplicationContext(), photo);
                    beginCrop(tempUri);
                }*/
                if (ConnectivityInfo.isInternetConnected(getApplication())) {
                    beginCrop(cameraImageUri);
                } else {
                    Toast.makeText(GroupInfo.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    Toast.makeText(this, "Sorry! Failed to capture image",
                            Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            Uri uri = Crop.getOutput(data);
            String filePath = uri.getPath();

            File image = new File(filePath);
            Bitmap compressBmp = Compressor.getDefault(GroupInfo.this).compressToBitmap(image);
            uploadImage(compressBmp);

        } else if (requestCode == ADD_CONTACT) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SmarttyContactsService.savedNumber = savedNumber;
                    SmarttyContactsService.bindContactService(GroupInfo.this, false);
                    SmarttyContactsService.setBroadCastSavedName(new SmarttyContactsService.BroadCastSavedName() {
                        @Override
                        public void savedName(final String name) {
                            allMembersList.get(savedNumberIndex).setName(name);
                            allMembersList.get(savedNumberIndex).setContactName(name);
                            updateAdapter();

                        }
                    });


                }
            });


        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void uploadImage(Bitmap circleBmp) {

        if (circleBmp != null) {
            try {
                File imgDir = new File(MessageFactory.PROFILE_IMAGE_PATH);
                if (!imgDir.exists()) {
                    imgDir.mkdirs();
                }
                showProgressDialog();
                String profileImgPath = imgDir.getPath() + "/" + Calendar.getInstance().getTimeInMillis() + "_pro.jpg";

                File file = new File(profileImgPath);
                if (file.exists()) {
                    file.delete();
                }
                try {
                    file.createNewFile();

                } catch (Exception e) {
                    File myDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), getString(R.string.app_name));
                    myDirectory.mkdir();
                    profileImgPath = myDirectory.getPath() + "/" + Calendar.getInstance().getTimeInMillis() + "_pro.jpg";
                    file = new File(profileImgPath);
                    boolean isCreated = file.createNewFile();
                    MyLog.d(TAG, "uploadImage: " + isCreated);
                }

                OutputStream outStream = new FileOutputStream(file);
                circleBmp.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.flush();
                outStream.close();

                String serverFileName = mCurrentUserId + "-" + mGroupId + "-g-"
                        + Calendar.getInstance().getTimeInMillis() + ".jpg";

                PictureMessage message = new PictureMessage(GroupInfo.this);
                JSONObject object = (JSONObject) message.createGroupProfileImageObject(serverFileName, profileImgPath);
                FileUploadDownloadManager fileUploadDownloadMgnr = new FileUploadDownloadManager(GroupInfo.this);
                Log.d(TAG, "onClick: startFileUpload8");
                fileUploadDownloadMgnr.startFileUpload(EventBus.getDefault(), object);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(GroupInfo.this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(GroupInfo.this);
    }

    private void exit_delete() {
        boolean hasGroupInfo = groupInfoSession.hasGroupInfo(mCurrentUserId + "-" + mGroupId + "-g");
        GroupInfoPojo infoPojo;
        if (hasGroupInfo) {
            infoPojo = groupInfoSession.getGroupInfo(mCurrentUserId + "-" + mGroupId + "-g");
            if (infoPojo.isLiveGroup()) {
                //    btnExitGroup.setVisibility(View.VISIBLE);
                //    btnDeleteGroup.setVisibility(View.GONE);
            } else {
                //    btnExitGroup.setVisibility(View.GONE);
                //   btnDeleteGroup.setVisibility(View.VISIBLE);
            }
        }
    }

    private void showDeleteGroupAlert() {

        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setNegativeButtonText("Cancel");
        dialog.setPositiveButtonText("Delete");
        dialog.setMessage("Delete " + mGroupName + " group");
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                performDeleteGroup();
                Intent intent = new Intent(GroupInfo.this, NewHomeScreenActivty.class);
                startActivity(intent);
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getSupportFragmentManager(), "Delete group alert");
    }

    @Override
    public void onMuteDialogClosed(boolean isMuted) {
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(mCurrentUserId, null, mGroupId, false);

        swMute.setChecked((muteData != null && muteData.getMuteStatus().equals("1")));
    }

    @Override
    public void onItemClick(View view, int position) {
        onItemClick(position);
    }

    @Override
    public void onItemLongClick(View view, int position) {

    }

    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        int hours = (int) (millis / (1000 * 60 * 60));
        int minutes = (int) ((millis % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (int) (((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000);

        buf

                .append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }

    private void mute_gone_exit() {
        if (isLiveGroup) {

            swMute.setVisibility(View.VISIBLE);
            mute.setText(getString(R.string.mute_notifications));
        } else {
            swMute.setVisibility(View.GONE);
            mute.setText(getString(R.string.no_longer_participant_in_group));
        }
    }

    private ChatLockPojo getChatLockdetailfromDB(int position) {
        String id = mCurrentUserId.concat("-").concat(uid);
        MessageDbController dbController = CoreController.getDBInstance(this);
        String convId = userInfoSession.getChatConvId(id);
        String receiverId = userInfoSession.getReceiverIdByConvId(convId);
        ChatLockPojo pojo = dbController.getChatLockData(receiverId, MessageFactory.CHAT_TYPE_SINGLE);
        return pojo;
    }

    private void largeList() {
        View groupDeleteLayout = findViewById(R.id.group_delete_exit_layout);
        View topLayout = findViewById(R.id.top_layout);

        RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params2.removeRule(RelativeLayout.BELOW);
        params2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        groupDeleteLayout.setLayoutParams(params2);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ABOVE, R.id.group_delete_exit_layout);
        topLayout.setLayoutParams(params);


    }

    private class LoadGroupAsyncTask extends AsyncTask<Void, Void, Void> {
        JSONObject object;

        public LoadGroupAsyncTask(JSONObject object) {
            this.object = object;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            loadGroupDetails(object);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }
    }

    public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {

        private List<MessageItemChat> horizontalList;

        public HorizontalAdapter(List<MessageItemChat> horizontalList) {
            this.horizontalList = horizontalList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.horizontal_item_view, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            holder.Vido.setVisibility(View.GONE);
            holder.duration.setVisibility(View.GONE);
            holder.durationBg.setVisibility(View.GONE);

            if (horizontalList.size() < 5) {

                if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.audio)) {
                    if (horizontalList.get(position).getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                        holder.Vido.setVisibility(View.GONE);
                        holder.duration.setVisibility(View.VISIBLE);
                        holder.durationBg.setVisibility(View.VISIBLE);
                        holder.image.setImageResource(R.drawable.ic_media_audio);
                        String path = horizontalList.get(position).getChatFileLocalPath();
                        File file = new File(path);
                        String duration = "";
                        if (file.exists()) {
                            duration = horizontalList.get(position).getDuration();
                        }
                        holder.duration.setText(duration);
                    } else if (horizontalList.get(position).getaudiotype() == MessageFactory.AUDIO_FROM_ATTACHMENT) {
                        holder.Vido.setVisibility(View.GONE);
                        holder.duration.setVisibility(View.VISIBLE);
                        holder.durationBg.setVisibility(View.VISIBLE);
                        holder.image.setImageResource(R.drawable.ic_media_audio);
                        String path = horizontalList.get(position).getChatFileLocalPath();
                        File file = new File(path);
                        String duration = "";
                        if (file.exists()) {
                            duration = horizontalList.get(position).getDuration();
                        }
                        holder.duration.setText(duration);
                    }
                }
                if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.picture)) {
                    if (horizontalList.get(position).getImagePath() != null) {
                        String path = horizontalList.get(position).getImagePath();
                        File file = new File(path);
                        if (file.exists()) {

                            AppUtils.loadLocalImage(GroupInfo.this, path, holder.image, 100, 100);
                        }
                    } else if (horizontalList.get(position).getChatFileLocalPath() != null) {
                        String path = horizontalList.get(position).getChatFileLocalPath();
                        File file = new File(path);
                        if (file.exists()) {
                            AppUtils.loadLocalImage(GroupInfo.this, path, holder.image, 100, 100);
                        }
                    }

                }

                if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.video)) {

                    if (horizontalList.get(position).getVideoPath() != null) {
                        String path = horizontalList.get(position).getVideoPath();
                        File file = new File(path);
                        if (file.exists()) {
                            AppUtils.loadLocalVideoThumbanail(GroupInfo.this, path, holder.image);

                            MediaMetadataRetriever mdr = new MediaMetadataRetriever();
                            mdr.setDataSource(path);
                            String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                            String setduration = getTimeString(AppUtils.parseLong(duration));
                            holder.Vido.setVisibility(View.VISIBLE);
                            holder.duration.setVisibility(View.VISIBLE);
                            holder.durationBg.setVisibility(View.VISIBLE);
                            holder.duration.setText(setduration);
                        }

                    } else if (horizontalList.get(position).getChatFileLocalPath() != null) {
                        String path = horizontalList.get(position).getChatFileLocalPath();
                        File file = new File(path);
                        if (file.exists()) {
                            /*Bitmap thumbBmp = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MICRO_KIND);
                            if(thumbBmp != null){
                                Bitmap bitmap = Bitmap.createScaledBitmap(thumbBmp, 128, 128, true);
                                holder.image.setImageBitmap(bitmap);
                            }*/
                            AppUtils.loadLocalVideoThumbanail(GroupInfo.this, path, holder.image);

                            MediaMetadataRetriever mdr = new MediaMetadataRetriever();
                            mdr.setDataSource(horizontalList.get(position).getChatFileLocalPath());
                            String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                            String setduration = getTimeString(AppUtils.parseLong(duration));
                            holder.Vido.setVisibility(View.VISIBLE);
                            holder.duration.setVisibility(View.VISIBLE);
                            holder.durationBg.setVisibility(View.VISIBLE);
                            holder.duration.setText(setduration);
                        }

                    }


                } else if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.document)) {
                    holder.Vido.setVisibility(View.GONE);
                    holder.duration.setVisibility(View.GONE);
                    holder.durationBg.setVisibility(View.GONE);
                    String path = horizontalList.get(position).getChatFileLocalPath();
                    String extension = FileUploadDownloadManager.getFileExtnFromPath(path);
                    if (extension.contains("txt")) {
                        holder.image.setImageResource(R.drawable.ic_media_txt);
                    } else if (extension.contains("doc")) {
                        holder.image.setImageResource(R.drawable.ic_media_doc);
                    } else if (extension.contains("ppt")) {
                        holder.image.setImageResource(R.drawable.ic_media_ppt);
                    } else if (extension.contains("xls")) {
                        holder.image.setImageResource(R.drawable.ic_media_xls);
                    } else if (extension.contains("pdf")) {
                        holder.image.setImageResource(R.drawable.ic_media_pdf);
                    }


                }

            } else if (horizontalList.size() >= 5) {

                if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.audio)) {
                    if (horizontalList.get(position).getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                        holder.Vido.setVisibility(View.GONE);
                        holder.duration.setVisibility(View.VISIBLE);
                        holder.durationBg.setVisibility(View.VISIBLE);
                        holder.image.setImageResource(R.drawable.ic_media_audio);
                        String path = horizontalList.get(position).getChatFileLocalPath();
                        File file = new File(path);
                        String duration = "";
                        if (file.exists()) {
                            duration = horizontalList.get(position).getDuration();
                        }
                        holder.duration.setText(duration);
                    } else if (horizontalList.get(position).getaudiotype() == MessageFactory.AUDIO_FROM_ATTACHMENT) {
                        holder.Vido.setVisibility(View.GONE);
                        holder.duration.setVisibility(View.VISIBLE);
                        holder.durationBg.setVisibility(View.VISIBLE);
                        holder.image.setImageResource(R.drawable.ic_media_audio);
                        String path = horizontalList.get(position).getChatFileLocalPath();
                        File file = new File(path);
                        String duration = "";
                        if (file.exists()) {
                            duration = horizontalList.get(position).getDuration();
                        }
                        holder.duration.setText(duration);
                    }
                }
                if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.picture)) {
                    if (horizontalList.get(position).getImagePath() != null) {
                        String path = horizontalList.get(position).getImagePath();
                        File file = new File(path);
                        if (file.exists()) {
/*                            Bitmap myBitmap = BitmapFactory.decodeFile(path);
                            Bitmap bitmap = Bitmap.createScaledBitmap(myBitmap, 128, 128, true);
                            holder.image.setImageBitmap(bitmap);*/
                            AppUtils.loadLocalImage(GroupInfo.this, path, holder.image, 100, 100);
                        }
                    } else if (horizontalList.get(position).getChatFileLocalPath() != null) {
                        String path = horizontalList.get(position).getChatFileLocalPath();
                        File file = new File(path);
                        if (file.exists()) {
/*                            Bitmap myBitmap = BitmapFactory.decodeFile(path);
                            Bitmap bitmap = Bitmap.createScaledBitmap(myBitmap, 128, 128, true);
                            holder.image.setImageBitmap(bitmap);
                            holder.image.setImageBitmap(myBitmap);*/
                            AppUtils.loadLocalImage(GroupInfo.this, path, holder.image, 100, 100);
                        }
                    }

                }

                if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.video)) {

                    if (horizontalList.get(position).getVideoPath() != null) {
                        String path = horizontalList.get(position).getVideoPath();
                        File file = new File(path);
                        if (file.exists()) {
/*                            Bitmap thumbBmp = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MICRO_KIND);
                            Bitmap bitmap = Bitmap.createScaledBitmap(thumbBmp, 128, 128, true);
                            holder.image.setImageBitmap(bitmap);*/
                            AppUtils.loadLocalImage(GroupInfo.this, path, holder.image, 100, 100);
                            MediaMetadataRetriever mdr = new MediaMetadataRetriever();
                            mdr.setDataSource(horizontalList.get(position).getChatFileLocalPath());
                            String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                            String setduration = getTimeString(AppUtils.parseLong(duration));
                            holder.Vido.setVisibility(View.VISIBLE);
                            holder.duration.setVisibility(View.VISIBLE);
                            holder.durationBg.setVisibility(View.VISIBLE);
                            holder.duration.setText(setduration);
                        }

                    } else if (horizontalList.get(position).getChatFileLocalPath() != null) {
                        String path = horizontalList.get(position).getChatFileLocalPath();
                        File file = new File(path);
                        if (file.exists()) {
/*                            Bitmap thumbBmp = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MICRO_KIND);
                            Bitmap bitmap = Bitmap.createScaledBitmap(thumbBmp, 128, 128, true);
                            holder.image.setImageBitmap(bitmap);*/
                            AppUtils.loadLocalImage(GroupInfo.this, path, holder.image, 100, 100);
                            MediaMetadataRetriever mdr = new MediaMetadataRetriever();
                            mdr.setDataSource(horizontalList.get(position).getChatFileLocalPath());
                            String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                            String setduration = getTimeString(AppUtils.parseLong(duration));
                            holder.Vido.setVisibility(View.VISIBLE);
                            holder.duration.setVisibility(View.VISIBLE);
                            holder.durationBg.setVisibility(View.VISIBLE);
                            holder.duration.setText(setduration);
                        }

                    } else if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.document)) {
                        holder.Vido.setVisibility(View.GONE);
                        holder.duration.setVisibility(View.GONE);
                        holder.durationBg.setVisibility(View.GONE);
                        String path = horizontalList.get(position).getChatFileLocalPath();
                        String extension = FileUploadDownloadManager.getFileExtnFromPath(path);
                        if (extension.contains("txt")) {
                            holder.image.setImageResource(R.drawable.ic_media_txt);
                        } else if (extension.contains("doc")) {
                            holder.image.setImageResource(R.drawable.ic_media_doc);
                        } else if (extension.contains("ppt")) {
                            holder.image.setImageResource(R.drawable.ic_media_ppt);
                        } else if (extension.contains("xls")) {
                            holder.image.setImageResource(R.drawable.ic_media_xls);
                        } else if (extension.contains("pdf")) {
                            holder.image.setImageResource(R.drawable.ic_media_pdf);
                        }


                    }
                }
                if (horizontalList.size() - 1 == position) {
                    holder.arrow.setVisibility(View.VISIBLE);
                } else {
                    holder.arrow.setVisibility(View.GONE);
                }
            }

            holder.arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(GroupInfo.this, MediaAcitivity.class);
                    intent.putExtra("username", mGroupName);
                    intent.putExtra("docid", mLocDbDocId);
                    startActivity(intent);
                    // Toast.makeText(UserInfo.this,holder.txtView.getText().toString(),Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return horizontalList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView image, arrow, Vido;
            public AvnNextLTProDemiTextView duration;
            public View durationBg;

            public MyViewHolder(View view) {
                super(view);
                image = view.findViewById(R.id.Image);
                arrow = view.findViewById(R.id.arrow);
                Vido = view.findViewById(R.id.Vido);
                duration = view.findViewById(R.id.duration);
                durationBg = view.findViewById(R.id.duration_background);
            }
        }
    }

}
