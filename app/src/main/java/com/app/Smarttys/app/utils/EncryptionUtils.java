package com.app.Smarttys.app.utils;

import android.content.Context;
import android.util.Base64;

import com.app.Smarttys.app.encryption.RSAUtil;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;

import org.json.JSONObject;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user134 on 6/19/2018.
 */

public class EncryptionUtils {
    private static final String TAG = "EncryptionUtils";

    public static String getScMessageResponseDecrypted(String msg, List<String> scMessageResponseJsonKeysList, Context context) {
        String messageAfterDecrypt = msg;
        try {
            String securityToken = SessionManager.getInstance(context).getSecurityToken();
            RSAUtil rsaUtil = new RSAUtil();
            String publicKeyString = SessionManager.getInstance(context).getPublicEncryptionKey();
            String privateKeyString = SessionManager.getInstance(context).getPrivateEncryptionKey();
            rsaUtil.init(publicKeyString, privateKeyString);
            messageAfterDecrypt = CryptLibDecryption.decrypt(securityToken, messageAfterDecrypt);
            JSONObject jsonObject = new JSONObject(messageAfterDecrypt);
            String toUserId = jsonObject.getString("to");
            ContactDB_Sqlite contactDB_sqlite = new ContactDB_Sqlite(context);
            String userToken = contactDB_sqlite.getSecurityToken(toUserId);
            String encodedToken = AppUtils.SHA256(userToken);
            userToken = encodedToken.replace("\n", "");
            //userToken=convertHexToBase64String(userToken);
            MyLog.d(TAG, "getScMessageResponseDecrypted: " + userToken);
            JSONObject dataObj = jsonObject.getJSONObject("data");
            for (String key : scMessageResponseJsonKeysList) {
                if (dataObj.has(key)) {
                    try {
                        String encryptedTxt = dataObj.getString(key);
                        //  encryptedTxt=encryptedTxt.replace("\n","");
                        String decryptedTest = CryptLibDecryption.decrypt(userToken, encryptedTxt);
                        MyLog.d(TAG, "<<<getDecryptedMessage decryptedTest: " + decryptedTest);
                        String decryptedTest2 = rsaUtil.getDecryptedMsg(decryptedTest);
                        MyLog.d(TAG, "<<<getDecryptedMessage decryptedTest2: " + decryptedTest2);
                        if (decryptedTest2 == null) {
                            decryptedTest2 = encryptedTxt;
                        }
                        dataObj.put(key, decryptedTest2);
                    } catch (Exception e) {
                        MyLog.e(TAG, "getScMessageResponseDecrypted: ", e);
                    }
                }
            }
            jsonObject.put("data", dataObj);

            messageAfterDecrypt = jsonObject.toString();
        } catch (Exception e) {
            MyLog.e(TAG, "getScMessageResponseDecrypted: ", e);
        }
        return messageAfterDecrypt;
    }


    public static String getCommonDecryptedMessage(String msg, String userIdKey, List<String> scMessageResponseJsonKeysList, Context context) {
        String messageAfterDecrypt = msg;
        try {
            String iv = "chat";
            String securityToken = SessionManager.getInstance(context).getSecurityToken();
            RSAUtil rsaUtil = new RSAUtil();
            String publicKeyString = SessionManager.getInstance(context).getPublicEncryptionKey();
            String privateKeyString = SessionManager.getInstance(context).getPrivateEncryptionKey();
            rsaUtil.init(publicKeyString, privateKeyString);
            //messageAfterDecrypt = CryptLibDecryption.decrypt(securityToken, messageAfterDecrypt);
            JSONObject jsonObject = new JSONObject(messageAfterDecrypt);
            String toUserId = jsonObject.getString(userIdKey);

            ContactDB_Sqlite contactDB_sqlite = new ContactDB_Sqlite(context);
            String userToken = contactDB_sqlite.getSecurityToken(toUserId);

            //userToken=convertHexToBase64String(userToken);
            MyLog.d(TAG, "getScMessageResponseDecrypted: " + userToken);
            String encodedToken = AppUtils.SHA256(userToken);
            userToken = encodedToken.replace("\n", "");

            for (String key : scMessageResponseJsonKeysList) {
                if (jsonObject.has(key)) {
                    try {
                        String encryptedTxt = jsonObject.getString(key);
                        //  encryptedTxt=encryptedTxt.replace("\n","");
                        String decryptedTest = CryptLibDecryption.decrypt(userToken, encryptedTxt);
/*                        CryptLib cryptLib=new CryptLib();
                        String decryptedTest = cryptLib.decryptCipherText(encryptedTxt,userToken,iv );*/
                        MyLog.d(TAG, "<<<getDecryptedMessage decryptedTest: " + decryptedTest);
                        String decryptedTest2 = rsaUtil.getDecryptedMsg(decryptedTest);
                        MyLog.d(TAG, "<<<getDecryptedMessage decryptedTest2: " + decryptedTest2);
                        jsonObject.put(key, decryptedTest2);
                    } catch (Exception e) {
                        MyLog.e(TAG, "getCommonDecryptedMessage: ", e);
                    }
                }
            }


            messageAfterDecrypt = jsonObject.toString();
        } catch (Exception e) {
            MyLog.e(TAG, "getScMessageResponseDecrypted: ", e);
        }
        return messageAfterDecrypt;
    }

    public static Object getCommonEncryptedMsg(Object message, Context context, ArrayList<String> jsonKeys) {
        try {
            RSAUtil rsaUtil = new RSAUtil();
            CryptLib _crypt = new CryptLib();
            String iv = "chat";
            String securityToken = SessionManager.getInstance(context).getSecurityToken();
            String publicKeyString = SessionManager.getInstance(context).getPublicEncryptionKey();
            String privateKeyString = SessionManager.getInstance(context).getPrivateEncryptionKey();
            //Log.d(TAG, "<<<getEncryptedMessage publicKeyString: "+publicKeyString);
            //Log.d(TAG, "<<<getEncryptedMessage privateKeyString: "+privateKeyString);

            rsaUtil.init(publicKeyString, privateKeyString);
            JSONObject jsonObject = new JSONObject(message.toString());
            String toUserId = jsonObject.getString("to");
            ContactDB_Sqlite contactDB_sqlite = new ContactDB_Sqlite(context);
            String userToken = contactDB_sqlite.getSecurityToken(toUserId);
            // String securityToken = SessionManager.getInstance(context).getSecurityToken();
            //userToken=securityToken;
            MyLog.d(TAG, "<<<getEncryptedMessage userToken: " + userToken);
            for (String key : jsonKeys) {
                if (jsonObject.has(key)) {
                    String originalText = jsonObject.getString(key);
                    String rsaEncryptedMsg = rsaUtil.getEncryptedMsg(originalText);
                    if (originalText.equals(rsaEncryptedMsg)) {
                        continue;
                    }
                    String rsaDecrypted = rsaUtil.getDecryptedMsg(rsaEncryptedMsg);
                    MyLog.d(TAG, "rsa decrypted: " + rsaDecrypted);
                    MyLog.d(TAG, "<<<getEncryptedMessage:  rsaEncryptedMsg " + rsaEncryptedMsg);
                    String tokenEncryptedMsg = _crypt.encryptSimple(rsaEncryptedMsg, userToken, iv); //encrypt
                    MyLog.d(TAG, "<<<getEncryptedMessage: tokenEncryptedMsg  " + tokenEncryptedMsg);
                    String decryptedTest = _crypt.decryptCipherText(tokenEncryptedMsg, userToken, iv);
                    MyLog.d(TAG, "<<<getEncryptedMessage decryptedTest: " + decryptedTest);
                    /*// tokenEncryptedMsg=tokenEncryptedMsg.replace("\n","");
                    String decryptedTest = _crypt.decryptCipherText( tokenEncryptedMsg,userToken,iv);
                    Log.d(TAG, "<<<getEncryptedMessage decryptedTest: " + decryptedTest);
                    String decryptedTest2 = rsaUtil.getDecryptedMsg(decryptedTest);
                    Log.d(TAG, "<<<getEncryptedMessage decryptedTest2: " + decryptedTest2);*/
                    jsonObject.put(key, tokenEncryptedMsg);

                }
            }

/*            jsonObject.put("user_token",userToken);
            jsonObject.put("security_key",securityToken);
            jsonObject.put("publickey",publicKeyString);
            jsonObject.put("privatekey",privateKeyString);*/
            //   String overAllEncryption= _crypt.encryptSimple(jsonObject.toString(), securityToken, iv); //final encryption
            MyLog.d(TAG, "getEncryptedMessage final: " + jsonObject.toString());
            return jsonObject.toString();
        } catch (Exception e) {
            MyLog.e(TAG, "getEncryptedMessage: ", e);

        }
        return message;
    }

    public static Object getMessageResponseEncryptedMsg(Object message, Context context, ArrayList<String> jsonKeys) {
        try {
            RSAUtil rsaUtil = new RSAUtil();
            CryptLib _crypt = new CryptLib();
            String iv = "chat";

            String publicKeyString = SessionManager.getInstance(context).getPublicEncryptionKey();
            String privateKeyString = SessionManager.getInstance(context).getPrivateEncryptionKey();
            rsaUtil.init(publicKeyString, privateKeyString);
            //Log.d(TAG, "<<<getEncryptedMessage publicKeyString: "+publicKeyString);
            //Log.d(TAG, "<<<getEncryptedMessage privateKeyString: "+privateKeyString);

            JSONObject jsonObject = new JSONObject(message.toString());
            String toUserId = jsonObject.getString("to");
            ContactDB_Sqlite contactDB_sqlite = new ContactDB_Sqlite(context);
            String userToken = contactDB_sqlite.getSecurityToken(toUserId);
            MyLog.d(TAG, "<<<getEncryptedMessage userToken: " + userToken);

            JSONObject dataObj = jsonObject.getJSONObject("data");
            for (String key : jsonKeys) {
                if (dataObj.has(key)) {
                    String rsaEncryptedMsg = rsaUtil.getEncryptedMsg(dataObj.getString(key));
                    rsaEncryptedMsg = rsaEncryptedMsg.replace("\n", "");
                    MyLog.d(TAG, "<<<getEncryptedMessage:  rsaEncryptedMsg " + rsaEncryptedMsg);
                    String tokenEncryptedMsg = _crypt.encryptSimple(rsaEncryptedMsg, userToken, iv); //encrypt
                    MyLog.d(TAG, "<<<getEncryptedMessage: tokenEncryptedMsg  " + tokenEncryptedMsg);
                    // tokenEncryptedMsg=tokenEncryptedMsg.replace("\n","");
                    String decryptedTest = CryptLibDecryption.decrypt(userToken, tokenEncryptedMsg);
                    MyLog.d(TAG, "<<<getEncryptedMessage decryptedTest: " + decryptedTest);
                    String decryptedTest2 = rsaUtil.getDecryptedMsg(decryptedTest);
                    MyLog.d(TAG, "<<<getEncryptedMessage decryptedTest2: " + decryptedTest2);
                    dataObj.put(key, tokenEncryptedMsg);
                }
                jsonObject.put("data", dataObj);
                return jsonObject;
            }
        } catch (Exception e) {
            MyLog.e(TAG, "getCommonncryptedMsg: ", e);

        }
        return message;
    }

    public static String convertHexToBase64String(String hexString) {
        String base64 = "";
        try {

            BigInteger bigInteger = new BigInteger(hexString, 10);
            base64 = Base64.encodeToString(bigInteger.toByteArray(), Base64.NO_WRAP);

        } catch (Exception e) {
            MyLog.e(TAG, "convertHexToBase64String: ", e);
        }
        return base64;
    }

    public static String convertHexToBase64StringOld(String hexString) {
        String base64 = "";
        try {
            int token = Integer.parseInt(hexString);
            hexString = Integer.toHexString(token);
            byte[] decodedHex = org.apache.commons.codec.binary.Hex.decodeHex(hexString.toCharArray());
            base64 = Base64.encodeToString(decodedHex, Base64.NO_WRAP);
        } catch (Exception e) {
            MyLog.e(TAG, "convertHexToBase64String: ", e);
        }
        return base64;
    }
}
