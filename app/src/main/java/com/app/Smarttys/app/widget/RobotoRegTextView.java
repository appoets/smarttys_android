package com.app.Smarttys.app.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.app.Smarttys.core.smarttyhelperclass.SmarttyFontUtils;

/**
 * Created by CAS60 on 2/17/2017.
 */
public class RobotoRegTextView extends TextView {

    public RobotoRegTextView(Context context) {
        super(context);
        init();
    }

    public RobotoRegTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RobotoRegTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface face = Typeface.createFromAsset(getContext().getAssets(), SmarttyFontUtils.getRobotoRegularName());
        setTypeface(face);
    }
}
