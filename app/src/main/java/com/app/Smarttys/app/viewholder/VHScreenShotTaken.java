package com.app.Smarttys.app.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;


/**
 * Created by user145 on 4/6/2018.
 */

public class VHScreenShotTaken extends RecyclerView.ViewHolder {

    public TextView tv_screen_shot_taken;


    public VHScreenShotTaken(View itemView) {
        super(itemView);
        tv_screen_shot_taken = itemView.findViewById(R.id.tv_screen_shot_taken);
    }
}
