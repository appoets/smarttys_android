package com.app.Smarttys.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${pradeep} on 24/6/16.
 */
public class SharedPreference {

    public static final String mypreference = "docpref";
    private static SharedPreference pref;

    /* singleton implementation for shared pref values*/

    public SharedPreference() {
        super();
    }

    public static SharedPreference getInstance() {
        if (pref == null)
            pref = new SharedPreference();
        return pref;
    }

    public void save(Context context, String Keyvalue, String text) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(mypreference, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2
        editor.putString(Keyvalue, text); //3
        editor.apply(); //4

    }


    public List<String> getPaymentDoctorArray(Context context, String Keyvalue) {
        SharedPreferences mPrefs = context.getSharedPreferences("PhotoCollage", Context.MODE_PRIVATE);
        List<String> savedCollage = new ArrayList<String>();
        SharedPreferences settings;
        String text;
        // settings = context.getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        text = mPrefs.getString(Keyvalue, null);
        // return text;
        Gson gson = new Gson();
        //String json = mPrefs.getString("myJson", "");
        if (text == null) {
            savedCollage = new ArrayList<String>();
        } else {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            savedCollage = gson.fromJson(text, type);
        }
        return savedCollage;
    }


    public String getValue(Context context, String Keyvalue) {
        SharedPreferences settings;
        String text;
        settings = context.getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        text = settings.getString(Keyvalue, null);
        return text;
    }


    public void clearSharedPreference(Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.clear();
        editor.apply();
    }

    public void removeValue(Context context, String Keyvalue) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.remove(Keyvalue);
        editor.apply();
    }

    public void saveBool(Context context, String Keyvalue, boolean status) {
        if (null == context)
            return;
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(mypreference, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2
        editor.putBoolean(Keyvalue, status); //3
        editor.apply(); //4
    }

    public boolean getBool(Context context, String Keyvalue) {
        try {
            SharedPreferences settings;
            boolean text;
            settings = context.getSharedPreferences(mypreference, Context.MODE_PRIVATE);
            text = settings.getBoolean(Keyvalue, false);
            return text;

        } catch (Exception e) {
            return false;
        }
    }

    public void saveInt(Context context, String Keyvalue, int text) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(mypreference, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2
        editor.putInt("theme", text); //3
        editor.apply(); //4
    }

    public int getInt(Context context, String Keyvalue) {
        SharedPreferences settings;
        int text;
        settings = context.getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        text = settings.getInt(Keyvalue, 0);
        return text;
    }


}
