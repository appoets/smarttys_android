package com.app.Smarttys.app.utils;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.hardware.fingerprint.FingerprintManagerCompat;

import com.app.Smarttys.R;

import static android.content.Context.KEYGUARD_SERVICE;

public class LoginAuthUtils {

    private static final String TAG = "LoginAuthUtils";

    public static void startDefaultPasswordPage(Activity context, int reqCode) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            KeyguardManager km = (KeyguardManager) context.getSystemService(KEYGUARD_SERVICE);

            if (km != null && km.isKeyguardSecure()) {
                Intent authIntent = km.createConfirmDeviceCredentialIntent(context.getString(R.string.login_with_pin), context.getString(R.string.pls_enter_phone_lock));
                context.startActivityForResult(authIntent, reqCode);
            }
        }
    }

    public static boolean isFingerPrintDevice(Context context) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //Fingerprint API only available on from Android 6.0 (M)
                //FingerprintManager fingerprintManager = (FingerprintManager) context.getSystemService(Context.FINGERPRINT_SERVICE);

                FingerprintManagerCompat fingerprintManager = FingerprintManagerCompat.from(context);
                if (fingerprintManager != null) {
                    if (!fingerprintManager.isHardwareDetected()) {
                        // Device doesn't support fingerprint authentication
                        MyLog.d(TAG, "isFingerPrintDevice: no hardware detected ");
                        return false;
                    } else if (!fingerprintManager.hasEnrolledFingerprints()) {
                        // User hasn't enrolled any fingerprints to authenticate with
                        MyLog.d(TAG, "isFingerPrintDevice: not enrolled finger prints");
                        return false;
                    } else {
                        // Everything is ready for fingerprint authentication

                        return true;
                    }

                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "isFingerPrintDevice: ", e);
        }
        return false;
    }

    public static boolean isDeviceHasLock(Context context) {

        KeyguardManager manager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        if (manager != null) {
            return manager.isKeyguardSecure();
        }

        return false;
    }
}
