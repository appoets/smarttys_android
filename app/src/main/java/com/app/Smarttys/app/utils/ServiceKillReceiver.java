package com.app.Smarttys.app.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by user134 on 3/26/2018.
 */

public class ServiceKillReceiver extends BroadcastReceiver {
    private static final String TAG = "ServiceKillReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        MyLog.e(TAG, "ServiceKillReceiver onReceive: ");
        AppUtils.restartService(context);
    }
}
