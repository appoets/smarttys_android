package com.app.Smarttys.app.utils;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;
import com.squareup.picasso.RequestHandler;

/**
 * Created by user134 on 4/2/2018.
 */

public class VideoRequestHandler extends RequestHandler {
    private static final String TAG = "VideoRequestHandler";
    public static String SCHEME_VIDEO = "video";

    @Override
    public boolean canHandleRequest(Request data) {
        String scheme = data.uri.getScheme();
        return (SCHEME_VIDEO.equals(scheme));
    }

    @Override
    public Result load(Request data, int arg1) {
        try {
            Bitmap bm = ThumbnailUtils.createVideoThumbnail(data.uri.getPath(), MediaStore.Images.Thumbnails.MINI_KIND);

            if (bm != null) {

                return new Result(bm, Picasso.LoadedFrom.DISK);
            }

        } catch (Exception e) {
            MyLog.e(TAG, "load: ", e);
        }

        return null;
    }
}
