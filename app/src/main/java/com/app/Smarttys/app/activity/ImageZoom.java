package com.app.Smarttys.app.activity;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.ZoomImageView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.smarttyhelperclass.SmarttyImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by CAS56 on 12/28/2016.
 */
public class ImageZoom extends CoreActivity {
    private static final String TAG = "ImageZoom";
    private ImageView back, share, save;
    private ZoomImageView imagezoom_image;
    private Bitmap bitmap;
    private AvnNextLTProDemiTextView profilepic_usename;
    private Uri tempUri;
    private boolean fromProfile;
    private String imageFilePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imagezoom);
        back = findViewById(R.id.backarrow_imagezoom);
        save = findViewById(R.id.save);
        profilepic_usename = findViewById(R.id.profile_username);
        share = findViewById(R.id.share);
        getSupportActionBar().hide();
        imagezoom_image = findViewById(R.id.imagezoom_image);
        imagezoom_image.setOnPhotoTapListener(new ZoomImageView.OnPhotoTapListener() {
            @Override
            public void onPhotoTap(View view, float x, float y) {
                if (findViewById(R.id.imagezoom).getVisibility() == View.GONE)
                    findViewById(R.id.imagezoom).setVisibility(View.VISIBLE);
                else
                    findViewById(R.id.imagezoom).setVisibility(View.GONE);
            }
        });
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int zoomImageHeight = displayMetrics.heightPixels;
        int zoomImageWidth = displayMetrics.widthPixels;


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("ProfilePath")) {
                String pic = bundle.getString("ProfilePath", "");

                String receviername = bundle.getString("Profilepicname");

                if (!pic.equals("")) {
                    imageFilePath = AppUtils.getValidProfilePath(pic);
                    AppUtils.loadImage(this, imageFilePath, imagezoom_image, 0, R.mipmap.chat_attachment_profile_default_image_frame);
/*                    Picasso.with(this).load(pic).error(R.mipmap.chat_attachment_profile_default_image_frame)
                            .into(new Target() {
                                @Override
                                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                    imagezoom_image.setImageBitmap(bitmap);
                                    share.setVisibility(View.VISIBLE);
                                    save.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onBitmapFailed(Drawable errorDrawable) {

                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {

                                }
                            });*/
                }

                profilepic_usename.setVisibility(View.VISIBLE);
                profilepic_usename.setText(receviername);

            } else {
                share.setVisibility(View.VISIBLE);
                String imagePath = bundle.getString("image");
                if (imagePath == null) {
                    imagePath = getIntent().getStringExtra("image");
                }
                imageFilePath = imagePath;
//                imagezoom_image.setImageURI(Uri.parse(image));
                Bitmap bitmap = SmarttyImageUtils.decodeBitmapFromFile(imagePath, zoomImageWidth, zoomImageHeight);
                if (bitmap != null) {
                    imagezoom_image.setImageBitmap(bitmap);
                } else {
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    Bitmap bitmapp = BitmapFactory.decodeFile(imagePath, bmOptions);
                    if (bitmapp == null) {
                        Glide
                                .with(this)
                                .load(imagePath)
                                .asBitmap()
                                .error(R.drawable.nav_menu_background)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .dontAnimate()
                                .fitCenter()
                                .into(new SimpleTarget<Bitmap>() {

                                    @Override
                                    public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                        // TODO Auto-generated method stub
                                        imagezoom_image.setImageBitmap(arg0);
                                    }

                                    @Override
                                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                        super.onLoadFailed(e, errorDrawable);
                                    }
                                });
                    } else {
                        imagezoom_image.setImageBitmap(bitmapp);
                    }
                }
            }
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    initProgress("Initializing...", true);
                    //showProgressDialog();
                    //bitmap = ((BitmapDrawable) imagezoom_image.getDrawable()).getBitmap();
                    /*if (bitmap != null) {
                        tempUri = getImageUri(bitmap);
                    }*/
                    tempUri = Uri.fromFile(new File(imageFilePath));
                    //shareIt();
                    onShareClick();
                    hideProgressDialog();
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveImage();
            }
        });

    }

    private void saveImage() {

        if (imagezoom_image.getDrawable() != null) {
            Bitmap profileBmp = ((BitmapDrawable) imagezoom_image.getDrawable()).getBitmap();

            File myDir = new File(MessageFactory.PROFILE_IMAGE_PATH);
            if (!myDir.exists()) {
                myDir.mkdirs();
            }
            String fname = "Image-" + Calendar.getInstance().getTimeInMillis() + ".jpg";
            File file = new File(myDir, fname);
            if (file.exists()) file.delete();
            try {
                file.createNewFile();
                FileOutputStream out = new FileOutputStream(file);
                profileBmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
                Toast.makeText(ImageZoom.this, "profile picture saved", Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
        }

    }


    public void onShareClick() {
        String[] blacklist = new String[]{"com.any.package", "net.other.package"};
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_STREAM, tempUri);
        intent.putExtra(Intent.EXTRA_TEXT, "");
        startActivity(generateCustomChooserIntent(intent, blacklist));
    }

    private Intent generateCustomChooserIntent(Intent prototype, String[] forbiddenChoices) {
        List<Intent> targetedShareIntents = new ArrayList<Intent>();
        List<HashMap<String, String>> intentMetaInfo = new ArrayList<HashMap<String, String>>();
        Intent chooserIntent;

        Intent dummy = new Intent(prototype.getAction());
        dummy.setType(prototype.getType());
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(dummy, 0);

        if (!resInfo.isEmpty()) {
            for (ResolveInfo resolveInfo : resInfo) {
                if (resolveInfo.activityInfo == null || Arrays.asList(forbiddenChoices).contains(resolveInfo.activityInfo.packageName))
                    continue;
                HashMap<String, String> info = new HashMap<String, String>();
                info.put("packageName", resolveInfo.activityInfo.packageName);
                info.put("className", resolveInfo.activityInfo.name);
                info.put("simpleName", String.valueOf(resolveInfo.activityInfo.loadLabel(getPackageManager())));
                intentMetaInfo.add(info);
            }

            if (!intentMetaInfo.isEmpty()) {
                // sorting for nice readability
                Collections.sort(intentMetaInfo, new Comparator<HashMap<String, String>>() {
                    @Override
                    public int compare(HashMap<String, String> map, HashMap<String, String> map2) {
                        return map.get("simpleName").compareTo(map2.get("simpleName"));
                    }
                });

                // create the custom intent list
                for (HashMap<String, String> metaInfo : intentMetaInfo) {
                    Intent targetedShareIntent = (Intent) prototype.clone();
                    targetedShareIntent.setPackage(metaInfo.get("packageName"));
                    targetedShareIntent.setClassName(metaInfo.get("packageName"), metaInfo.get("className"));
                    targetedShareIntents.add(targetedShareIntent);
                }


                chooserIntent = Intent.createChooser(targetedShareIntents.remove(targetedShareIntents.size() - 1), "share");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
                return chooserIntent;
            }
        }

        return Intent.createChooser(prototype, "Share it");
    }


}
