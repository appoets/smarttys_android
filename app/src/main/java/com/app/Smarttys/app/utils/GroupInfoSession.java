package com.app.Smarttys.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.Smarttys.core.model.GroupInfoPojo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by CAS60 on 1/28/2017.
 */
public class GroupInfoSession {

    private static final String TAG = "GroupInfoSession";
    private final String GROUP_INFO_PREF = "GroupInfoPref";
    private Context mContext;
    private SharedPreferences groupInfoPref;
    private SharedPreferences.Editor groupInfoPrefEditor;
    private Gson gson;
    private GsonBuilder gsonBuilder;

    public GroupInfoSession(Context context) {
        this.mContext = context;
        groupInfoPref = mContext.getSharedPreferences(GROUP_INFO_PREF, Context.MODE_PRIVATE);
    }

    public boolean hasGroupInfo(String docId) {
        String infoKey = docId.concat("-hasInfo");
        return groupInfoPref.getBoolean(infoKey, false);
    }

    public GroupInfoPojo getGroupInfo(String docId) {
        String dataKey = docId.concat("-groupData");
        String data = groupInfoPref.getString(dataKey, "");
        try {
            if (!data.equals("")) {
                gsonBuilder = new GsonBuilder();
                gson = gsonBuilder.create();

                GroupInfoPojo infoPojo = gson.fromJson(data, GroupInfoPojo.class);
                return infoPojo;
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public void setGroupMembers(String groupId, JSONObject membersList) {
        try {
            groupInfoPrefEditor = groupInfoPref.edit();
            if (membersList == null) {
                groupInfoPrefEditor.putString(groupId, "");
            } else {
                groupInfoPrefEditor.putString(groupId, membersList.toString());
            }
            groupInfoPrefEditor.apply();
        } catch (Exception e) {
            MyLog.e(TAG, "setGroupMembers: ", e);
        }
    }

    public void clear() {
        groupInfoPrefEditor = groupInfoPref.edit();
        groupInfoPrefEditor.clear();
        groupInfoPrefEditor.apply();
    }

    public String getGroupMembers(String groupId) {

        try {
            return groupInfoPref.getString(groupId, null);
        } catch (Exception e) {
            MyLog.e(TAG, "getGroupMembers: ", e);
        }
        return null;
    }

    public void updateGroupInfo(String docId, GroupInfoPojo infoPojo) {
        gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();

        try {
            JSONObject dataObj;
            if (hasGroupInfo(docId)) {
                GroupInfoPojo oldInfo = getGroupInfo(docId);
                String data = gson.toJson(oldInfo);
                dataObj = new JSONObject(data);
            } else {
                dataObj = new JSONObject();
            }

            String newData = gson.toJson(infoPojo);
            JSONObject newObj = new JSONObject(newData);

            Iterator<?> keys = newObj.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                dataObj.put(key, newObj.get(key));
            }

            String dataKey = docId.concat("-groupData");
            String infoKey = docId.concat("-hasInfo");
            groupInfoPrefEditor = groupInfoPref.edit();

            groupInfoPrefEditor.putBoolean(infoKey, true);
            groupInfoPrefEditor.putString(dataKey, dataObj.toString());
            groupInfoPrefEditor.apply();
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void deleteGroupInfo(String docId) {
        String dataKey = docId.concat("-groupData");
        String infoKey = docId.concat("-hasInfo");

        groupInfoPrefEditor = groupInfoPref.edit();
        groupInfoPrefEditor.remove(dataKey);
        groupInfoPrefEditor.remove(infoKey);
        groupInfoPrefEditor.apply();
    }

    public List<String> getGroupIdList() {
        List<String> groupIdList = new ArrayList<>();

        Map<String, ?> allEntries = groupInfoPref.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            String key = entry.getKey();
            if (key.contains("-groupData")) {
                gsonBuilder = new GsonBuilder();
                gson = gsonBuilder.create();
                String value = entry.getValue().toString();

                GroupInfoPojo infoPojo = gson.fromJson(value, GroupInfoPojo.class);
                if (infoPojo.isLiveGroup() && infoPojo.getGroupId() != null && !infoPojo.getGroupId().equals("")) {
                    groupIdList.add(infoPojo.getGroupId());
                }
            }
        }
        return groupIdList;
    }
}
