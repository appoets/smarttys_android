package com.app.Smarttys.app.utils;

/**
 * Created by CAS60 on 7/10/2017.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.app.Smarttys.core.SessionManager;

public class DateChangeBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        long deviceTS = System.currentTimeMillis();
        long serverTS = SessionManager.getInstance(context).getServerTS();

        if (serverTS != 0) {
            // Add 1 minute on every minute to Server timestamp
            if (intent.getAction().equalsIgnoreCase(Intent.ACTION_TIME_TICK)) {
                serverTS = serverTS + (60 * 1000);
            }
            MyLog.e("DateBroadcastReceiver", "serverTime" + serverTS);

            long timeDiff = deviceTS - serverTS;
            MyLog.e("DateBroadcastReceiver", "timeDiff" + timeDiff);

            //  SessionManager.getInstance(context).setServerTimeDifference(serverTS, timeDiff);
        }
    }
}
