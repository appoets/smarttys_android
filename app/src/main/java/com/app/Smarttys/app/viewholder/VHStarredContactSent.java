package com.app.Smarttys.app.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.widget.CircleImageView;


/**
 * Created by CAS63 on 2/6/2017.
 */
public class VHStarredContactSent extends RecyclerView.ViewHolder {

    public TextView senderName, time, contactName, contactNumber, invite, add, invite1, add1, message, tvDateLbl, tvSecretLbl, toname, fromname;
    public LinearLayout contact_add_invite;
    public ImageView starredindicator_below, imageViewindicatior, ivUserProfile;
    public ImageView singleTick, doubleTickGreen, doubleTickBlue, clock;
    public View v1;
    public RelativeLayout rlContactSent, selection_layout, relative_layout_message;
    public CircleImageView contactImage;

    /**
     * one more field to show contact data to be added
     */
    public VHStarredContactSent(View view) {
        super(view);

        rlContactSent = view.findViewById(R.id.rlContactSent);
        ivUserProfile = view.findViewById(R.id.userprofile);
        imageViewindicatior = view.findViewById(R.id.imageView);
        senderName = view.findViewById(R.id.lblMsgFrom);
        contact_add_invite = view.findViewById(R.id.contact_add_invite);
        contactName = view.findViewById(R.id.contactName);
        starredindicator_below = view.findViewById(R.id.starredindicator_below);
        contactNumber = view.findViewById(R.id.contactNumber);
        contactImage = view.findViewById(R.id.contactImage);
        time = view.findViewById(R.id.ts);
        singleTick = view.findViewById(R.id.single_tick_green);
        doubleTickGreen = view.findViewById(R.id.double_tick_green);
        doubleTickBlue = view.findViewById(R.id.double_tick_blue);
        clock = view.findViewById(R.id.clock);
        tvDateLbl = view.findViewById(R.id.datelbl);
        tvSecretLbl = view.findViewById(R.id.tvSecretLbl);
        invite = view.findViewById(R.id.invite);
        message = view.findViewById(R.id.message);
        add = view.findViewById(R.id.add);
        invite1 = view.findViewById(R.id.invite_1);
        add1 = view.findViewById(R.id.add_1);
        v1 = view.findViewById(R.id.v1);
        selection_layout = view.findViewById(R.id.selection_layout);
        relative_layout_message = view.findViewById(R.id.relative_layout_message);
        toname = view.findViewById(R.id.toname);
        fromname = view.findViewById(R.id.fromname);


    }
}
