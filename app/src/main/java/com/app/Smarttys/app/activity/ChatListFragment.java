package com.app.Smarttys.app.activity;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.ChatListAdapter;
import com.app.Smarttys.app.calls.CallsActivity;
import com.app.Smarttys.app.dialog.ChatLockPwdDialog;
import com.app.Smarttys.app.dialog.CustomAlertDialog;
import com.app.Smarttys.app.dialog.MuteAlertDialog;
import com.app.Smarttys.app.dialog.ProfileImageDialog;
import com.app.Smarttys.app.model.FirstTimeGroupRefreshed;
import com.app.Smarttys.app.model.GroupInviteBraodCast;
import com.app.Smarttys.app.model.NewGroup_DB;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.ConnectivityInfo;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.GroupInfoSession;
import com.app.Smarttys.app.utils.MuteUnmute;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.SharedPreference;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.ActivityLauncher;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.ShortcutBadgeManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.IncomingMessage;
import com.app.Smarttys.core.message.MessageAck;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.CallItemChat;
import com.app.Smarttys.core.model.GroupInfoPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.MuteStatusPojo;
import com.app.Smarttys.core.model.MuteUserPojo;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.service.Constants;
import com.app.Smarttys.core.socket.MessageService;
import com.app.Smarttys.core.socket.SocketManager;
import com.bumptech.glide.BitmapTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import me.leolin.shortcutbadger.ShortcutBadger;

public class ChatListFragment extends Fragment implements View.OnClickListener, MuteAlertDialog.MuteAlertCloseListener, AdapterView.OnItemLongClickListener {
    private static final String TAG = "NewChatList" + "performance";
    private static final String SECRET_CHAT_PREFIX = "secret_";
    public static boolean isChatListPage;
    public static int count = 0;
    private static String mCurrentUserId;
    private final int QR_REQUEST_CODE = 25;
    //    public NewChatListAdapter mAdapter;
    public ChatListAdapter mAdapter;
    public ArrayList<MessageItemChat> mChatData = new ArrayList<>();
    public boolean isthisGroup = false;
    public ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
    boolean mDBrefresh;
    ContactDB_Sqlite contactDB_sqlite;
    View view;
    HashMap<String, MessageItemChat> uniqueStore = new HashMap<>();
    HashMap<String, Long> typingEventMap = new HashMap<>();
    String receiverID;
    Getcontactname getcontactname;
    String archivecount, archivecount_group;
    Session session;
    AvnNextLTProRegTextView archivetext;
    String receiverDocumentID;
    String from = "chatlist";
    Boolean myMenuClickFlag = false;
    String username, profileimage, value;
    Bitmap myTemp = null;
    ShortcutBadgeManager shortcutBadgeManager;
    String phoneno;
    Receiver receiver;
    private TextView userMessagechat;
    private View progressBar;
    //    private ListView chat_list;
    private RecyclerView chat_list;
    private MyReceiver myReceiver = null;
    private Context mContext;
    private ImageButton ibNewChat;
    private SessionManager sessionManager;
    private String uniqueCurrentID;
    private MessageDbController db;
    private GroupInfoSession groupInfoSession;
    private RelativeLayout archive_relativelayout;
    private Handler typingHandler;
    private long timeOutTyping = 3000;
    Runnable typingRunnable = new Runnable() {
        @Override
        public void run() {
            long currentTime = Calendar.getInstance().getTimeInMillis();
            ArrayList<String> removeList = new ArrayList<>();
//            Iterator it = typingEventMap.entrySet().iterator();
            for (Iterator it = typingEventMap.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry pair = (Map.Entry) it.next();
                String key = (String) pair.getKey();
                long timeOut = (long) pair.getValue();

                long timeDiff = currentTime - timeOut;
                if (timeDiff > MessageFactory.TYING_MESSAGE_TIMEOUT) {
//                    typingEventMap.remove(key);
                    MessageItemChat chat = uniqueStore.get(key);
                    if (chat != null) {
                        int index = mChatData.indexOf(chat);
                        chat.setTypingAt(0);

                        mChatData.set(index, chat);
                        uniqueStore.put(key, chat);
                        mAdapter.notifyDataSetChanged();

                    }
                }

            }
            if (typingEventMap.size() > 0) {
                timeOutTyping = currentTime - Collections.max(typingEventMap.values());
                typingHandler.postDelayed(typingRunnable, timeOutTyping);
            } else {
                typingHandler.removeCallbacks(typingRunnable);
            }
        }
    };
    private UserInfoSession userInfoSession;
    private SearchView searchView;
    private Menu menuLongClick, menuNormal;
    private boolean isSecretChatDeleted;
    private ArrayList<String> getUsersList = new ArrayList<>();
    private List<String> incomingMsgIds = new ArrayList<>();
    private int mFirstVisibleItemPosition, mLastVisibleItemPosition;
    private LinearLayout mLnrcallStatus;

    public static void performGroupExit(String groupid) {
        long ts = Calendar.getInstance().getTimeInMillis();
        String msgId = mCurrentUserId + "-" + groupid + "-g-" + ts;

        try {
            JSONObject exitObject = new JSONObject();
            exitObject.put("groupType", SocketManager.ACTION_EXIT_GROUP);
            exitObject.put("from", mCurrentUserId);
            exitObject.put("groupId", groupid);
            exitObject.put("id", ts);
            exitObject.put("toDocId", msgId);

            SendMessageEvent exitGroupEvent = new SendMessageEvent();
            exitGroupEvent.setEventName(SocketManager.EVENT_GROUP);
            exitGroupEvent.setMessageObject(exitObject);
            EventBus.getDefault().post(exitGroupEvent);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        contactDB_sqlite = CoreController.getContactSqliteDBintstance(getActivity());
        if (view == null) {
            view = inflater.inflate(R.layout.activity_new_chat_list, container, false);
        } else {
            if (view.getParent() != null)
                ((ViewGroup) view.getParent()).removeView(view);
        }
        intitialize(view);


        mAdapter.setChatListItemClickListener(new ChatListAdapter.ChatListItemClickListener() {
            @Override
            public void onItemClick(MessageItemChat list, View view, int position, long imageTS) {
                Log.d(TAG, "onItemClick: ");
                if (view.getId() == R.id.storeImage) {
                    String Uid = mChatData.get(position).getMessageId().split("-")[1];
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("MessageItem", mChatData.get(position));
                    bundle.putString("userID", Uid);
//                    /*if(view instanceof CircleImageView) {
//                        CircleImageView imageView = (CircleImageView) view;
//                        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
//                        SerializeBitmap serializeBitmap = new SerializeBitmap();
//                        serializeBitmap.setBitmap(bitmap);
//
//                        bundle.putSerializable("ProfilePic", serializeBitmap);
//                    }*/
//
////                    String imageTS = contactDB_sqlite.getDpUpdatedTime(Uid);
////                    final String avatar = Constants.USER_PROFILE_URL + userId + ".jpg?id=" + imageTS;
                    bundle.putSerializable("ProfilePic", null);
                    //need
                    if (list.isGroup()) {
                        bundle.putSerializable("GroupChat", true);
                    } else {
                        bundle.putSerializable("GroupChat", false);
                    }
                    bundle.putLong("imageTS", imageTS);
                    bundle.putBoolean("FromSecretChat", false);
                    ProfileImageDialog dialog = new ProfileImageDialog();
                    dialog.setArguments(bundle);
                    dialog.show(getFragmentManager(), "profile");
                }
//                /*else if(isPasswordProtected){
//
//                    final CustomAlertDialog dialogAlert = new CustomAlertDialog();
//                    dialogAlert.setTitle("Enter Password");
//                    dialogAlert.setEditTextdata("Enter your password");
//                    dialogAlert.setPositiveButtonText("OK");
//                    dialogAlert.setNegativeButtonText("Forgot Password");
//
//                    dialogAlert.setCancelable(false);
//
//                    dialogAlert.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
//                        @Override
//                        public void onPositiveButtonClick() {
//
//                            dialogAlert.dismiss();
//                            callSuccessAlert();
//                        }
//
//                        @Override
//                        public void onNegativeButtonClick() {
//                            dialogAlert.dismiss();
//                        }
//                    });
//                    dialogAlert.show(getFragmentManager(), "PasswordAlert");
//                }*/
                else {

                    if (position >= 0) {
//                        ChatLockPojo lockPojo = getChatLockdetailfromDB(position, list);

                        if (myMenuClickFlag) {

                            mChatData.get(position).setSelected(!mChatData.get(position).isSelected());

                            if (mChatData.get(position).isSelected()) {
                                selectedChatItems.add(mChatData.get(position));
                                count = count + 1;
                            } else {
                                selectedChatItems.remove(mChatData.get(position));
                                count = count - 1;
                            }
                            mAdapter.notifyDataSetChanged();

                            onCreateOptionsMenu(menuLongClick, getActivity().getMenuInflater());

                            if (list.isGroup()) {
                                if (count <= 0) {
                                    myMenuClickFlag = false;
                                    onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                                    count = 0;
//                            getActivity().getMenuInflater().inflate(R.menu.chats_screen, menuNormal);
                                }
                            } else {
                                if (count == 0) {
                                    myMenuClickFlag = false;
                                    onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
//                            getActivity().getMenuInflater().inflate(R.menu.chats_screen, menuNormal);
                                }
                            }

                        }
//                        else if (sessionManager.getLockChatEnabled().equals("1") && lockPojo != null) {
//                            String stat = lockPojo.getStatus();
//                            String pwd = lockPojo.getPassword();
//
//                            MessageItemChat e = mAdapter.getItem(position);
//                            String docID = e.getMessageId();
//                            String[] ids = docID.split("-");
//                            String documentid = "";
//                            if (list.isGroup()) {
//                                documentid = ids[0] + "-" + ids[1] + "-g";
//                            } else {
//                                documentid = ids[0] + "-" + ids[1];
//                            }
//
//                            if (stat.equals("1")) {
//                                openUnlockChatDialog(documentid, stat, pwd, position);
//                            } else {
//                                navigateTochatviewpage(position, list);
//                            }
//                        }
                        else {
                            navigateTochatviewpage(position, list);
                        }
                    }

                }
                Log.d("ChatListEnd", Calendar.getInstance().getTimeInMillis() + "");
            }

            @Override
            public void onItemLongClick(MessageItemChat list, View view, int position) {
                isthisGroup = list.isGroup();
                if (position >= 0) {
                    mChatData.get(position).setSelected(!mChatData.get(position).isSelected());

                    ImageView ivProfielPic = (ImageView) view.findViewById(R.id.storeImage);
                    if (ivProfielPic.getDrawable() != null) {
                        try {
                            //  myTemp = ((BitmapDrawable) ivProfielPic.getDrawable()).getBitmap();

                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }


                    if (mChatData.get(position).isSelected()) {
                        selectedChatItems.add(mChatData.get(position));
                        count = count + 1;
                    } else {
                        selectedChatItems.remove(mChatData.get(position));
                        count = count - 1;
                    }
//                mAdapter.notifyDataSetChanged();
                    if (list.isGroup()) {
                        mAdapter.notifyDataSetChanged();
                        myMenuClickFlag = true;
                        MessageItemChat e = mChatData.get(position);
                        e.setSelected(true);
                        mChatData.set(position, e);
                        mAdapter.notifyDataSetChanged();
                        username = e.getSenderName();
                        profileimage = e.getAvatarImageUrl();
                    } else {
                        myMenuClickFlag = true;
                        MessageItemChat e = mChatData.get(position);
                        e.setSelected(true);
                        mChatData.set(position, e);
                        mAdapter.notifyDataSetChanged();
                        username = e.getSenderName();
                        phoneno = e.getSenderMsisdn();
                        profileimage = e.getAvatarImageUrl();
                        if (count == 0) {
                            myMenuClickFlag = false;
                        }
                    }
//                count = count + 1;
                    onCreateOptionsMenu(menuLongClick, getActivity().getMenuInflater()); /*else {
                    tick.setVisibility(View.GONE);


                }*/
                }
            }
        });

        return view;
    }

    private void openUnlockChatDialog(String docId, String status, String pwd, int position) {
        String convId = userInfoSession.getChatConvId(docId);
        ChatLockPwdDialog dialog = new ChatLockPwdDialog();
        dialog.setTextLabel1("Enter your Password");
        dialog.setEditTextdata("Enter new Password");
        dialog.setforgotpwdlabel("Forgot Password");
        dialog.setHeader("Unlock Chat");
        dialog.setButtonText("Unlock");
        Bundle bundle = new Bundle();
        bundle.putSerializable("MessageItem", mChatData.get(position));
        bundle.putString("convID", convId);
        bundle.putString("status", "1");
        bundle.putString("pwd", pwd);
        bundle.putString("page", "chatlist");
        bundle.putString("type", "single");
        bundle.putString("from", uniqueCurrentID);
        dialog.setArguments(bundle);
        dialog.show(getFragmentManager(), "chatunLock");

    }

    private void movetoChatActivity(MessageItemChat e) {
        String[] array = e.getMessageId().split("-");
        String msgid = "";
        Intent intent = new Intent(getActivity(), ChatPageActivity.class);
        if (e.isSecretChat())
            intent = new Intent(getActivity(), SecretChatViewActivity.class);
        intent.putExtra("receiverUid", e.getNumberInDevice());
        intent.putExtra("receiverName", e.getSenderName());
        if (array[0].equalsIgnoreCase(uniqueCurrentID)) {
            receiverDocumentID = array[1];
        } else if (array[1].equalsIgnoreCase(uniqueCurrentID)) {
            receiverDocumentID = array[0];
        }
        if (e.getMessageId().contains("-g")) {
            msgid = array[3];
        } else {
            msgid = array[2];
        }
        intent.putExtra("documentId", receiverDocumentID);
        intent.putExtra("Username", e.getSenderName());
        intent.putExtra("Image", e.getAvatarImageUrl());
        intent.putExtra("msisdn", e.getSenderMsisdn());
        if (from.equalsIgnoreCase("star") || from.equalsIgnoreCase("webLink")) {
            intent.putExtra("msgid", msgid);
            intent.putExtra("starred", e.getMessageId());
        }
        intent.putExtra("type", 0);
        getActivity().startActivity(intent);
        getActivity().overridePendingTransition(R.anim.fast_enter, R.anim.fast_exit);
    }

    private void navigateTochatviewpage(int position, MessageItemChat e) {
        MyLog.d(TAG, "navigateTochatviewpage: chatmove");
        String[] array = e.getMessageId().split("-");
        String msgid = "";
        Intent intent = new Intent(getActivity(), ChatPageActivity.class);
        if (e.isSecretChat())
            intent = new Intent(getActivity(), SecretChatViewActivity.class);
        intent.putExtra("receiverUid", e.getNumberInDevice());
        intent.putExtra("receiverName", e.getSenderName());
        if (array[0].equalsIgnoreCase(uniqueCurrentID)) {
            receiverDocumentID = array[1];
        } else if (array[1].equalsIgnoreCase(uniqueCurrentID)) {
            receiverDocumentID = array[0];
        }
        if (e.getMessageId().contains("-g")) {
            msgid = array[3];
        } else {
            msgid = array[2];
        }
        intent.putExtra("documentId", receiverDocumentID);
        intent.putExtra("Username", e.getSenderName());
        intent.putExtra("Image", e.getAvatarImageUrl());
        intent.putExtra("msisdn", e.getSenderMsisdn());
        if (from.equalsIgnoreCase("star") || from.equalsIgnoreCase("webLink")) {
            intent.putExtra("msgid", msgid);
            intent.putExtra("starred", e.getMessageId());
        }
        intent.putExtra("type", 0);
        getActivity().startActivity(intent);
        //getActivity().overridePendingTransition(R.anim.fast_enter, R.anim.fast_exit);
        getActivity().overridePendingTransition(0, 0);
    }

    private void intitialize(View view) {

        db = CoreController.getDBInstance(getActivity());
        sessionManager = SessionManager.getInstance(getActivity());
        getcontactname = new Getcontactname(getContext());
        //groupchat
        groupInfoSession = new GroupInfoSession(getActivity());
        session = new Session(getActivity());
        typingHandler = new Handler();
        userInfoSession = new UserInfoSession(getContext());
        shortcutBadgeManager = new ShortcutBadgeManager(getActivity());

        uniqueCurrentID = SessionManager.getInstance(getActivity()).getCurrentUserID();

        mCurrentUserId = SessionManager.getInstance(getActivity()).getCurrentUserID();
        mLnrcallStatus = view.findViewById(R.id.lnrcallStatus);

        userMessagechat = view.findViewById(R.id.userMessagechat);
        chat_list = view.findViewById(R.id.chat_list);
        ibNewChat = view.findViewById(R.id.ibNewChat);
        archive_relativelayout = view.findViewById(R.id.archive_relativelayout);
        archivetext = view.findViewById(R.id.archive);
        progressBar = view.findViewById(R.id.progress_bar);
//        mAdapter = new ChatListAdapter(view.getContext(), mChatData, getFragmentManager(),this);
//        chat_list.setAdapter(mAdapter);

        mAdapter = new ChatListAdapter(view.getContext(), mChatData);
//        mAdapter.setCallback(GroupChatList.this);
        chat_list.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        chat_list.setItemAnimator(new DefaultItemAnimator());
        chat_list.setAdapter(mAdapter);

        //loadDbChatItem();

//        receiver = new Receiver();
       /* IntentFilter filter = new IntentFilter();
        filter.addAction("com.selectedchat.remove");
        if (null != getActivity())
            getActivity().registerReceiver(receiver, filter);*/

        OnclickListener();


    }

    private void OnclickListener() {
        mLnrcallStatus.setOnClickListener(this);
        ibNewChat.setOnClickListener(this);
        archivetext.setOnClickListener(this);
    }

    //----------------------------------Click Listener---------------------------------------
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.lnrcallStatus:
                //cHECK CALL STATUS AND hIDE THE bAR
                boolean mCallOngoing = SharedPreference.getInstance().getBool(CoreController.mcontext, "callongoing");
                if (mCallOngoing) {
                    //  registerCallReceiver();


                    Intent callIntent = new Intent(getActivity(), CallsActivity.class);
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    getActivity().startActivity(callIntent);

                } else {
                    if (mLnrcallStatus.getVisibility() == View.VISIBLE) {
                        mLnrcallStatus.setVisibility(View.GONE);
                    }
                }
                break;

            case R.id.ibNewChat:
                ActivityLauncher.launchSettingContactScreen(getActivity());
                break;

            case R.id.archive:
                Intent aIntent = new Intent(getActivity(), Archivelist.class);
                aIntent.putExtra("from", "chatlist");
                startActivity(aIntent);
                break;
        }

    }

    public void checkcallstatus() {
        boolean mCallOngoing = SharedPreference.getInstance().getBool(CoreController.mcontext, "callongoing");
        Log.e("checkcallstatus", "checkcallstatus" + mCallOngoing);
        if (mCallOngoing) {
            //  registerCallReceiver();

            if (mLnrcallStatus.getVisibility() == View.GONE) {
                mLnrcallStatus.setVisibility(View.VISIBLE);
            }
        } else {
            if (mLnrcallStatus.getVisibility() == View.VISIBLE) {
                mLnrcallStatus.setVisibility(View.GONE);
            }
        }
    }

    //--------------------------------------------ListItem Long Click Functions-------------------------
    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {


        return false;
    }

    private void loadDbChatItem() {
        MyLog.d(TAG, "loadDbChatItem: 1");
        uniqueCurrentID = SessionManager.getInstance(getActivity()).getCurrentUserID();
        ArrayList<MessageItemChat> singleChats = db.selectChatList(MessageFactory.CHAT_TYPE_SINGLE);
        ArrayList<MessageItemChat> groupChats = db.selectChatList(MessageFactory.CHAT_TYPE_GROUP);
        MyLog.d(TAG, "loadDbChatItem: 2");

        uniqueStore.clear();

        for (MessageItemChat msgItem : singleChats) {
            if (msgItem.getReceiverID() != null) {
                receiverID = msgItem.getReceiverID();
                String docId = uniqueCurrentID + "-" + receiverID;
                if (!session.getarchive(docId)) {
                    String msisdn = null, name = null;
                    if (msgItem.getSenderMsisdn() != null) {
                        name = getcontactname.getSendername(receiverID, msgItem.getSenderMsisdn());
                        msisdn = msgItem.getSenderMsisdn();
                    } else {

                        msisdn = contactDB_sqlite.getSingleData(receiverID, ContactDB_Sqlite.MSISDN);
                        if (msisdn != null && msisdn.length() > 0) {
                            name = getcontactname.getSendername(receiverID, msisdn);
                        }
                    }

                    if (db.getChatClearedStatus(receiverID, MessageFactory.CHAT_TYPE_SINGLE)
                            == MessageFactory.CHAT_STATUS_CLEARED) {
                        msgItem = new MessageItemChat();
                        msgItem.setReceiverID(receiverID);
                        msgItem.setMessageType("");
                        msgItem.setMessageId(docId.concat("-0"));
                        msgItem.setTextMessage("");
                        msgItem.setTS("0");
                        msgItem.setGroup(false);
                    }

                    if (msisdn != null) {
                        msgItem.setSenderMsisdn(msisdn);
                        msgItem.setSenderName(name);
                        msgItem.setGroup(false);
                        uniqueStore.put(docId, msgItem);
                    }
                }

                updateToconvSettings(uniqueCurrentID, receiverID);
            }
        }

        MyLog.d(TAG, "loadDbChatItem: 3");
        for (MessageItemChat msgItem : groupChats) {

            String Doc_Id = "";

            String groupId = msgItem.getReceiverID();
            String docId = mCurrentUserId + "-" + groupId + "-g";

            MyLog.d("GroupdID", groupId + "  ___ " + docId);

            String[] array = msgItem.getMessageId().split("-");

            if (array[0].equalsIgnoreCase(uniqueCurrentID)) {
                Doc_Id = array[1];
            } else if (array[1].equalsIgnoreCase(uniqueCurrentID)) {
                Doc_Id = array[0];
            }

            String message_docId = mCurrentUserId + "-" + Doc_Id + "-g";


            // Get group details if not updated from messageservice class
            if (!groupInfoSession.hasGroupInfo(docId)
                    || groupInfoSession.getGroupInfo(docId).getGroupName() == null) {
                getGroupDetails(groupId);
            }

            if (db.getChatClearedStatus(groupId, MessageFactory.CHAT_TYPE_GROUP)
                    == MessageFactory.CHAT_STATUS_CLEARED) {
                msgItem = new MessageItemChat();
                msgItem.setMessageType("");
                msgItem.setReceiverID(groupId);
                msgItem.setMessageId(docId.concat("-0"));
                msgItem.setTextMessage("");
                msgItem.setTS("0");
                msgItem.setGroup(true);

            } else if (db.getChatListCount(message_docId) == 0) {

                msgItem = new MessageItemChat();
                msgItem.setMessageType("");
                msgItem.setReceiverID(groupId);
                msgItem.setMessageId(docId.concat("-0"));
                msgItem.setTextMessage("");
                msgItem.setTS("0");
                msgItem.setGroup(true);
            }

            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);
            if (!session.getarchivegroup(docId)) {

                if (infoPojo != null) {
                    String avatarPath = infoPojo.getAvatarPath();
                    msgItem.setAvatarImageUrl(avatarPath);
                    msgItem.setSenderName(infoPojo.getGroupName());
                    msgItem.setGroupName(infoPojo.getGroupName());
                    msgItem.setGroup(true);
                }
                uniqueStore.put(docId, msgItem);

            }
        }
        MyLog.d(TAG, "loadDbChatItem: 4");
        updateChatList();
        setArchieveText();
        MyLog.d(TAG, "loadDbChatItem: 5");
        progressBar.setVisibility(View.GONE);
    }

    private void updateToconvSettings(String from, String to) {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_TO_CONV_SETTING);
        try {
            JSONObject obj = new JSONObject();
            obj.put("from", from);
            obj.put("to", to);
            obj.put("type", "single");
            obj.put("chat_type", "normal");
            messageEvent.setMessageObject(obj);
            EventBus.getDefault().post(messageEvent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void getGroupDetails(String groupId) {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_GROUP_DETAILS);

        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            object.put("convId", groupId);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void updateChatList() {

        try {
            MyLog.d(TAG, "updateChatList: ");
            mChatData.clear();

            for (MessageItemChat value : uniqueStore.values()) {

                if (value.getMessageId() != null && value.getMessageId().contains("-g-")) {

                    value.setSenderName(value.getGroupName());

                } else {
                    if (value.getSenderName() == null || value.getSenderName().isEmpty()) {
                        String phContactName = getcontactname.getSendername(value.getReceiverID(), value.getSenderMsisdn());
                        value.setSenderName(phContactName);
                    }

                }
                String groupName = value.getGroupName();
                if (value.isGroup() && groupName != null && groupName.length() > 0) {
                    MyLog.d(TAG, "updateChatList: groupname :" + groupName);
                    value.setSenderName(value.getGroupName());
/*                    Log.d(TAG, "groupmsgmiss groupname: "+value.getSenderName());
                    Log.d(TAG, "groupmsgmiss msgType: "+value.getMessageType());
                    Log.d(TAG, "groupmsgmiss msg: "+value.getTextMessage());*/
                    value.setadshowstatus(false);
                    mChatData.add(value);

                } else {

                    value.setadshowstatus(false);
                    mChatData.add(value);
                }

            }
            if (mChatData.size() > 0) {
                userMessagechat.setVisibility(View.GONE);
            }
            Collections.sort(mChatData, new ChatListSorter());
            //add dummy item in chatlist for advt
           /* if(sessionManager.gethomepageadintegrationposition().equalsIgnoreCase("list")){

                if(mChatData.size()>1){

                    MessageItemChat pojo=mChatData.get(1);
                    mChatData.remove(1);

                    MessageItemChat pojo1=new MessageItemChat();
                    pojo1.setadshowstatus(true);
                    mChatData.add(pojo1);
                    mChatData.add(pojo);

                }

            }*/
            chat_list.post(new Runnable() {
                @Override
                public void run() {

                    mAdapter.notifyDataSetChanged();

                }
            });


        } catch (Exception e) {

            MyLog.e(TAG, "UpdateChatError", e);
        }

    }

    private void setArchieveText() {
        archivecount = String.valueOf(session.getarchivecount());
        archivecount_group = String.valueOf(session.getarchivecountgroup());

        int count = Integer.parseInt(archivecount) + Integer.parseInt(archivecount_group);

        if (session.getarchivecount() <= 0 && session.getarchivecountgroup() <= 0) {
            archive_relativelayout.setVisibility(View.GONE);
        } else {
            archive_relativelayout.setVisibility(View.VISIBLE);
            archivetext.setText("Archived chats (" + count + ")");
        }

    }

    @Override
    public void onMuteDialogClosed(boolean isMuted) {

        loadDbChatItem();
    }


    /*private ChatLockPojo getChatLockdetailfromDB(int position, MessageItemChat list) {
        MessageItemChat e = mAdapter.getItems(position);
        String docID = e.getMessageId();
        String[] id = docID.split("-");
        ChatLockPojo lockPojo;
        if (list.isGroup()) {
            String documentID = id[0] + "-" + id[1] + "-g";
            String convId = userInfoSession.getChatConvId(documentID);
            String receiverId = userInfoSession.getReceiverIdByConvId(convId);
            lockPojo = db.getChatLockData(receiverId, MessageFactory.CHAT_TYPE_GROUP);
        } else {
            String documentID = id[0] + "-" + id[1];
            String convId = userInfoSession.getChatConvId(documentID);
            String receiverId = userInfoSession.getReceiverIdByConvId(convId);
            lockPojo = db.getChatLockData(receiverId, MessageFactory.CHAT_TYPE_SINGLE);
        }

        return lockPojo;
    }*/

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


    }

    @Override
    public void onPause() {
        super.onPause();
        // Stop update of typing message
        if (typingRunnable != null) {
            typingHandler.removeCallbacks(typingRunnable);
        }
        unregisterCallReceiver();
        isChatListPage = false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach: ");
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        MyLog.d(TAG, "onResume: 1");
        registerCallReceiver();
        receiver = new Receiver();
        /*IntentFilter filter = new IntentFilter();
        filter.addAction("com.selectedchat.remove");
        getActivity().registerReceiver(receiver, filter);*/

        checkcallstatus();
        if (mChatData == null || mChatData.size() == 0) {
            progressBar.setVisibility(View.VISIBLE);
        }

        if (menuNormal != null)
            showChatIcon(menuNormal);

        setArchieveText();
        try {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
            } else {
                ShortcutBadger.removeCount(getContext()); //for 1.1.4+
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
        final Handler handler = new Handler();
    /*    handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                mAdapter.notifyDataSetChanged();
            }
        }, 1000);*/
        mDBrefresh = SharedPreference.getInstance().getBool(getActivity(), "dbrefresh");
        if (!mDBrefresh) {
            int delay = 10000;
            if (CoreController.isNobelNet)
                delay = 20000;
            SmarttyContactsService.bindContactService(getActivity(), true);
            CoreActivity.showProgres();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms
                    SharedPreference.getInstance().saveBool(getActivity(), "dbrefresh", true);
                    loadDbChatItem();
                    CoreActivity.hidepDialog();
                }
            }, delay);
        } else {
            //loadDbChatItem();
            SmarttyContactsService.bindContactService(getActivity(), true);
            CoreActivity.showProgres();
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    loadDbChatItem();
                }
            }, 100);
//            mAdapter.notifyDataSetChanged();

//new DbAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            isChatListPage = true;
        }
        MyLog.d(TAG, "onResume: 2");
    }

    @Override
    public void onStop() {

       /* try {
            if (receiver != null && getActivity() != null) {
                //Need to do it on Destory
                try {
                    MyLog.d(TAG, "onStop: ");
                    getActivity().unregisterReceiver(receiver);
                } catch (Exception e) {
                    MyLog.e(TAG, "onStop: ", e);
                }
            }*/


       /*     if (myReceiver != null) {
                try {
                    getActivity().unregisterReceiver(myReceiver);
                } catch (Exception e) {
                    Log.e(TAG, "onStop: ", e);
                }
            }*/

       /* } catch (Exception e) {

        }*/
        super.onStop();
        EventBus.getDefault().unregister(this);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem secretMenuItem = menu.findItem(R.id.select_secret_chat_contact);
        if (secretMenuItem != null) {
            if (sessionManager.getSecretChatEnabled().equals("1")) {
                // secretMenuItem.setVisible(true);
                secretMenuItem.setVisible(false);

            } else {
                secretMenuItem.setVisible(false);
            }
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        MyLog.e("flag", "" + myMenuClickFlag);
        menuNormal = menu;
        menuLongClick = menu;

        if (isthisGroup) {
            if (myMenuClickFlag) {
                getActivity().getMenuInflater().inflate(R.menu.grouplist_longclick_menu, menu);
                MenuItem markasunread_menu = menu.findItem(R.id.markasunread_menu);
                MenuItem mute = menu.findItem(R.id.mute_menu);
                MenuItem unmute = menu.findItem(R.id.unmute_menu);
                MenuItem markread_menu = menu.findItem(R.id.markread_menu);
                MenuItem Exitgroup = menu.findItem(R.id.Exitgroup);
                MenuItem add_shortcut = menu.findItem(R.id.addchatshortcut_menu);
                final MenuItem Groupinfo = menu.findItem(R.id.Groupinfo);
                MenuItem Deletegroup = menu.findItem(R.id.delete);
                MenuItem archive = menu.findItem(R.id.archive_menu);
                ArrayList<Boolean> exitanddelete = new ArrayList<>();

                for (int i = 0; i < selectedChatItems.size(); i++) {
                    String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                    String receiverId = arrIds[1];
                    boolean hasGroupInfo = groupInfoSession.hasGroupInfo(mCurrentUserId + "-" + receiverId + "-g");
                    GroupInfoPojo infoPojo;
                    if (hasGroupInfo) {
                        infoPojo = groupInfoSession.getGroupInfo(mCurrentUserId + "-" + receiverId + "-g");
                        if (infoPojo.isLiveGroup()) {
                            exitanddelete.add(infoPojo.isLiveGroup());
                        } else {
                            exitanddelete.add(infoPojo.isLiveGroup());
                        }
                    }
                }
                boolean group = false;
                boolean single = false;
                if (exitanddelete.contains(true) && exitanddelete.contains(false)) {
                    Exitgroup.setVisible(false);
                    Deletegroup.setVisible(false);
                } else if (exitanddelete.contains(true)) {
                    for (int m = 0; m <= selectedChatItems.size() - 1; m++) {
                        if (selectedChatItems.get(m).isGroup()) {
                            group = true;
                        } else {
                            single = true;
                        }
                    }
                    if (group == true && single == false) {
                        Exitgroup.setVisible(true);
                    } else {
                        Exitgroup.setVisible(false);
                    }
                    Deletegroup.setVisible(false);
                } else {
                    Exitgroup.setVisible(false);
                    for (int m = 0; m <= selectedChatItems.size() - 1; m++) {
                        if (selectedChatItems.get(m).isGroup()) {
                            group = true;
                        } else {
                            single = true;
                        }
                    }
                    if (group == true && single == false) {
                        Deletegroup.setVisible(true);
                    } else {
                        Deletegroup.setVisible(false);
                    }

                }
                if (count >= 2) {
                    add_shortcut.setVisible(false);
                    Groupinfo.setVisible(false);

                }

                ArrayList<Boolean> mutearray = new ArrayList<>();


                for (int i = 0; i < selectedChatItems.size(); i++) {
                    String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                    String groupId = arrIds[1];
                    MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(mCurrentUserId, null, groupId, false);

                    if (muteData != null && muteData.getMuteStatus().equals("1")) {
                        mutearray.add(true);
                    /*mute.setVisible(true);
                    unmute.setVisible(false);*/
                    } else {
                        mutearray.add(false);
                    /*mute.setVisible(false);
                    unmute.setVisible(true);*/
                    }
                }
                if (mutearray.contains(true) && mutearray.contains(false)) {
                    mute.setVisible(false);
                    unmute.setVisible(false);
                } else if (mutearray.contains(true)) {
                    mute.setVisible(false);
                    unmute.setVisible(true);
                } else {
                    mute.setVisible(true);
                    unmute.setVisible(false);
                }
                ArrayList<String> mark = new ArrayList<>();
                for (int i = 0; i < selectedChatItems.size(); i++) {
                    String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");

                    String receiverId = arrIds[1];
                    String docId = mCurrentUserId.concat("-").concat(receiverId);
                    String convId = null;
                    if (selectedChatItems.get(i).getMessageId().contains("-g-")) {
                        convId = receiverId;
                    } else {
                        convId = userInfoSession.getChatConvId(docId);
                    }
                    int countMsg = 0;
                    if (convId != null && !convId.isEmpty())
                        countMsg = shortcutBadgeManager.getSingleBadgeCount(convId);


                    if (countMsg > 0) {
                        mark.add(Constants.UNREAD_MSG);
                    }
                    if (session.getmark(receiverId)) {
                        mark.add("" + session.getmark(receiverId));
                    } else {
                        mark.add("" + false);
                    }
                }
                if (mark.contains(Constants.UNREAD_MSG)) {
                    markread_menu.setVisible(true);
                    markasunread_menu.setVisible(false);
                } else {
                    if (mark.contains("true")) {
                        markread_menu.setVisible(false);
                        markasunread_menu.setVisible(true);
                    } else {
                        markread_menu.setVisible(true);
                        markasunread_menu.setVisible(false);
                    }
                }
            /*if (markflag) {
                markasunread_menu.setVisible(true);
                markread_menu.setVisible(false);
            } else {
                markasunread_menu.setVisible(false);
                markread_menu.setVisible(true);
            }*/
                add_shortcut.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        for (int i = 0; i < selectedChatItems.size(); i++) {
                            addShortcut(selectedChatItems.get(i).getAvatarImageUrl(), selectedChatItems.get(i).isGroup());
                            int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                            mChatData.get(chatIndex).setSelected(!selectedChatItems.get(i).isSelected()); // setchangemenu();
                        }
                        myMenuClickFlag = false;
                        count = 0;
                        mAdapter.notifyDataSetChanged();

                        onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        return false;
                    }
                });
                Groupinfo.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        //setchangemenu();
                        viewcontact(true);
                        myMenuClickFlag = false;
                        mAdapter.notifyDataSetChanged();

                        count = 0;
                        onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        return false;
                    }
                });
                Exitgroup.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        showExitGroupAlert();
                        return false;
                    }
                });
                Deletegroup.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        for (int i = 0; i < selectedChatItems.size(); i++) {
                            try {
                                String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                                String receiverId = arrIds[1];
                                String docId = mCurrentUserId.concat("-").concat(receiverId).concat("-g");

                                try {
                                    JSONObject object = new JSONObject();
                                    object.put("convId", receiverId);
                                    object.put("from", mCurrentUserId);
                                    object.put("type", MessageFactory.CHAT_TYPE_GROUP);

                                    SendMessageEvent event = new SendMessageEvent();
                                    event.setEventName(SocketManager.EVENT_DELETE_CHAT);
                                    event.setMessageObject(object);
                                    EventBus.getDefault().post(event);
                                } catch (JSONException e) {
                                    MyLog.e(TAG, "", e);
                                }


                                db.deleteChat(docId, MessageFactory.CHAT_TYPE_GROUP);

                            } catch (Exception e) {
                                MyLog.e(TAG, "", e);
                            }
                        }
                        selectedChatItems.clear();
                        loadDbChatItem();
                        myMenuClickFlag = false;
                        count = 0;
                        mAdapter.notifyDataSetChanged();

                        onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        return false;
                    }
                });
                markasunread_menu.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        for (int i = 0; i < selectedChatItems.size(); i++) {
                            try {
                                String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                                String receiverId = arrIds[1];
//                            String docId = mCurrentUserId.concat("-").concat(receiverId).concat("-g");
                                try {
                                    JSONObject object = new JSONObject();
                                    object.put("convId", receiverId);
                                    object.put("from", mCurrentUserId);
                                    object.put("status", 1);
                                    object.put("type", "group");
                                    SendMessageEvent event = new SendMessageEvent();
                                    event.setEventName(SocketManager.EVENT_MARKREAD);
                                    event.setMessageObject(object);
                                    EventBus.getDefault().post(event);
                                } catch (JSONException e) {
                                    MyLog.e(TAG, "", e);
                                }
                                session.putmark(receiverId);
                                //System.out.println("docid" + receiverId);

                                int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                                mChatData.get(chatIndex).setSelected(!selectedChatItems.get(i).isSelected());


                                NewHomeScreenActivty activity = (NewHomeScreenActivty) getActivity();
                                activity.changeTabTextCount();

                                // Badge working if supported devices

                            } catch (Exception e) {
                                MyLog.e(TAG, "", e);
                            }
                            // setchangemenu();
                        }
                        myMenuClickFlag = false;
                        mAdapter.notifyDataSetChanged();

                        selectedChatItems.clear();
                        count = 0;
                        onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        return false;
                    }
                });
                markread_menu.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        ShortcutBadgeManager shortcutBadgeMgnr = new ShortcutBadgeManager(getActivity());

                        for (int i = 0; i < selectedChatItems.size(); i++) {
                            try {
                                String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                                String receiverId = arrIds[1];
                                String docId = uniqueCurrentID.concat("-").concat(receiverId);
                                String msgId = selectedChatItems.get(i).get_id();
                                try {
                                    JSONObject object = new JSONObject();
                                    object.put("convId", receiverId);
                                    object.put("from", mCurrentUserId);
                                    object.put("status", 0);
                                    object.put("type", "group");
                                    SendMessageEvent event = new SendMessageEvent();
                                    event.setEventName(SocketManager.EVENT_MARKREAD);
                                    event.setMessageObject(object);
                                    EventBus.getDefault().post(event);
                                } catch (JSONException e) {
                                    MyLog.e(TAG, "", e);
                                }
                                sendAckToServer(receiverId, docId, msgId, selectedChatItems.get(i).getConvId());
                                session.Removemark(receiverId);
                                //System.out.println("docid" + receiverId);
                                int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                                mChatData.get(chatIndex).setSelected(!selectedChatItems.get(i).isSelected());

                                shortcutBadgeMgnr.removeMessageCount(receiverId, msgId);
                                int totalCount = shortcutBadgeMgnr.getTotalCount();

                                NewHomeScreenActivty activity = (NewHomeScreenActivty) getActivity();
                                activity.changeTabTextCount();

                                // Badge working if supported devices
                                try {
                                    ShortcutBadger.applyCount(getActivity(), totalCount);
                                } catch (Exception e) {
                                    MyLog.e(TAG, "", e);
                                }

                            } catch (Exception e) {
                                MyLog.e(TAG, "", e);
                            }

                            // setchangemenu();
                        }
                        myMenuClickFlag = false;
                        count = 0;
                        for (int i = 0; i < selectedChatItems.size(); i++) {
                            addShortcut(selectedChatItems.get(i).getAvatarImageUrl(), selectedChatItems.get(i).isGroup());
                            int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                            mChatData.get(chatIndex).setSelected(!selectedChatItems.get(i).isSelected()); // setchangemenu();
                        }
                        selectedChatItems.clear();
                        mAdapter.notifyDataSetChanged();

                        return false;
                    }
                });
                unmute.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (ConnectivityInfo.isInternetConnected(getContext())) {
                            for (int i = 0; i < selectedChatItems.size(); i++) {
                                try {
                                    String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                                    String receiverId = arrIds[1];

                                    MuteUnmute.performUnMute(getActivity(), EventBus.getDefault(), receiverId,
                                            MessageFactory.CHAT_TYPE_GROUP, "no");

                                    int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                                    if (chatIndex > -1) {
                                        mChatData.get(chatIndex).setSelected(!selectedChatItems.get(i).isSelected());
                                    }
                                    mAdapter.notifyDataSetChanged();

                                } catch (Exception e) {
                                    MyLog.e(TAG, "", e);
                                }
                            }

                            myMenuClickFlag = false;
                            mAdapter.notifyDataSetChanged();

                            selectedChatItems.clear();
                            count = 0;
                            onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        } else {
                            Toast.makeText(getActivity(), "Check Your Network connection", Toast.LENGTH_SHORT).show();
                        }
                        return false;

                    }
                });
                mute.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (ConnectivityInfo.isInternetConnected(getContext())) {

                            ArrayList<MuteUserPojo> muteUserList = new ArrayList<>();
                            for (int i = 0; i < selectedChatItems.size(); i++) {
                                String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                                String receiverId = arrIds[1];

                                String chatType = MessageFactory.CHAT_TYPE_GROUP;
                                if (selectedChatItems.get(i).getMessageId().contains("-g")) {
                                    chatType = MessageFactory.CHAT_TYPE_GROUP;
                                }

                                MuteUserPojo muteUserPojo = new MuteUserPojo();
                                muteUserPojo.setReceiverId(receiverId);
                                muteUserPojo.setChatType(chatType);
                                muteUserPojo.setSecretType("no");
                                muteUserList.add(muteUserPojo);

                                int index = mChatData.indexOf(selectedChatItems.get(i));
                                if (index > -1) {
                                    mChatData.get(index).setSelected(false);
                                }
                            }

                            Bundle putBundle = new Bundle();
                            putBundle.putSerializable("MuteUserList", muteUserList);

                            MuteAlertDialog dialog = new MuteAlertDialog();
                            dialog.setArguments(putBundle);
                            dialog.setCancelable(false);
                            dialog.setMuteAlertCloseListener(ChatListFragment.this);
                            dialog.show(getFragmentManager(), "Mute");

                            selectedChatItems.clear();
                            myMenuClickFlag = false;
                            mAdapter.notifyDataSetChanged();

                            count = 0;
                            onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        } else {
                            Toast.makeText(getActivity(), "Check Network Connection", Toast.LENGTH_SHORT).show();

                        }

                        return false;
                    }

                });
                archive.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String receiverId = "";
                        for (int i = 0; i < selectedChatItems.size(); i++) {
                            try {
                                String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                                receiverId = arrIds[1];
                                String docId = mCurrentUserId.concat("-").concat(receiverId);
                                if (!session.getarchivegroup(mCurrentUserId + "-" + receiverId + "-g")) {
                                    session.putarchivegroup(mCurrentUserId + "-" + receiverId + "-g");
                                }
                                try {
                                    JSONObject object = new JSONObject();
                                    object.put("convId", receiverId);
                                    object.put("from", mCurrentUserId);
                                    object.put("status", 1);
                                    object.put("type", MessageFactory.CHAT_TYPE_GROUP);
                                    SendMessageEvent event = new SendMessageEvent();
                                    event.setEventName(SocketManager.EVENT_ARCHIVE_UNARCHIVE);
                                    event.setMessageObject(object);
                                    EventBus.getDefault().post(event);
                                } catch (JSONException e) {
                                    MyLog.e(TAG, "", e);
                                }
                            } catch (Exception e) {
                                MyLog.e(TAG, "", e);
                            }
                        }
                       /* int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                        mChatData.remove(chatIndex);
                        mAdapter.notifyDataSetChanged();*/

                        setArchieveText();
                        selectedChatItems.clear();
                        myMenuClickFlag = false;
                        loadDbChatItem();
                        mAdapter.notifyDataSetChanged();

                        count = 0;
                        onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        return false;
                    }
                });
            } else {
                //System.out.println("-------chats_screen---------");
                getActivity().getMenuInflater().inflate(R.menu.chats_screen, menu);
                MenuItem logout = menu.findItem(R.id.logout);
                logout.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        logOut(true, getString(R.string.web_logout_msg));
                        return true;
                    }
                });


                MenuItem mobileLogout = menu.findItem(R.id.mobile_logout);
                mobileLogout.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        logOut(false, getString(R.string.mobile_logout_msg));
                        return true;
                    }
                });


                MenuItem searchItem = menu.findItem(R.id.chatsc_searchIcon);
                searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        searchView.setIconifiedByDefault(true);
                        searchView.setIconified(true);
                        searchView.setQuery("", false);
                        searchView.clearFocus();
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {

                        if (newText.equals("") && newText.isEmpty()) {
                            searchView.clearFocus();
                        }
                        mAdapter.getFilter().filter(newText);
                        return false;
                    }
                });

                searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                    @Override
                    public boolean onClose() {

                        showChatIcon(menu);
                        return false;
                    }
                });

                searchView.setOnSearchClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideChatIcon(menu);

                    }
                });

                SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
                searchView.setIconifiedByDefault(true);
                searchView.setQuery("", false);
                searchView.clearFocus();
                searchView.setIconified(true);

                AutoCompleteTextView searchTextView = searchView.findViewById(R.id.search_src_text);
                try {
                    Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
                    mCursorDrawableRes.setAccessible(true);
                    mCursorDrawableRes.set(searchTextView, 0);
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            }
        } else {
            if (myMenuClickFlag) {
                //System.out.println("-------chatlist_longclick_menu---------");
                getActivity().getMenuInflater().inflate(R.menu.chatlist_longclick_menu, menu);
                MenuItem add_shortcut = menu.findItem(R.id.addchatshortcut_menu);
                final MenuItem viewcontact = menu.findItem(R.id.viewcontact_menu);
                MenuItem markasunread_menu = menu.findItem(R.id.markasunread_menu);
                MenuItem addcontact_menu = menu.findItem(R.id.adddcontact_menu);
                MenuItem mute = menu.findItem(R.id.mute_menu);
                MenuItem unmute = menu.findItem(R.id.unmute_menu);
                final MenuItem delete = menu.findItem(R.id.delete_menu);
                MenuItem markread_menu = menu.findItem(R.id.markread_menu);
                final MenuItem archive = menu.findItem(R.id.archive_menu);
                addcontact_menu.setVisible(false);
                if (count >= 2) {
                    addcontact_menu.setVisible(false);
                    add_shortcut.setVisible(false);
                    viewcontact.setVisible(false);
                    delete.setVisible(false);
                    markasunread_menu.setVisible(false);
                    markread_menu.setVisible(false);
                }

                ArrayList<Boolean> mutearray = new ArrayList<>();

                for (int i = 0; i < selectedChatItems.size(); i++) {
                    String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                    String receiverId = arrIds[1];
                    String docId = uniqueCurrentID + "-" + receiverId;
                    String convId = userInfoSession.getChatConvId(docId);
                    MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(uniqueCurrentID, receiverId, convId, false);
                    mutearray.add((muteData != null && muteData.getMuteStatus().equals("1")));

                    /*if (sessionManager.getMuteStatus(docId)) {

                     *//*mute.setVisible(true);
                    unmute.setVisible(false);*//*
                } else {
                    mutearray.add(false);
                    *//*mute.setVisible(false);
                    unmute.setVisible(true);*//*
                }*/
                }
                if (mutearray.contains(true) && mutearray.contains(false)) {
                    mute.setVisible(false);
                    unmute.setVisible(false);
                } else if (mutearray.contains(true)) {
                    mute.setVisible(false);
                    unmute.setVisible(true);
                } else {
                    mute.setVisible(true);
                    unmute.setVisible(false);
                }


                ArrayList<String> mark = new ArrayList<>();
                for (int i = 0; i < selectedChatItems.size(); i++) {
                    String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");

                    String receiverId = arrIds[1];
                    String docId = mCurrentUserId.concat("-").concat(receiverId);
                    String convId = null;
                    if (selectedChatItems.get(i).getMessageId().contains("-g-")) {
                        convId = receiverId;
                    } else {
                        convId = userInfoSession.getChatConvId(docId);
                    }
                    int countMsg = 0;
                    if (convId != null && !convId.isEmpty())
                        countMsg = shortcutBadgeManager.getSingleBadgeCount(convId);

/*                    if (session.getmark(receiverId)) {
                        mark.add(session.getmark(receiverId));
                    } else {
                        mark.add(false);
                    }*/

                    if (countMsg > 0) {
                        mark.add(Constants.UNREAD_MSG);
                    }
                    if (session.getmark(receiverId)) {
                        mark.add("" + session.getmark(receiverId));
                    } else {
                        mark.add("" + false);
                    }
                }
                if (mark.contains(Constants.UNREAD_MSG)) {
                    markread_menu.setVisible(true);
                    markasunread_menu.setVisible(false);
                } else {
                    if (mark.contains("true")) {
                        markread_menu.setVisible(false);
                        markasunread_menu.setVisible(true);
                    } else {
                        markread_menu.setVisible(true);
                        markasunread_menu.setVisible(false);
                    }
                }
            /*if (markflag) {
                markasunread_menu.setVisible(true);
                markread_menu.setVisible(false);
            } else {
                markasunread_menu.setVisible(false);
                markread_menu.setVisible(true);
            }*/
                if (selectedChatItems.size() == 1) {
                    String[] arrIds = selectedChatItems.get(0).getMessageId().split("-");
                    String receiverId = arrIds[1];
                    long contactRevision = contactDB_sqlite.getOpponenet_UserDetails_savedRevision(receiverId);
//                long contactRevision = contactsDB.getSavedRevision(receiverId);
                    long syncedRevision = sessionManager.getContactSavedRevision();
                    if (syncedRevision > contactRevision) {
                        addcontact_menu.setVisible(true);
                    }
                }
                if (selectedChatItems.size() == 1 && selectedChatItems.get(0).isSecretChat()) {
                    archive.setVisible(false);
                    markasunread_menu.setVisible(false);
                    markread_menu.setVisible(false);
                    mute.setVisible(false);
                    unmute.setVisible(false);
                }
                addcontact_menu.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                        intent.putExtra(ContactsContract.Intents.Insert.PHONE, phoneno);
                        startActivity(intent);
                        for (int i = 0; i < mChatData.size(); i++) {
                            mChatData.get(i).setSelected(false);
                        }
                        selectedChatItems.clear();
                        myMenuClickFlag = false;
                        count = 0;
                        mAdapter.notifyDataSetChanged();

                        onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        return false;
                    }
                });
                delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String msg = "";
                        if (selectedChatItems.size() > 1) {
                            msg = "Delete" + " " + selectedChatItems.size() + " " + "chats?";
                        } else {
                            msg = "Are you sure you want to delete chat messages in this chat?";
                        }
                        final CustomAlertDialog dialog = new CustomAlertDialog();
                        dialog.setNegativeButtonText("Cancel");
                        dialog.setPositiveButtonText("Delete");
                        dialog.setMessage(msg);
                        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
                            @Override
                            public void onPositiveButtonClick() {
                                for (int i = 0; i < selectedChatItems.size(); i++) {
                                    try {
                                        String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                                        String receiverId = arrIds[1];
                                        String docId = uniqueCurrentID.concat("-").concat(receiverId);
                                        boolean isSecretChat = selectedChatItems.get(i).isSecretChat();
                                        if (isSecretChat) {
                                            db.deleteChat(docId + "-secret", MessageFactory.CHAT_TYPE_SECRET);
                                            if (selectedChatItems.size() == 1)
                                                isSecretChatDeleted = true;
                                        } else {
                                            db.deleteChat(docId, MessageFactory.CHAT_TYPE_SINGLE);
                                        }
                                        if (!isSecretChat && userInfoSession.hasChatConvId(docId)) {
                                            try {
                                                String convId = userInfoSession.getChatConvId(docId);

                                                JSONObject object = new JSONObject();
                                                object.put("convId", convId);
                                                object.put("from", uniqueCurrentID);
                                                object.put("type", MessageFactory.CHAT_TYPE_SINGLE);

                                                SendMessageEvent event = new SendMessageEvent();
                                                event.setEventName(SocketManager.EVENT_DELETE_CHAT);
                                                event.setMessageObject(object);
                                                EventBus.getDefault().post(event);
                                            } catch (JSONException e) {
                                                MyLog.e(TAG, "", e);
                                            }
                                        }


                                    } catch (Exception e) {
                                        MyLog.e(TAG, "", e);
                                    }
                                }
                                selectedChatItems.clear();
                                loadDbChatItem();
                                myMenuClickFlag = false;
                                count = 0;
                                mAdapter.notifyDataSetChanged();

                                onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                            }

                            @Override
                            public void onNegativeButtonClick() {
                                selectedChatItems.clear();
                                loadDbChatItem();
                                myMenuClickFlag = false;
                                count = 0;
                                mAdapter.notifyDataSetChanged();

                                onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                                dialog.dismiss();
                            }
                        });
                        dialog.show(getFragmentManager(), "Delete member alert");
                        return false;
                    }
                });
                add_shortcut.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        for (int i = 0; i < selectedChatItems.size(); i++) {
                            try {
                                addShortcut(selectedChatItems.get(i).getAvatarImageUrl(), selectedChatItems.get(i).isGroup());
                                int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                                mChatData.get(chatIndex).setSelected(!selectedChatItems.get(i).isSelected()); // setchangemenu();
                            } catch (Exception e) {
                                Log.e(TAG, "onMenuItemClick: ", e);
                            }

                        }
                        myMenuClickFlag = false;
                        count = 0;
                        mAdapter.notifyDataSetChanged();

                        onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        return false;
                    }
                });
                viewcontact.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        //setchangemenu();
                        viewcontact(false);
                        myMenuClickFlag = false;
                        mAdapter.notifyDataSetChanged();

                        count = 0;
                        //onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        return false;
                    }
                });
                markasunread_menu.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {


                        if (ConnectivityInfo.isInternetConnected(getContext())) {
                            for (int i = 0; i < selectedChatItems.size(); i++) {
                                try {
                                    String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                                    String receiverId = arrIds[1];
                                    String docId = uniqueCurrentID.concat("-").concat(receiverId);
                                    if (userInfoSession.hasChatConvId(docId)) {
                                        String convId = userInfoSession.getChatConvId(docId);
                                        try {
                                            JSONObject object = new JSONObject();
                                            object.put("convId", convId);
                                            object.put("from", uniqueCurrentID);
                                            object.put("status", 1);
                                            object.put("type", "single");
                                            SendMessageEvent event = new SendMessageEvent();
                                            event.setEventName(SocketManager.EVENT_MARKREAD);
                                            event.setMessageObject(object);
                                            EventBus.getDefault().post(event);
                                        } catch (JSONException e) {
                                            MyLog.e(TAG, "", e);
                                        }
                                    }
                                    session.putmark(receiverId);
                                    //System.out.println("docid" + receiverId);

                                    int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                                    mChatData.get(chatIndex).setSelected(!selectedChatItems.get(i).isSelected());


                                    NewHomeScreenActivty activity = (NewHomeScreenActivty) getActivity();
                                    activity.changeTabTextCount();
                                } catch (Exception e) {
                                    MyLog.e(TAG, "", e);
                                }

                                // Badge working if supported devices


                                // setchangemenu();

                            }

                        } else {
                            for (int i = 0; i < selectedChatItems.size(); i++) {
                                int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                                mChatData.get(chatIndex).setSelected(!selectedChatItems.get(i).isSelected());
                                mAdapter.notifyDataSetChanged();

                            }
                            Toast.makeText(getActivity(), "Check Network Connection", Toast.LENGTH_SHORT).show();
                        }
                        myMenuClickFlag = false;
                        mAdapter.notifyDataSetChanged();

                        selectedChatItems.clear();
                        count = 0;
                        onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        return false;
                    }
                });
                markread_menu.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        ShortcutBadgeManager shortcutBadgeMgnr = new ShortcutBadgeManager(getActivity());

                        if (ConnectivityInfo.isInternetConnected(getContext())) {
                            for (int i = 0; i < selectedChatItems.size(); i++) {
                                try {
                                    if (selectedChatItems.size() > 0) {
                                        String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                                        String receiverId = arrIds[1];
                                        String docId = uniqueCurrentID.concat("-").concat(receiverId);
                                        String msgId = selectedChatItems.get(i).get_id();
                                        if (userInfoSession.hasChatConvId(docId)) {
                                            String convId = userInfoSession.getChatConvId(docId);
                                            try {
                                                JSONObject object = new JSONObject();
                                                object.put("convId", convId);
                                                object.put("from", uniqueCurrentID);
                                                object.put("status", 0);
                                                object.put("type", "single");
                                                SendMessageEvent event = new SendMessageEvent();
                                                event.setEventName(SocketManager.EVENT_MARKREAD);
                                                event.setMessageObject(object);
                                                EventBus.getDefault().post(event);
                                            } catch (JSONException e) {
                                                MyLog.e(TAG, "", e);
                                            }
                                            sendAckToServer(receiverId, docId, msgId, selectedChatItems.get(i).getConvId());
                                            shortcutBadgeMgnr.removeMessageCount(convId, msgId);
                                            int totalCount = shortcutBadgeMgnr.getTotalCount();

                                            NewHomeScreenActivty activity = (NewHomeScreenActivty) getActivity();
                                            activity.changeTabTextCount();

                                            // Badge working if supported devices
                                            try {
                                                ShortcutBadger.applyCount(getActivity(), totalCount);
                                            } catch (Exception e) {
                                                MyLog.e(TAG, "", e);
                                            }
                                        }

                                        session.Removemark(receiverId);
                                        //System.out.println("docid" + receiverId);
                                        int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                                        mChatData.get(chatIndex).setSelected(!selectedChatItems.get(i).isSelected());
                                    }
                                } catch (Exception e) {
                                    MyLog.e(TAG, "", e);
                                }
                                // setchangemenu();
                            }
                            myMenuClickFlag = false;
                            for (int i = 0; i < selectedChatItems.size(); i++) {
                                int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                                mChatData.get(chatIndex).setSelected(!selectedChatItems.get(i).isSelected());
                            }
                            selectedChatItems.clear();
                            mAdapter.notifyDataSetChanged();


                            count = 0;
                            //onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        } else {
                            for (int i = 0; i < selectedChatItems.size(); i++) {
                                int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                                mChatData.get(chatIndex).setSelected(!selectedChatItems.get(i).isSelected());
                            }
                            mAdapter.notifyDataSetChanged();

                            Toast.makeText(getActivity(), "Check Network Connection", Toast.LENGTH_SHORT).show();

                        }
                        return false;
                    }
                });
                unmute.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (ConnectivityInfo.isInternetConnected(getContext())) {
                            for (int i = 0; i < selectedChatItems.size(); i++) {
                                try {
                                    String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                                    String receiverId = arrIds[1];

                                    MuteUnmute.performUnMute(getActivity(), EventBus.getDefault(), receiverId,
                                            MessageFactory.CHAT_TYPE_SINGLE, "no");

                                    int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                                    if (chatIndex > -1) {
                                        mChatData.get(chatIndex).setSelected(!selectedChatItems.get(i).isSelected());
                                    }
                                } catch (Exception e) {
                                    MyLog.e(TAG, "", e);
                                }
                            }
                            mAdapter.notifyDataSetChanged();

                            selectedChatItems.clear();
                            myMenuClickFlag = false;
                            count = 0;
                            onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        } else {
                            for (int i = 0; i < selectedChatItems.size(); i++) {
                                int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                                mChatData.get(chatIndex).setSelected(!selectedChatItems.get(i).isSelected());
                                mAdapter.notifyDataSetChanged();

                            }
                            Toast.makeText(getActivity(), "Check Network Connection", Toast.LENGTH_SHORT).show();
                        }
                        myMenuClickFlag = false;
                        mAdapter.notifyDataSetChanged();

                        selectedChatItems.clear();

                        count = 0;
                        onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        return false;

                    }
                });
                mute.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (ConnectivityInfo.isInternetConnected(getContext())) {

                            ArrayList<MuteUserPojo> muteUserList = new ArrayList<>();
                            for (int i = 0; i < selectedChatItems.size(); i++) {
                                boolean isSecretChat = selectedChatItems.get(i).isSecretChat();
                                //skip the mute option for secret chat
                                if (isSecretChat)
                                    continue;
                                String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                                String receiverId = arrIds[1];

                                String chatType = MessageFactory.CHAT_TYPE_SINGLE;
                                if (selectedChatItems.get(i).getMessageId().contains("-g")) {
                                    chatType = MessageFactory.CHAT_TYPE_GROUP;
                                }

                                MuteUserPojo muteUserPojo = new MuteUserPojo();
                                muteUserPojo.setReceiverId(receiverId);
                                muteUserPojo.setChatType(chatType);
                                muteUserPojo.setSecretType("no");
                                muteUserList.add(muteUserPojo);

                                int index = mChatData.indexOf(selectedChatItems.get(i));
                                if (index > -1) {
                                    mChatData.get(index).setSelected(false);
                                }
                            }

                            Bundle putBundle = new Bundle();
                            putBundle.putSerializable("MuteUserList", muteUserList);

                            MuteAlertDialog dialog = new MuteAlertDialog();
                            dialog.setArguments(putBundle);
                            dialog.setCancelable(false);
                            dialog.setMuteAlertCloseListener(ChatListFragment.this);
                            dialog.show(getFragmentManager(), "Mute");
                        } else {
                            for (int i = 0; i < selectedChatItems.size(); i++) {
                                int chatIndex = mChatData.indexOf(selectedChatItems.get(i));
                                mChatData.get(chatIndex).setSelected(!selectedChatItems.get(i).isSelected());
                            }
                            mAdapter.notifyDataSetChanged();

                            Toast.makeText(getActivity(), "Check Network Connection", Toast.LENGTH_SHORT).show();

                        }
                        selectedChatItems.clear();

                        myMenuClickFlag = false;
                        mAdapter.notifyDataSetChanged();

                        count = 0;
                        onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        return false;
                    }
                });

                archive.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String receiverId = "";
                        for (int i = 0; i < selectedChatItems.size(); i++) {
                            try {
                                String[] arrIds = selectedChatItems.get(i).getMessageId().split("-");
                                receiverId = arrIds[1];
                                if (!session.getarchive(uniqueCurrentID + "-" + receiverId)) {
                                    session.putarchive(uniqueCurrentID + "-" + receiverId);
                                }
                                String docId = uniqueCurrentID.concat("-").concat(receiverId);
                                if (userInfoSession.hasChatConvId(docId)) {
                                    try {
                                        String convId = userInfoSession.getChatConvId(docId);
                                        JSONObject object = new JSONObject();
                                        object.put("convId", convId);
                                        object.put("from", uniqueCurrentID);
                                        object.put("status", 1);
                                        object.put("type", MessageFactory.CHAT_TYPE_SINGLE);
                                        SendMessageEvent event = new SendMessageEvent();
                                        event.setEventName(SocketManager.EVENT_ARCHIVE_UNARCHIVE);
                                        event.setMessageObject(object);
                                        EventBus.getDefault().post(event);
                                    } catch (JSONException e) {
                                        MyLog.e(TAG, "", e);
                                    }
                                }

                            } catch (Exception e) {
                                MyLog.e(TAG, "", e);
                            }
                        }

                        setArchieveText();

                        selectedChatItems.clear();
                        myMenuClickFlag = false;
                        loadDbChatItem();
                        mAdapter.notifyDataSetChanged();

                        count = 0;
                        onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
                        return false;
                    }
                });


            } else {
                //System.out.println("-------chats_screen---------");
                getActivity().getMenuInflater().inflate(R.menu.chats_screen, menu);

                MenuItem logout = menu.findItem(R.id.logout);
                logout.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        logOut(true, getString(R.string.web_logout_msg));
                        return true;
                    }
                });

                MenuItem mobileLogout = menu.findItem(R.id.mobile_logout);
                mobileLogout.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        logOut(false, getString(R.string.mobile_logout_msg));
                        return true;
                    }
                });

                MenuItem searchItem = menu.findItem(R.id.chatsc_searchIcon);
                searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
                setSearchViewCollapseExpand(menu);

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        searchView.setIconifiedByDefault(true);
                        searchView.setIconified(true);
                        searchView.setQuery("", false);
                        searchView.clearFocus();
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {

                        if (newText.equals("") && newText.isEmpty()) {
                            searchView.clearFocus();
                            //closeKeypad();
                        }
                        mAdapter.getFilter().filter(newText);
                        return false;
                    }
                });

                searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                    @Override
                    public boolean onClose() {
                        showChatIcon(menu);
                        return false;
                    }
                });

                searchView.setOnSearchClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideChatIcon(menu);
                    }
                });

                SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
                searchView.setIconifiedByDefault(true);
                searchView.setQuery("", false);
                searchView.clearFocus();
                searchView.setIconified(true);

                AutoCompleteTextView searchTextView = searchView.findViewById(R.id.search_src_text);
                searchTextView.setTextColor(Color.WHITE);

                try {
                    Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
                    mCursorDrawableRes.setAccessible(true);
                    mCursorDrawableRes.set(searchTextView, 0);
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }

            }
        }

    }


    //----------------------------------------Menu Function--------------------------------------------------

    private void hideChatIcon(Menu menu) {
        menu.findItem(R.id.new_group_notification).setVisible(false);
        menu.findItem(R.id.chatsceen_chatIcon).setVisible(false);
    }

    private void showChatIcon(Menu menu) {
        try {
            if (CoreController.isRaad) {

                if (isRaadGroupInviteAvailable()) {
                    menu.findItem(R.id.new_group_notification).setVisible(true);
                    menu.findItem(R.id.chatsceen_chatIcon).setVisible(false);
                } else {
                    menu.findItem(R.id.new_group_notification).setVisible(false);
                }

            } else {
                menu.findItem(R.id.new_group_notification).setVisible(false);
                menu.findItem(R.id.chatsceen_chatIcon).setVisible(true);
            }
        } catch (Exception e) {
            //Log.e(TAG, "showChatIcon: ", e);
        }
    }

    private void logOut(final boolean isWebLogout, String msg) {

        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setNegativeButtonText(getString(R.string.cancel));
        dialog.setPositiveButtonText(getString(R.string.confirm));
        dialog.setMessage(msg);
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {

                NotificationManager notifManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                notifManager.cancelAll();
                if (ConnectivityInfo.isInternetConnected(getActivity())) {
                    if (isWebLogout)
                        notifyLogoutToServer();
                    else {
                        Session session = new Session(getActivity());
                        session.clearData();
                        SessionManager.getInstance(getActivity()).clearSharedPrefs();
                        MessageService.clearDB(getActivity());
                        sessionManager.logoutUser(false);
                    }

                } else {
                    Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getFragmentManager(), "Delete member alert");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        Intent i;
        switch (id) {

            case R.id.select_contact:
                ActivityLauncher.launchSettingContactScreen(getActivity());
                //   startActivity(new Intent(getActivity(), ConnectActivity.class));
                return true;


            case R.id.chatsceen_chatIcon:
                ActivityLauncher.launchSettingContactScreen(getActivity());
                return true;

            case R.id.new_group_notification:
                ActivityLauncher.launchGroupInvitationPage(getActivity());
                return true;

            case R.id.select_groupchat:

                i = new Intent(getActivity(), SelectPeopleForGroupChat.class);
                startActivity(i);
//                getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                return true;

            case R.id.smartty_web:
                if (ConnectivityInfo.isInternetConnected(getActivity())) {
                    Intent qrIntent = new Intent(getActivity(), QRCodeScan.class);
                    startActivityForResult(qrIntent, QR_REQUEST_CODE);
                } else {
                    Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }
                return true;

            case R.id.chats_status:
                ActivityLauncher.launchStatusScreen(getActivity());
                return true;

            case R.id.starred:
                Intent staresintent = new Intent(getActivity(), StarredItemList.class);
                startActivity(staresintent);
                return true;

            case R.id.chats_settings:
                ActivityLauncher.launchSettingScreen(getActivity());

                return true;
            case R.id.select_secret_chat_contact:
                Intent secretintent = new Intent(getActivity(), SecretChatList.class);
                startActivity(secretintent);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }


    }

    private void addShortcut(String path, final boolean isGroupChat) {

        MyLog.d("profileimage", profileimage);
        String[] arrIds = selectedChatItems.get(0).getMessageId().split("-");
        final String receiverId = arrIds[1];
        String imageTS = contactDB_sqlite.getDpUpdatedTime(receiverId);
        if (imageTS == null || imageTS.isEmpty())
            imageTS = "1";
        final String avatar = Constants.USER_PROFILE_URL + receiverId + "?id=" + imageTS;
        final String receiverMsisdn = selectedChatItems.get(0).getSenderMsisdn();

        try {
            final Bitmap bitmap = null;
            MyLog.d(TAG, "loadImage imagetest1: " + path);

            if (isGroupChat && path.isEmpty() || AppUtils.isEmptyImage(path)) {
                MyLog.d(TAG, "loadImage imagetest2 empty image: ");
                ShortcutBadgeManager.addChatShortcut(getActivity(), isGroupChat, receiverId, username,
                        avatar, receiverMsisdn, null);
            } else {
                if (isGroupChat)
                    path = AppUtils.getValidGroupPath(path);
                else {
                    String userImagePath = contactDB_sqlite.getSingleData(receiverId, ContactDB_Sqlite.AVATARIMAGEURL);
                    path = AppUtils.getValidProfilePath(userImagePath);
                }
                BitmapTypeRequest glideRequestmgr = Glide.with(getContext()).load(AppUtils.getGlideURL(path, getContext())).asBitmap();


                glideRequestmgr.diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontTransform()
                        .dontAnimate()
                        .into(new SimpleTarget<Bitmap>() {

                            @Override
                            public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                ShortcutBadgeManager.addChatShortcut(getActivity(), isGroupChat, receiverId, username,
                                        avatar, receiverMsisdn, arg0);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                ShortcutBadgeManager.addChatShortcut(getActivity(), isGroupChat, receiverId, username,
                                        avatar, receiverMsisdn, null);
                            }
                        });
            }
        } catch (Exception e) {
            MyLog.e(TAG, "LoadImage: ", e);
        }
    }


    //--------------------------------------Option Item Click Functions------------------------------------

    private boolean isRaadGroupInviteAvailable() {
        try {
            NewGroup_DB db = CoreController.getmNewGroup_db(getActivity());
            return db.getRowCount() > 0;
        } catch (Exception e) {
            return false;
        }
    }

    private void viewcontact(boolean value) {
        if (value == true) {
            Intent infoIntent = new Intent(getActivity(), GroupInfo.class);
            String[] arrIds = selectedChatItems.get(0).getMessageId().split("-");
            infoIntent.putExtra("GroupId", arrIds[1]);
            infoIntent.putExtra("GroupName", username);
            infoIntent.putExtra("UserAvatar", profileimage);
            startActivity(infoIntent);
        } else {
            Intent infoIntent = new Intent(getActivity(), UserInfo.class);
            String[] arrIds = selectedChatItems.get(0).getMessageId().split("-");
            infoIntent.putExtra("UserId", arrIds[1]);
            infoIntent.putExtra("UserName", username);
            infoIntent.putExtra("UserAvatar", profileimage);
            infoIntent.putExtra("UserNumber", phoneno);
            infoIntent.putExtra("FromSecretChat", false);
            startActivity(infoIntent);
        }

    }

    private void showExitGroupAlert() {
        final CustomAlertDialog alert = new CustomAlertDialog();
        alert.setMessage("Exit " + username + " group");
        alert.setPositiveButtonText("Exit");
        alert.setNegativeButtonText("Cancel");

        alert.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                if (selectedChatItems.size() > 0) {
                    if (ConnectivityInfo.isInternetConnected(getActivity())) {
                        for (int j = 0; j < selectedChatItems.size(); j++) {
                            try {
                                if (selectedChatItems.size() > 0) {
                                    String[] arrIds = selectedChatItems.get(j).getMessageId().split("-");
                                    String receiverId = arrIds[1];
                                    int chatIndex = mChatData.indexOf(selectedChatItems.get(j));
                                    mChatData.get(chatIndex).setSelected(!selectedChatItems.get(j).isSelected());
                                    performGroupExit(receiverId);
                                } else {
                                    break;
                                }
                            } catch (Exception e) {
                                MyLog.e(TAG, "", e);
                            }

                        }
                    } else {
                        Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                    myMenuClickFlag = false;
                    mAdapter.notifyDataSetChanged();

                    selectedChatItems.clear();
                    count = 0;
                    onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());

                }
            }

            @Override
            public void onNegativeButtonClick() {
                alert.dismiss();
                myMenuClickFlag = false;
                mAdapter.notifyDataSetChanged();

                selectedChatItems.clear();
                count = 0;
                onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
            }
        });

        alert.show(getFragmentManager(), "Alert");
    }

    private void notifyLogoutToServer() {
        try {
            JSONObject logoutObj = new JSONObject();
            logoutObj.put("from", uniqueCurrentID);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_MOBILE_TO_WEB_LOGOUT);
            event.setMessageObject(logoutObj);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void setSearchViewCollapseExpand(Menu menu) {

        MenuItem searchItem = menu.findItem(R.id.chatsc_searchIcon);

        if (searchItem != null) {

            MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    NewHomeScreenActivty homeScreen = (NewHomeScreenActivty) getActivity();
                    homeScreen.getSupportActionBar().show();
                    return true;
                }

                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    NewHomeScreenActivty homeScreen = (NewHomeScreenActivty) getActivity();
                    homeScreen.getSupportActionBar().hide();
                    return true;
                }
            });

            MenuItemCompat.setActionView(searchItem, searchView);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void newGroupInvite(GroupInviteBraodCast groupInviteBraodCast) {
        //if(groupInviteBraodCast.isInvited())
        showChatIcon(menuNormal);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(FirstTimeGroupRefreshed event) {
        loadDbChatItem();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_TYPING)) {
            loadTypingStatus(event);
        }
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CALL_STATUS)) {
            Log.e("ChatListFragment", "ChatListFragment EVENT_CALL_STATUS");

            Object[] obj = event.getObjectsArray();
            loadCallStatusFromOpponentUser(obj[0].toString());

        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_DELETE_CHAT)) {
            loadDbChatItem();
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CLEAR_CHAT)) {
            loadDbChatItem();
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GROUP)) {
            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String groupAction = object.getString("groupType");
                if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EVENT_GROUP_MESSAGE)) {
                    loadGroupMessage(event);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_JOIN_NEW_GROUP)) {
                    //  joinToGroup(object);
                    loadDbChatItem();
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_CHANGE_GROUP_NAME)) {
                    loadChangeGroupNameMessage(object);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EXIT_GROUP)) {
                    loadExitMessage(object);
                } else if (groupAction.equals(SocketManager.ACTION_CHANGE_GROUP_DP)) {
                    performGroupChangeDp(object);
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_MESSAGE)) {
            loadMessage(event);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_QR_DATA_RES)) {
            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                MyLog.d("QRSCANNER", object.toString());
                MyLog.e("web", "" + object);
                String err = object.getString("err");
                MyLog.e("userId", "" + err);
                if (err.equalsIgnoreCase("0")) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.app_name) + " web connected", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Couldn't scan code. Make sure you're on " + Constants.BASE_IP + "/web and scan again", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GROUP_DETAILS)) {
            //loadGroupDetails(event);
            //   loadDbChatItem();

        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_VIEW_CHAT)) {
            Object[] obj = event.getObjectsArray();
            changeMsgViewedStatus(obj[0].toString());
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_DELETE_CHAT)) {
            Object[] obj = event.getObjectsArray();
            loadDeleteChat(obj[0].toString());
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_ARCHIVE_UNARCHIVE)) {
            Object[] obj = event.getObjectsArray();
            // loadarchivelist(obj[0].toString());
//            loadarchive(obj[0].toString());
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_PRIVACY_SETTINGS)) {
            loadPrivacySetting(event);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_BLOCK_USER)) {
            loadBlockUserdata(event);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_MUTE)) {
            loadMuteMessage(event.getObjectsArray()[0].toString());
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_MARKREAD)) {
            loadMark();
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_MESSAGE_RES)) {
            loadMessageRes(event);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GET_USER_DETAILS)) {
            // loadUserDetails(event.getObjectsArray()[0].toString());
            new loadUserDetails(event.getObjectsArray()[0].toString()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_DELETE_MESSAGE)) {
            deleteSingleMessage(event);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_IMAGE_UPLOAD)) {
            updateProfileImage(event);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CLEAR_DB)) {
            MessageService.clearDB(getActivity());
            if (chat_list != null) {
                mChatData = null;
                chat_list.setAdapter(null);
            }
        }
    }

    private void loadTypingStatus(ReceviceMessageEvent event) {
        Object[] object = event.getObjectsArray();
        try {
            JSONObject jsonObject = new JSONObject(object[0].toString());
            String from = (String) jsonObject.get("from");
            String to = (String) jsonObject.get("to");
            String convId = (String) jsonObject.get("convId");
            String type = (String) jsonObject.get("type");

            if (type.equalsIgnoreCase("group")) {
                if (type.equalsIgnoreCase("group") && !from.equalsIgnoreCase(mCurrentUserId)) {
                    String msisdn = contactDB_sqlite.getSingleData(from, ContactDB_Sqlite.MSISDN);
                    if (msisdn != null && msisdn.length() > 0) {
                        String typingPerson = getcontactname.getSendername(from, msisdn);
                        setTypingMessage(convId, typingPerson);
                    } else {
                        getUserDetails(from);
                    }
                }
            } else {

                boolean blockedStatus = contactDB_sqlite.getBlockedStatus(to, false).equals("1");

                if (to != null && to.equalsIgnoreCase(uniqueCurrentID) && !blockedStatus) {
                    // Document id reversed from receiver
                    String docId = to + "-" + from;
                    if (uniqueStore.containsKey(docId)) {

                        if (userInfoSession.hasChatConvId(docId)) {
                            String savedConvId = userInfoSession.getChatConvId(docId);
                            if (savedConvId != null && savedConvId.equalsIgnoreCase(convId)) {

                                MessageItemChat msgItem = uniqueStore.get(docId);
                                int index = mChatData.indexOf(msgItem);

                                long currentTime = Calendar.getInstance().getTimeInMillis();

                                msgItem.setTypingAt(currentTime);
                                handleReceiverTypingEvent(docId, currentTime);
                                mChatData.set(index, msgItem);
                                uniqueStore.put(docId, msgItem);
                                mAdapter.notifyDataSetChanged();

                            }
                        }
                    }

                }
            }


        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void setTypingMessage(String groupId, String typingPerson) {
        String docId = mCurrentUserId + "-" + groupId + "-g";

        MessageItemChat msgItem = null;
        if (uniqueStore.containsKey(docId)) {

            msgItem = uniqueStore.get(docId);
        }
        if (uniqueStore.containsKey(SECRET_CHAT_PREFIX + docId)) {
            msgItem = uniqueStore.get(SECRET_CHAT_PREFIX + docId);
        }


        if (msgItem != null) {
            int index = mChatData.indexOf(msgItem);

            long currentTime = Calendar.getInstance().getTimeInMillis();

            msgItem.setTypingAt(currentTime);
            msgItem.setTypePerson(typingPerson);
            handleReceiverTypingEvent(docId, currentTime);
            mChatData.set(index, msgItem);
            uniqueStore.put(docId, msgItem);
            mAdapter.notifyDataSetChanged();

        }

    }

    //---------------------------------Event Received Method------------------------------------------------

    private void handleReceiverTypingEvent(String docId, long currentTime) {
        if (typingEventMap.size() == 0) {
            timeOutTyping = MessageFactory.TYING_MESSAGE_TIMEOUT;
        }
        typingEventMap.put(docId, currentTime);
        typingHandler.postDelayed(typingRunnable, timeOutTyping);
    }


    //-----------------------------------------Typing Status Event Method-----------------------------------------------

    public void getUserDetails(String userId) {
        getUsersList.add(userId);


        boolean isUserAvailableInDb = contactDB_sqlite.isUserAvailableInDB(userId);
        if (!isUserAvailableInDb) {
            try {
                JSONObject eventObj = new JSONObject();
                eventObj.put("userId", userId);

                SendMessageEvent event = new SendMessageEvent();
                event.setEventName(SocketManager.EVENT_GET_USER_DETAILS);
                event.setMessageObject(eventObj);
                EventBus.getDefault().post(event);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
    }

    //--------------------------------Start Typing-------------------------------------

    private void loadGroupMessage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            //{"type":0,"payload":"hjjk","id":"5808c7f76090cc12841d4b46-5808cb46bf5aa60a70fd5c23","from":"5808c7f76090cc12841d4b46","doc_id":"5808c7f76090cc12841d4b46-5808cb46bf5aa60a70fd5c23-1477902694564","thumbnail":"","dataSize":"","timestamp":1477902830388,"convId":"5816e58855dea4ec14bf7dd7"}
            //Check it has type
            if (objects.has("type")) {
                Object object = objects.get("type");

                Integer type = 0;
                if (object instanceof String) {
                    type = Integer.valueOf((String) objects.get("type"));
                } else if (object instanceof Integer) {
                    type = Integer.valueOf((Integer) objects.get("type"));
                }

                if (type == 6 || type == 23) {

                } else {
                    String payLoad = "";
                    payLoad = objects.optString("payload");
                    Object idObj = objects.get("id");
                    String id = "";
                    if (idObj instanceof String) {
                        id = String.valueOf(objects.get("id"));
                    } else if (idObj instanceof Integer) {
                        id = String.valueOf(objects.get("id"));
                    }
                    String from = objects.getString("from");
                    String to = objects.getString("to");
                    if (objects.has("toDocId")) {
                        String toDocId = (String) objects.get("toDocId");
                        String[] splitDocId = toDocId.split("-");
                        String docId = mCurrentUserId.concat("-").concat(splitDocId[1]).concat("-")
                                .concat(splitDocId[2]).concat("-")
                                .concat(splitDocId[3]);
                        String name = objects.getString("msisdn");
                        String ts = objects.getString("timestamp");
                        String groupName = objects.getString("groupName");
                        String starredStatus = objects.getString("isStar");

                        MessageItemChat item = new MessageItemChat();
                        item.setTS(ts);
                        item.setStarredStatus(starredStatus);

                        String groupDocId = mCurrentUserId.concat("-").concat(to).concat("-g");
                        if (groupInfoSession.hasGroupInfo(groupDocId)) {
                            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(groupDocId);
                            if (infoPojo != null) {
                                String avatarPath = infoPojo.getAvatarPath();
                                item.setAvatarImageUrl(avatarPath);
                            }
                        }

                        item.setSenderName(name);
                        item.setGroupName(groupName);
                        item.setIsSelf(false);
                        item.setMessageId(docId);
                        item.setMessageType("" + type);
                        item.sethasNewMessage(true);
                        String strType = "" + type;
                        if (strType.equalsIgnoreCase("" + MessageFactory.text)) {
                            item.setTextMessage(payLoad);
                        } else if (strType.equalsIgnoreCase("" + MessageFactory.contact)) {
//                        item.setContactInfo(payLoad);
                        } else if (strType.equalsIgnoreCase("" + MessageFactory.picture)) {
                            item.setImagePath(payLoad);
                        }
                        item.setCount(1);
                        String uniqueID = new String();

                        String messageAck = new String();
                        if (from.equalsIgnoreCase(mCurrentUserId)) {
                            uniqueID = from + "-" + to + "-g";
                        } else /*if (to.equalsIgnoreCase(uniqueCurrentID))*/ {
                            uniqueID = mCurrentUserId + "-" + to + "-g";
//                messageAck = from;
                        }

                        messageAck = from;
                        sendGroupAckToServer(messageAck, to, "" + id);
//                    db.updateChatMessage(uniqueID, item);
                        uniqueStore.put(uniqueID, item);


                        if (session.getarchivecountgroup() >= 0) {
                            if (session.getarchivegroup(mCurrentUserId + "-" + to + "-g"))
                                session.removearchivegroup(mCurrentUserId + "-" + to + "-g");
                            MyLog.d("Archeivecount_loaggroup", String.valueOf(session.getarchivecount()));
                            setArchieveText();
                        }

                        updateChatList();
                    }
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

    }

    //-------------------------------------------Receiver Typing Event----------------------------------

    private void sendGroupAckToServer(String from, String groupId, String id) {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_GROUP);
        try {
            JSONObject groupAckObj = new JSONObject();
            groupAckObj.put("groupType", SocketManager.ACTION_ACK_GROUP_MESSAGE);
            groupAckObj.put("from", from);
            groupAckObj.put("groupId", groupId);
            groupAckObj.put("status", MessageFactory.GROUP_MSG_DELIVER_ACK);
            groupAckObj.put("msgId", id);
            messageEvent.setMessageObject(groupAckObj);
            EventBus.getDefault().post(messageEvent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //-----------------------------------------------Get User Details-------------------------------------------

    private void sendAckToServer(String to, String doc_id, String id, String mConvid) {
        MyLog.d(TAG, "$$$$$$ sendAckToServer(): ");
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_MESSAGE_ACK);
        MessageAck ack = (MessageAck) MessageFactory.getMessage(MessageFactory.message_ack, getContext());
        messageEvent.setMessageObject((JSONObject) ack.getMessageObject(to, doc_id, MessageFactory.DELIVERY_STATUS_READ, id, false, mConvid));
        EventBus.getDefault().post(messageEvent);
    }


    //--------------------------------------------Load Group Messages--------------------------------------

    public void loadExitMessage(JSONObject object) {
        loadDbChatItem();
    }

    //-------------------------------------Send Group Acknowledgement Send to Server-------------------------

    public void loadChangeGroupNameMessage(JSONObject object) {
        loadDbChatItem();
    }

    private void performGroupChangeDp(JSONObject object) {
        try {
            String err = object.getString("err");

            if (err.equalsIgnoreCase("0")) {

                String message = object.getString("message");
                String groupId = object.getString("groupId");
                String avatar = object.getString("avatar");


                for (int i = 0; i < mChatData.size(); i++) {

                    if (mChatData.get(i).getMessageId().contains(groupId)) {
                        mChatData.get(i).setAvatarImageUrl(avatar);

                        String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");
                        GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);
                        infoPojo.setAvatarPath(avatar);
                        groupInfoSession.updateGroupInfo(docId, infoPojo);
                        break;
                    }
                }
                mAdapter.notifyDataSetChanged();


            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private synchronized void loadMessage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {

            JSONObject objects = new JSONObject(array[0].toString());

            if (Integer.parseInt(objects.optString("type")) == 23) {
                return;
            }
            String secretType = objects.getString("secret_type");
            if (secretType != null && secretType.equalsIgnoreCase("no")) {
                IncomingMessage incomingMsg = new IncomingMessage(getActivity());
                MessageItemChat item = incomingMsg.loadSingleMessage(objects);
                String from = objects.getString("from");
                String to = objects.getString("to");
                String id = objects.getString("id");
                if (incomingMsgIds.contains(id)) {
                    return;
                }
                MyLog.d(TAG, "loadMessage msgId: " + id);
                incomingMsgIds.add(id);
                String docId;
                if (from != null && from.equalsIgnoreCase(uniqueCurrentID)) {
                    docId = from + "-" + to;
                } else {
                    docId = to + "-" + from;
                }
                item.setMessageId(docId + "-" + id);

                if (session.getarchivecount() >= 0) {
                    if (session.getarchive(uniqueCurrentID + "-" + to))
                        session.removearchive(uniqueCurrentID + "-" + to);

                    setArchieveText();
                }
                if (session.getarchivecountgroup() >= 0) {
                    if (session.getarchivegroup(uniqueCurrentID + "-" + to + "-g"))
                        session.removearchivegroup(uniqueCurrentID + "-" + to + "-g");
                }

                if (uniqueStore.containsKey(docId)) {
                    MessageItemChat oldItem = uniqueStore.get(docId);
                    if (oldItem.isSelected()) {
                        item.setSelected(oldItem.isSelected());
                        int selectedIndex = selectedChatItems.indexOf(oldItem);
                        selectedChatItems.set(selectedIndex, item);
                    }
                } else if (uniqueStore.containsKey(SECRET_CHAT_PREFIX + docId)) {
                    MessageItemChat oldItem = uniqueStore.get(SECRET_CHAT_PREFIX + docId);
                    if (oldItem.isSelected()) {
                        item.setSelected(oldItem.isSelected());
                        int selectedIndex = selectedChatItems.indexOf(oldItem);
                        selectedChatItems.set(selectedIndex, item);
                    }
                }

                // db.updateChatMessage(uniqueID, item);
                uniqueStore.put(docId, item);
                updateChatList();

            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void changeMsgViewedStatus(String data) {

        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");
            String mode = object.getString("mode");

            if (from != null && from.equalsIgnoreCase(uniqueCurrentID) &&
                    mode != null && mode.equalsIgnoreCase("web")) {
                String chatType = object.getString("type");
                /*String convId = object.getString("convId");
                String to = object.getString("to");*/

                if (chatType != null && chatType.equalsIgnoreCase("single")) {
                    loadDbChatItem();
                    NewHomeScreenActivty activity = (NewHomeScreenActivty) getActivity();
                    activity.changeTabTextCount();
                } else if (chatType != null && chatType.equalsIgnoreCase("group")) {
                    loadDbChatItem();
                    NewHomeScreenActivty activity = (NewHomeScreenActivty) getActivity();
                    activity.changeTabTextCount();
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }


    //-------------------------------------Change DP in Group-------------------------------------------

    private void loadDeleteChat(String data) {
        try {
            JSONObject object = new JSONObject(data);

            try {
                String from = object.getString("from");
                String convId = object.getString("convId");
                String type = object.getString("type");

                if (from != null && from.equalsIgnoreCase(uniqueCurrentID)) {
                    String receiverId;
                    if (type != null && type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                        receiverId = convId;
                    } else {
                        receiverId = userInfoSession.getReceiverIdByConvId(convId);
                    }

                    if (!receiverId.equals("")) {
                        String docId = from.concat("-").concat(receiverId);
                        if (type != null && type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                            docId = docId.concat("-g");
                        }

                        if (isSecretChatDeleted) {
                            isSecretChatDeleted = false;
                            uniqueStore.remove(SECRET_CHAT_PREFIX + docId);
                            updateChatList();
                            mAdapter.notifyDataSetChanged();


                            NewHomeScreenActivty activity = (NewHomeScreenActivty) getActivity();
                            activity.changeTabTextCount();
                        } else {
                            uniqueStore.remove(docId);
                            updateChatList();
                            mAdapter.notifyDataSetChanged();


                            NewHomeScreenActivty activity = (NewHomeScreenActivty) getActivity();
                            activity.changeTabTextCount();
                        }
                    }
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    //----------------------------------------------Load Messages Response------------------------------------------

    private void loadPrivacySetting(ReceviceMessageEvent event) {
        mAdapter.notifyDataSetChanged();

    }


    //-----------------------------------Message View Status Maintain------------------------------------

    private void loadBlockUserdata(ReceviceMessageEvent event) {
        try {
            Object[] obj = event.getObjectsArray();
            JSONObject object = new JSONObject(obj[0].toString());
            MyLog.e("Response---", object.toString());
            String stat = object.getString("status");
            String toid = object.getString("to");
            String from = object.getString("from");
            if (uniqueCurrentID != null && uniqueCurrentID.equalsIgnoreCase(toid)) {
                mAdapter.notifyDataSetChanged();


            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    //---------------------------------------------------Load Delete Chat----------------------------------------

    private void loadMuteMessage(String data) {
        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");
            String chatType = object.getString("type");
            String secretType = "no";
            if (object.has("secret_type")) {
                secretType = object.getString("secret_type");
            }

            if (from != null && from.equalsIgnoreCase(uniqueCurrentID)
                    && chatType != null && chatType.equalsIgnoreCase("single")
                    && secretType.equalsIgnoreCase("no") && mAdapter != null) {
                mAdapter.notifyDataSetChanged();

            } else if (from != null && from.equalsIgnoreCase(mCurrentUserId) &&
                    chatType != null && chatType.equalsIgnoreCase("group") && mAdapter != null) {
                mAdapter.notifyDataSetChanged();

            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    //-------------------------------------Privacy settings----------------------------------

    private void loadMark() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();

        }
    }

    //------------------------------------------Load Block Users---------------------------------

    private void loadMessageRes(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            int errorState = objects.getInt("err");
            if (errorState == 0) {
                JSONObject msgData = objects.getJSONObject("data");

                String from = msgData.getString("from");
                String secretType = msgData.getString("secret_type");
                if (from != null && from.equalsIgnoreCase(uniqueCurrentID) &&
                        secretType != null && secretType.equalsIgnoreCase("no")) {

                    String toUserId = msgData.getString("to");

                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

//----------------------------------------------Load Mute Messages--------------------------------

    private void loadUserDetails(String data) {
        try {
            JSONObject object = new JSONObject(data);
            String userId = object.getString("id");
            if (getUsersList.contains(userId) && mAdapter != null) {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        // UI code goes here
                        mAdapter.notifyDataSetChanged();

                    }
                });

            } else {
                String docId = uniqueCurrentID + "-" + userId;
                if (uniqueStore.containsKey(docId) || uniqueStore.containsKey(SECRET_CHAT_PREFIX + docId)) {
                    try {
                        for (int i = mFirstVisibleItemPosition; i <= mLastVisibleItemPosition; i++) {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    // UI code goes here
                                    mAdapter.notifyDataSetChanged();

                                }
                            });

                        }
                    } catch (Exception e) {
                        MyLog.e(TAG, "loadUserDetails: ", e);
                    }
//            receivedUserDetailsMap.put(userId, Calendar.getInstance().getTimeInMillis());
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadCallStatusFromOpponentUser(String data) {
        Log.e("ChatListFragment", "ChatListFragment" + data);

        uniqueCurrentID = SessionManager.getInstance(getActivity()).getCurrentUserID();
        MyLog.e(TAG, "loadCallOpponent" + "data" + data);
        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");
            String to = object.getString("to");
            String id = object.getString("id");

            String recordId = object.getString("recordId");
            String status = object.getString("call_status");
            String toDocId = object.getString("toDocId");

            if (!uniqueCurrentID.equals(from)) {
                //Incoming call data save in db if rejected
                try {
                    MessageDbController db = CoreController.getDBInstance(mContext);
                    CallItemChat callItemChat = new CallItemChat();

                    //Check call status 6 and answered dont update for user
                    if (Integer.parseInt(status) == 6) {
                        SharedPreference.getInstance().saveBool(mContext, "callongoing", false);
                        checkcallstatus();


                    } else if (Integer.parseInt(status) == 5) {
                        SharedPreference.getInstance().saveBool(mContext, "callongoing", false);
                        checkcallstatus();

                    }
                } catch (Exception ex) {
                    MyLog.e(TAG, "loadCallStatusFromOpponentUser: ", ex);
                }
            }
            if (Integer.parseInt(status) == 5) {
                SharedPreference.getInstance().saveBool(mContext, "callongoing", false);
                checkcallstatus();

                MessageDbController db = CoreController.getDBInstance(mContext);
                if (!uniqueCurrentID.equals(from)) {
                    //check call is missed or attended
                    boolean isAnsweredToUser = SharedPreference.getInstance().getBool(mContext, "isAnsweredToUser");
                    if (!isAnsweredToUser) {

                    } else {

                        SharedPreference.getInstance().saveBool(mContext, "callongoing", false);
                        checkcallstatus();

                    }
                }
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    //------------Delete Chat-------------------
    private void deleteSingleMessage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {

            JSONObject objects = new JSONObject(array[0].toString());
            int errorState = objects.getInt("err");
            if (errorState == 0) {
                String fromId = objects.getString("from");

                if (fromId != null && !fromId.equalsIgnoreCase(uniqueCurrentID)) {
                    String deleteStatus = objects.getString("status");
                    String chat_id = (String) objects.get("doc_id");
                    String[] ids = chat_id.split("-");

                    String docId;
                    String msgId;

                    docId = ids[1] + "-" + ids[0];
                    msgId = docId + "-" + ids[2];

                    if (deleteStatus != null && deleteStatus.equalsIgnoreCase("1")) {
                        if (mChatData.size() > 0) {
                            for (int i = 0; i < mChatData.size(); i++) {
                                if (mChatData.get(i).getMessageId().equalsIgnoreCase(msgId)) {
//                                    mChatData.get(i).setMessageType("Delete Others");
                                    mChatData.get(i).setMessageType(MessageFactory.DELETE_OTHER + "");
                                    mChatData.get(i).setIsSelf(false);

                                    mAdapter.notifyDataSetChanged();

                                    break;
                                }
                            }

                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updateProfileImage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            Log.e(TAG, "EVENT_IMAGE_UPLOAD" + objects);
            String from = objects.getString("from");
            String type = objects.getString("type");
            String docId = uniqueCurrentID + "-" + from;
            if (type.equalsIgnoreCase("single") && (uniqueStore.containsKey(docId)
                    || uniqueStore.containsKey(SECRET_CHAT_PREFIX + docId))) {
                String path = objects.getString("file") + "?id=" + Calendar.getInstance().getTimeInMillis();
                MyLog.d(TAG, "updateProfileImage: " + path);
                mAdapter.notifyDataSetChanged();

            }
        } catch (Exception e) {
            MyLog.e(TAG, "updateProfileImage: ", e);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //registerCallReceiver();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == QR_REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK) {
            String qrData = data.getStringExtra("QRData");

            try {
                JSONObject object = new JSONObject();
                object.put("_id", SessionManager.getInstance(getActivity()).getCurrentUserID());
                object.put("msisdn", SessionManager.getInstance(getActivity()).getPhoneNumberOfCurrentUser());
                object.put("random", qrData);

                SendMessageEvent qrEvent = new SendMessageEvent();
                qrEvent.setEventName(SocketManager.EVENT_QR_DATA);
                qrEvent.setMessageObject(object);
                EventBus.getDefault().post(qrEvent);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }

        }
    }

    private void registerCallReceiver() {
        //Register BroadcastReceiver
        //to receive event from our service
        // if (myReceiver == null) {
        try {
            myReceiver = new MyReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(MessageService.SENDMESAGGE);
            mContext.registerReceiver(myReceiver, intentFilter);
        } catch (Exception e) {

        }
        //  }
    }

    //-------------------------------------------Profile IMage Update Method------------------------------

    private void unregisterCallReceiver() {
        if (myReceiver != null) {
            try {
                getActivity().unregisterReceiver(myReceiver);
            } catch (Exception e) {
                Log.e(TAG, "onStop: ", e);
            }
        }
    }

    class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("com.selectedchat.remove")) {
                for (int i = 0; i < mChatData.size(); i++) {
                    mChatData.get(i).setSelected(false);
                }
                count = 0;
                selectedChatItems.clear();
                mAdapter.notifyDataSetChanged();

                onCreateOptionsMenu(menuLongClick, getActivity().getMenuInflater());
                myMenuClickFlag = false;
                onCreateOptionsMenu(menuNormal, getActivity().getMenuInflater());
            }
        }
    }

    private class ChatListSorter implements Comparator<MessageItemChat> {

        @Override
        public int compare(MessageItemChat item1, MessageItemChat item2) {
            long item1Time = AppUtils.parseLong(item1.getTS());
            long item2Time = AppUtils.parseLong(item2.getTS());

            if (item1Time < item2Time) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    public class DbAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            loadDbChatItem();
            return null;
        }
    }

    private class loadUserDetails extends AsyncTask<String, Void, String> {
        String datagolbal;

        public loadUserDetails(String data) {
            datagolbal = data;
        }

        @Override
        protected String doInBackground(String... params) {
            loadUserDetails(datagolbal);
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            //UI thread
        }
    }

    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            if (arg1.hasExtra("message")) {
                checkcallstatus();
                //  System.out.println(arg1.getStringExtra("message"));
            }
        }
    }

   /* @Override
    public void adremovelistener(int position) {

        if(mChatData.size()>1){

            mChatData.remove(1);

            chat_list.post(new Runnable() {
                @Override
                public void run() {

                    mAdapter.notifyDataSetChanged();

                }
            });
        }
    }*/
}
