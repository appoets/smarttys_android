package com.app.Smarttys.app.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.CircleImageView;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.model.SmarttyContactModel;

import java.util.ArrayList;
import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;


/**
 * Created by CAS63 on 3/7/2017.
 */
public class BlockListAdapter extends RecyclerView.Adapter<BlockListAdapter.MyViewHolder> {

    private Context context;
    private Getcontactname getcontactname;
    private ItemFilter mFilter = new ItemFilter();

    private List<SmarttyContactModel> mListData = new ArrayList<>();

    public BlockListAdapter(Context context, List<SmarttyContactModel> mListData) {
        this.context = context;
        this.mListData = mListData;
        getcontactname = new Getcontactname(context);
    }

    public SmarttyContactModel getItem(int position) {
        return mListData.get(position);
    }

    public void updateInfo(ArrayList<SmarttyContactModel> aitem) {
        this.mListData = aitem;
        notifyDataSetChanged();
    }

    public Filter getFilter() {
        return mFilter;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.block_user_from_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SmarttyContactModel smarttyContactModel = mListData.get(position);
        holder.title.setText(smarttyContactModel.getFirstName());
        holder.user_status.setText(smarttyContactModel.getStatus());

        getcontactname.configProfilepic(holder.userImage, smarttyContactModel.get_id(), false,
                false, R.mipmap.chat_attachment_profile_default_image_frame);
    }

    @Override
    public int getItemCount() {
        return this.mListData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public AvnNextLTProDemiTextView title;
        public EmojiconTextView user_status;
        public CircleImageView userImage;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.userName_block);
            user_status = view.findViewById(R.id.user_status);
            userImage = view.findViewById(R.id.userImage);

            Typeface tf = CoreController.getInstance().getAvnNextLTProRegularTypeface();
            user_status.setTypeface(tf);
        }
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            final ArrayList<SmarttyContactModel> nlist = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                results.values = mListData;
                results.count = mListData.size();
                //System.out.println("nlist size in if part" + results.count);
            } else {
                for (int i = 0; i < mListData.size(); i++) {
                    SmarttyContactModel item = mListData.get(i);
                    MyLog.e("fgfg", filterString);
                    if (item.getFirstName().toLowerCase().contains(filterString)) {

                        nlist.add(item);
                    }
                }
                results.values = nlist;
                results.count = nlist.size();

            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
            mListData = (ArrayList<SmarttyContactModel>) results.values;
            notifyDataSetChanged();
               /* ArrayList<MessageItemChat> filtered = (ArrayList<MessageItemChat>) results.values;
                notifyDataSetChanged();*/
        }

    }
}
