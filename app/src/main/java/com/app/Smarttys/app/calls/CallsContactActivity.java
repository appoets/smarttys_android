package com.app.Smarttys.app.calls;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.SmarttyContactsService;
import com.app.Smarttys.app.dialog.CustomAlertDialog;
import com.app.Smarttys.app.utils.BlockUserUtils;
import com.app.Smarttys.app.utils.ConnectivityInfo;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.RecyclerViewItemClickListener;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.IncomingMessage;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.CallItemChat;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.socket.SocketManager;

import org.appspot.apprtc.CallActivity;
import org.appspot.apprtc.WebrtcConstants;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by CAS63 on 7/28/2017.
 */
public class CallsContactActivity extends CoreActivity {

    private static final String TAG = "CallsContactActivity";
    private final int AUDIO_RECORD_PERMISSION_REQUEST_CODE = 14;
    private List<SmarttyContactModel> smarttyEntries = new ArrayList<>();
    private RecyclerView myCallContacts;
    private CallContactAdapter myAdapter;
    private ImageView mySearchIMG;
    private EditText etSearch;
    private ImageView myMainBackLAY, mySearchBackLAY;
    private TextView myHeaderTitle, myContactsCount;
    private ProgressDialog progressDialog;
    private Session session;
    private String mCurrentUserId, toUserId;
    private Handler eventHandler;
    private Runnable eventRunnable;
    private int callItemType;
    private int callItemPosition;
    private String mReceiverId = "";
    private String to = "";
    private String receiverMsisdn = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_contacts_listview);
        initialize();
        initData();
        Log.e("CheckScreen", "I am here");
    }

    private void onItemClicks() {
        myAdapter.setCallContactsItemClickListener(new RecyclerViewItemClickListener() {
            @Override
            public void onRVItemClick(View parentView, int position) {

                SmarttyContactModel contact = myAdapter.getItem(position);

                switch (parentView.getId()) {

                    case R.id.audio_call_layout:

                        mReceiverId = contact.get_id();
                        to = contact.get_id();
                        receiverMsisdn = contact.getMsisdn();
                        try {

                            Activecall(false);

                        } catch (Exception e) {

                        }

                        break;

                    case R.id.video_call_layout:

                        mReceiverId = contact.get_id();
                        to = contact.get_id();
                        receiverMsisdn = contact.getMsisdn();

                        try {
                            Activecall(true);
                        } catch (Exception e) {

                        }
                        break;
                }
            }
        });

        mySearchIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performContactsSearch();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
        });


        myMainBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hideKeyboard();
                finish();
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (myAdapter != null) {
                        myAdapter.getFilter().filter(etSearch.getText().toString());
                        return false;
                    }
                }
                return true;
            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case AUDIO_RECORD_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        //performCall(callItemPosition,callItemType);
                    }
                }
                break;
        }
    }


    private void Activecall(boolean isVideoCall) {
        if (ConnectivityInfo.isInternetConnected(this))
            if (checkAudioRecordPermission()) {
                CallMessage message = new CallMessage(CallsContactActivity.this);
                boolean isOutgoingCall = true;

                String roomid = message.getroomid();
                String timestamp = message.getroomid();
                String callid = mCurrentUserId + "-" + mReceiverId + "-" + timestamp;

                if (!CallsActivity.isStarted) {
                    if (isOutgoingCall) {
                        CallsActivity.opponentUserId = to;
                    }

                    PreferenceManager.setDefaultValues(getApplicationContext(), org.appspot.apprtc.R.xml.preferences, false);
                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                    String keyprefRoomServerUrl = getApplicationContext().getString(org.appspot.apprtc.R.string.pref_room_server_url_key);
                    boolean isTurnServerEnabled = WebrtcConstants.isTurnServerEnabled;

                    String roomUrlDefault = getString(org.appspot.apprtc.R.string.pref_room_server_url_default);
                    if (isTurnServerEnabled)
                        roomUrlDefault = WebrtcConstants.OWN_TURN_SERVER;

                    // TODO: 16/04/2019 webrtc preference url replaced here with static url
                    String roomUrl = sharedPref.getString(keyprefRoomServerUrl, roomUrlDefault);


                    int videoWidth = 0;
                    int videoHeight = 0;
                    String resolution = getApplicationContext().getString(org.appspot.apprtc.R.string.pref_resolution_default);
                    String[] dimensions = resolution.split("[ x]+");
                    if (dimensions.length == 2) {
                        try {
                            videoWidth = Integer.parseInt(dimensions[0]);
                            videoHeight = Integer.parseInt(dimensions[1]);
                        } catch (NumberFormatException e) {
                            videoWidth = 0;
                            videoHeight = 0;
                            MyLog.e("SmarttyCallError", "Wrong video resolution setting: " + resolution);
                        }
                    }
                    Uri uri = Uri.parse(roomUrl);
                    Intent intent = new Intent(getApplicationContext(), CallsActivity.class);
                    intent.setData(uri);
                    intent.putExtra(CallsActivity.EXTRA_IS_OUTGOING_CALL, isOutgoingCall);
                    intent.putExtra(CallsActivity.EXTRA_DOC_ID, callid);
                    intent.putExtra(CallsActivity.EXTRA_FROM_USER_ID, mCurrentUserId);
                    intent.putExtra(CallsActivity.EXTRA_TO_USER_ID, to);
                    intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, receiverMsisdn);
                    intent.putExtra(CallsActivity.EXTRA_OPPONENT_PROFILE_PIC, "");
                    intent.putExtra(CallsActivity.EXTRA_NAVIGATE_FROM, getApplicationContext().getClass().getSimpleName()); // For navigating from call activity
                    intent.putExtra(CallsActivity.EXTRA_CALL_CONNECT_STATUS, "0");
                    intent.putExtra(CallsActivity.EXTRA_CALL_TIME_STAMP, timestamp);

                    intent.putExtra(CallsActivity.EXTRA_ROOMID, roomid);
                    intent.putExtra(CallsActivity.EXTRA_LOOPBACK, false);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_CALL, isVideoCall);
                    intent.putExtra(CallsActivity.EXTRA_SCREENCAPTURE, false);
                    intent.putExtra(CallsActivity.EXTRA_CAMERA2, true);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_WIDTH, videoWidth);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_HEIGHT, videoHeight);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_FPS, 0);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_BITRATE, 0);
                    intent.putExtra(CallsActivity.EXTRA_VIDEOCODEC, getApplicationContext().getString(org.appspot.apprtc.R.string.pref_videocodec_default));
                    intent.putExtra(CallsActivity.EXTRA_HWCODEC_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_CAPTURETOTEXTURE_ENABLED, true);
                    intent.putExtra(CallsActivity.EXTRA_FLEXFEC_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_NOAUDIOPROCESSING_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_AECDUMP_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_OPENSLES_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AEC, false);
                    intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AGC, false);
                    intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_NS, false);
                    intent.putExtra(CallsActivity.EXTRA_ENABLE_LEVEL_CONTROL, false);
                    intent.putExtra(CallsActivity.EXTRA_AUDIO_BITRATE, 0);
                    intent.putExtra(CallsActivity.EXTRA_AUDIOCODEC, getApplicationContext().getString(org.appspot.apprtc.R.string.pref_audiocodec_default));
                    intent.putExtra(CallsActivity.EXTRA_DISPLAY_HUD, false);
                    intent.putExtra(CallsActivity.EXTRA_TRACING, false);
                    intent.putExtra(CallsActivity.EXTRA_CMDLINE, false);
                    intent.putExtra(CallsActivity.EXTRA_RUNTIME, 0);

                    intent.putExtra(CallActivity.EXTRA_DATA_CHANNEL_ENABLED, true);
                    intent.putExtra(CallActivity.EXTRA_ORDERED, true);
                    intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS_MS, -1);
                    intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS, -1);
                    intent.putExtra(CallActivity.EXTRA_PROTOCOL, getApplicationContext().getString(org.appspot.apprtc.R.string.pref_data_protocol_default));
                    intent.putExtra(CallActivity.EXTRA_NEGOTIATED, false);
                    intent.putExtra(CallActivity.EXTRA_ID, -1);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);


                }


            } else {
                requestAudioRecordPermission();
            }
    }


    private void performCall(int position, int callType, CallItemChat callItem, boolean isVideoCall) {
        callItemPosition = position;
        callItemType = callType;
        if (checkAudioRecordPermission()) {
            try {
                if (!CallMessage.isAlreadyCallClick) {
                    toUserId = smarttyEntries.get(position).get_id();
                    String msisdn = smarttyEntries.get(position).getMsisdn();

                    Getcontactname getcontactname = new Getcontactname(this);
                    String receiverName = getcontactname.getSendername(toUserId, msisdn);

                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
                    if (contactDB_sqlite.getBlockedStatus(toUserId, false).equals("1")) {
                        String msg = "Unblock " + receiverName + " to place a " +
                                getString(R.string.app_name) + " call";
                        displayAlert(msg);
                    } else {
                        CallMessage message = new CallMessage(CallsContactActivity.this);
                        JSONObject object = (JSONObject) message.getMessageObject(toUserId, callType);

                        if (object != null) {
                            SendMessageEvent callEvent = new SendMessageEvent();
                            callEvent.setEventName(SocketManager.EVENT_CALL);
                            callEvent.setMessageObject(object);
                            EventBus.getDefault().post(callEvent);
                            CallMessage.setCallClickTimeout();
                        }

                        try {
                            String to = object.getString("to");
                            String toUserAvatar = "";

                            MessageDbController db = CoreController.getDBInstance(CallsContactActivity.this);
                            db.updateCallLogs(callItem);

                            String ts = "";
                            CallMessage.openCallScreen(CallsContactActivity.this, mCurrentUserId, to, callItem.getCallId(),
                                    callItem.getRecordId(), toUserAvatar, callItem.getOpponentUserMsisdn(),
                                    MessageFactory.CALL_IN_FREE + "", isVideoCall, true, ts, "");
                        } catch (Exception e) {
                            MyLog.e(TAG, "performCall: ", e);
                        }
                    }
                } else {
                    Toast.makeText(CallsContactActivity.this, "Call in progress", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                MyLog.e(TAG, "performCall: ", e);
            }
        } else {
            requestAudioRecordPermission();
        }
    }

    private void requestAudioRecordPermission() {
        ActivityCompat.requestPermissions(CallsContactActivity.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, AUDIO_RECORD_PERMISSION_REQUEST_CODE);
    }

    private void displayAlert(String msg) {
        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(msg);
        dialog.setNegativeButtonText("Cancel");
        dialog.setPositiveButtonText("Unblock");
        dialog.setCancelable(false);

        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                if (!progressDialog.isShowing()) {
                    progressDialog.show();
                }

                BlockUserUtils.changeUserBlockedStatus(CallsContactActivity.this, EventBus.getDefault(),
                        mCurrentUserId, toUserId, false);

                dialog.dismiss();
            }

            @Override

            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getSupportFragmentManager(), "UnBlock a person");

    }

    private void performContactsSearch() {

        showSearchActions();
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                if (myAdapter != null) {
                    if (cs.length() > 0) {
                        if (cs.length() == 1) {
                            etSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.cancel_normal, 0);
                        }
                        myAdapter.getFilter().filter(cs);
                    } else {
                        etSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        myAdapter.updateInfo(smarttyEntries);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
        mySearchBackLAY.setVisibility(View.VISIBLE);
        myMainBackLAY.setVisibility(View.GONE);

        mySearchBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                etSearch.getText().clear();
              /*  if (myAdapter != null) {
                    myAdapter.updateInfo(smarttyEntries);
                }*/
                etSearch.setVisibility(View.GONE);
                mySearchIMG.setVisibility(View.VISIBLE);
                myContactsCount.setVisibility(View.VISIBLE);
                myHeaderTitle.setVisibility(View.VISIBLE);
                mySearchBackLAY.setVisibility(View.GONE);
                myMainBackLAY.setVisibility(View.VISIBLE);

            }
        });

    }

    private void showSearchActions() {
        mySearchBackLAY.setVisibility(View.VISIBLE);
        mySearchIMG.setVisibility(View.GONE);
        myMainBackLAY.setVisibility(View.GONE);
        myHeaderTitle.setVisibility(View.GONE);
        myContactsCount.setVisibility(View.GONE);
        //contact1_RelativeLayout.setVisibility(View.GONE);
        etSearch.setVisibility(View.VISIBLE);
        etSearch.requestFocus();
    }


    private void initData() {

        session = new Session(this);
        mCurrentUserId = SessionManager.getInstance(this).getCurrentUserID();

        smarttyEntries.clear();
        myAdapter = new CallContactAdapter(CallsContactActivity.this, smarttyEntries);

        loadContactsFromDB();
        if (smarttyEntries != null && smarttyEntries.size() == 0) {
            progressDialog.setMessage(getString(R.string.loading_in));
            progressDialog.show();

            progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    if (eventHandler != null) {
                        eventHandler.removeCallbacks(eventRunnable);
                    }
                }
            });
            Log.e(TAG, "SocketManager.isConnected: " + SocketManager.isConnected + "startContactService");

            SmarttyContactsService.startContactService(this, false);
            setEventTimeout();
        }
    }

    private void setEventTimeout() {
        if (eventHandler == null) {
            eventHandler = new Handler();
            eventRunnable = new Runnable() {
                @Override
                public void run() {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        Toast.makeText(CallsContactActivity.this, "Try again", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();

                        if (smarttyEntries != null && smarttyEntries.size() == 0) {
                            loadContactsFromDB();
                        }
                    }
                }
            };
        }

        eventHandler.postDelayed(eventRunnable, SocketManager.CONTACT_REFRESH_TIMEOUT);
    }

    private void loadContactsFromDB() {
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        smarttyEntries = contactDB_sqlite.getSavedSmarttyContacts();
        myContactsCount.setText(smarttyEntries.size() + " " + getString(R.string.contact));

        if (smarttyEntries != null && smarttyEntries.size() > 0) {
            Collections.sort(smarttyEntries, Getcontactname.nameAscComparator);
            myAdapter.updateInfo(smarttyEntries);
            myAdapter.notifyDataSetChanged();
            myCallContacts.setVisibility(View.VISIBLE);
            //contact_empty.setVisibility(View.GONE);
            myCallContacts.setAdapter(myAdapter);
            onItemClicks();
        } else {
            //contact_empty.setVisibility(View.VISIBLE);
            myCallContacts.setVisibility(View.GONE);
            //contact_empty.setText("No Contacts Available for chat");
        }
    }

    private void initialize() {
        myCallContacts = findViewById(R.id.activity_call_contacts_Viewitems);
        mySearchIMG = findViewById(R.id.activity_call_contacts_listview_searchIMG);
        etSearch = findViewById(R.id.activity_call_contacts_listview_etSearch);
        myMainBackLAY = findViewById(R.id.activity_call_contacts_listview_backIMG);
        mySearchBackLAY = findViewById(R.id.activity_call_contacts_listview_backIMG_search);
        myHeaderTitle = findViewById(R.id.activity_call_contacts_listview_headerName);
        myContactsCount = findViewById(R.id.activity_call_contacts_listview_contactCount);

        LinearLayoutManager mediaManager = new LinearLayoutManager(CallsContactActivity.this, LinearLayoutManager.VERTICAL, false);
        myCallContacts.setLayoutManager(mediaManager);

        progressDialog = getProgressDialogInstance();
        progressDialog.setMessage(getString(R.string.loading_in));

        etSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP && etSearch.getCompoundDrawables()[DRAWABLE_RIGHT] != null) {
                    if (event.getRawX() >= (etSearch.getRight() - etSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        etSearch.setText("");
                        return false;
                    }
                }
                return false;
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        switch (event.getEventName()) {

            case SocketManager.EVENT_CALL_RESPONSE:
                loadCallResMessage(event);
                break;

            case SocketManager.EVENT_BLOCK_USER:
                loadBlockEventMessage(event);
                break;

            case SocketManager.EVENT_GET_CONTACTS: {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        loadContactsFromDB();
                    }
                }, 3000);
            }
            break;
        }

    }

    private void loadBlockEventMessage(ReceviceMessageEvent event) {

        try {
            Object[] obj = event.getObjectsArray();
            JSONObject object = new JSONObject(obj[0].toString());

            String status = object.getString("status");
            String to = object.getString("to");
            String from = object.getString("from");

            if (mCurrentUserId.equalsIgnoreCase(from) && to.equalsIgnoreCase(toUserId)) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (status.equalsIgnoreCase("1")) {
                    Toast.makeText(this, "Number is blocked", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Number is Unblocked", Toast.LENGTH_SHORT).show();
                }
            }

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadCallResMessage(ReceviceMessageEvent event) {
        Object[] obj = event.getObjectsArray();
        try {
            JSONObject object = new JSONObject(obj[0].toString());
            JSONObject callObj = object.getJSONObject("data");

            String from = callObj.getString("from");
            String callStatus = callObj.getString("call_status");
            String callConnect = MessageFactory.CALL_IN_FREE + "";
            if (callObj.has("call_connect")) {
                callConnect = callObj.getString("call_connect");
            }

            if (from.equalsIgnoreCase(mCurrentUserId) && callStatus.equals(MessageFactory.CALL_STATUS_CALLING + "")) {
                String to = callObj.getString("to");

                IncomingMessage incomingMsg = new IncomingMessage(CallsContactActivity.this);
                CallItemChat callItem = incomingMsg.loadOutgoingCall(callObj);

                boolean isVideoCall = false;
                if (callItem.getCallType().equals(MessageFactory.video_call + "")) {
                    isVideoCall = true;
                }

                String toUserAvatar = callObj.getString("To_avatar") + "?=id" + Calendar.getInstance().getTimeInMillis();

                MessageDbController db = CoreController.getDBInstance(this);
                db.updateCallLogs(callItem);

                String ts = callObj.getString("timestamp");

                CallMessage.openCallScreen(CallsContactActivity.this, mCurrentUserId, to, callItem.getCallId(),
                        callItem.getRecordId(), toUserAvatar, callItem.getOpponentUserMsisdn(), callConnect,
                        isVideoCall, true, ts, "");
                finish();
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        hideKeyboard();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideKeyboard();
    }

    @Override
    public void onBackPressed() {
        if (etSearch.getVisibility() == View.GONE) {
            hideKeyboard();
            super.onBackPressed();
        } else {
            mySearchBackLAY.performClick();
        }
    }
}
