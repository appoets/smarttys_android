package com.app.Smarttys.app.CustomFontTextView;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class NewFontTextView extends AppCompatTextView {

    public NewFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public NewFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NewFontTextView(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Karla-Bold.ttf");
        setTypeface(tf);
    }
}