package com.app.Smarttys.app.utils;

public interface NetworkStateReceiverListener {
    public void networkAvailable();

    public void networkUnavailable();
}
