package com.app.Smarttys.app.activity;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.RItemAdapter;
import com.app.Smarttys.app.adapter.SNGAdapter;
import com.app.Smarttys.app.dialog.CustomAlertDialog;
import com.app.Smarttys.app.utils.BlockUserUtils;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.app.widget.CircleImageView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SelectPeopleForGroupChat extends CoreActivity {

    private static final String TAG = "SelectPeopleForGroupCha";
    RecyclerView lvContacts;
    AvnNextLTProRegTextView contact_empty_selectgroup;
    ImageView backarrow, backButton;
    HorizontalScrollView selectgroupmember;
    LinearLayout mainlayout;
    AvnNextLTProRegTextView selectcontactmember;
    AvnNextLTProDemiTextView selectcontact;
    Button doneButton;
    EditText etSearch;
    List<Map<String, String>> mylist = new ArrayList<>();
    private List<SmarttyContactModel> filterContacts;
    private SNGAdapter adapter;
    private SearchView searchView;
    private SessionManager sessionManager;
    private ImageView search;
    private ArrayList<String> member = new ArrayList<>();
    private String mCurrentUserId;

    private Getcontactname getcontactname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_group);
        getSupportActionBar().hide();
        /* Get the participant Provider to get the list of participants */

        contact_empty_selectgroup = findViewById(R.id.contact_empty_selectgroup);
        ArrayList<SmarttyContactModel> smarttyEntries = new ArrayList<>();
        filterContacts = new ArrayList<>();

        lvContacts = findViewById(R.id.listContactsgroup);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SelectPeopleForGroupChat.this.getApplicationContext());
        lvContacts.setLayoutManager(mLayoutManager);
        lvContacts.setItemAnimator(new DefaultItemAnimator());
        lvContacts.setNestedScrollingEnabled(false);
        mainlayout = findViewById(R.id.maincontainer);
        selectcontactmember = findViewById(R.id.selectcontactmember);
        selectcontact = findViewById(R.id.selectcontact);
        backButton = findViewById(R.id.backarrow_contactsetting);
        etSearch = findViewById(R.id.etSearch);
        backarrow = findViewById(R.id.backarrow);
        search = findViewById(R.id.search);
        doneButton = findViewById(R.id.doneButton);
        selectgroupmember = findViewById(R.id.selectgroupmember);

        getcontactname = new Getcontactname(this);
        sessionManager = SessionManager.getInstance(this);
        mCurrentUserId = sessionManager.getCurrentUserID();

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        smarttyEntries = contactDB_sqlite.getSavedSmarttyContacts();
        Collections.sort(smarttyEntries, Getcontactname.nameAscComparator);
        adapter = new SNGAdapter(SelectPeopleForGroupChat.this, filterContacts);

        if (smarttyEntries.size() > 0) {

            for (SmarttyContactModel item : smarttyEntries) {
                String userId = item.get_id();
                if (!contactDB_sqlite.getBlockedMineStatus(userId, false).equals("1")) {
//                if (!contactsDB.getBlockedMineStatus(userId, false).equals("1")) {
                    filterContacts.add(item);
                }
            }

            Collections.sort(filterContacts, Getcontactname.nameAscComparator);
            smarttyEntries.clear();
            smarttyEntries.addAll(filterContacts);

            lvContacts.setAdapter(adapter);
            lvContacts.setVisibility(View.VISIBLE);
            contact_empty_selectgroup.setVisibility(View.GONE);
            doneButton.setVisibility(View.VISIBLE);
        } else {
            lvContacts.setVisibility(View.GONE);
            contact_empty_selectgroup.setVisibility(View.VISIBLE);
            contact_empty_selectgroup.setText("No Contacts Available to form Group");
            doneButton.setVisibility(View.GONE);
        }

        search.setOnClickListener(v -> {
            backarrow.setVisibility(View.VISIBLE);
            search.setVisibility(View.GONE);
            backButton.setVisibility(View.GONE);
            selectcontact.setVisibility(View.GONE);
            selectcontactmember.setVisibility(View.GONE);

            etSearch.setVisibility(View.VISIBLE);
            etSearch.requestFocus();

            etSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                    SelectPeopleForGroupChat.this.adapter.getFilter().filter(cs);
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                              int arg3) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                }
            });
            backarrow.setVisibility(View.VISIBLE);
            backButton.setVisibility(View.GONE);

            backarrow.setOnClickListener(v1 -> {
                etSearch.getText().clear();
                adapter.updateInfo(filterContacts);
                etSearch.setVisibility(View.GONE);
                search.setVisibility(View.VISIBLE);
                selectcontactmember.setVisibility(View.VISIBLE);
                selectcontact.setVisibility(View.VISIBLE);
                backarrow.setVisibility(View.GONE);
                backButton.setVisibility(View.VISIBLE);

                if (SelectPeopleForGroupChat.this.getCurrentFocus() != null) {
                    InputMethodManager inputMethodManager =
                            (InputMethodManager) getApplicationContext().getSystemService(
                                    Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(
                            SelectPeopleForGroupChat.this.getCurrentFocus().getWindowToken(), 0);

                }
            });

        });
        backButton.setOnClickListener(v -> finish());

        doneButton.setOnClickListener(v -> {
            if (member.size() > 0) {
                Intent intent = new Intent(SelectPeopleForGroupChat.this, GroupCreation.class);
                intent.putStringArrayListExtra("mylist", member);
                startActivity(intent);
            } else {
                Toast.makeText(SelectPeopleForGroupChat.this, "Please select atleast one member", Toast.LENGTH_LONG).show();
            }
        });


        lvContacts.addOnItemTouchListener(new RItemAdapter(
                SelectPeopleForGroupChat.this, lvContacts, new RItemAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                SmarttyContactModel selectedContact = adapter.getItem(position);
                performAddMember(view, selectedContact);
            }

            @Override
            public void onItemLongClick(View view, int position) {
            }
        }));

    }

    private void performAddMember(View view, SmarttyContactModel selectedContact) {
        String userId = selectedContact.get_id();
        String name = selectedContact.getMsisdn();
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);

        if (!contactDB_sqlite.getBlockedStatus(userId, false).equals("1")) {
            addContactToGroup(view, selectedContact);
        } else {
            Getcontactname getcontactname = new Getcontactname(this);
            String message = "Unblock" + " " + getcontactname.getSendername(userId, name) + " to add group?";
            displayAlert(message, userId);
        }
    }

    private void displayAlert(final String txt, final String userId) {
        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(txt);
        dialog.setNegativeButtonText("Cancel");
        dialog.setPositiveButtonText("Unblock");
        dialog.setCancelable(false);
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                BlockUserUtils.changeUserBlockedStatus(SelectPeopleForGroupChat.this, EventBus.getDefault(),
                        mCurrentUserId, userId, false);
                dialog.dismiss();
            }

            @Override

            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getSupportFragmentManager(), "Unblock a person");
    }

    private void addContactToGroup(View view, SmarttyContactModel selectedItem) {
        CheckBox c = view.findViewById(R.id.selectedmember);

        LayoutInflater inflater = (LayoutInflater) SelectPeopleForGroupChat.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final Map<String, String> myMAp = new HashMap<>();
        myMAp.clear();

        myMAp.put("receiverUid", selectedItem.getNumberInDevice());
        myMAp.put("receiverName", selectedItem.getFirstName());
        myMAp.put("documentId", selectedItem.get_id());
        myMAp.put("Username", selectedItem.getFirstName());
        myMAp.put("Image", selectedItem.getAvatarImageUrl());
        myMAp.put("Status", selectedItem.getStatus());
        myMAp.put("id", selectedItem.get_id());
        String memberId = selectedItem.get_id();
        if (memberId != null && !memberId.equals("")) {
            member.add(memberId);

            mylist.add(myMAp);
            if (member.size() > 0) {
                selectgroupmember.setVisibility(View.VISIBLE);
            } else {
                selectgroupmember.setVisibility(View.GONE);
            }

            final View view1 = inflater.inflate(R.layout.viewtoinflate, mainlayout, false);
            final CircleImageView image = view1.findViewById(R.id.image);
            final AvnNextLTProRegTextView phoneNumber = view1.findViewById(R.id.phonenumber);

            phoneNumber.setText(selectedItem.getNumberInDevice());
            getcontactname.configProfilepic(image, selectedItem.get_id(), true, false, R.mipmap.chat_attachment_profile_default_image_frame);

            final AvnNextLTProRegTextView Selectedmemname = view1.findViewById(R.id.selectedmembername);
            Selectedmemname.setText(selectedItem.getFirstName());
            ImageView removeicon = view1.findViewById(R.id.removeicon);
            image.setPadding(20, 0, 0, 0);
            removeicon.setOnClickListener(v -> {
                MyLog.d("called 2", "" + phoneNumber.getText().toString());

                mainlayout.removeView(view1);

                for (int i = 0; i < mylist.size(); i++) {
                    if (mylist.get(i).get("receiverUid").equals(phoneNumber.getText().toString())) {
                        SmarttyContactModel contact = new SmarttyContactModel();
                        contact.setNumberInDevice(mylist.get(i).get("receiverUid"));
                        //  contact.setNumberInDevice(mylist.get(i).get("receiverUid"));
                        Log.e("SelectPeopleGroupChat", "setNumberInDevice" + mylist.get(i).get("receiverUid"));

                        contact.setFirstName(mylist.get(i).get("receiverName"));
                        contact.setAvatarImageUrl(mylist.get(i).get("Image"));
                        contact.setStatus(mylist.get(i).get("Status"));
                        contact.set_id(mylist.get(i).get("id"));
                        filterContacts.add(0, contact);
                        runOnUiThread(() -> adapter.notifyDataSetChanged());

                        mylist.remove(i);
                        member.remove(i);
                        break;
                    }
                }
            });

            mainlayout.addView(view1);

            int index = filterContacts.indexOf(selectedItem);
            if (index > -1) {
                filterContacts.remove(index);
            }

            etSearch.setText("");
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.select_people_for_group, menu);
        MenuItem searchItem = menu.findItem(R.id.menuSearch);

        if (filterContacts != null && filterContacts.size() > 0) {

            searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    searchView.setIconifiedByDefault(true);
                    searchView.setIconified(true);
                    searchView.setQuery("", false);
                    searchView.clearFocus();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {

                    if (newText.equals("") && newText.isEmpty()) {
                        searchView.clearFocus();
                        //closeKeypad();
                    }
                    adapter.getFilter().filter(newText);
                    return false;
                }
            });

            searchView.setOnCloseListener(() -> {
                menu.findItem(R.id.menuSearch).setVisible(true);
                return false;
            });

            searchView.setOnSearchClickListener(v -> menu.findItem(R.id.menuSearch).setVisible(false));

            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            searchView.setIconifiedByDefault(true);
            searchView.setQuery("", false);
            searchView.clearFocus();
            searchView.setIconified(true);

            AutoCompleteTextView searchTextView = searchView.findViewById(R.id.search_src_text);
            try {
                Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
                mCursorDrawableRes.setAccessible(true);
                mCursorDrawableRes.set(searchTextView, null);
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_PRIVACY_SETTINGS)) {
            loadPrivacySetting(event);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_BLOCK_USER)) {
            loadBlockUserMessage(event);
        }
    }

    private void loadPrivacySetting(ReceviceMessageEvent event) {
        adapter.notifyDataSetChanged();
    }

    private void loadBlockUserMessage(ReceviceMessageEvent event) {
        try {
            Object[] obj = event.getObjectsArray();
            JSONObject object = new JSONObject(obj[0].toString());

            String from = object.getString("from");
            String to = object.getString("to");

            if (mCurrentUserId.equalsIgnoreCase(from)) {
                for (int i = 0; i < filterContacts.size(); i++) {
                    String userId = filterContacts.get(i).get_id();
                    if (userId.equals(to)) {
                        String stat = object.getString("status");
                        if (stat.equalsIgnoreCase("1")) {
                            Toast.makeText(this, "Number is blocked", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this, "Number is Unblocked", Toast.LENGTH_SHORT).show();
                        }
                        adapter.notifyDataSetChanged();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }
}