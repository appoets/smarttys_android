package com.app.Smarttys.app.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.StarredMessageAdapter;
import com.app.Smarttys.app.dialog.ChatLockPwdDialog;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.ChatLockPojo;
import com.app.Smarttys.core.model.MessageItemChat;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by CAS63 on 2/3/2017.
 */
public class StarredItemList extends CoreActivity implements StarredMessageAdapter.ChatMessageItemClickListener {
    private static final String TAG = "StarredItemList";
    MessageItemChat myitems;
    StarredMessageAdapter mAdapter;
    ArrayList<MessageItemChat> items;
    ArrayList<MessageItemChat> dbItems;
    MessageDbController db;
    RecyclerView rvdata;
    String uniqueCurrentID;
    private Boolean isGroupChat;
    private String from, receiverDocumentID;
    private LinearLayoutManager mLayoutManager;
    private SearchView searchView;
    private UserInfoSession userInfoSession;
    private Getcontactname getcontactname;
//    public TextView tv_starredmsg_no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.starred_listview);
        db = CoreController.getDBInstance(this);
//        tv_starredmsg_no = (TextView) findViewById(R.id.tv_nostarred);
        userInfoSession = new UserInfoSession(StarredItemList.this);
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.starred_messages));
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        getcontactname = new Getcontactname(StarredItemList.this);
        actionBar.setDisplayHomeAsUpEnabled(true);
        from = SessionManager.getInstance(this).getCurrentUserID();
        rvdata = findViewById(R.id.rvstarred);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvdata.setLayoutManager(mLayoutManager);
        rvdata.setItemAnimator(new DefaultItemAnimator());
        rvdata.setHasFixedSize(true);

        dbItems = new ArrayList<>();
        mAdapter = new StarredMessageAdapter(StarredItemList.this, dbItems, StarredItemList.this.getSupportFragmentManager());
//        showProgressDialog();
        updatestaredmessages_fromDB();
//        new LoadStarredMessageTask().execute();
    }

    private void updatestaredmessages_fromDB() {
        dbItems = db.selectAllStarredMessages();
        if (dbItems.size() > 0 && mAdapter != null) {
            mAdapter.setItemClickListener(StarredItemList.this);
            rvdata.setAdapter(mAdapter);
        } else {
            Toast.makeText(StarredItemList.this, R.string.no_starred_messages_found, Toast.LENGTH_SHORT).show();
        }
    }


//    private class LoadStarredMessageTask extends AsyncTask<Void, Void, Void> {
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//
//            return dbItems;
//        }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//            super.onPostExecute(aVoid);
//            hideProgressDialog();
//
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.select_people_for_group, menu);
        MenuItem searchItem = menu.findItem(R.id.menuSearch);

        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do whatever you need
                return true; // KEEP IT TO TRUE OR IT DOESN'T OPEN !!
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do whatever you need
                return true; // OR FALSE IF YOU DIDN'T WANT IT TO CLOSE!
            }
        });
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.setIconifiedByDefault(true);
                searchView.setIconified(true);
                searchView.setQuery("", false);
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (newText.equals("") && newText.isEmpty()) {
                    searchView.clearFocus();
                    //closeKeypad();
                }
                if (newText.length() > 0) {
                    mAdapter.getFilter().filter(newText);
                    if (StarredMessageAdapter.starredserach_result) {
//                        tv_starredmsg_no.setVisibility(View.VISIBLE);
                    } else {
//                        tv_starredmsg_no.setVisibility(View.GONE);
                    }
                } else {
                    mAdapter.updateInfo(dbItems);
//                    tv_starredmsg_no.setVisibility(View.GONE);
                }

                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                menu.findItem(R.id.menuSearch).setVisible(true);
                return false;
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.findItem(R.id.menuSearch).setVisible(false);
            }
        });


        searchView.setIconifiedByDefault(true);
        searchView.setQuery("", false);
        searchView.clearFocus();
        searchView.setIconified(true);

        AutoCompleteTextView searchTextView = searchView.findViewById(R.id.search_src_text);
        searchTextView.setTextColor(Color.WHITE);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, 0);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

        MenuItemCompat.setActionView(searchItem, searchView);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {  //do you back event work here
            searchView.onActionViewCollapsed();

        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(View itemView, int position) {

        ChatLockPojo lockPojo = getChatLockdetailfromDB(position);
        if (SessionManager.getInstance(StarredItemList.this).getLockChatEnabled().equals("1")
                && lockPojo != null) {
            String stat = "", pwd = null;

            stat = lockPojo.getStatus();
            pwd = lockPojo.getPassword();

            MessageItemChat e = mAdapter.getItem(position);
            String docID = e.getMessageId();
            String[] ids = docID.split("-");
            String documentID = "";
            if (e.getMessageId().contains("-g")) {
                documentID = ids[0] + "-" + ids[1] + "-g";
            } else {
                documentID = ids[0] + "-" + ids[1];
            }
            if (stat.equals("1")) {
                openUnlockChatDialog(documentID, stat, pwd, position);
            } else {
                navigateTochatviewpage(position);
            }
        } else {
            navigateTochatviewpage(position);
        }

        mAdapter.stopAudioOnNavigate();
    }

    @Override
    public void onItemLongClick(View itemView, int position) {

    }

    private void navigateTochatviewpage(int position) {
        MessageItemChat msgItem = mAdapter.getItem(position);
        String msgId = msgItem.getMessageId();

        if (msgId.contains("-g")) {
            msgItem.setSenderName(msgItem.getGroupName());
        }

        getcontactname.navigateToChatViewpagewithmessageitems(msgItem, "star");
        finish();
    }

    private void openUnlockChatDialog(String docId, String status, String pwd, int position) {
        String convId = userInfoSession.getChatConvId(docId);
        ChatLockPwdDialog dialog = new ChatLockPwdDialog();
        dialog.setTextLabel1("Enter your Password");
        dialog.setEditTextdata("Enter new Password");
        dialog.setforgotpwdlabel("Forgot Password");
        dialog.setHeader("Unlock Chat");
        dialog.setButtonText("Unlock");
        Bundle bundle = new Bundle();
        bundle.putSerializable("MessageItem", mAdapter.getItem(position));
        if (docId.contains("-g")) {
            bundle.putString("convID", docId.split("-")[1]);
        } else {
            bundle.putString("convID", convId);
        }
        bundle.putString("status", "1");
        bundle.putString("pwd", pwd);
        bundle.putString("page", "chatlist");
        if (docId.contains("-g")) {
            bundle.putString("type", "group");
        } else {
            bundle.putString("type", "single");
        }
        bundle.putString("from", uniqueCurrentID);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "chatunLock");
    }

    private ChatLockPojo getChatLockdetailfromDB(int position) {

        MessageItemChat e = mAdapter.getItem(position);
        String docID = e.getMessageId();
        String[] id = docID.split("-");
        String documentID = "";
        String chatType = MessageFactory.CHAT_TYPE_SINGLE;
        if (e.getMessageId().contains("-g")) {
            documentID = id[0] + "-" + id[1] + "-g";
            chatType = MessageFactory.CHAT_TYPE_GROUP;
        } else {
            documentID = id[0] + "-" + id[1];
        }
        MessageDbController dbController = CoreController.getDBInstance(this);
        String convId = userInfoSession.getChatConvId(documentID);
        String receiverId = userInfoSession.getReceiverIdByConvId(convId);
        ChatLockPojo pojo = dbController.getChatLockData(receiverId, chatType);
        return pojo;
    }

    public class CustomSearchView extends SearchView {
        public CustomSearchView(final Context context) {
            super(context);
            this.setIconifiedByDefault(true);
        }

        @Override
        public boolean dispatchKeyEventPreIme(KeyEvent event) {
            if (event.getKeyCode() == KeyEvent.KEYCODE_BACK &&
                    event.getAction() == KeyEvent.ACTION_UP) {
                this.onActionViewCollapsed();
            }
            return super.dispatchKeyEventPreIme(event);
        }
    }
}




