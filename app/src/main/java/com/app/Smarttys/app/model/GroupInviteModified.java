package com.app.Smarttys.app.model;

public class GroupInviteModified {
    private String groupId;

    public GroupInviteModified(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
