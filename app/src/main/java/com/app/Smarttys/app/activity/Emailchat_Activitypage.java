package com.app.Smarttys.app.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.SmarttyCASocket;
import com.app.Smarttys.app.dialog.CustomAlertDialog;
import com.app.Smarttys.app.utils.EmailChatHistoryUtils;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by CAS56 on 3/9/2017.
 */
public class Emailchat_Activitypage extends CoreActivity implements AdapterView.OnItemClickListener {

    private static final String TAG = "Emailchat_Activitypage";
    final Context context = this;
    RecyclerView lvContacts;
    SmarttyCASocket adapter;
    InputMethodManager inputMethodManager;
    ProgressDialog dialog;
    AvnNextLTProRegTextView resevernameforward;
    ImageView sendmessage;
    RelativeLayout sendlayout;
    String mCurrentUserId, textMsgFromVendor;
    String id, mReceiverName;
    ArrayList<SmarttyContactModel> smarttyEntries;
    private SessionManager sessionManager;
    private List<SmarttyContactModel> selectedContactsList;
    private List<SmarttyContactModel> dataList;
    private FileUploadDownloadManager uploadDownloadManager;
    private ArrayList<String> myEmailChatInfo = new ArrayList<String>();
    private SearchView searchView;
    //    MessageDbController db;
    private String contact;
    private ArrayList<MessageItemChat> mChatData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forward_contact);

        setTitle("Choose chat..");
        lvContacts = findViewById(R.id.listContacts);

        sendmessage = findViewById(R.id.overlapImage);
        resevernameforward = findViewById(R.id.chat_text_view);
        sendlayout = findViewById(R.id.sendlayout);

        sendmessage.setVisibility(View.GONE);
        sendlayout.setVisibility(View.GONE);

        mChatData = new ArrayList<>();
        uploadDownloadManager = new FileUploadDownloadManager(Emailchat_Activitypage.this);
        mCurrentUserId = SessionManager.getInstance(this).getCurrentUserID();

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        smarttyEntries = contactDB_sqlite.getSavedSmarttyContacts();
        if (smarttyEntries == null) {
            syncContacts();
        } else {
            dataList = new ArrayList<>();
            for (SmarttyContactModel contact : smarttyEntries) {
                contact.setSelected(false);
                dataList.add(contact);
            }

            Collections.sort(dataList, Getcontactname.nameAscComparator);
            adapter = new SmarttyCASocket(Emailchat_Activitypage.this, dataList);
            lvContacts.setAdapter(adapter);
            lvContacts.setHasFixedSize(true);
            LinearLayoutManager mediaManager = new LinearLayoutManager(Emailchat_Activitypage.this, LinearLayoutManager.VERTICAL, false);
            lvContacts.setLayoutManager(mediaManager);
        }

        selectedContactsList = new ArrayList<>();
        adapter.setChatListItemClickListener(new SmarttyCASocket.ChatListItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                SmarttyContactModel userData = adapter.getItem(position);
                id = userData.get_id();
                if (userData.getFirstName() == null || userData.getFirstName().equals("")) {
                    mReceiverName = userData.getMsisdn();
                } else {
                    mReceiverName = userData.getFirstName();
                }
                loadFromDB();
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        });

        sessionManager = SessionManager.getInstance(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {


    }

    private void loadFromDB() {
        ArrayList<MessageItemChat> items;
        String singeDocId = mCurrentUserId.concat("-").concat(id);
        MessageDbController db = CoreController.getDBInstance(this);
        items = db.selectAllChatMessages(singeDocId, MessageFactory.CHAT_TYPE_SINGLE);
        mChatData.clear();
        mChatData.addAll(items);

        if (mChatData.size() > 0) {
            performMenuEmailChat();
        } else {
            Toast.makeText(this, "No conversation found", Toast.LENGTH_SHORT).show();
        }

    }

    private void performMenuEmailChat() {
        final String msg = "Attaching media will generate a large email message.";

        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setNegativeButtonText("Without media");
        dialog.setPositiveButtonText("Attach media");
        dialog.setMessage(msg);
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                dialog.dismiss();
                final ProgressDialog dialogpr = ProgressDialog.show(Emailchat_Activitypage.this, "",
                        "Loading. Please wait...", true);
                final Timer timer2 = new Timer();
                timer2.schedule(new TimerTask() {
                    public void run() {
                        dialogpr.dismiss();
                        timer2.cancel(); //this will cancel the timer of the system
                    }
                }, 2500); // the timer will count 2.5 seconds....

                String docId;
                docId = mCurrentUserId.concat("-").concat(id);
                EmailChatHistoryUtils emilChat = new EmailChatHistoryUtils(Emailchat_Activitypage.this);
                emilChat.send(docId, mReceiverName, true, true, MessageFactory.CHAT_TYPE_SINGLE);
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
                String docId;
                docId = mCurrentUserId.concat("-").concat(id);

                EmailChatHistoryUtils emilChat = new EmailChatHistoryUtils(Emailchat_Activitypage.this);
                emilChat.send(docId, mReceiverName, false, true, MessageFactory.CHAT_TYPE_SINGLE);
            }
        });
        dialog.show(getSupportFragmentManager(), "Delete member alert");
    }


    private void syncContacts() {
        contactsFromCursor();
    }

    private void contactsFromCursor() {
        SmarttyContactsService.contactEntries = new ArrayList<>();
        showProgressDialog();

        Uri contactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI; // The content URI of the phone contacts
        String[] projection = {                                  // The columns to return for each row
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER
        };
        String selection = null;                                 //Selection criteria
        String[] selectionArgs = {};                             //Selection criteria
        String sortOrder = null;                                 //The sort order for the returned rows

        Cursor cursor = getContentResolver().query(contactsUri, projection, selection, selectionArgs, sortOrder);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            JSONArray arrContacts = new JSONArray();
            do {
                SmarttyContactModel d = new SmarttyContactModel();
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));


                d.setFirstName(name);
//                contact += phNumber.trim() + ",";

                try {
                    String phNo = phNumber.replace(" ", "").replace("(", "").replace(")", "").replace("-", "");
                    d.setNumberInDevice(phNo);
                    Log.e("EmailCHAT", "setNumberInDevice" + phNo);

                    JSONObject contactObj = new JSONObject();
                    contactObj.put("Phno", phNo);
                    contactObj.put("Name", name);
                    arrContacts.put(contactObj);
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }

                SmarttyContactsService.contactEntries.add(d);

            } while (cursor.moveToNext());
            contact = arrContacts.toString();
        }
        updateDataToTheServer();
    }

    private void updateDataToTheServer() {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_GET_FAVORITE);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("indexAt", "0");
            jsonObject.put("msisdn", SessionManager.getInstance(this).getPhoneNumberOfCurrentUser());
            jsonObject.put("Contacts", contact);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        messageEvent.setMessageObject(jsonObject);
        EventBus.getDefault().post(messageEvent);
    }


    private void storeContact(ReceviceMessageEvent event) throws JSONException {
        Object[] args = event.getObjectsArray();
        JSONObject data = new JSONObject(args[0].toString());
        JSONArray array = data.getJSONArray("Favorites");

        smarttyEntries = new ArrayList<>();

        for (int contactIndex = 0; contactIndex < SmarttyContactsService.contactEntries.size(); contactIndex++) {
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = new JSONObject(array.get(i).toString());
                String msisdn = obj.getString("msisdn");
                String id = obj.getString("_id");
                String profilePic = obj.getString("ProfilePic");

                SmarttyContactModel entry = SmarttyContactsService.contactEntries.get(contactIndex);
                if (msisdn.contains(entry.getNumberInDevice())) {
                    smarttyEntries.add(entry);

                }
            }
        }

        dataList = new ArrayList<>();
        for (SmarttyContactModel contact : smarttyEntries) {
            contact.setSelected(false);
            dataList.add(contact);
        }

        adapter = new SmarttyCASocket(Emailchat_Activitypage.this, dataList);
        lvContacts.setAdapter(adapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (SocketManager.EVENT_GET_CONTACTS.equalsIgnoreCase(event.getEventName())) {
            try {
                storeContact(event);
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } else if (SocketManager.EVENT_GET_CONTACTS.equalsIgnoreCase(event.getEventName())) {
            try {
                storeContact(event);
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_forward_contact, menu);
        MenuItem searchItem = menu.findItem(R.id.chats_searchIcon);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.setIconifiedByDefault(true);
                searchView.setIconified(true);
                searchView.setQuery("", false);
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.equals("") && newText.isEmpty()) {
                    searchView.clearFocus();
                }
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        searchView.setIconifiedByDefault(true);
        searchView.setQuery("", false);
        searchView.clearFocus();
        searchView.setIconified(true);

        AutoCompleteTextView searchTextView = searchView.findViewById(R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, 0); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}

