package com.app.Smarttys.app.adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.GroupInfo;
import com.app.Smarttys.app.dialog.ProfileImageDialog;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.widget.AvnNextLTProDemiButton;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.model.GroupMembersPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.smarttyhelperclass.SmarttyRegularExp;
import com.kyleduo.switchbutton.SwitchButton;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by CAS60 on 11/29/2016.
 */

public class GroupInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>/*RecyclerView.Adapter<GroupInfoAdapter.ViewHolderGroupInfo>*/ {

    private static final String TAG = GroupInfoAdapter.class.getSimpleName();
    final int VIEW_TYPE_HEADER = 0;
    final int VIEW_TYPE_RESOURCEPAGE = 1;
    final int VIEW_TYPE_PARENTPAGE = 2;
    final int VIEW_TYPE_COACHPAGE = 3;
    final int VIEW_TYPE_FOOTER = 4;
    final int VIEW_TYPE_MEMBERTYPE = 5;
    private final Pattern sPattern = Pattern.compile("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$");
    SessionManager sessionmanager;
    Getcontactname getcontactname;
    private List<GroupMembersPojo> membersList;
    private Context mContext;
    private RItemAdapter.OnItemClickListener clickListener;
    private FragmentManager fragmentManager;
    private String currentUserId = "";
    private GroupInfo mActivity;
    private boolean isGroupMembersAvailable = false;


    public GroupInfoAdapter(GroupInfo mActivity, Context mContext, List<GroupMembersPojo> list, FragmentManager fragmentManager, RItemAdapter.OnItemClickListener listener) {
        this.mActivity = mActivity;
        this.membersList = getTempList(list);
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
        sessionmanager = SessionManager.getInstance(mContext);
        getcontactname = new Getcontactname(mContext);
        clickListener = listener;
        currentUserId = sessionmanager.getCurrentUserID();
    }


    private List<GroupMembersPojo> getTempList(List<GroupMembersPojo> list) {
        List<GroupMembersPojo> tempList = new ArrayList<>();
        tempList.add(0, new GroupMembersPojo());
        tempList.add(1, new GroupMembersPojo());
        tempList.add(2, new GroupMembersPojo());
        tempList.add(3, new GroupMembersPojo());

        /*if (list.size() == 0) {
            Log.e("list", "list zero" + list.size());
            tempList.add(new GroupMembersPojo());
        } else {
            for (int i = 0; i < list.size(); i++) {
                tempList.add(i + 3, list.get(i));
            }
        }*/

        if (list.size() > 0) {
            isGroupMembersAvailable = true;
            for (int i = 0; i < list.size(); i++) {
                tempList.add(i + 4, list.get(i));
            }
        } else {
            isGroupMembersAvailable = false;
        }
        Log.e(TAG, "tempList: original size: " + list.size());

        tempList.add(tempList.size(), new GroupMembersPojo());
        return tempList;
    }

    public void updateList(List<GroupMembersPojo> list) {
        // membersList.clear();
        // notifyDataSetChanged();

        this.membersList = getTempList(list);
    }

    @Override
    public int getItemCount() {
        if (membersList == null)
            return 0;
        return membersList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        Log.e(TAG, "onCreateViewHolder" + membersList.size() + " viewType" + viewType);


        if (viewType == VIEW_TYPE_HEADER) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_groupheader, null);
            ViewHolderHeader homeView = new ViewHolderHeader(v);
            return homeView;
        } else if (viewType == VIEW_TYPE_RESOURCEPAGE) {
            View v4 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_grouphorizontal, null);
            ViewHolderFiles resourceView = new ViewHolderFiles(v4);
            return resourceView;
        } else if (viewType == VIEW_TYPE_PARENTPAGE) {
            View v1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_groupnotify, null);
            ViewHolderMute parentView = new ViewHolderMute(v1);
            return parentView;
        } else if (viewType == VIEW_TYPE_FOOTER) {
            Log.e(TAG, "onCreateViewHolder:  bottom view");
            View v90 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_groupbottom, null);
            ViewHolderBottom formView = new ViewHolderBottom(v90);
            return formView;

        } else if (viewType == VIEW_TYPE_MEMBERTYPE) {
            Log.e(TAG, "onCreateViewHolder:  bottom view");
            View v21 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_groupmemberheader, null);
            ViewHolderGroupMember formView = new ViewHolderGroupMember(v21);
            return formView;

        } else {
            View v3 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_group_info, null);
            ViewHolderGroupInfo coachView = new ViewHolderGroupInfo(v3);
            return coachView;
        }


    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int position) {
        Log.e(TAG, "onBindViewHolder: position=" + position);
        if (viewHolder instanceof ViewHolderHeader) {
            Log.e(TAG, "onBindViewHolder: header");
            mActivity.media(((ViewHolderHeader) viewHolder).media_lineralayout, ((ViewHolderHeader) viewHolder).medialayout, ((ViewHolderHeader) viewHolder).mediacount);
        } else if (position == VIEW_TYPE_RESOURCEPAGE) {/*else if (viewHolder instanceof ViewHolderFiles) {*/
            //  Log.e(TAG, "onBindViewHolder: files");
            //  Log.e("onBindViewHolder", "rvMediainit");
            mActivity.rvMediainit(((ViewHolderFiles) viewHolder).rvMedia);
        } else if (viewHolder instanceof ViewHolderMute) {
            Log.e(TAG, "onBindViewHolder: media");
            mActivity.mutecheckbox(((ViewHolderMute) viewHolder).swMute, ((ViewHolderMute) viewHolder).mute);

        } else if (viewHolder instanceof ViewHolderBottom) {
            Log.e(TAG, "onBindViewHolder: bottom");
            mActivity.footerinit(((ViewHolderBottom) viewHolder).btnDeleteGroup, ((ViewHolderBottom) viewHolder).btnExitGroup);

        } else if (viewHolder instanceof ViewHolderGroupMember) {
            Log.e(TAG, "onBindViewHolder: groupheader");
            mActivity.groupmemberinit(((ViewHolderGroupMember) viewHolder).tvParticipantTitle, ((ViewHolderGroupMember) viewHolder).groupempty, ((ViewHolderGroupMember) viewHolder).tvMembersCount, ((ViewHolderGroupMember) viewHolder).tvAddMember, ((ViewHolderGroupMember) viewHolder).media_lineralayout);
            ((ViewHolderGroupMember) viewHolder).tvAddMember.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mActivity.Addmemeber();
                }
            });
        } else {


            final GroupMembersPojo member = membersList.get(position);
            final String memmberid = member.getUserId();
            MyLog.d(TAG, "onBindViewHolder:  " + position + ": " + member.getName());
            if (member.getContactName() != null && !member.getContactName().trim().isEmpty())
                ((ViewHolderGroupInfo) viewHolder).tvName.setText(member.getContactName());
            else
                ((ViewHolderGroupInfo) viewHolder).tvName.setText(member.getName());

            ((ViewHolderGroupInfo) viewHolder).tvPendingUser.setVisibility(View.GONE);
            if (member.getIsPending() != null && member.getIsPending().equals("1")) {
                ((ViewHolderGroupInfo) viewHolder).tvPendingUser.setVisibility(View.VISIBLE);
            } else {
                ((ViewHolderGroupInfo) viewHolder).tvName.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            }

            ((ViewHolderGroupInfo) viewHolder).ivUserDp.setImageResource(R.mipmap.chat_attachment_profile_default_image_frame);
            ((ViewHolderGroupInfo) viewHolder).tvOriginalName.post(new Runnable() {
                @Override
                public void run() {
                    String originalName = member.getUserOriginalName();
              /* try {
                   if (SmarttyRegularExp.isEncodedBase64String(originalName)) {
                       byte[] arrStatus = Base64.decode(originalName, Base64.DEFAULT);
                       originalName = new String(arrStatus, "UTF-8");
                       originalName = "~" + originalName;
                   }
               } catch (Exception e) {
                   Log.e(TAG, "run: ", e);
               } finally {
                   holder.tvOriginalName.setText(originalName);
               }*/
                    ((ViewHolderGroupInfo) viewHolder).tvOriginalName.setText(originalName);

                }

            });
            ((ViewHolderGroupInfo) viewHolder).tvAdmin.setVisibility(View.GONE);
            if (member.getIsAdminUser() != null) {
                if (member.getIsAdminUser().equalsIgnoreCase("1")) {
                    ((ViewHolderGroupInfo) viewHolder).tvAdmin.setVisibility(View.VISIBLE);
                }
            }


            final String status = membersList.get(position).getStatus();

            if (currentUserId.equalsIgnoreCase(memmberid)) {
                ((ViewHolderGroupInfo) viewHolder).tvStatus.setText(sessionmanager.getcurrentUserstatus());


/*            String userprofilepic = sessionmanager.getUserProfilePic();
           Picasso.with(mContext).load(Constants.SOCKET_IP + userprofilepic).error(
                   R.drawable.personprofile)
                   .transform(new CircleTransform()).into(holder.ivUserDp);*/
                AppUtils.loadProfilePic(mContext, ((ViewHolderGroupInfo) viewHolder).ivUserDp);
            } else {
                ((ViewHolderGroupInfo) viewHolder).tvStatus.post(new Runnable() {
                    @Override
                    public void run() {
                        String statusFinal = status;
                        Log.e("statusFinal", "statusFinal" + statusFinal);
                        //Check it is null or empty
                        if (!AppUtils.isEmpty(statusFinal)) {
                            try {
                                if (SmarttyRegularExp.isEncodedBase64String(status)) {
                                    byte[] arrStatus = Base64.decode(status, Base64.DEFAULT);
                                    statusFinal = new String(arrStatus, StandardCharsets.UTF_8);
                                }
                            } catch (Exception e) {
                                MyLog.e(TAG, "run: ", e);
                            } finally {
                                // getcontactname.setProfileStatusText(holder.tvStatus, memmberid, statusFinal, false);

                                ((ViewHolderGroupInfo) viewHolder).tvStatus.setText(statusFinal);

                            }
                        }


                    }
                });
                getcontactname.configProfilepic(((ViewHolderGroupInfo) viewHolder).ivUserDp, memmberid, false, true, R.mipmap.chat_attachment_profile_default_image_frame);
                ((ViewHolderGroupInfo) viewHolder).ivUserDp.setOnClickListener(new ProfileClick(position, ((ViewHolderGroupInfo) viewHolder)));

                ((ViewHolderGroupInfo) viewHolder).clickArea.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickListener.onItemClick(((ViewHolderGroupInfo) viewHolder).clickArea, position);
                    }
                });
            }
        }
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemViewType(int position) {

        if (membersList == null)
            return super.getItemViewType(position);
        if (membersList.size() > 0) {
            if (position == 0)
                return VIEW_TYPE_HEADER;
            else if (position == 1)
                return VIEW_TYPE_RESOURCEPAGE;
            else if (position == 2)
                return VIEW_TYPE_PARENTPAGE;
            if (isGroupMembersAvailable) {
                if (position == 3) {
                    return VIEW_TYPE_MEMBERTYPE;
                } else if (position == membersList.size() - 1)
                    return VIEW_TYPE_FOOTER;

                else
                    // return super.getItemViewType(position);
                    return VIEW_TYPE_COACHPAGE;

            } else {
                if (position == 3) {
                    return VIEW_TYPE_MEMBERTYPE;
                } else if (position == membersList.size() - 1)
                    return VIEW_TYPE_FOOTER;

                else
                    // return super.getItemViewType(position);
                    return VIEW_TYPE_COACHPAGE;

            }

        }
        return super.getItemViewType(position);
    }

    public class ViewHolderGroupInfo extends RecyclerView.ViewHolder {
        ImageView ivUserDp;
        TextView tvName, tvAdmin, tvPendingUser, tvStatus, tvOriginalName;
        View clickArea;

        public ViewHolderGroupInfo(View itemView) {
            super(itemView);

            ivUserDp = itemView.findViewById(R.id.ivUserDp);
            tvName = itemView.findViewById(R.id.tvName);
            tvAdmin = itemView.findViewById(R.id.tvAdmin);
            tvPendingUser = itemView.findViewById(R.id.tvPending);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvOriginalName = itemView.findViewById(R.id.tv_original_name);
            clickArea = itemView.findViewById(R.id.view_item_click);
        }
    }

    public class ViewHolderHeader extends RecyclerView.ViewHolder {
        LinearLayout media_lineralayout;
        RelativeLayout medialayout;
        TextView mediacount;

        public ViewHolderHeader(View itemView) {
            super(itemView);
            media_lineralayout = itemView.findViewById(R.id.media_lineralayout);
            medialayout = itemView.findViewById(R.id.medialayout);
            mediacount = itemView.findViewById(R.id.mediacount);

        }
    }

    public class ViewHolderFiles extends RecyclerView.ViewHolder {
        RecyclerView rvMedia;
        FrameLayout listfiles;

        public ViewHolderFiles(View itemView) {
            super(itemView);
            rvMedia = itemView.findViewById(R.id.rvMedia);
            listfiles = itemView.findViewById(R.id.listfiles);

        }
    }

    public class ViewHolderMute extends RecyclerView.ViewHolder {
        AvnNextLTProDemiTextView mute;
        private SwitchButton swMute;

        public ViewHolderMute(View itemView) {
            super(itemView);
            swMute = itemView.findViewById(R.id.swMute);
            mute = itemView.findViewById(R.id.mute);

        }
    }

    public class ViewHolderBottom extends RecyclerView.ViewHolder {
        RelativeLayout group_delete_exit_layout;
        AvnNextLTProDemiButton btnExitGroup;
        AvnNextLTProDemiButton btnDeleteGroup;

        public ViewHolderBottom(View itemView) {
            super(itemView);
            group_delete_exit_layout = itemView.findViewById(R.id.group_delete_exit_layout);
            btnDeleteGroup = itemView.findViewById(R.id.btnDeleteGroup);
            btnExitGroup = itemView.findViewById(R.id.btnExitGroup);


        }
    }

    public class ViewHolderGroupMember extends RecyclerView.ViewHolder {
        LinearLayout media_lineralayout;
        AvnNextLTProDemiTextView tvParticipantTitle;
        AvnNextLTProRegTextView groupempty, tvMembersCount, tvAddMember;
        ProgressBar pbHeaderProgress;

        public ViewHolderGroupMember(View itemView) {
            super(itemView);
            media_lineralayout = itemView.findViewById(R.id.media_lineralayout);
            tvParticipantTitle = itemView.findViewById(R.id.tvParticipantTitle);
            groupempty = itemView.findViewById(R.id.groupempty);
            tvAddMember = itemView.findViewById(R.id.tvAddMember);
            tvMembersCount = itemView.findViewById(R.id.tvMembersCount);
            pbHeaderProgress = itemView.findViewById(R.id.pbHeaderProgress);

        }
    }

    private class ProfileClick implements View.OnClickListener {

        int position = 0;
        ViewHolderGroupInfo holder;

        public ProfileClick(int position, ViewHolderGroupInfo holder) {

            this.position = position;
            this.holder = holder;


        }

        @Override
        public void onClick(View view) {

            //MessageItemChat list=(MessageItemChat) mDisplayedValues.get(position);

            MessageItemChat messageItemChat = new MessageItemChat();
            GroupMembersPojo groupMembersPojo = membersList.get(position);
            messageItemChat.setNumberInDevice(groupMembersPojo.getMsisdn());
            Log.e("GroupInfoAdapter", "setNumberInDevice" + groupMembersPojo.getMsisdn());

            messageItemChat.setAvatarImageUrl(groupMembersPojo.getUserDp());
            messageItemChat.setSenderName(groupMembersPojo.getContactName());
            messageItemChat.setSenderMsisdn(groupMembersPojo.getMsisdn());
            messageItemChat.setMessageId(currentUserId + "-" + groupMembersPojo.getUserId());

            String Uid = groupMembersPojo.getUserId();
            Bundle bundle = new Bundle();
            bundle.putSerializable("MessageItem", messageItemChat);
            bundle.putString("userID", Uid);

            bundle.putSerializable("ProfilePic", null);
            //need
            bundle.putSerializable("GroupChat", false);
            long imageTS = Calendar.getInstance().getTimeInMillis();
            bundle.putLong("imageTS", imageTS);
            bundle.putBoolean("FromSecretChat", false);
            ProfileImageDialog dialog = new ProfileImageDialog();
            dialog.setArguments(bundle);
            dialog.show(fragmentManager, "profile");
            GroupInfo.dismissInfo();
        }
    }

}

