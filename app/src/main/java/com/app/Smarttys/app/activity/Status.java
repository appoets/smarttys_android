package com.app.Smarttys.app.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.cursoradapter.widget.SimpleCursorAdapter;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.ConnectivityInfo;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.DatabaseClassForDB;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;


public class Status extends CoreActivity implements View.OnClickListener {
    /* Green Dao database to get the user status */
    protected static final String TAG = "ActionBarActivity";
    private static boolean isToasted = false;
    private final int NEW_STATUS_REQUEST_CODE = 2;
    ImageView backarrow_status;
    RelativeLayout rl_status;
    AvnNextLTProRegTextView currentStatus, selectStatus;
    SimpleCursorAdapter dataAdapter;
    Session session;
    int selectedPosition = -1;
    ArrayList<String> list = new ArrayList<String>();
    ArrayAdapter<String> adapter;
    //StatusAdapter adapter;
    DatabaseClassForDB statusDB = new DatabaseClassForDB(this);
    View v;
    ImageButton editStatus;
    Button updateButton;
    private ListView lv1;
    private EmojiconTextView textViewStatus;
    private com.app.Smarttys.core.model.SCLoginModel SCLoginModel = null;
    private ProgressDialog progressDialog;
    private Handler eventHandler;
    private Runnable eventRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.status);
        //setTitle("Status");
        session = new Session(Status.this);
        getSupportActionBar().hide();
        isToasted = false;
        /* Get the user status from local database */
        v = new View(Status.this);
        backarrow_status = findViewById(R.id.backarrow_status);
        backarrow_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        textViewStatus = findViewById(R.id.textViewStatus);
        textViewStatus.setTextColor(ContextCompat.getColor(Status.this, R.color.appthemecolour));
        /* Get the current user */

        textViewStatus.setText(SessionManager.getInstance(Status.this).getcurrentUserstatus());
        editStatus = findViewById(R.id.editStatus);
        updateButton = findViewById(R.id.updateButton);

        editStatus.setOnClickListener(this);

        rl_status = findViewById(R.id.statusInfo_rl);
        rl_status.setOnClickListener(this);

        lv1 = findViewById(R.id.listViewStatus);
        //statusDB.insetData();
        list = statusDB.displayStatusList();
        selectStatus = findViewById(R.id.selectStatus);
        currentStatus = findViewById(R.id.currentStatus);

        final Typeface typeface = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        textViewStatus.setTypeface(typeface);

        progressDialog = getProgressDialogInstance();
        progressDialog.setMessage(getString(R.string.loading_in));
        progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (eventHandler != null) {
                    eventHandler.removeCallbacks(eventRunnable);
                }
            }
        });

        String[] values = {"Hey there! I am using " + getResources().getString(R.string.app_name), "Available", "Busy", "At school", "At the movies",
                "At work", "Battery about to die"};

        for (int i = 0; i < values.length; i++) {
            // Only add to list if not status added in local db
            if (!list.contains(values[i])) {
                list.add(values[i]);
            }
            if (SessionManager.getInstance(Status.this).getcurrentUserstatus().equals(list.get(i)))
                selectedPosition = i;
        }


        lv1.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        adapter = new ArrayAdapter<String>
                (this, R.layout.simple_list_item_emoji, list) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                // Get the Item from ListView
                View view = super.getView(position, convertView, parent);

                // Initialize a TextView for ListView each Item
                TextView tv = view.findViewById(android.R.id.text1);
                tv.setTypeface(typeface);
                tv.setTextSize(13);
                tv.setTextScaleX(1);

                if (selectedPosition != position) {
                    // Set the text color of TextView (ListView Item)
                    tv.setTextColor(Color.BLACK);
                } else {
                    tv.setTextColor(ContextCompat.getColor(Status.this, R.color.colorPrimary));
                }

                // Generate ListView Item using TextView
                return view;
            }
        };
        lv1.setAdapter(adapter);

        //v = lv1.getChildAt(selectedPosition);
        // v.setBackgroundColor(Color.parseColor("#e3e3e3"));
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                View v = getViewByPosition(position, lv1);
                TextView tv = view.findViewById(android.R.id.text1);
//                session.putposition(position);
                String val = (String) parent.getItemAtPosition(position);
                textViewStatus.setText(val);
                lv1.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                lv1.setItemChecked(position, true);
                //System.out.println("position===================>status" + position);
                //v.setBackgroundColor(Color.parseColor("#e3e3e3"));

                for (int i = 0; i < list.size(); i++) {
                    View v1 = getViewByPosition(i, lv1);
                    if (i != position) {
                        tv.setTextColor(Color.BLACK);
                    } else {
                        tv.setTextColor(ContextCompat.getColor(Status.this, R.color.colorPrimary));
                    }
                }
                //    final View renderer = super.getView(position, convertView, parent);
                /* Upload the status to the server and update the database */
                updateUserData(val);
            }
        });


    }

    public View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    @Override
    public void onClick(View v) {
        // Intent intent = null;
        switch (v.getId()) {
            case R.id.editStatus:
            case R.id.statusInfo_rl:
                String status = textViewStatus.getText().toString();
                Intent intent = new Intent(Status.this, StatusNewScreen.class);
                intent.putExtra("STATUS", status);
                startActivityForResult(intent, NEW_STATUS_REQUEST_CODE);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed here it is 2
        if (resultCode == RESULT_OK && requestCode == NEW_STATUS_REQUEST_CODE) {
            String message = data.getStringExtra("MESSAGE");

            textViewStatus.setText(message);
            /* Update the status on the server here */
            updateUserData(message);

            addNewStatus(message);
        }

    }

    private Boolean internetCheck() {
        ConnectivityManager cm =
                (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;

    }

    private void addNewStatus(String status) {
        if (!list.contains(status)) {
            list.add(0, status);
            selectedPosition = 0;
        } else {
            int index = list.indexOf(status);
            selectedPosition = index;
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getMenuInflater().inflate(R.menu.status_overflow, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
    /*    if (id == R.id.delete_all) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                    Status.this);
            alertDialog
                    .setMessage("Are you sure you want to delete all your statuses?");
            alertDialog.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            list.clear();
                            lv1.setAdapter(adapter);
                            statusDB.deleteAllStatus();

                        }
                    });

            alertDialog.show();
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }


    /* This method will post the current user data to the server */
    private void updateUserData(String status) {
        if (ConnectivityInfo.isInternetConnected(this)) {
            if (!progressDialog.isShowing()) {
                progressDialog.show();
                setEventTimeout();
            }

            try {
                JSONObject statusObj = new JSONObject();
                statusObj.put("from", SessionManager.getInstance(Status.this).getCurrentUserID());
                statusObj.put("status", status);

                SendMessageEvent statusEvent = new SendMessageEvent();
                statusEvent.setMessageObject(statusObj);
                statusEvent.setEventName(SocketManager.EVENT_CHANGE_PROFILE_STATUS);
                EventBus.getDefault().post(statusEvent);
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void setEventTimeout() {
        if (eventHandler == null) {
            eventHandler = new Handler();
            eventRunnable = new Runnable() {
                @Override
                public void run() {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        Toast.makeText(Status.this, "Try again", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }
            };
        }

        eventHandler.postDelayed(eventRunnable, SocketManager.RESPONSE_TIMEOUT);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {

        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CHANGE_PROFILE_STATUS)) {
            Object[] array = event.getObjectsArray();
            try {
                JSONObject object = new JSONObject(array[0].toString());

                String err = object.getString("err");
                if (err.equalsIgnoreCase("0")) {

                    String from = object.getString("from");
                    if (from.equalsIgnoreCase(SessionManager.getInstance(Status.this).getCurrentUserID())) {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                        String message = object.getString("message");
                        message = message.replace("Sucess", "Success");
                        if (!isToasted) {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            isToasted = true;
                        }
                        String status = object.getString("status");

                        byte[] decodeStatus = Base64.decode(status, Base64.DEFAULT);
                        status = new String(decodeStatus, StandardCharsets.UTF_8);
                        textViewStatus.setText(status);
                        addNewStatus(status);
                    }
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(Status.this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(Status.this);
    }
}
