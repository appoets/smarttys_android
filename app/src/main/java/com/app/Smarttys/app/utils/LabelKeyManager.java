package com.app.Smarttys.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by CAS60 on 10/16/2017.
 */
public class LabelKeyManager {

    private final String LABEL_KEY_PREF = "LabelKeyPref";
    private SharedPreferences labelPref;
    private SharedPreferences.Editor labelEditor;

    public LabelKeyManager(Context context) {
        labelPref = context.getSharedPreferences(LABEL_KEY_PREF, Context.MODE_PRIVATE);
        labelEditor = labelPref.edit();
    }

    public static LabelKeyManager getInstance(Context context) {
        return new LabelKeyManager(context);
    }
}
