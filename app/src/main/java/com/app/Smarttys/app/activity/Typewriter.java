package com.app.Smarttys.app.activity;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;

import com.app.Smarttys.app.utils.MyLog;

public class Typewriter extends androidx.appcompat.widget.AppCompatTextView {

    private CharSequence mTextToHide, mTextToDisplay;
    private int mIndex;
    private long mInitialDelay = 2000; //Default 500ms delay
    private long mDelay = 500; //Default 500ms delay
    private Handler mHandler = new Handler();
    private Runnable characterAdder = new Runnable() {
        @Override
        public void run() {
            //            setText(mText.subSequence(0, mIndex++));
            //            if (mIndex <= mText.length()) {
            //                mHandler.postDelayed(characterAdder, mDelay);
            //            }

            if (mTextToHide != null
                    && mTextToDisplay != null && mIndex < mTextToHide.length()) {
                MyLog.d("Typewriter", "mIndex = " + mIndex);
                setText(mTextToDisplay.subSequence(mIndex++, mTextToDisplay.length()));
                mHandler.postDelayed(characterAdder, mDelay);
            }
        }
    };

    public Typewriter(Context context) {
        super(context);
    }

    public Typewriter(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void animateText(CharSequence textToHide, String textToDisplay) {
        mTextToHide = textToHide;
        mTextToDisplay = textToDisplay;
        mIndex = 0;

        //        setText("");
        setText(textToDisplay);
        mHandler.removeCallbacks(characterAdder);
        mHandler.postDelayed(characterAdder, mInitialDelay);
    }

    public void setCharacterDelay(long millis) {
        mDelay = millis;
    }

    public void setInitialDelay(long millis) {
        mInitialDelay = millis;
    }
}