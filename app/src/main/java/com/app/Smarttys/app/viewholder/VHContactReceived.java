package com.app.Smarttys.app.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.widget.CircleImageView;


/**
 * Created by Casperon Technologyon 02/04/16.
 */
public class VHContactReceived extends RecyclerView.ViewHolder {
    public TextView senderName, time, contactName, contactNumber, invite, add, invite1, add1, message1, tvDateLbl, tvSecretLbl;
    public ImageView starredindicator_below, imageViewindicatior;
    public RelativeLayout selection_layout;
    public View v1;
    public LinearLayout contact_add_invite;
    public CircleImageView contactimage;

    /**
     * one more field to show contact data to be added
     */
    public VHContactReceived(View view) {
        super(view);


        imageViewindicatior = view.findViewById(R.id.imageView);
        senderName = view.findViewById(R.id.lblMsgFrom);
        starredindicator_below = view.findViewById(R.id.starredindicator_below);
        contactimage = view.findViewById(R.id.contactImage);

        time = view.findViewById(R.id.ts);
        tvDateLbl = view.findViewById(R.id.tvDateLbl);
        tvSecretLbl = view.findViewById(R.id.tvSecretLbl);

        contactName = view.findViewById(R.id.contactName);

        contactNumber = view.findViewById(R.id.contactNumber);

        invite = view.findViewById(R.id.invite);
        v1 = view.findViewById(R.id.v1);
        add = view.findViewById(R.id.add);
        invite1 = view.findViewById(R.id.invite_1);
        message1 = view.findViewById(R.id.message_1);
        add1 = view.findViewById(R.id.add_1);
        contact_add_invite = view.findViewById(R.id.contact_add_invite);
        selection_layout = view.findViewById(R.id.selection_layout);

    }
}
