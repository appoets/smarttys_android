package com.app.Smarttys.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.LoginAuthUtils;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.service.Constants;
import com.multidots.fingerprintauth.FingerPrintAuthCallback;
import com.multidots.fingerprintauth.FingerPrintAuthHelper;

public class LoginAuthenticationActivity extends Activity implements FingerPrintAuthCallback {

    private static final String TAG = "LoginAuthenticationActi";
    SessionManager sessionmanager;
    private FingerPrintAuthHelper fingerPrintAuthHelper;
    private int INTENT_AUTHENTICATE = 10;
    private boolean isDeviceHaveAuth = false;
    private Button btnPinUnlock;
    private View tvOr;
    private ImageView ivFingerPrint;
    private TextView tvFingerPrintDesc;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyLog.d(TAG, "onCreate: ");

        sessionmanager = SessionManager.getInstance(LoginAuthenticationActivity.this);
        session = new Session(LoginAuthenticationActivity.this);

        Constants.IS_FROM_PASSWORD_PAGE = true;
        SessionManager.getInstance(this).setIsDeviceLocked(false);
        setContentView(R.layout.actiivity_login_auth);
        btnPinUnlock = findViewById(R.id.btn_unlock_with_pin);
        tvOr = findViewById(R.id.tv_or);
        ivFingerPrint = findViewById(R.id.iv_finger_print);
        tvFingerPrintDesc = findViewById(R.id.tv_finger_print_unlock);

        if (sessionmanager.gethomepageadintegrationposition().equalsIgnoreCase("")) {

            sessionmanager.setHomePageAdintegrationposition("bottom");

        } else if (sessionmanager.gethomepageadintegrationposition().equalsIgnoreCase("bottom")) {

            sessionmanager.setHomePageAdintegrationposition("top");

        } else if (sessionmanager.gethomepageadintegrationposition().equalsIgnoreCase("top")) {

            sessionmanager.setHomePageAdintegrationposition("list");

        } else if (sessionmanager.gethomepageadintegrationposition().equalsIgnoreCase("list")) {

            sessionmanager.setHomePageAdintegrationposition("bottom");
        }

        if (SessionManager.getInstance(this).isDeviceLockEnabled()) {
            init();
        } else {
            startNextPage();
        }
    }

    private void hideFingerPrintViews() {
        ivFingerPrint.setVisibility(View.GONE);
        tvFingerPrintDesc.setVisibility(View.GONE);
    }

    private void init() {
        fingerPrintAuthHelper = FingerPrintAuthHelper.getHelper(this, this);
        if (LoginAuthUtils.isDeviceHasLock(this)) {
            if (!isDeviceHaveAuth) {
                isDeviceHaveAuth = true;
            } else {
                tvOr.setVisibility(View.VISIBLE);
            }
            btnPinUnlock.setVisibility(View.VISIBLE);

            btnPinUnlock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LoginAuthUtils.startDefaultPasswordPage(LoginAuthenticationActivity.this, INTENT_AUTHENTICATE);
                }
            });
        }
        if (!isDeviceHaveAuth) {
            startNextPage();
        } else {
            if (!SessionManager.getInstance(this).getlogin()) {
                startNextPage();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (fingerPrintAuthHelper != null)
            fingerPrintAuthHelper.stopAuth();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (fingerPrintAuthHelper != null) {
            fingerPrintAuthHelper.startAuth();
        }
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == INTENT_AUTHENTICATE) {
            if (resultCode == RESULT_OK) {
                //do something you want when pass the security
                startNextPage();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onNoFingerPrintHardwareFound() {
        hideFingerPrintViews();
    }

    @Override
    public void onNoFingerPrintRegistered() {
        hideFingerPrintViews();
    }

    @Override
    public void onBelowMarshmallow() {
        hideFingerPrintViews();
    }

    @Override
    public void onAuthSuccess(FingerprintManager.CryptoObject cryptoObject) {
        Toast.makeText(this, getString(R.string.Verification_Successful), Toast.LENGTH_SHORT).show();
        startNextPage();
    }

    @Override
    public void onAuthFailed(int errorCode, String errorMessage) {
        //  Toast.makeText(this, "" + errorMessage, Toast.LENGTH_SHORT).show();
    }

    private void startNextPage() {


        if (session.getContactPermission() && SessionManager.getInstance(LoginAuthenticationActivity.this).getlogin()) {

            MyLog.d(TAG, "performance startNextPage: ");
            Intent intent = new Intent(this, InitialLoaderActivitySmartty.class);
            startActivity(intent);
            finish();

        } else {

            MyLog.d(TAG, "performance startNextPage: ");
            Intent intent = new Intent(this, ContactPermissionActivity.class);
            startActivity(intent);
            finish();
        }


    }
}
