package com.app.Smarttys.app.utils;

import android.content.Context;

import org.json.JSONObject;


/**
 * Created by user134 on 6/19/2018.
 */

public class EncryptionTesting {
    private static final String TAG = "EncryptionUtils";

    public static String simpleMsgEncrypt(String msg, String token, Context context) {
        String originalMsg = msg;
        try {
            String iv = "chat";
            String encodedToken = token;
            encodedToken = encodedToken.replace("\n", "");
            //userToken=convertHexToBase64String(userToken);
            MyLog.d(TAG, "getScMessageResponseDecrypted: " + encodedToken);
            CryptLib cryptLib = new CryptLib();
            return cryptLib.encryptSimple(msg, encodedToken, iv);


        } catch (Exception e) {
            MyLog.e(TAG, "getScMessageResponseDecrypted: ", e);
        }
        return originalMsg;
    }

    public static String simpleJsonEncrypt(JSONObject msg, String token, Context context) {
        String originalMsg = msg.toString();
        try {
            String iv = "chat";
            String encodedToken = token;
            encodedToken = encodedToken.replace("\n", "");
            //userToken=convertHexToBase64String(userToken);
            MyLog.d(TAG, "getScMessageResponseDecrypted: " + encodedToken);
            CryptLib cryptLib = new CryptLib();
            return cryptLib.encryptSimple(msg.toString(), encodedToken, iv);

        } catch (Exception e) {
            MyLog.e(TAG, "getScMessageResponseDecrypted: ", e);
        }
        return originalMsg;
    }

    public static String simpleMsgDecrypt(String msg, String token, Context context) {
        String originalMsg = msg;
        try {
            String iv = "chat";
            String encodedToken = token;
            encodedToken = encodedToken.replace("\n", "");
            MyLog.d(TAG, "getScMessageResponseDecrypted: " + encodedToken);
            CryptLib cryptLib = new CryptLib();
            return cryptLib.decryptCipherText(msg, encodedToken, iv);
        } catch (Exception e) {
            MyLog.e(TAG, "getScMessageResponseDecrypted: ", e);
        }
        return originalMsg;
    }


    public static String simpleJsonDecrypt(JSONObject msg, String token, Context context) {
        String originalMsg = msg.toString();
        try {
            String iv = "chat";
            String encodedToken = token;
            encodedToken = encodedToken.replace("\n", "");
            MyLog.d(TAG, "getScMessageResponseDecrypted: " + encodedToken);
            CryptLib cryptLib = new CryptLib();
            return cryptLib.decryptCipherText(msg.toString(), encodedToken, iv);
        } catch (Exception e) {
            MyLog.e(TAG, "getScMessageResponseDecrypted: ", e);
        }
        return originalMsg;
    }


}
