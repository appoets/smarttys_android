package com.app.Smarttys.app.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.Smarttys.R;
import com.app.Smarttys.app.dialog.CustomAlertDialog;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.LocationUtil;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.SharedPreference;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.app.widget.AvnNextLTProDemiEditText;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.ActivityLauncher;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.ShortcutBadgeManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.model.SCLoginModel;
import com.app.Smarttys.core.service.Constants;
import com.app.Smarttys.core.service.ServiceRequest;
import com.app.Smarttys.core.smarttyhelperclass.SmarttyDialogUtils;
import com.app.Smarttys.core.smarttyhelperclass.SmarttyPermissionValidator;
import com.app.Smarttys.fcm.MyFirebaseMessagingService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by  CASPERON TECH on 10/5/2016.
 */
public class AddInvitationScreen extends CoreActivity implements View.OnClickListener {
    private static final String LETTER_SPACING = " ";
    private static final String TAG = AddInvitationScreen.class.getSimpleName();
    public static boolean SKIP_OTP_VERIFICATION = true;
    static String[] country;
    static String[] codes;
    private final int selectedCountry = 11;
    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 123;
    public AvnNextLTProDemiEditText phoneNumber;
    String code = "", otp = "";
    AvnNextLTProDemiTextView selectCountry, choseCountry;
    Boolean isagree = false;
    private Context mContext;
    private SessionManager sessionManager;
    private AvnNextLTProRegTextView supMessenger, conformCountyCode;
    private ImageView ivTermsAndConditions;
    // private ImageView okButton;
    private Button okButton;
    private EditText edReferalCode, edEmail;
    // private boolean termsAndConditionsAccepted = true;
    private com.app.Smarttys.core.model.SCLoginModel SCLoginModel;
    ServiceRequest.ServiceListener verifyCodeListener = new ServiceRequest.ServiceListener() {
        @Override
        public void onCompleteListener(String response) {
            //    Log.d("Loginrequest", response);
            hidepDialog();

            if (SCLoginModel == null)
                SCLoginModel = new SCLoginModel();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            SCLoginModel = gson.fromJson(response, SCLoginModel.class);
            //  Log.d(TAG, "onCompleteListener: loginCount: " + SCLoginModel.getLoginCount());
            // success
            if (SCLoginModel.getErrNum().equals("0")) {
                // if (Constants.IS_ENCRYPTION_ENABLED) {
                setToken(SCLoginModel);
                //}
                /* Move to the profile info screen */
//                Toast.makeText(VerifyPhoneScreen.this, "Mobile number verified successfully", Toast.LENGTH_SHORT).show();
                SessionManager.getInstance(AddInvitationScreen.this).IsnumberVerified(true);
                SessionManager.getInstance(AddInvitationScreen.this).setLoginCount(SCLoginModel.getLoginCount());
                //Log.d("CODE", loginModel.getCode());
                SharedPreferences shPref = getSharedPreferences("global_settings",
                        MODE_PRIVATE);
                SharedPreferences.Editor et = shPref.edit();
                //Save my token
                SharedPreference.getInstance().save(AddInvitationScreen.this, "securitytoken", SCLoginModel.getToken());
                et.putString("userId", SessionManager.getInstance(AddInvitationScreen.this).getPhoneNumberOfCurrentUser());
                et.apply();
                Session session = new Session(AddInvitationScreen.this);
                session.putgalleryPrefs("def");
                ActivityLauncher.launchProfileInfoScreen(AddInvitationScreen.this, SessionManager.getInstance(AddInvitationScreen.this).getPhoneNumberOfCurrentUser());

            } else {
                Toast.makeText(AddInvitationScreen.this, "Code mismatch. Please re-enter the code", Toast.LENGTH_LONG).show();
                SharedPreferences shPref = getSharedPreferences("global_settings",
                        MODE_PRIVATE);
                SharedPreferences.Editor et = shPref.edit();
                et.putString("userId", SessionManager.getInstance(AddInvitationScreen.this).getPhoneNumberOfCurrentUser());
                et.apply();
            }
        }

        @Override
        public void onErrorListener(int state) {
            hidepDialog();
            hideProgressDialog();
        }
    };
    //   private GPSTracker gpsTracker;
    private List<String> codeList;
    private List<String> countryList;
    private ArrayList<SmarttyPermissionValidator.Constants> myPermissionConstantsArrayList;
    private boolean isDeninedRTPs = false;
    private boolean showRationaleRTPs = false;
    //   private SocketManager mSocketManager;
    private boolean country_wise_filter = false;
    private String GCM_Id = "";
    private ArrayList<String> list;
    private ArrayList<String> codelist;
    private ImageView scroll_country;
    private RelativeLayout code_fetch_layout;
    private boolean isMassChat, isTwitgo;
    private ServiceRequest.ServiceListener resendInviteCodeListner = new ServiceRequest.ServiceListener() {

        @Override
        public void onCompleteListener(String response) {
            hidepDialog();
            try {
                JSONObject result = new JSONObject(response);
                if (result.has("message"))
                    Toast.makeText(AddInvitationScreen.this, result.getString("message"), Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(AddInvitationScreen.this, "Invalid phone number!", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e(TAG, "onCompleteListener: ", e);
            }
        }

        @Override
        public void onErrorListener(int state) {
            hidepDialog();
        }
    };
    private GPSTracker gpsTracker;
    private ServiceRequest.ServiceListener verifcationListener = new ServiceRequest.ServiceListener() {
        @Override
        public void onCompleteListener(String response) {
            hidepDialog();
            okButton.setEnabled(true);

            if (SCLoginModel == null)
                SCLoginModel = new SCLoginModel();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            SCLoginModel = gson.fromJson(response, SCLoginModel.class);
            Log.e("verifcationListener", "verifcationListener" + response);
            otp = SCLoginModel.getCode();
            if (SCLoginModel.getProfilePic() != null && !SCLoginModel.getProfilePic().isEmpty()) {
                // String imgPath = SCLoginModel.getProfilePic() + "?id=" + AppUtils.eodMillis();
                String imgPath = SCLoginModel.getProfilePic();
                Log.e("Start", "adssds " + imgPath);

                SessionManager.getInstance(AddInvitationScreen.this).setUserProfilePic(AppUtils.getValidProfilePath(imgPath));
            }
//Save my token
            SharedPreference.getInstance().save(AddInvitationScreen.this, "securitytoken", SCLoginModel.getToken());
            if (SCLoginModel.getStatus() != null && !SCLoginModel.getStatus().equalsIgnoreCase("")) {
                //        SessionManager.getInstance(VerifyPhoneScreen.this).setcurrentUserstatus((SCLoginModel.getStatus()));
                try {
                    String status = new String(Base64.decode(SCLoginModel.getStatus(), Base64.DEFAULT));
                    SessionManager.getInstance(AddInvitationScreen.this).setcurrentUserstatus(status);
                } catch (Exception e) {
                    SessionManager.getInstance(AddInvitationScreen.this).setcurrentUserstatus(SCLoginModel.getStatus());
                }


            } else {
                SessionManager.getInstance(AddInvitationScreen.this).setcurrentUserstatus("Hey there! I am using "
                        + getResources().getString(R.string.app_name));
            }
            if (SCLoginModel.get_id() != null) {
                SessionManager.getInstance(AddInvitationScreen.this).setCurrentUserID(SCLoginModel.get_id());
                SharedPreference.getInstance().save(mContext, "userid", SCLoginModel.get_id());
                clearDataIfUserChanged(SCLoginModel.get_id());
            }

            // if (Constants.IS_ENCRYPTION_ENABLED) {
            //setToken(SCLoginModel);
            // }
            // success
            if (SCLoginModel.getErrNum().equals("0")) {
                if (SKIP_OTP_VERIFICATION)
                    SessionManager.getInstance(AddInvitationScreen.this).Islogedin(true);
                String message = "+" + code + phoneNumber.getText().toString();
                SessionManager.getInstance(AddInvitationScreen.this).setPhoneNumberOfCurrentUser(message);
                if (SCLoginModel.getName() != null && !SCLoginModel.getName().isEmpty()) {
                    SessionManager.getInstance(AddInvitationScreen.this).setnameOfCurrentUser(((SCLoginModel.getName())));
                }
                SessionManager.getInstance(AddInvitationScreen.this).setUserCountryCode("+" + code);
                SessionManager.getInstance(AddInvitationScreen.this).setUserMobileNoWithoutCountryCode(phoneNumber.getText().toString());

                try {
                    JSONObject object = new JSONObject(response);
                    if (SessionManager.getInstance(AddInvitationScreen.this).getTwilioMode().equalsIgnoreCase(
                            SessionManager.TWILIO_DEV_MODE)) {
                        SessionManager.getInstance(AddInvitationScreen.this).setLoginOTP(object.getString("code"));
                    }
                    SessionManager.getInstance(AddInvitationScreen.this).setLoginEmailOTP(object.getString("email_otp"));

                } catch (JSONException e) {
                    Log.e(TAG, "", e);
                }
                if (SKIP_OTP_VERIFICATION) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            skipOTP(otp);

                        }
                    }, 500);
                } else {
                    hideProgressDialog();
                    ActivityLauncher.launchSMSVerificationScreen(AddInvitationScreen.this, message, "" + code, phoneNumber.getText().toString(), otp, GCM_Id);
                    Toast.makeText(AddInvitationScreen.this, SCLoginModel.getMessage(), Toast.LENGTH_SHORT).show();

                }
            } else if (SCLoginModel.getErrNum().equals("1")) {
                String error = SCLoginModel.getMessage();
                Toast.makeText(AddInvitationScreen.this, error, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onErrorListener(int state) {
            hideProgressDialog();
            SmarttyDialogUtils.showCheckInternetDialog(AddInvitationScreen.this);
            hidepDialog();
            okButton.setEnabled(true);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
        isTwitgo = getResources().getBoolean(R.bool.is_twitgo);
        if (isMassChat)
            setContentView(R.layout.activity_invitation_mass_chat);
        else if (isTwitgo)
            setContentView(R.layout.activity_verify_phone_screen_twitgo);
        else
            setContentView(R.layout.activity_verify_phone_screen);
        //  Log.d(TAG, "onCreate: ");

        mContext = AddInvitationScreen.this;
//        setTitle(R.string.verify_phone_number_txt);
        initView();
        initData();
        setCountryCode();
        if (CoreController.isRaad) {
            fetchLocation();
        }
        phoneNumber.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == EditorInfo.IME_ACTION_DONE) {
                    findViewById(R.id.okButton).performClick();
                    return true;
                }
                return false;
            }
        });
        phoneNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    findViewById(R.id.okButton).performClick();
                    return true;
                }
                return false;
            }
        });
        phoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String s = editable.toString();
                if (country_wise_filter) {
                    if (s.equalsIgnoreCase("0") || s.equalsIgnoreCase("00") || s.equalsIgnoreCase("000")) {
                        phoneNumber.setText("");
                        phoneNumber.setError("Enter valid Mobile Number");
                    }
                }
            }
        });


        scroll_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityLauncher.launchChooseCountryScreen(AddInvitationScreen.this, selectedCountry);
            }
        });


        code_fetch_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ActivityLauncher.launchChooseCountryScreen(AddInvitationScreen.this, selectedCountry);
            }
        });

    }

    private void fetchLocation() {
        try {
            Log.d(TAG, "fetchLocation1: " + LocationUtil.getLastKnownLocation(true, this).getLatitude());
        } catch (Exception e) {
            Log.e(TAG, "fetchLocation: ", e);
        }
    }

    private void initData() {
        Resources res = getResources();
        String[] code = res.getStringArray(R.array.country_code);
        String[] country = res.getStringArray(R.array.country_list);
        codeList = Arrays.asList(code);
        countryList = Arrays.asList(country);
        //    Log.e("" + code.length, "" + country.length);

        ShortcutBadgeManager manager = new ShortcutBadgeManager(this);
        manager.clearBadgeCount();
        manager.setContactLastRefreshTime(0L);
        SessionManager.getInstance(AddInvitationScreen.this).setnameOfCurrentUser("");
        checkAndRequestPermissions();
        //   initSocketCallback();
        //  mSocketManager.connect();
        //  getSettings();
        //  getSettingsAPI();
        //Check callback instance

    }

    private void initView() {
        choseCountry = findViewById(R.id.selectCountry);
        selectCountry = findViewById(R.id.selectCountry);
        okButton = findViewById(R.id.okButton);
        sessionManager = SessionManager.getInstance(AddInvitationScreen.this);
        phoneNumber = findViewById(R.id.phoneNumber);
        if (isMassChat)
            edReferalCode = findViewById(R.id.ed_invitation_code);
        if (isTwitgo)
            edEmail = findViewById(R.id.etEmailId);
        scroll_country = findViewById(R.id.scroll_country);
        code_fetch_layout = findViewById(R.id.code_fetch_layout);
        okButton.setEnabled(true);

        Context c = getApplicationContext();
        Resources res = c.getResources();

        country = res.getStringArray(R.array.country_list);
        codes = res.getStringArray(R.array.country_code);

        list = new ArrayList<String>();
        codelist = new ArrayList<String>();
        list.clear();
        codelist.clear();
        for (int i = 0; i < country.length; i++) {
            list.add(country[i]);
            codelist.add(codes[i]);
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                final Dialog dialog = new Dialog(AddInvitationScreen.this);
                final AvnNextLTProDemiTextView agree, disagree;
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_termsand_condition);
                agree = dialog.findViewById(R.id.agreed);
                disagree = dialog.findViewById(R.id.disagree);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);

                /*tvTermsAndConditions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.show();
                        agree.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                isagree = true;
                                tvTermsAndConditions_checkbox.setChecked(isagree);
                                dialog.dismiss();
                            }
                        });
                        disagree.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                isagree = false;
                                tvTermsAndConditions_checkbox.setChecked(isagree);

                            }
                        });

                    }
                });*/
            }
        }, 100);
       /* tvTermsAndConditions_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    isagree = true;
                    tvTermsAndConditions_checkbox.setChecked(isChecked);
                } else {
                    isagree = false;
                    tvTermsAndConditions_checkbox.setChecked(isChecked);
                }
            }
        });*/
        choseCountry.setOnClickListener(this);
        okButton.setOnClickListener(this);


//        Intent i = getIntent();
        //  String text = i.getStringExtra("TextBox");
        //  phoneNumber.setText(text);

    }

    @Override
    public void onStop() {
        super.onStop();
        //mSocketManager.clearCallBack();
    }

    private void checkAndRequestPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            myPermissionConstantsArrayList = new ArrayList<>();




            /*if (SmarttyPermissionValidator.checkPermission(VerifyPhoneScreen.this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE)) {
                onPermissionGranted();
            }*/
        } else {
            onPermissionGranted();
        }
    }

    private void onPermissionGranted() {


    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_MULTIPLE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++) {
                        String permission = permissions[i];
                        if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            isDeninedRTPs = true;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                showRationaleRTPs = shouldShowRequestPermissionRationale(permission);
                            }
                        }
                        break;
                    }
                    onPermissionResult();
                } else {

                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
     /*   if(!AppUtils.isServiceRunning(this,MessageService.class)){
            AppUtils.startService(this, MessageService.class);
        }*/
    }

    private void onPermissionResult() {
        if (isDeninedRTPs) {
            if (!showRationaleRTPs) {
                //goToSettings();
                SmarttyDialogUtils.showPermissionDeniedDialog(AddInvitationScreen.this);
            } else {
                isDeninedRTPs = false;
                SmarttyPermissionValidator.checkPermission(this,
                        myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE);
            }
        } else {
            onPermissionGranted();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.selectCountry || view.getId() == R.id.countryDropDownMain) {
            ActivityLauncher.launchChooseCountryScreen(this, selectedCountry);
        }
        if (view.getId() == R.id.okButton) {
            if (AppUtils.isNetworkAvailable(mContext)) {

                //  getSettingsAPI();
                //   if (mSocketManager.isConnected()) {
                hideKeyboard();
                okButton.setEnabled(false);


                //  String code = choseCountry.getText().toString();
                if (!code.equalsIgnoreCase("Code") && !code.equalsIgnoreCase("")) {

                    String cCode = code;
                    String uPhone = phoneNumber.getText().toString();

                    String mMisidin = "" + cCode + uPhone;
                    String CountryCode = "" + cCode;
                    String Email = "" + edReferalCode.getText().toString();
                    showProgressDialog();
                    getRequestInviteAPI(mMisidin, uPhone, CountryCode, Email);
                } else {
                    Toast.makeText(AddInvitationScreen.this, R.string.selectcontrycode, Toast.LENGTH_SHORT).show();
                    okButton.setEnabled(true);
                }

            } else {
                Toast.makeText(AddInvitationScreen.this, R.string.networkerror, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed here it is 2
        if (resultCode != RESULT_CANCELED && requestCode == selectedCountry) {
            String message = data.getStringExtra("MESSAGE");
            code = data.getStringExtra("CODE");
            if (code.equalsIgnoreCase("263")) {
                country_wise_filter = true;
                choseCountry.setText("  (+" + code + ")");
            } else {
                country_wise_filter = false;
                choseCountry.setText("  (+" + code + ")");
            }
            sessionManager.setCountryCodeOfCurrentUser("+" + code);
        }
    }

    /* This API checks if the user has entered a number of not.
     * In case the number is not entered, an alert is displayed to the user
     * else send this number to the server and execute the login API
     */
    private void showAlertDialog(String title, String msg) {
        String number = phoneNumber.getText().toString().trim();
        number = number.replace(" ", "");
        if (number.length() < 5) {
            CustomAlertDialog dialog = new CustomAlertDialog();
            dialog.setMessage("Enter valid number");
            dialog.setPositiveButtonText("OK");
            dialog.setNegativeButtonText("CANCEL");
            dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
                @Override
                public void onPositiveButtonClick() {

                }

                @Override
                public void onNegativeButtonClick() {

                }
            });
            dialog.show(getSupportFragmentManager(), "CustomAlert");
        } else {
            /*if (!termsAndConditionsAccepted) {
                CustomAlertDialog dialog = new CustomAlertDialog();
                dialog.setMessage("Please Accept Terms and Conditions");
                dialog.setPositiveButtonText("OK");
                dialog.setNegativeButtonText("CANCEL");
                dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
                    @Override
                    public void onPositiveButtonClick() {

                    }

                    @Override
                    public void onNegativeButtonClick() {

                    }
                });
                dialog.show(getSupportFragmentManager(), "CustomAlert");
            } else {*/

            CustomAlertDialog dialog = new CustomAlertDialog();
            dialog.setPositiveButtonText("OK");
            dialog.setNegativeButtonText("EDIT");

            // String msg2 = "<b>We will be verifying the phone number:\n\n" + "+" + code + " " + phoneNumber.getText().toString() + "\n\nIs this OK, or would you like to edit the number?<b>";
            String msg2 = "We will be verifying the phone number:<br><br>" + "<b>+" + code + " " + phoneNumber.getText().toString() + "</b><br><br>Is this OK, or would you like to edit the number?";
            dialog.setMessage(msg2);

            dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
                @Override
                public void onPositiveButtonClick() {

                    makeVerificationRequest();
                }

                @Override
                public void onNegativeButtonClick() {

                }
            });
            dialog.show(getSupportFragmentManager(), "CustomAlert");
        }
        // }
    }

/*
    private void getRequestInviteAPI(final String mMisidin, final String uPhone, final String CountryCode, final String Email) {
        // prepare the Request
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, Constants.REQUESTINVITE, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject object) {
                        // display response
                        Log.e("Response", object.toString());
                      *//*  try {
                            String twilioMode = "";
                            if (object.has("twilio"))
                                twilioMode = object.optString("twilio");
                            if (!twilioMode.equals(SessionManager.TWILIO_DEV_MODE)) {
                                //Twilio Sms
                                SKIP_OTP_VERIFICATION = false;
                            } else if (object.has("jawal")) {
                                //Raad Sms
                                twilioMode = object.optString("jawal");
                                if (!twilioMode.equals(SessionManager.TWILIO_DEV_MODE))
                                    SKIP_OTP_VERIFICATION = false;
                            }
                            SessionManager sessionManager = SessionManager.getInstance(AddInvitationScreen.this);
                            sessionManager.setTwilioMode(twilioMode);
                            // mSocketManager.disconnect();
                            //Check Encryption is available or not
                            if (object.has("is_encryption_available")) {
                                String is_encryption_available = object.getString("is_encryption_available");
                                if (Integer.parseInt(is_encryption_available) == 1) {

                                    SessionManager.getInstance(mContext).setIsEncryptionEnabled(true);
                                } else if (Integer.parseInt(is_encryption_available) == 0) {

                                    SessionManager.getInstance(mContext).setIsEncryptionEnabled(false);
                                }
                            } else {
                                SessionManager.getInstance(mContext).setIsEncryptionEnabled(false);
                            }
                            String contactUsEmailId = object.getString("contactus_email_address");
                            String chatLock = object.getString("chat_lock");
                            String secretChat = object.getString("secret_chat");
                            sessionManager.setContactUsEMailId(contactUsEmailId);
                            sessionManager.setLockChatEnabled(chatLock);
                            sessionManager.setSecretChatEnabled(secretChat);
                     //     Log.e("SKIP_OTP_VERIFICATION", "SKIP_OTP_VERIFICATION" + SKIP_OTP_VERIFICATION);
                        } catch (Exception e) {
                            Log.e("Exception", "Exception" + e);
                        }*//*
                    }
                },
               *//* new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //     Log.d("Error.Response", error);
                    }
                }*//*
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("onErrorResponse", error.toString());

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("msisdn", mMisidin);
                params.put("PhNumber", uPhone);
                params.put("CountryCode", CountryCode);
                params.put("Email", String.valueOf(Email));
                Log.e("params","params"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
       *//* );

// add it to the RequestQueue
        queue.add(getRequest);*//*
        };
        queue.add(getRequest);
    }

  */

    private void makeVerificationRequest() {
        String number = phoneNumber.getText().toString().trim();
        number = number.replace(" ", "");
        if (isMassChat && edReferalCode.getText().toString().trim().isEmpty()) {
            Toast.makeText(AddInvitationScreen.this, "Enter invitation code", Toast.LENGTH_SHORT).show();
            okButton.setEnabled(true);
            return;
        } else if (isTwitgo && edEmail.getText().toString().trim().isEmpty()) {
            Toast.makeText(AddInvitationScreen.this, "Enter email address", Toast.LENGTH_SHORT).show();
            okButton.setEnabled(true);
            return;
        } else if (number.equals("") || number.length() < 4) {
            Toast.makeText(AddInvitationScreen.this, R.string.validno, Toast.LENGTH_SHORT).show();
            okButton.setEnabled(true);
            return;
        }
        if (MyFirebaseMessagingService.token != null && MyFirebaseMessagingService.token.length() > 0)
            GCM_Id = MyFirebaseMessagingService.token;
        else
            GCM_Id = MyFirebaseMessagingService.getFCMToken(this);

        if (GCM_Id == null || GCM_Id.isEmpty()) {

            // Toast.makeText(getApplicationContext(),"GCM Empty",Toast.LENGTH_LONG).show();

            GCM_Id = "121";
        }


        //   showProgressDialog();
        HashMap<String, String> params = new HashMap<String, String>();
        String cCode = code;
        String uPhone = phoneNumber.getText().toString();
        //System.out.println("Country_code" + "" + cCode);
        params.put("msisdn", "+" + cCode + uPhone);
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        params.put("DeviceId", android_id);
        params.put("gcm_id", GCM_Id);
        params.put("manufacturer", Build.MANUFACTURER);
        params.put("Version", Build.VERSION.RELEASE);
        params.put("OS", "android");
        params.put("PhNumber", uPhone);
        params.put("CountryCode", "+" + cCode);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String currentDateandTime = sdf.format(new Date());
        params.put("DateTime", currentDateandTime);
        params.put("callToken", "Android");
        if (isMassChat) {
            //add invitatation code
            String referralCode = edReferalCode.getText().toString();
            params.put("invite_code", referralCode);
        }

        if (isTwitgo) {
            String email = edEmail.getText().toString();
            params.put("email", email);
        }
        if (CoreController.isRaad) {
            params.put("server_security_key", "tawari123");//raad
            params.put("Appversion", "1");//raad
            params.put("brand", "" + Build.MANUFACTURER);//raad
            params.put("model", "" + Build.MODEL);//raad
            params.put("lat", "" + getLatitude());//raad
            params.put("lng", "" + getLongitude());//raad
        }

        ServiceRequest request = new ServiceRequest(this);
        request.makeServiceRequest(Constants.VERIFY_NUMBER_REQUEST, Request.Method.POST, params, verifcationListener);
        showProgres();
    }

    private void clearDataIfUserChanged(String userId) {
        String prevLoginUserId = sessionManager.getPrevLoginUserId();
        if (!prevLoginUserId.equals("") && !prevLoginUserId.equalsIgnoreCase(userId)) {
            MessageDbController msgDb = CoreController.getDBInstance(AddInvitationScreen.this);
            msgDb.deleteDatabase();

            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(AddInvitationScreen.this);
            contactDB_sqlite.deleteDatabase();

            UserInfoSession userInfoSession = new UserInfoSession(AddInvitationScreen.this);
            userInfoSession.clearData();

            Session session = new Session(AddInvitationScreen.this);
            session.clearData();

        }
    }

    public void getRequestInviteAPI(final String mMisidin, final String uPhone, final String CountryCode, final String Email) {

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.POST, Constants.REQUESTINVITE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response.toString());

                try {
                    //    mPostCommentResponse.requestCompleted();
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("errNum")) {
                        String mErrNum = jsonObject.getString("errNum");
                        if (Integer.parseInt(mErrNum) == 0) {
                            Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(mContext, VerifyPhoneScreen.class);
                            mContext.startActivity(intent);
                        } else if (Integer.parseInt(mErrNum) == 1) {
                            Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        }
                    }
                } catch (Exception e) {

                }
                okButton.setEnabled(true);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  mPostCommentResponse.requestEndedWithError(error);
                Log.e("onErrorResponse", error.toString());
                hideProgressDialog();
                okButton.setEnabled(true);

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("msisdn", "+" + mMisidin);
                params.put("PhNumber", uPhone);
                params.put("CountryCode", "+" + CountryCode);
                params.put("email", String.valueOf(Email));
                Log.e("params", "params" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //  mSocketManager.disconnect();
    }

    private void skipOTP(final String OTP) {

        if (MyFirebaseMessagingService.token != null && MyFirebaseMessagingService.token.length() > 0)
            GCM_Id = MyFirebaseMessagingService.token;
        else
            GCM_Id = MyFirebaseMessagingService.getFCMToken(this);

        if (GCM_Id == null || GCM_Id.isEmpty()) {

            // Toast.makeText(getApplicationContext(),"GCM Empty",Toast.LENGTH_LONG).show();

            GCM_Id = "121";
        }


        //  Log.d(TAG, "skipOTP: " + OTP);

        String msisdn = SessionManager.getInstance(this).getPhoneNumberOfCurrentUser();
        String mobileNo = SessionManager.getInstance(this).getUserMobileNoWithoutCountryCode();
        String countryCode = SessionManager.getInstance(this).getUserCountryCode();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("msisdn", msisdn);
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        params.put("DeviceId", android_id);
        params.put("gcm_id", GCM_Id);
        params.put("manufacturer", Build.MANUFACTURER);
        params.put("Version", Build.VERSION.RELEASE);
        params.put("OS", "android");
        params.put("PhNumber", mobileNo);
        params.put("CountryCode", countryCode);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String currentDateandTime = sdf.format(new Date());
        params.put("DateTime", currentDateandTime);
        params.put("code", OTP);
        params.put("callToken", "Android");

        if (CoreController.isRaad) {
            params.put("server_security_key", "tawari123");//raad
            params.put("Appversion", "1");//raad
            params.put("brand", "" + Build.MANUFACTURER);//raad
            params.put("model", "" + Build.MODEL);//raad
            params.put("lat", "" + getLatitude());//raad
            params.put("lng", "" + getLongitude());//raad
        }

        if (isTwitgo) {
            params.put("email_otp", SessionManager.getInstance(this).getLoginEmailOTP());
        }
        params.put("pushToken", SessionManager.getInstance(this).getCurrentUserID());
        ServiceRequest request = new ServiceRequest(this);
        request.makeServiceRequest(Constants.VERIFY_SMS_CODE, Request.Method.POST, params, verifyCodeListener);
        showProgres();

    }

    public String getCountryZipCode() {
        String CountryID = "";
        String CountryZipCode = "";

/*        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID = manager.getSimCountryIso().toUpperCase();*/

        CountryID = Locale.getDefault().getCountry();
        String networkCoutry = Locale.getDefault().getISO3Country();
        MyLog.d(TAG, "getCountryZipCode: " + networkCoutry);
        String[] rl = this.getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                CountryZipCode = g[0];
                break;
            }
        }
        return CountryZipCode;
    }

    private void setCountryCode() {
        if (getCountryZipCode() != null && getCountryZipCode().length() > 0) {
            code = getCountryZipCode();
            choseCountry.setText("  (+" + code + ")");
            if (CoreController.isRaad) {
                code = "966";
                choseCountry.setText("  (+" + 966 + ")");
            }
            sessionManager.setCountryCodeOfCurrentUser("+" + code);
        }
    }

    public double getLatitude() {
        if (gpsTracker == null)
            gpsTracker = new GPSTracker(this);
        if (gpsTracker.getLocation() != null && gpsTracker.getLatitude() > 0) {
            return gpsTracker.getLatitude();
        }
        return 0;
    }

    public double getLongitude() {
        if (gpsTracker == null)
            gpsTracker = new GPSTracker(this);
        if (gpsTracker.getLocation() != null && gpsTracker.getLongitude() > 0) {
            return gpsTracker.getLongitude();
        }
        return 0;
    }

    public void setToken(SCLoginModel SCLoginModel) {
        if (SCLoginModel.getToken() != null) {
            String tokenOriginal = SCLoginModel.getToken();
            String encodedToken = AppUtils.SHA256(tokenOriginal);
            MyLog.d(TAG, "onCompleteListener securityToken: " + tokenOriginal);

            sessionManager.setUserSecurityToken(tokenOriginal.replace("\n", ""));
            if (encodedToken != null)
                sessionManager.setUserSecurityTokenHash(encodedToken.replace("\n", ""));
        }
    }

    public void resend_code(View view) {
        try {
            hideProgressDialog();
            String phoneNumberStr = phoneNumber.getText().toString();
            if (phoneNumberStr.isEmpty()) {
                Toast.makeText(AddInvitationScreen.this, "Please enter phone number", Toast.LENGTH_SHORT).show();
                return;
            }
            String phNumberWithCountryCode = "+" + code + phoneNumber.getText().toString();
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("msisdn", phNumberWithCountryCode);
            ServiceRequest request = new ServiceRequest(this);
            request.makeServiceRequest(Constants.RESEND_INVITE_CODE_REQUEST, Request.Method.POST, params, resendInviteCodeListner);
            showProgres();
        } catch (Exception e) {
            Log.e(TAG, "resend_code: ", e);
        }
    }
}


