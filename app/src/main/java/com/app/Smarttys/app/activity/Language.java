package com.app.Smarttys.app.activity;

import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.SharedPreference;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.core.CoreActivity;

import java.util.Locale;

public class Language extends CoreActivity {

    RelativeLayout privacy, security, changenumber, deletemyaccount, language;
    ImageView backpress;
    AvnNextLTProDemiTextView privacy_text, security_text, change_num_text, delete_text, text_actionbar_1;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lan);
        getSupportActionBar().hide();
        mContext = Language.this;
        privacy_text = findViewById(R.id.account_txt1);
        security_text = findViewById(R.id.account_txt2);
        change_num_text = findViewById(R.id.account_txt3);
        delete_text = findViewById(R.id.account_txt4);

        backpress = findViewById(R.id.backarrow_account);
        privacy = findViewById(R.id.account_r2);
        deletemyaccount = findViewById(R.id.account_r5);
        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreference.getInstance().save(mContext, "lan", "ar");
                CheckLanguage("ar", "AE");
                TaskStackBuilder.create(mContext)
                        .addNextIntent(new Intent(mContext, NewHomeScreenActivty.class))
                        .addNextIntent(getIntent())
                        .startActivities();
            }
        });

        deletemyaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreference.getInstance().save(mContext, "lan", "en");

                TaskStackBuilder.create(mContext)
                        .addNextIntent(new Intent(mContext, NewHomeScreenActivty.class))
                        .addNextIntent(getIntent())
                        .startActivities();
            }
        });


        backpress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                ActivityLauncher.launchSettingScreen(Account_main_list.this);
                onBackPressed();
            }
        });
    }

    public void CheckLanguage(String mLanguagecode, String mCountrycode) {
        Locale myLocale = new Locale(mLanguagecode, mCountrycode);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}





