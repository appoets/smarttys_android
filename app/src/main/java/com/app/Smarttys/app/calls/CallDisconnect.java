package com.app.Smarttys.app.calls;

//event class for EventBus - to trigger MessageService to CallsActivity
public class CallDisconnect {
    private String eventName;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

}
