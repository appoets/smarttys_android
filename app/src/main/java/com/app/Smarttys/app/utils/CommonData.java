package com.app.Smarttys.app.utils;

import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.status.model.StatusModel;

import java.util.ArrayList;

public class CommonData {

    public static ArrayList<MessageItemChat> forwardedItems = new ArrayList<>();

    public static ArrayList<StatusModel> forwardedstatusItems = new ArrayList<>();

    public static ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();

    public static void setForwardedItems(ArrayList<MessageItemChat> forwardedItems) {
        CommonData.forwardedItems = forwardedItems;
    }


    public static void setStatusItems(ArrayList<StatusModel> forwardedItems) {
        CommonData.forwardedstatusItems = forwardedItems;
    }
}
