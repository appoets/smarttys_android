package com.app.Smarttys.app.calls;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.RecyclerViewItemClickListener;
import com.app.Smarttys.app.utils.TimeStampUtils;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.CallItemChat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by CAS60 on 7/28/2017.
 */
public class CallHistoryAdapter extends RecyclerView.Adapter<CallHistoryAdapter.VHCallHistory> implements Filterable {

    public List<CallItemChat> mDisplayedValues;
    public List<CallItemChat> mDisplayedValues_previous = new ArrayList<>();
    private Context mContext;
    private RecyclerViewItemClickListener listener;
    private List<CallItemChat> mOriginalValues;

    private Getcontactname getcontactname;

    public CallHistoryAdapter(Context context, ArrayList<CallItemChat> callItemList) {
        this.mContext = context;
        this.mDisplayedValues = callItemList;
        this.mOriginalValues = callItemList;

        getcontactname = new Getcontactname(mContext);
    }

    @Override
    public VHCallHistory onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_call_history, parent, false);
        VHCallHistory holder = new VHCallHistory(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final VHCallHistory holder, int position) {
        CallItemChat callItem = null;
        try {
            callItem = mDisplayedValues.get(position);

        } catch (Exception e) {
            Log.e("tag", "onBindViewHolder: ", e);
        }
        if (callItem == null)
            return;
        String toUserId = callItem.getOpponentUserId();
        getcontactname.configProfilepic(holder.ivProfilePic, toUserId, false, false, R.mipmap.chat_attachment_profile_default_image_frame);


        String Name = getcontactname.getSendername(toUserId, callItem.getOpponentUserMsisdn());

        //System.out.println("CALL HISTORY NAME"+Name);

        String senderName = getcontactname.getSendername(toUserId, callItem.getOpponentUserMsisdn());
        if (senderName != null && !senderName.isEmpty())
            holder.tvName.setText(senderName);
        else {
            String contactno = callItem.getOpponentUserMsisdn();
            if (contactno != null && !contactno.isEmpty())
                holder.tvName.setText(contactno);
        }

        if (callItem.getCallType().equals(MessageFactory.video_call + "")) {
            holder.ibCall.setImageResource(R.drawable.lets_video);
            holder.ibCall.setTag(MessageFactory.video_call + "");
        } else {
            holder.ibCall.setImageResource(R.drawable.lets_call_icon);
            holder.ibCall.setTag(MessageFactory.audio_call + "");
        }
        holder.ibCall.setTag(R.string.data, callItem);
        holder.parent.setTag(R.string.data, callItem);
        if (!callItem.isSelf()) {
            String callStatus = callItem.getCallStatus();
            if (callStatus.equals(MessageFactory.CALL_STATUS_REJECTED + "")
                    || callStatus.equals(MessageFactory.CALL_STATUS_MISSED + "")) {
                holder.ivCallType.setBackgroundResource(R.drawable.ic_in_call_missed);
            } else {
                holder.ivCallType.setBackgroundResource(R.drawable.ic_in_call_received);
            }
        } else {
            holder.ivCallType.setBackgroundResource(R.drawable.ic_out_call);
        }

        setDateText(callItem.getCallCount(), holder.tvTS, callItem.getTS());


        holder.call_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onRVItemClick(holder.ibCall, holder.getAdapterPosition());
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onRVItemClick(holder.itemView, holder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDisplayedValues.size();
    }

    public CallItemChat getItem(int position) {
        return mDisplayedValues.get(position);
    }

    public void setOnItemClickListener(RecyclerViewItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<CallItemChat>) results.values; // has the filtered values
                if (mDisplayedValues.size() == 0) {
//                    Toast.makeText(context, "No Contacts Matching Your Query...", Toast.LENGTH_SHORT).show();
                }

                if (mDisplayedValues != mDisplayedValues_previous) {
                    notifyDataSetChanged();  // notifies the data with new filtered values
                    mDisplayedValues_previous = mDisplayedValues;
                }

            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<CallItemChat> FilteredArrList = new ArrayList<>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {


                        String contactName = mOriginalValues.get(i).getCallerName();
                        String contactNo = mOriginalValues.get(i).getOpponentUserMsisdn();

                        if (contactName.toLowerCase().contains(constraint) || contactNo.toLowerCase().contains(constraint)) {
                            FilteredArrList.add(mOriginalValues.get(i));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    private void setDateText(int callCount, TextView tvDateLbl, String ts) {
        Date today = TimeStampUtils.getDateFormat(Calendar.getInstance().getTimeInMillis());
        Date yesterday = TimeStampUtils.getYesterdayDate(today);

        Date currentItemDate = TimeStampUtils.getDateFormat(AppUtils.parseLong(ts));
        String time = TimeStampUtils.get12HrTimeFormat(mContext, ts);

        if (currentItemDate != null) {
            if (currentItemDate.equals(today)) {
                String txt = "(" + callCount + ") " + mContext.getString(R.string.today) + " " + time;
                tvDateLbl.setText(txt);
            } else if (currentItemDate.equals(yesterday)) {
                String txt = "(" + callCount + ") " + mContext.getString(R.string.yesterday) + " " + time;
                tvDateLbl.setText(txt);
            } else {
                DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
                String formatDate = df.format(currentItemDate);
                String txt = "(" + callCount + ") " + formatDate + " " + time;
                tvDateLbl.setText(txt);
            }
        } else {
            tvDateLbl.setText("");
        }
    }

    public class VHCallHistory extends RecyclerView.ViewHolder {

        private ImageView ivProfilePic, ivCallType;
        private TextView tvName, tvTS;
        private ImageView ibCall;
        private RelativeLayout parent;
        private RelativeLayout call_layout;

        public VHCallHistory(View itemView) {
            super(itemView);
            parent = itemView.findViewById(R.id.rlParent);
            ivProfilePic = itemView.findViewById(R.id.ivProfilePic);
            ivCallType = itemView.findViewById(R.id.ivCallType);
            tvName = itemView.findViewById(R.id.tvName);
            tvTS = itemView.findViewById(R.id.tvTS);
            ibCall = itemView.findViewById(R.id.ibCall);
            call_layout = itemView.findViewById(R.id.call_layout);
        }
    }

}
