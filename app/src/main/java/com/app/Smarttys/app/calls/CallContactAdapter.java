package com.app.Smarttys.app.calls;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.RecyclerViewItemClickListener;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.app.widget.CircleImageView;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by CAS63 on 7/28/2017.
 */
public class CallContactAdapter extends RecyclerView.Adapter<CallContactAdapter.MyViewHolder> implements Filterable, FastScrollRecyclerView.SectionedAdapter {

    private static final String TAG = "CallContactAdapter";
    public List<SmarttyContactModel> mDisplayedValues;
    private Context context;
    private List<SmarttyContactModel> mOriginalValues;
    private Session session;
    private Getcontactname getcontactname;
    private String myid;
    private RecyclerViewItemClickListener myListener;

    public CallContactAdapter(Context context, List<SmarttyContactModel> data) {
        this.context = context;
        this.mDisplayedValues = data;
        this.mOriginalValues = data;
        session = new Session(context);
        getcontactname = new Getcontactname(context);
        myid = SessionManager.getInstance(context).getCurrentUserID();
    }

    public SmarttyContactModel getItem(int position) {
        return mDisplayedValues.get(position);
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        SmarttyContactModel contact = mDisplayedValues.get(position);
        return contact.getFirstName().substring(0, 1);
    }

    public void updateInfo(List<SmarttyContactModel> aitem) {
        this.mDisplayedValues = aitem;
        this.mOriginalValues = aitem;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_inflater_call_contacts_items, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        setItemClickListener(holder, position);
        SmarttyContactModel contact = mDisplayedValues.get(position);
        holder.tvStatus.setTextSize(13);
        holder.tvName.setText(contact.getFirstName());
        Log.e("Type", "GetType " + contact.getType());
        if (contact.getType().equalsIgnoreCase("Hey there! I am using Smarttys"))
            holder.tvStatus.setText("Hey there! I am using Smarttys");
        else
            holder.tvStatus.setText(contact.getType());
        holder.pos = position;

        try {
            String to = contact.get_id();
            getcontactname.configProfilepic(holder.ivUser, to, false, false, R.mipmap.chat_attachment_profile_default_image_frame);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void setCallContactsItemClickListener(RecyclerViewItemClickListener listener) {
        this.myListener = listener;
    }

    private void setItemClickListener(final MyViewHolder vh2, final int position) {
        if (myListener != null) {

            vh2.audio_call_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myListener.onRVItemClick(vh2.audio_call_layout, position);
                }
            });

            vh2.video_call_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    myListener.onRVItemClick(vh2.video_call_layout, position);

                }
            });

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mDisplayedValues.size();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<SmarttyContactModel>) results.values; // has the filtered values
                if (mDisplayedValues.size() == 0) {
//                    Toast.makeText(context, "No Contacts Matching Your Query...", Toast.LENGTH_SHORT).show();
                }

                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<SmarttyContactModel> FilteredArrList = new ArrayList<>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {
                        String contactName = mOriginalValues.get(i).getFirstName();
                        String contactNo = mOriginalValues.get(i).getNumberInDevice();

                        if (contactName.toLowerCase().contains(constraint) || contactNo.toLowerCase().contains(constraint)) {
                            SmarttyContactModel mycontact = new SmarttyContactModel();
                            mycontact.setFirstName(mOriginalValues.get(i).getFirstName());

                            mycontact.set_id(mOriginalValues.get(i).get_id());
                            mycontact.setStatus(mOriginalValues.get(i).getStatus());
                            mycontact.setAvatarImageUrl(mOriginalValues.get(i).getAvatarImageUrl());
                            Log.e("CallContactAdapter", "setNumberInDevice" + mOriginalValues.get(i).getNumberInDevice());

                            mycontact.setNumberInDevice(mOriginalValues.get(i).getNumberInDevice());

                            FilteredArrList.add(mycontact);
                        }


                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public AvnNextLTProDemiTextView tvName;
        public ImageView tick;
        public int pos;
        public ImageView myAudioCallIMG, myVideoCallIMG;
        protected AvnNextLTProRegTextView tvStatus;
        protected CircleImageView ivUser;
        private RelativeLayout audio_call_layout;
        private RelativeLayout video_call_layout;

        public MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.layout_inflater_contacts_items_uNameTXT);
            tvStatus = view.findViewById(R.id.layout_inflater_contacts_items_devstatusTXT);
            myAudioCallIMG = view.findViewById(R.id.layout_inflater_contacts_items_callIMG);
            myVideoCallIMG = view.findViewById(R.id.layout_inflater_contacts_items_videocallIMG);
            ivUser = view.findViewById(R.id.layout_inflater_contacts_items_userPhoto_contacts);
            audio_call_layout = view.findViewById(R.id.audio_call_layout);
            video_call_layout = view.findViewById(R.id.video_call_layout);

            tvName.setTextColor(Color.parseColor("#3f3f3f"));
            tvStatus.setTextColor(Color.parseColor("#808080"));
            Typeface face2 = CoreController.getInstance().getAvnNextLTProRegularTypeface();
            tvStatus.setTypeface(face2);
            tvStatus.setTextSize(13);
        }
    }


}
