package com.app.Smarttys.app.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.DeviceLockSettingAdapter;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.LoginAuthUtils;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.SessionManager;
import com.kyleduo.switchbutton.SwitchButton;

public class ChatSettingsActivity extends CoreActivity {

    private SwitchButton switchButton;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_settings);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setSwitchSelection();
    }

    private void setSwitchSelection() {
        if (AppUtils.isDeviceLockEnabled(this)) {
            switchButton.setChecked(true);
        } else {
            switchButton.setChecked(false);
        }
    }

    private void initView() {
        switchButton = findViewById(R.id.switch_chat_lock);

        recyclerView = findViewById(R.id.rv_chat_settings);
        recyclerView.setLayoutManager(AppUtils.getDefaultLayoutManger(this));
        setAdapter();
        setSwitchSelection();
        //setListVisiblity();
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (LoginAuthUtils.isDeviceHasLock(ChatSettingsActivity.this)) {
                    SessionManager.getInstance(ChatSettingsActivity.this).setDeviceLock(isChecked);
                } else {
                    switchButton.setChecked(false);
                    Toast.makeText(ChatSettingsActivity.this, "Your device not have default lock settings. Please enable & try again", Toast.LENGTH_SHORT).show();
                }
                //setListVisiblity();
            }
        });

        findViewById(R.id.iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setListVisiblity() {
        if (AppUtils.isDeviceLockEnabled(this)) {
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.INVISIBLE);
        }
    }

    private void setAdapter() {
        DeviceLockSettingAdapter deviceLockSettingAdapter = new DeviceLockSettingAdapter(this);
        recyclerView.setAdapter(deviceLockSettingAdapter);
    }


}
