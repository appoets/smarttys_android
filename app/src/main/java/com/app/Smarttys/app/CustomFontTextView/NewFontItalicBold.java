package com.app.Smarttys.app.CustomFontTextView;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class NewFontItalicBold extends AppCompatTextView {

    public NewFontItalicBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public NewFontItalicBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NewFontItalicBold(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Karla-BoldItalic.ttf");
        setTypeface(tf);
    }
}