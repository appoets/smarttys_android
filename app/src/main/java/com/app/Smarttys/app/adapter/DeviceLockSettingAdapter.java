package com.app.Smarttys.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.LoginAuthUtils;
import com.app.Smarttys.core.SessionManager;

import java.util.ArrayList;

public class DeviceLockSettingAdapter extends RecyclerView.Adapter<DeviceLockSettingAdapter.ViewHolder> {

    private Context context;

    private ArrayList<String> titles = new ArrayList<>();

    public DeviceLockSettingAdapter(Context context) {
        this.context = context;
        titles.clear();
        titles.add(context.getString(R.string.immediately));
        titles.add(context.getString(R.string.after_1_minute));
        titles.add(context.getString(R.string.after_15_minutes));
        titles.add(context.getString(R.string.after_one_hour));

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.listrow_device_lock_settings, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int type = SessionManager.getInstance(context).getDeviceLockType();
        holder.ivTick.setVisibility(View.GONE);
        holder.tvTitle.setText(titles.get(position));
        if (type == position)
            holder.ivTick.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        return titles.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        private ImageView ivTick;
        private View rootView;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            ivTick = itemView.findViewById(R.id.iv_tick);
            rootView = itemView.findViewById(R.id.root_view);

            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (LoginAuthUtils.isDeviceHasLock(context)) {
                        SessionManager.getInstance(context).setDeviceLockDurationType(getAdapterPosition());
                        notifyDataSetChanged();
                    } else {
                        Toast.makeText(context, "Your device not have default lock settings. Please enable & try again", Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }
    }
}
