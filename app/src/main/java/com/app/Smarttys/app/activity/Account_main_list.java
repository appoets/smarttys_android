package com.app.Smarttys.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.app.Smarttys.R;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.core.CoreActivity;

public class Account_main_list extends CoreActivity {

    RelativeLayout privacy, security, changenumber, deletemyaccount, language;
    ImageView backpress;
    AvnNextLTProDemiTextView privacy_text, security_text, change_num_text, delete_text, text_actionbar_1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountmain);
        getSupportActionBar().hide();

        privacy_text = findViewById(R.id.account_txt1);
        security_text = findViewById(R.id.account_txt2);
        change_num_text = findViewById(R.id.account_txt3);
        delete_text = findViewById(R.id.account_txt4);

        backpress = findViewById(R.id.backarrow_account);
        privacy = findViewById(R.id.account_r2);
        security = findViewById(R.id.account_r3);
        changenumber = findViewById(R.id.account_r4);
        deletemyaccount = findViewById(R.id.account_r5);
        language = findViewById(R.id.account_r6);

        language.setVisibility(View.GONE);
        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_privacy = new Intent(getApplicationContext(), SmarttyPrivacy.class);
                startActivity(intent_privacy);

            }
        });
        security.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_security = new Intent(getApplicationContext(), Security.class);
                startActivity(intent_security);
            }
        });
        changenumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_change = new Intent(getApplicationContext(), ChangeNumber.class);
                startActivity(intent_change);
            }
        });
        deletemyaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_del = new Intent(getApplicationContext(), DeleteAccount.class);
                startActivity(intent_del);

            }
        });
        language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_del = new Intent(getApplicationContext(), Language.class);
                startActivity(intent_del);

            }
        });
        // Get ListView object from xml

        backpress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                ActivityLauncher.launchSettingScreen(Account_main_list.this);
                finish();
            }
        });
    }

}





