package com.app.Smarttys.app.calls;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.KeyguardManager;
import android.app.PictureInPictureParams;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Rational;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.Smarttys.ConnectivityReceiver;
import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.ChatListFragment;
import com.app.Smarttys.app.activity.NewHomeScreenActivty;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.ConnectivityInfo;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.NetworkStateReceiver;
import com.app.Smarttys.app.utils.SharedPreference;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.IncomingMessage;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.CallItemChat;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.MessageService;
import com.app.Smarttys.core.socket.SocketManager;

import org.appspot.apprtc.AppRTCAudioManager;
import org.appspot.apprtc.AppRTCClient;
import org.appspot.apprtc.CallActivity;
import org.appspot.apprtc.CallFragment;
import org.appspot.apprtc.CpuMonitor;
import org.appspot.apprtc.DirectRTCClient;
import org.appspot.apprtc.HudFragment;
import org.appspot.apprtc.PeerConnectionClient;
import org.appspot.apprtc.PercentFrameLayout;
import org.appspot.apprtc.UnhandledExceptionHandler;
import org.appspot.apprtc.WebSocketRTCClient;
import org.appspot.apprtc.WebrtcConstants;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.Camera1Enumerator;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.EglBase;
import org.webrtc.FileVideoCapturer;
import org.webrtc.IceCandidate;
import org.webrtc.Logging;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RendererCommon;
import org.webrtc.ScreenCapturerAndroid;
import org.webrtc.SessionDescription;
import org.webrtc.StatsReport;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoFileRenderer;
import org.webrtc.VideoRenderer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import static org.appspot.apprtc.WebrtcConstants.isTurnServerEnabled;


/**
 * Created by CAS60 on 7/18/2017.
 */

/**
 * Activity for peer connection call setup, call waiting
 * and call view.
 */
public class CallsActivity extends Activity implements View.OnTouchListener, AppRTCClient.SignalingEvents,
        PeerConnectionClient.PeerConnectionEvents,
        CallFragment.OnCallEvents, ConnectivityReceiver.ConnectivityReceiverListener {

    public static final String EXTRA_DOC_ID = "DocId";
    public static final String EXTRA_FROM_USER_ID = "FromUserId";
    public static final String EXTRA_TO_USER_ID = "ToUserId";
    public static final String EXTRA_USER_MSISDN = "Msisdn";
    public static final String EXTRA_IS_OUTGOING_CALL = "OutgoingCall";
    public static final String EXTRA_OPPONENT_PROFILE_PIC = "ProfilePic";
    public static final String EXTRA_NAVIGATE_FROM = "NavigateFrom";
    public static final String EXTRA_CALL_CONNECT_STATUS = "CallConnectStatus";
    public static final String EXTRA_CALL_TIME_STAMP = "CallTimeStamp";
    public static final String RECONNECTING = "Reconnecting";
    public static final String CALLDURATION = "Callduration";
    public static final String RECORDID = "RecordId";
    public static final String EXTRA_ROOMID = "org.appspot.apprtc.ROOMID";
    public static final String EXTRA_LOOPBACK = "org.appspot.apprtc.LOOPBACK";
    public static final String EXTRA_VIDEO_CALL = "org.appspot.apprtc.VIDEO_CALL";
    public static final String EXTRA_SCREENCAPTURE = "org.appspot.apprtc.SCREENCAPTURE";
    public static final String EXTRA_CAMERA2 = "org.appspot.apprtc.CAMERA2";
    public static final String EXTRA_VIDEO_WIDTH = "org.appspot.apprtc.VIDEO_WIDTH";
    public static final String EXTRA_VIDEO_HEIGHT = "org.appspot.apprtc.VIDEO_HEIGHT";
    public static final String EXTRA_VIDEO_FPS = "org.appspot.apprtc.VIDEO_FPS";
    public static final String EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED =
            "org.appsopt.apprtc.VIDEO_CAPTUREQUALITYSLIDER";
    public static final String EXTRA_VIDEO_BITRATE = "org.appspot.apprtc.VIDEO_BITRATE";
    public static final String EXTRA_VIDEOCODEC = "org.appspot.apprtc.VIDEOCODEC";
    public static final String EXTRA_HWCODEC_ENABLED = "org.appspot.apprtc.HWCODEC";
    public static final String EXTRA_CAPTURETOTEXTURE_ENABLED = "org.appspot.apprtc.CAPTURETOTEXTURE";
    public static final String EXTRA_FLEXFEC_ENABLED = "org.appspot.apprtc.FLEXFEC";
    public static final String EXTRA_AUDIO_BITRATE = "org.appspot.apprtc.AUDIO_BITRATE";
    public static final String EXTRA_AUDIOCODEC = "org.appspot.apprtc.AUDIOCODEC";
    public static final String EXTRA_NOAUDIOPROCESSING_ENABLED =
            "org.appspot.apprtc.NOAUDIOPROCESSING";
    public static final String EXTRA_AECDUMP_ENABLED = "org.appspot.apprtc.AECDUMP";
    public static final String EXTRA_OPENSLES_ENABLED = "org.appspot.apprtc.OPENSLES";
    public static final String EXTRA_DISABLE_BUILT_IN_AEC = "org.appspot.apprtc.DISABLE_BUILT_IN_AEC";
    public static final String EXTRA_DISABLE_BUILT_IN_AGC = "org.appspot.apprtc.DISABLE_BUILT_IN_AGC";
    public static final String EXTRA_DISABLE_BUILT_IN_NS = "org.appspot.apprtc.DISABLE_BUILT_IN_NS";
    public static final String EXTRA_ENABLE_LEVEL_CONTROL = "org.appspot.apprtc.ENABLE_LEVEL_CONTROL";
    public static final String EXTRA_DISPLAY_HUD = "org.appspot.apprtc.DISPLAY_HUD";
    public static final String EXTRA_TRACING = "org.appspot.apprtc.TRACING";
    public static final String EXTRA_CMDLINE = "org.appspot.apprtc.CMDLINE";
    public static final String EXTRA_RUNTIME = "org.appspot.apprtc.RUNTIME";
    public static final String EXTRA_VIDEO_FILE_AS_CAMERA = "org.appspot.apprtc.VIDEO_FILE_AS_CAMERA";
    public static final String EXTRA_SAVE_REMOTE_VIDEO_TO_FILE =
            "org.appspot.apprtc.SAVE_REMOTE_VIDEO_TO_FILE";
    public static final String EXTRA_SAVE_REMOTE_VIDEO_TO_FILE_WIDTH =
            "org.appspot.apprtc.SAVE_REMOTE_VIDEO_TO_FILE_WIDTH";
    public static final String EXTRA_SAVE_REMOTE_VIDEO_TO_FILE_HEIGHT =
            "org.appspot.apprtc.SAVE_REMOTE_VIDEO_TO_FILE_HEIGHT";
    public static final String EXTRA_USE_VALUES_FROM_INTENT =
            "org.appspot.apprtc.USE_VALUES_FROM_INTENT";
    public static final String EXTRA_DATA_CHANNEL_ENABLED = "org.appspot.apprtc.DATA_CHANNEL_ENABLED";
    public static final String EXTRA_ORDERED = "org.appspot.apprtc.ORDERED";
    public static final String EXTRA_MAX_RETRANSMITS_MS = "org.appspot.apprtc.MAX_RETRANSMITS_MS";
    public static final String EXTRA_MAX_RETRANSMITS = "org.appspot.apprtc.MAX_RETRANSMITS";
    public static final String EXTRA_PROTOCOL = "org.appspot.apprtc.PROTOCOL";
    public static final String EXTRA_NEGOTIATED = "org.appspot.apprtc.NEGOTIATED";
    public static final String EXTRA_ID = "org.appspot.apprtc.ID";
    public static final long MISSED_CALL_TIMEOUT = 60 * 1000; // 60 seconds
    // we can be in one of these 3 states
    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private static final String TAG = "CallRTCClient";
    private static final int CAPTURE_PERMISSION_REQUEST_CODE = 1;
    // List of mandatory application permissions.
    private static final String[] MANDATORY_PERMISSIONS = {"android.permission.MODIFY_AUDIO_SETTINGS", "android.permission.RECORD_AUDIO", "android.permission.INTERNET"};
    // Peer connection statistics callback period in ms.
    private static final int STAT_CALLBACK_PERIOD = 1000;
    // Local preview screen position before call is connected.
    private static final int LOCAL_X_CONNECTING = 0;
    private static final int LOCAL_Y_CONNECTING = 0;
    private static final int LOCAL_WIDTH_CONNECTING = 100;
    private static final int LOCAL_HEIGHT_CONNECTING = 100;
    // Local preview screen position after call is connected.
    private static final int LOCAL_X_CONNECTED = 72;
    private static final int LOCAL_Y_CONNECTED = 72;
    private static final int LOCAL_WIDTH_CONNECTED = 25;
    private static final int LOCAL_HEIGHT_CONNECTED = 25;
    // Remote video screen position
    private static final int REMOTE_X = 0;
    private static final int REMOTE_Y = 0;
    private static final int REMOTE_WIDTH = 100;
    private static final int REMOTE_HEIGHT = 100;
    public static Activity mActivity;
    public static boolean isStarted;
    public static boolean canEndCall;
    public static String opponentUserId = "";
    private static Intent mediaProjectionPermissionResultData;
    private static int mediaProjectionPermissionResultCode;
    private final List<VideoRenderer.Callbacks> remoteRenderers =
            new ArrayList<VideoRenderer.Callbacks>();
    private final long CALL_RETRY_DURATION = 6000; // 6 seconds
    private final long RECONNECT_CALL_TIMEOUT = 10 * 1000; // 10 seconds
    private final long OFFLINE_RECONNECT_CALL_TIMEOUT = 40 * 1000; // 40 seconds
    public int cameraswirchcount = 0;
    public boolean cameraswitched = false;
    public boolean mReconnecting;
    public Activity activity;
    int _xDelta;
    int _yDelta;
    String msisdn;
    FrameLayout rootlayout;
    Handler mHandlerr = null;
    Runnable mRunnable = null;
    boolean mDisconnectcall;
    int screenWidth = 0;
    int screenHeight = 0;
    int lastX = 0, lastY = 0;
    ImageView arrow;
    NetworkStateReceiver.NetworkStateReceiverListener mListenerr;
    boolean mStartTimer;
    // these matrices will be used to move and zoom image
    private Matrix matrix = new Matrix();
    private Matrix savedMatrix = new Matrix();
    private int mode = NONE;
    // remember some things for zooming
    private PointF start = new PointF();
    private PointF mid = new PointF();
    private float oldDist = 1f;
    private float d = 0f;
    private float newRot = 0f;
    private float[] lastEvent = null;
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    // private PictureInPictureParams.Builder mPictureInPictureParamsBuilder;// = new PictureInPictureParams.Builder();
    private int field = 0x00000020;
    private Context mContext;
    private PeerConnectionClient peerConnectionClient = null;
    private AppRTCClient appRtcClient;
    private AppRTCClient.SignalingParameters signalingParameters;
    private AppRTCAudioManager audioManager = null;
    private EglBase rootEglBase;
    private SurfaceViewRenderer localRender;
    private SurfaceViewRenderer remoteRenderScreen;
    private VideoFileRenderer videoFileRenderer;
    private PercentFrameLayout localRenderLayout;
    private PercentFrameLayout remoteRenderLayout;
    private TextView tvName, tvCallLbl, tvDuration, tvCallStatus;
    private ImageView ivProfilePic;
    private RelativeLayout relative;
    private RendererCommon.ScalingType scalingType;
    private Toast logToast;
    private boolean commandLineRun;
    private int runTimeMs;
    private boolean activityRunning;
    private AppRTCClient.RoomConnectionParameters roomConnectionParameters;
    private PeerConnectionClient.PeerConnectionParameters peerConnectionParameters;
    private boolean iceConnected;
    private boolean isError;
    private boolean callControlFragmentVisible = true;
    private long callStartedTimeMs = 0;
    private boolean micEnabled = true;
    private boolean speakerEnabled = true;
    private boolean screencaptureEnabled = false;
    private Ringtone ringtone;
    private Vibrator vibrator;
    // Controls
    private CallFragment callFragment;
    private HudFragment hudFragment;
    private CpuMonitor cpuMonitor;
    private String mCurrentUserId, fromUserId, toUserId, mRoomId, mRecordId, mCallId, mPrevCallStatus, mCallTs;
    private MediaPlayer mediaPlayer;
    private boolean isConnectedToUser = false, needToSendServer = true, isOutgoingCall, isVideoCall,
            isArrivedToUser, isAnsweredToUser, isCallReconnecting, isNetworkConnected;
    private Timer timer;
    private int callDuration = 0;
    private Handler callTimeoutHandler, retryCallHandler, reconnectHandler;
    private Runnable callTimeoutRunnable, retryCallRunnable, reconnectRunnable;
    private String mCallerName;
    private boolean showDisconnectNotify = true;
    BroadcastReceiver incomingCallReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            long currentCallTS = 0, newCallTS = 0;
            if (mCallTs != null && !mCallTs.equals("0")) {
                currentCallTS = AppUtils.parseLong(mCallTs);
            }

            String strNewCallTS = intent.getStringExtra(EXTRA_CALL_TIME_STAMP);
            if (strNewCallTS != null && !strNewCallTS.equals("0")) {
                newCallTS = AppUtils.parseLong(strNewCallTS);
            }

            String from = intent.getStringExtra(EXTRA_FROM_USER_ID);
            if (newCallTS > currentCallTS && from.equalsIgnoreCase(toUserId)) {
                String callId = intent.getStringExtra(EXTRA_DOC_ID);
                String fromMsisdn = intent.getStringExtra(EXTRA_USER_MSISDN);
                String to = intent.getStringExtra(EXTRA_TO_USER_ID);
                String roomId = intent.getStringExtra(EXTRA_DOC_ID);
                boolean isVideoCall = intent.getBooleanExtra(EXTRA_VIDEO_CALL, false);

               /* CallMessage.openCallScreen(context, from, to, callId, roomId, "", fromMsisdn,
                        MessageFactory.CALL_IN_FREE + "", isVideoCall, false, strNewCallTS);*/
                showDisconnectNotify = false;
                finish();
            }

            MyLog.d("Broadcastincoming", strNewCallTS + "---" + currentCallTS);
        }
    };
    private RelativeLayout disconnect_layout;
    private RelativeLayout Call_Disconnect;
    private ImageView ibToggleSpeaker;
    private ImageView button_call_switch_camera;
    private ImageView button_call_toggle_mic;
    private LinearLayout buttons_call_container;
    private RelativeLayout bottom_layout;
    private RelativeLayout call_header;
    private String Outgoing_call_type = "";
    private ChatListFragment mHomeFragment;
    private RelativeLayout local_video_relativelayout;
    private RelativeLayout dummy_layout;
    private PictureInPictureParams.Builder mPictureInPictureParamsBuilder = null;//\ = new PictureInPictureParams.Builder();

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyLog.e(TAG, "onCreate");
        isStarted = true;
        canEndCall = false;
        mActivity = CallsActivity.this;
        mContext = CallsActivity.this;
        isNetworkConnected = ConnectivityReceiver.isConnected();
        SessionManager.getInstance(this).setIsScreenActivated(true);
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        IntentFilter intentFilter = new IntentFilter(getPackageName() + ".incoming_call");
        registerReceiver(incomingCallReceiver, intentFilter);
        CoreController.getInstance().setConnectivityListener(this);
        SharedPreference.getInstance().saveBool(mContext, "isAnsweredToUser", false);
        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(field, getLocalClassName());
        if (!wakeLock.isHeld()) {
            wakeLock.acquire();
        }

        Thread.setDefaultUncaughtExceptionHandler(new UnhandledExceptionHandler(this));

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);


        setContentView(R.layout.activity_av_calls);

        disableLockScreen();

        iceConnected = false;
        signalingParameters = null;
        scalingType = RendererCommon.ScalingType.SCALE_ASPECT_FILL;

        // Create UI controls.
        relative = findViewById(R.id.relative);
        localRender = findViewById(R.id.local_video_view);
        remoteRenderScreen = findViewById(R.id.remote_video_view);
        localRenderLayout = findViewById(R.id.local_video_layout);
        remoteRenderLayout = findViewById(R.id.remote_video_layout);
        arrow = findViewById(R.id.arrow);
        arrow.setVisibility(View.VISIBLE);
        tvName = findViewById(R.id.tvName);
        tvCallLbl = findViewById(R.id.tvCallLbl);
        tvDuration = findViewById(R.id.tvDuration);
        tvCallStatus = findViewById(R.id.tvCallStatus);
        ivProfilePic = findViewById(R.id.ivProfilePic);
        disconnect_layout = findViewById(R.id.disconnect_layout);
        Call_Disconnect = findViewById(R.id.Call_Disconnect);
        ibToggleSpeaker = findViewById(R.id.ibToggleSpeaker);
        button_call_switch_camera = findViewById(R.id.button_call_switch_camera);
        button_call_toggle_mic = findViewById(R.id.button_call_toggle_mic);

        buttons_call_container = findViewById(R.id.buttons_call_container);
        bottom_layout = findViewById(R.id.bottom_layout);
        call_header = findViewById(R.id.call_header);

        local_video_relativelayout = findViewById(R.id.local_video_relativelayout);

        dummy_layout = findViewById(R.id.dummy_layout);


        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreference.getInstance().saveBool(mContext, "callongoing", true);
                //check call is started or not
                //  IncomingCallActivity.isStarted = true;
                if (isVideoCall) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startPictureInPictureFeature();
                    } else {
                        Intent backIntent = new Intent(CallsActivity.this, NewHomeScreenActivty.class);
                        backIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(backIntent);
                    }
                } else {
                    Intent backIntent = new Intent(CallsActivity.this, NewHomeScreenActivty.class);
                    backIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(backIntent);
                }

            }
        });
        Call_Disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                canEndCall = true;
                mDisconnectcall = true;
                disconnect(false);
                // startPictureInPictureFeature();
            }
        });
        ibToggleSpeaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean enabled = SpeakerOn();
                if (enabled) {
                    ibToggleSpeaker.setImageResource(org.appspot.apprtc.R.drawable.ic_specker_on);
                } else {
                    ibToggleSpeaker.setImageResource(org.appspot.apprtc.R.drawable.ic_specker_off);
                }
            }
        });
        button_call_switch_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (peerConnectionClient != null) {
                    peerConnectionClient.switchCamera();
                    cameraswitched = !cameraswitched;
                    updateVideoView();
                }
            }
        });
        button_call_toggle_mic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean enabled = ToggleMic();
                button_call_toggle_mic.setAlpha(enabled ? 1.0f : 0.3f);
            }
        });


//---------------------------------------------------New Code End--------------------------------------------------


        callFragment = new CallFragment();
        hudFragment = new HudFragment();

        // Show/hide call control fragment on view click.
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleCallControlFragmentVisibility();
            }
        };

        localRender.setOnClickListener(listener);
        remoteRenderScreen.setOnClickListener(listener);
        remoteRenderers.add(remoteRenderScreen);

        final Intent intent = getIntent();


        // Create video renderers.
        rootEglBase = EglBase.create();
        localRender.init(rootEglBase.getEglBaseContext(), null);
        String saveRemoteVideoToFile = intent.getStringExtra(EXTRA_SAVE_REMOTE_VIDEO_TO_FILE);

        // When saveRemoteVideoToFile is set we save the video from the remote to a file.
        if (saveRemoteVideoToFile != null) {
            int videoOutWidth = intent.getIntExtra(EXTRA_SAVE_REMOTE_VIDEO_TO_FILE_WIDTH, 0);
            int videoOutHeight = intent.getIntExtra(EXTRA_SAVE_REMOTE_VIDEO_TO_FILE_HEIGHT, 0);
            try {
                videoFileRenderer = new VideoFileRenderer(
                        saveRemoteVideoToFile, videoOutWidth, videoOutHeight, rootEglBase.getEglBaseContext());
                remoteRenderers.add(videoFileRenderer);
            } catch (IOException e) {
                throw new RuntimeException(
                        "Failed to open video file for output: " + saveRemoteVideoToFile, e);
            }
        }
        remoteRenderScreen.init(rootEglBase.getEglBaseContext(), null);

        localRender.setZOrderMediaOverlay(true);
        localRender.setEnableHardwareScaler(true /* enabled */);
        remoteRenderScreen.setEnableHardwareScaler(true /* enabled */);
        updateVideoView();

        // Check for mandatory permissions.
        for (String permission : MANDATORY_PERMISSIONS) {
            if (checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                logAndToast("Permission " + permission + " is not granted");
                setResult(RESULT_CANCELED);
                finish();
                return;
            }
        }
//Check uri exists


        Uri roomUri = intent.getData();

        if (roomUri == null) {
            logAndToast(getString(org.appspot.apprtc.R.string.missing_url));
            MyLog.e(TAG, "Didn't get any URL in intent!");
            setResult(RESULT_CANCELED);
            finish();
            return;
        }
        //TURN SERVER CODE
        if (isTurnServerEnabled) {
            roomUri = Uri.parse(WebrtcConstants.OWN_TURN_SERVER);
        }
        mCurrentUserId = SessionManager.getInstance(this).getCurrentUserID();

        mCallId = intent.getStringExtra(EXTRA_DOC_ID);
        fromUserId = intent.getStringExtra(EXTRA_FROM_USER_ID);
        toUserId = intent.getStringExtra(EXTRA_TO_USER_ID);
        Log.e(TAG, "fromUserId" + "fromUserId" + fromUserId + "mCurrentUserId" + mCurrentUserId + "toUserId" + toUserId + "opponentUserId" + opponentUserId);
        // Log.e("toUserId","toUserId"+toUserId);
        opponentUserId = intent.getStringExtra(EXTRA_TO_USER_ID);


        if (AppUtils.isEmpty(opponentUserId)) {
            opponentUserId = toUserId;
        }
        mCallTs = intent.getStringExtra(EXTRA_CALL_TIME_STAMP);
        isVideoCall = intent.getBooleanExtra(EXTRA_VIDEO_CALL, true);


        Getcontactname getcontactname = new Getcontactname(this);

        // For audio calls only
        if (!isVideoCall) {
            Outgoing_call_type = "0";


            buttons_call_container.setWeightSum(2);
            button_call_switch_camera.setVisibility(View.GONE);
            button_call_toggle_mic.setVisibility(View.VISIBLE);


            String profilePic = intent.getStringExtra(EXTRA_OPPONENT_PROFILE_PIC);

            if (fromUserId.equalsIgnoreCase(SessionManager.getInstance(this).getCurrentUserID())) {
                getcontactname.configProfilepic(ivProfilePic, toUserId, false, false, R.drawable.avatar_contact);


            } else {
                getcontactname.configProfilepic(ivProfilePic, fromUserId, false, false, R.drawable.avatar_contact);
            }
//            callFragment.setRootBackColor(ContextCompat.getColor(VideoCallActivity.this, R.color.colorPrimary));
        } else {
            Outgoing_call_type = "1";
            ivProfilePic.setVisibility(View.GONE);
            buttons_call_container.setWeightSum(3);
            button_call_switch_camera.setVisibility(View.VISIBLE);
            bottom_layout.setBackgroundColor(Color.TRANSPARENT);
            call_header.setBackgroundColor(Color.TRANSPARENT);

        }

        showInfoCtrls();

        msisdn = intent.getStringExtra(EXTRA_USER_MSISDN);
        String mCheckReconnecting = intent.getStringExtra(RECONNECTING);
        if (!AppUtils.isEmpty(mCheckReconnecting)) {
            if (mCheckReconnecting.equalsIgnoreCase("true")) {
                mReconnecting = true;
            }
        }
        int mCheckCallDuration = intent.getIntExtra(CALLDURATION, 0);
        if (mCheckCallDuration != 0) {
            callDuration = mCheckCallDuration;
        }
        if (fromUserId.equalsIgnoreCase(SessionManager.getInstance(this).getCurrentUserID())) {
            mCallerName = getcontactname.getSendername(toUserId, msisdn);
        } else {
            mCallerName = getcontactname.getSendername(fromUserId, msisdn);
        }
        tvName.setText(mCallerName);

        isOutgoingCall = getIntent().getBooleanExtra(EXTRA_IS_OUTGOING_CALL, false);
//Check Call Duration
        if (isOutgoingCall) {
            if (!mReconnecting) {
                tvDuration.setText("CALLING");
            } else {
                tvDuration.setText("Reconnecting...");
                Log.e("Reconnecting", "Reconnecting" + "Reconnecting" + mCallTs);

            }
            Log.e("mRecordId", "mRecordId" + mRecordId);
            startRetryCallConnect();
            // For send busy status to third user until opponent user pick the call
            IncomingCallActivity.isStarted = true;
            String callConnectedStatus = getIntent().getExtras().getString(EXTRA_CALL_CONNECT_STATUS, MessageFactory.CALL_IN_FREE + "");
            mPrevCallStatus = callConnectedStatus;
            setCallStatusText(callConnectedStatus);
            handleCallTimeout();
        } else {
            if (!mReconnecting) {
                tvDuration.setText("Connecting..");
            } else {
                tvDuration.setText("Reconnecting...");
                Log.e("Reconnecting", "Reconnecting" + "Reconnecting" + mCallTs);

            }
            // tvDuration.setText("Connecting..");
            tvCallStatus.setVisibility(View.GONE);
        }
        Log.e("mRecordId", "mRecordId" + mRecordId);

        // Get Intent parameters.
        mRoomId = intent.getStringExtra(EXTRA_ROOMID);
        MyLog.d(TAG, "Room ID: " + mRoomId);
        if (mRoomId == null || mRoomId.length() == 0) {
            logAndToast(getString(org.appspot.apprtc.R.string.missing_url));
            MyLog.e(TAG, "Incorrect room ID in intent!");
            setResult(RESULT_CANCELED);
            finish();
            return;
        }

        boolean loopback = intent.getBooleanExtra(EXTRA_LOOPBACK, false);
        boolean tracing = intent.getBooleanExtra(EXTRA_TRACING, false);

        int videoWidth = intent.getIntExtra(EXTRA_VIDEO_WIDTH, 0);
        int videoHeight = intent.getIntExtra(EXTRA_VIDEO_HEIGHT, 0);

        screencaptureEnabled = intent.getBooleanExtra(EXTRA_SCREENCAPTURE, false);
        // If capturing format is not specified for screencapture, use screen resolution.
        if (screencaptureEnabled && videoWidth == 0 && videoHeight == 0) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            WindowManager windowManager =
                    (WindowManager) getApplication().getSystemService(Context.WINDOW_SERVICE);
            windowManager.getDefaultDisplay().getRealMetrics(displayMetrics);
            videoWidth = displayMetrics.widthPixels;
            videoHeight = displayMetrics.heightPixels;
        }
        PeerConnectionClient.DataChannelParameters dataChannelParameters = null;
        if (intent.getBooleanExtra(EXTRA_DATA_CHANNEL_ENABLED, true)) {
            dataChannelParameters = new PeerConnectionClient.DataChannelParameters(intent.getBooleanExtra(EXTRA_ORDERED, true),
                    intent.getIntExtra(EXTRA_MAX_RETRANSMITS_MS, -1),
                    intent.getIntExtra(EXTRA_MAX_RETRANSMITS, -1), intent.getStringExtra(EXTRA_PROTOCOL),
                    intent.getBooleanExtra(EXTRA_NEGOTIATED, false), intent.getIntExtra(EXTRA_ID, -1));
        }
        peerConnectionParameters =
                new PeerConnectionClient.PeerConnectionParameters(intent.getBooleanExtra(EXTRA_VIDEO_CALL, true), loopback,
                        tracing, videoWidth, videoHeight, intent.getIntExtra(EXTRA_VIDEO_FPS, 0),
                        intent.getIntExtra(EXTRA_VIDEO_BITRATE, 0), intent.getStringExtra(EXTRA_VIDEOCODEC),
                        intent.getBooleanExtra(EXTRA_HWCODEC_ENABLED, true),
                        intent.getBooleanExtra(EXTRA_FLEXFEC_ENABLED, false),
                        intent.getIntExtra(EXTRA_AUDIO_BITRATE, 0), intent.getStringExtra(EXTRA_AUDIOCODEC),
                        intent.getBooleanExtra(EXTRA_NOAUDIOPROCESSING_ENABLED, false),
                        intent.getBooleanExtra(EXTRA_AECDUMP_ENABLED, false),
                        intent.getBooleanExtra(EXTRA_OPENSLES_ENABLED, false),
                        intent.getBooleanExtra(EXTRA_DISABLE_BUILT_IN_AEC, false),
                        intent.getBooleanExtra(EXTRA_DISABLE_BUILT_IN_AGC, false),
                        intent.getBooleanExtra(EXTRA_DISABLE_BUILT_IN_NS, false),
                        intent.getBooleanExtra(EXTRA_ENABLE_LEVEL_CONTROL, false), dataChannelParameters);
        commandLineRun = intent.getBooleanExtra(EXTRA_CMDLINE, false);
        runTimeMs = intent.getIntExtra(EXTRA_RUNTIME, 0);

        MyLog.d(TAG, "VIDEO_FILE: '" + intent.getStringExtra(EXTRA_VIDEO_FILE_AS_CAMERA) + "'");

        // Create connection client. Use DirectRTCClient if room name is an IP otherwise use the
        // standard WebSocketRTCClient.
        if (loopback || !DirectRTCClient.IP_PATTERN.matcher(mRoomId).matches()) {
            appRtcClient = new WebSocketRTCClient(this);
        } else {
            Log.i(TAG, "Using DirectRTCClient because room name looks like an IP.");
            appRtcClient = new DirectRTCClient(this);
        }
        // Create connection parameters.
        roomConnectionParameters = new AppRTCClient.RoomConnectionParameters(roomUri.toString(), mRoomId, loopback);


        if (CpuMonitor.isSupported()) {
            cpuMonitor = new CpuMonitor(this);
            hudFragment.setCpuMonitor(cpuMonitor);
        }
        // Send intent arguments to fragments.
        callFragment.setArguments(intent.getExtras());
        hudFragment.setArguments(intent.getExtras());
        // Activate call and HUD fragments and start the call.
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(org.appspot.apprtc.R.id.call_fragment_container, callFragment);
        ft.add(org.appspot.apprtc.R.id.hud_fragment_container, hudFragment);
        ft.commit();

        //Check it is Reconnecting or not

        // For command line execution run connection for <runTimeMs> and exit.
        if (commandLineRun && runTimeMs > 0) {
            (new Handler()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "disconnect" + "runTimeMs" + runTimeMs);

                    disconnect(false);
                }
            }, runTimeMs);
        }

        peerConnectionClient = PeerConnectionClient.getInstance();
        if (loopback) {
            PeerConnectionFactory.Options options = new PeerConnectionFactory.Options();
            options.networkIgnoreMask = 0;
            peerConnectionClient.setPeerConnectionFactoryOptions(options);
        }
        peerConnectionClient.createPeerConnectionFactory(
                CallsActivity.this, peerConnectionParameters, CallsActivity.this);

        if (screencaptureEnabled) {
            MediaProjectionManager mediaProjectionManager =
                    (MediaProjectionManager) getApplication().getSystemService(
                            Context.MEDIA_PROJECTION_SERVICE);
            startActivityForResult(
                    mediaProjectionManager.createScreenCaptureIntent(), CAPTURE_PERMISSION_REQUEST_CODE);
        } else {
            startCall();
        }


        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private void setCallStatusText(String callConnectedStatus) {

        if (mediaPlayer == null) {
//            if (callConnectedStatus.equals(MessageFactory.CALL_IN_FREE + "")) {
//                playCallerTone(R.raw.call_tone);
//            } else {
//                playCallerTone(R.raw.call_busy);
//            }
        } else if (!mPrevCallStatus.equals(callConnectedStatus)) {
//            if (callConnectedStatus.equals(MessageFactory.CALL_IN_FREE + "")) {
//                playCallerTone(R.raw.call_tone);
//            } else {
//                playCallerTone(R.raw.call_busy);
//            }
        }
        Log.e(TAG, "setCallStatusText" + callConnectedStatus);
        mPrevCallStatus = callConnectedStatus;

        switch (callConnectedStatus) {

            case MessageFactory.CALL_IN_FREE + "": {
                tvCallStatus.setVisibility(View.GONE);
                playCallerTone(R.raw.call_tone);
            }
            break;

            case MessageFactory.CALL_IN_RINGING + "": {
                tvCallStatus.setVisibility(View.VISIBLE);
                // tvCallStatus.setText(mCallerName + " busy");
                //playCallerTone(R.raw.call_busy);
            }
            break;
            case MessageFactory.CALL_IN_WAITING + "": {
                tvCallStatus.setVisibility(View.VISIBLE);
                // tvCallStatus.setText(mCallerName + " in another call");
                playCallerTone(R.raw.call_busy);
            }
            break;
        }
    }

    private void playCallerTone(int resId) {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        mediaPlayer = MediaPlayer.create(CallsActivity.this, resId);
        mediaPlayer.setLooping(true);
        setMediaVolume(40);
        mediaPlayer.start();
    }

    private void setMediaVolume(int volume) {
        AudioManager audio = (AudioManager) getSystemService(AUDIO_SERVICE);
        int currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
        int maxVolume = audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int volumePercent = (volume * maxVolume) / 100;
        audio.setStreamVolume(AudioManager.STREAM_MUSIC, volumePercent, 0);
    }

    private void startRetryCallConnect() {
        retryCallHandler = new Handler();
        retryCallRunnable = new Runnable() {
            @Override
            public void run() {
                if (!isArrivedToUser && !isConnectedToUser && !CallMessage.arrivedCallId.equalsIgnoreCase(mCallId)) {
                    long timeDiff = System.currentTimeMillis() - callStartedTimeMs;
                    if (mRecordId != null) {
                        if (timeDiff < 45 * 1000) {
                            try {
                                JSONObject object = new JSONObject();
                                object.put("from", mCurrentUserId);
                                object.put("to", toUserId);
                                object.put("recordId", mRecordId);

                                SendMessageEvent event = new SendMessageEvent();
                                event.setEventName(SocketManager.EVENT_RETRY_CALL_CONNECT);
                                event.setMessageObject(object);
                                Log.e("mRecordId", "mRecordId" + mRecordId);
                                EventBus.getDefault().post(event);
                            } catch (JSONException e) {
                                MyLog.e(TAG, "", e);
                            }


                            MessageDbController dbController = CoreController.getDBInstance(CallsActivity.this);
                            CallItemChat callItem = dbController.getCallStatus(mCallId);
                            if (callItem == null || callItem.getCallStatus().equals(MessageFactory.CALL_STATUS_CALLING + "")) {
                                retryCallHandler.postDelayed(retryCallRunnable, CALL_RETRY_DURATION);
                            }
                        }
                    }
                } else {
                    stopRetryCallConnect();
                }
            }
        };
        retryCallHandler.postDelayed(retryCallRunnable, CALL_RETRY_DURATION);
    }

    private void showInfoCtrls() {
        tvName.setVisibility(View.VISIBLE);
        tvCallLbl.setVisibility(View.VISIBLE);
        tvDuration.setVisibility(View.VISIBLE);
        // tvDuration.setText("");

        if (isVideoCall) {
            tvCallLbl.setText(getString(R.string.video_call_appname) + " VIDEO CALL");
        } else {
            tvCallLbl.setText(getString(R.string.video_call_appname) + " VOICE CALL");
        }

        if (isCallReconnecting) {
            //      tvDuration.setVisibility(View.INVISIBLE);
            //  tvCallStatus.setVisibility(View.VISIBLE);
            // tvCallStatus.setText("Reconnecting");
        }
    }

    private void hideInfoCtrls() {
        tvName.setVisibility(View.VISIBLE);
        tvCallLbl.setVisibility(View.GONE);
        tvDuration.setVisibility(View.VISIBLE);
        tvCallStatus.setVisibility(View.GONE);
    }

    private void handleCallTimeout() {
        callTimeoutHandler = new Handler();
        callTimeoutRunnable = new Runnable() {
            @Override
            public void run() {
                if (!iceConnected) {
                    Log.e(TAG, "disconnect" + " handleCallTimeout" + !iceConnected);

                    disconnect(true);
                }

            }
        };
        callTimeoutHandler.postDelayed(callTimeoutRunnable, MISSED_CALL_TIMEOUT);
    }

    private void disableLockScreen() {
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Activity.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
        lock.disableKeyguard();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != CAPTURE_PERMISSION_REQUEST_CODE)
            return;
        mediaProjectionPermissionResultCode = resultCode;
        mediaProjectionPermissionResultData = data;
        Log.e(TAG, "onActivityResult");
        startCall();
    }

    private boolean useCamera2() {
        return Camera2Enumerator.isSupported(this) && getIntent().getBooleanExtra(EXTRA_CAMERA2, true);
    }

    private boolean captureToTexture() {
        return getIntent().getBooleanExtra(EXTRA_CAPTURETOTEXTURE_ENABLED, false);
    }

    private VideoCapturer createCameraCapturer(CameraEnumerator enumerator) {
        final String[] deviceNames = enumerator.getDeviceNames();

        // First, try to find front facing camera
        Logging.d(TAG, "Looking for front facing cameras.");
        for (String deviceName : deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                Logging.d(TAG, "Creating front facing camera capturer.");
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        // Front facing camera not found, try something else
        Logging.d(TAG, "Looking for other cameras.");
        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                Logging.d(TAG, "Creating other camera capturer.");
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        return null;
    }

    // Activity interfaces
    @Override
    public void onPause() {
        super.onPause();
        //  CoreController.getInstance().unsetConnectivityListener();

        // If called while in PIP mode, do not pause playback
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (isInPictureInPictureMode()) {
                // Continue playback

            } else {
                // Use existing playback logic for paused Activity behavior.
                if (peerConnectionClient != null && !screencaptureEnabled) {
                    peerConnectionClient.stopVideoSource();

                }
            }
        } else {
            if (peerConnectionClient != null && !screencaptureEnabled) {
                peerConnectionClient.stopVideoSource();

            }
        }
        activityRunning = false;
        // Don't stop the video when using screencapture to allow user to show other apps to the remote
        // end.
        if (CpuMonitor.isSupported()) {
            cpuMonitor.pause();
        }
        String callDocId = "";
        String id = "";
        if (mCallId != null) {
            String[] splitIds = mCallId.split("-");
            id = splitIds[2];
            callDocId = toUserId + "-" + fromUserId + "-" + id;
        }
        //    mSensorManager.unregisterListener(this);


    /*    JSONObject object = CallMessage.getCallStatusObject(toUserId, fromUserId, id, callDocId, mRecordId, MessageFactory.CALL_STATUS_PAUSE);
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_CALL_STATUS);
        event.setMessageObject(object);
        EventBus.getDefault().post(event);*/
    }

    @Override
    public void onResume() {
        super.onResume();
//        registerCallReceiver();

        activityRunning = true;
        // if (activityRunning) {
        //     CoreController.getInstance().setConnectivityListener(this);
        //   }
        // Video is not paused for screencapture. See onPause.
        if (peerConnectionClient != null && !screencaptureEnabled) {
            peerConnectionClient.startVideoSource();
        }
        if (CpuMonitor.isSupported()) {

            cpuMonitor.resume();
        }
        //     mSensorManager.registerListener(this, mProximity, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    protected void onDestroy() {
        CoreController.getInstance().unsetConnectivityListener();

        //disconnect(false);
        //     networkStateReceiver.removeListener(mListenerr);
        //    this.unregisterReceiver(networkStateReceiver);
        opponentUserId = "";
        unregisterReceiver(incomingCallReceiver);

        stopRetryCallConnect();
        stopReconnectCall();
        SessionManager.getInstance(this).setIsScreenActivated(false);
        if (callTimeoutHandler != null && callTimeoutRunnable != null) {
            callTimeoutHandler.removeCallbacks(callTimeoutRunnable);
        }

        if (logToast != null) {
            logToast.cancel();
        }
        activityRunning = false;
        rootEglBase.release();

        isStarted = false;

        // For remove busy status once opponent user pick the call
        IncomingCallActivity.isStarted = false;

        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }


        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    // CallFragment.OnCallEvents interface implementation.
    @Override
    public void onCallHangUp() {
        Log.e(TAG, "onCallHangUp" + canEndCall);
        canEndCall = true;
        //
        // disconnect(true);
    }

    @Override
    public void onCameraSwitch() {
        if (peerConnectionClient != null) {
            peerConnectionClient.switchCamera();
            cameraswitched = !cameraswitched;
            updateVideoView();
        }
    }

    @Override
    public void onVideoScalingSwitch(RendererCommon.ScalingType scalingType) {
        this.scalingType = scalingType;
        updateVideoView();
    }

    @Override
    public void onCaptureFormatChange(int width, int height, int framerate) {
        if (peerConnectionClient != null) {
            peerConnectionClient.changeCaptureFormat(width, height, framerate);
        }
    }

    @Override
    public boolean onToggleMic() {
        if (peerConnectionClient != null) {
            micEnabled = !micEnabled;
            peerConnectionClient.setAudioEnabled(micEnabled);
        }
        return micEnabled;
    }

    @Override
    public boolean onToggleSpeaker() {

//        speakerEnabled = !speakerEnabled;

        if (audioManager.getSelectedAudioDevice().equals(AppRTCAudioManager.AudioDevice.EARPIECE)) {
            audioManager.setAudioDeviceInternal(AppRTCAudioManager.AudioDevice.SPEAKER_PHONE);
            setMediaVolume(80);
            speakerEnabled = true;
        } else {
            audioManager.setAudioDeviceInternal(AppRTCAudioManager.AudioDevice.EARPIECE);
            setMediaVolume(40);
            speakerEnabled = false;
        }

        return speakerEnabled;
    }

    @Override
    public void onPageClick() {
        if (isVideoCall) {
            if (tvName.getVisibility() == View.VISIBLE) {
                hideInfoCtrls();

            } else {
                showInfoCtrls();
            }
        }
    }

    @Override
    public void gotomsg() {
        onBackPressed();
    }

    // Helper functions.
    private void toggleCallControlFragmentVisibility() {
        if (!iceConnected || !callFragment.isAdded()) {
            return;
        }
        // Show/hide call control fragment
        callControlFragmentVisible = !callControlFragmentVisible;
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (callControlFragmentVisible) {
            disconnect_layout.setVisibility(View.VISIBLE);
            bottom_layout.setVisibility(View.VISIBLE);
            call_header.setVisibility(View.VISIBLE);

            ft.show(callFragment);
            ft.show(hudFragment);

        } else {

            disconnect_layout.setVisibility(View.GONE);
            bottom_layout.setVisibility(View.GONE);
            call_header.setVisibility(View.GONE);

            ft.hide(callFragment);
            ft.hide(hudFragment);
        }
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    private void updateVideoView() {
        try {
            Log.e(TAG, "updateVideoView" + cameraswitched + iceConnected);


            if (cameraswitched) {

                if (remoteRenderLayout != null) {
                    remoteRenderLayout.setPosition(REMOTE_X, REMOTE_Y, REMOTE_WIDTH, REMOTE_HEIGHT);
                }
                if (remoteRenderScreen != null) {
                    remoteRenderScreen.setScalingType(scalingType);
                    remoteRenderScreen.setMirror(false);
                }
                // For remove busy status once opponent user pick the call
                IncomingCallActivity.isStarted = false;


                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }

                if (iceConnected) {
                    isConnectedToUser = true;

                    boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                    if (is_gossip) {
                        arrow.setVisibility(View.VISIBLE);
                    }
                    mStartTimer = true;
                    startTimer();
                    Log.e(TAG, "iceConnected" + iceConnected + "startTimer" + isConnectedToUser);

//            startNotification();
                    if (localRenderLayout != null) {

                        localRenderLayout.setPosition(
                                LOCAL_X_CONNECTED, LOCAL_Y_CONNECTED, LOCAL_WIDTH_CONNECTED, LOCAL_HEIGHT_CONNECTED);
                    }
                    if (localRender != null) {

                        localRender.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
                    }
                } else {
                    if (localRenderLayout != null) {

                        localRenderLayout.setPosition(
                                LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING, LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING);
                    }
                    if (localRender != null) {
                        localRender.setScalingType(scalingType);
                    }
                }
                if (localRender != null) {

                    localRender.setMirror(false);
                    localRender.requestLayout();

                }
                if (remoteRenderScreen != null) {

                    remoteRenderScreen.requestLayout();
                }
            } else {
                if (remoteRenderLayout != null) {
                    remoteRenderLayout.setPosition(REMOTE_X, REMOTE_Y, REMOTE_WIDTH, REMOTE_HEIGHT);
                }
                if (remoteRenderScreen != null) {
                    remoteRenderScreen.setScalingType(scalingType);
                    remoteRenderScreen.setMirror(false);
                }
                // For remove busy status once opponent user pick the call
                IncomingCallActivity.isStarted = false;

                Log.e(TAG, "updateVideoView" + cameraswitched + iceConnected);

                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }

                if (iceConnected) {

                    isConnectedToUser = true;
                    mStartTimer = true;

                    startTimer();

                    Log.e(TAG, "iceConnected" + iceConnected + "startTimer" + isConnectedToUser);


                    //     final RegionView lRegionView = new RegionView(this);

                    // lRegionView.addView(lp);


                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) local_video_relativelayout.getLayoutParams();
                    lp.addRule(RelativeLayout.ABOVE, dummy_layout.getId());
//            startNotification();
                    if (localRenderLayout != null) {
                        localRenderLayout.setPosition(LOCAL_X_CONNECTED, LOCAL_Y_CONNECTED, LOCAL_WIDTH_CONNECTED, LOCAL_HEIGHT_CONNECTED);
                    }
                    if (localRender != null) {
                        localRender.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
                    }
                    //  local_video_relativelayout.addView(lRegionView);

                } else {

                    localRenderLayout.setPosition(
                            LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING, LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING);
                    if (localRender != null) {
                        localRender.setScalingType(scalingType);
                    }
                }

                if (localRender != null) {

                    localRender.setMirror(true);

                    localRender.requestLayout();
                }
                if (remoteRenderScreen != null) {

                    remoteRenderScreen.requestLayout();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "updateVideoView" + e.getMessage());
        }

    }

    @Override
    public void onUserLeaveHint() {
        if (isVideoCall) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (!isInPictureInPictureMode()) {
                    if (mPictureInPictureParamsBuilder == null) {
                        mPictureInPictureParamsBuilder = new PictureInPictureParams.Builder();
                    }
                    Rational aspectRatio = new Rational(remoteRenderScreen.getWidth(), remoteRenderScreen.getHeight());
                    mPictureInPictureParamsBuilder.setAspectRatio(aspectRatio).build();
                    enterPictureInPictureMode(mPictureInPictureParamsBuilder.build());

                }
            }
        }
    }

    private void startPictureInPictureFeature() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (mPictureInPictureParamsBuilder == null) {
                mPictureInPictureParamsBuilder = new PictureInPictureParams.Builder();
            }
            Rational aspectRatio = new Rational(remoteRenderScreen.getWidth(), remoteRenderScreen.getHeight());
            mPictureInPictureParamsBuilder.setAspectRatio(aspectRatio).build();
            enterPictureInPictureMode(mPictureInPictureParamsBuilder.build());
        }

    }

    private void startCall() {
        if (appRtcClient == null) {
            MyLog.e(TAG, "AppRTC client is not allocated for a call.");
            return;
        }
        callStartedTimeMs = System.currentTimeMillis();

        // Start room connection.
        logAndToast(getString(org.appspot.apprtc.R.string.connecting_to, roomConnectionParameters.roomUrl));
        appRtcClient.connectToRoom(roomConnectionParameters);

        // Create and audio manager that will take care of audio routing,
        // audio modes, audio DEVICE enumeration etc.
        audioManager = AppRTCAudioManager.create(this);
        // Store existing audio settings and change audio mode to
        // MODE_IN_COMMUNICATION for best possible VoIP performance.
        MyLog.d(TAG, "Starting the audio manager...");
        try {

            audioManager.start(new AppRTCAudioManager.AudioManagerEvents() {
                // This method will be called each time the number of available audio
                // devices has changed.
                @Override
                public void onAudioDeviceChanged(
                        AppRTCAudioManager.AudioDevice audioDevice, Set<AppRTCAudioManager.AudioDevice> availableAudioDevices) {
                    onAudioManagerDevicesChanged(audioDevice, availableAudioDevices);
                }
            });

        } catch (Exception e) {

        }
    }

    // Should be called from UI thread
    private void callConnected() {
        Log.e(TAG, "callConnected onIceConnected" + iceConnected + isVideoCall);

        if (isVideoCall) {
            //video call connected start picture mode
            //  startPictureInPictureFeature();
            hideInfoCtrls();
        } else {

        }

        stopReconnectCall();

        long delta = System.currentTimeMillis() - callStartedTimeMs;
        Log.e(TAG, "Call connected: delay=" + delta + "ms");

        if (peerConnectionClient == null || isError) {
            //Check peerConnectionClient
            if (peerConnectionClient != null) {
                Log.e(TAG, "peerConnectionClient not null" + peerConnectionClient);

            } else {
                Log.e(TAG, "peerConnectionClient  null");
                return;
            }
            Log.e(TAG, "Call is connected in closed or error state" + isError);


        }
        // Update video view.
        updateVideoView();
        // Enable statistics callback.
        peerConnectionClient.enableStatsEvents(true, STAT_CALLBACK_PERIOD);
    }

    // This method is called when the audio manager reports audio DEVICE change,
    // e.g. from wired headset to speakerphone.
    private void onAudioManagerDevicesChanged(
            final AppRTCAudioManager.AudioDevice device, final Set<AppRTCAudioManager.AudioDevice> availableDevices) {
        MyLog.d(TAG, "onAudioManagerDevicesChanged: " + availableDevices + ", "
                + "selected: " + device);
        // TODO(henrika): add callback handler.
    }

    // Disconnect from remote resources, dispose of local resources, and exit.
    private void disconnect(final boolean isMissedCall) {
        Log.e(TAG, "disconnect" + isMissedCall + "needToSendServer" + needToSendServer);
        CallMessage.isAlreadyCallClick = false;
        if (timer != null) {
            timer.cancel();
        }

        if (needToSendServer) {
            if (!isMissedCall) {
                sendCallDisconnectToServer(isMissedCall, true);
            } else {
                sendCallDisconnectToServer(isMissedCall, true);
            }
            needToSendServer = false;
        } else {
            MessageDbController db = CoreController.getDBInstance(this);
            db.updateCallStatus(getCallId(), MessageFactory.CALL_STATUS_END, getCallDuration());
        }

        activityRunning = false;
        if (appRtcClient != null) {
            appRtcClient.disconnectFromRoom();
            appRtcClient = null;
        }
        if (peerConnectionClient != null) {
            peerConnectionClient.close();
            peerConnectionClient = null;
        }
        if (localRender != null) {
            localRender.release();
            localRender = null;
        }
        if (videoFileRenderer != null) {
            videoFileRenderer.release();
            videoFileRenderer = null;
        }
        if (remoteRenderScreen != null) {
            remoteRenderScreen.release();
            remoteRenderScreen = null;
        }
        if (audioManager != null) {
            audioManager.stop();
            audioManager = null;
        }
        if (iceConnected && !isError) {
            setResult(RESULT_OK);
        } else {
            setResult(RESULT_CANCELED);
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isMissedCall)
                    if (showDisconnectNotify) {
                        //    Toast.makeText(CallsActivity.this, getString(R.string.call_disconnected), Toast.LENGTH_SHORT).show();
                    }
                finish();
            }
        }, 1000);

    }

    private void disconnectWithErrorMessage(final String errorMessage) {
        if (commandLineRun || !activityRunning) {
            MyLog.e(TAG, "Critical error: " + errorMessage);
            disconnect(false);
        } else {
           /* new AlertDialog.Builder(this)
                    .setTitle(getText(org.appspot.apprtc.R.string.channel_error_title))
                    .setMessage(errorMessage)
                    .setCancelable(false)
                    .setNeutralButton(org.appspot.apprtc.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    disconnect(false);
                                }
                            })
                    .create()
                    .show();*/

        }
    }

    // Log |msg| and Toast about it.
    private void logAndToast(String msg) {
        MyLog.d(TAG, msg);
        if (logToast != null) {
            logToast.cancel();
        }
//        logToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
//        logToast.show();
    }

    private void reportError(final String description) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isError) {
                    isError = true;
                    // disconnectWithErrorMessage(description);
                }
            }
        });
    }

    private VideoCapturer createVideoCapturer() {
        VideoCapturer videoCapturer = null;
        String videoFileAsCamera = getIntent().getStringExtra(EXTRA_VIDEO_FILE_AS_CAMERA);
        if (videoFileAsCamera != null) {
            try {
                videoCapturer = new FileVideoCapturer(videoFileAsCamera);
            } catch (IOException e) {
                reportError("Failed to open video file for emulated camera");
                return null;
            }
        } else if (screencaptureEnabled) {
            if (mediaProjectionPermissionResultCode != Activity.RESULT_OK) {
                reportError("User didn't give permission to capture the screen.");
                return null;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                return new ScreenCapturerAndroid(
                        mediaProjectionPermissionResultData, new MediaProjection.Callback() {
                    @Override
                    public void onStop() {
                        reportError("User revoked permission to capture the screen.");
                    }
                });
            }
        } else if (useCamera2()) {
            if (!captureToTexture()) {
                reportError(getString(org.appspot.apprtc.R.string.camera2_texture_only_error));
                return null;
            }

            Logging.d(TAG, "Creating capturer using camera2 API.");
            videoCapturer = createCameraCapturer(new Camera2Enumerator(this));
        } else {
            Logging.d(TAG, "Creating capturer using camera1 API.");
            videoCapturer = createCameraCapturer(new Camera1Enumerator(captureToTexture()));
        }
        if (videoCapturer == null) {
            reportError("Failed to open camera");
            return null;
        }
        return videoCapturer;
    }

    // -----Implementation of AppRTCClient.AppRTCSignalingEvents ---------------
    // All callbacks are invoked from websocket signaling looper thread and
    // are routed to UI thread.
    private void onConnectedToRoomInternal(final AppRTCClient.SignalingParameters params) {
        final long delta = System.currentTimeMillis() - callStartedTimeMs;

        signalingParameters = params;
        logAndToast("Creating peer connection, delay=" + delta + "ms");
        VideoCapturer videoCapturer = null;
        if (peerConnectionParameters.videoCallEnabled) {
            videoCapturer = createVideoCapturer();
        }
        peerConnectionClient.createPeerConnection(rootEglBase.getEglBaseContext(), localRender,
                remoteRenderers, videoCapturer, signalingParameters);

        if (signalingParameters.initiator) {
            logAndToast("Creating OFFER...");
            // Create offer. Offer SDP will be sent to answering client in
            // PeerConnectionEvents.onLocalDescription event.
            peerConnectionClient.createOffer();
        } else {
            if (params.offerSdp != null) {
                peerConnectionClient.setRemoteDescription(params.offerSdp);
                logAndToast("Creating ANSWER...");
                // Create answer. Answer SDP will be sent to offering client in
                // PeerConnectionEvents.onLocalDescription event.
                peerConnectionClient.createAnswer();
            }
            if (params.iceCandidates != null) {
                // Add remote ICE candidates from room.
                for (IceCandidate iceCandidate : params.iceCandidates) {
                    peerConnectionClient.addRemoteIceCandidate(iceCandidate);
                }
            }
        }
    }

    @Override
    public void onConnectedToRoom(final AppRTCClient.SignalingParameters params) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                onConnectedToRoomInternal(params);
            }
        });
    }

    @Override
    public void onRemoteDescription(final SessionDescription sdp) {
        final long delta = System.currentTimeMillis() - callStartedTimeMs;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (peerConnectionClient == null) {
                    MyLog.e(TAG, "Received remote SDP for non-initilized peer connection.");
                    return;
                }
                logAndToast("Received remote " + sdp.type + ", delay=" + delta + "ms");
                peerConnectionClient.setRemoteDescription(sdp);
                if (!signalingParameters.initiator) {
                    logAndToast("Creating ANSWER...");
                    // Create answer. Answer SDP will be sent to offering client in
                    // PeerConnectionEvents.onLocalDescription event.
                    peerConnectionClient.createAnswer();
                }

            }
        });
    }

    @Override
    public void onRemoteIceCandidate(final IceCandidate candidate) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (peerConnectionClient == null) {
                    MyLog.e(TAG, "Received ICE candidate for a non-initialized peer connection.");
                    return;
                }
                peerConnectionClient.addRemoteIceCandidate(candidate);
            }
        });
    }

    @Override
    public void onRemoteIceCandidatesRemoved(final IceCandidate[] candidates) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (peerConnectionClient == null) {
                    MyLog.e(TAG, "Received ICE candidate removals for a non-initialized peer connection.");
                    return;
                }
                peerConnectionClient.removeRemoteIceCandidates(candidates);
            }
        });
    }

    @Override
    public void onChannelClose() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                logAndToast("Remote end hung up; dropping PeerConnection");
                /*disconnect(false);
                showAlert(CallsActivity.this, "On channel close");*/
                Log.e("onChannelClose", "canEndCall" + canEndCall);
                Log.e("onChannelClose", "isNetworkConnected" + isNetworkConnected);
                Log.e("onChannelClose", "reconnectCall" + canEndCall);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isNetworkConnected) {
                            Log.e(TAG, "onChannelClose reconnectCall canEndCall" + canEndCall);

                            reconnectCall();
                        }
                    }
                }, 2000);
            }
        });
    }

    @Override
    public void onNetworkLostError(String description) {
        Log.e("description", "description" + description);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(CallsActivity.this, "Network is too slow,Please Try Aain...", Toast.LENGTH_SHORT).show();

                canEndCall = true;
                mDisconnectcall = true;
                disconnect(false);
            }
        }, 1000);

    }

    @Override
    public void onChannelError(final String description) {
        Log.e(TAG, "onChannelError" + description);
//Cancel timer and show reconnecting
        if (isOutgoingCall) {

            mStartTimer = false;
            cancelTimer();
            ShowReconnecting();
        }

        reportError(description);
    }

    //TURN SERVER CODE
    @Override
    public void turnServerMsgFromCaller(JSONObject jsonObject) {
        if (isTurnServerEnabled) {
            SendMessageEvent sendMessageEvent = new SendMessageEvent();
            sendMessageEvent.setEventName(SocketManager.EVENT_TURN_MESSAGE_FROM_CALLER);
            Log.e(TAG, "EVENT_TURN_MESSAGE_FROM_CALLER" + jsonObject);
            sendMessageEvent.setMessageObject(jsonObject);
            EventBus.getDefault().post(sendMessageEvent);
        }
    }

    //TURN SERVER CODE
    @Override
    public void turnServerMsg(String msg) {
        Log.e(TAG, "turnServerMsg" + isTurnServerEnabled);

        if (isTurnServerEnabled) {
            JSONObject object = new JSONObject();
            SendMessageEvent sendMessageEvent = new SendMessageEvent();
            sendMessageEvent.setEventName(SocketManager.EVENT_TURN_MESSAGE);

            try {
                object.put("from", mCurrentUserId);
                object.put("to", fromUserId);
                object.put("message", msg);
                Log.e(TAG, "EVENT_TURN_MESSAGE" + object);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            sendMessageEvent.setMessageObject(object);
            EventBus.getDefault().post(sendMessageEvent);
        }
    }

    // -----Implementation of PeerConnectionClient.PeerConnectionEvents.---------
    // Send local peer connection SDP and ICE candidates to remote party.
    // All callbacks are invoked from peer connection client looper thread and
    // are routed to UI thread.
    @Override
    public void onLocalDescription(final SessionDescription sdp) {


        final long delta = System.currentTimeMillis() - callStartedTimeMs;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (appRtcClient != null) {
                    logAndToast("Sending " + sdp.type + ", delay=" + delta + "ms");
                    if (signalingParameters.initiator) {
                        appRtcClient.sendOfferSdp(sdp);
                    } else {
                        appRtcClient.sendAnswerSdp(sdp);
                    }
                }
                if (peerConnectionParameters.videoMaxBitrate > 0) {
                    MyLog.d(TAG, "Set video maximum bitrate: " + peerConnectionParameters.videoMaxBitrate);
                    peerConnectionClient.setVideoMaxBitrate(peerConnectionParameters.videoMaxBitrate);
                }
            }
        });
        //https://raadsecure.com/
//Check Reconnecting is true or not
        if (isOutgoingCall) {

            //------------------------------Send Value to Opponent User------------------------------------

            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {


                    //    Log.e(TAG, "EVENT_CALL hANDLER");
                    JSONObject object = new JSONObject();
                    try {
                        Log.e(TAG, "EVENT_CALL hANDLER" + isOutgoingCall + "mReconnecting" + mReconnecting);

                        if (isOutgoingCall) {
                            object.put("from", mCurrentUserId);
                            if (mReconnecting) {
                                object.put("reconnecting", "true");
                            }
                            if (AppUtils.isEmpty(opponentUserId)) {
                                opponentUserId = toUserId;
                                Log.e(TAG, "EVENT_CALL opponentUserId" + opponentUserId + "toUserId" + toUserId);

                            }
                            object.put("to", opponentUserId);
                            object.put("type", Outgoing_call_type);
                            object.put("id", mRoomId);
                            object.put("roomid", mRoomId);
                            object.put("toDocId", mCurrentUserId + "-" + opponentUserId + "-" + mRoomId);
                        } else {
                            object.put("from", mCurrentUserId);


                            //   object.put("reconnecting", "true");
                            //  mReconnecting=false;
                            object.put("to", fromUserId);
                            object.put("type", Outgoing_call_type);
                            object.put("id", mRoomId);
                            object.put("roomid", mRoomId);
                            object.put("toDocId", mCurrentUserId + "-" + opponentUserId + "-" + mRoomId);
                        }

                        if (object != null) {
                            SendMessageEvent callEvent = new SendMessageEvent();
                            callEvent.setEventName(SocketManager.EVENT_CALL);
                            Log.e(TAG, isOutgoingCall + " EVENT_CALL sc_call" + object);
                            callEvent.setMessageObject(object);
                            EventBus.getDefault().post(callEvent);
                        }
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                }
            }, 3000);

        }
/*

        //------------------------------Send Value to Opponent User------------------------------------

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {


                //    Log.e(TAG, "EVENT_CALL hANDLER");
                JSONObject object = new JSONObject();
                try {
                    Log.e(TAG, "EVENT_CALL hANDLER" + isOutgoingCall + "mReconnecting" + mReconnecting);

                    if (isOutgoingCall) {
                        object.put("from", mCurrentUserId);
                        if (mReconnecting) {
                            object.put("reconnecting", "true");
                        }
                        if (AppUtils.isEmpty(opponentUserId)) {
                            opponentUserId = toUserId;
                            Log.e(TAG, "EVENT_CALL opponentUserId" + opponentUserId + "toUserId" + toUserId);

                        }
                        object.put("to", opponentUserId);
                        object.put("type", Outgoing_call_type);
                        object.put("id", mRoomId);
                        object.put("roomid", mRoomId);
                        object.put("toDocId", mCurrentUserId + "-" + opponentUserId + "-" + mRoomId);
                    } else {
                        object.put("from", mCurrentUserId);


                        //   object.put("reconnecting", "true");
                        //  mReconnecting=false;
                        object.put("to", fromUserId);
                        object.put("type", Outgoing_call_type);
                        object.put("id", mRoomId);
                        object.put("roomid", mRoomId);
                        object.put("toDocId", mCurrentUserId + "-" + opponentUserId + "-" + mRoomId);
                    }

                    if (object != null) {
                        SendMessageEvent callEvent = new SendMessageEvent();
                        callEvent.setEventName(SocketManager.EVENT_CALL);
                        Log.e(TAG, isOutgoingCall+" EVENT_CALL sc_call" + object);
                        callEvent.setMessageObject(object);
                        EventBus.getDefault().post(callEvent);
                    }
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }
        }, 3000);
*/


    }

    @Override
    public void onIceCandidate(final IceCandidate candidate) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (appRtcClient != null) {
                    appRtcClient.sendLocalIceCandidate(candidate);
                }
            }
        });
    }

    @Override
    public void onIceCandidatesRemoved(final IceCandidate[] candidates) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (appRtcClient != null) {
                    appRtcClient.sendLocalIceCandidateRemovals(candidates);
                }
            }
        });
    }

    @Override
    public void onIceConnected() {
        Log.e(TAG, "iceConnected onIceConnected" + iceConnected + "needToSendServer" + needToSendServer);
        iceConnected = true;
        Log.e(TAG, "iceConnected onIceConnected" + iceConnected + "needToSendServer" + needToSendServer);
        /*if (isOutgoingCall) {
            needToSendServer = true;
        }*/
        //Chekc Timer value and Reconntinue
        mStartTimer = true;
        final long delta = System.currentTimeMillis() - callStartedTimeMs;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                logAndToast("ICE connected, delay=" + delta + "ms");
                Log.e(TAG, "iceConnected onIceConnected" + iceConnected);

                iceConnected = true;
                // Log.e(TAG, "iceConnected onIceConnected" + iceConnected);

                boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                if (is_gossip) {
                    arrow.setVisibility(View.VISIBLE);
                }

              /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startPictureInPictureFeature();
                }*/
                callConnected();

            }
        });
    }

    @Override
    public void onIceDisconnected() {
        Log.e(TAG, "onIceDisconnected" + "isNetworkConnected" + isNetworkConnected + "canEndCall" + canEndCall);
//Check Reconnecting is true or false Sometimes sc_call status doesnt tigger


        cancelTimer();
        mStartTimer = false;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                logAndToast("ICE disconnected");


                iceConnected = false;
                Log.e(TAG, "isNetworkConnected" + "isNetworkConnected" + isNetworkConnected);

                //Check the network is disconnected
               /* if (isNetworkConnected) {
                    canEndCall = true;
                }else {
                 canEndCall = false;
                }*/


                Log.e(TAG, "onIceDisconnected" + "canEndCall" + canEndCall);

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {


                        if (canEndCall) {

                            disconnect(false);
                        } else {
                            if (isNetworkConnected) {
                                Log.e("onIceDisconnected", "reconnectCall" + canEndCall);
                                Log.e(TAG, "onIceDisconnected reconnectCall canEndCall" + canEndCall);

                                reconnectCall();
                            }

                            //             reconnectCall();
                        }
                    }
                }, 2000);
            }
        });
    }

    @Override
    public void onIceFailed() {
        //Check Network Connection if it and reconnect call for sender or reciever
        Log.e(TAG, "onIceFailed" + "canEndCall" + canEndCall);
        Log.e(TAG, "onIceFailed" + "isNetworkConnected" + isNetworkConnected);
        Log.e(TAG, "onIceFailed" + "isStarted" + IncomingCallActivity.isStarted);
        Log.e(TAG, "onIceFailed" + "isStarted" + CallsActivity.isStarted);
        Log.e(TAG, "onIceFailed" + "isOutgoingCall" + isOutgoingCall);
      /*  mReconnecting = true;
        if (isOutgoingCall) {
// It is sender //Trigeer Sc call and Reconnect
            if (ConnectivityReceiver.isConnected()) {

                // ShowReconnecting();
                DisconnectFromRoom();
                //Disconnect From Room Reconnet call


            } else {
                //Show reconnecting
                //  ShowReconnecting();
                DisconnectFromRoom();
            }
        } else {
            //It is receiver
            //  ShowReconnecting();
            DisconnectFromRoom();

        }*/
/*
        if (isOutgoingCall) {
// It is sender //Trigeer Sc call and Reconnect
            if (ConnectivityReceiver.isConnected()) {

                // ShowReconnecting();
                DisconnectFromRoom();
                //Disconnect From Room Reconnet call


            } else {
                //Show reconnecting
                //  ShowReconnecting();
                DisconnectFromRoom();
            }
        } else {
            //It is receiver
            //  ShowReconnecting();
            DisconnectFromRoom();

        }*/

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                logAndToast("onIceFailed");


                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        mReconnecting = true;
                        isNetworkConnected = false;

                        if (isOutgoingCall) {
// It is sender //Trigeer Sc call and Reconnect
                            if (ConnectivityReceiver.isConnected()) {

                                // ShowReconnecting();
                                DisconnectFromRoom();
                                //Disconnect From Room Reconnet call


                            } else {
                                //Show reconnecting
                                //  ShowReconnecting();
                                DisconnectFromRoom();
                            }
                        } else {
                            //It is receiver
                            //  ShowReconnecting();
                            DisconnectFromRoom();

                        }
                   /*     if (isOutgoingCall) {
// It is sender //Trigeer Sc call and Reconnect
                            if (ConnectivityReceiver.isConnected()) {

                                // ShowReconnecting();
                                DisconnectFromRoom();
                                //Disconnect From Room Reconnet call


                            } else {
                                //Show reconnecting
                                //  ShowReconnecting();
                                DisconnectFromRoom();
                            }
                        } else {
                            //It is receiver
                            //  ShowReconnecting();
                            DisconnectFromRoom();

                        }*/

                    }
                }, 2000);
            }
        });
    }

    public void ShowReconnecting() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvDuration.setText("Reconnecting...");
                tvDuration.setVisibility(View.VISIBLE);
                tvCallStatus.setText("");
            }
        });
    }

    @Override
    public void onPeerConnectionClosed() {
        Log.e(TAG, "onPeerConnectionClosed");

        //Check onNetworkChange is called
        Log.e(TAG, "onPeerConnectionClosed" + "canEndCall" + canEndCall);

        isNetworkConnected = false;
        Log.e(TAG, "onPeerConnectionClosed" + "isNetworkConnected" + isNetworkConnected);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                logAndToast("onIceFailed");


                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        //   if (isOutgoingCall) {
                        Log.e(TAG, "onPeerConnectionClosed" + "canEndCall" + canEndCall);
                        Log.e(TAG, "onPeerConnectionClosed" + "isNetworkConnected" + isNetworkConnected);

                        if (ConnectivityReceiver.isConnected()) {
                            mStartTimer = false;
                            cancelTimer();
                            if (!canEndCall) {
                                ShowReconnecting();
                                ReconnectInitimate();
                            }
                            if (isConnectedToUser) {
                                isConnectedToUser = false;
                            }

                        }
                        //   }
                        if (!canEndCall) {
                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    Log.e(TAG, "onPeerConnectionClosed Handler" + "canEndCall" + canEndCall);
                                    Log.e(TAG, "onPeerConnectionClosed Handler" + "isNetworkConnected" + isNetworkConnected);

                                    //CheckNetworkConnection is true

                                    if (ConnectivityReceiver.isConnected()) {
                                        //Check recevieer or sender
                                        if (!isOutgoingCall) {
                                            if (!isNetworkConnected) {
                                                Log.e(TAG, "onPeerConnectionClosed Handler" + "TriggerReconnectcalltosender" + isNetworkConnected);

                                                TriggerReconnectcalltosender();

                                            }
                                        }
                                    }
                                }
                            }, 4000);

                        }

                    }
                }, 2000);
            }
        });


    }

    @Override
    public void onPeerConnectionStatsReady(final StatsReport[] reports) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isError && iceConnected) {
                    hudFragment.updateEncoderStatistics(reports);
                }
            }
        });
    }

    @Override
    public void onPeerConnectionError(final String description) {
        reportError(description);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();


        if (wakeLock.isHeld()) {
            wakeLock.release();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CallDisconnect event) {
        Log.e(TAG, "disconnect" + "onMessageEvent" + event.toString());
        Log.e(TAG, "disconnect" + "getEventName" + event.getEventName().toString());

//Check it is  false
        if (event.getEventName().toString() != null && !event.getEventName().toString().isEmpty() && !event.getEventName().toString().equals("null")) {
            Log.e(TAG, "disconnect" + "getEventName not null");

            if (event.getEventName().equals("false")) {
                Log.e(TAG, "disconnect" + "getEventName false");

            }
        } else {
            disconnect(false);
            Log.e(TAG, "disconnect" + "disconnect  called");

        }

        //   disconnect(false);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {

        MyLog.d("Event_name", event.getEventName());

        switch (event.getEventName()) {
            case SocketManager.EVENT_CALL_STATUS_RESONSE: {

                if (!isOutgoingCall) {

                }
                String data = event.getObjectsArray()[0].toString();
                Log.e(TAG, "EVENT_CALL_STATUS_RESONSE" + data + "isOutgoingCall" + isOutgoingCall);

            }
            break;

            case SocketManager.EVENT_CALL_STATUS: {

                String data = event.getObjectsArray()[0].toString();
                loadCallStatusMessage(data);
                Log.e(TAG, "EVENT_CALL_STATUS sc_call_status");
            }
            break;

            case SocketManager.EVENT_RECONNECTINTIMATE: {

                String data = event.getObjectsArray()[0].toString();
                //  loadCallStatusMessage(data);
                Log.e(TAG, "EVENT_RECONNECTINTIMATE" + data);
                //   if (!isOutgoingCall){
                DisconnectFromRoom();
                // }
            }
            break;
            case SocketManager.EVENT_CALL: {

                String data = event.getObjectsArray()[0].toString();
                loadIncomingCallData(data);

                Log.e(TAG, "EVENT_CALL EVENT_CALL");
            }

            case SocketManager.EVENT_CALL_RECONNECT: {


                final String data = event.getObjectsArray()[0].toString();
                //   loadCallStatusMessage(data);
                //Triger sc_call

                //    ReConnectCall();
                Log.e(TAG, "EVENT_CALL_RECONNECT  EVENT_CALL_RECONNECT" + data + "canEndCall" + canEndCall);
//Check there is an incoming call from opponennt
                if (!CallsActivity.isStarted && !IncomingCallActivity.isStarted) {

                }
                Log.e(TAG, " EVENT_CALL_RECONNECT CallsActivity.isStarted  " + CallsActivity.isStarted + "IncomingCallActivity.isStarted" + IncomingCallActivity.isStarted);

                if (isOutgoingCall) {
                    mHandlerr = new Handler();

                    mHandlerr.postDelayed(mRunnable = new Runnable() {
                        @Override
                        public void run() {


                            if (!AppUtils.isServiceRunning(mContext, MessageService.class)) {
                                AppUtils.startService(mContext, MessageService.class);

                                Log.e("isServiceRunning", "isServiceRunning not running");

                                mHandlerr.postDelayed(mRunnable, 1000);

                            } else {
                                if (MessageService.manager != null && MessageService.manager.isConnected()) {

                                    mHandlerr.removeCallbacksAndMessages(null);
                                    //    ReconnectRoom();
                                    Log.e("ReconnectRoom", "ReconnectRoom Intent called ");
//Check it is reconnecting


                                    try {
                                        final JSONObject callObj = new JSONObject(data);

                                        if (callObj.has("reconnecting")) {
                                            String reconnecting = callObj.getString("reconnecting");
                                            Log.e("reconnecting", "reconnecting Intent called " + reconnecting);


                                            if (reconnecting.equals("false")) {


                                            } else {
                                                if (isOutgoingCall) {
                                                    long mIncrementmRoomId = Long.valueOf(mRoomId);
                                                    mIncrementmRoomId = mIncrementmRoomId + 1;
                                                    Log.e("mIncrementRecordId", "mIncrementmRoomId" + mIncrementmRoomId);

                                                    mRoomId = String.valueOf(mIncrementmRoomId);
                                                    Log.e("mRoomId", "mIncrementmRoomId" + mRoomId);

                                                }


                                                String roomUrlDefault = mContext.getString(org.appspot.apprtc.R.string.pref_room_server_url_default);
                                                if (isTurnServerEnabled)
                                                    roomUrlDefault = WebrtcConstants.OWN_TURN_SERVER;

                                                Uri uri = Uri.parse(roomUrlDefault);
                                                Intent intent = new Intent(mContext, CallsActivity.class);

                                                intent.setData(uri);
                                                intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, msisdn);

                                                intent.putExtra(CallsActivity.CALLDURATION, callDuration);

                                                intent.putExtra(CallsActivity.RECONNECTING, "true");

                                                intent.putExtra(CallsActivity.EXTRA_IS_OUTGOING_CALL, isOutgoingCall);
                                                intent.putExtra(CallsActivity.EXTRA_DOC_ID, mCallId);
                                                intent.putExtra(CallsActivity.EXTRA_FROM_USER_ID, fromUserId);
                                                intent.putExtra(CallsActivity.EXTRA_TO_USER_ID, toUserId);
                                                // intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, msisdn);
                                                //   intent.putExtra(CallsActivity.EXTRA_OPPONENT_PROFILE_PIC, opponentUserProfilePic);
                                                intent.putExtra(CallsActivity.EXTRA_NAVIGATE_FROM, mContext.getClass().getSimpleName()); // For navigating from call activity
                                                intent.putExtra(CallsActivity.EXTRA_CALL_CONNECT_STATUS, mPrevCallStatus);
                                                intent.putExtra(CallsActivity.EXTRA_CALL_TIME_STAMP, mCallTs);

                                                intent.putExtra(CallsActivity.EXTRA_ROOMID, mRoomId);
                                                intent.putExtra(CallsActivity.EXTRA_LOOPBACK, false);
                                                intent.putExtra(CallsActivity.EXTRA_VIDEO_CALL, isVideoCall);
                                                intent.putExtra(CallsActivity.EXTRA_SCREENCAPTURE, false);
                                                intent.putExtra(CallsActivity.EXTRA_CAMERA2, true);
                                                //    intent.putExtra(CallsActivity.EXTRA_VIDEO_WIDTH, videoWidth);
                                                //     intent.putExtra(CallsActivity.EXTRA_VIDEO_HEIGHT, videoHeight);


                                                Log.e(TAG, "EXTRA_IS_OUTGOING_CALL" + isOutgoingCall);

                                                Log.e(TAG, "EXTRA_DOC_ID" + mCallId);

                                                Log.e(TAG, "EXTRA_FROM_USER_ID" + fromUserId);

                                                Log.e(TAG, "EXTRA_TO_USER_ID" + toUserId);
                                                Log.e(TAG, "EXTRA_CALL_TIME_STAMP" + mCallTs);

                                                //  Log.e(TAG,"EXTRA_USER_MSISDN"+msisdn);
                                                Log.e(TAG, "EXTRA_CALL_CONNECT_STATUS" + mPrevCallStatus);

                                                Log.e(TAG, "EXTRA_ROOMID" + mRoomId);
                                                intent.putExtra(CallsActivity.EXTRA_VIDEO_FPS, 0);
                                                intent.putExtra(CallsActivity.EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED, false);
                                                intent.putExtra(CallsActivity.EXTRA_VIDEO_BITRATE, 0);
                                                intent.putExtra(CallsActivity.EXTRA_VIDEOCODEC, mContext.getString(org.appspot.apprtc.R.string.pref_videocodec_default));
                                                intent.putExtra(CallsActivity.EXTRA_HWCODEC_ENABLED, false);
                                                intent.putExtra(CallsActivity.EXTRA_CAPTURETOTEXTURE_ENABLED, true);
                                                intent.putExtra(CallsActivity.EXTRA_FLEXFEC_ENABLED, false);
                                                intent.putExtra(CallsActivity.EXTRA_NOAUDIOPROCESSING_ENABLED, false);
                                                intent.putExtra(CallsActivity.EXTRA_AECDUMP_ENABLED, false);

                                                intent.putExtra(CallsActivity.EXTRA_OPENSLES_ENABLED, false);
                                                intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AEC, false);
                                                intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AGC, false);
                                                intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_NS, false);
                                                intent.putExtra(CallsActivity.EXTRA_ENABLE_LEVEL_CONTROL, false);
                                                intent.putExtra(CallsActivity.EXTRA_AUDIO_BITRATE, 0);
                                                intent.putExtra(CallsActivity.EXTRA_AUDIOCODEC, mContext.getString(org.appspot.apprtc.R.string.pref_audiocodec_default));
                                                intent.putExtra(CallsActivity.EXTRA_DISPLAY_HUD, false);
                                                intent.putExtra(CallsActivity.EXTRA_TRACING, false);
                                                intent.putExtra(CallsActivity.EXTRA_CMDLINE, false);
                                                intent.putExtra(CallsActivity.EXTRA_RUNTIME, 0);

                                                intent.putExtra(CallActivity.EXTRA_DATA_CHANNEL_ENABLED, true);
                                                intent.putExtra(CallActivity.EXTRA_ORDERED, true);
                                                intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS_MS, -1);
                                                intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS, -1);
                                                intent.putExtra(CallActivity.EXTRA_PROTOCOL, mContext.getString(org.appspot.apprtc.R.string.pref_data_protocol_default));
                                                intent.putExtra(CallActivity.EXTRA_NEGOTIATED, false);
                                                intent.putExtra(CallActivity.EXTRA_ID, -1);

                                                mActivity.finish();
                                                overridePendingTransition(0, 0);

                                                //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                mContext.startActivity(intent);

                                                overridePendingTransition(0, 0);

                                            }
                                        }

                                    } catch (Exception e) {

                                    }
                                  /*  if (isOutgoingCall) {
                                        long mIncrementmRoomId = Long.valueOf(mRoomId);
                                        mIncrementmRoomId = mIncrementmRoomId + 1;
                                        Log.e("mIncrementRecordId", "mIncrementmRoomId" + mIncrementmRoomId);

                                        mRoomId = String.valueOf(mIncrementmRoomId);
                                        Log.e("mRoomId", "mIncrementmRoomId" + mRoomId);

                                    }


                                    String roomUrlDefault = mContext.getString(org.appspot.apprtc.R.string.pref_room_server_url_default);
                                    if (isTurnServerEnabled)
                                        roomUrlDefault = WebrtcConstants.OWN_TURN_SERVER;

                                    Uri uri = Uri.parse(roomUrlDefault);
                                    Intent intent = new Intent(mContext, CallsActivity.class);

                                    intent.setData(uri);
                                    intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, msisdn);

                                    intent.putExtra(CallsActivity.CALLDURATION, callDuration);

                                    intent.putExtra(CallsActivity.RECONNECTING, "true");

                                    intent.putExtra(CallsActivity.EXTRA_IS_OUTGOING_CALL, isOutgoingCall);
                                    intent.putExtra(CallsActivity.EXTRA_DOC_ID, mCallId);
                                    intent.putExtra(CallsActivity.EXTRA_FROM_USER_ID, fromUserId);
                                    intent.putExtra(CallsActivity.EXTRA_TO_USER_ID, toUserId);
                                    // intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, msisdn);
                                    //   intent.putExtra(CallsActivity.EXTRA_OPPONENT_PROFILE_PIC, opponentUserProfilePic);
                                    intent.putExtra(CallsActivity.EXTRA_NAVIGATE_FROM, mContext.getClass().getSimpleName()); // For navigating from call activity
                                    intent.putExtra(CallsActivity.EXTRA_CALL_CONNECT_STATUS, mPrevCallStatus);
                                    intent.putExtra(CallsActivity.EXTRA_CALL_TIME_STAMP, mCallTs);

                                    intent.putExtra(CallsActivity.EXTRA_ROOMID, mRoomId);
                                    intent.putExtra(CallsActivity.EXTRA_LOOPBACK, false);
                                    intent.putExtra(CallsActivity.EXTRA_VIDEO_CALL, isVideoCall);
                                    intent.putExtra(CallsActivity.EXTRA_SCREENCAPTURE, false);
                                    intent.putExtra(CallsActivity.EXTRA_CAMERA2, true);
                                    //    intent.putExtra(CallsActivity.EXTRA_VIDEO_WIDTH, videoWidth);
                                    //     intent.putExtra(CallsActivity.EXTRA_VIDEO_HEIGHT, videoHeight);


                                    Log.e(TAG, "EXTRA_IS_OUTGOING_CALL" + isOutgoingCall);

                                    Log.e(TAG, "EXTRA_DOC_ID" + mCallId);

                                    Log.e(TAG, "EXTRA_FROM_USER_ID" + fromUserId);

                                    Log.e(TAG, "EXTRA_TO_USER_ID" + toUserId);
                                    Log.e(TAG, "EXTRA_CALL_TIME_STAMP" + mCallTs);

                                    //  Log.e(TAG,"EXTRA_USER_MSISDN"+msisdn);
                                    Log.e(TAG, "EXTRA_CALL_CONNECT_STATUS" + mPrevCallStatus);

                                    Log.e(TAG, "EXTRA_ROOMID" + mRoomId);
                                    intent.putExtra(CallsActivity.EXTRA_VIDEO_FPS, 0);
                                    intent.putExtra(CallsActivity.EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED, false);
                                    intent.putExtra(CallsActivity.EXTRA_VIDEO_BITRATE, 0);
                                    intent.putExtra(CallsActivity.EXTRA_VIDEOCODEC, mContext.getString(org.appspot.apprtc.R.string.pref_videocodec_default));
                                    intent.putExtra(CallsActivity.EXTRA_HWCODEC_ENABLED, false);
                                    intent.putExtra(CallsActivity.EXTRA_CAPTURETOTEXTURE_ENABLED, true);
                                    intent.putExtra(CallsActivity.EXTRA_FLEXFEC_ENABLED, false);
                                    intent.putExtra(CallsActivity.EXTRA_NOAUDIOPROCESSING_ENABLED, false);
                                    intent.putExtra(CallsActivity.EXTRA_AECDUMP_ENABLED, false);

                                    intent.putExtra(CallsActivity.EXTRA_OPENSLES_ENABLED, false);
                                    intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AEC, false);
                                    intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AGC, false);
                                    intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_NS, false);
                                    intent.putExtra(CallsActivity.EXTRA_ENABLE_LEVEL_CONTROL, false);
                                    intent.putExtra(CallsActivity.EXTRA_AUDIO_BITRATE, 0);
                                    intent.putExtra(CallsActivity.EXTRA_AUDIOCODEC, mContext.getString(org.appspot.apprtc.R.string.pref_audiocodec_default));
                                    intent.putExtra(CallsActivity.EXTRA_DISPLAY_HUD, false);
                                    intent.putExtra(CallsActivity.EXTRA_TRACING, false);
                                    intent.putExtra(CallsActivity.EXTRA_CMDLINE, false);
                                    intent.putExtra(CallsActivity.EXTRA_RUNTIME, 0);

                                    intent.putExtra(CallActivity.EXTRA_DATA_CHANNEL_ENABLED, true);
                                    intent.putExtra(CallActivity.EXTRA_ORDERED, true);
                                    intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS_MS, -1);
                                    intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS, -1);
                                    intent.putExtra(CallActivity.EXTRA_PROTOCOL, mContext.getString(org.appspot.apprtc.R.string.pref_data_protocol_default));
                                    intent.putExtra(CallActivity.EXTRA_NEGOTIATED, false);
                                    intent.putExtra(CallActivity.EXTRA_ID, -1);

                                    mActivity.finish();
                                    overridePendingTransition(0, 0);

                                    //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    mContext.startActivity(intent);

                                    overridePendingTransition(0, 0);
                             */
                                } else {
                                    mHandlerr.postDelayed(mRunnable, 4000);
                                    MessageService.manager.connect();
                                }
                            }
                        }
                    }, 1000);
                }
            }

            break;
            case SocketManager.EVENT_DISCONNECT_CALL: {
                String data = event.getObjectsArray()[0].toString();
                loadCallStatusMessage(data);
            }
            break;

            case SocketManager.EVENT_CALL_RESPONSE: {
                String data = event.getObjectsArray()[0].toString();

                Log.e(TAG, "needToSendServer" + needToSendServer + "EVENT_CALL_RESPONSE" + data);
                if (isOutgoingCall) {
                    if (!needToSendServer) {
                        needToSendServer = true;
                    }
                }
                calldisconnect(data);
            }
            break;

            case SocketManager.EVENT_RETRY_CALL_CONNECT: {
                String data = event.getObjectsArray()[0].toString();

                loadCallRetryResponse(data);
            }
            break;

            //TURN SERVER CODE
            case SocketManager.EVENT_TURN_MESSAGE: {
                try {
                    if (isTurnServerEnabled) {
                        event.getObjectsArray()[0] = SocketManager.getDecryptedMessage(CallsActivity.this, (event.getObjectsArray()[0].toString()), event.getEventName());
                        JSONObject jsonObject = new JSONObject(event.getObjectsArray()[0].toString());
                        Log.e(TAG, "EVENT_TURN_MESSAGE response" + jsonObject);

                        String toUserId = jsonObject.getString("to");
                        if (toUserId.equals(SessionManager.getInstance(this).getCurrentUserID())) {
                            Log.e(TAG, "EVENT_TURN_MESSAGE equals" + jsonObject);
                            String msg = jsonObject.optString("message");
                            if (appRtcClient != null) {
                                Log.e(TAG, "appRtcClient not null");
                                if (msg != null) {
                                    Log.e(TAG, "appRtcClient  msg not null");

                                    appRtcClient.onReceiveTurnMessage(msg);
                                } else {
                                    Log.e(TAG, "appRtcClient msg is null");

                                }
                            } else {
                                Log.e(TAG, "appRtcClient null");
                            }
                        }
                    }
                    //  Log.e(TAG, "EVENT_TURN_MESSAGE response" + WebrtcConstants.isTurnServerEnabled);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            break;


        }
    }

    private void loadIncomingCallData(String data) {


        Log.e(TAG, "loadIncomingCallData" + data);
        String uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            final JSONObject callObj = new JSONObject(data);
            String from = callObj.getString("from");
            String id = callObj.getString("id");
            IncomingMessage incomingMsg = new IncomingMessage(mContext);


            MyLog.e(TAG, "loadIncomingCallData CallsActivity.isStarted" + CallsActivity.isStarted + "IncomingCallActivity.isStarted" + IncomingCallActivity.isStarted);
            String reconnecting = "";
            final String to = callObj.getString("to");
            String callStatus = callObj.getString("call_status");
            final String Room_id = "" + callObj.optString("roomid");
            String recordId = callObj.getString("recordId");

          /*  if (AppUtils.isEmpty(mRecordId)){
                if ( callObj.has("recordId")){
                    mRecordId=callObj.getString("recordId");
                    Log.e(TAG, "loadIncomingCallData isEmpty mRecordId"+mRecordId);
                }
            }else {
                Log.e(TAG, "loadIncomingCallData  not isEmpty mRecordId"+mRecordId);

            }*/
            if (to.equalsIgnoreCase(uniqueCurrentID) && callStatus.equals(MessageFactory.CALL_STATUS_CALLING + "")) {
                final CallItemChat callItem = incomingMsg.loadIncomingCall(callObj);
                boolean isVideoCall = false;
                if (callItem.getCallType().equals(MessageFactory.video_call + "")) {
                    isVideoCall = true;
                }
                if (!CallsActivity.isStarted && !IncomingCallActivity.isStarted) {
                    if (callObj.has("reconnecting")) {
                        reconnecting = callObj.getString("reconnecting");

                        if (reconnecting.equals("true")) {
                            final String ts = callObj.getString("timestamp");


                            String[] splitIds = callItem.getCallId().split("-");
                            String mId = splitIds[2];
                            String callDocId = to + "-" + callItem.getOpponentUserId() + "-" + mId;
                            String type = "" + MessageFactory.audio_call;
                            if (isVideoCall)
                                type = "" + MessageFactory.video_call;

                            JSONObject object = CallMessage.getCallStatusObject(uniqueCurrentID, callItem.getOpponentUserId(),
                                    mId, callDocId, recordId, MessageFactory.CALL_STATUS_ANSWERED, type);


                            SendMessageEvent event = new SendMessageEvent();

                            event.setEventName(SocketManager.EVENT_CALL_STATUS);
                            event.setMessageObject(object);
                            EventBus.getDefault().post(event);


                            final boolean finalIsVideoCall = isVideoCall;

                            //Trigger the Same Screen

/*
                           CallsActivity.mActivity.finish();

                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    CallMessage.openCallScreen(mContext, callItem.getOpponentUserId(), to, callItem.getCallId(),
                                            Room_id, "", callItem.getOpponentUserMsisdn(), MessageFactory.CALL_IN_FREE + "",
                                            finalIsVideoCall, false, ts, "true");

                                    // finish();
                                }
                            }, 1000);*/

/*

                            intent.putExtra(CallsActivity.EXTRA_DOC_ID, id);
                            intent.putExtra(CallsActivity.EXTRA_FROM_USER_ID, from);
                            intent.putExtra(CallsActivity.EXTRA_TO_USER_ID, to);
                            intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, msisdn);
                            intent.putExtra(CallsActivity.EXTRA_OPPONENT_PROFILE_PIC, opponentUserProfilePic);
                            intent.putExtra(CallsActivity.EXTRA_NAVIGATE_FROM, context.getClass().getSimpleName()); // For navigating from call activity
                            intent.putExtra(CallsActivity.EXTRA_CALL_CONNECT_STATUS, callConnect);
                            intent.putExtra(CallsActivity.EXTRA_CALL_TIME_STAMP, ts);

                            intent.putExtra(CallsActivity.EXTRA_ROOMID, roomId);

*/


                            String roomUrlDefault = mContext.getString(org.appspot.apprtc.R.string.pref_room_server_url_default);
                            if (isTurnServerEnabled)
                                roomUrlDefault = WebrtcConstants.OWN_TURN_SERVER;

                            Uri uri = Uri.parse(roomUrlDefault);
                            Intent intent = new Intent(mContext, CallsActivity.class);

                            intent.setData(uri);


                            intent.putExtra(CallsActivity.CALLDURATION, callDuration);

                            intent.putExtra(CallsActivity.RECONNECTING, "true");

                            intent.putExtra(CallsActivity.EXTRA_IS_OUTGOING_CALL, false);
                            intent.putExtra(CallsActivity.EXTRA_DOC_ID, callItem.getCallId());
                            intent.putExtra(CallsActivity.EXTRA_FROM_USER_ID, callItem.getOpponentUserId());
                            intent.putExtra(CallsActivity.EXTRA_TO_USER_ID, to);
                            // intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, msisdn);
                            //   intent.putExtra(CallsActivity.EXTRA_OPPONENT_PROFILE_PIC, opponentUserProfilePic);
                            //   intent.putExtra(CallsActivity.EXTRA_NAVIGATE_FROM, mContext.getClass().getSimpleName()); // For navigating from call activity
                            intent.putExtra(CallsActivity.EXTRA_CALL_CONNECT_STATUS, MessageFactory.CALL_IN_FREE + "");
                            intent.putExtra(CallsActivity.EXTRA_CALL_TIME_STAMP, ts);
                            intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, msisdn);
                            intent.putExtra(CallsActivity.EXTRA_VIDEO_CALL, finalIsVideoCall);

                            intent.putExtra(CallsActivity.EXTRA_ROOMID, Room_id);
                            intent.putExtra(CallsActivity.EXTRA_LOOPBACK, false);
                            intent.putExtra(CallsActivity.EXTRA_SCREENCAPTURE, false);
                            intent.putExtra(CallsActivity.EXTRA_CAMERA2, true);
                            //    intent.putExtra(CallsActivity.EXTRA_VIDEO_WIDTH, videoWidth);
                            //     intent.putExtra(CallsActivity.EXTRA_VIDEO_HEIGHT, videoHeight);


                            Log.e(TAG, "EXTRA_IS_OUTGOING_CALL" + isOutgoingCall);

                            Log.e(TAG, "EXTRA_DOC_ID" + mCallId);

                            Log.e(TAG, "EXTRA_FROM_USER_ID" + fromUserId);

                            Log.e(TAG, "EXTRA_TO_USER_ID" + toUserId);
                            Log.e(TAG, "EXTRA_CALL_TIME_STAMP" + mCallTs);

                            //  Log.e(TAG,"EXTRA_USER_MSISDN"+msisdn);
                            Log.e(TAG, "EXTRA_CALL_CONNECT_STATUS" + mPrevCallStatus);

                            Log.e(TAG, "EXTRA_ROOMID" + mRoomId);
                            intent.putExtra(CallsActivity.EXTRA_VIDEO_FPS, 0);
                            intent.putExtra(CallsActivity.EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED, false);
                            intent.putExtra(CallsActivity.EXTRA_VIDEO_BITRATE, 0);
                            intent.putExtra(CallsActivity.EXTRA_VIDEOCODEC, mContext.getString(org.appspot.apprtc.R.string.pref_videocodec_default));
                            intent.putExtra(CallsActivity.EXTRA_HWCODEC_ENABLED, false);
                            intent.putExtra(CallsActivity.EXTRA_CAPTURETOTEXTURE_ENABLED, true);
                            intent.putExtra(CallsActivity.EXTRA_FLEXFEC_ENABLED, false);
                            intent.putExtra(CallsActivity.EXTRA_NOAUDIOPROCESSING_ENABLED, false);
                            intent.putExtra(CallsActivity.EXTRA_AECDUMP_ENABLED, false);

                            intent.putExtra(CallsActivity.EXTRA_OPENSLES_ENABLED, false);
                            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AEC, false);
                            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AGC, false);
                            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_NS, false);
                            intent.putExtra(CallsActivity.EXTRA_ENABLE_LEVEL_CONTROL, false);
                            intent.putExtra(CallsActivity.EXTRA_AUDIO_BITRATE, 0);
                            intent.putExtra(CallsActivity.EXTRA_AUDIOCODEC, mContext.getString(org.appspot.apprtc.R.string.pref_audiocodec_default));
                            intent.putExtra(CallsActivity.EXTRA_DISPLAY_HUD, false);
                            intent.putExtra(CallsActivity.EXTRA_TRACING, false);
                            intent.putExtra(CallsActivity.EXTRA_CMDLINE, false);
                            intent.putExtra(CallsActivity.EXTRA_RUNTIME, 0);

                            intent.putExtra(CallActivity.EXTRA_DATA_CHANNEL_ENABLED, true);
                            intent.putExtra(CallActivity.EXTRA_ORDERED, true);
                            intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS_MS, -1);
                            intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS, -1);
                            intent.putExtra(CallActivity.EXTRA_PROTOCOL, mContext.getString(org.appspot.apprtc.R.string.pref_data_protocol_default));
                            intent.putExtra(CallActivity.EXTRA_NEGOTIATED, false);
                            intent.putExtra(CallActivity.EXTRA_ID, -1);

                            mActivity.finish();
                            overridePendingTransition(0, 0);

                            //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);

                            overridePendingTransition(0, 0);
                        }
                    }

                } else {
                    if (callObj.has("reconnecting")) {


                        reconnecting = callObj.getString("reconnecting");

                        Log.e("reconnecting", "reconnecting" + reconnecting);
                        if (reconnecting.equals("true")) {
                            final String ts = callObj.getString("timestamp");


                            String[] splitIds = callItem.getCallId().split("-");
                            String mId = splitIds[2];
                            String callDocId = to + "-" + callItem.getOpponentUserId() + "-" + mId;
                            String type = "" + MessageFactory.audio_call;
                            if (isVideoCall)
                                type = "" + MessageFactory.video_call;

                            JSONObject object = CallMessage.getCallStatusObject(uniqueCurrentID, callItem.getOpponentUserId(),
                                    mId, callDocId, recordId, MessageFactory.CALL_STATUS_ANSWERED, type);


                            SendMessageEvent event = new SendMessageEvent();

                            event.setEventName(SocketManager.EVENT_CALL_STATUS);
                            event.setMessageObject(object);
                            EventBus.getDefault().post(event);


                            final boolean finalIsVideoCall = isVideoCall;


                           /* CallsActivity.mActivity.finish();
                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    CallMessage.openCallScreen(mcontext, callItem.getOpponentUserId(), to, callItem.getCallId(),
                                            Room_id, "", callItem.getOpponentUserMsisdn(), MessageFactory.CALL_IN_FREE + "",
                                            finalIsVideoCall, false, ts, "true");

                                    // finish();
                                }
                            }, 1000);
*/


                            String roomUrlDefault = mContext.getString(org.appspot.apprtc.R.string.pref_room_server_url_default);
                            if (isTurnServerEnabled)
                                roomUrlDefault = WebrtcConstants.OWN_TURN_SERVER;

                            Uri uri = Uri.parse(roomUrlDefault);
                            Intent intent = new Intent(mContext, CallsActivity.class);

                            intent.setData(uri);


                            intent.putExtra(CallsActivity.CALLDURATION, callDuration);

                            intent.putExtra(CallsActivity.RECONNECTING, "true");

                            intent.putExtra(CallsActivity.EXTRA_IS_OUTGOING_CALL, false);
                            intent.putExtra(CallsActivity.EXTRA_DOC_ID, callItem.getCallId());
                            intent.putExtra(CallsActivity.EXTRA_FROM_USER_ID, callItem.getOpponentUserId());
                            intent.putExtra(CallsActivity.EXTRA_TO_USER_ID, to);
                            // intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, msisdn);
                            //   intent.putExtra(CallsActivity.EXTRA_OPPONENT_PROFILE_PIC, opponentUserProfilePic);
                            //   intent.putExtra(CallsActivity.EXTRA_NAVIGATE_FROM, mContext.getClass().getSimpleName()); // For navigating from call activity
                            intent.putExtra(CallsActivity.EXTRA_CALL_CONNECT_STATUS, MessageFactory.CALL_IN_FREE + "");
                            intent.putExtra(CallsActivity.EXTRA_CALL_TIME_STAMP, ts);
                            intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, msisdn);
                            intent.putExtra(CallsActivity.EXTRA_VIDEO_CALL, finalIsVideoCall);

                            intent.putExtra(CallsActivity.EXTRA_ROOMID, Room_id);
                            intent.putExtra(CallsActivity.EXTRA_LOOPBACK, false);
                            intent.putExtra(CallsActivity.EXTRA_SCREENCAPTURE, false);
                            intent.putExtra(CallsActivity.EXTRA_CAMERA2, true);
                            //    intent.putExtra(CallsActivity.EXTRA_VIDEO_WIDTH, videoWidth);
                            //     intent.putExtra(CallsActivity.EXTRA_VIDEO_HEIGHT, videoHeight);


                            Log.e(TAG, "EXTRA_IS_OUTGOING_CALL" + isOutgoingCall);

                            Log.e(TAG, "EXTRA_DOC_ID" + mCallId);

                            Log.e(TAG, "EXTRA_FROM_USER_ID" + fromUserId);

                            Log.e(TAG, "EXTRA_TO_USER_ID" + toUserId);
                            Log.e(TAG, "EXTRA_CALL_TIME_STAMP" + mCallTs);

                            //  Log.e(TAG,"EXTRA_USER_MSISDN"+msisdn);
                            Log.e(TAG, "EXTRA_CALL_CONNECT_STATUS" + mPrevCallStatus);

                            Log.e(TAG, "EXTRA_ROOMID" + mRoomId);
                            intent.putExtra(CallsActivity.EXTRA_VIDEO_FPS, 0);
                            intent.putExtra(CallsActivity.EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED, false);
                            intent.putExtra(CallsActivity.EXTRA_VIDEO_BITRATE, 0);
                            intent.putExtra(CallsActivity.EXTRA_VIDEOCODEC, mContext.getString(org.appspot.apprtc.R.string.pref_videocodec_default));
                            intent.putExtra(CallsActivity.EXTRA_HWCODEC_ENABLED, false);
                            intent.putExtra(CallsActivity.EXTRA_CAPTURETOTEXTURE_ENABLED, true);
                            intent.putExtra(CallsActivity.EXTRA_FLEXFEC_ENABLED, false);
                            intent.putExtra(CallsActivity.EXTRA_NOAUDIOPROCESSING_ENABLED, false);
                            intent.putExtra(CallsActivity.EXTRA_AECDUMP_ENABLED, false);

                            intent.putExtra(CallsActivity.EXTRA_OPENSLES_ENABLED, false);
                            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AEC, false);
                            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AGC, false);
                            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_NS, false);
                            intent.putExtra(CallsActivity.EXTRA_ENABLE_LEVEL_CONTROL, false);
                            intent.putExtra(CallsActivity.EXTRA_AUDIO_BITRATE, 0);
                            intent.putExtra(CallsActivity.EXTRA_AUDIOCODEC, mContext.getString(org.appspot.apprtc.R.string.pref_audiocodec_default));
                            intent.putExtra(CallsActivity.EXTRA_DISPLAY_HUD, false);
                            intent.putExtra(CallsActivity.EXTRA_TRACING, false);
                            intent.putExtra(CallsActivity.EXTRA_CMDLINE, false);
                            intent.putExtra(CallsActivity.EXTRA_RUNTIME, 0);

                            intent.putExtra(CallActivity.EXTRA_DATA_CHANNEL_ENABLED, true);
                            intent.putExtra(CallActivity.EXTRA_ORDERED, true);
                            intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS_MS, -1);
                            intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS, -1);
                            intent.putExtra(CallActivity.EXTRA_PROTOCOL, mContext.getString(org.appspot.apprtc.R.string.pref_data_protocol_default));
                            intent.putExtra(CallActivity.EXTRA_NEGOTIATED, false);
                            intent.putExtra(CallActivity.EXTRA_ID, -1);

                            mActivity.finish();
                            overridePendingTransition(0, 0);

                            //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);

                            overridePendingTransition(0, 0);
                        }
                    }
                }
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void calldisconnect(String data) {
        try {
            JSONObject object = new JSONObject(data);
            JSONObject dataObj = object.getJSONObject("data");
            if (dataObj.has("recordId")) {
                mRecordId = dataObj.getString("recordId");
            }
            Log.e("mRecordId", "mRecordId" + mRecordId);
            String call_status = "";
            mCallId = dataObj.getString("doc_id");
            call_status = dataObj.getString("call_connect");
            /*if( call_status.equals(""+MessageFactory.CALL_STATUS_REJECTED) || call_status.equals(""+MessageFactory.CALL_STATUS_END) ){
                finish();
            }*/
            if (Integer.parseInt(call_status) == MessageFactory.CALL_IN_WAITING) {
                needToSendServer = true;
                //  tvCallStatus.setText("User Busy");
                //   Toast.makeText(CallsActivity.this, "User Busy", Toast.LENGTH_SHORT).show();

              /*  new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //     finish();
                        Toast.makeText(CallsActivity.this, "User Busy", Toast.LENGTH_SHORT).show();

                        disconnect(true);
                    }
                }, 3000);*/
            } else if (Integer.parseInt(call_status) == MessageFactory.CALL_IN_FREE) {
                needToSendServer = true;
                //   Toast.makeText(CallsActivity.this, "User Busy", Toast.LENGTH_SHORT).show();

              /*  new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //     finish();
                        Toast.makeText(CallsActivity.this, "User Busy", Toast.LENGTH_SHORT).show();

                        disconnect(true);
                    }
                }, 3000);*/
            } else {
                needToSendServer = true;
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadCallStatusMessage(String data) {
        Log.e(TAG, "EVENT_CALL_STATUS" + data);
        try {
            JSONObject object = new JSONObject(data);
            String to = null;
            String recordId = object.getString("recordId");
            String status = object.getString("call_status");
            if (object.has("to")) {
                to = object.getString("to");
            }

            if (AppUtils.isEmpty(mRecordId)) {
                if (object.has("recordId")) {
                    mRecordId = object.getString("recordId");
                    Log.e(TAG, "EVENT_CALL_STATUS isEmpty mRecordId" + mRecordId);
                }
            } else {
                Log.e(TAG, "EVENT_CALL_STATUS  not isEmpty mRecordId" + mRecordId);
            }
            ////   if (!isOutgoingCall) {
            //    Log.e(TAG, "EVENT_CALL_STATUS mRecordId"+mRecordId);
            //    mRecordId=object.getString("recordId");
          /*  if (AppUtils.isEmpty(mRecordId)){
                    if ( object.has("recordId")){
                        mRecordId=object.getString("recordId");
                        Log.e(TAG, "EVENT_CALL_STATUS isEmpty mRecordId"+mRecordId);
                    }
                }else {
                Log.e(TAG, "EVENT_CALL_STATUS  not isEmpty mRecordId"+mRecordId);

            }*/
            //  }

            if (recordId.equalsIgnoreCase(mRecordId)) {
                Log.e(TAG, "EVENT_CALL_STATUS equalsIgnoreCase" + mRecordId + "canEndCall" + canEndCall);
                Log.e(TAG, "EVENT_CALL_STATUS status" + status);

                switch (status) {
                    case MessageFactory.CALL_STATUS_END + "":
                        canEndCall = true;
                    case MessageFactory.CALL_STATUS_REJECTED + "":
                        break;
//                        SharedPreference.getInstance().saveBool(mContext, "callongoing",false);
                    case MessageFactory.CALL_STATUS_RECEIVED + "":

                    case MessageFactory.CALL_STATUS_MISSED + "":
                        SharedPreference.getInstance().saveBool(mContext, "callongoing", false);

                        needToSendServer = false;
                        canEndCall = true;
                        Log.e(TAG, "CALL_STATUS_MISSED" + canEndCall);

                        Log.e(TAG, "disconnect" + "loadCallStatusMessage");

                        disconnect(false);
                        //Check it is user and call status is ringing
                        if (mCurrentUserId.equalsIgnoreCase(to)) {
                            MyLog.e("loadCallStatusMessage", "mCurrentUserId" + mCurrentUserId + "to" + to + "equals");
                            callSpeaker();
                        }
                        break;
                    case MessageFactory.CALL_STATUS_ARRIVED + "":
                        stopRetryCallConnect();
                        if (mCurrentUserId.equalsIgnoreCase(to)) {
                            MyLog.e("loadCallStatusMessage", "mCurrentUserId" + mCurrentUserId + "to" + to + "equals");
                            callSpeaker();
                        }
                        break;
                    case MessageFactory.CALL_STATUS_ANSWERED + "":
//Check it is user or receiverr
                        Log.e(TAG, "CALL_STATUS_ANSWERED");
                        boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                        if (is_gossip) {
                            arrow.setVisibility(View.VISIBLE);
                        }

                        //   if (isOutgoingCall) {
                        mReconnecting = true;
                        //  }
                        if (mCurrentUserId.equalsIgnoreCase(to)) {
                            Log.e("sender", "CALL_STATUS_ANSWERED" + mCurrentUserId + "to" + to + "equals");
                        } else {
                            Log.e("receiver", "CALL_STATUS_ANSWERED" + mCurrentUserId + "to" + to + "equals");
                        }

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isAnsweredToUser = true;
                                SharedPreference.getInstance().saveBool(mContext, "isAnsweredToUser", true);
                                if (callTimeoutHandler != null) {
                                    callTimeoutHandler.removeCallbacks(callTimeoutRunnable);
                                }
                            }
                        }, 3000);

                        break;
                    case MessageFactory.CALL_STATUS_PAUSE + "":

                        logAndToast(mCallerName + " video call is paused");
                        break;
                }
            } else {
                Log.e(TAG, "EVENT_CALL_STATUS not equalsIgnoreCase" + mRecordId + "canEndCall" + canEndCall);
//Check call status end call true
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadCallRetryResponse(String data) {
        try {
            MyLog.e(TAG, "loadCallRetryResponse: " + data);
            JSONObject object = new JSONObject(data);

            if (object.has("err")) {
                String mErr = object.getString("err");
                if (Integer.parseInt(mErr) == 1) {
                    String mMessage = object.getString("msg");
                    if (mMessage.equals("Already Done!")) {

                    } else if (mMessage.equals("Invalid RecordId Id")) {
                        Toast.makeText(mContext, mMessage, Toast.LENGTH_SHORT).show();
                      /*  new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(mContext, "Please Try Again...", Toast.LENGTH_SHORT).show();
                                disconnect(false);
                                finish();

                            }
                        }, 3000);*/
                    }
                    //{"err":1,"msg":"Invalid RecordId Id"}

                      /*  new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                disconnect(false);
                                finish();

                            }
                        }, 3000);*/


                }
            }
            if (object.has("from")) {
                String from = object.getString("from");
                String recordId = object.getString("recordId");

                if (from.equalsIgnoreCase(mCurrentUserId) && mRecordId.equalsIgnoreCase(recordId)) {
                    if (object.has("call_connect")) {
                        String callConnect = object.getString("call_connect");
                        setCallStatusText(callConnect);
                    }
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void stopRetryCallConnect() {
        CallMessage.arrivedCallId = "";
        isArrivedToUser = true;
        if (retryCallRunnable != null && retryCallHandler != null) {
            retryCallHandler.removeCallbacks(retryCallRunnable);
        }
    }

    private void stopReconnectCall() {

        isCallReconnecting = false;
        if (!isVideoCall) {
            tvDuration.setVisibility(View.VISIBLE);
        } else {
            hideInfoCtrls();
        }

        if (reconnectRunnable != null && reconnectHandler != null) {
            reconnectHandler.removeCallbacks(reconnectRunnable);
            tvCallStatus.setVisibility(View.GONE);
        }
    }

    private String getCallId() {
        String id = "";
        if (mCallId != null) {

            String[] splitIds = mCallId.split("-");
            id = splitIds[2];
        }

        if (isOutgoingCall) {
            return toUserId + "-" + fromUserId + "-" + id;


        } else {
            return fromUserId + "-" + toUserId + "-" + id;
        }

    }

    private void sendCallDisconnectToServer(boolean isMissedCall, boolean needDBUpdate) {
        //Check call Id is empoty or not
        MessageDbController dbController = CoreController.getDBInstance(CallsActivity.this);
        Log.e(TAG, "callItem" + "mCallId" + mCallId + "isOutgoingCall" + isOutgoingCall);

        CallItemChat callItemm = dbController.getCallStatus(mCallId);
        if (!isOutgoingCall) {
            Log.e(TAG, "callItem" + "getRecordId" + callItemm.getRecordId());
            if (AppUtils.isEmpty(mRecordId)) {
                mRecordId = callItemm.getRecordId();
            }
        }

        MessageDbController db = CoreController.getDBInstance(this);
        JSONObject object;

        String id = "";
        if (mCallId != null) {

            String[] splitIds = mCallId.split("-");
            id = splitIds[2];
        }


        if (isOutgoingCall) {
            //  String callDocId = toUserId + "-" + fromUserId + "-" + id;
            String callDocId = fromUserId + "-" + toUserId + "-" + id;
            if (isConnectedToUser || isAnsweredToUser) {

                String type = "" + MessageFactory.audio_call;
                if (isVideoCall)
                    type = "" + MessageFactory.video_call;
                object = CallMessage.getCallStatusObject(fromUserId, toUserId, id, callDocId, mRecordId, MessageFactory.CALL_STATUS_END, type);
                if (needDBUpdate) {
                    SharedPreference.getInstance().saveBool(mContext, "callongoing", false);

                    db.updateOutGoingCallStatus(callDocId, MessageFactory.CALL_STATUS_END, getCallDuration());
                }
            } else if (isMissedCall) {
                String type = "" + MessageFactory.audio_call;
                if (isVideoCall)
                    type = "" + MessageFactory.video_call;
                object = CallMessage.getCallStatusObject(fromUserId, toUserId, id, callDocId, mRecordId, MessageFactory.CALL_STATUS_MISSED, type);
                if (needDBUpdate) {
                    if (mCurrentUserId.equals(fromUserId)) {
                        //Check call is incoming or missed
                        if (!isConnectedToUser || !isAnsweredToUser) {
                            try {
                                object.put("call_status", MessageFactory.CALL_STATUS_REJECTED);
                                SharedPreference.getInstance().saveBool(mContext, "callongoing", false);

                                CallItemChat callItem = new IncomingMessage(this).loadfromOfflineOutgoingCall(object);
                                db.updateCallLogs(callItem);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                object.put("call_status", MessageFactory.CALL_STATUS_REJECTED);
                                SharedPreference.getInstance().saveBool(mContext, "callongoing", false);

                                CallItemChat callItem = new IncomingMessage(this).loadfromOfflineCall(object);
                                db.updateCallLogs(callItem);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {

                        db.updateOutGoingCallStatus(callDocId, MessageFactory.CALL_STATUS_MISSED, getCallDuration());
                    }
                }
            } else {
                String type = "" + MessageFactory.audio_call;
                if (isVideoCall)
                    type = "" + MessageFactory.video_call;
                object = CallMessage.getCallStatusObject(fromUserId, toUserId, id, callDocId, mRecordId, MessageFactory.CALL_STATUS_REJECTED, type);
                if (needDBUpdate) {
                    db.updateOutGoingCallStatus(callDocId, MessageFactory.CALL_STATUS_REJECTED, getCallDuration());
                }
                SharedPreference.getInstance().saveBool(mContext, "callongoing", false);

            }
        } else {
            String type = "" + MessageFactory.audio_call;
            if (isVideoCall)
                type = "" + MessageFactory.video_call;
            String callDocId = toUserId + "-" + fromUserId + "-" + id;
            //Check mRecordId is null or empty

            object = CallMessage.getCallStatusObject(toUserId, fromUserId, id, callDocId, mRecordId, MessageFactory.CALL_STATUS_END, type);
            if (needDBUpdate) {
                db.updateCallStatus(callDocId, MessageFactory.CALL_STATUS_END, getCallDuration());
            }
            SharedPreference.getInstance().saveBool(mContext, "callongoing", false);

        }

        SendMessageEvent event = new SendMessageEvent();

        if (isMissedCall) {
            event.setEventName(SocketManager.EVENT_CALL_STATUS);
        } else if (needDBUpdate) {
            event.setEventName(SocketManager.EVENT_CALL_STATUS);
        } else {
            event.setEventName(SocketManager.EVENT_CALL_STATUS);
        }
        event.setMessageObject(object);
        EventBus.getDefault().post(event);
        Log.e(TAG, "sc_call_status" + object);


    }

    private void sendAckToServer(JSONObject object) {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_CALL_STATUS);
        event.setMessageObject(object);
        EventBus.getDefault().post(event);
    }

    private void startTimer() {
        if (timer == null) {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    //  Log.e(TAG, "startTimer" + "isNetworkConnected" + isNetworkConnected + "mStartTimer" + mStartTimer);
                    if (isNetworkConnected) {
                        if (mStartTimer) {
                            callDuration++;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    tvDuration.setText(getCallDuration());
                                }
                            });
                        }
                      /*  callDuration++;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvDuration.setText(getCallDuration());
                            }
                        });*/
                    } else {
                        //   tvDuration.setText("Reconnecting..");
                        timer.cancel();
//                tvDuration.setText("Reconnecting..");
                    }
                }
            }, 1000, 1000);
        }
    }

    /*private void startNotification() {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(ns);

        Notification notification = new Notification(R.drawable.ic_launcher, null,
                System.currentTimeMillis());

        RemoteViews notificationView = new RemoteViews(getPackageName(),
                R.layout.av_call_notification);

        //the intent that is started when the notification is clicked (works)
        Intent notificationIntent = new Intent(this, VideoCallActivity.class);
        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        notification.contentView = notificationView;
        notification.contentIntent = pendingNotificationIntent;
        notification.flags |= Notification.FLAG_NO_CLEAR;

        //this is the intent that is supposed to be called when the
        //button is clicked
        Intent switchIntent = new Intent(this, VideoCallActivity.class);
        PendingIntent pendingSwitchIntent = PendingIntent.getBroadcast(this, 0,
                switchIntent, 0);

        notificationView.setOnClickPendingIntent(R.id.closeOnFlash,
                pendingSwitchIntent);

        notificationManager.notify(1, notification);
    }*/

    private String getCallDuration() {
        int hr, min, sec;
        String durationStr, secStr, minStr, hrStr = "";

        sec = callDuration % 60;
        min = (callDuration / 60) % 60;
        hr = (callDuration / (60 * 60)) % 60;
        if (sec > 0) {
            tvCallStatus.setText("");
        }

        if (sec < 10) {
            secStr = "0" + sec;
        } else {
            secStr = String.valueOf(sec);
        }

        if (min < 10) {
            minStr = "0" + min + ":";
        } else {
            minStr = min + ":";
        }

        if (hr > 0) {
            if (hr < 10) {
                hrStr = "0" + hr + ":";
            } else {
                hrStr = hr + ":";
            }
        }

        durationStr = hrStr + minStr + secStr;
        return durationStr;
    }

    @Override
    public void onBackPressed() {
      /*  if (!CallsActivity.isStarted) {
            SharedPreference.getInstance().saveBool(mContext, "isStarted", false);

            finish();
            SharedPreference.getInstance().saveBool(mContext, "callongoing", false);
        } else {
            SharedPreference.getInstance().saveBool(mContext, "isStarted", true);

            super.onBackPressed();
        }*/

        /*Class backActivity;
        String navigateFrom = getIntent().getExtras().getString(EXTRA_NAVIGATE_FROM, "");
        if (ChatViewActivity.class.getSimpleName().equals(navigateFrom) && !ChatViewActivity.isKilled) {
            backActivity = ChatViewActivity.class;
        } else if (CallHistoryActivity.class.getSimpleName().equals(navigateFrom)) {
            backActivity = CallHistoryActivity.class;
        } else if (CallInfoActivity.class.getSimpleName().equals(navigateFrom) && !CallInfoActivity.isKilled) {
            backActivity = CallInfoActivity.class;
        } else {
            backActivity = HomeScreen.class;
        }
        Intent backIntent = new Intent(CallsActivity.this, backActivity);
//        backIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(backIntent);*/

    }

    private void reconnectCall() {

        // Reset timer for once reconnected call time starts
       /* if(timer != null) {
            timer.cancel();
            timer = null;
        }*/
        Log.e(TAG, "reconnectRunnable " + "reconnectRunnable" + reconnectRunnable);

        if (reconnectRunnable == null) {
            reconnectHandler = new Handler();
            reconnectRunnable = new Runnable() {
                @Override
                public void run() {

                    Log.e(TAG, "disconnect " + canEndCall + "reconnectCall1" + isNetworkConnected);
                    if (canEndCall) {
                        disconnect(false);
                    }
                }
            };
        }

        long timeout = RECONNECT_CALL_TIMEOUT;
        if (!ConnectivityInfo.isInternetConnected(this)) {
            timeout = OFFLINE_RECONNECT_CALL_TIMEOUT;
        }


        reconnectHandler.postDelayed(reconnectRunnable, timeout);

        if (appRtcClient != null) {
            appRtcClient.disconnectFromRoom();
            appRtcClient.connectToRoom(roomConnectionParameters);
        }
        Log.e(TAG, "canEndCall " + "canEndCall" + !canEndCall);

        if (!canEndCall) {
            isCallReconnecting = true;
            showInfoCtrls();
        } else {
            Log.e(TAG, "disconnect " + "reconnectCall2" + canEndCall);
            //Check if the network is disconnected
            if (canEndCall) {
                disconnect(false);
            }
        }
    }


    //-----------------------------------New Code-----------------------------------------

    private boolean ToggleMic() {
        if (peerConnectionClient != null) {
            micEnabled = !micEnabled;
            peerConnectionClient.setAudioEnabled(micEnabled);
        }
        return micEnabled;
    }

    private boolean SpeakerOn() {
        if (audioManager.getSelectedAudioDevice() != null) {
            if (audioManager.getSelectedAudioDevice().equals(AppRTCAudioManager.AudioDevice.EARPIECE)) {
                audioManager.setAudioDeviceInternal(AppRTCAudioManager.AudioDevice.SPEAKER_PHONE);
                setMediaVolume(80);
                speakerEnabled = true;
            } else {
                audioManager.setAudioDeviceInternal(AppRTCAudioManager.AudioDevice.EARPIECE);
                setMediaVolume(40);
                speakerEnabled = false;
            }
            return speakerEnabled;

        } else {
            speakerEnabled = false;
            return speakerEnabled;

        }
    }

    @Override
    public void onPictureInPictureModeChanged(boolean isInPictureInPictureMode, Configuration newConfig) {

        if (isInPictureInPictureMode) {
            // Hide the full-screen UI (controls, etc.) while in picture-in-picture mode.
            disconnect_layout.setVisibility(View.GONE);
            bottom_layout.setVisibility(View.GONE);
            call_header.setVisibility(View.GONE);
            localRender.setVisibility(View.GONE);
            //     localRender.setLayoutParams(new WindowManager.LayoutParams(130, 130));

        } else {
            // Restore the full-screen UI.
            disconnect_layout.setVisibility(View.VISIBLE);
            bottom_layout.setVisibility(View.VISIBLE);
            call_header.setVisibility(View.VISIBLE);
            localRender.setVisibility(View.VISIBLE);

        }
    }


    public void callSpeaker() {
        boolean enabled = SpeakerOn();
        if (enabled) {
            ibToggleSpeaker.setImageResource(org.appspot.apprtc.R.drawable.ic_specker_on);
        } else {
            ibToggleSpeaker.setImageResource(org.appspot.apprtc.R.drawable.ic_specker_off);
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        //  return false;

        //fin.setImageBitmap(bmap);

        final int Y = (int) event.getRawY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
                _yDelta = Y - lParams.bottomMargin;
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
                layoutParams.bottomMargin = (Y - _yDelta);
                layoutParams.topMargin = -layoutParams.bottomMargin;
                v.setLayoutParams(layoutParams);
                v.animate().translationY(Y - _yDelta).setDuration(0);
                break;
        }
        findViewById(R.id.local_video_relativelayout).invalidate();
        return true;
    }

    /**
     * Determine the space between the first two fingers
     */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        float s = x * x + y * y;
        return (float) Math.sqrt(s);
    }

    /**
     * Calculate the mid point of the first two fingers
     */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    /**
     * Calculate the degree to be rotated by.
     *
     * @param event
     * @return Degrees
     */
    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }


    @Override
    public void onNetworkConnectionChanged(final boolean isConnected) {
        Log.e(TAG, "onNetworkConnectionChanged" + isConnected + "isConnectedToUser" + isConnectedToUser + "canEndcall" + canEndCall + "isNetworkConnected" + isNetworkConnected);
        //Check call Status before Reconnecting

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {


                Log.e(TAG, "isConnected" + isConnected + "canEndCall" + canEndCall);

                if (!isConnected) {
                    mStartTimer = false;
                    cancelTimer();
                    if (!canEndCall) {
                        ShowReconnecting();
                        //Trigger reconnect intimate
                        //    if (isOutgoingCall){
                        ReconnectInitimate();
                        //   }
                    }
                    if (isConnectedToUser) {
                        isConnectedToUser = false;
                    }

                    isNetworkConnected = isConnected;


                } else {
                    Log.e(TAG, "isNetworkConnected" + isNetworkConnected);

                    if (!isNetworkConnected) {
                        isNetworkConnected = isConnected;
                        Log.e("isNetworkConnected", "isNetworkConnected" + isNetworkConnected + "isOutgoingCall" + isOutgoingCall);

                        if (isOutgoingCall) {
                            //Trigger Sc_call and call Retry
                            //Check can end call true or false if it is false
                            Log.e("onNetworkConnectionChanged", "isOutgoingCall" + isOutgoingCall + "canEndCall" + canEndCall);

                            if (!canEndCall) {
                                mHandlerr = new Handler();

                                mHandlerr.postDelayed(mRunnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!AppUtils.isServiceRunning(mContext, MessageService.class)) {
                                            AppUtils.startService(mContext, MessageService.class);

                                            Log.e("isServiceRunning", "isServiceRunning not running");

                                            mHandlerr.postDelayed(mRunnable, 1000);

                                        } else {
                                            if (MessageService.manager != null && MessageService.manager.isConnected()) {

                                                mHandlerr.removeCallbacksAndMessages(null);
                                                //    ReconnectRoom();
                                                Log.e("ReconnectRoom", "ReconnectRoom Intent called ");

                                                if (isOutgoingCall) {

                                                    new Handler().postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {

                                                            if (isOutgoingCall) {
                                                                long mIncrementmRoomId = Long.valueOf(mRoomId);
                                                                mIncrementmRoomId = mIncrementmRoomId + 1;
                                                                Log.e("mIncrementRecordId", "mIncrementmRoomId" + mIncrementmRoomId);
                                                                mRoomId = String.valueOf(mIncrementmRoomId);
                                                                Log.e("mRoomId", "mIncrementmRoomId" + mRoomId);

                                                            }

                                                            String roomUrlDefault = mContext.getString(org.appspot.apprtc.R.string.pref_room_server_url_default);
                                                            if (isTurnServerEnabled)
                                                                roomUrlDefault = WebrtcConstants.OWN_TURN_SERVER;

                                                            Uri uri = Uri.parse(roomUrlDefault);
                                                            Intent intent = new Intent(mContext, CallsActivity.class);

                                                            intent.setData(uri);


                                                            intent.putExtra(CallsActivity.CALLDURATION, callDuration);

                                                            intent.putExtra(CallsActivity.RECONNECTING, "true");

                                                            intent.putExtra(CallsActivity.EXTRA_IS_OUTGOING_CALL, isOutgoingCall);
                                                            intent.putExtra(CallsActivity.EXTRA_DOC_ID, mCallId);
                                                            intent.putExtra(CallsActivity.EXTRA_FROM_USER_ID, fromUserId);
                                                            intent.putExtra(CallsActivity.EXTRA_TO_USER_ID, toUserId);
                                                            // intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, msisdn);
                                                            //   intent.putExtra(CallsActivity.EXTRA_OPPONENT_PROFILE_PIC, opponentUserProfilePic);
                                                            intent.putExtra(CallsActivity.EXTRA_NAVIGATE_FROM, mContext.getClass().getSimpleName()); // For navigating from call activity
                                                            intent.putExtra(CallsActivity.EXTRA_CALL_CONNECT_STATUS, mPrevCallStatus);
                                                            intent.putExtra(CallsActivity.EXTRA_CALL_TIME_STAMP, mCallTs);
                                                            intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, msisdn);

                                                            intent.putExtra(CallsActivity.EXTRA_ROOMID, mRoomId);
                                                            intent.putExtra(CallsActivity.EXTRA_LOOPBACK, false);
                                                            intent.putExtra(CallsActivity.EXTRA_VIDEO_CALL, isVideoCall);
                                                            intent.putExtra(CallsActivity.EXTRA_SCREENCAPTURE, false);
                                                            intent.putExtra(CallsActivity.EXTRA_CAMERA2, true);
                                                            //    intent.putExtra(CallsActivity.EXTRA_VIDEO_WIDTH, videoWidth);
                                                            //     intent.putExtra(CallsActivity.EXTRA_VIDEO_HEIGHT, videoHeight);


                                                            Log.e(TAG, "EXTRA_IS_OUTGOING_CALL" + isOutgoingCall);

                                                            Log.e(TAG, "EXTRA_DOC_ID" + mCallId);

                                                            Log.e(TAG, "EXTRA_FROM_USER_ID" + fromUserId);

                                                            Log.e(TAG, "EXTRA_TO_USER_ID" + toUserId);
                                                            Log.e(TAG, "EXTRA_CALL_TIME_STAMP" + mCallTs);

                                                            //  Log.e(TAG,"EXTRA_USER_MSISDN"+msisdn);
                                                            Log.e(TAG, "EXTRA_CALL_CONNECT_STATUS" + mPrevCallStatus);

                                                            Log.e(TAG, "EXTRA_ROOMID" + mRoomId);
                                                            intent.putExtra(CallsActivity.EXTRA_VIDEO_FPS, 0);
                                                            intent.putExtra(CallsActivity.EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED, false);
                                                            intent.putExtra(CallsActivity.EXTRA_VIDEO_BITRATE, 0);
                                                            intent.putExtra(CallsActivity.EXTRA_VIDEOCODEC, mContext.getString(org.appspot.apprtc.R.string.pref_videocodec_default));
                                                            intent.putExtra(CallsActivity.EXTRA_HWCODEC_ENABLED, false);
                                                            intent.putExtra(CallsActivity.EXTRA_CAPTURETOTEXTURE_ENABLED, true);
                                                            intent.putExtra(CallsActivity.EXTRA_FLEXFEC_ENABLED, false);
                                                            intent.putExtra(CallsActivity.EXTRA_NOAUDIOPROCESSING_ENABLED, false);
                                                            intent.putExtra(CallsActivity.EXTRA_AECDUMP_ENABLED, false);

                                                            intent.putExtra(CallsActivity.EXTRA_OPENSLES_ENABLED, false);
                                                            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AEC, false);
                                                            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AGC, false);
                                                            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_NS, false);
                                                            intent.putExtra(CallsActivity.EXTRA_ENABLE_LEVEL_CONTROL, false);
                                                            intent.putExtra(CallsActivity.EXTRA_AUDIO_BITRATE, 0);
                                                            intent.putExtra(CallsActivity.EXTRA_AUDIOCODEC, mContext.getString(org.appspot.apprtc.R.string.pref_audiocodec_default));
                                                            intent.putExtra(CallsActivity.EXTRA_DISPLAY_HUD, false);
                                                            intent.putExtra(CallsActivity.EXTRA_TRACING, false);
                                                            intent.putExtra(CallsActivity.EXTRA_CMDLINE, false);
                                                            intent.putExtra(CallsActivity.EXTRA_RUNTIME, 0);

                                                            intent.putExtra(CallActivity.EXTRA_DATA_CHANNEL_ENABLED, true);
                                                            intent.putExtra(CallActivity.EXTRA_ORDERED, true);
                                                            intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS_MS, -1);
                                                            intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS, -1);
                                                            intent.putExtra(CallActivity.EXTRA_PROTOCOL, mContext.getString(org.appspot.apprtc.R.string.pref_data_protocol_default));
                                                            intent.putExtra(CallActivity.EXTRA_NEGOTIATED, false);
                                                            intent.putExtra(CallActivity.EXTRA_ID, -1);

                                                            mActivity.finish();
                                                            overridePendingTransition(0, 0);

                                                            //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            mContext.startActivity(intent);

                                                            overridePendingTransition(0, 0);

                                                        }
                                                    }, 2000);

                                                } else {
                                                    if (!canEndCall) {
                                                        if (!isOutgoingCall) {
                                                            mHandlerr = new Handler();

                                                            mHandlerr.postDelayed(mRunnable = new Runnable() {
                                                                @Override
                                                                public void run() {

                                                                    Log.e(TAG, "onNetworkConnectionChanged Handler" + "TriggerReconnectcalltosender" + isNetworkConnected);

                                                                    TriggerReconnectcalltosender();


                                                                }
                                                            }, 1000);
                                                        }

                                                    }
                                                }

                                            } else {
                                                mHandlerr.postDelayed(mRunnable, 4000);
                                                MessageService.manager.connect();
                                            }
                                        }
                                    }
                                }, 1000);
                            }
                        } else {
                            Log.e("onNetworkConnectionChanged", "isOutgoingCall" + isOutgoingCall + "canEndCall" + canEndCall);

                            if (!canEndCall) {
                                if (!isOutgoingCall) {
                                    mHandlerr = new Handler();

                                    mHandlerr.postDelayed(mRunnable = new Runnable() {
                                        @Override
                                        public void run() {
                                            if (!AppUtils.isServiceRunning(mContext, MessageService.class)) {
                                                AppUtils.startService(mContext, MessageService.class);

                                                Log.e("isServiceRunning", "isServiceRunning not running");

                                                mHandlerr.postDelayed(mRunnable, 1000);

                                            } else {
                                                if (MessageService.manager != null && MessageService.manager.isConnected()) {

                                                    mHandlerr.removeCallbacksAndMessages(null);
                                                    Log.e(TAG, "onNetworkConnectionChanged Handler" + "TriggerReconnectcalltosender" + isNetworkConnected);


                                                    TriggerReconnectcalltosender();
                                                } else {
                                                    mHandlerr.postDelayed(mRunnable, 4000);
                                                    MessageService.manager.connect();
                                                }
                                            }
                                        }
                                    }, 1000);
                                }

                            }
                        }
                    } else {

                        //    Toast.makeText(CallsActivity.this, "Network is connected already", Toast.LENGTH_SHORT).show();

                    }

                }

            }
        }, 2000);


    }

    public void DisconnectFromRoom() {
        //Remove all Previous Connection
        ShowReconnecting();

        if (timer != null) {
            timer.cancel();
        }

        if (needToSendServer) {

            needToSendServer = false;
        }
        if (appRtcClient != null) {
            appRtcClient.disconnectFromRoom();
            appRtcClient = null;
        }
        if (peerConnectionClient != null) {
            peerConnectionClient.close();
            peerConnectionClient = null;
        }

        if (remoteRenderScreen != null) {
            remoteRenderScreen.release();
            remoteRenderScreen = null;
        }

        //Reconnect my Call
        // tvDuration.setText("Reconnecting..");

        Log.e("mRoomId", "mRoomId" + mRoomId);

        if (isOutgoingCall) {

            if (ConnectivityReceiver.isConnected()) {

                //If its sender and reconnect to my room  and receiver

                //  startRetryCallConnect();
                // For send busy status to third user until opponent user pick the call
                //   IncomingCallActivity.isStarted = true;
                //   ReconnectRoom();
                //   startCall();
              /*  Bundle configBundle = new Bundle();

                configBundle.putString(EXTRA_CALL_CONNECT_STATUS, mPrevCallStatus);

                configBundle.putString(EXTRA_DOC_ID, mCallId);
                configBundle.putString(EXTRA_FROM_USER_ID, fromUserId);
                configBundle.putString(EXTRA_TO_USER_ID, toUserId);
                configBundle.putString(mCallTs, EXTRA_CALL_TIME_STAMP);
                configBundle.putBoolean(EXTRA_VIDEO_CALL, true);
                configBundle.putString(EXTRA_ROOMID, mRoomId);


                onCreate(configBundle);
*/
/*
                mHandlerr = new Handler();

                mHandlerr.postDelayed(mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        if (!AppUtils.isServiceRunning(mContext, MessageService.class)) {
                            AppUtils.startService(mContext, MessageService.class);

                            Log.e("isServiceRunning", "isServiceRunning not running");

                            mHandlerr.postDelayed(mRunnable, 1000);

                        }else {
                            if (MessageService.manager != null && MessageService.manager.isConnected()) {

                                mHandlerr.removeCallbacksAndMessages(null);
                                ReconnectRoom();
                            }else {
                                mHandlerr.postDelayed(mRunnable, 1000);

                            }
                        }
                    }
                    }, 1000);*/

                //  ReconnectRoom();
            }
        }

    }

    public void ReconnectRoom() {
        Log.e("ReconnectRoom", "ReconnectRoom");

        iceConnected = false;
        signalingParameters = null;
        scalingType = RendererCommon.ScalingType.SCALE_ASPECT_FILL;

        Log.e("remoteRenderers", "remoteRenderers" + remoteRenderers.size());
        if (isOutgoingCall) {
            long mIncrementmRoomId = Long.valueOf(mRoomId);
            mIncrementmRoomId = mIncrementmRoomId + 1;
            Log.e("mIncrementRecordId", "mIncrementmRoomId" + mIncrementmRoomId);

            mRoomId = String.valueOf(mIncrementmRoomId);
            Log.e("mRoomId", "mIncrementmRoomId" + mRoomId);

        }

        remoteRenderers.clear();
        remoteRenderers.add(remoteRenderScreen);
        Log.e("remoteRenderers added value", "remoteRenderers" + remoteRenderers.size());
        updateVideoView();

        if (cameraswitched) {
            if (remoteRenderLayout != null) {
                remoteRenderLayout.setPosition(REMOTE_X, REMOTE_Y, REMOTE_WIDTH, REMOTE_HEIGHT);
                remoteRenderScreen.setScalingType(scalingType);
                remoteRenderScreen.setMirror(false);
            }
            // For remove busy status once opponent user pick the call
            IncomingCallActivity.isStarted = false;


            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            Log.e(TAG, "iceConnected" + iceConnected + "startTimer" + isConnectedToUser);

            if (iceConnected) {
                isConnectedToUser = true;

                mStartTimer = true;
                startTimer();
                Log.e(TAG, "iceConnected" + iceConnected + "startTimer" + isConnectedToUser);

//            startNotification();
                if (localRenderLayout != null) {
                    localRenderLayout.setPosition(
                            LOCAL_X_CONNECTED, LOCAL_Y_CONNECTED, LOCAL_WIDTH_CONNECTED, LOCAL_HEIGHT_CONNECTED);
                    if (localRender != null) {
                        localRender.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
                    }
                }
            } else {
                if (localRenderLayout != null) {

                    localRenderLayout.setPosition(
                            LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING, LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING);
                    localRender.setScalingType(scalingType);
                }
            }
            if (localRenderLayout != null) {

                localRender.setMirror(false);

                localRender.requestLayout();
            }
            if (remoteRenderScreen != null) {
                remoteRenderScreen.requestLayout();

            }
        } else {
            if (remoteRenderScreen != null) {

                remoteRenderLayout.setPosition(REMOTE_X, REMOTE_Y, REMOTE_WIDTH, REMOTE_HEIGHT);
                remoteRenderScreen.setScalingType(scalingType);
                remoteRenderScreen.setMirror(false);
            }
            // For remove busy status once opponent user pick the call
            IncomingCallActivity.isStarted = false;


            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            Log.e(TAG, "iceConnected" + iceConnected + "isConnectedToUser" + isConnectedToUser);

            if (iceConnected) {

                isConnectedToUser = true;
                mStartTimer = true;

                startTimer();

                Log.e(TAG, "iceConnected" + iceConnected + "startTimer" + isConnectedToUser);


                //     final RegionView lRegionView = new RegionView(this);

                // lRegionView.addView(lp);

                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) local_video_relativelayout.getLayoutParams();
                lp.addRule(RelativeLayout.ABOVE, dummy_layout.getId());
//            startNotification();
                if (localRenderLayout != null) {

                    localRenderLayout.setPosition(LOCAL_X_CONNECTED, LOCAL_Y_CONNECTED, LOCAL_WIDTH_CONNECTED, LOCAL_HEIGHT_CONNECTED);

                }
                if (localRender != null) {

                    localRender.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
                }
                //  local_video_relativelayout.addView(lRegionView);

            } else {
                if (localRenderLayout != null) {

                    localRenderLayout.setPosition(
                            LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING, LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING);
                    if (localRender != null) {

                        localRender.setScalingType(scalingType);
                    }
                }
                if (localRender != null) {
                    localRender.setMirror(true);

                    localRender.requestLayout();

                }
            }
            if (remoteRenderScreen != null) {

                remoteRenderScreen.requestLayout();
            }
        }
        if (isOutgoingCall) {
            tvDuration.setText("Reconnecting...");

            Log.e("mRecordId", "mRecordId" + mRecordId);
            //    startRetryCallConnect();
            // For send busy status to third user until opponent user pick the call
            IncomingCallActivity.isStarted = true;

            //   handleCallTimeout();
        } else {
            IncomingCallActivity.isStarted = false;

        }

        Log.e("mRecordId", "mRecordId" + mRoomId);
        if (false || !DirectRTCClient.IP_PATTERN.matcher(mRoomId).matches()) {
            appRtcClient = new WebSocketRTCClient(this);
        } else {
            Log.i(TAG, "Using DirectRTCClient because room name looks like an IP.");
            appRtcClient = new DirectRTCClient(this);
        }
        Uri roomUri = null;
        //TURN SERVER CODE
        if (isTurnServerEnabled) {
            roomUri = Uri.parse(WebrtcConstants.OWN_TURN_SERVER);
        }
        // Create connection parameters.
        roomConnectionParameters = new AppRTCClient.RoomConnectionParameters(roomUri.toString(), mRoomId, false);

        peerConnectionClient = PeerConnectionClient.getInstance();
        if (false) {
            PeerConnectionFactory.Options options = new PeerConnectionFactory.Options();
            options.networkIgnoreMask = 0;
            peerConnectionClient.setPeerConnectionFactoryOptions(options);
        }
        peerConnectionClient.createPeerConnectionFactory(CallsActivity.this, peerConnectionParameters, CallsActivity.this);

        if (screencaptureEnabled) {
            MediaProjectionManager mediaProjectionManager =
                    null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mediaProjectionManager = (MediaProjectionManager) getApplication().getSystemService(
                        Context.MEDIA_PROJECTION_SERVICE);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                startActivityForResult(
                        mediaProjectionManager.createScreenCaptureIntent(), CAPTURE_PERMISSION_REQUEST_CODE);
            }
        } else {
            startCall();
        }

        /*     signalingParameters = null;
        Log.e("remoteRenderers", "remoteRenderers" + remoteRenderers.size());
        remoteRenderers.clear();

        remoteRenderers.add(remoteRenderScreen);
        Log.e("remoteRenderers added value", "remoteRenderers" + remoteRenderers.size());

        IncomingCallActivity.isStarted = false;


        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }

        Log.e("mRecordId", "mRecordId" + mRoomId);
        if (false || !DirectRTCClient.IP_PATTERN.matcher(mRoomId).matches()) {
            appRtcClient = new WebSocketRTCClient(this);
        } else {
            Log.i(TAG, "Using DirectRTCClient because room name looks like an IP.");
            appRtcClient = new DirectRTCClient(this);
        }
        Uri roomUri = null;
        //TURN SERVER CODE
        if (WebrtcConstants.isTurnServerEnabled) {
            roomUri = Uri.parse(WebrtcConstants.OWN_TURN_SERVER);
        }
        // Create connection parameters.
        roomConnectionParameters = new AppRTCClient.RoomConnectionParameters(roomUri.toString(), mRoomId, false);

        peerConnectionClient = PeerConnectionClient.getInstance();
        if (false) {
            PeerConnectionFactory.Options options = new PeerConnectionFactory.Options();
            options.networkIgnoreMask = 0;
            peerConnectionClient.setPeerConnectionFactoryOptions(options);
        }
        peerConnectionClient.createPeerConnectionFactory(CallsActivity.this, peerConnectionParameters, CallsActivity.this);

        if (screencaptureEnabled) {
            MediaProjectionManager mediaProjectionManager =
                    null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mediaProjectionManager = (MediaProjectionManager) getApplication().getSystemService(
                        Context.MEDIA_PROJECTION_SERVICE);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                startActivityForResult(
                        mediaProjectionManager.createScreenCaptureIntent(), CAPTURE_PERMISSION_REQUEST_CODE);
            }
        } else {
            startCall();
        }*/
    }

    public void TriggerReconnectcalltosender() {


        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {


                //    Log.e(TAG, "EVENT_CALL hANDLER");
                JSONObject object = new JSONObject();
                try {
                    object.put("from", mCurrentUserId);
                    object.put("to", fromUserId);


                    if (object != null) {
                        SendMessageEvent callEvent = new SendMessageEvent();
                        callEvent.setEventName(SocketManager.EVENT_CALL_RECONNECT);
                        callEvent.setMessageObject(object);
                        Log.e(TAG, "sc_call_reconnect_hold" + object);
                        EventBus.getDefault().post(callEvent);
                    }
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }
        }, 2000);
    }

    public void ReconnectInitimate() {
        JSONObject object = new JSONObject();
        try {

            object.put("from", mCurrentUserId);
            object.put("to", toUserId);
            if (mCurrentUserId.equalsIgnoreCase(toUserId)) {
                object.put("to", fromUserId);
            }
//From  and To is same
            Log.e(TAG, "fromUserId" + fromUserId);
            Log.e(TAG, "toUserId" + toUserId);
            Log.e(TAG, "mCallId" + mCallId);
            Log.e(TAG, "opponentUserId" + opponentUserId);

            SendMessageEvent callEvent = new SendMessageEvent();
            callEvent.setEventName(SocketManager.EVENT_RECONNECTINTIMATE);
            callEvent.setMessageObject(object);
            Log.e(TAG, "EVENT_RECONNECTINTIMATE" + object);
            EventBus.getDefault().post(callEvent);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void cancelTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "onRestart");
    }
}

