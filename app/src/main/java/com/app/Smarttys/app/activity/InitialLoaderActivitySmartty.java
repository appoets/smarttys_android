package com.app.Smarttys.app.activity;

import android.Manifest;
import android.accounts.AccountAuthenticatorActivity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.core.ActivityLauncher;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.service.ContactsSync;
import com.app.Smarttys.core.socket.MessageService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsResult;

import java.security.MessageDigest;

import static com.app.Smarttys.app.activity.VerifyPhoneScreen.SKIP_OTP_VERIFICATION;

/**
 * Created by Administrator on 10/7/2016.
 */
public class InitialLoaderActivitySmartty extends AccountAuthenticatorActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = InitialLoaderActivitySmartty.class.getSimpleName();
    final int PERMISSION_REQUEST_CODE = 111;
    private final int CONTACTS_REQUEST_CODE = 10;
    private final int CONTACTS1_REQUEST_CODE = 20;
    private final int AUDIO_RECORD_PERMISSION_REQUEST_CODE = 1;
    RelativeLayout rlParent;
    ImageView splash;
    Handler mHandler = new Handler();
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    String GCM_Id = "";
    boolean isSmartty, isTruemobile;
    Toast mToast;
    private boolean mShowToast;
    private Handler handleCheckStatus = null;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isSmartty = getResources().getBoolean(R.bool.is_smartty);
        isTruemobile = getResources().getBoolean(R.bool.is_truemobile);

        Log.e(TAG, "isTruemobile" + isTruemobile);

        mContext = InitialLoaderActivitySmartty.this;
        Log.e(TAG, "app_name" + getResources().getString(R.string.app_name));

        Log.e(TAG, "isSmartty" + isSmartty);
        if (getResources().getString(R.string.app_name).equalsIgnoreCase("Smarttys")) {
            setContentView(R.layout.activity_loader_smartty);
            rlParent = findViewById(R.id.rlParent);
            splash = findViewById(R.id.splash);
            /*final int sdk = android.os.Build.VERSION.SDK_INT;

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                rlParent.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.splash));

            } else {
                rlParent.setBackground(ContextCompat.getDrawable(mContext, R.drawable.splash));
            }
            rlParent.setBackgroundColor(ContextCompat.getColor(mContext, R.color.app_color));*/
        } else if (getResources().getString(R.string.app_name).equalsIgnoreCase("TrueMobile")) {

            setContentView(R.layout.activity_loader_smartty);
            splash = findViewById(R.id.splash);

            rlParent = findViewById(R.id.rlParent);
            /*final int sdk = android.os.Build.VERSION.SDK_INT;

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                //   splash.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.splash));
                //    splash.setImageResource(R.drawable.splash);

            } else {
                //  splash.setImageResource(R.drawable.splash);

                //  splash.setBackground(ContextCompat.getDrawable(mContext, R.drawable.splash));
            }
            //    splash.setBackgroundColor(ContextCompat.getColor(mContext, R.color.app_color));*/

        } else {
            setContentView(R.layout.activity_loader_smartty_new);

        }
        MyLog.d(TAG, "performance onCreate: ");

        handleCheckStatus = new Handler();

        mGoogleApiClient = new GoogleApiClient.Builder(InitialLoaderActivitySmartty.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                MyLog.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (SessionManager.getInstance(mContext).getlogin()) {
            // loadActivity(SessionManager.getInstance(mContext));

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {

                if (checkSelfPermission(Manifest.permission.READ_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED) {
                    Contactdialog(CONTACTS1_REQUEST_CODE);
                    // requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, CONTACTS_REQUEST_CODE);
                } else {

                    loadActivity(SessionManager.getInstance(mContext));
                }

            } else {

                loadActivity(SessionManager.getInstance(mContext));
            }
        } else {

            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkWriteExternalStoragePermission() || !checkphonecall() || !canRecordAudio()) {
                    requestPermission();
                } else {
                    setLocation();
                }
            } else {
                setLocation();
            }
        }
        MyLog.d(TAG, "performance onCreate: 2");
    }


    private void loadActivity(final SessionManager mSessionManager) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!mSessionManager.getlogin()) {
                    ActivityLauncher.launchVerifyPhoneScreen(InitialLoaderActivitySmartty.this);
                } else if (!mSessionManager.getnumberVerified()) {
                    if (SKIP_OTP_VERIFICATION) {
                        ActivityLauncher.launchVerifyPhoneScreen(InitialLoaderActivitySmartty.this);
                    } else
                        ActivityLauncher.launchSMSVerificationScreen(InitialLoaderActivitySmartty.this, null, null, null, null, GCM_Id);
                } else if (!mSessionManager.getIsprofileUpdate()) {
                    ActivityLauncher.launchProfileInfoScreen(InitialLoaderActivitySmartty.this, null);
                } else if (!mSessionManager.getBackupRestored()) {

                    if (!MessageService.isStarted()) {
                        //  AppUtils.startService(mContext, MessageService.class);
                        startService();
                    }
                    ActivityLauncher.launchHomeScreen(InitialLoaderActivitySmartty.this);

                } else {

                    if (!MessageService.isStarted()) {
                        startService();
                    }
                    ActivityLauncher.launchHomeScreen(InitialLoaderActivitySmartty.this);
                }
            }
        }, 700);

    }

    public void startService() {

        AppUtils.startService(InitialLoaderActivitySmartty.this, MessageService.class);

    }

    private void loadContacts() {
        CoreController.registerContactObserver();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AppUtils.isNetworkAvailable(mContext)) {


                    if (AppUtils.isServiceRunning(mContext, ContactsSync.class)) {
                        mHandler.removeCallbacksAndMessages(null);
                        if (SessionManager.getInstance(mContext).getlogin()) {
                            loadActivity(SessionManager.getInstance(mContext));
                        } else {
                            SessionManager sessionManager = SessionManager.getInstance(InitialLoaderActivitySmartty.this);
                            loadActivity(sessionManager);
                        }
                    } else {

                        int contactsCount = 0;

                        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
                        contactsCount = contactDB_sqlite.getContactsCount();
                        if (contactsCount == 0) {
                            if (!ContactsSync.isStarted)
                                //    AppUtils.startService(mContext, ContactsSync.class);
                                startService();
                        }

                        mHandler.removeCallbacksAndMessages(null);
                        if (SessionManager.getInstance(mContext).getlogin()) {
                            loadActivity(SessionManager.getInstance(mContext));
                        } else {
                            // mHandler.postDelayed(runnable, 3000);
                            SessionManager sessionManager = SessionManager.getInstance(InitialLoaderActivitySmartty.this);
                            loadActivity(sessionManager);

                        }
                        //}
                    }

                } else {
                    //     Toast.makeText(mContext, getString(R.string.networkerror), Toast.LENGTH_SHORT).show();


                    if (getString(R.string.app_name).equals("Smarttys")) {
                        if (!mShowToast) {
                            showAToast(getString(R.string.networkerror));

                            mShowToast = true;

                        }
                    } else {
                        showAToast(getString(R.string.networkerror));
                    }
                    mHandler.postDelayed(this, 3000);
                }

            }
        }, 700);

    }

    public void showAToast(String message) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        mToast.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {


            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CoreController.getLocation();
                    setLocation();
                } else {
                    finish();
                }
                break;
            case CONTACTS_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //   loadContacts();
                    setLocation();
                } else {
                    finish();
                }
                break;
            case CONTACTS1_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    loadActivity(SessionManager.getInstance(mContext));

                } else {
                    finish();
                }
                break;
            case AUDIO_RECORD_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadContacts();
                } else {
                    finish();
                }
                break;
        }

    }

    public boolean canRecordAudio() {
        int recordPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        return recordPermission == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkphonecall() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        if (CoreController.isRaad) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE
                    , Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);

        } else
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    private void setLocation() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (checkSelfPermission(Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {
                //requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, CONTACTS_REQUEST_CODE);
                Contactdialog(CONTACTS1_REQUEST_CODE);
            } else if (checkSelfPermission(Manifest.permission.RECORD_AUDIO)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, AUDIO_RECORD_PERMISSION_REQUEST_CODE);

            } else {
                loadContacts();
            }
        } else {
            loadContacts();
        }
    }

    /*private void setLocation() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (checkSelfPermission(Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, CONTACTS_REQUEST_CODE);
            } else {
                loadContacts();
            }
        } else {
            loadContacts();
        }
    }
*/
    private void AfterGpsActivesetLocation() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (checkSelfPermission(Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, CONTACTS_REQUEST_CODE);
            } else {
                loadContacts();
            }
        } else {
            loadContacts();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MyLog.e(TAG, "onActivityResult");
        if (requestCode == 9) {
            if (resultCode == RESULT_OK) {
                loadContacts();
            }
        }

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handleCheckStatus != null) {
            handleCheckStatus.removeCallbacksAndMessages(null);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void Contactdialog(final int request_code) {

        try {

            final Dialog dialog = new Dialog(InitialLoaderActivitySmartty.this);
            final AvnNextLTProDemiTextView agree, disagree;
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.contact_info_dialog_layout);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);

            LinearLayout confirm_layout = dialog.findViewById(R.id.confirm_layout);
            LinearLayout not_now_layout = dialog.findViewById(R.id.not_now_layout);

            confirm_layout.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, request_code);
                }
            });

            not_now_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    finish();
                }
            });

            dialog.show();

        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

    }
}
