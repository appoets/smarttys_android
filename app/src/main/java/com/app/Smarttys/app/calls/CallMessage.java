package com.app.Smarttys.app.calls;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.smarttyhelperclass.SmarttyUtilities;

import org.appspot.apprtc.CallActivity;
import org.appspot.apprtc.WebrtcConstants;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by CAS60 on 7/18/2017.
 */
public class CallMessage {

    public static final long CALL_CLICK_TIMEOUT = 15000;
    private static final String TAG = CallMessage.class.getSimpleName() + ">>>@@";
    public static boolean isAlreadyCallClick;
    // Remove retry call event until opponent user gets new call notification
    public static String arrivedCallId = "";
    private Context context;
    private String id, mCurrentUserId, tsForServerEpoch, tsForServer;

    public CallMessage(Context context) {
        this.context = context;
        mCurrentUserId = SessionManager.getInstance(context).getCurrentUserID();

    }

    public static JSONObject getCallStatusObject(String from, String to, String id,
                                                 String callDocId, String recordId, int callStatus, String type) {
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("id", id);
            object.put("toDocId", callDocId);
            object.put("recordId", recordId);
            object.put("type", type);
            object.put("call_status", callStatus + "");
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return object;
    }

    public static JSONObject getOpponentCallConnectedObject(String from, String to, String callDocId,
                                                            String recordId, int connectStatus) {
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("toDocId", callDocId);
            object.put("recordId", recordId);
            object.put("connected_status", connectStatus);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return object;
    }

    public static void openCallScreen(Context context, String from, String to, String id, String roomId,
                                      String opponentUserProfilePic, String msisdn, String callConnect,
                                      boolean isVideoCall, boolean isOutgoingCall, String ts, String mReconnecting) {
        MyLog.d(TAG, "openCallScreen: start");
        if (!CallsActivity.isStarted) {
            if (isOutgoingCall) {
                CallsActivity.opponentUserId = to;
            }

            PreferenceManager.setDefaultValues(context, org.appspot.apprtc.R.xml.preferences, false);
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

            String keyprefRoomServerUrl = context.getString(org.appspot.apprtc.R.string.pref_room_server_url_key);
            boolean isTurnServerEnabled = WebrtcConstants.isTurnServerEnabled;

            String roomUrlDefault = context.getString(org.appspot.apprtc.R.string.pref_room_server_url_default);
            if (isTurnServerEnabled)
                roomUrlDefault = WebrtcConstants.OWN_TURN_SERVER;

            // TODO: 16/04/2019 webrtc preference url replaced here with static url
            String roomUrl = sharedPref.getString(keyprefRoomServerUrl, roomUrlDefault);


            MyLog.d(TAG, "openCallScreen: 1");

            int videoWidth = 0;
            int videoHeight = 0;
            String resolution = context.getString(org.appspot.apprtc.R.string.pref_resolution_default);
            String[] dimensions = resolution.split("[ x]+");
            if (dimensions.length == 2) {
                try {
                    videoWidth = Integer.parseInt(dimensions[0]);
                    videoHeight = Integer.parseInt(dimensions[1]);
                } catch (NumberFormatException e) {
                    videoWidth = 0;
                    videoHeight = 0;
                    MyLog.e("SmarttyCallError", "Wrong video resolution setting: " + resolution);
                }
            }
            MyLog.d(TAG, "openCallScreen: 2");
            Uri uri = Uri.parse(roomUrl);
            Intent intent = new Intent(context, CallsActivity.class);

            intent.setData(uri);
            if (!AppUtils.isEmpty(mReconnecting)) {
                intent.putExtra(CallsActivity.RECONNECTING, mReconnecting);

            }
            intent.putExtra(CallsActivity.EXTRA_IS_OUTGOING_CALL, isOutgoingCall);
            intent.putExtra(CallsActivity.EXTRA_DOC_ID, id);
            intent.putExtra(CallsActivity.EXTRA_FROM_USER_ID, from);
            intent.putExtra(CallsActivity.EXTRA_TO_USER_ID, to);
            intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, msisdn);
            intent.putExtra(CallsActivity.EXTRA_OPPONENT_PROFILE_PIC, opponentUserProfilePic);
            intent.putExtra(CallsActivity.EXTRA_NAVIGATE_FROM, context.getClass().getSimpleName()); // For navigating from call activity
            intent.putExtra(CallsActivity.EXTRA_CALL_CONNECT_STATUS, callConnect);
            intent.putExtra(CallsActivity.EXTRA_CALL_TIME_STAMP, ts);

            intent.putExtra(CallsActivity.EXTRA_ROOMID, roomId);


            Log.e(TAG, "EXTRA_IS_OUTGOING_CALL" + isOutgoingCall);

            Log.e(TAG, "EXTRA_DOC_ID" + id);

            Log.e(TAG, "EXTRA_FROM_USER_ID" + from);

            Log.e(TAG, "EXTRA_TO_USER_ID" + to);
            Log.e(TAG, "EXTRA_CALL_TIME_STAMP" + ts);

            Log.e(TAG, "EXTRA_USER_MSISDN" + msisdn);
            Log.e(TAG, "EXTRA_CALL_CONNECT_STATUS" + callConnect);

            Log.e(TAG, "EXTRA_ROOMID" + roomId);

            intent.putExtra(CallsActivity.EXTRA_LOOPBACK, false);
            intent.putExtra(CallsActivity.EXTRA_VIDEO_CALL, isVideoCall);
            intent.putExtra(CallsActivity.EXTRA_SCREENCAPTURE, false);
            intent.putExtra(CallsActivity.EXTRA_CAMERA2, true);
            intent.putExtra(CallsActivity.EXTRA_VIDEO_WIDTH, videoWidth);
            intent.putExtra(CallsActivity.EXTRA_VIDEO_HEIGHT, videoHeight);
            intent.putExtra(CallsActivity.EXTRA_VIDEO_FPS, 0);
            intent.putExtra(CallsActivity.EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED, false);
            intent.putExtra(CallsActivity.EXTRA_VIDEO_BITRATE, 0);
            intent.putExtra(CallsActivity.EXTRA_VIDEOCODEC, context.getString(org.appspot.apprtc.R.string.pref_videocodec_default));
            intent.putExtra(CallsActivity.EXTRA_HWCODEC_ENABLED, false);
            intent.putExtra(CallsActivity.EXTRA_CAPTURETOTEXTURE_ENABLED, true);
            intent.putExtra(CallsActivity.EXTRA_FLEXFEC_ENABLED, false);
            intent.putExtra(CallsActivity.EXTRA_NOAUDIOPROCESSING_ENABLED, false);
            intent.putExtra(CallsActivity.EXTRA_AECDUMP_ENABLED, false);

            intent.putExtra(CallsActivity.EXTRA_OPENSLES_ENABLED, false);
            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AEC, false);
            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AGC, false);
            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_NS, false);
            intent.putExtra(CallsActivity.EXTRA_ENABLE_LEVEL_CONTROL, false);
            intent.putExtra(CallsActivity.EXTRA_AUDIO_BITRATE, 0);
            intent.putExtra(CallsActivity.EXTRA_AUDIOCODEC, context.getString(org.appspot.apprtc.R.string.pref_audiocodec_default));
            intent.putExtra(CallsActivity.EXTRA_DISPLAY_HUD, false);
            intent.putExtra(CallsActivity.EXTRA_TRACING, false);
            intent.putExtra(CallsActivity.EXTRA_CMDLINE, false);
            intent.putExtra(CallsActivity.EXTRA_RUNTIME, 0);

            intent.putExtra(CallActivity.EXTRA_DATA_CHANNEL_ENABLED, true);
            intent.putExtra(CallActivity.EXTRA_ORDERED, true);
            intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS_MS, -1);
            intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS, -1);
            intent.putExtra(CallActivity.EXTRA_PROTOCOL, context.getString(org.appspot.apprtc.R.string.pref_data_protocol_default));
            intent.putExtra(CallActivity.EXTRA_NEGOTIATED, false);
            intent.putExtra(CallActivity.EXTRA_ID, -1);

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);


            //overridePendingTransition(0, 0);

            MyLog.d(TAG, "openCallScreen: 3");
//            context.startService(intent);
        }
    }

/*    public CallItemChat getCallCount(String currentUserId, String receiverId, CallItemChat currentCallItem) {
        MessageDbController dbController = CoreController.getDBInstance(context);
        CallItemChat topCallItem = dbController.selectTopCallLog(currentUserId);

        String callAtObj = null;
        if (topCallItem != null && topCallItem.isSelf() == currentCallItem.isSelf()
                && topCallItem.getOpponentUserId().equalsIgnoreCase(receiverId)
                && topCallItem.getCallType().equals(currentCallItem.getCallType())) {
            callAtObj = topCallItem.getCalledAtObj();
        }

        try {
            JSONArray arrTimes;
            if (callAtObj == null || callAtObj.equals("")) {
                arrTimes = new JSONArray();
            } else {
                arrTimes = new JSONArray(callAtObj);
            }
            arrTimes.put(currentCallItem.getTS());
            currentCallItem.setCallCount(arrTimes.length());
            currentCallItem.setCalledAtObj(arrTimes.toString());
        } catch (JSONException e) {
            Log.e(TAG,"",e);
        }

        return currentCallItem;
    }*/

    public static void resumeCall(Context context) {
        Intent callIntent = new Intent(context, CallsActivity.class);
        callIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(callIntent);
    }

/*
    public static JSONObject getCallStatusObject(String from, String to, String id,
                                                 String callDocId, String recordId, int callStatus) {
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("id", id);
            object.put("toDocId", callDocId);
            object.put("recordId", recordId);
            object.put("call_status", callStatus + "");
        } catch (JSONException e) {
            Log.e(TAG,"",e);
        }
        return object;
    }
*/

    public static void setCallClickTimeout() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    isAlreadyCallClick = false;
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            }
        }, CALL_CLICK_TIMEOUT);

        isAlreadyCallClick = true;
    }

    public Object getMessageObject(String to, int callType) {
        tsForServer = SmarttyUtilities.tsInGmt();
        tsForServerEpoch = new SmarttyUtilities().gmtToEpoch(tsForServer);
        setId(mCurrentUserId + "-" + to);

        JSONObject object = new JSONObject();
        try {
            object.put("from", mCurrentUserId);
            object.put("to", to);
            object.put("type", callType);
            object.put("id", tsForServerEpoch);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);
            return object;
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
            return null;
        }
    }

    public String getId() {
        return id;
    }



   /* public static void ReConnectopenCallScreen(Context context, String from, String to, String id, String roomId,
                                      String opponentUserProfilePic, String msisdn, String callConnect,
                                      boolean isVideoCall, boolean isOutgoingCall, String ts) {
        MyLog.d(TAG, "openCallScreen: start");
        if (!CallsActivity.isStarted) {
            if (isOutgoingCall) {
                CallsActivity.opponentUserId = to;
            }

            PreferenceManager.setDefaultValues(context, org.appspot.apprtc.R.xml.preferences, false);
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

            String keyprefRoomServerUrl = context.getString(org.appspot.apprtc.R.string.pref_room_server_url_key);
            boolean isTurnServerEnabled = WebrtcConstants.isTurnServerEnabled;

            String roomUrlDefault=context.getString(org.appspot.apprtc.R.string.pref_room_server_url_default);
            if(isTurnServerEnabled)
                roomUrlDefault= WebrtcConstants.OWN_TURN_SERVER;

            // TODO: 16/04/2019 webrtc preference url replaced here with static url
            String roomUrl = sharedPref.getString(keyprefRoomServerUrl, roomUrlDefault);


            MyLog.d(TAG, "openCallScreen: 1");

            int videoWidth = 0;
            int videoHeight = 0;
            String resolution = context.getString(org.appspot.apprtc.R.string.pref_resolution_default);
            String[] dimensions = resolution.split("[ x]+");
            if (dimensions.length == 2) {
                try {
                    videoWidth = Integer.parseInt(dimensions[0]);
                    videoHeight = Integer.parseInt(dimensions[1]);
                } catch (NumberFormatException e) {
                    videoWidth = 0;
                    videoHeight = 0;
                    MyLog.e("SmarttyCallError", "Wrong video resolution setting: " + resolution);
                }
            }
            MyLog.d(TAG, "openCallScreen: 2");
            Uri uri = Uri.parse(roomUrl);
            Bundle intent = new Bundle();
         //   Intent intent = new Intent(context, CallsActivity.class);

            intent.(uri);

            intent.putBoolean(CallsActivity.EXTRA_IS_OUTGOING_CALL, isOutgoingCall);
            intent.putString(CallsActivity.EXTRA_DOC_ID, id);
            intent.putString(CallsActivity.EXTRA_FROM_USER_ID, from);
            intent.putString(CallsActivity.EXTRA_TO_USER_ID, to);
            intent.putString(CallsActivity.EXTRA_USER_MSISDN, msisdn);
            intent.putString(CallsActivity.EXTRA_OPPONENT_PROFILE_PIC, opponentUserProfilePic);
            intent.putString(CallsActivity.EXTRA_NAVIGATE_FROM, context.getClass().getSimpleName()); // For navigating from call activity
            intent.putString(CallsActivity.EXTRA_CALL_CONNECT_STATUS, callConnect);
            intent.putString(CallsActivity.EXTRA_CALL_TIME_STAMP, ts);

            intent.putString(CallsActivity.EXTRA_ROOMID, roomId);
            intent.putString(CallsActivity.EXTRA_LOOPBACK, false);
            intent.putString(CallsActivity.EXTRA_VIDEO_CALL, isVideoCall);
            intent.putString(CallsActivity.EXTRA_SCREENCAPTURE, false);
            intent.putString(CallsActivity.EXTRA_CAMERA2, true);
            intent.putString(CallsActivity.EXTRA_VIDEO_WIDTH, videoWidth);
            intent.putString(CallsActivity.EXTRA_VIDEO_HEIGHT, videoHeight);
            intent.putString(CallsActivity.EXTRA_VIDEO_FPS, 0);
            intent.putString(CallsActivity.EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED, false);
            intent.putString(CallsActivity.EXTRA_VIDEO_BITRATE, 0);
            intent.putString(CallsActivity.EXTRA_VIDEOCODEC, context.getString(org.appspot.apprtc.R.string.pref_videocodec_default));
            intent.putString(CallsActivity.EXTRA_HWCODEC_ENABLED, false);
            intent.putString(CallsActivity.EXTRA_CAPTURETOTEXTURE_ENABLED, true);
            intent.putString(CallsActivity.EXTRA_FLEXFEC_ENABLED, false);
            intent.putString(CallsActivity.EXTRA_NOAUDIOPROCESSING_ENABLED, false);
            intent.putString(CallsActivity.EXTRA_AECDUMP_ENABLED, false);

            intent.putString(CallsActivity.EXTRA_OPENSLES_ENABLED, false);
            intent.putString(CallsActivity.EXTRA_DISABLE_BUILT_IN_AEC, false);
            intent.putString(CallsActivity.EXTRA_DISABLE_BUILT_IN_AGC, false);
            intent.putString(CallsActivity.EXTRA_DISABLE_BUILT_IN_NS, false);
            intent.putString(CallsActivity.EXTRA_ENABLE_LEVEL_CONTROL, false);
            intent.putString(CallsActivity.EXTRA_AUDIO_BITRATE, 0);
            intent.putString(CallsActivity.EXTRA_AUDIOCODEC, context.getString(org.appspot.apprtc.R.string.pref_audiocodec_default));
            intent.putString(CallsActivity.EXTRA_DISPLAY_HUD, false);
            intent.putString(CallsActivity.EXTRA_TRACING, false);
            intent.putString(CallsActivity.EXTRA_CMDLINE, false);
            intent.putString(CallsActivity.EXTRA_RUNTIME, 0);

            intent.putString(CallActivity.EXTRA_DATA_CHANNEL_ENABLED, true);
            intent.putString(CallActivity.EXTRA_ORDERED, true);
            intent.putString(CallActivity.EXTRA_MAX_RETRANSMITS_MS, -1);
            intent.putString(CallActivity.EXTRA_MAX_RETRANSMITS, -1);
            intent.putString(CallActivity.EXTRA_PROTOCOL, context.getString(org.appspot.apprtc.R.string.pref_data_protocol_default));
            intent.putString(CallActivity.EXTRA_NEGOTIATED, false);
            intent.pu(CallActivity.EXTRA_ID, -1);

          //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
          //  context.startActivity(intent);
            MyLog.d(TAG, "openCallScreen: 3");
//            context.startService(intent);
        }
    }

*/

    public void setId(String id) {
        this.id = id;
    }

    public String getShortTimeFormat() {
        long deviceTS = Calendar.getInstance().getTimeInMillis();
        long timeDiff = SessionManager.getInstance(context).getServerTimeDifference();

        long localTime = deviceTS - timeDiff;
        return String.valueOf(localTime);
    }

    public String getroomid() {
        tsForServer = SmarttyUtilities.tsInGmt();
        tsForServerEpoch = new SmarttyUtilities().gmtToEpoch(tsForServer);
        return tsForServerEpoch;
    }
}
