package com.app.Smarttys.app.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.MimeTypeMap;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.ForwardContactAdapter;
import com.app.Smarttys.app.adapter.RItemAdapter;
import com.app.Smarttys.app.utils.ChatListUtil;
import com.app.Smarttys.app.utils.ForwardFileUtil;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.Keys;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.ActivityLauncher;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.AudioMessage;
import com.app.Smarttys.core.message.DocumentMessage;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.message.PictureMessage;
import com.app.Smarttys.core.message.TextMessage;
import com.app.Smarttys.core.message.VideoMessage;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.service.Constants;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.core.uploadtoserver.FilePathUtils;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;
import com.app.Smarttys.status.model.StatusModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by CAS60 on 5/18/2017.
 */
public class ShareFromThirdPartyAppActivity extends AppCompatActivity {

    private static final String TAG = ShareFromThirdPartyAppActivity.class.getSimpleName();
    RecyclerView lvContacts, rvFreqContact;
    ForwardContactAdapter adapter, frequentAdapter;
    AvnNextLTProRegTextView resevernameforward;
    ImageView sendmessage;
    RelativeLayout Sendlayout;
    String mCurrentUserId, textMsgFromVendor;
    ArrayList<SmarttyContactModel> grouplist = new ArrayList<>();
    ArrayList<SmarttyContactModel> frequentList = new ArrayList<>();
    ArrayList<String> userIdList = new ArrayList<>();
    ContactsRefreshReceiver mContactReceiver;
    private SessionManager sessionManager;
    private Session session;
    private List<SmarttyContactModel> selectedContactsList;
    private ArrayList<SmarttyContactModel> dataList;
    private TextView tvFrequentLbl;
    private FileUploadDownloadManager uploadDownloadManager;
    private SearchView searchView;
    //    private List<Uri> uriList;
    //    private int messageType = 100;
    private Intent receivedIntent;
    private ArrayList<SmarttyContactModel> smarttyEntries;
    ContactsRefreshReceiver contactsRefreshReceiver = new ContactsRefreshReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);

            if (frequentList.size() == 0 && smarttyEntries.size() == 0) {
                loadContactsFromDB();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forward_with_freq_contact);
        MyLog.d(TAG, "onCreate: filetest");
        //initProgress("Connecting...",false);
        //showProgressDialog();

/*        if(!AppUtils.isServiceRunning(this,MessageService.class)) {
            MyLog.d(TAG, "onCreate: filetest service not start");
            AppUtils.startService(this, MessageService.class);
        }*/

        sendmessage = findViewById(R.id.overlapImage);
        resevernameforward = findViewById(R.id.chat_text_view);
        uploadDownloadManager = new FileUploadDownloadManager(ShareFromThirdPartyAppActivity.this);
        sessionManager = SessionManager.getInstance(this);

/*    if (!sessionManager.isValidDevice()) {

        Toast.makeText(this,"Your session expired. Please login again!", Toast.LENGTH_SHORT).show();
        MessageService.clearDB(this);
        sessionManager.logoutUser(false);
        finish();
    }*/

        mCurrentUserId = sessionManager.getCurrentUserID();
        session = new Session(this);
        setTitle("Send to..");

        tvFrequentLbl = findViewById(R.id.tvFrequentLbl);

        lvContacts = findViewById(R.id.listContacts);
        LinearLayoutManager mediaManager = new LinearLayoutManager(ShareFromThirdPartyAppActivity.this, LinearLayoutManager.VERTICAL, false);
        lvContacts.setLayoutManager(mediaManager);
        lvContacts.setNestedScrollingEnabled(false);
        rvFreqContact = findViewById(R.id.rvFreqContact);
        LinearLayoutManager freqManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvFreqContact.setLayoutManager(freqManager);
        rvFreqContact.setNestedScrollingEnabled(false);

        /*        initProgress("Loading contacts...", true);*/

        //grouplist
        grouplist = ChatListUtil.getGroupList(this);
        loadContactsFromDB();
        selectedContactsList = new ArrayList<>();

        lvContacts.addOnItemTouchListener(new RItemAdapter(this, lvContacts, new RItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                SmarttyContactModel userData = null;
                try {
                    userData = adapter.getItem(position);
                } catch (Exception e) {
                    MyLog.e(TAG, "onItemClick: ", e);
                }
                if (userData != null) {
                    userData.setSelected(!userData.isSelected());

                    if (userData.isSelected()) {
                        selectedContactsList.add(userData);
                    } else {
                        selectedContactsList.remove(userData);
                    }

                    dataList.set(position, userData);
                    adapter.notifyDataSetChanged();
                }
                if (selectedContactsList.size() == 0) {
                    Sendlayout.setVisibility(View.GONE);
                    sendmessage.setVisibility(View.GONE);
                } else {

                    Sendlayout.setVisibility(View.VISIBLE);
                    sendmessage.setVisibility(View.VISIBLE);

                    Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                            R.anim.bottom_up);
                    Sendlayout.setAnimation(animation);

                    StringBuilder sb = new StringBuilder();
                    int nameIndex = 0;
                    for (SmarttyContactModel contact : selectedContactsList) {
                        sb.append(contact.getFirstName());
                        nameIndex++;
                        if (selectedContactsList.size() > nameIndex) {
                            sb.append(",");
                        }
                    }

                    resevernameforward.setText(sb);

                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        rvFreqContact.addOnItemTouchListener(new RItemAdapter(this, rvFreqContact, new RItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                try {
                    if (position >= 0 && frequentList != null) {
                        SmarttyContactModel userData = frequentList.get(position);
                        if (userData != null) {
                            userData.setSelected(!userData.isSelected());

                            if (userData.isSelected()) {
                                selectedContactsList.add(userData);
                            } else {
                                selectedContactsList.remove(userData);
                            }

                            frequentList.set(position, userData);
                            frequentAdapter.notifyDataSetChanged();
                        }
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "onItemClick: ", e);
                }
                if (selectedContactsList.size() == 0) {
                    Sendlayout.setVisibility(View.GONE);
                    sendmessage.setVisibility(View.GONE);
                } else {

                    Sendlayout.setVisibility(View.VISIBLE);
                    sendmessage.setVisibility(View.VISIBLE);

                    Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                            R.anim.bottom_up);
                    Sendlayout.setAnimation(animation);

                    StringBuilder sb = new StringBuilder();
                    int nameIndex = 0;
                    for (SmarttyContactModel contact : selectedContactsList) {
                        sb.append(contact.getFirstName());
                        nameIndex++;
                        if (selectedContactsList.size() > nameIndex) {
                            sb.append(",");
                        }
                    }

                    resevernameforward.setText(sb);
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        receivedIntent = getIntent();

        Sendlayout = findViewById(R.id.sendlayout);

        sendmessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (selectedContactsList.size() > 0 && sessionManager.isValidDevice()) {
                if (selectedContactsList.size() > 0) {
                    sendmessage.setEnabled(false); //for disable duplicate clicks
                    sendMessage();
                }
            }
        });

        sessionManager = SessionManager.getInstance(this);
        MyLog.d(TAG, "onCreate: end");

    }

    private void sendMessage() {
        String shareAction = receivedIntent.getAction();
        String type = receivedIntent.getType();

        if (type != null) {

            if (Intent.ACTION_SEND.equals(shareAction)) {
                Uri uri = null;
                try {
                    uri = (Uri) receivedIntent.getExtras().get(Intent.EXTRA_STREAM);
                    textMsgFromVendor = receivedIntent.getExtras().getString(Intent.EXTRA_TEXT, "");
                } catch (Exception e) {
                    MyLog.e(TAG, "sendMessage: ", e);
                }
                if (uri == null) {
                    textMsgFromVendor = receivedIntent.getExtras().getString(Intent.EXTRA_TEXT, "");
                    sendTextMessage();
                } else {
                    if (textMsgFromVendor == null)
                        textMsgFromVendor = "";
                    if (type.startsWith("image/gif")) {
                        Toast.makeText(this, "GIF not supported", Toast.LENGTH_LONG).show();
                        finish();
                        return;

                    }
                    if (type.startsWith("image/")) {
                        sendImageChatMessage(uri, textMsgFromVendor);
                    } else if (type.startsWith("audio/")) {
                        sendAudioMessage(uri);
                    } else if (type.startsWith("video/")) {
                        sendVideoChatMessage(uri, textMsgFromVendor);
                    } else if (type.startsWith("application/") || type.startsWith("text/plain")) {
                        sendDocumentMessage(uri);
                    }
                }
            } else if (Intent.ACTION_SEND_MULTIPLE.equals(shareAction)) {

                ArrayList<Parcelable> list = getIntent().getParcelableArrayListExtra(Intent.EXTRA_STREAM);
                for (Parcelable parcel : list) {
                    Uri uri = (Uri) parcel;
                    String mimeType = "";

                    String extension = getFileExtnFromPath(uri.getPath());
                    if (extension != null) {
                        if (extension.startsWith(".3g")) {
                            if (isVideoFile(uri.getPath())) {
                                mimeType = "video/3gpp";
                            } else {
                                mimeType = "audio/3gpp";
                            }
                        } else {
                            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                        }
                    }

                    if (mimeType != null) {
                        if (mimeType.startsWith("image/")) {
                            sendImageChatMessage(uri, textMsgFromVendor);
                        } else if (mimeType.startsWith("audio/")) {
                            sendAudioMessage(uri);
                        } else if (mimeType.startsWith("video/")) {
                            sendVideoChatMessage(uri, textMsgFromVendor);
                        } else if (mimeType.startsWith("application/") || type.startsWith("text/plain")) {
                            sendDocumentMessage(uri);
                        }
                    }
                }
            } else if (shareAction != null && shareAction.equals(Keys.ACTION_STATUS_MESSAGES)) {
                Bundle bundle = getIntent().getExtras();
                ArrayList<StatusModel> list =
                        (ArrayList<StatusModel>) bundle.getSerializable(Intent.EXTRA_TEXT);
                if (list != null)
                    for (StatusModel statusModel : list) {
                        if (statusModel != null) {
                            if (statusModel.isImage()) {
                                sendImageChatMessage(Uri.fromFile(new File(statusModel.getLocalPath())), statusModel.getCaption());
                            } else if (statusModel.isVideo()) {
                                sendVideoChatMessage(Uri.fromFile(new File(statusModel.getLocalPath())), statusModel.getCaption());

                            }
                        }
                    }
            }

            Constants.IS_FROM_SHARING_PAGE = true;
            ActivityLauncher.launchHomeScreen(ShareFromThirdPartyAppActivity.this);
        }

    }

    public String getFileExtnFromPath(String fileName) {
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    private boolean isVideoFile(String path) {
        int height = 0;
        try {
            File file = new File(path);
            MediaPlayer mp = new MediaPlayer();
            FileInputStream fs;
            FileDescriptor fd;
            fs = new FileInputStream(file);
            fd = fs.getFD();
            mp.setDataSource(fd);
            mp.prepare();
            height = mp.getVideoHeight();
            mp.release();
        } catch (Exception e) {
            MyLog.e("test", "Exception trying to determine if 3gp file is video.", e);
        }
        return height > 0;
    }

    private void loadContactsFromDB() {
        List<String> lists = session.getBlockedIds();

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        smarttyEntries = contactDB_sqlite.getSavedSmarttyContacts();

        List<SmarttyContactModel> frequentContact = contactDB_sqlite.getFrequentContacts(this, mCurrentUserId);

        if (frequentContact.size() > 0) {
            tvFrequentLbl.setVisibility(View.VISIBLE);
            for (SmarttyContactModel data : frequentContact) {
                frequentList.add(data);
                userIdList.add(data.get_id());
            }
            frequentAdapter = new ForwardContactAdapter(this, frequentList);
            rvFreqContact.setAdapter(frequentAdapter);
        }

        dataList = new ArrayList<>();
        if (lists != null && lists.size() != 0) {
//                for (int i = 0; i < lists.size(); i++) {
            for (SmarttyContactModel contact : smarttyEntries) {
                if (!lists.contains(contact.get_id()) && !userIdList.contains(contact.get_id())) {
                    dataList.add(contact);
                }
            }

            for (SmarttyContactModel contact : grouplist) {
                if (!lists.contains(contact.get_id()) && !userIdList.contains(contact.get_id())) {
                    dataList.add(contact);
                }
            }
//                }
        } else {
            for (SmarttyContactModel contact : smarttyEntries) {
                if (!userIdList.contains(contact.get_id())) {
                    dataList.add(contact);
                }
            }
            for (SmarttyContactModel contact : grouplist) {
                if (!userIdList.contains(contact.get_id())) {
                    dataList.add(contact);
                }
            }
        }

        Collections.sort(dataList, Getcontactname.nameAscComparator);
        adapter = new ForwardContactAdapter(this, dataList);
        lvContacts.setAdapter(adapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (SocketManager.EVENT_GET_CONTACTS.equalsIgnoreCase(event.getEventName())) {
            Object[] args = event.getObjectsArray();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_forward_contact, menu);
        MenuItem searchItem = menu.findItem(R.id.chats_searchIcon);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.setIconifiedByDefault(true);
                searchView.setIconified(true);
                searchView.setQuery("", false);
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.equals("") && newText.isEmpty()) {
                    searchView.clearFocus();
                }
                adapter.getFilter().filter(newText);
                if (frequentAdapter != null) {
                    frequentAdapter.getFilter().filter(newText);
                }
                return false;
            }
        });

        searchView.setIconifiedByDefault(true);
        searchView.setQuery("", false);
        searchView.clearFocus();
        searchView.setIconified(true);

        AutoCompleteTextView searchTextView = searchView.findViewById(R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, 0); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void sendTextMessage() {

        // Text Message max length
        if (textMsgFromVendor.length() > 1024) {
            textMsgFromVendor = textMsgFromVendor.substring(0, 1023);
        }

        MessageDbController db = CoreController.getDBInstance(this);

        for (int i = 0; i < selectedContactsList.size(); i++) {
            String to = selectedContactsList.get(i).get_id();
            String receiverMsisdn = selectedContactsList.get(i).getNumberInDevice();
            SmarttyContactModel userData = selectedContactsList.get(i);
            if (session.getarchivecount() != 0) {
                if (session.getarchive(mCurrentUserId + "-" + to))
                    session.removearchive(mCurrentUserId + "-" + to);
            }
            if (session.getarchivecountgroup() != 0) {
                if (session.getarchivegroup(mCurrentUserId + "-" + to + "-g"))
                    session.removearchivegroup(mCurrentUserId + "-" + to + "-g");
            }
            SendMessageEvent messageEvent = new SendMessageEvent();
            TextMessage message = (TextMessage) MessageFactory.getMessage(MessageFactory.text, this);
            JSONObject msgObj;
            if (userData.isGroup()) {
                messageEvent.setEventName(SocketManager.EVENT_GROUP);
                msgObj = (JSONObject) message.getGroupMessageObject(userData.get_id(), textMsgFromVendor, userData.getFirstName());

                try {
                    msgObj.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE);
                    msgObj.put("userName", userData.getFirstName());
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
                messageEvent.setMessageObject(msgObj);
                MessageItemChat item = message.createMessageItem(true, textMsgFromVendor, MessageFactory.DELIVERY_STATUS_NOT_SENT, userData.get_id(), userData.getFirstName());
                item.setGroupName(userData.getFirstName());

                db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);


            } else {
                msgObj = (JSONObject) message.getMessageObject(to, textMsgFromVendor, false);
                messageEvent.setEventName(SocketManager.EVENT_MESSAGE);

                MessageItemChat item = message.createMessageItem(true, textMsgFromVendor,
                        MessageFactory.DELIVERY_STATUS_NOT_SENT, to, "");
                messageEvent.setMessageObject(msgObj);

                item.setSenderMsisdn(receiverMsisdn);
                item.setSenderName(receiverMsisdn);
                db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SINGLE);
            }
            EventBus.getDefault().post(messageEvent);
        }

        ActivityLauncher.launchHomeScreen(ShareFromThirdPartyAppActivity.this);
    }

    public void sendImageChatMessage(Uri uri, String caption) {

        MessageDbController db = CoreController.getDBInstance(this);

        for (int i = 0; i < selectedContactsList.size(); i++) {
            String to = selectedContactsList.get(i).get_id();
            String receiverMsisdn = selectedContactsList.get(i).getNumberInDevice();
            SmarttyContactModel userData = selectedContactsList.get(i);
            String imgPath = getImageFilePath(uri);

            if (imgPath != null && !imgPath.equals("")) {

                PictureMessage message = (PictureMessage) MessageFactory.getMessage(MessageFactory.picture, this);

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imgPath, options);
                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;
                File file = ForwardFileUtil.getValidPictureFile(imgPath);
                MessageItemChat item;
                if (userData.isGroup()) {
                    message.getGroupMessageObject(userData.get_id(), file.getPath(), userData.getFirstName());
                    item = message.createMessageItem(true, "", file.getPath(), MessageFactory.DELIVERY_STATUS_NOT_SENT,
                            userData.get_id(), userData.getFirstName(), imageWidth, imageHeight);
                } else {
                    message.getMessageObject(userData.get_id(), file.getPath(), false);
                    item = message.createMessageItem(true, caption, imgPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                            to, "", imageWidth, imageHeight);
                    item.setSenderMsisdn(receiverMsisdn);


                }


                String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(imgPath);
                String imgName = item.getMessageId() + fileExtension;
                String docId = mCurrentUserId + "-" + to;

                JSONObject uploadObj;
                if (userData.isGroup()) {
                    item.setGroupName(userData.getFirstName());
                    docId = mCurrentUserId + "-" + userData.get_id() + "-g";
                    uploadObj = (JSONObject) message.createImageUploadObject(item.getMessageId(), docId,
                            imgName, file.getPath(), userData.getFirstName(), "", MessageFactory.CHAT_TYPE_GROUP, false);
                    item.setImagePath(imgPath);
                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);
                } else {
                    uploadObj = (JSONObject) message.createImageUploadObject(item.getMessageId(), docId, imgName, imgPath,
                            "", caption, MessageFactory.CHAT_TYPE_SINGLE, false);

                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SINGLE);
                }
                uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);
            }

        }

    }

    private void sendDocumentMessage(Uri uri) {

        MessageDbController db = CoreController.getDBInstance(this);
        String docPath = uri.getPath();

        if (docPath != null && !docPath.equals("")) {
            for (int i = 0; i < selectedContactsList.size(); i++) {
                String to = selectedContactsList.get(i).get_id();
                SmarttyContactModel userData = selectedContactsList.get(i);
                String receiverMsisdn = selectedContactsList.get(i).getNumberInDevice();

                DocumentMessage message = (DocumentMessage) MessageFactory.getMessage(MessageFactory.document, this);

                message.getMessageObject(to, docPath, false);
                File file = ForwardFileUtil.getValidDocumentFile(docPath);
                MessageItemChat item = null;
                docPath = file.getPath();
                if (userData.isGroup()) {
                    message.getGroupMessageObject(userData.get_id(), docPath, userData.getFirstName());
                    item = message.createMessageItem(true, docPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                            userData.get_id(), userData.getFirstName());
                } else {
                    message.getMessageObject(userData.get_id(), docPath, false);
                    item = message.createMessageItem(true, docPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                            to, "");
                    item.setSenderMsisdn(receiverMsisdn);

                }


                String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(docPath);
                String docName = item.getMessageId() + fileExtension;

                String docId = mCurrentUserId + "-" + to;

                JSONObject uploadObj;
                if (userData.isGroup()) {
                    item.setGroupName(userData.getFirstName());
                    docId = mCurrentUserId + "-" + userData.get_id() + "-g";
                    uploadObj = (JSONObject) message.createDocUploadObject(item.getMessageId(), docId,
                            docName, docPath, userData.getFirstName(), MessageFactory.CHAT_TYPE_GROUP, false);
                } else {
                    uploadObj = (JSONObject) message.createDocUploadObject(item.getMessageId(), docId,
                            docName, docPath, "", MessageFactory.CHAT_TYPE_SINGLE, false);
                }
                uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);

                if (userData.isGroup()) {
                    item.setChatFileLocalPath(docPath);
                    item.setSenderMsisdn(userData.getNumberInDevice());
                    item.setSenderName(userData.getFirstName());
                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);
                } else {
                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SINGLE);
                }
            }

        }
    }

    private void sendVideoChatMessage(Uri uri, String caption) {
        MessageDbController db = CoreController.getDBInstance(this);

        String videoPath = getVideoFilePath(uri);
        if (videoPath != null && !videoPath.equals("")) {
            Bitmap thumbBmp = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MICRO_KIND);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            thumbBmp.compress(Bitmap.CompressFormat.PNG, 10, out);
            byte[] thumbArray = out.toByteArray();
            String thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT);
            try {
                out.close();
            } catch (IOException e) {
                MyLog.e(TAG, "", e);
            }

            for (int i = 0; i < selectedContactsList.size(); i++) {
                String to = selectedContactsList.get(i).get_id();
                String receiverMsisdn = selectedContactsList.get(i).getNumberInDevice();
                SmarttyContactModel userData = selectedContactsList.get(i);
                VideoMessage message = (VideoMessage) MessageFactory.getMessage(MessageFactory.video, this);
                if (userData.isGroup()) {
                    message.getGroupMessageObject(userData.get_id(), videoPath, userData.getFirstName());
                } else {
                    message.getMessageObject(userData.get_id(), videoPath, false);
                }

                MessageItemChat item = message.createMessageItem(true, videoPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                        to, "", caption);


                if (thumbData != null) {
                    item.setThumbnailData(thumbData);
                }


                String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(videoPath);
                String videoName = item.getMessageId() + fileExtension;


                JSONObject uploadObj;
                String docId;
                if (userData.isGroup()) {
                    item.setGroupName(userData.getFirstName());
                    docId = mCurrentUserId + "-" + userData.get_id() + "-g";
                    item.setVideoPath(videoPath);
                    uploadObj = (JSONObject) message.createVideoUploadObject(item.getMessageId(), docId,
                            videoName, videoPath, userData.getFirstName(), "", MessageFactory.CHAT_TYPE_GROUP, false);
                } else {
                    docId = mCurrentUserId + "-" + to;
                    uploadObj = (JSONObject) message.createVideoUploadObject(item.getMessageId(), docId,
                            videoName, videoPath, userData.getFirstName(), "", MessageFactory.CHAT_TYPE_SINGLE, false);
                }
                uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);

                if (userData.isGroup()) {
                    item.setSenderMsisdn(userData.getNumberInDevice());
                    item.setSenderName(userData.getFirstName());

                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);
                } else {
                    item.setSenderMsisdn(receiverMsisdn);
                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SINGLE);
                }
            }
        }
    }

    private void sendAudioMessage(Uri uri) {

        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(ShareFromThirdPartyAppActivity.this, uri);
        String duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        duration = getTimeString(Long.parseLong(duration));

        MessageDbController db = CoreController.getDBInstance(this);

        String audioPath = getAudioFilePath(uri);
        if (audioPath != null && !audioPath.equals("")) {
            for (int i = 0; i < selectedContactsList.size(); i++) {
                String to = selectedContactsList.get(i).get_id();
                String receiverMsisdn = selectedContactsList.get(i).getNumberInDevice();

                AudioMessage message = (AudioMessage) MessageFactory.getMessage(MessageFactory.audio, this);
                message.getMessageObject(to, audioPath, false);

                MessageItemChat item = message.createMessageItem(true, audioPath, duration, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                        to, "", MessageFactory.AUDIO_FROM_ATTACHMENT);
                item.setSenderMsisdn(receiverMsisdn);
                item.setaudiotype(MessageFactory.AUDIO_FROM_ATTACHMENT);

                db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SINGLE);

                String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(audioPath);
                String audioName = item.getMessageId() + fileExtension;
                String docId = mCurrentUserId + "-" + to;

                JSONObject uploadObj = (JSONObject) message.createAudioUploadObject(item.getMessageId(), docId,
                        audioName, audioPath, duration, "", MessageFactory.AUDIO_FROM_ATTACHMENT,
                        MessageFactory.CHAT_TYPE_SINGLE, false);
                uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);
            }
        }

    }

    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        int hours = (int) (millis / (1000 * 60 * 60));
        int minutes = (int) ((millis % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (int) (((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000);

        buf.append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }

    private String getImageFilePath(Uri uri) {
        String path = null;
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
            if (cursor != null) {
                try {
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    path = cursor.getString(columnIndex);
                } catch (Exception e) {
                    MyLog.e(TAG, "getImageFilePath: ", e);
                } finally {
                    cursor.close();
                }

                if (path == null) {
                    path = FilePathUtils.getImageRealFilePath(ShareFromThirdPartyAppActivity.this, uri);
                }
            } else {
                path = uri.getPath();
            }
        } catch (Exception e) {
            MyLog.e(TAG, "getImageFilePath: ", e);
        }
        return path;
    }

    private String getAudioFilePath(Uri uri) {
        String path = null;

        String[] filePathColumn = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            path = cursor.getString(columnIndex);
            cursor.close();

            if (path == null) {
                path = FilePathUtils.getAudioRealFilePath(ShareFromThirdPartyAppActivity.this, uri);
            }
        } else {
            path = uri.getPath();
        }

        return path;
    }

    private String getVideoFilePath(Uri uri) {
        String path = null;

        String[] filePathColumn = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            path = cursor.getString(columnIndex);
            cursor.close();

            if (path == null) {
                path = FilePathUtils.getVideoRealFilePath(ShareFromThirdPartyAppActivity.this, uri);
            }
        } else {
            path = uri.getPath();
        }

        return path;
    }

    /*private String getFilePath(Uri uri, int messageType) {

        String path = null;

        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            path = cursor.getString(columnIndex);
            cursor.close();

            if (path == null) {
                switch (messageType) {

                    case MessageFactory.picture: {
                        path = FileUploadDownloadManager.getImageRealFilePath(ShareFromThirdPartyAppActivity.this, uri);
                    }
                    break;

                    case MessageFactory.document: {
                        path = uri.getLocalPath();
                    }
                    break;

                    case MessageFactory.audio: {
                        path = FileUploadDownloadManager.getAudioRealFilePath(ShareFromThirdPartyAppActivity.this, uri);
                    }
                    break;

                    case MessageFactory.video: {
                        path = FileUploadDownloadManager.getVideoRealFilePath(ShareFromThirdPartyAppActivity.this, uri);
                    }
                    break;
                }
            }
        } else {
            path = uri.getLocalPath();
        }

        return path;
    }*/


}

