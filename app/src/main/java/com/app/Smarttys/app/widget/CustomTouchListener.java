package com.app.Smarttys.app.widget;

import android.view.MotionEvent;
import android.view.View;

import org.jetbrains.annotations.NotNull;

import kotlin.jvm.internal.Intrinsics;

public class CustomTouchListener implements View.OnTouchListener {
    private final int screenWidth;
    private final int screenHeight;
    private float dX;
    private float dY;

    public CustomTouchListener(int screenWidth, int screenHeight) {
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }

    public boolean onTouch(@NotNull View view, @NotNull MotionEvent event) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(event, "event");
        float newX = 0.0F;
        float newY = 0.0F;
        switch (event.getAction()) {
            case 0:
                this.dX = view.getX() - event.getRawX();
                this.dY = view.getY() - event.getRawY();
                break;
            case 1:
            default:
                return true;
            case 2:
                newX = event.getRawX() + this.dX;
                newY = event.getRawY() + this.dY;
                if (newX <= (float) 0 || newX >= (float) (this.screenWidth - view.getWidth()) || newY <= (float) 0 || newY >= (float) (this.screenHeight - view.getHeight())) {
                    return true;
                }

                view.animate().x(newX).y(newY).setDuration(0L).start();
        }

        return true;
    }

    public final int getScreenWidth() {
        return this.screenWidth;
    }

    public final int getScreenHeight() {
        return this.screenHeight;
    }
}

