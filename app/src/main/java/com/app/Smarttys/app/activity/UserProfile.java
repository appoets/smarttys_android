package com.app.Smarttys.app.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.Smarttys.R;
import com.app.Smarttys.app.dialog.CustomMultiTextItemsDialog;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.ConnectivityInfo;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.app.widget.CircleImageView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.PictureMessage;
import com.app.Smarttys.core.model.MultiTextDialogPojo;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.soundcloud.android.crop.Crop;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.zelory.compressor.Compressor;

/**
 * Created by CAS63 on 12/1/2016.
 */
public class UserProfile extends CoreActivity {

    private static final String TAG = "UserProfile";
    final Context context = this;
    private final int GALLERY_REQUEST_CODE = 1;
    private final int CAMERA_REQUEST_CODE = 2;
    private final int CAMERA_PERMISSION_REQUEST_CODE = 3;
    private final int CHANGE_UNAME = 5;
    private final int CHANGE_SURNAME = 6;
    private final int CHANGE_EMAIL = 7;
    boolean mUpload = false;
    private CircleImageView profile;
    private ImageButton ibProfilePic;
    private AvnNextLTProDemiTextView notification_actionbar_1, Username, tvSurname, tvEmail;
    private AvnNextLTProRegTextView Status, phonenumber, usermessage;
    private ImageView backimg_uprofile;
    private ImageButton editStatus, ibSurname, ibEmail;
    private Uri cameraImageUri;
    private String mCurrentUserId;
    // private ProgressBar upload_progress;

    public static Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap circuleBitmap = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(circuleBitmap);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return circuleBitmap;
    }

    /* private final int REQUEST_PICK = 3;
     private final int REQUEST_CROP = 4;
 */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        String uniqueCurrentID = SessionManager.getInstance(UserProfile.this).getCurrentUserID();
        Log.e("aksadssda", "ddd " + uniqueCurrentID);
        if (savedInstanceState != null) {
            cameraImageUri = Uri.parse(savedInstanceState.getString("ImageUri"));
        } else {
            cameraImageUri = Uri.parse("");
        }

        profile = findViewById(R.id.userprofile1);
        ibProfilePic = findViewById(R.id.ibProfilePic);
        Status = findViewById(R.id.statustextview2);
        Username = findViewById(R.id.username2);
        tvSurname = findViewById(R.id.tvSurname);
        tvEmail = findViewById(R.id.tvEmail);
        usermessage = findViewById(R.id.usermessage);
        editStatus = findViewById(R.id.editStatus);
        ibSurname = findViewById(R.id.ibSurname);
        ibEmail = findViewById(R.id.ibEmail);
        backimg_uprofile = findViewById(R.id.backarrow_userprofile);
        phonenumber = findViewById(R.id.contactnumber);
        notification_actionbar_1 = findViewById(R.id.notification_actionbar_1);
        //    upload_progress = (ProgressBar) findViewById(R.id.upload_progress);
        getSupportActionBar().hide();

        backimg_uprofile.setOnClickListener(v -> {
//                ActivityLauncher.launchSettingScreen(UserProfile.this);
            finish();
        });
        final SessionManager sessionMgr = SessionManager.getInstance(UserProfile.this);
        mCurrentUserId = sessionMgr.getCurrentUserID();
        Log.e("aksadssda", "mCurrentUserId  " + mCurrentUserId);
        //String mypic = AppUtils.getProfileFilePath(this);
        //Log.d("Whatsup Image Url", sessionMgr.getUserProfilePic() + " " + mypic);
        // Bitmap mybitmap=getBitmapFromurl();
        /*if (!mypic.isEmpty()) {
            Picasso.with(UserProfile.this).load(mypic).error(
                    R.mipmap.chat_attachment_profile_default_image_frame).into(profile);
            //new getBitmapFromurl().execute(mypic);
        }*/

        if (!SessionManager.getInstance(UserProfile.this).getcurrentUserstatus().isEmpty()) {
            Status.setText(SessionManager.getInstance(UserProfile.this).getcurrentUserstatus());
        }
        Username.setText(SessionManager.getInstance(UserProfile.this).getnameOfCurrentUser());
        tvSurname.setText(SessionManager.getInstance(UserProfile.this).getKeyUserLastName());
        tvEmail.setText(SessionManager.getInstance(UserProfile.this).getnameOfEmail());
        phonenumber.setText(SessionManager.getInstance(UserProfile.this).getPhoneNumberOfCurrentUser());

        final String pic = AppUtils.getProfileFilePath(this);
        if (pic != null && !pic.isEmpty()) {
         /*   Picasso.with(this).load(pic).error(R.mipmap.chat_attachment_profile_default_image_frame)
                    .transform(new CircleTransform()).into(profile);*/

      /*   GlideUrl   glideUrl = new GlideUrl(pic,
                    new LazyHeaders.Builder()
                            .addHeader("authorization", SessionManager.getInstance(context).getSecurityToken())
                            .addHeader("requesttype", "site")
                            .addHeader("userid", SessionManager.getInstance(context).getCurrentUserID())
                            .addHeader("referer", pic)
                            .build());
            Glide.with(this).load(glideUrl).error(R.mipmap.chat_attachment_profile_default_image_frame)
                   .into(profile);*/
          /*  GlideUrl glideUrl = null;
            //       Picasso.with(NewHomeScreenActivty.this).load(path).error(R.drawable.nav_menu_background).noPlaceholder().into(ivProfilePic);
            if (AppUtils.isEncryptionEnabled(context)) {
                glideUrl = new GlideUrl(pic,
                        new LazyHeaders.Builder()
                                .addHeader("authorization", SessionManager.getInstance(context).getSecurityToken())
                                .addHeader("requesttype", "site")
                                .addHeader("userid", SessionManager.getInstance(context).getCurrentUserID())
                                .addHeader("referer", pic)
                                .build());

            } else {
                glideUrl = new GlideUrl(pic,
                        new LazyHeaders.Builder()
                                .addHeader("authorization", SessionManager.getInstance(context).getSecurityToken())
                                .addHeader("requesttype", "site")
                                .addHeader("userid", SessionManager.getInstance(context).getCurrentUserID())
                                .addHeader("referer", pic)
                                .build());
            }
            Glide
                    .with(context)
                    .load(glideUrl)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .error(R.mipmap.chat_attachment_profile_default_image_frame)
                    .into(new SimpleTarget<Bitmap>() {

                        @Override
                        public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                            // TODO Auto-generated method stub
                            profile.setImageBitmap(arg0);
                        }
                    });
*/

            Glide.with(context)
                    // .load(/*profilePicPath*/AppUtils.getGlideURL(profilePicPath, context))
                    .load(pic)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .into(new SimpleTarget<Bitmap>() {

                        @Override
                        public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                            // TODO Auto-generated method stub
                            profile.setImageBitmap(arg0);
                        }
                    });
        } else {
            profile.setImageResource(R.mipmap.chat_attachment_profile_default_image_frame);
        }

        profile.setOnClickListener(view -> {
            /*String userAvatar = SessionManager.getInstance(UserProfile.this).getUserProfilePic();
            String[] userpic = userAvatar.split("id=");
            if (!userpic[0].equalsIgnoreCase("?")) {
                if (!userAvatar.equalsIgnoreCase("")) {
                    Intent intent = new Intent(UserProfile.this, ImageZoom.class);
                    intent.putExtra("ProfilePath", userAvatar);
                    startActivity(intent);
                }
            }*/
            Intent intent = new Intent(UserProfile.this, ImageZoom.class);
            intent.putExtra("ProfilePath", AppUtils.getProfileFilePath(UserProfile.this));
            startActivity(intent);
        });

        initProgress(getString(R.string.loading_in), true);

        ibProfilePic.setOnClickListener(v -> {
            mUpload = false;
            //String userAvatar = SessionManager.getInstance(UserProfile.this).getUserProfilePic();
            //String[] userpic = userAvatar.split("id=");
            String profilePicPath = AppUtils.getProfileFilePath(UserProfile.this);
            List<MultiTextDialogPojo> labelsList = new ArrayList<>();
            MultiTextDialogPojo label = new MultiTextDialogPojo();
            label.setImageResource(R.drawable.blue_camera);
            label.setLabelText(getString(R.string.take_image_from_camera));
            labelsList.add(label);

            label = new MultiTextDialogPojo();
            label.setImageResource(R.drawable.gallery);
            label.setLabelText(getString(R.string.add_image_from_gallery));
            labelsList.add(label);

            if (profilePicPath != null && profilePicPath.length() > 0) {
                label = new MultiTextDialogPojo();
                label.setImageResource(R.drawable.gallery);
                label.setLabelText(getString(R.string.remove_profile_picture));
                labelsList.add(label);
            }

            CustomMultiTextItemsDialog dialog = new CustomMultiTextItemsDialog();
            dialog.setTitleText(getString(R.string.profile_picture));
            dialog.setNegativeButtonText(getString(R.string.cancel));
            dialog.setLabelsList(labelsList);

            dialog.setDialogItemClickListener(position -> {
                switch (position) {
                    case 0:
                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                            StrictMode.setVmPolicy(builder.build());
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    CAMERA_PERMISSION_REQUEST_CODE);
                        } else {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File cameraImageOutputFile = new File(
                                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                                    createCameraImageFileName());
                            cameraImageUri = Uri.fromFile(cameraImageOutputFile);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri);
                            intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                            startActivityForResult(intent, CAMERA_REQUEST_CODE);
                        }
                        break;
                    case 1:
                        try {
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            startActivityForResult(photoPickerIntent, GALLERY_REQUEST_CODE);
                        } catch (Exception e) {
                            MyLog.e(TAG, "onDialogItemClick: ", e);
                        }
                        break;
                    case 2:
                        if (ConnectivityInfo.isInternetConnected(getApplication())) {
                            //    upload_progress.setVisibility(View.VISIBLE);
                            FileUploadDownloadManager uploadDownloadManager = new FileUploadDownloadManager(context);
                            uploadDownloadManager.setRemovePhoto(true);
                            uploadDownloadManager.updatePropic(EventBus.getDefault());
                            showProgres();
                        } else {
                            Toast.makeText(UserProfile.this, getString(R.string.networkerror), Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            });
            dialog.show(getSupportFragmentManager(), "Profile Pic");
        });
        editStatus.setOnClickListener(v -> {
            Intent intent = new Intent(UserProfile.this, ChangeNameScreen.class);
            intent.putExtra("name", Username.getText().toString());
            startActivityForResult(intent, CHANGE_UNAME);
        });
        ibSurname.setOnClickListener(v -> {
            Intent intent = new Intent(UserProfile.this, ChangeNameScreen.class);
            intent.putExtra("surname", tvSurname.getText().toString());
            startActivityForResult(intent, CHANGE_SURNAME);
        });
        ibEmail.setOnClickListener(v -> {
            Intent intent = new Intent(UserProfile.this, ChangeNameScreen.class);
            intent.putExtra("email", tvEmail.getText().toString());
            startActivityForResult(intent, CHANGE_EMAIL);
        });
        Status.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), Status.class);
            startActivity(intent);
        });
        phonenumber.setOnClickListener(v -> {
           /* Intent intent = new Intent(getApplicationContext(), ChangeNumber.class);
            startActivity(intent);*/
        });
        initProfilePicData();
    }

    private void initProfilePicData() {
        if (getIntent().getAction() != null && getIntent().getData() != null
                && getIntent().getAction().equalsIgnoreCase(Intent.ACTION_ATTACH_DATA)) {
            String path = getIntent().getData().getPath();
            if (path == null) {
                path = getRealFilePath(getIntent());
            }

            if (path != null && !path.equals("")) {
                beginCrop(getIntent().getData());
            }
        }
    }

    private String getRealFilePath(Intent data) {
        Uri selectedImage = data.getData();
        String wholeID = DocumentsContract.getDocumentId(selectedImage);
        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];
        String[] column = {MediaStore.Images.Media.DATA};
        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = getContentResolver().
                query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);
        String filePath = "";
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    private String createCameraImageFileName() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return timeStamp + ".jpg";
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("ImageUri", cameraImageUri.toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    Uri selectedImageUri = data.getData();
                    if (ConnectivityInfo.isInternetConnected(getApplication())) {
                        beginCrop(selectedImageUri);
                    } else {
                        Toast.makeText(UserProfile.this, getString(R.string.networkerror), Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    Toast.makeText(this, "Sorry! Failed to capture image",
                            Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (ConnectivityInfo.isInternetConnected(getApplication())) {
                    beginCrop(cameraImageUri);
                } else {
                    Toast.makeText(UserProfile.this, getString(R.string.networkerror), Toast.LENGTH_SHORT).show();
                }
            } else {
                if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    Toast.makeText(this, "Sorry! Failed to capture image",
                            Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            Uri uri = Crop.getOutput(data);

            String filePath = uri.getPath();
            File image = new File(filePath);
            Bitmap compressBmp = Compressor.getDefault(UserProfile.this).compressToBitmap(image);

            profile.setImageBitmap(compressBmp);
            if (!mUpload) {
                uploadImage(compressBmp);
                mUpload = true;
            }
        } else if (resultCode == RESULT_OK && requestCode == CHANGE_UNAME) {
            boolean message = data.getBooleanExtra("name", false);
            if (message) {
                Username.setText(SessionManager.getInstance(UserProfile.this).getnameOfCurrentUser());
            }
        } else if (resultCode == RESULT_OK && requestCode == CHANGE_SURNAME) {
            boolean message = data.getBooleanExtra("surname", false);
            if (message) {
                tvSurname.setText(SessionManager.getInstance(UserProfile.this).getKeyUserLastName());
            }
        } else if (resultCode == RESULT_OK && requestCode == CHANGE_EMAIL) {
            boolean message = data.getBooleanExtra("email", false);
            if (message) {
                tvEmail.setText(SessionManager.getInstance(UserProfile.this).getnameOfEmail());
            }
        }
    }

    private void uploadImage(Bitmap circleBmp) {
        if (circleBmp != null) {
            try {
                showProgres();
                //String profileImgPath = imgDir + "/" + Calendar.getInstance().getTimeInMillis() + "_pro.jpg";
                File file = getBitmapFile(circleBmp, "pro_" + Calendar.getInstance().getTimeInMillis() + ".jpg");
                //File compressFile = Compressor.getDefault(UserProfile.this).compressToFile(file);
                //    Log.d("USER_PROFILE", "uploadImage: " + file.getAbsolutePath());
                //     Log.d("USER_PROFILE", "file exist: " + file.exists());

                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + file.getAbsolutePath())));
                String serverFileName = mCurrentUserId.concat(".jpg");
                PictureMessage message = new PictureMessage(UserProfile.this);
                JSONObject object = (JSONObject) message.createUserProfileImageObject(serverFileName, file.getAbsolutePath());

                MessageDbController db = CoreController.getDBInstance(context);
                try {
                    db.userpicinsertitem(serverFileName, "uploading", object);
                    //    db.FirsttimeFileUploadinsert(serverFileName, "firsttimestoredb", object);

                    Log.e(TAG, "userpicinsertitem" + object);
                    // setUploadProgress(msgId, 1, object);
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
                FileUploadDownloadManager fileUploadDownloadMgnr = new FileUploadDownloadManager(UserProfile.this);
                Log.d(TAG, "onClick: startFileUpload15");
                fileUploadDownloadMgnr.startFileUpload(EventBus.getDefault(), object);
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
        }
    }

    private File getBitmapFile(Bitmap imageToSave, String fileName) {
        String path;
        path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).toString();

        File file = new File(path + "/" + fileName);

        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        //  hideProgressDialog();

        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_IMAGE_UPLOAD)) {
            hidepDialog();
            Object[] array = event.getObjectsArray();
            String path = "";
            try {
                JSONObject objects = new JSONObject(array[0].toString());
                Log.e(TAG, "objects" + objects);
                String err = objects.getString("err");
                String message = objects.getString("message");
//                String removePhoto = objects.getString("removePhoto");
                String from = objects.getString("from");
                String type = objects.getString("type");

                String removePhoto = null;
                if (objects.has("removePhoto")) {
                    removePhoto = objects.getString("removePhoto");

                    if (removePhoto.equalsIgnoreCase("yes")) {
                        //    message = "Profile image removed successfully";
                        Toast.makeText(context, getString(R.string.profile_image_removed_successfully), Toast.LENGTH_SHORT).show();
                    }
                }

                if (from.equalsIgnoreCase(SessionManager.getInstance(UserProfile.this).getCurrentUserID()) && type.equalsIgnoreCase("single")) {
                    if (!objects.getString("file").equals("") || removePhoto != null) {
                        path = objects.getString("file") + "?id=" + Calendar.getInstance().getTimeInMillis();
                        final String finalPath = AppUtils.getValidProfilePath(path);
                        if (removePhoto.equalsIgnoreCase("yes")) {
                            //  message = "Profile image removed successfully";
                            mUpload = false;
                            SessionManager.getInstance(UserProfile.this).setUserProfilePic("");
                            profile.setImageResource(R.mipmap.chat_attachment_profile_default_image_frame);
                        } else {
                            mUpload = false;
                            SessionManager.getInstance(UserProfile.this).setUserProfilePic(finalPath);
                            Glide
                                    .with(context)
                                    .load(/*profilePicPath*/AppUtils.getGlideURL(finalPath, context))
                                    .asBitmap()
                                    .error(R.mipmap.chat_attachment_profile_default_image_frame)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .dontAnimate()
                                    .into(new SimpleTarget<Bitmap>() {

                                        @Override
                                        public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                            // TODO Auto-generated method stub
                                            profile.setImageBitmap(arg0);
                                            Toast.makeText(UserProfile.this, R.string.profileupdated, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                        //   SessionManager.getInstance(UserProfile.this).setUserProfilePic(finalPath);
                    }
                }
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CHANGE_PROFILE_STATUS)) {
            Object[] array = event.getObjectsArray();
            try {
                JSONObject object = new JSONObject(array[0].toString());

                String err = object.getString("err");
                if (err.equalsIgnoreCase("0")) {

                    String from = object.getString("from");
                    if (from.equalsIgnoreCase(mCurrentUserId)) {
                        String message = object.getString("message");
                        String status = object.getString("status");

                        byte[] decodeStatus = Base64.decode(status, Base64.DEFAULT);
                        status = new String(decodeStatus, StandardCharsets.UTF_8);
                        Status.setText(status);
                    }
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CHANGE_USER_NAME)) {
            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String err = object.getString("err");
                // byte[] uname=name.getBytes();

                if (err.equals("0")) {

                    String from = object.getString("from");
                    if (from.equalsIgnoreCase(mCurrentUserId)) {

                        String name = object.getString("name");
                        String surname = object.getString("surname");
                        String email = object.getString("email");
                      /*  byte[] data = Base64.decode(name, Base64.DEFAULT);
                        try {
                            name = new String(data, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            Log.e(TAG,"",e);
                        }*/
                        Username.setText(name);
                        tvSurname.setText(surname);
                        tvEmail.setText(email);
                    }
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(UserProfile.this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(UserProfile.this);
    }

    private void beginCrop(Uri source) {
        MyLog.d(TAG, "beginCrop: ");
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
       /* if (resultCode == RESULT_OK) {
            profile.setImageURI(Crop.getOutput(result));
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        Status.setText(SessionManager.getInstance(UserProfile.this).getcurrentUserstatus());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        try {
            bytes.close();
        } catch (IOException e) {
            MyLog.e(TAG, "", e);
        }
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}