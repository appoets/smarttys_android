package com.app.Smarttys.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.WebviewModule.PolicyWebview;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.service.Constants;


public class ContactPermissionActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout allow_layout;
    private Button privacy_button;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_permission);

        init();
    }

    private void init() {

        session = new Session(ContactPermissionActivity.this);

        allow_layout = findViewById(R.id.allow_layout);
        privacy_button = findViewById(R.id.privacy_button);

        OnclickListener();
    }

    private void OnclickListener() {

        allow_layout.setOnClickListener(this);
        privacy_button.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.allow_layout:
                Allow();
                break;

            case R.id.privacy_button:
                PrivacyPolicy();
                break;
        }
    }

    private void Allow() {

        session.SetContactpermission(true);
        Intent intent = new Intent(this, InitialLoaderActivitySmartty.class);
        startActivity(intent);
        finish();
    }

    private void PrivacyPolicy() {

        try {

            Intent intent = new Intent(this, PolicyWebview.class);
            intent.putExtra("web_url", Constants.pricvacy_policy);
            startActivity(intent);

         /*   Uri uriterms = Uri.parse(Constants.pricvacy_policy);
            Intent intentterms = new Intent(Intent.ACTION_VIEW, uriterms);
            startActivity(intentterms);*/

        } catch (Exception e) {

        }
    }
}
