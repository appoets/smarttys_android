package com.app.Smarttys.app.activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.RItemAdapter;
import com.app.Smarttys.app.adapter.SmarttyCASocket;
import com.app.Smarttys.app.dialog.ChatLockPwdDialog;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.ActivityLauncher;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.ChatLockPojo;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SettingContact extends CoreActivity {

    private static final String TAG = SettingContact.class.getSimpleName() + ">>";
    boolean refreshcontactsset = false;
    RecyclerView lvContacts;
    SmarttyCASocket adapter;
    EditText etSearch;
    AvnNextLTProDemiTextView selectcontact;
    AvnNextLTProRegTextView selectcontactmember;
    ImageView serach, overflow, backarrow, backButton;
    InputMethodManager inputMethodManager;
    Button newGroup;
    Getcontactname getcontactname;
    RelativeLayout overflowlayout, contact1_RelativeLayout;
    AvnNextLTProRegTextView contact_empty;
    String username, profileimage;
    String receiverDocumentID, uniqueCurrentID;
    List<SmarttyContactModel> smarttyEntries = new ArrayList<>();
    private EditText mSearchEt;
    private UserInfoSession userInfoSession;
    private SessionManager sessionManager;
    private SearchView searchView;
    private Menu contactMenu;
    private ProgressBar pbLoader;

    private LinearLayoutManager mContactListManager;

    private SwipeRefreshLayout swipeRefreshLayout;
    private long lastUpdatedMillis = 0;
    private boolean isRefreshRequested = false;
    ContactsRefreshReceiver contactsRefreshReceiver = new ContactsRefreshReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            pbLoader.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            loadContactsFromDB();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyLog.d(TAG, "onCreate: ");
        setContentView(R.layout.contactsetting);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.nowletschat.android.contact_refresh");
        registerReceiver(contactsRefreshReceiver, intentFilter);

        userInfoSession = new UserInfoSession(getApplicationContext());

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        lvContacts = findViewById(R.id.listContacts);
        backButton = findViewById(R.id.backarrow_contactsetting);
        backarrow = findViewById(R.id.backarrow);
        contact_empty = findViewById(R.id.contact_empty);
        overflow = findViewById(R.id.overflow);
        overflowlayout = findViewById(R.id.overflowLayout);
        contact1_RelativeLayout = findViewById(R.id.r1contact);
        newGroup = findViewById(R.id.newGroup);
        //new_group_layout = (RelativeLayout) findViewById(R.id.new_group_layout);
        serach = findViewById(R.id.search);
        etSearch = findViewById(R.id.etSearch);
        selectcontact = findViewById(R.id.selectcontact);
        selectcontactmember = findViewById(R.id.selectcontactmember);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getcontactname = new Getcontactname(SettingContact.this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            swipeRefreshLayout.setRefreshing(false);
            refreshContacts();
        });

        etSearch.setOnTouchListener((v, event) -> {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;
            if (event.getAction() == MotionEvent.ACTION_UP && etSearch.getCompoundDrawables()[DRAWABLE_RIGHT] != null) {
                if (event.getRawX() >= (etSearch.getRight() - etSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    etSearch.setText("");
                    return false;
                }
            }
            return false;
        });

        mContactListManager = new LinearLayoutManager(SettingContact.this, LinearLayoutManager.VERTICAL, false);
        lvContacts.setLayoutManager(mContactListManager);
        lvContacts.setNestedScrollingEnabled(false);
//        lvContacts.addOnScrollListener(contactScrollListener);

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        sessionManager = SessionManager.getInstance(this);

        pbLoader = findViewById(R.id.pbLoader);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable drawableProgress = DrawableCompat.wrap(pbLoader.getIndeterminateDrawable());
            DrawableCompat.setTint(drawableProgress, ContextCompat.getColor(this, android.R.color.holo_green_light));
            pbLoader.setIndeterminateDrawable(DrawableCompat.unwrap(drawableProgress));
        } else {
            pbLoader.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
        }

        loadContactsFromDB();

        if (!SessionManager.getInstance(this).isContactSyncFinished()) {
            refreshContacts();
        }

        lvContacts.addOnItemTouchListener(new RItemAdapter(this, lvContacts, new RItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                SmarttyContactModel e = adapter.getItem(position);
                if (e.getStatus() != null && e.getStatus().length() > 0) {
                    ChatLockPojo lockPojo = getChatLockdetailfromDB(position);
                    if (sessionManager != null && sessionManager.getLockChatEnabled().equals("1") && lockPojo != null) {
                        String stat = "", pwd = null;
                        stat = lockPojo.getStatus();
                        pwd = lockPojo.getPassword();
                        String docID = e.get_id();
                        String documentid = uniqueCurrentID.concat("-").concat(docID);
                        if (stat.equals("1")) {
                            openUnlockChatDialog(documentid, stat, pwd, position);
                        } else {
                            //navigateToChatviewPage(e);
                            getcontactname.navigateToChatviewPageforSmarttyModel(e);
                            finish();
                        }
                    } else {
                        getcontactname.navigateToChatviewPageforSmarttyModel(e);
                        finish();
                    }
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        newGroup.setOnClickListener(v -> {
            Intent i = new Intent(getApplication(), SelectPeopleForGroupChat.class);
            startActivity(i);
        });

        serach.setOnClickListener(v -> {
            showSearchActions();
            etSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                    // When user changed the Text
                    if (adapter != null) {
                        if (cs.length() > 0) {
                            if (cs.length() == 1) {
                                etSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.cancel_normal, 0);
                            }
                            SettingContact.this.adapter.getFilter().filter(cs);
                        } else {
                            etSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            adapter.updateInfo(smarttyEntries);
                        }
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                              int arg3) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                }
            });
            backarrow.setVisibility(View.VISIBLE);
            backButton.setVisibility(View.GONE);

            backarrow.setOnClickListener(v1 -> {
                etSearch.getText().clear();
                    /*if (adapter != null) {
                        adapter.updateInfo(smarttyEntries);
                    }*/
                etSearch.setVisibility(View.GONE);
                serach.setVisibility(View.VISIBLE);
                //overflowlayout.setVisibility(View.VISIBLE);
                selectcontactmember.setVisibility(View.VISIBLE);
                selectcontact.setVisibility(View.VISIBLE);
                backarrow.setVisibility(View.GONE);
                backButton.setVisibility(View.VISIBLE);
                hideKeyboard();
            });
            showKeyboard();
        });

        backButton.setOnClickListener(v -> finish());

        /* Variables for serch */
    }

    private <E> boolean isDuplicateList(final List<E> l1, final List<E> l2) {
        final Set<E> set = new HashSet<>(l1);
        final Set<E> set2 = new HashSet<>(l2);
        return l1.size() == l2.size() && set.containsAll(set2);
    }

    private void loadContactsFromDB() {
        long currentMillis = System.currentTimeMillis();
        long diffMillis = currentMillis - lastUpdatedMillis;

        if (isRefreshRequested || (diffMillis > 20000)) {
            isRefreshRequested = false;
            lastUpdatedMillis = System.currentTimeMillis();
            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
            List<SmarttyContactModel> updatedContactList = contactDB_sqlite.getSavedSmarttyContacts();
            Collections.sort(updatedContactList, Getcontactname.nameAscComparator);
            Log.d(TAG, "loadContactsFromDB: start");
            if (updatedContactList != null && isDuplicateList(updatedContactList, smarttyEntries)) {
                Log.d(TAG, "loadContactsFromDB: no need refresh");
                return;
            } else {
                Log.d(TAG, "loadContactsFromDB: need refresh");
                smarttyEntries = updatedContactList;
            }

            if (smarttyEntries != null && smarttyEntries.size() > 0) {
                MyLog.d(TAG, "ContactsTest loadContactsFromDB: size " + smarttyEntries.size());
                Collections.sort(smarttyEntries, Getcontactname.nameAscComparator);
                adapter = new SmarttyCASocket(SettingContact.this, smarttyEntries);

                lvContacts.setVisibility(View.VISIBLE);
                contact_empty.setVisibility(View.GONE);
                lvContacts.getRecycledViewPool().setMaxRecycledViews(0, 100);
                adapter.setHasStableIds(true);
                lvContacts.setAdapter(adapter);
                selectcontactmember.setText(smarttyEntries.size() + " " + getString(R.string.Contacts));
//            notifyScrollChanged();
                if (refreshcontactsset) {
                    Toast.makeText(SettingContact.this, getString(R.string.your_contact_list_has_been_updated), Toast.LENGTH_SHORT).show();
                    refreshcontactsset = false;
                }
            } else {
                MyLog.d(TAG, "loadContactsFromDB: no contacts in DB");
                contact_empty.setVisibility(View.VISIBLE);
                lvContacts.setVisibility(View.GONE);

                // TODO: Translate the below str
                contact_empty.setText("No contacts available for chat");
                selectcontactmember.setText("0 " + getString(R.string.Contacts));
            }
        }
    }

    private void updateProfileImage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            String from = objects.getString("from");
            String type = objects.getString("type");
            //String docId = uniqueCurrentID + "-" + from;
            if (smarttyEntries == null)
                return;
            for (SmarttyContactModel smarttyContactModel : smarttyEntries) {
                if (type.equalsIgnoreCase("single") && smarttyContactModel.get_id().equalsIgnoreCase(from)) {
                    adapter.notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "updateProfileImage: ", e);
        }
    }

    private void showSearchActions() {
        backarrow.setVisibility(View.VISIBLE);
        serach.setVisibility(View.GONE);
        backButton.setVisibility(View.GONE);
        selectcontact.setVisibility(View.GONE);
        selectcontactmember.setVisibility(View.GONE);
        //contact1_RelativeLayout.setVisibility(View.GONE);
        etSearch.setVisibility(View.VISIBLE);
        etSearch.requestFocus();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        contactMenu = menu;
        getMenuInflater().inflate(R.menu.new_chat, menu);
        return super.onCreateOptionsMenu(contactMenu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        hideKeyboard();
        switch (item.getItemId()) {
            case R.id.menuinvitefriends:
                AppUtils.shareApp(SettingContact.this);
                break;
            case R.id.menucontacts:
                /*Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivity(intent);*/
                String data = "content://contacts/people/";
                Intent contactIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data));
                startActivity(contactIntent);
                break;
            case R.id.menurefresh:
                if (isNetworkConnected()) {
                    refreshContacts();
                    refreshcontactsset = true;
                } else {
                    Toast.makeText(SettingContact.this, "Please check your inernet connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.menuaboutHelp:
                ActivityLauncher.launchAbouthelp(SettingContact.this);
                break;
            case R.id.chats_contactIcon:
                try {
                    Intent newIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                    // Sets the MIME type to match the Contacts Provider
                    newIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                    startActivity(newIntent);
                } catch (Exception e) {
                    MyLog.e(TAG, "onOptionsItemSelected: ", e);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    private void refreshContacts() {
        isRefreshRequested = true;
        MyLog.d(TAG, "ContactsTest refreshContacts CLICK: ");
        pbLoader.setVisibility(View.VISIBLE);
        SmarttyContactsService.bindContactService(this, true);
    }

    private void openUnlockChatDialog(String docId, String status, String pwd, int position) {
        String convId = userInfoSession.getChatConvId(docId);
        ChatLockPwdDialog dialog = new ChatLockPwdDialog();
        dialog.setTextLabel1("Enter your Password");
        dialog.setEditTextdata("Enter new Password");
        dialog.setforgotpwdlabel("Forgot Password");
        dialog.setHeader("Unlock Chat");
        dialog.setButtonText("Unlock");
        Bundle bundle = new Bundle();
        bundle.putSerializable("socketitems", adapter.getItem(position));
        bundle.putString("convID", convId);
        bundle.putString("status", "1");
        bundle.putString("pwd", pwd);
        bundle.putString("page", "chatlist");
        bundle.putString("type", "single");
        bundle.putString("from", uniqueCurrentID);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "chatunLock");
    }

    private ChatLockPojo getChatLockdetailfromDB(int position) {
        SmarttyContactModel e = adapter.getItem(position);
        String docID = e.get_id();
        String id = uniqueCurrentID.concat("-").concat(docID);
        MessageDbController dbController = CoreController.getDBInstance(this);
        String convId = userInfoSession.getChatConvId(id);
        String receiverId = userInfoSession.getReceiverIdByConvId(convId);
        return dbController.getChatLockData(receiverId, MessageFactory.CHAT_TYPE_SINGLE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (SocketManager.EVENT_GET_CONTACTS.equalsIgnoreCase(event.getEventName())) {

        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GET_USER_DETAILS)) {
//            loadUserDetails(event.getObjectsArray()[0].toString());
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_IMAGE_UPLOAD)) {
            updateProfileImage(event);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        hideKeyboard();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideKeyboard();
        unregisterReceiver(contactsRefreshReceiver);
    }

    @Override
    public void onBackPressed() {
        if (etSearch.getVisibility() == View.GONE) {
            hideKeyboard();
            super.onBackPressed();
        } else {
            backarrow.performClick();
        }
    }
}