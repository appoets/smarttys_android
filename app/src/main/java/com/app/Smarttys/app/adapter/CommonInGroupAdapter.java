package com.app.Smarttys.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.app.widget.CircleImageView;
import com.app.Smarttys.core.model.CommonInGroupPojo;

import java.util.ArrayList;

/**
 * Created by CAS60 on 12/27/2016.
 */
public class CommonInGroupAdapter extends RecyclerView.Adapter<CommonInGroupAdapter.CommonInGroupHolder> {

    private Context mContext;
    private ArrayList<CommonInGroupPojo> dataList;

    public CommonInGroupAdapter(Context mContext, ArrayList<CommonInGroupPojo> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }

    @Override
    public CommonInGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_common_in_group,
                parent, false);
        CommonInGroupHolder holder = new CommonInGroupHolder(view);
        return holder;
    }

    public CommonInGroupPojo getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public void onBindViewHolder(CommonInGroupHolder holder, int position) {

        String avatarPath = dataList.get(position).getAvatarPath();
        if (avatarPath != null && !avatarPath.equals("")) {
        /*    Picasso.with(mContext).load(AppUtils.getValidProfilePath(avatarPath)).error(
                    R.mipmap.chat_attachment_profile_default_image_frame).transform(new CircleTransform()).into(holder.ivGroupDp);
        */
            AppUtils.loadImage(mContext, AppUtils.getValidGroupPath(avatarPath), holder.ivGroupDp, 100, R.mipmap.group_chat_attachment_profile_icon);
        } else {
            holder.ivGroupDp.setImageResource(R.mipmap.chat_attachment_profile_default_image_frame);
        }
        holder.tvGroupName.setText(dataList.get(position).getGroupName());
        if (dataList.get(position).getGroupContactNames() != null) {
            holder.tvGroupContacts.setText(dataList.get(position).getGroupContactNames());
        } else {
            holder.tvGroupContacts.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class CommonInGroupHolder extends RecyclerView.ViewHolder {

        public CircleImageView ivGroupDp;
        public AvnNextLTProRegTextView tvGroupName, tvGroupContacts;

        public CommonInGroupHolder(View itemView) {
            super(itemView);

            ivGroupDp = itemView.findViewById(R.id.ivGroupDp);
            tvGroupName = itemView.findViewById(R.id.tvGroupName);
            tvGroupContacts = itemView.findViewById(R.id.tvGroupContacts);
        }
    }

}
