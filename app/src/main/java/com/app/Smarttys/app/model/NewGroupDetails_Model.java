package com.app.Smarttys.app.model;

import java.io.Serializable;

//Only for RAAD
public class NewGroupDetails_Model implements Serializable {

    private String jsonObj;
    private String recordId;
    private String id;
    private int groupType;
    private int err;
    private String groupId;
    private String convId;
    private String createdBy;
    private String admin;
    private String profilePic;
    private String groupName;
    private String timestamp;
    private String groupMembers;
    private String from_;
    private String msisdn;
    private String type;

    public NewGroupDetails_Model() {
    }

    public NewGroupDetails_Model(String jsonObj, String recordId, String id, int groupType, int err, String groupId, String convId, String createdBy, String admin, String profilePic, String groupName, String timestamp, String groupMembers, String from_, String msisdn, String type) {
        this.jsonObj = jsonObj;
        this.recordId = recordId;
        this.id = id;
        this.groupType = groupType;
        this.err = err;
        this.groupId = groupId;
        this.convId = convId;
        this.createdBy = createdBy;
        this.admin = admin;
        this.profilePic = profilePic;
        this.groupName = groupName;
        this.timestamp = timestamp;
        this.groupMembers = groupMembers;
        this.from_ = from_;
        this.msisdn = msisdn;
        this.type = type;
    }


    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getGroupType() {
        return groupType;
    }

    public void setGroupType(int groupType) {
        this.groupType = groupType;
    }

    public int getErr() {
        return err;
    }

    public void setErr(int err) {
        this.err = err;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getConvId() {
        return convId;
    }

    public void setConvId(String convId) {
        this.convId = convId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(String groupMembers) {
        this.groupMembers = groupMembers;
    }

    public String getFrom_() {
        return from_;
    }

    public void setFrom_(String from_) {
        this.from_ = from_;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getJsonObj() {
        return jsonObj;
    }

    public void setJsonObj(String jsonObj) {
        this.jsonObj = jsonObj;
    }
}
