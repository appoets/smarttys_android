package com.app.Smarttys.app.utils;

import android.app.Activity;
import android.util.Log;

import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.GroupInfoPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.SmarttyContactModel;

import java.util.ArrayList;
import java.util.List;

public class ChatListUtil {
    private static final String TAG = "ChatListUtil";

    public static ArrayList<SmarttyContactModel> getGroupList(Activity activity) {
        ArrayList<SmarttyContactModel> grouplist = new ArrayList<>();
        MessageDbController db = CoreController.getDBInstance(activity);
        SessionManager sessionManager = SessionManager.getInstance(activity);
        Session session = new Session(activity);
        String mCurrentUserId = sessionManager.getCurrentUserID();
        GroupInfoSession groupInfoSession = new GroupInfoSession(activity);
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(activity);
        Getcontactname getcontactname = new Getcontactname(activity);
        ArrayList<MessageItemChat> groupChats = db.selectChatList(MessageFactory.CHAT_TYPE_GROUP);
        List<String> lists = session.getBlockedIds();
        for (MessageItemChat msgItem : groupChats) {
            try {
//            getGroupDetails(msgItem.getConvId());
//            Log.d("getDataFromDB", msgItem.getGroupName());
                String groupId = msgItem.getReceiverID();
                String docIdd = mCurrentUserId + "-" + groupId + "-g";
                GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docIdd);


                if (lists != null && lists.size() != 0) {
                    if (!lists.contains(msgItem.getReceiverID())) {
                        if (infoPojo.getGroupName() != null) {
                            StringBuilder sb = null;
                            SmarttyContactModel vSmarttyContactModel = new SmarttyContactModel();
                            String groupid = msgItem.getReceiverID().split("-")[0];
                            vSmarttyContactModel.set_id(groupid);
                            vSmarttyContactModel.setType(msgItem.getGroupEventType());
                            vSmarttyContactModel.setAvatarImageUrl(infoPojo.getAvatarPath());
                            vSmarttyContactModel.setFirstName(infoPojo.getGroupName());
                            String from = msgItem.getMessageId().split("-")[0];
                            String to = msgItem.getReceiverID();
                            String docId = from + "-" + to + "-g";
                            boolean hasGroupInfo = groupInfoSession.hasGroupInfo(docId);
                            if (hasGroupInfo) {
                                infoPojo = groupInfoSession.getGroupInfo(docId);
                                sb = new StringBuilder();
                                String memername = "";
                                if (infoPojo != null && infoPojo.getGroupMembers() != null) {
                                    String[] contacts = infoPojo.getGroupMembers().split(",");

                                    for (int i = 0; i < contacts.length; i++) {
                                        if (!contacts[i].equalsIgnoreCase(from)) {
                                            String msisdn = contactDB_sqlite.getSingleData(contacts[i], ContactDB_Sqlite.MSISDN);
                                            if (msisdn != null) {
                                                memername = getcontactname.getSendername(contacts[i], msisdn);
                                                sb.append(memername);
                                                if (contacts.length - 1 != i) {
                                                    sb.append(", ");
                                                }
                                            }
                                        } else {
                                            memername = "You";
                                            sb.append(memername);
                                            if (contacts.length - 1 != i) {
                                                sb.append(", ");
                                            }
                                        }
                                    }
                                }
                            }

                            vSmarttyContactModel.setStatus(String.valueOf(sb));
                            if (infoPojo != null && infoPojo.isLiveGroup())
                                grouplist.add(vSmarttyContactModel);
                        }
                    }
                } else {
                    if (infoPojo.getGroupName() != null) {
                        StringBuilder sb = null;
                        SmarttyContactModel vSmarttyContactModel = new SmarttyContactModel();
                        String groupid = msgItem.getReceiverID().split("-")[0];
                        vSmarttyContactModel.set_id(groupid);
                        vSmarttyContactModel.setType("");
                        vSmarttyContactModel.setAvatarImageUrl(infoPojo.getAvatarPath());
                        vSmarttyContactModel.setFirstName(infoPojo.getGroupName());
                        vSmarttyContactModel.setGroup(true);
                        String from = msgItem.getMessageId().split("-")[0];
                        String to = msgItem.getReceiverID();
                        String docId = from + "-" + to + "-g";

                        boolean hasGroupInfo = groupInfoSession.hasGroupInfo(docId);

                        if (hasGroupInfo) {

                            infoPojo = groupInfoSession.getGroupInfo(docId);
                            sb = new StringBuilder();
                            String memername = "";
                            if (infoPojo != null && infoPojo.getGroupMembers() != null) {
                                String[] contacts = infoPojo.getGroupMembers().split(",");

                                for (int i = 0; i < contacts.length; i++) {
                                    if (!contacts[i].equalsIgnoreCase(from)) {
                                        String msisdn = contactDB_sqlite.getSingleData(contacts[i], ContactDB_Sqlite.MSISDN);
                                        if (msisdn != null) {
                                            memername = getcontactname.getSendername(contacts[i], msisdn);
                                            sb.append(memername);
                                            if (contacts.length - 1 != i) {
                                                sb.append(", ");
                                            }
                                        }
                                    } else {
                                        memername = "You";
                                        sb.append(memername);
                                        if (contacts.length - 1 != i) {
                                            sb.append(", ");
                                        }
                                    }
                                }
                            }
                        }

                        vSmarttyContactModel.setStatus(String.valueOf(sb));
                        if (infoPojo != null && infoPojo.isLiveGroup())
                            grouplist.add(vSmarttyContactModel);
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "getGroupList: ", e);
            }
        }
        return grouplist;
    }
}
