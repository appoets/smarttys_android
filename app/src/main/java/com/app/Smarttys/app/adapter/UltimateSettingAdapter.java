package com.app.Smarttys.app.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;

import com.app.Smarttys.R;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.core.model.SmarttySettingsModel;

import java.util.ArrayList;

/*
 *  * Created by CAS60 on 12/26/2016.
 * */
public class UltimateSettingAdapter extends BaseAdapter {

    private ArrayList<SmarttySettingsModel> dataList;
    private AvnNextLTProDemiTextView textView_settings;
    private Context context;
    //private ArrayList<SingleRowSetting> list;
    private ImageView imageView_settings;

    public UltimateSettingAdapter(Context c, ArrayList<SmarttySettingsModel> dataList) {
        context = c;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.settings_list_view, parent, false);
        textView_settings = row.findViewById(R.id.textView_settings);
        imageView_settings = row.findViewById(R.id.imageView_settings);
        textView_settings.setText(dataList.get(position).getTitle());
        imageView_settings.setImageResource(dataList.get(position).getResourceId());
        imageView_settings.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary));
        return row;
    }


}