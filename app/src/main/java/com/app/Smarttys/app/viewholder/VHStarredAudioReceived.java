package com.app.Smarttys.app.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.widget.CircleImageView;
import com.github.lzyzsd.circleprogress.DonutProgress;

/**
 * Created by CAS63 on 2/6/2017.
 */
public class VHStarredAudioReceived extends RecyclerView.ViewHolder {

    /**
     *
     */
    public TextView senderName, time, duration, time_ts, fromname, toname, datelbl, recodingduration;
    public SeekBar sbDuration;
    public ImageView playButton, download, headset, starredindicator, userprofile, record_icon;
    public RelativeLayout rlPlayCtrl, audiotrack_layout;
    public DonutProgress pbDownload;
    public CircleImageView record_image;

    public VHStarredAudioReceived(View view) {
        super(view);
        rlPlayCtrl = view.findViewById(R.id.rlPlayCtrl);
        toname = view.findViewById(R.id.toname);
        fromname = view.findViewById(R.id.fromname);
        datelbl = view.findViewById(R.id.datelbl);
        playButton = view.findViewById(R.id.imageView26);
        headset = view.findViewById(R.id.headset);
        download = view.findViewById(R.id.download);
        senderName = view.findViewById(R.id.lblMsgFrom);
        recodingduration = view.findViewById(R.id.recodingduration);
        duration = view.findViewById(R.id.duration);
        audiotrack_layout = view.findViewById(R.id.audiotrack_layout);
        userprofile = view.findViewById(R.id.userprofile);
        record_image = view.findViewById(R.id.record_image);
        //tsNextLine = (TextView) view.findViewById(R.id.tsNextLine);
        starredindicator = view.findViewById(R.id.starredindicator);
        time_ts = view.findViewById(R.id.time_ts);
        record_icon = view.findViewById(R.id.record_icon);
        sbDuration = view.findViewById(R.id.sbDuration);
        pbDownload = view.findViewById(R.id.pbDownload);
        pbDownload.setMax(100);
    }
}
