package com.app.Smarttys.app.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.app.Smarttys.R;
import com.app.Smarttys.app.dialog.CustomAlertDialog;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.widget.AvnNextLTProDemiButton;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.service.Constants;
import com.app.Smarttys.core.socket.SocketManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import io.socket.client.Socket;

/**
 * Created by CAS60 on 5/8/2017.
 */
public class ChatBackUpRestoreActivity extends CoreActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int REQUEST_CODE_RESOLUTION = 1;
    private static final String TAG = ChatBackUpRestoreActivity.class.getSimpleName();
    private final int BACKUP_ERROR_GOOGLE_DRIVE = 1;
    private final int BACKUP_ERROR_INVALID_DRIVE_ID = 2;
    private final int BACKUP_ERROR_OTHERS = 3;
    private final int BACKUP_SUCCESS = 4;
    private final int GOOGLE_DRIVE_ID_INVALID_STATUS_CODE = 1502;
    private ScrollView svParent;
    private AvnNextLTProRegTextView tvGmailId, tvBackupTime, tvSize;
    private AvnNextLTProDemiButton btnRestore, btnSkip;
    private ProgressDialog progressDialog;
    private GoogleApiClient mGoogleApiClient;
    private SocketManager mSocketManager;
    private String mCurrentUserId, mChatDbName, mDriveFileName, mBackupGmailId;
    private DriveId mDriveId;
    private boolean isDriveSettingsCalled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_backup_restore);
        MyLog.d(TAG, "onCreate: ");
        initView();
        initData();
    }

    private void initView() {
        svParent = findViewById(R.id.svParent);

        tvGmailId = findViewById(R.id.tvGmailId);
        tvBackupTime = findViewById(R.id.tvBackupTime);
        tvSize = findViewById(R.id.tvSize);

        btnRestore = findViewById(R.id.btnRestore);
        btnSkip = findViewById(R.id.btnSkip);

        btnRestore.setOnClickListener(this);
        btnSkip.setOnClickListener(this);

        progressDialog = getProgressDialogInstance();
        progressDialog.setMessage(getString(R.string.loading_in));


    }

    private void initData() {
        mCurrentUserId = SessionManager.getInstance(ChatBackUpRestoreActivity.this).getCurrentUserID();
        mChatDbName = MessageDbController.DB_NAME;

        initSocketCallback();

        if (!mSocketManager.isConnected()) {

            mSocketManager.connect();
        }

        progressDialog.show();
    }

    private void initSocketCallback() {
        if (mSocketManager != null)
            return;
        mSocketManager = SocketManager.getInstance();
        mSocketManager.init(ChatBackUpRestoreActivity.this, new SocketManager.SocketCallBack() {
            @Override
            public void onSuccessListener(String eventName, Object... response) {
                if (eventName.equalsIgnoreCase(SocketManager.EVENT_GET_GOOGLE_DRIVE_SETTINGS)) {
                    String data = response[0].toString();

                    try {
                        JSONObject object = new JSONObject(data);
                        String from = object.getString("from");

                        if (from.equalsIgnoreCase(mCurrentUserId)) {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            isDriveSettingsCalled = true;

                            JSONObject driveObj = object.getJSONObject("drive_settings");

                            if (driveObj.length() > 0 && driveObj.getLong("FileSize") > 0) {
                                long backUpSize = driveObj.getLong("FileSize");
                                long backUpTs = driveObj.getLong("CreatedTs");
                                mBackupGmailId = driveObj.getString("BackUpGmailId");
                                String backUpOver = driveObj.getString("BackUpOver");
                                String duration = driveObj.getString("BackUpDuration");

                                mDriveFileName = driveObj.getString("FileName");
                                mDriveId = DriveId.decodeFromString(driveObj.getString("DriveId"));

                                connectGoogleApiClient();

                                long fileSizeInKB = backUpSize / 1024;
                                long fileSizeInMB = fileSizeInKB / (1024 * 1024);


                                String size = fileSizeInKB + " KB";
                                if (fileSizeInMB > 0) {
                                    size = fileSizeInMB + " MB";
                                }

                                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a", Locale.ENGLISH);
                                Date dateTs = new Date(backUpTs);
                                final String backUpTime = format.format(dateTs);

                                final String finalSize = size;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        svParent.setVisibility(View.VISIBLE);
                                        tvSize.setText("Size : " + finalSize);
                                        tvGmailId.setText("Account : " + mBackupGmailId);
                                        tvBackupTime.setText("Last Back-up : " + backUpTime);
                                    }
                                });

                                SessionManager sessionManager = SessionManager.getInstance(ChatBackUpRestoreActivity.this);
                                sessionManager.setBackUpMailAccount(mBackupGmailId);
                                sessionManager.setBackUpSize(backUpSize);
                                sessionManager.setBackUpTS(backUpTs);
                                sessionManager.setBackUpOver(backUpOver);
                                sessionManager.setBackUpDuration(duration);
                                sessionManager.setBackUpDriveFileName(mDriveFileName);
                                sessionManager.setBackUpDriveFileId(driveObj.getString("DriveId"));
                            } else {
                                goHomeScreen();
                            }
                        }
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                } else if (eventName.equalsIgnoreCase(Socket.EVENT_CONNECT)) {

                    createUser();

                } else if (eventName.equalsIgnoreCase(SocketManager.EVENT_USER_CREATED)) {
                    if (!isDriveSettingsCalled) {
                        //MessageService.testEncrypt(ChatBackUpRestoreActivity.this);
                        checkBackupDetailsExists();
                    }
                } else if (eventName.equalsIgnoreCase(SocketManager.EVENT_SKIP_BACKUP_MESSAGES)) {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    String data = response[0].toString();
                    try {
                        JSONObject object = new JSONObject(data);
                        if (object.has("status") && object.getString("status").equalsIgnoreCase("1")) {
                            goHomeScreen();
                        } else {
                            goHomeScreen();
                        }
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                }
            }
        });
    }

    private void createUser() {
        try {
            JSONObject object = new JSONObject();
            object.put("_id", mCurrentUserId);
            object.put("mode", "phone");
            object.put("chat_type", "single");
            object.put("device", Constants.DEVICE);
            String securityToken = SessionManager.getInstance(getApplicationContext()).getSecurityToken();
            MyLog.d("createUser", "securityToken: " + securityToken);
            object.put("token", securityToken);


            mSocketManager.send(object, SocketManager.EVENT_CREATE_USER);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient == null && mBackupGmailId != null && !mBackupGmailId.equals("")) {
            // Create the API client and bind it to an instance variable.
            // We use this instance as the callback for connection and connection
            // failures.
            // Since no account name is passed, the user is prompted to choose.
            connectGoogleApiClient();
        } else if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            connectGoogleApiClient();
        }
    }

    @Override
    protected void onPause() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onPause();
    }

    private void connectGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_FILE)
                .addScope(Drive.SCOPE_APPFOLDER)
                .setAccountName(mBackupGmailId)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mGoogleApiClient.connect();
    }

    private void checkBackupDetailsExists() {
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            MyLog.d(TAG, "getDriveSettings: ");
            mSocketManager.send(object, SocketManager.EVENT_GET_GOOGLE_DRIVE_SETTINGS);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnRestore:
                if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                    performDownloadFromDrive();

                } else {
                    connectGoogleApiClient();
                }

                break;

            case R.id.btnSkip:
//                goHomeScreen();
                performSkipBackupMessages();
                break;
        }
    }

    // Remove old messages from server if user skips message back up restore
    private void performSkipBackupMessages() {
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            mSocketManager.send(object, SocketManager.EVENT_SKIP_BACKUP_MESSAGES);

            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog.show();
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void goHomeScreen() {
        CoreController.setDBInstance(ChatBackUpRestoreActivity.this);
        SessionManager sessionManager = SessionManager.getInstance(ChatBackUpRestoreActivity.this);
        sessionManager.IsBackupRestored(true);
        //  mSocketManager.disconnect();
        Intent intent = new Intent(ChatBackUpRestoreActivity.this, NewHomeScreenActivty.class);
        startActivity(intent);
        finish();

//        Handler handler = new Handler(Looper.getMainLooper());
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                // Run your task here
//
//            }
//        }, 5000);

    }

    private void performDownloadFromDrive() {
        downloadFile(mDriveId, mDriveFileName);
    }

    private void downloadFile(final DriveId driveId, final String gdFilename) {
        final String filePath = MessageFactory.DATABASE_PATH + gdFilename.split("/")[1];

        final String dbDirPath = "/data/data/" + getPackageName() + "/databases/";

        AsyncTask<Void, Void, Integer> task = new AsyncTask<Void, Void, Integer>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                progressDialog.setCancelable(false);
                progressDialog.show();

                File dbDir = new File(filePath);
                if (!dbDir.exists()) {
                    try {
                        dbDir.createNewFile();
                    } catch (IOException e) {
                        MyLog.e(TAG, "", e);
                    }
                }

//                File storageDbDir = new File(MessageFactory.DATABASE_PATH);
                File storageDbDir = new File(dbDirPath);
                if (!storageDbDir.exists()) {
                    storageDbDir.mkdirs();
                }
            }

            @Override
            protected void onPostExecute(Integer result) {
                super.onPostExecute(result);
                try {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }

                if (result == BACKUP_SUCCESS) {
                    Toast.makeText(ChatBackUpRestoreActivity.this, "Messages restored successfully", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            goHomeScreen();
                        }
                    }, 1000);
                } else if (result == BACKUP_ERROR_OTHERS) {
                    Toast.makeText(ChatBackUpRestoreActivity.this, "Unable to fetch data from google drive", Toast.LENGTH_SHORT).show();
                    performSkipBackupMessages();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            goHomeScreen();
                        }
                    }, 2000);
                } else if (result == BACKUP_ERROR_INVALID_DRIVE_ID) {
                    Toast.makeText(ChatBackUpRestoreActivity.this, "Backup file not found in google drive", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChatBackUpRestoreActivity.this, "Try again", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            protected Integer doInBackground(Void... params) {
            /*    DriveFile file = Drive.DriveApi.getFile(mGoogleApiClient, driveId);

                DriveApi.DriveContentsResult driveContentsResult = file.open(
                        mGoogleApiClient,
                        DriveFile.MODE_READ_ONLY, null).await();

               *//* Query query = new Query.Builder()
                        .addFilter(Filters.eq(SearchableField.TITLE, gdFilename))
                        .build();
                PendingResult<DriveApi.MetadataBufferResult> result = Drive.DriveApi.query(mGoogleApiClient, query);
                result.setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
                    @Override
                    public void onResult(@NonNull DriveApi.MetadataBufferResult metadataBufferResult) {
                        Log.d("data", metadataBufferResult.toString());
                    }
                });*//*


                DriveContents driveContents = driveContentsResult.getDriveContents();
                if (driveContentsResult.getStatus().getStatusCode() == GOOGLE_DRIVE_ID_INVALID_STATUS_CODE) {
                    return BACKUP_ERROR_INVALID_DRIVE_ID;
                }
                if (driveContents == null) {
                    return BACKUP_ERROR_GOOGLE_DRIVE;
                } else {
                    InputStream inputstream = driveContents.getInputStream();

                    try {
                        File dbZipFile = new File(filePath);
                        if (dbZipFile.exists()) {
                            dbZipFile.delete();
                        }
                        dbZipFile.createNewFile();

                        FileOutputStream fileOutput = new FileOutputStream(filePath);

                        byte[] buffer = new byte[1024];
                        int bufferLength = 0;
                        while ((bufferLength = inputstream.read(buffer)) > 0) {
                            fileOutput.write(buffer, 0, bufferLength);
                        }
                        fileOutput.close();
                        inputstream.close();

                        unzip(filePath, dbDirPath);
                        return BACKUP_SUCCESS;
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        Log.d("BackUpRestoreError", "IOException occurred");
                        Log.e(TAG,"",e);
                        return BACKUP_ERROR_OTHERS;
                    }

                }*/

                return BACKUP_ERROR_GOOGLE_DRIVE;
            }

        };
        task.execute();
    }

    public boolean unzip(String zipFile, String dbFileDirPath) {

        MyLog.d("UnzipFile", zipFile + "   " + dbFileDirPath);
        try {
            FileInputStream fin = new FileInputStream(zipFile);
            ZipInputStream zin = new ZipInputStream(fin);
            ZipEntry ze = null;
            while ((ze = zin.getNextEntry()) != null) {
//                String dbFilePath = dbFileDirPath + "/" + ze.getName();
                String dbFilePath = ze.getName();
                File file = new File(dbFileDirPath);
                if (!file.exists()) {
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        MyLog.e(TAG, "", e);
                    }
                }

                FileOutputStream fout = new FileOutputStream(dbFilePath);
                for (int c = zin.read(); c != -1; c = zin.read()) {
                    fout.write(c);
                }

                zin.closeEntry();
                fout.close();
            }
            zin.close();
            fin.close();
            return true;
        } catch (Exception e) {
            MyLog.d("BackUpRestoreError", "Zip occurred - " + e);
            return false;
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {

        MyLog.d("BackUpRestoreError - " + result.getErrorCode(), result.toString());

        switch (result.getErrorCode()) {
            case ConnectionResult.INVALID_ACCOUNT:
                showGmailLoginAlert("Login with " + mBackupGmailId + " in gmail app and then try to backup your "
                        + " chat history.");
                break;

            case ConnectionResult.INTERNAL_ERROR:
                Toast.makeText(this, "Internal Error Occurred", Toast.LENGTH_SHORT).show();
                break;

            case ConnectionResult.TIMEOUT:
                Toast.makeText(this, "Timeout", Toast.LENGTH_SHORT).show();
                break;

            case ConnectionResult.NETWORK_ERROR:
                Toast.makeText(this, "Network Error", Toast.LENGTH_SHORT).show();
                break;

            default:
                if (!result.hasResolution()) {
                    // show the localized error dialog.
                    if (result.getErrorCode() == ConnectionResult.INVALID_ACCOUNT) {
                        showGmailLoginAlert("Login with " + mBackupGmailId + " in gmail app and then try to restore your "
                                + " chat history. If you skip this step, you can't restore your messages.");
                    } else {
                        if (result.hasResolution()) {
                            try {
                                result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
                            } catch (Exception e) {
                                MyLog.e("Restore", "Exception while starting resolution activity", e);
                            }
                        } else {
//                            GoogleApiAvailability.getInstance().getErrorDialog(this, result.getErrorCode(), 0).show();
                            new AlertDialog.Builder(ChatBackUpRestoreActivity.this)
                                    .setTitle("Signin Failed")
                                    .setMessage("Try with different account")
                                    .setCancelable(false)
                                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // Whatever...
                                            dialog.dismiss();
                                        }
                                    }).show();
                            return;
                        }
                    }
                    return;
                } else {
                    // The failure has a resolution. Resolve it.
                    // Called typically when the app is not yet authorized, and an
                    // authorization
                    // dialog is displayed to the user.
                    try {
                        result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
                    } catch (Exception e) {
                        MyLog.e("Chat restore", "Exception while starting resolution activity", e);
                    }
                }
        }
    }

    private void showGmailLoginAlert(String msg) {
        CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(msg);
        dialog.setPositiveButtonText("Proceed");
        dialog.setNegativeButtonText("Skip");
        dialog.setCancelable(false);
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                Intent addAccountIntent = new Intent(android.provider.Settings.ACTION_ADD_ACCOUNT)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    addAccountIntent.putExtra(Settings.EXTRA_ACCOUNT_TYPES, new String[]{"com.google"});
                }
                startActivity(addAccountIntent);
            }

            @Override
            public void onNegativeButtonClick() {
                performSkipBackupMessages();
//                goHomeScreen();
            }
        });
        dialog.show(getSupportFragmentManager(), "Account Alert");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //mSocketManager.disconnect();
    }
}
