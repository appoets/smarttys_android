package com.app.Smarttys.app.activity;

/**
 * Created by CAS63 on 12/17/2016.
 */

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.WallpaperAdapter;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.Session;

public class WallpaperColor extends CoreActivity {
    final Context context = this;
    public String[] mThumbIds = {
            "#212F3C", "#839192", "#CA6F1E", "#F5B041", "#82E0AA", "#5DADE2", "#A569BD", "#F5B7B1", "#D98880", "#0B5345"
    };
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.solid_colors);
        getSupportActionBar().show();
        setTitle("Solid Color");
        session = new Session(WallpaperColor.this);


        final GridView gridview = findViewById(R.id.gridview);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                String itemValue = mThumbIds[position];

                //System.out.print("------------------------------------->" + itemValue);
                session.putgalleryPrefs("");
                session.putColor(itemValue);
                Toast.makeText(WallpaperColor.this, getString(R.string.wallpaper_set), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        gridview.setAdapter(new WallpaperAdapter(context, mThumbIds));

    }


}