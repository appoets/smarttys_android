package com.app.Smarttys.app.activity;

public interface ItemClickListener {
    void itemClick(int position);
}