package com.app.Smarttys.app.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Log;

import com.app.Smarttys.core.model.SmarttyContactModel;

import java.util.ArrayList;

/**
 * Created by Administrator on 10/21/2016.
 */
public class LoadContacts extends AsyncTask<Void, Void, Void> {
    static ArrayList<SmarttyContactModel> data;
    Context context;
    ContactsCallBack contactsCallBack;
    private ProgressDialog dialog;
    public LoadContacts(Context context) {
        this.context = context;
    }

    public void setContactsCallBack(ContactsCallBack contactsCallBack) {
        this.contactsCallBack = contactsCallBack;
    }

    @Override
    protected void onPreExecute() {
        if (data != null && contactsCallBack != null)
            contactsCallBack.loadContact(data);
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (contactsCallBack != null)
            contactsCallBack.loadContact(data);
        super.onPostExecute(aVoid);
    }

    @Override
    protected Void doInBackground(Void... params) {
        data = new ArrayList<>();
        Cursor c = context.getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                null, null, null);
        while (c.moveToNext()) {
            SmarttyContactModel d = new SmarttyContactModel();
            String contactName = c
                    .getString(c
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phNumber = c
                    .getString(c
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            String phNo = phNumber.replace(" ", "").replace("(", "").replace(")", "").replace("-", "");
            d.setNumberInDevice(phNo);
            Log.e("LoadContacts", "setNumberInDevice" + phNo);

            d.setFirstName(contactName);

            data.add(d);
        }
        return null;
    }

    interface ContactsCallBack {
        void loadContact(ArrayList<SmarttyContactModel> data);
    }
}