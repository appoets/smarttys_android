package com.app.Smarttys.app.adapter;

/**
 *
 */

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

public class PagerAdapter extends FragmentStatePagerAdapter {
    List<Fragment> mTabsList;

    public PagerAdapter(FragmentManager fm, List<Fragment> mTabsList) {
        super(fm);
        this.mTabsList = mTabsList;
    }

    @Override
    public Fragment getItem(int position) {
        return mTabsList.get(position);
    }

    @Override
    public int getCount() {
        return mTabsList.size();
    }
}