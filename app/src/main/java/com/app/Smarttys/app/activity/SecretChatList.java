package com.app.Smarttys.app.activity;


import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.SecretChatListAdapter;
import com.app.Smarttys.app.dialog.ProfileImageDialog;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by CAS63 on 4/20/2017.
 */
public class SecretChatList extends CoreActivity {

    private static final String TAG = "SecretChatList";
    public static boolean isChatListPage;
    private final int QR_REQUEST_CODE = 25;
    AvnNextLTProRegTextView archivetext;
    Getcontactname getcontactname;
    // View view;
    Bitmap myTemp = null;
    HashMap<String, MessageItemChat> uniqueStore = new HashMap<>();
    HashMap<String, Long> typingEventMap = new HashMap<>();
    String phoneno;
    //FastScrollRecyclerView fastScroller;
    String archivecount;
    String username, profileimage, value;
    Session session;
    int count = 0;
    String receiverDocumentID = new String();
    Boolean muteflag = true;
    String sessionarchived;
    private SecretChatListAdapter mAdapter;
    private RecyclerView recyclerView_chat;
    private ArrayList<MessageItemChat> mChatData = new ArrayList<>();
    private SearchView searchView;
    private AvnNextLTProRegTextView userMessagechat;
    private RelativeLayout archive_relativelayout;
    private MessageDbController db;
    private String uniqueCurrentID;
    private Menu menuLongClick, menuNormal;
    private UserInfoSession userInfoSession;
    private ImageView chatIcon, searchIcon;
    private EditText editTextSearch;
    private ImageView backarrow, backButton;
    private TextView selectchat;
    private ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
    private Handler typingHandler;
    private long timeOutTyping = 3000;
    Runnable typingRunnable = new Runnable() {
        @Override
        public void run() {
            long currentTime = Calendar.getInstance().getTimeInMillis();

            ArrayList<String> removeList = new ArrayList<>();
//            Iterator it = typingEventMap.entrySet().iterator();
            for (Iterator it = typingEventMap.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry pair = (Map.Entry) it.next();
                String key = (String) pair.getKey();
                long timeOut = (long) pair.getValue();

                long timeDiff = currentTime - timeOut;
                if (timeDiff > MessageFactory.TYING_MESSAGE_TIMEOUT) {
//                    typingEventMap.remove(key);

                    MessageItemChat chat = uniqueStore.get(key);
                    int index = mChatData.indexOf(chat);
                    chat.setTypingAt(0);

                    mChatData.set(index, chat);
                    uniqueStore.put(key, chat);
                    mAdapter.notifyDataSetChanged();
                }
                /*else {
                    typingEventMap.put(key, timeDiff);
                }*/

                //System.out.println(pair.getKey() + " = " + pair.getValue());
            }
            if (typingEventMap.size() > 0) {
                timeOutTyping = currentTime - Collections.max(typingEventMap.values());
                MyLog.d("MaxTimeout", "" + timeOutTyping);
                typingHandler.postDelayed(typingRunnable, timeOutTyping);
            } else {
                typingHandler.removeCallbacks(typingRunnable);
            }
        }
    };
    private LinearLayoutManager mChatListManager;
    private int mFirstVisibleItemPosition, mLastVisibleItemPosition;
    private RecyclerView.OnScrollListener chatListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            notifyScrollChanged();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.secret_chatlist);
        getcontactname = new Getcontactname(SecretChatList.this);
        initData();
    }

    private void initData() {
        uniqueCurrentID = SessionManager.getInstance(SecretChatList.this).getCurrentUserID();
        userInfoSession = new UserInfoSession(SecretChatList.this);
        chatIcon = findViewById(R.id.chatIcon);
        searchIcon = findViewById(R.id.search);
        editTextSearch = findViewById(R.id.etSearch);
        selectchat = findViewById(R.id.selectchat);
        backarrow = findViewById(R.id.backarrow);
        backButton = findViewById(R.id.backarrow_secretchat);
        userMessagechat = findViewById(R.id.userMessagechat);
        archive_relativelayout = findViewById(R.id.archive_relativelayout);
        archivetext = findViewById(R.id.archive);
        archivetext.setVisibility(View.GONE);
        recyclerView_chat = findViewById(R.id.rv);
        recyclerView_chat.setHasFixedSize(true);

        mAdapter = new SecretChatListAdapter(SecretChatList.this, mChatData);
        mChatListManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView_chat.setLayoutManager(mChatListManager);
        recyclerView_chat.setItemAnimator(new DefaultItemAnimator());
        recyclerView_chat.setAdapter(mAdapter);
        recyclerView_chat.addOnScrollListener(chatListener);
        notifyScrollChanged();

        db = CoreController.getDBInstance(this);
        session = new Session(SecretChatList.this);
        archivecount = String.valueOf(session.getarchivecount());
       /* fastScroller = (FastScrollRecyclerView) findViewById(R.id.fastscroll);
        fastScroller.setLayoutManager(new LinearLayoutManager(SecretChatList.this));
        fastScroller.setAdapter(mAdapter);*/


        typingHandler = new Handler();

        if (session.getarchivecount() <= 0) {
            archive_relativelayout.setVisibility(View.GONE);
        } else {
            archive_relativelayout.setVisibility(View.VISIBLE);
            archivetext.setText("Archived chats" + "(" + archivecount + ")");
        }

        archivetext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aIntent = new Intent(SecretChatList.this, Archivelist.class);
                aIntent.putExtra("from", "chatlist");
                startActivity(aIntent);

            }
        });

        chatIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent secretChat = new Intent(getApplicationContext(), SelectSecretChatContact.class);
                startActivity(secretChat);


            }
        });


        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSearch();
            }


        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goHomeScreen();
            }
        });

        mAdapter.setChatListItemClickListener(new SecretChatListAdapter.ChatListItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (view.getId() == R.id.storeImage) {
                    String userId = mChatData.get(position).getMessageId().split("-")[1];

                    Bundle bundle = new Bundle();
                    bundle.putSerializable("MessageItem", mChatData.get(position));

                    /*if(view instanceof CircleImageView) {
                        CircleImageView imageView = (CircleImageView) view;
                        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
                        SerializeBitmap serializeBitmap = new SerializeBitmap();
                        serializeBitmap.setBitmap(bitmap);

                        bundle.putSerializable("ProfilePic", serializeBitmap);
                    }*/

                    bundle.putSerializable("ProfilePic", null);
                    bundle.putSerializable("GroupChat", false);
                    bundle.putString("userID", userId);
//                    bundle.putLong("imageTS", imageTS);
                    bundle.putBoolean("FromSecretChat", true);

                    ProfileImageDialog dialog = new ProfileImageDialog();
                    dialog.setArguments(bundle);
                    dialog.show(getSupportFragmentManager(), "profile");
                } else {

                    if (position >= 0) {
                        NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notifManager.cancelAll();

                        Intent intent = new Intent(SecretChatList.this, SecretChatViewActivity.class);
//                        Intent intent = new Intent(getActivity(), NewChatViewActivity.class);
                        MessageItemChat e = mAdapter.getItem(position);
                        if (e != null) {
                            if (e.getStatus() != null) {
                                String[] array = e.getMessageId().split("-");
                                username = e.getSenderName();
                                profileimage = e.getAvatarImageUrl();
                                intent.putExtra("receiverUid", e.getNumberInDevice());
                                intent.putExtra("receiverName", e.getSenderName());
                                if (array[0].equalsIgnoreCase(uniqueCurrentID)) {
                                    receiverDocumentID = array[1];
                                } else if (array[1].equalsIgnoreCase(uniqueCurrentID)) {
                                    receiverDocumentID = array[0];
                                }
                                intent.putExtra("documentId", receiverDocumentID);
                                intent.putExtra("Username", e.getSenderName());
                                intent.putExtra("Image", e.getAvatarImageUrl());
                                intent.putExtra("msisdn", e.getSenderMsisdn());
                                intent.putExtra("type", 0);

                                startActivity(intent);
                                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            } else {
                                Toast.makeText(SecretChatList.this, getResources().getString(R.string.status_not_available), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        });

    }

    private void goHomeScreen() {
//        Intent intent = new Intent(this, HomeScreen.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
        finish();
    }

    private void performSearch() {
        backarrow.setVisibility(View.VISIBLE);
        searchIcon.setVisibility(View.GONE);
        chatIcon.setVisibility(View.GONE);
        selectchat.setVisibility(View.GONE);
        //selectcontactmember.setVisibility(View.GONE);
        editTextSearch.setVisibility(View.VISIBLE);
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                if (cs.length() > 0) {
                    SecretChatList.this.mAdapter.getFilter().filter(cs);
                } else {

                    mAdapter.updateInfo(mChatData);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }


        });

        backButton.setVisibility(View.GONE);

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextSearch.getText().clear();
                mAdapter.updateInfo(mChatData);
                editTextSearch.setVisibility(View.GONE);
                searchIcon.setVisibility(View.VISIBLE);
                chatIcon.setVisibility(View.VISIBLE);
                selectchat.setVisibility(View.VISIBLE);
                backarrow.setVisibility(View.GONE);
                backButton.setVisibility(View.VISIBLE);

                if (SecretChatList.this.getCurrentFocus() != null) {
                    InputMethodManager inputMethodManager =
                            (InputMethodManager) getApplicationContext().getSystemService(
                                    Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(
                            SecretChatList.this.getCurrentFocus().getWindowToken(), 0);
                }
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        loadDbChatItem();
        isChatListPage = true;

        archivecount = String.valueOf(session.getarchivecount());
        if (session.getarchivecount() <= 0) {
            archive_relativelayout.setVisibility(View.GONE);
        } else {
            archive_relativelayout.setVisibility(View.VISIBLE);
            archivetext.setText("Archived chats" + "(" + archivecount + ")");
        }

        /*try {
            sendGetAllChatHistory();
        } catch (JSONException e) {
        }*/
    }

    private void loadDbChatItem() {

        ArrayList<MessageItemChat> databases = db.selectChatList(MessageFactory.CHAT_TYPE_SECRET);
        Session session = new Session(SecretChatList.this);
        uniqueStore.clear();

        for (MessageItemChat msgItem : databases) {
            if (msgItem.getTextMessage() == null)
                continue;
            String docID = uniqueCurrentID + "-" + msgItem.getReceiverID() + "-" + MessageFactory.CHAT_TYPE_SECRET;

            if (!session.getarchive(docID)) {
                String[] array = docID.split("-");
                String receiverID;
                if (array[0].equalsIgnoreCase(uniqueCurrentID)) {
                    receiverID = array[1];
                } else {
                    receiverID = array[0];
                }

                if (msgItem.getSenderMsisdn() != null) {
                    String name = getcontactname.getSendername(receiverID, msgItem.getSenderMsisdn());
                    msgItem.setSenderName(name);
                }

                long serverTimeDiff = SessionManager.getInstance(SecretChatList.this).getServerTimeDifference();
                ArrayList<MessageItemChat> chatItemsCount = db.selectAllSecretChatMessage(docID, serverTimeDiff);
                if (chatItemsCount != null && chatItemsCount.size() > 0)
                    uniqueStore.put(docID, msgItem);
            }
        }
        updateChatList();
        if (session.getarchivecount() == 0) {
            archive_relativelayout.setVisibility(View.GONE);
        } else {
            archive_relativelayout.setVisibility(View.VISIBLE);
            archivetext.setText("Archived chats" + "(" + archivecount
                    + ")");
        }

        notifyScrollChanged();
    }

    private void updateChatList() {
        mChatData.clear();
        for (MessageItemChat value : uniqueStore.values()) {
            String phContactName = getcontactname.getSendername(value.getReceiverID(), value.getSenderMsisdn());
            value.setSenderName(phContactName);
            mChatData.add(value);
        }
        if (mChatData.size() > 0) {
            userMessagechat.setVisibility(View.GONE);
        }
        if (mChatData != null)
            Collections.sort(mChatData, new ChatListSorter());
        mAdapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_TYPING)) {
            loadTypingStatus(event);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_MESSAGE)) {
            loadMessage(event);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_QR_DATA)) {
            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                MyLog.e("web", "" + object);
                String userId = object.getString("_id");
                MyLog.e("userId", "" + userId);
                if (userId.equalsIgnoreCase(uniqueCurrentID)) {
                    Toast.makeText(SecretChatList.this, getResources().getString(R.string.app_name) + " web connected", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_VIEW_CHAT)) {
            Object[] obj = event.getObjectsArray();
            changeMsgViewedStatus(obj[0].toString());
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_DELETE_CHAT)) {
            Object[] obj = event.getObjectsArray();
            loadDeleteChat(obj[0].toString());
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_ARCHIVE_UNARCHIVE)) {
            Object[] obj = event.getObjectsArray();
            //loadarchivelist(obj[0].toString());
            loadarchive(obj[0].toString());
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_PRIVACY_SETTINGS)) {
            loadPrivacySetting(event);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_MUTE)) {
            loadMuteMessage(event.getObjectsArray()[0].toString());
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GET_USER_DETAILS)) {
            loadUserDetails(event.getObjectsArray()[0].toString());
        }
    }

    private void loadMuteMessage(String data) {
        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");
            String chatType = object.getString("type");
            String secretType = "no";
            if (object.has("secret_type")) {
                secretType = object.getString("secret_type");
            }

            if (from.equalsIgnoreCase(uniqueCurrentID) && chatType.equalsIgnoreCase("single")
                    && secretType.equalsIgnoreCase("yes") && mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadPrivacySetting(ReceviceMessageEvent event) {
        mAdapter.notifyDataSetChanged();
    }

    private void loadarchive(String data) {
        try {
            JSONObject object = new JSONObject(data);

            try {
                String from = object.getString("from");
                String convId = object.getString("convId");
                String type = object.getString("type");
                String status = object.getString("status");

                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    String receiverId;
                    if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                        receiverId = convId;
                    } else {
                        receiverId = userInfoSession.getReceiverIdByConvId(convId);
                    }

                    if (!receiverId.equals("")) {
                        String docId = from.concat("-").concat(receiverId);
                        if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                            docId = docId.concat("-g");
                        }

                        if (uniqueStore.containsKey(docId)) {
                            uniqueStore.remove(docId);
                            updateChatList();
                            mAdapter.notifyDataSetChanged();
                        }
                    }

                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }

    private void loadDeleteChat(String data) {
        try {
            JSONObject object = new JSONObject(data);

            try {
                String from = object.getString("from");
                String convId = object.getString("convId");
                String type = object.getString("type");

                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    String receiverId;
                    if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                        receiverId = convId;
                    } else {
                        receiverId = userInfoSession.getReceiverIdByConvId(convId);
                    }

                    if (!receiverId.equals("")) {
                        String docId = from.concat("-").concat(receiverId);
                        if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                            docId = docId.concat("-g");
                        }

                        if (uniqueStore.containsKey(docId)) {
                            uniqueStore.remove(docId);
                            updateChatList();
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void changeMsgViewedStatus(String data) {

        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");
            String mode = object.getString("mode");

            if (from.equalsIgnoreCase(uniqueCurrentID) && mode.equalsIgnoreCase("web")) {
                String to = object.getString("to");
                String chatType = object.getString("type");

                if (chatType.equalsIgnoreCase("single")) {
                    loadDbChatItem();
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }

  /*  private void sendAckToServer(String to, String doc_id, String id) {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_MESSAGE_ACK);
        MessageAck ack = (MessageAck) MessageFactory.getMessage(MessageFactory.message_ack, SecretChatList.this);
        messageEvent.setMessageObject((JSONObject) ack.getMessageObject(to, doc_id, MessageFactory.DELIVERY_STATUS_DELIVERED, id, true));
        EventBus.getDefault().post(messageEvent);
    }*/

    private void loadMessage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            String message;
            JSONObject objects = new JSONObject(array[0].toString());
            //{"type":0,"payload":"hjjk","id":"5808c7f76090cc12841d4b46-5808cb46bf5aa60a70fd5c23","from":"5808c7f76090cc12841d4b46","doc_id":"5808c7f76090cc12841d4b46-5808cb46bf5aa60a70fd5c23-1477902694564","thumbnail":"","dataSize":"","timestamp":1477902830388,"convId":"5816e58855dea4ec14bf7dd7"}
            String type = objects.getString("type");
            String secretType = objects.getString("secret_type");
            if (secretType.equalsIgnoreCase("yes")) {
                String payLoad = objects.getString("payload");
                String id = (String) objects.get("id");
                String from = objects.getString("from");
                String to = objects.getString("to");
                String doc_id = (String) objects.get("doc_id");
                String thumbnail = objects.getString("thumbnail");
                String name = objects.getString("Name");
                String senderMsisdn = objects.getString("ContactMsisdn");
                String dataSize = objects.getString("filesize");
                String convId = objects.getString("convId");
                String recordId = objects.getString("recordId");
                String ts = objects.getString("timestamp");
                MessageItemChat item = new MessageItemChat();
                item.setConvId(convId);
                item.setRecordId(recordId);
                item.setTS(ts);
                item.setReceiverID(from);

                try {
                    String phContactName = getcontactname.getSendername(from, senderMsisdn);

//                item.setSenderName("" + new String(Base64.decode(name, Base64.DEFAULT), "UTF-8"));
                    item.setSenderName(phContactName);
                    item.setSenderMsisdn(senderMsisdn);
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }


                item.setMessageType(type);

                item.sethasNewMessage(true);
                item.setTextMessage(payLoad);
                item.setCount(1);
                String uniqueID = new String();
                String messageAck = new String();
                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    uniqueID = from + "-" + to;
                    messageAck = to;
                    item.setIsSelf(true);
                } else if (to.equalsIgnoreCase(uniqueCurrentID)) {
                    uniqueID = to + "-" + from;
                    messageAck = from;
//                    sendAckToServer(from, doc_id, "" + id);
                    item.setIsSelf(false);
                }

                uniqueID = uniqueID + "-" + MessageFactory.CHAT_TYPE_SECRET;

                item.setMessageId(uniqueID + "-" + id);

                JSONObject linkObj = objects.getJSONObject("link_details");

                String recLink = "";
                if (linkObj.has("url")) {
                    recLink = linkObj.getString("url");
                }

                String recLinkTitle = "";
                if (linkObj.has("title")) {
                    recLinkTitle = linkObj.getString("title");
                }

                String recLinkDesc = "";
                if (linkObj.has("description")) {
                    recLinkDesc = linkObj.getString("description");
                }

                String recLinkImgUrl = "";
                if (linkObj.has("image")) {
                    recLinkImgUrl = linkObj.getString("image");
                }

                String recLinkImgThumb = "";
                if (linkObj.has("thumbnail_data")) {
                    recLinkImgThumb = linkObj.getString("thumbnail_data");
                }

                if (type.equalsIgnoreCase("" + MessageFactory.text)) {
                    item.setTextMessage(payLoad);
                } else if (type.equalsIgnoreCase("" + MessageFactory.contact)) {
//                item.setContactInfo(payLoad);
                    String contactName = objects.getString("contact_name");
                    String contactNumber = objects.getString("createdTomsisdn");
                    String contactDetails = objects.getString("contact_details");
                    if (objects.has("createdTo")) {
                        String contactSmarttyId = objects.getString("createdTo");
                        item.setContactSmarttyId(contactSmarttyId);
                    }
                    item.setContactName(contactName);
                    item.setContactNumber(contactNumber);
                    item.setDetailedContacts(contactDetails);
                } else if (type.equalsIgnoreCase("" + MessageFactory.picture)) {
                    if (thumbnail != null && !thumbnail.equals("")) {
                        File dir = new File(MessageFactory.IMAGE_STORAGE_PATH);
                        if (!dir.exists()) {
                            dir.mkdir();
                        }
                        String localFileName = MessageFactory.getMessageFileName(
                                MessageFactory.picture, id, getFileExtension(thumbnail));
                        String filePath = MessageFactory.IMAGE_STORAGE_PATH + localFileName;
                        item.setChatFileLocalPath(filePath);
                    }

                    item.setChatFileServerPath(thumbnail);
                    item.setFileSize(dataSize);
                } else if (type.equalsIgnoreCase("" + MessageFactory.audio)) {

                    if (thumbnail != null && !thumbnail.equals("")) {
                        File dir = new File(MessageFactory.AUDIO_STORAGE_PATH);
                        if (!dir.exists()) {
                            dir.mkdir();
                        }
                        String localFileName = MessageFactory.getMessageFileName(
                                MessageFactory.audio, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                        String filePath = MessageFactory.AUDIO_STORAGE_PATH + localFileName;
                        item.setChatFileLocalPath(filePath);
                    }

                    item.setChatFileServerPath(thumbnail);

                    String thumbData = objects.getString("thumbnail_data");
                    item.setThumbnailData(thumbData);
                    item.setFileSize(dataSize);
                }

                item.setTextMessage(payLoad);
                item.setWebLink(recLink);
                item.setWebLinkTitle(recLinkTitle);
                item.setWebLinkDesc(recLinkDesc);
                item.setWebLinkImgUrl(recLinkImgUrl);
                item.setWebLinkImgThumb(recLinkImgThumb);
                item.setFileBufferAt(0);
                item.setUploadDownloadProgress(0);
                item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_NOT_START);

           /* MessageItemChat topMsgItem = db.selectTopChatMessage(uniqueID);
            String prevMsgCount = topMsgItem.getNewMessageCount();
            if (prevMsgCount != null) {
                if (Integer.parseInt(prevMsgCount) > 0) {
                    int count = Integer.parseInt(prevMsgCount) + 1;
                    item.setNewMessageCount(String.valueOf(count));
                } else {
                    item.setNewMessageCount("1");
                }
            }*/

                if (session.getarchivecount() != 0) {
                    if (session.getarchive(uniqueCurrentID + "-" + to))
                        session.removearchive(uniqueCurrentID + "-" + to);

                }
                if (session.getarchivecountgroup() != 0) {
                    if (session.getarchivegroup(uniqueCurrentID + "-" + to + "-g"))
                        session.removearchivegroup(uniqueCurrentID + "-" + to + "-g");

                }

                // db.updateChatMessage(uniqueID, item);
                uniqueStore.put(uniqueID, item);
                updateChatList();

            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

/*
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.clear();
        Log.e("flag", "" + myMenuClickFlag);
        menuNormal = menu;
        menuLongClick = menu;
        getMenuInflater().inflate(R.menu.secret_chatlist, menu);
        MenuItem searchItem = menu.findItem(R.id.chatsc_searchIcon);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        //setSearchViewCollapseExpand(menu);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.setIconifiedByDefault(true);
                searchView.setIconified(true);
                searchView.setQuery("", false);
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (newText.equals("") && newText.isEmpty()) {
                    searchView.clearFocus();
                    //closeKeypad();
                }
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                menu.findItem(R.id.chatsceen_chatIcon).setVisible(true);
                return false;
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.findItem(R.id.chatsceen_chatIcon).setVisible(false);
            }
        });

        SearchManager searchManager = (SearchManager) SecretChatList.this.getSystemService(Context.SEARCH_SERVICE);
        searchView.setIconifiedByDefault(true);
        searchView.setQuery("", false);
        searchView.clearFocus();
        searchView.setIconified(true);

        AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchTextView.setTextColor(Color.WHITE);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, 0);
        } catch (Exception e) {
            Log.e(TAG,"",e);
        }

        return super.onCreateOptionsMenu(menu);
    }
*/


    /*private void setSearchViewCollapseExpand(Menu menu) {

        MenuItem searchItem = menu.findItem(R.id.chatsc_searchIcon);

        if (searchItem != null) {

            MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    HomeScreen homeScreen = new HomeScreen();
                    homeScreen.getSupportActionBar().show();
                    return true;
                }

                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    HomeScreen homeScreen = new HomeScreen();
                    homeScreen.getSupportActionBar().hide();
                    return true;
                }
            });

            MenuItemCompat.setActionView(searchItem, searchView);
        }
    }*/

    private String getFileExtension(String fileName) {
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return "." + fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == QR_REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK) {
            String qrData = data.getStringExtra("QRData");

            try {
                JSONObject object = new JSONObject();
                object.put("_id", SessionManager.getInstance(SecretChatList.this).getCurrentUserID());
                object.put("msisdn", SessionManager.getInstance(SecretChatList.this).getPhoneNumberOfCurrentUser());
                object.put("random", qrData);

                SendMessageEvent qrEvent = new SendMessageEvent();
                qrEvent.setEventName(SocketManager.EVENT_QR_DATA);
                qrEvent.setMessageObject(object);
                EventBus.getDefault().post(qrEvent);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }

        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // Stop update of typing message
        if (typingRunnable != null) {
            typingHandler.removeCallbacks(typingRunnable);
        }

        isChatListPage = false;
    }

    private void loadTypingStatus(ReceviceMessageEvent event) {
        Object[] object = event.getObjectsArray();
        try {
            JSONObject jsonObject = new JSONObject(object[0].toString());
            String from = (String) jsonObject.get("from");
            String to = (String) jsonObject.get("to");
            String convId = (String) jsonObject.get("convId");
            String type = (String) jsonObject.get("type");

            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
            boolean blockedStatus = contactDB_sqlite.getBlockedStatus(from, false).equals("1");

            if (to.equalsIgnoreCase(uniqueCurrentID) && !blockedStatus) {
                // Document id reversed from receiver
                String docId = to + "-" + from + "-secret";

                if (uniqueStore.containsKey(docId)) {

                    if (userInfoSession.hasChatConvId(docId)) {
                        String savedConvId = userInfoSession.getChatConvId(docId);
                        if (savedConvId.equalsIgnoreCase(convId)) {
                            MessageItemChat msgItem = uniqueStore.get(docId);
                            int index = mChatData.indexOf(msgItem);

                            long currentTime = Calendar.getInstance().getTimeInMillis();

                            msgItem.setTypingAt(currentTime);
                            handleReceiverTypingEvent(docId, currentTime);
                            mChatData.set(index, msgItem);
                            uniqueStore.put(docId, msgItem);
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                }

            }

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void handleReceiverTypingEvent(String docId, long currentTime) {
        if (typingEventMap.size() == 0) {
            timeOutTyping = MessageFactory.TYING_MESSAGE_TIMEOUT;
        }
        typingEventMap.put(docId, currentTime);
        typingHandler.postDelayed(typingRunnable, timeOutTyping);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goHomeScreen();
    }

    private void notifyScrollChanged() {
        mFirstVisibleItemPosition = mChatListManager.findFirstVisibleItemPosition();
        mLastVisibleItemPosition = mChatListManager.findLastVisibleItemPosition();
        int totalItems = mChatData.size();

/*        try {
            if (totalItems > mFirstVisibleItemPosition && totalItems > mLastVisibleItemPosition) {
                for (int i = mFirstVisibleItemPosition; i <= mLastVisibleItemPosition; i++) {
                    String userId = mChatData.get(i).getReceiverID();
                 //   MessageService.getUpdatedUserDetails(EventBus.getDefault(), userId);
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            Log.e(TAG,"",e);
        }*/
    }

    private void loadUserDetails(String data) {
        try {
            JSONObject object = new JSONObject(data);
            String userId = object.getString("id");
            String docId = uniqueCurrentID + "-" + userId;
            if (uniqueStore.containsKey(docId)) {
                try {
                    for (int i = mFirstVisibleItemPosition; i <= mLastVisibleItemPosition; i++) {
                        if (mChatData.get(i).getReceiverID().equalsIgnoreCase(userId)) {
                            mAdapter.notifyItemChanged(i);
                        }
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    MyLog.e(TAG, "", e);
                }
//            receivedUserDetailsMap.put(userId, Calendar.getInstance().getTimeInMillis());
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private class ChatListSorter implements Comparator<MessageItemChat> {

        @Override
        public int compare(MessageItemChat item1, MessageItemChat item2) {
            long item1Time = Long.parseLong(item1.getTS());
            long item2Time = Long.parseLong(item2.getTS());

            if (item1Time < item2Time) {
                return 1;
            } else {
                return -1;
            }
        }

    }
}