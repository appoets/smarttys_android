package com.app.Smarttys.app.model;

/**
 * Created by user145 on 4/24/2018.
 */

public class CreatePost_Model {
    private String docId;
    private String userId;
    private String path;
    private String uploadType;
    private String id, type, original_FileName, fileName;
    private String fileSize, fileHeight, fileWidth, upload_Progress;

    private String thumbnail_Data, duration, audio_type;

    private boolean isImage;
    private boolean isVideo;
    private String post_Type = "", record_ID = "", collections_Name = "", Uniquechild_id = "";

    private String Status = "";

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isImage() {
        return isImage;
    }

    public void setImage(boolean image) {
        isImage = image;
    }

    public boolean isVideo() {
        return isVideo;
    }

    public void setVideo(boolean video) {
        isVideo = video;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUploadType() {
        return uploadType;
    }

    public void setUploadType(String uploadType) {
        this.uploadType = uploadType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileHeight() {
        return fileHeight;
    }

    public void setFileHeight(String fileHeight) {
        this.fileHeight = fileHeight;
    }

    public String getFileWidth() {
        return fileWidth;
    }

    public void setFileWidth(String fileWidth) {
        this.fileWidth = fileWidth;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getOriginal_FileName() {
        return original_FileName;
    }

    public void setOriginal_FileName(String original_FileName) {
        this.original_FileName = original_FileName;
    }

    public String getThumbnail_Data() {
        return thumbnail_Data;
    }

    public void setThumbnail_Data(String thumbnail_Data) {
        this.thumbnail_Data = thumbnail_Data;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getAudio_type() {
        return audio_type;
    }

    public void setAudio_type(String audio_type) {
        this.audio_type = audio_type;
    }

    public String getUpload_Progress() {
        return upload_Progress;
    }

    public void setUpload_Progress(String upload_Progress) {
        this.upload_Progress = upload_Progress;
    }

    public String getPost_Type() {
        return post_Type;
    }

    public void setPost_Type(String post_Type) {
        this.post_Type = post_Type;
    }

    public String getRecord_ID() {
        return record_ID;
    }

    public void setRecord_ID(String record_ID) {
        this.record_ID = record_ID;
    }

    public String getCollections_Name() {
        return collections_Name;
    }

    public void setCollections_Name(String collections_Name) {
        this.collections_Name = collections_Name;
    }

    public String getUniquechild_id() {
        return Uniquechild_id;
    }

    public void setUniquechild_id(String uniquechild_id) {
        Uniquechild_id = uniquechild_id;
    }
}
