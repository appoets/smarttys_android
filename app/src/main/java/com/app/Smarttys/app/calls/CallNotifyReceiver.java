package com.app.Smarttys.app.calls;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

/**
 * Created by CAS60 on 8/2/2017.
 */

public class CallNotifyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent();
        service.setComponent(new ComponentName(context, CallNotifyService.class));
        context.stopService(service);
    }

}
