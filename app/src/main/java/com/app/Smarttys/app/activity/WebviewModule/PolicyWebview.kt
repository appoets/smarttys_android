package com.app.Smarttys.app.activity.WebviewModule

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.app.Smarttys.R

class PolicyWebview : AppCompatActivity(), View.OnClickListener {

    private lateinit var mContext: Activity

    private lateinit var webview_view: WebView
    private lateinit var webView_progressbar: ProgressBar
    private lateinit var ivClose: ImageView

    private var Url: String = ""
    private var type: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_policy_webview)

        mContext = this@PolicyWebview

        init()
    }

    private fun init() {

        webview_view = findViewById(R.id.webview_view)
        webView_progressbar = findViewById(R.id.webView_progressbar)
        ivClose = findViewById(R.id.ivClose)


        webview_view.getSettings().setJavaScriptEnabled(true);
        webview_view.getSettings().setPluginState(WebSettings.PluginState.ON)

        Url = intent.getStringExtra("web_url")

        webview_view.loadUrl(Url)

        OnclickListener()

        webview_view.setWebChromeClient(object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                if (progress < 100 && webView_progressbar.getVisibility() == ProgressBar.GONE) {
                    webView_progressbar.setVisibility(ProgressBar.VISIBLE)
                }
                webView_progressbar.setProgress(progress)

                if (progress == 100) {
                    webView_progressbar.setVisibility(ProgressBar.GONE)


                }
            }
        })
    }

    private fun OnclickListener() {

        ivClose.setOnClickListener(this)

    }


    override fun onClick(v: View?) {

        when (v!!.id) {

            R.id.ivClose
            -> Pageclose()

        }

    }

    private fun Pageclose() {

        finish()
    }

}
