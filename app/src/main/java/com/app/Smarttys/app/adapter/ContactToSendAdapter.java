package com.app.Smarttys.app.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.core.model.ContactToSend;

import java.util.ArrayList;

/**
 * Created by CAS63 on 3/7/2017.
 */
public class ContactToSendAdapter extends RecyclerView.Adapter<ContactToSendAdapter.MyViewHolder> {

    Activity context;
    View itemView;
    private ArrayList<ContactToSend> contacts = new ArrayList<>();
    @NonNull
    private OnItemCheckListener onItemCheckListener;

    public ContactToSendAdapter(Activity context, ArrayList<ContactToSend> ContactToSend, @NonNull OnItemCheckListener onItemCheckListener) {
        super();
        this.context = context;
        this.contacts = ContactToSend;
        this.onItemCheckListener = onItemCheckListener;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.send_contact_list_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final ContactToSend contactToSend = contacts.get(position);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.checkBox.setChecked(!(holder.checkBox.isChecked()));
                if (holder.checkBox.isChecked()) {
                    onItemCheckListener.onItemCheck(contactToSend);
                } else {
                    onItemCheckListener.onItemUncheck(contactToSend);
                }
            }
        });

        holder.Utype.setText(contactToSend.getSubType());
        holder.Unumber.setText(contactToSend.getNumber());
        if (contactToSend.getType().equalsIgnoreCase("Email")) {
            holder.image.setImageResource(R.drawable.send_mail);
        } else if (contactToSend.getType().equalsIgnoreCase("Phone")) {
            holder.image.setImageResource(R.drawable.send_phone);
        } else if (contactToSend.getType().equalsIgnoreCase("Address")) {
            holder.image.setImageResource(R.drawable.send_loc);
        } else if (contactToSend.getType().equalsIgnoreCase("Instant Messenger")) {
            holder.image.setImageResource(R.drawable.send_chat);
        }
        //String image = blockListPojo.getImagePath();
        //  String path =  Constants.SOCKET_IP.concat(image);
    }

    @Override
    public int getItemCount() {
        return this.contacts.size();
    }


    public interface OnItemCheckListener {
        void onItemCheck(ContactToSend ContactToSend);

        void onItemUncheck(ContactToSend ContactToSend);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView Unumber, Utype;
        public ImageView image;
        public CheckBox checkBox;
        View ItemView;


        public MyViewHolder(View ItemView) {
            super(ItemView);
            this.ItemView = ItemView;
            Unumber = ItemView.findViewById(R.id.contact_save_number);

            Utype = ItemView.findViewById(R.id.detail);
            image = ItemView.findViewById(R.id.imageview_contact);
            checkBox = ItemView.findViewById(R.id.checkContact_elmts);
            checkBox.setClickable(false);
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            ItemView.setOnClickListener(onClickListener);
        }
    }


}

