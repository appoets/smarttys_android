package com.app.Smarttys.app.adapter;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Telephony;
import android.text.Html;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.ChatPageActivity;
import com.app.Smarttys.app.activity.ForwardContact;
import com.app.Smarttys.app.activity.ImageZoom;
import com.app.Smarttys.app.activity.Savecontact;
import com.app.Smarttys.app.dialog.ChatLockPwdDialog;
import com.app.Smarttys.app.dialog.CustomMultiTextItemsDialog;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.CommonData;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.TimeStampUtils;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.app.viewholder.VHAudioReceived;
import com.app.Smarttys.app.viewholder.VHAudioSent;
import com.app.Smarttys.app.viewholder.VHContactReceived;
import com.app.Smarttys.app.viewholder.VHContactSent;
import com.app.Smarttys.app.viewholder.VHDocumentReceived;
import com.app.Smarttys.app.viewholder.VHDocumentSent;
import com.app.Smarttys.app.viewholder.VHImageReceived;
import com.app.Smarttys.app.viewholder.VHImageSent;
import com.app.Smarttys.app.viewholder.VHInfoMessage;
import com.app.Smarttys.app.viewholder.VHLocationReceived;
import com.app.Smarttys.app.viewholder.VHLocationSent;
import com.app.Smarttys.app.viewholder.VHMessageReceived;
import com.app.Smarttys.app.viewholder.VHMessageSent;
import com.app.Smarttys.app.viewholder.VHScreenShotTaken;
import com.app.Smarttys.app.viewholder.VHVideoReceived;
import com.app.Smarttys.app.viewholder.VHVideoSent;
import com.app.Smarttys.app.viewholder.VHWebLinkReceived;
import com.app.Smarttys.app.viewholder.VHWebLinkSent;
import com.app.Smarttys.app.viewholder.VHservermessage;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.ChatLockPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.MultiTextDialogPojo;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.service.Constants;
import com.app.Smarttys.core.smarttyhelperclass.SmarttyImageUtils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by CAS60 on 4/20/2017.
 */
public class SecretChatMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private static final String TAG = "SecretAdapter" + ">>>@@@";
    public static int countcheck = 0;
    public static List<SmarttyContactModel> smarttyContacts;
    private final int MESSAGERECEIVED = 0;
    private final int MESSAGESENT = 1;
    private final int IMAGERECEIVED = 2;
    private final int IMAGESENT = 3;
    private final int VIDEORECEIVED = 4;
    private final int VIDEOSENT = 5;
    private final int LOCATIONRECEIVED = 6;
    private final int LOCATIONSENT = 7;
    private final int CONTACTRECEIVED = 8;
    private final int CONTACTSENT = 9;
    private final int AUDIORECEIVED = 10;
    private final int AUDIOSENT = 11;
    private final int ServerMessAGE = 12;
    private final int WEB_LINK_RECEIVED = 13;
    private final int WEB_LINK_SENT = 14;
    private final int DOCUMENT_RECEIVED = 15;
    private final int DOCUMENT_SENT = 16;
    private final int GROUP_EVENT_INFO = 17;
    private final int SCREEN_SHOT_TAKEN = 18;
    Getcontactname getcontactname;
    private ArrayList<MessageItemChat> mListData = new ArrayList<>();
    private Context mContext;
    private FragmentManager fragmentManager;
    private MediaPlayer mPlayer;
    private String thumnail = "", mCurrentUserId;
    private ItemFilter mFilter = new ItemFilter();
    private Boolean FirstItemSelected = false;
    private String smarttycontactid = "";
    private int lastPlayedAt = -1;
    private Timer mTimer;
    private String sessionTxtSize = "Small";
    private UserInfoSession userInfoSession;
    private int selectedItemColor, unSelectedItemColor;
    private Typeface face = CoreController.getInstance().getAvnNextLTProRegularTypeface();

    public SecretChatMessageAdapter(Context mContext, ArrayList<MessageItemChat> mListData, FragmentManager fragmentManager) {
        this.mListData = mListData;
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
        setHasStableIds(true);

        userInfoSession = new UserInfoSession(mContext);
        Session session = new Session(mContext);


        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        getcontactname = new Getcontactname(mContext);
        selectedItemColor = ContextCompat.getColor(mContext, R.color.selected_chat);
        unSelectedItemColor = Color.TRANSPARENT;
        sessionTxtSize = session.gettextsize();

    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    @Override
    public int getItemCount() {
        return this.mListData.size();
    }

    @Override
    public int getItemViewType(int position) {

        String type = mListData.get(position).getMessageType();

        if (mListData.get(position).isDate()) {
            return ServerMessAGE;
        }

        if (mListData.get(position).isSelf()) {

            switch (type) {
                case ("" + MessageFactory.text):
                    return MESSAGESENT;

                case ("" + MessageFactory.picture):
                    return IMAGESENT;

                case ("" + MessageFactory.video):
                    return VIDEOSENT;

                case ("" + MessageFactory.location):
                    return LOCATIONSENT;

                case ("" + MessageFactory.contact):
                    return CONTACTSENT;

                case ("" + MessageFactory.audio):
                    return AUDIOSENT;

                case ("" + MessageFactory.document):
                    return DOCUMENT_SENT;

                case ("" + MessageFactory.SCREEN_SHOT_TAKEN):
                    return SCREEN_SHOT_TAKEN;

                default:
                    return WEB_LINK_SENT;
            }

        } else {

            switch (type) {
                case ("" + MessageFactory.text):
                    return MESSAGERECEIVED;

                case ("" + MessageFactory.picture):
                    return IMAGERECEIVED;

                case ("" + MessageFactory.video):
                    return VIDEORECEIVED;

                case ("" + MessageFactory.location):
                    return LOCATIONRECEIVED;

                case ("" + MessageFactory.contact):
                    return CONTACTRECEIVED;

                case ("" + MessageFactory.audio):
                    return AUDIORECEIVED;

                case ("" + MessageFactory.document):
                    return DOCUMENT_RECEIVED;

                case ("" + MessageFactory.group_event_info):
                    return GROUP_EVENT_INFO;

                case ("" + MessageFactory.SCREEN_SHOT_TAKEN):
                    return SCREEN_SHOT_TAKEN;
                default:
                    return WEB_LINK_RECEIVED;
            }
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v1;

        switch (viewType) {
            case MESSAGERECEIVED:
                v1 = inflater.inflate(R.layout.message_received_new, viewGroup, false);

                TextView rsize = v1.findViewById(R.id.txtMsg);
                rsize.setTypeface(face);
                if (sessionTxtSize.equalsIgnoreCase("Small"))
                    rsize.setTextSize(11);
                else if (sessionTxtSize.equalsIgnoreCase("Medium"))
                    rsize.setTextSize(14);
                else if (sessionTxtSize.equalsIgnoreCase("Large"))
                    rsize.setTextSize(17);

                viewHolder = new VHMessageReceived(v1);

                break;

            case IMAGERECEIVED:
                v1 = inflater.inflate(R.layout.image_received_latest, viewGroup, false);
                viewHolder = new VHImageReceived(v1);
                break;

            case VIDEORECEIVED:
                v1 = inflater.inflate(R.layout.video_received_latest, viewGroup, false);
                viewHolder = new VHVideoReceived(v1);
                break;

            case LOCATIONRECEIVED:
                v1 = inflater.inflate(R.layout.location_received_new, viewGroup, false);
                viewHolder = new VHLocationReceived(v1);
                break;

            case CONTACTRECEIVED:
                v1 = inflater.inflate(R.layout.contact_received_new, viewGroup, false);
                viewHolder = new VHContactReceived(v1);
                break;

            case DOCUMENT_RECEIVED:
                v1 = inflater.inflate(R.layout.vh_document_received_new, viewGroup, false);

                TextView docRecsize = v1.findViewById(R.id.txtMsg);
                docRecsize.setTypeface(face);
                if (sessionTxtSize.equalsIgnoreCase("Small"))
                    docRecsize.setTextSize(11);
                else if (sessionTxtSize.equalsIgnoreCase("Medium"))
                    docRecsize.setTextSize(14);
                else if (sessionTxtSize.equalsIgnoreCase("Large"))
                    docRecsize.setTextSize(17);

                viewHolder = new VHDocumentReceived(v1);
                break;

            case AUDIORECEIVED:
                v1 = inflater.inflate(R.layout.audio_received_new, viewGroup, false);
                viewHolder = new VHAudioReceived(v1);
                break;

            case WEB_LINK_RECEIVED:
                v1 = inflater.inflate(R.layout.vh_web_link_received_new, viewGroup, false);

                TextView tvLinkMsg = v1.findViewById(R.id.txtMsg);
                tvLinkMsg.setTypeface(face);
                if (sessionTxtSize.equalsIgnoreCase("Small"))
                    tvLinkMsg.setTextSize(11);
                else if (sessionTxtSize.equalsIgnoreCase("Medium"))
                    tvLinkMsg.setTextSize(14);
                else if (sessionTxtSize.equalsIgnoreCase("Large"))
                    tvLinkMsg.setTextSize(17);
                viewHolder = new VHWebLinkReceived(v1);
                break;


            case MESSAGESENT:
                v1 = inflater.inflate(R.layout.message_sent_new, viewGroup, false);
                TextView ssize = v1.findViewById(R.id.txtMsg);
                ssize.setTypeface(face);
                if (sessionTxtSize.equalsIgnoreCase("Small"))
                    ssize.setTextSize(11);
                else if (sessionTxtSize.equalsIgnoreCase("Medium"))
                    ssize.setTextSize(14);
                else if (sessionTxtSize.equalsIgnoreCase("Large"))
                    ssize.setTextSize(17);
                viewHolder = new VHMessageSent(v1);
                break;

            case IMAGESENT:
                v1 = inflater.inflate(R.layout.image_sent_latest, viewGroup, false);
                viewHolder = new VHImageSent(v1);
                break;

            case VIDEOSENT:
                v1 = inflater.inflate(R.layout.video_sent_latest, viewGroup, false);
                viewHolder = new VHVideoSent(v1);
                break;

            case LOCATIONSENT:
                v1 = inflater.inflate(R.layout.location_sent, viewGroup, false);
                viewHolder = new VHLocationSent(v1);
                break;


            case CONTACTSENT:
                v1 = inflater.inflate(R.layout.contact_sent, viewGroup, false);
                viewHolder = new VHContactSent(v1);
                break;

            case ServerMessAGE:
                v1 = inflater.inflate(R.layout.servermessage, viewGroup, false);
                viewHolder = new VHservermessage(v1);
                break;

            case WEB_LINK_SENT:
                v1 = inflater.inflate(R.layout.vh_web_link_sent, viewGroup, false);

                TextView tvLinkMsgSent = v1.findViewById(R.id.txtMsg);
                tvLinkMsgSent.setTypeface(face);
                if (sessionTxtSize.equalsIgnoreCase("Small"))
                    tvLinkMsgSent.setTextSize(11);
                else if (sessionTxtSize.equalsIgnoreCase("Medium"))
                    tvLinkMsgSent.setTextSize(14);
                else if (sessionTxtSize.equalsIgnoreCase("Large"))
                    tvLinkMsgSent.setTextSize(17);

                viewHolder = new VHWebLinkSent(v1);
                break;

            case DOCUMENT_SENT:
                v1 = inflater.inflate(R.layout.vh_document_sent, viewGroup, false);
                TextView docSize = v1.findViewById(R.id.txtMsg);
                docSize.setTypeface(face);
                if (sessionTxtSize.equalsIgnoreCase("Small"))
                    docSize.setTextSize(11);
                else if (sessionTxtSize.equalsIgnoreCase("Medium"))
                    docSize.setTextSize(14);
                else if (sessionTxtSize.equalsIgnoreCase("Large"))
                    docSize.setTextSize(17);
                viewHolder = new VHDocumentSent(v1);
                break;

            case GROUP_EVENT_INFO:
                v1 = inflater.inflate(R.layout.vh_info_msg, viewGroup, false);


                viewHolder = new VHInfoMessage(v1);
                break;

            case SCREEN_SHOT_TAKEN:
                v1 = inflater.inflate(R.layout.vh_screen_shot_taken, viewGroup, false);
                viewHolder = new VHScreenShotTaken(v1);
                break;
            default:
                v1 = inflater.inflate(R.layout.audio_sent_new, viewGroup, false);
                viewHolder = new VHAudioSent(v1);
                break;


        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        MyLog.d(TAG, "onBindViewHolder: " + viewHolder.getItemViewType());

        switch (viewHolder.getItemViewType()) {


            case MESSAGERECEIVED:
                VHMessageReceived vh2 = (VHMessageReceived) viewHolder;

                configureViewHolderMessageReceived(vh2, position);
                configureDateLabel(vh2.tvDateLbl, position);
                configureSecretTimerLabel(vh2.tvSecretLbl, position);

                break;

            case IMAGERECEIVED:
                VHImageReceived vh3 = (VHImageReceived) viewHolder;
                configureViewHolderImageReceived(vh3, position);
                configureDateLabel(vh3.tvDateLbl, position);
                configureSecretTimerLabel(vh3.tvSecretLbl, position);
                break;

            case VIDEORECEIVED:

                VHVideoReceived vh4 = (VHVideoReceived) viewHolder;
                configureViewHolderVideoReceived(vh4, position);
                configureDateLabel(vh4.tvDateLbl, position);
                configureSecretTimerLabel(vh4.tvSecretLbl, position);
                break;

            case LOCATIONRECEIVED:

                VHLocationReceived vh5 = (VHLocationReceived) viewHolder;
                configureViewHolderLocationReceived(vh5, position);
                configureDateLabel(vh5.tvDateLbl, position);
                configureSecretTimerLabel(vh5.tvSecretLbl, position);
                break;

            case CONTACTRECEIVED:

                VHContactReceived vh6 = (VHContactReceived) viewHolder;
                configureViewHolderContactReceived(vh6, position);
                configureDateLabel(vh6.tvDateLbl, position);
                configureSecretTimerLabel(vh6.tvSecretLbl, position);
                break;

            case AUDIORECEIVED:

                VHAudioReceived vh7 = (VHAudioReceived) viewHolder;
                configureViewHolderAudioReceived(vh7, position);
                configureDateLabel(vh7.tvDateLbl, position);
                configureSecretTimerLabel(vh7.tvSecretLbl, position);
                break;

            case DOCUMENT_RECEIVED:
                VHDocumentReceived vhDocumentReceived = (VHDocumentReceived) viewHolder;
                configureViewHolderDocumentReceived(vhDocumentReceived, position);
                configureDateLabel(vhDocumentReceived.tvDateLbl, position);
                configureSecretTimerLabel(vhDocumentReceived.tvSecretLbl, position);
                break;

            case WEB_LINK_RECEIVED:
                VHWebLinkReceived vhWebReceived = (VHWebLinkReceived) viewHolder;
                configureViewHolderWebLinkReceived(vhWebReceived, position);
                configureDateLabel(vhWebReceived.tvDateLbl, position);
                configureSecretTimerLabel(vhWebReceived.tvSecretLbl, position);
                break;


            case MESSAGESENT:
                VHMessageSent vh8 = (VHMessageSent) viewHolder;
                configureViewHolderMessageSent(vh8, position);
                configureDateLabel(vh8.tvDateLbl, position);
                configureSecretTimerLabel(vh8.tvSecretLbl, position);
                break;

            case IMAGESENT:

                VHImageSent vh9 = (VHImageSent) viewHolder;
                configureViewHolderImageSent(vh9, position);
                configureDateLabel(vh9.tvDateLbl, position);
                configureSecretTimerLabel(vh9.tvSecretLbl, position);
                break;

            case VIDEOSENT:

                VHVideoSent vh10 = (VHVideoSent) viewHolder;
                configureViewHolderVideoSent(vh10, position);
                configureDateLabel(vh10.tvDateLbl, position);
                configureSecretTimerLabel(vh10.tvSecretLbl, position);
                break;

            case LOCATIONSENT:
                VHLocationSent vh11 = (VHLocationSent) viewHolder;
                configureViewHolderLocationSent(vh11, position);
                configureDateLabel(vh11.tvDateLbl, position);
                configureSecretTimerLabel(vh11.tvSecretLbl, position);
                break;


            case CONTACTSENT:
                VHContactSent vh12 = (VHContactSent) viewHolder;
                configureViewHolderContactSent(vh12, position);
                configureDateLabel(vh12.tvDateLbl, position);
                configureSecretTimerLabel(vh12.tvSecretLbl, position);
                break;

            case ServerMessAGE:

                VHservermessage vh14 = (VHservermessage) viewHolder;
                configureSecretTimerLabel(vh14.tvServerMsgLbl, position);
                configureDateLabel(vh14.tvDateLbl, position);
//                vh14.tvServerMsgLbl.setVisibility(View.VISIBLE);
                break;

            case WEB_LINK_SENT:
                VHWebLinkSent vhWebSent = (VHWebLinkSent) viewHolder;
                configureViewHolderWebLinkSent(vhWebSent, position);
                configureDateLabel(vhWebSent.tvDateLbl, position);
                configureSecretTimerLabel(vhWebSent.tvSecretLbl, position);
                break;

            case DOCUMENT_SENT:
                VHDocumentSent vhDocumentSent = (VHDocumentSent) viewHolder;
                configureViewHolderDocumentSent(vhDocumentSent, position);
                configureDateLabel(vhDocumentSent.tvDateLbl, position);
                configureSecretTimerLabel(vhDocumentSent.tvSecretLbl, position);
                break;

            case GROUP_EVENT_INFO:
                VHInfoMessage vhInfo = (VHInfoMessage) viewHolder;
                configureViewHolderInfoMessage(vhInfo, position);
                configureDateLabel(vhInfo.tvDateLbl, position);
                break;

            case SCREEN_SHOT_TAKEN:
                VHScreenShotTaken vhScreenShotTaken = (VHScreenShotTaken) viewHolder;
                configureScreenShotTaken(vhScreenShotTaken.tv_screen_shot_taken, position);
                break;

            default:
                VHAudioSent vh13 = (VHAudioSent) viewHolder;
                configureViewHolderAudioSent(vh13, position);
                configureDateLabel(vh13.tvDateLbl, position);
                configureSecretTimerLabel(vh13.tvSecretLbl, position);
                break;


        }
    }

    private void configureViewHolderInfoMessage(VHInfoMessage vh2, int position) {
        final MessageItemChat message = mListData.get(position);


        vh2.tvInfoMsg.setVisibility(View.VISIBLE);

        String createdBy = message.getCreatedByUserId();
        String createdTo = message.getCreatedToUserId();
        String groupName = message.getGroupName();

        String createdByName, createdToName;
        String msg = null;

        switch (message.getGroupEventType()) {

            case "" + MessageFactory.join_new_group:
                if (createdBy.equalsIgnoreCase(mCurrentUserId)) {
                    createdByName = "You";
                } else {
                    createdByName = getContactNameIfExists(createdBy);
                }
                msg = createdByName + " created group '" + groupName + "'";
                break;

            case "" + MessageFactory.add_group_member:
                if (createdBy.equalsIgnoreCase(mCurrentUserId)) {
                    createdByName = "You";
                } else {
                    createdByName = getContactNameIfExists(createdBy);
                }
                if (createdTo.equalsIgnoreCase(mCurrentUserId)) {
                    createdToName = "You";
                } else {
                    createdToName = getContactNameIfExists(createdTo);
                }
                msg = createdByName + " added " + createdToName;
                break;

            case "" + MessageFactory.change_group_icon:
                if (createdBy.equalsIgnoreCase(mCurrentUserId)) {
                    createdByName = "You";
                } else {
                    createdByName = getContactNameIfExists(createdBy);
                }

                msg = createdByName + " changed group's icon";
                break;

            case "" + MessageFactory.change_group_name:
                if (createdBy.equalsIgnoreCase(mCurrentUserId)) {
                    createdByName = "You";
                } else {
                    createdByName = getContactNameIfExists(createdBy);
                }

                msg = createdByName + " changed group's name '" + message.getPrevGroupName() + "' to '"
                        + message.getGroupName() + "'";
                break;

            case "" + MessageFactory.delete_member_by_admin:
                if (createdBy.equalsIgnoreCase(mCurrentUserId)) {
                    createdByName = "You";
                } else {
                    createdByName = getContactNameIfExists(createdBy);
                }

                if (createdTo.equalsIgnoreCase(mCurrentUserId)) {
                    createdToName = "You";
                } else {
                    createdToName = getContactNameIfExists(createdTo);
                }
                msg = createdByName + " removed " + createdToName;
                break;

            case "" + MessageFactory.make_admin_member:
                if (createdTo.equalsIgnoreCase(mCurrentUserId)) {
                    createdToName = "You are ";
                } else {
                    createdToName = getContactNameIfExists(createdTo);
                }
                msg = createdToName + " now admin";
                break;

            case "" + MessageFactory.exit_group:
                if (createdBy.equalsIgnoreCase(mCurrentUserId)) {
                    createdByName = "You ";
                } else {
                    createdByName = getContactNameIfExists(createdBy);
                }
                msg = createdByName + " left";
                break;

        }

        if (msg != null) {
            vh2.tvInfoMsg.setText(msg);
        }

    }

    private String getContactNameIfExists(String userId) {
        String userName = null;

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
        SmarttyContactModel info = contactDB_sqlite.getUserOpponenetDetails(userId);

        if (info != null) {
            userName = getcontactname.getSendername(userId, info.getMsisdn());
        }
        return userName;
    }

    private void configureViewHolderDocumentSent(VHDocumentSent vhDocumentSent, int position) {
        // vhDocumentSent.relative_layout_message.setBackgroundResource(R.drawable.secret_msg_sent_bg);

        final MessageItemChat message = mListData.get(position);

        if (message != null) {
            vhDocumentSent.pbUpload.setVisibility(View.GONE);
            if (message.isSelected())
                vhDocumentSent.selection_layout.setBackgroundColor(selectedItemColor);
                // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
            else
                vhDocumentSent.selection_layout.setBackgroundColor(unSelectedItemColor);

            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                vhDocumentSent.starred.setVisibility(View.VISIBLE);
            } else {
                vhDocumentSent.starred.setVisibility(View.GONE);
            }

            // vh2.senderName.setText(message.getSenderName());
            String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            mydate = mydate.replace(".", "");
            vhDocumentSent.time.setText(mydate);
            try {
                vhDocumentSent.message.setText(Html.fromHtml("<u>" + message.getTextMessage() + "</u>"));
            } catch (Exception e) {
                MyLog.e(TAG, "configureViewHolderDocumentSent: ", e);
            }

            if ((message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_UPLOADING &&
                    message.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_NOT_SENT))
                    || message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_DOWNLOADING) {
                vhDocumentSent.pbUpload.setVisibility(View.VISIBLE);
            } else {
                vhDocumentSent.pbUpload.setVisibility(View.GONE);
            }

            if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START) {
                vhDocumentSent.iBtnDownload.setVisibility(View.VISIBLE);
            } else {
                vhDocumentSent.iBtnDownload.setVisibility(View.GONE);
            }

            //vhDocumentSent.imageViewindicatior.setImageResource(R.drawable.ic_secret_sent_corner);
            if (position == 0) {
                if (message.isInfoMsg() == false && message.isSelf()) {
                    vhDocumentSent.imageViewindicatior.setVisibility(View.VISIBLE);
                }
            } else {

                MessageItemChat prevmsg = mListData.get(position - 1);
                if (message.isSelf() == !prevmsg.isSelf() || prevmsg.isDate()) {
                    vhDocumentSent.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vhDocumentSent.imageViewindicatior.setVisibility(View.GONE);
                }

            }

            String status = message.getDeliveryStatus();
          /*  if (status == null) {
                status = MessageFactory.DELIVERY_STATUS_SENT;
            }*/
            switch (status) {
                case "3":
                    vhDocumentSent.clock.setVisibility(View.GONE);

                    vhDocumentSent.ivTick.setImageResource(R.drawable.message_deliver_tick);
                    vhDocumentSent.ivTick.setVisibility(View.VISIBLE);
                    break;
                case "2":
                    vhDocumentSent.clock.setVisibility(View.GONE);
                    vhDocumentSent.ivTick.setImageResource(R.mipmap.ic_double_tick);
                    vhDocumentSent.ivTick.setVisibility(View.VISIBLE);

                    break;
                case "1":

                    vhDocumentSent.clock.setVisibility(View.GONE);
                    vhDocumentSent.ivTick.setVisibility(View.VISIBLE);
                    vhDocumentSent.ivTick.setImageResource(R.mipmap.ic_single_tick);

                    break;
                default:
                    vhDocumentSent.clock.setVisibility(View.VISIBLE);
                    vhDocumentSent.ivTick.setVisibility(View.GONE);
                    break;
            }

        }
    }

    private void configureViewHolderDocumentReceived(final VHDocumentReceived vhDocumentReceived, int position) {
        final MessageItemChat message = mListData.get(position);

        vhDocumentReceived.iBtnDownload.setVisibility(View.GONE);
        vhDocumentReceived.Pbdownload.setVisibility(View.GONE);
        vhDocumentReceived.ivDoc.setImageResource(R.drawable.document);
        if (message.isSelected())
            vhDocumentReceived.selection_layout.setBackgroundColor(selectedItemColor);
            // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
        else
            vhDocumentReceived.selection_layout.setBackgroundColor(unSelectedItemColor);

        if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED))
            vhDocumentReceived.starred.setVisibility(View.VISIBLE);
        else
            vhDocumentReceived.starred.setVisibility(View.GONE);
        if (position == 0) {
            if (!message.isInfoMsg() && !message.isSelf()) {
                vhDocumentReceived.imageViewindicatior.setVisibility(View.VISIBLE);
            } else {
                vhDocumentReceived.imageViewindicatior.setVisibility(View.GONE);
            }
        } else {
            MessageItemChat prevmsg = mListData.get(position - 1);
            if ((!message.isSelf() == prevmsg.isSelf()) || prevmsg.isDate()) {
                vhDocumentReceived.imageViewindicatior.setVisibility(View.VISIBLE);
            } else {
                vhDocumentReceived.imageViewindicatior.setVisibility(View.GONE);
            }

        }
        if (message != null) {
            countcheck++;
            MyLog.d("SecretchatlistName", message.getTextMessage());
            try {
                vhDocumentReceived.message.setText(Html.fromHtml("<u>" + message.getTextMessage() + "</u>"));

            } catch (Exception e) {
                MyLog.e(TAG, "Error", e);
            }


            String mess = message.getTextMessage();
            String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            mydate = mydate.replace(".", "");

            vhDocumentReceived.time.setText(mydate);

            vhDocumentReceived.relative_layout_message.setVisibility(View.VISIBLE);
            vhDocumentReceived.tvInfoMsg.setVisibility(View.GONE);
            if (message.isInfoMsg()) {
                vhDocumentReceived.relative_layout_message.setVisibility(View.GONE);
                vhDocumentReceived.tvInfoMsg.setVisibility(View.VISIBLE);

                vhDocumentReceived.tvInfoMsg.post(new Runnable() {
                    @Override
                    public void run() {
                        vhDocumentReceived.tvInfoMsg.setText(message.getTextMessage());
                    }
                });
            }

            if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START) {
                vhDocumentReceived.iBtnDownload.setVisibility(View.VISIBLE);
                vhDocumentReceived.Pbdownload.setVisibility(View.GONE);
                vhDocumentReceived.iBtnDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        vhDocumentReceived.iBtnDownload.setVisibility(View.GONE);
                        vhDocumentReceived.Pbdownload.setVisibility(View.VISIBLE);
                    }
                });

            } else {
                vhDocumentReceived.iBtnDownload.setVisibility(View.GONE);
                vhDocumentReceived.Pbdownload.setVisibility(View.GONE);
            }
        }
    }

    private void configureViewHolderWebLinkSent(final VHWebLinkSent vhWebSent, int position) {
        //  vhWebSent.relative_layout_message.setBackgroundResource(R.drawable.secret_msg_sent_bg);

        final MessageItemChat message = mListData.get(position);

        if (message != null) {

            if (message.isSelected())
                vhWebSent.selection_layout.setBackgroundColor(selectedItemColor);
                // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
            else
                vhWebSent.selection_layout.setBackgroundColor(unSelectedItemColor);

            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                vhWebSent.starred.setVisibility(View.VISIBLE);
            } else {
                vhWebSent.starred.setVisibility(View.GONE);
            }

            //  vhWebSent.imageViewindicatior.setImageResource(R.drawable.ic_secret_sent_corner);

            if (position == 0) {
                if (message.isInfoMsg() == false && message.isSelf()) {
                    vhWebSent.imageViewindicatior.setVisibility(View.VISIBLE);
                }
            } else {

                MessageItemChat prevmsg = mListData.get(position - 1);
                if (message.isSelf() == !prevmsg.isSelf() || prevmsg.isDate()) {
                    vhWebSent.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vhWebSent.imageViewindicatior.setVisibility(View.GONE);
                }

            }
            // vh2.senderName.setText(message.getSenderName());
            String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            mydate = mydate.replace(".", "");
            vhWebSent.time.setText(mydate);
            try {
                vhWebSent.message.post(new Runnable() {
                    @Override
                    public void run() {
                        vhWebSent.message.setText(message.getTextMessage());
                    }
                });
            } catch (Exception e) {
                MyLog.e(TAG, "configureViewHolderWebLinkSent: ", e);
            }

            //    vhWebSent.imageViewindicatior.setImageResource(R.drawable.ic_secret_sent_corner);
            if (position == 0) {
                if (message.isInfoMsg() == false && message.isSelf()) {
                    vhWebSent.imageViewindicatior.setVisibility(View.VISIBLE);
                }
            } else {

                MessageItemChat prevmsg = mListData.get(position - 1);
                if (message.isSelf() == !prevmsg.isSelf() || prevmsg.isDate()) {
                    vhWebSent.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vhWebSent.imageViewindicatior.setVisibility(View.GONE);
                }

            }
            vhWebSent.tvWebLink.setText(message.getWebLink());
//            vhWebSent.tvWebLinkDesc.setText(message.getWebLinkDesc());
            vhWebSent.tvWebTitle.setText(message.getWebLinkTitle());
            /*if (message.getWebLinkImgUrl() != null && message.getWebLinkImgUrl().equals("")) {
                vhWebSent.ivWebLink.setVisibility(View.VISIBLE);
                Picasso.with(mContext).load(message.getWebLinkImgUrl()).into(vhWebSent.ivWebLink);
            } else {
                vhWebSent.ivWebLink.setVisibility(View.GONE);
                vhWebSent.ivWebLink.setBackground(null);
            }*/

            String linkImgUrl = message.getWebLink() + "/favicon.ico";
            Picasso.with(mContext).load(linkImgUrl).into(vhWebSent.ivWebLink);


            String status = message.getDeliveryStatus();
          /*  if (status == null) {
                status = MessageFactory.DELIVERY_STATUS_SENT;
            }*/
            switch (status) {
                case "3":
                    vhWebSent.clock.setVisibility(View.GONE);
                    vhWebSent.ivTick.setImageResource(R.drawable.message_deliver_tick);
                    vhWebSent.ivTick.setVisibility(View.VISIBLE);
                    break;
                case "2":
                    vhWebSent.clock.setVisibility(View.GONE);
                    vhWebSent.ivTick.setImageResource(R.mipmap.ic_double_tick);
                    vhWebSent.ivTick.setVisibility(View.VISIBLE);

                    break;
                case "1":
                    vhWebSent.clock.setVisibility(View.GONE);
                    vhWebSent.ivTick.setVisibility(View.VISIBLE);
                    vhWebSent.ivTick.setImageResource(R.mipmap.ic_single_tick);

                    break;
                default:
                    vhWebSent.clock.setVisibility(View.VISIBLE);
                    vhWebSent.ivTick.setVisibility(View.GONE);
                    break;
            }

        }
    }

    private void configureViewHolderWebLinkReceived(final VHWebLinkReceived vhWebReceived, int position) {
        final MessageItemChat message = mListData.get(position);

        if (message.isSelected())
            vhWebReceived.selection_layout.setBackgroundColor(selectedItemColor);
            // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
        else
            vhWebReceived.selection_layout.setBackgroundColor(unSelectedItemColor);

        if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED))
            vhWebReceived.starred.setVisibility(View.VISIBLE);
        else
            vhWebReceived.starred.setVisibility(View.GONE);

        if (message != null) {

            String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            mydate = mydate.replace(".", "");
            vhWebReceived.time.setText(mydate);
            try {
                vhWebReceived.message.post(new Runnable() {
                    @Override
                    public void run() {
                        vhWebReceived.message.setText(message.getTextMessage());
                    }
                });

            } catch (Exception e) {
                MyLog.e(TAG, "configureViewHolderWebLinkReceived: ", e);
            }
            if (position == 0) {
                if (!message.isInfoMsg() && !message.isSelf()) {
                    vhWebReceived.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vhWebReceived.imageViewindicatior.setVisibility(View.GONE);
                }
            } else {
                MessageItemChat prevmsg = mListData.get(position - 1);
                if ((!message.isSelf() == prevmsg.isSelf()) || prevmsg.isDate()) {
                    vhWebReceived.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vhWebReceived.imageViewindicatior.setVisibility(View.GONE);
                }

            }
            vhWebReceived.relative_layout_message.setVisibility(View.VISIBLE);
            vhWebReceived.tvInfoMsg.setVisibility(View.GONE);
            if (message.isInfoMsg()) {
                vhWebReceived.relative_layout_message.setVisibility(View.GONE);
                vhWebReceived.tvInfoMsg.setVisibility(View.VISIBLE);

                vhWebReceived.tvInfoMsg.post(new Runnable() {
                    @Override
                    public void run() {
                        vhWebReceived.tvInfoMsg.setText(message.getTextMessage());

                    }
                });
            }

            vhWebReceived.tvWebLink.setText(message.getWebLink());
            vhWebReceived.tvWebTitle.setText(message.getWebLinkTitle());
//            vhWebReceived.tvWebLinkDesc.setText(message.getWebLinkDesc());
            /*if (message.getWebLinkImgUrl() != null && !message.getWebLinkImgUrl().equals("")) {
                vhWebReceived.ivWebLink.setVisibility(View.VISIBLE);
                Picasso.with(mContext).load(message.getWebLinkImgUrl()).into(vhWebReceived.ivWebLink);
            } else {
                vhWebReceived.ivWebLink.setVisibility(View.GONE);
                vhWebReceived.ivWebLink.setBackground(null);
            }*/
            String linkImgUrl = message.getWebLink() + "/favicon.ico";
            Picasso.with(mContext).load(linkImgUrl).into(vhWebReceived.ivWebLink);
        }
    }

    private void configureViewHolderMessageReceived(final VHMessageReceived vh2, final int position) {
        final MessageItemChat message = mListData.get(position);

        vh2.replaylayout_recevied.setVisibility(View.GONE);
        vh2.cameraphoto.setVisibility(View.GONE);
        vh2.time.setVisibility(View.GONE);
        vh2.ts_below.setVisibility(View.GONE);
        vh2.starred.setVisibility(View.GONE);
        vh2.starredindicator_below.setVisibility(View.GONE);
        vh2.time.setVisibility(View.GONE);
        vh2.relpaymessage_recevied.setVisibility(View.GONE);
        vh2.relpaymessage_receviedmedio.setVisibility(View.GONE);
        if (mListData.get(position).getReplyMessage() != null && !mListData.get(position).getReplyMessage().equals("")) {
            vh2.replaylayout_recevied.setVisibility(View.VISIBLE);
            vh2.relpaymessage_recevied.setVisibility(View.VISIBLE);
            vh2.relpaymessage_recevied.post(new Runnable() {
                @Override
                public void run() {
                    vh2.relpaymessage_recevied.setText(message.getReplyMessage());
                }
            });
            vh2.lblMsgFrom2.setText(mListData.get(position).getReplySenser());
            if (message.getReplyType() != null && !message.getReplyType().equals("")) {
                if (Integer.parseInt(message.getReplyType()) == MessageFactory.document) {
                    vh2.relpaymessage_recevied.setVisibility(View.GONE);
                    vh2.replaylayout_recevied.setVisibility(View.VISIBLE);
                    vh2.cameraphoto.setVisibility(View.VISIBLE);
                    vh2.cameraphoto.setImageResource(R.drawable.ic_document);
                    vh2.relpaymessage_receviedmedio.setVisibility(View.VISIBLE);
                    vh2.relpaymessage_receviedmedio.setText(message.getReplyMessage());
                    vh2.lblMsgFrom2.setText(mListData.get(position).getReplySenser());
                }
                if (Integer.parseInt(message.getReplyType()) == MessageFactory.web_link) {
                    vh2.relpaymessage_recevied.setVisibility(View.VISIBLE);
                    vh2.replaylayout_recevied.setVisibility(View.VISIBLE);
                    vh2.relpaymessage_recevied.setText(message.getReplyMessage());
                    vh2.lblMsgFrom2.setText(mListData.get(position).getReplySenser());
                }
                if (Integer.parseInt(message.getReplyType()) == MessageFactory.contact) {
                    vh2.relpaymessage_recevied.setVisibility(View.GONE);
                    vh2.replaylayout_recevied.setVisibility(View.VISIBLE);
                    vh2.cameraphoto.setVisibility(View.VISIBLE);
                    vh2.cameraphoto.setImageResource(R.drawable.add_participant);
                    vh2.relpaymessage_receviedmedio.setVisibility(View.VISIBLE);
                    vh2.relpaymessage_receviedmedio.setText("Contact" + message.getReplyMessage());
                    vh2.lblMsgFrom2.setText(mListData.get(position).getReplySenser());
                }
            }
        }
        if (message.getreplyimagebase64() != null && !message.getreplyimagebase64().equals("")) {
            vh2.replaylayout_recevied.setVisibility(View.VISIBLE);
            vh2.relpaymessage_receviedmedio.setVisibility(View.VISIBLE);
            vh2.cameraphoto.setVisibility(View.VISIBLE);
            vh2.sentimage.setVisibility(View.VISIBLE);
            vh2.lblMsgFrom2.setText(mListData.get(position).getReplySenser());
            if (Integer.parseInt(message.getReplyType()) == MessageFactory.video) {
                vh2.relpaymessage_receviedmedio.setText("Video");
                vh2.cameraphoto.setImageResource(R.drawable.video_camera);
            } else if (Integer.parseInt(message.getReplyType()) == MessageFactory.picture) {
                vh2.relpaymessage_receviedmedio.setText("Photo");
                vh2.cameraphoto.setImageResource(R.mipmap.chat_attachment_camera_grey_icon_off);
            }
            final String imgpath = message.getreplyimgpath();
            vh2.sentimage.setImageBitmap(SmarttyImageUtils.decodeBitmapFromFile(imgpath, 50, 50));
            vh2.sentimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ImageZoom.class);
                    intent.putExtra("image", imgpath);
                    mContext.startActivity(intent);
                }
            });
        }
        if (message.getReplyType() != null && !message.getReplyType().equals("")) {
            if (Integer.parseInt(message.getReplyType()) == MessageFactory.audio) {
                vh2.replaylayout_recevied.setVisibility(View.VISIBLE);
                vh2.cameraphoto.setVisibility(View.VISIBLE);
                vh2.cameraphoto.setImageResource(R.drawable.audio);
                vh2.relpaymessage_receviedmedio.setVisibility(View.VISIBLE);
                vh2.relpaymessage_receviedmedio.setText("Audio");
                vh2.lblMsgFrom2.setText(mListData.get(position).getReplySenser());
            }
        }

        if (position == 0) {
            if (!message.isInfoMsg() && !message.isSelf()) {
                vh2.imageViewindicatior.setVisibility(View.VISIBLE);
            } else {
                vh2.imageViewindicatior.setVisibility(View.GONE);
            }
        } else {
            MessageItemChat prevmsg = mListData.get(position - 1);
            if ((!message.isSelf() == prevmsg.isSelf()) || prevmsg.isDate()) {
                vh2.imageViewindicatior.setVisibility(View.VISIBLE);
            } else {
                vh2.imageViewindicatior.setVisibility(View.GONE);
            }

        }

        if (message.isSelected())
            vh2.selection_layout.setBackgroundColor(selectedItemColor);
            // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
        else
            vh2.selection_layout.setBackgroundColor(unSelectedItemColor);
        String textmessage = message.getTextMessage();
        if (!textmessage.contains("\n") && textmessage.length() <= 20) {
            vh2.time.setVisibility(View.VISIBLE);
            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED))
                vh2.starredindicator_below.setVisibility(View.VISIBLE);
            else
                vh2.starredindicator_below.setVisibility(View.GONE);

            if (message != null) {

                String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
                mydate = mydate.replace(".", "");
                vh2.time.setText(mydate);

                try {
                    vh2.message.post(new Runnable() {
                        @Override
                        public void run() {
                            vh2.message.setText(message.getTextMessage());
                        }
                    });

                } catch (Exception e) {
                    MyLog.e(TAG, "configureViewHolderMessageReceived: ", e);
                }

                vh2.relative_layout_message.setVisibility(View.VISIBLE);
                vh2.tvInfoMsg.setVisibility(View.GONE);
                if (message.isInfoMsg()) {
                    vh2.relative_layout_message.setVisibility(View.GONE);
                    vh2.tvInfoMsg.setVisibility(View.VISIBLE);

                    vh2.tvInfoMsg.post(new Runnable() {
                        @Override
                        public void run() {
                            vh2.tvInfoMsg.setText(message.getTextMessage());
                        }
                    });

                }
            }
        } else {
            vh2.ts_below.setVisibility(View.VISIBLE);

            String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            mydate = mydate.replace(".", "");
            vh2.ts_below.setText(mydate);

            try {
                vh2.message.post(new Runnable() {
                    @Override
                    public void run() {
                        vh2.message.setText(message.getTextMessage());
                    }
                });

            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED))
                vh2.starred.setVisibility(View.VISIBLE);
            else
                vh2.starred.setVisibility(View.GONE);

            if (message != null) {

                // String mydate = convert24to12FromMsgId(message.getMessageId());
                // vh2.time.setText(mydate);
                //vh2.ts_below.setText(message.getTS());
                MyLog.d("position:  " + position, "msg: " + message.getTextMessage());
                try {
                    vh2.message.post(new Runnable() {
                        @Override
                        public void run() {
                            vh2.message.setText(message.getTextMessage());

                        }
                    });

                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }

                vh2.relative_layout_message.setVisibility(View.VISIBLE);
                vh2.tvInfoMsg.setVisibility(View.GONE);
                if (message.isInfoMsg()) {
                    vh2.relative_layout_message.setVisibility(View.GONE);
                    vh2.tvInfoMsg.setVisibility(View.VISIBLE);
                    vh2.tvInfoMsg.post(new Runnable() {
                        @Override
                        public void run() {
                            vh2.tvInfoMsg.setText(message.getTextMessage());

                        }
                    });
                }
            }

        }

        vh2.senderName.setVisibility(View.GONE);

    }

    public Bitmap ConvertToImage(String image) {
        byte[] imageAsBytes = Base64.decode(image.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

    private void configureViewHolderImageReceived(final VHImageReceived vh2, final int position) {

        final MessageItemChat message = mListData.get(position);
        configureDateLabel(vh2.time, position);
        String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());

        if (message != null) {

            if (message.getTextMessage() != null && !message.getTextMessage().equalsIgnoreCase("")) {
                vh2.captiontext.setVisibility(View.VISIBLE);
                vh2.rlTs.setVisibility(View.VISIBLE);
                vh2.abovecaption_layout.setVisibility(View.GONE);
                vh2.captiontext.post(new Runnable() {
                    @Override
                    public void run() {
                        vh2.captiontext.setText(message.getTextMessage());
                    }
                });
                if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                    vh2.starredindicator_below.setVisibility(View.VISIBLE);
                } else {
                    vh2.starredindicator_below.setVisibility(View.GONE);
                }
                vh2.time.setText(mydate);
            } else {
                vh2.rlTs.setVisibility(View.GONE);
                vh2.abovecaption_layout.setVisibility(View.VISIBLE);
                vh2.captiontext.setVisibility(View.GONE);
                if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                    vh2.starredindicator_above.setVisibility(View.VISIBLE);
                } else {
                    vh2.starredindicator_above.setVisibility(View.GONE);
                }
                vh2.ts_abovecaption.setText(mydate);
            }
            //  vh2.senderName.setText(message.getSenderName());


            if (position == 0) {
                if (!message.isInfoMsg() && !message.isSelf()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vh2.imageViewindicatior.setVisibility(View.GONE);
                }
            } else {
                MessageItemChat prevmsg = mListData.get(position - 1);
                if ((!message.isSelf() == prevmsg.isSelf()) || prevmsg.isDate()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vh2.imageViewindicatior.setVisibility(View.GONE);
                }

            }


            if (message.isSelected())
                vh2.selection_layout.setBackgroundColor(selectedItemColor);
                // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
            else
                vh2.selection_layout.setBackgroundColor(unSelectedItemColor);

            try {

                if (message.isSelf()) {

                } else {
                    final String image = message.getChatFileLocalPath();

                    if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                        vh2.download.setVisibility(View.GONE);
                        vh2.pbUpload.setVisibility(View.GONE);
                        vh2.forward_image.setVisibility(View.GONE);

                        File file = new File(image);
                        if (file.exists()) {
                            vh2.imageView.setImageBitmap(SmarttyImageUtils.decodeBitmapFromFile(image, 220, 200));
                            vh2.imageView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!FirstItemSelected) {
                                        Intent intent = new Intent(mContext, ImageZoom.class);
                                        intent.putExtra("image", image);
                                        mContext.startActivity(intent);
                                    }
                                }
                            });
                        } else {
                            String thumbnailData = message.getThumbnailData();
                            String imageDataBytes = thumbnailData.substring(thumbnailData.indexOf(",") + 1);
                            byte[] decodedString = Base64.decode(imageDataBytes, Base64.DEFAULT);
                            Bitmap thumbBmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            vh2.imageView.setImageBitmap(thumbBmp);
                            vh2.imageView.setOnClickListener(null);
                        }


                       /* vh2.imageView.setOnLongClickListener(new View.OnLongClickListener() {

                            @Override
                            public boolean onLongClick(View v) {
                                return true;
                            }
                        });
*/

                        vh2.forward_image.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                                selectedChatItems.add(message);

                                Bundle fwdBundle = new Bundle();
                                //    fwdBundle.putSerializable("MsgItemList", selectedChatItems);
                                fwdBundle.putBoolean("FromSmartty", true);
                                CommonData.setForwardedItems(selectedChatItems);

                                Intent intent = new Intent(mContext, ForwardContact.class);
                                intent.putExtras(fwdBundle);
                                mContext.startActivity(intent);
                            }
                        });
                    } else {
                        if (message.getUploadDownloadProgress() == 0) {
                            vh2.download.setVisibility(View.VISIBLE);
                            long filesize = AppUtils.parseLong(message.getFileSize());
                            vh2.imagesize.setText(size(filesize));
                            vh2.pbUpload.setVisibility(View.GONE);
                            vh2.imageView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    vh2.download.setVisibility(View.GONE);
                                    vh2.pbUpload.setVisibility(View.VISIBLE);
                                    vh2.pbUpload.setProgress(message.getUploadDownloadProgress());
                                }
                            });


                        } else {
                            vh2.download.setVisibility(View.GONE);
                            vh2.pbUpload.setVisibility(View.GONE);
                        }


                        try {
                            String thumbnailData = message.getThumbnailData();
                            String imageDataBytes = thumbnailData.substring(thumbnailData.indexOf(",") + 1);
                            byte[] decodedString = Base64.decode(imageDataBytes, Base64.DEFAULT);
                            Bitmap thumbBmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            vh2.imageView.setImageBitmap(thumbBmp);
                        } catch (Exception e) {
                            vh2.imageView.setImageBitmap(null);
                            MyLog.e(TAG, "", e);
                        }

                    }

                }

            } catch (OutOfMemoryError e) {
                MyLog.e(TAG, "", e);
            }
        }
        vh2.senderName.setVisibility(View.GONE);
    }

    private void configureViewHolderVideoReceived(final VHVideoReceived vh2, final int position) {
        final MessageItemChat message = mListData.get(position);
        if (message != null) {

            String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            String surationasec = "";
            //vh2.senderName.setText(message.getSenderName());

            if (message.getDuration() != null) {
                String duration = message.getDuration();
                surationasec = getTimeString(AppUtils.parseLong(duration));

            }
            if (message.getTextMessage() != null && !message.getTextMessage().equalsIgnoreCase("")) {
                vh2.video_belowlayout.setVisibility(View.VISIBLE);
                vh2.videoabove_layout.setVisibility(View.GONE);
                vh2.captiontext.setVisibility(View.VISIBLE);
                vh2.captiontext.post(new Runnable() {
                    @Override
                    public void run() {
                        vh2.captiontext.setText(message.getTextMessage());
                    }
                });
                vh2.time.setText(mydate);
                if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                    vh2.starredindicator_above.setVisibility(View.VISIBLE);
                } else {
                    vh2.starredindicator_above.setVisibility(View.GONE);
                }
            } else {
                vh2.video_belowlayout.setVisibility(View.GONE);
                vh2.videoabove_layout.setVisibility(View.VISIBLE);
                vh2.captiontext.setVisibility(View.GONE);
                vh2.duration_above.setText(surationasec);
                vh2.ts_abovecaption.setText(mydate);
                if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                    vh2.starredindicator_below.setVisibility(View.VISIBLE);
                } else {
                    vh2.starredindicator_below.setVisibility(View.GONE);
                }
            }


            if (position == 0) {
                if (!message.isInfoMsg() && !message.isSelf()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vh2.imageViewindicatior.setVisibility(View.GONE);
                }
            } else {
                MessageItemChat prevmsg = mListData.get(position - 1);
                if ((!message.isSelf() == prevmsg.isSelf()) || prevmsg.isDate()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vh2.imageViewindicatior.setVisibility(View.GONE);
                }

            }
            if (message.isSelected())
                vh2.selection_layout.setBackgroundColor(selectedItemColor);
                // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
            else
                vh2.selection_layout.setBackgroundColor(unSelectedItemColor);

            vh2.download.setVisibility(View.GONE);

            try {
                if (message.isSelf()) {

                    AppUtils.loadVideoThumbnail(mContext, message.getChatFileLocalPath(), vh2.thumbnail, 0.6f, 140);


                } else {
                    if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                        vh2.pbdownload.setVisibility(View.GONE);
                        /**
                         * image already downloaded
                         * */
                        AppUtils.loadVideoThumbnail(mContext, message.getChatFileLocalPath(), vh2.thumbnail, 0.6f, 140);

                        //vh2.imageView.setImageBitmap(bm);
                        //  }
                    } else if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START) {
                        vh2.download.setVisibility(View.VISIBLE);
                        long filesize = AppUtils.parseLong(message.getFileSize());
                        vh2.imagesize.setText(size(filesize));
                        vh2.overlay.setVisibility(View.GONE);
                        vh2.pbdownload.setVisibility(View.GONE);
                        vh2.download.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                vh2.download.setVisibility(View.GONE);
                                vh2.pbdownload.setVisibility(View.VISIBLE);

                            }
                        });


                    } else {
                        vh2.overlay.setVisibility(View.GONE);
                        vh2.download.setVisibility(View.GONE);
                        vh2.pbdownload.setVisibility(View.VISIBLE);
                    }

                    if (message.getThumbnailData() != null) {
                        vh2.thumbnail.post(new Runnable() {
                            @Override
                            public void run() {
                                String thumbData = message.getThumbnailData();
                                if (thumbData.startsWith("data:image/jpeg;base64,")) {
                                    thumbData = thumbData.replace("data:image/jpeg;base64,", "");
                                }
                                byte[] decodedString = Base64.decode(thumbData, Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                if (vh2.thumbnail != null)
                                    vh2.thumbnail.setImageBitmap(decodedByte);
                            }
                        });

                    }

                }
            } catch (Exception e) {
                MyLog.e(TAG, "configureViewHolderVideoReceived: ", e);
            }

        }


    }

    private void configureViewHolderLocationReceived(final VHLocationReceived vh2, final int position) {
        final MessageItemChat message = mListData.get(position);
        if (message != null) {

            if (message.isSelected())
                vh2.selection_layout.setBackgroundColor(selectedItemColor);
                // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
            else
                vh2.selection_layout.setBackgroundColor(unSelectedItemColor);

            vh2.time.setVisibility(View.VISIBLE);

            String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            mydate = mydate.replace(".", "");
            vh2.time.setText(mydate);
            if (position == 0) {
                if (!message.isInfoMsg() && !message.isSelf()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vh2.imageViewindicatior.setVisibility(View.GONE);
                }
            } else {
                MessageItemChat prevmsg = mListData.get(position - 1);
                if ((!message.isSelf() == prevmsg.isSelf()) || prevmsg.isDate()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vh2.imageViewindicatior.setVisibility(View.GONE);
                }

            }

            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                vh2.starredindicator_below.setVisibility(View.VISIBLE);
            } else {
                vh2.starredindicator_below.setVisibility(View.GONE);
            }


            String thumbData = message.getWebLinkImgThumb();
            if (thumbData != null) {
                if (thumbData.startsWith("data:image/jpeg;base64,")) {
                    thumbData = thumbData.replace("data:image/jpeg;base64,", "");
                }
                byte[] decodedString = Base64.decode(thumbData, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                vh2.ivMap.setImageBitmap(decodedByte);
            } else {
                vh2.ivMap.setImageBitmap(null);
            }

            if (message.getWebLinkImgUrl() != null) {
                ImageLoader imageLoader = CoreController.getInstance().getImageLoader();
                imageLoader.get(message.getWebLinkImgUrl(), new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        if (response.getBitmap() != null) {
                            vh2.ivMap.setImageBitmap(response.getBitmap());
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        vh2.ivMap.setImageBitmap(null);
                    }
                });
            } else {
                vh2.ivMap.setImageBitmap(null);
            }

        }
    }

    private void configureViewHolderContactReceived(VHContactReceived vh2, final int position) {
        final MessageItemChat message = mListData.get(position);
        final String contactName = message.getContactName();
        final String contactNumber = message.getContactNumber();
        final String contactAvatar = message.getAvatarImageUrl();
        final String contactDetails = message.getDetailedContacts();
        final String ContactSmarttyId = message.getContactSmarttyId();
        Boolean isSmarttycontact = false;
        vh2.contactimage.setImageResource(R.mipmap.contact_secret);
        vh2.add.setVisibility(View.GONE);
        vh2.invite.setVisibility(View.GONE);

        if (message != null) {
            if (message.isSelected()) {
                vh2.selection_layout.setBackgroundColor(selectedItemColor);
                // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
            } else {
                vh2.selection_layout.setBackgroundColor(unSelectedItemColor);
            }
            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                vh2.starredindicator_below.setVisibility(View.VISIBLE);
            } else {
                vh2.starredindicator_below.setVisibility(View.GONE);
            }

            vh2.senderName.setVisibility(View.GONE);

            if (position == 0) {
                if (!message.isInfoMsg() && !message.isSelf()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vh2.imageViewindicatior.setVisibility(View.GONE);
                }
            } else if (mListData.get(position - 1).isInfoMsg() && !message.isSelf() && !message.isInfoMsg()) {
                vh2.imageViewindicatior.setVisibility(View.VISIBLE);
            } else if (message.isInfoMsg() && mListData.get(position - 1).isInfoMsg()) {
                vh2.imageViewindicatior.setVisibility(View.GONE);
            } else {
                MessageItemChat prevmsg = mListData.get(position - 1);
                if ((!message.isSelf() == prevmsg.isSelf()) && !message.isInfoMsg()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vh2.imageViewindicatior.setVisibility(View.GONE);
                }

            }


            String ts = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            ts = ts.replace(".", "");
            vh2.time.setText(ts);


            if (contactAvatar != null && !contactAvatar.equals("")) {
                Picasso.with(mContext).load(contactAvatar).error(
                        R.mipmap.contact_secret)
//                        .transform(new CircleTransform())
                        .into(vh2.contactimage);
            } else {
                vh2.contactimage.setImageResource(R.mipmap.contact_secret);
            }


            if (contactNumber != null) {
                if (contactNumber.equals("")) {
                    vh2.add.setVisibility(View.GONE);
                    vh2.contact_add_invite.setVisibility(View.GONE);
                    vh2.v1.setVisibility(View.GONE);
                    vh2.invite.setVisibility(View.GONE);
                    vh2.message1.setVisibility(View.GONE);

                } else {
                    if (ContactSmarttyId != null && !ContactSmarttyId.equals("")) {
                        if (!ContactSmarttyId.equalsIgnoreCase(mCurrentUserId)) {
                            vh2.add.setVisibility(View.GONE);
                            vh2.add1.setVisibility(View.GONE);
                            vh2.invite1.setVisibility(View.GONE);
                            vh2.invite.setVisibility(View.GONE);
                            vh2.message1.setVisibility(View.VISIBLE);
                            vh2.v1.setVisibility(View.VISIBLE);
                        } else {
                            vh2.add.setVisibility(View.GONE);
                            vh2.add1.setVisibility(View.GONE);
                            vh2.invite1.setVisibility(View.GONE);
                            vh2.invite.setVisibility(View.GONE);
                            vh2.message1.setVisibility(View.GONE);
                            vh2.v1.setVisibility(View.GONE);
                        }
                    } else {
                        vh2.add.setVisibility(View.GONE);
                        vh2.add1.setVisibility(View.GONE);
                        vh2.invite1.setVisibility(View.GONE);
                        vh2.invite.setVisibility(View.VISIBLE);
                        vh2.v1.setVisibility(View.VISIBLE);
                        vh2.message1.setVisibility(View.GONE);
                    }

                }

            }

            vh2.invite1.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    performUserInvite(contactName, contactDetails);
                    return false;
                }
            });
            vh2.add1.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    Intent intent = new Intent(mContext, Savecontact.class);
                    intent.putExtra("name", contactName);
                    intent.putExtra("number", contactNumber);
                    mContext.startActivity(intent);

                    return false;
                }
            });
            vh2.contactName.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (ContactSmarttyId == null || !ContactSmarttyId.equalsIgnoreCase(mCurrentUserId)) {
                        Intent intent = new Intent(mContext, Savecontact.class);
                        intent.putExtra("name", contactName);
                        intent.putExtra("number", contactNumber);
                        intent.putExtra("contactList", contactDetails);
                        mContext.startActivity(intent);
                    }
                    return false;
                }
            });
            vh2.message1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checKForChatViewNavigation(contactNumber, contactName, contactAvatar, ContactSmarttyId);
                }
            });
            vh2.invite.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    performUserInvite(contactName, contactDetails);
                    return false;
                }
            });
            vh2.add.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    Intent intent = new Intent(mContext, Savecontact.class);
                    intent.putExtra("name", contactName);
                    intent.putExtra("number", contactNumber);
                    mContext.startActivity(intent);

                    return false;
                }
            });

            try {
                vh2.contactName.setText(contactName);
                vh2.contactNumber.setText(contactNumber);
            } catch (OutOfMemoryError e) {
                MyLog.e(TAG, "", e);
            }
            if (contactNumber == null || contactNumber.isEmpty()) {
                vh2.contactNumber.setText("No Number");
            }


        }
    }

    private void configureViewHolderAudioReceived(final VHAudioReceived vh2, final int position) {
        final MessageItemChat message = mListData.get(position);

        if (message != null) {
            // configureDateLabel(vh2.time, position);
            vh2.sbDuration.setProgressDrawable(ContextCompat.getDrawable(mContext, R.drawable.seekber_progress_secret));
            vh2.sbDuration.setThumb(ContextCompat.getDrawable(mContext, R.drawable.blueseekbarthum_secret));
            String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            mydate = mydate.replace(".", "");
            vh2.time_ts.setText(mydate);

            if (position == 0) {
                if (!message.isInfoMsg() && !message.isSelf()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vh2.imageViewindicatior.setVisibility(View.GONE);
                }
            } else {
                MessageItemChat prevmsg = mListData.get(position - 1);
                if ((!message.isSelf() == prevmsg.isSelf()) || prevmsg.isDate()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vh2.imageViewindicatior.setVisibility(View.GONE);
                }

            }

            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                vh2.starredindicator.setVisibility(View.VISIBLE);
            } else {
                vh2.starredindicator.setVisibility(View.GONE);
            }

            if (message.isSelected())
                vh2.selection_layout.setBackgroundColor(selectedItemColor);
                // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
            else
                vh2.selection_layout.setBackgroundColor(unSelectedItemColor);

            if (message.getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                vh2.audiotrack_layout_back.setVisibility(View.GONE);
                vh2.record_image.setVisibility(View.VISIBLE);
                vh2.record_icon.setVisibility(View.VISIBLE);
                vh2.record_icon.setImageResource(R.drawable.record_usericon_secret);
                vh2.recodingduration.setVisibility(View.VISIBLE);
                if (message.getDuration() != null) {
                    // String duration = message.getDuration();
                    // String surationasec = getTimeString(AppUtils.parseLong(duration));
                    vh2.recodingduration.setText(message.getDuration());
                }
                String[] array = message.getMessageId().split("-");

                getcontactname.configProfilepic(vh2.record_image, array[1], false, true, R.mipmap.chat_attachment_profile_default_image_frame);
            } else {
                vh2.audiotrack_layout_back.setVisibility(View.VISIBLE);
                vh2.audiotrack_layout_back.setBackgroundResource(R.drawable.secretbuttn);
                vh2.record_image.setVisibility(View.GONE);
                vh2.record_icon.setVisibility(View.GONE);
                vh2.recodingduration.setVisibility(View.GONE);
                String duration = message.getDuration();
                if (duration != null && !duration.equalsIgnoreCase("")) {
//                    String surationasec = getTimeString(AppUtils.parseLong(duration));
                    vh2.duration.setText(duration);
                }
            }

            try {
                if (message.isSelf()) {

                } else {
                    vh2.sbDuration.setProgress(message.getPlayerCurrentPosition());

                    if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                        final Uri uri = Uri.parse(message.getChatFileLocalPath());

                        // Remove user dragging in seekbar
                        vh2.download.setVisibility(View.GONE);
                        vh2.pbdownload.setVisibility(View.GONE);
                        vh2.playButton.setVisibility(View.VISIBLE);

                        if (message.isMediaPlaying()) {
                            long value = message.getPlayerCurrentPosition() * 1000;
                            if (message.getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                                vh2.recodingduration.setText(getTimeString(value));
                            } else {
                                vh2.duration.setText(getTimeString(value));
                            }
                            vh2.playButton.setBackgroundResource(R.drawable.ic_pause_secret);
                        } else {
                            vh2.playButton.setBackgroundResource(R.drawable.ic_secret_play);
                            if (message.getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                                vh2.recodingduration.setText(message.getDuration());
                            } else {
                                vh2.duration.setText(message.getDuration());
                            }
                        }


                        vh2.playButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (lastPlayedAt > -1) {
                                    mListData.get(lastPlayedAt).setIsMediaPlaying(false);
                                    mTimer.cancel();
                                    mPlayer.release();
                                }

                                if (lastPlayedAt != position) {
                                    playAudio(position, message, vh2.sbDuration);
                                } else {
                                    lastPlayedAt = -1;
                                }

                                notifyDataSetChanged();
                            }
                        });

                        vh2.sbDuration.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {
                                // TODO Auto-generated method stub
                                if (mListData.size() > position && mListData.get(position).isMediaPlaying()) {
                                    mTimer.cancel();
                                    mPlayer.release();
                                    playAudio(position, message, vh2.sbDuration);
                                }
                                try {
                                    notifyDataSetChanged();
                                } catch (Exception e) {
                                    MyLog.e(TAG, "", e);
                                }
                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                // TODO Auto-generated method stub
                                try {
                                    if (position != RecyclerView.NO_POSITION && mListData.size() > position && fromUser) {
                                        mListData.get(position).setPlayerCurrentPosition(progress);
                                    }
                                } catch (Exception e) {

                                }
                            }
                        });


                    } else {

                        if (message.getUploadDownloadProgress() == 0) {
                            vh2.download.setVisibility(View.VISIBLE);
                            vh2.playButton.setVisibility(View.GONE);
                            vh2.pbdownload.setVisibility(View.GONE);
                            vh2.download.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    vh2.download.setVisibility(View.GONE);
                                    vh2.pbdownload.setVisibility(View.VISIBLE);

                                }
                            });


                        } else {
                            vh2.download.setVisibility(View.GONE);
                            vh2.pbdownload.setVisibility(View.GONE);
                            vh2.playButton.setVisibility(View.VISIBLE);
                        }
                     /*   vh2.download.setVisibility(View.GONE);
                        vh2.playButton.setVisibility(View.GONE);
                        vh2.pbDownload.setVisibility(View.GONE);*/

                       /* if (message.getUploadDownloadProgress() == 0) {
                            vh2.download.setVisibility(View.VISIBLE);
                        } else if (message.getUploadDownloadProgress() > 0 &&
                                message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_UPLOADING) {
                            vh2.pbDownload.setVisibility(View.VISIBLE);
                            vh2.pbDownload.setProgress(message.getUploadDownloadProgress());
                        }*/


                    }


                }
            } catch (OutOfMemoryError e) {
                MyLog.e(TAG, "", e);
            }

        }
    }

    private void configureViewHolderMessageSent(final VHMessageSent vh2, final int position) {
        final MessageItemChat message = mListData.get(position);
        //  vh2.imageViewindicatior.setImageResource(R.drawable.ic_secret_sent_corner);
        //  vh2.relative_layout_message.setBackgroundResource(R.drawable.secret_msg_sent_bg);
        vh2.replaylayout.setVisibility(View.GONE);
        vh2.cameraphoto.setVisibility(View.GONE);
        vh2.sentimage.setVisibility(View.GONE);
        vh2.replaymessage.setVisibility(View.GONE);
        vh2.replaymessagemedio.setVisibility(View.GONE);
        vh2.clock.setVisibility(View.GONE);
//        vh2.singleTick.setVisibility(View.GONE);
//        vh2.doubleTickGreen.setVisibility(View.GONE);
        vh2.doubleTickBlue.setVisibility(View.GONE);
        vh2.time.setVisibility(View.GONE);
        vh2.timebelow.setVisibility(View.GONE);
        vh2.starredbelow.setVisibility(View.GONE);
        vh2.clockbelow.setVisibility(View.GONE);
        vh2.ivTickBelow.setVisibility(View.GONE);
        vh2.layout_reply_tsalign_above.setVisibility(View.GONE);
        vh2.layout_reply_tsalign.setVisibility(View.GONE);
        vh2.message_sent_singleChar_layout.setVisibility(View.GONE);


        if (message.getTextMessage().contains("\n")) {
            String[] aStrMsgArr = message.getTextMessage().split("\n");
            //System.out.println("----length-----" + aStrMsgArr.length);

            if (aStrMsgArr[0].length() < 10) {
                vh2.below_layout.setVisibility(View.GONE);
                vh2.message_sent_singleChar_layout.setVisibility(View.VISIBLE);
            }
            String ts = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            ts = ts.replace(".", "");
            vh2.ts_new.setText(ts);

            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                vh2.star_new.setVisibility(View.VISIBLE);
            } else {
                vh2.star_new.setVisibility(View.GONE);
            }

            String status = message.getDeliveryStatus();
            switch (status) {
                case "3":
                    vh2.clock_new.setVisibility(View.GONE);
                    vh2.ivSingleLineTick.setImageResource(R.drawable.message_deliver_tick);
                    vh2.ivSingleLineTick.setVisibility(View.VISIBLE);

                    break;
                case "2":
                    vh2.clock_new.setVisibility(View.GONE);

                    vh2.ivSingleLineTick.setImageResource(R.mipmap.ic_double_tick);
                    vh2.ivSingleLineTick.setVisibility(View.VISIBLE);

                    break;
                case "1":

                    vh2.clock_new.setVisibility(View.GONE);
                    vh2.ivSingleLineTick.setImageResource(R.mipmap.ic_single_tick);
                    vh2.ivSingleLineTick.setVisibility(View.VISIBLE);
                    break;
                default:
                    vh2.clock_new.setVisibility(View.VISIBLE);
                    vh2.ivSingleLineTick.setVisibility(View.GONE);

                    break;
            }
        }

        Boolean replymsg = false;
        if (position == 0) {
            if (message.isInfoMsg() == false && message.isSelf()) {
                vh2.imageViewindicatior.setVisibility(View.VISIBLE);
            }
        } else {

            MessageItemChat prevmsg = mListData.get(position - 1);
            if (message.isSelf() == !prevmsg.isSelf()) {
                vh2.imageViewindicatior.setVisibility(View.VISIBLE);
            } else {
                vh2.imageViewindicatior.setVisibility(View.GONE);
            }

        }

        if (mListData.get(position).getReplyMessage() != null && !mListData.get(position).getReplyMessage().equals("")) {
            vh2.replaymessage.setVisibility(View.VISIBLE);
            vh2.replaylayout.setVisibility(View.VISIBLE);
            replymsg = true;
            vh2.lblMsgFrom2.setText(message.getReplySenser());
            vh2.replaymessage.post(new Runnable() {
                @Override
                public void run() {
                    vh2.replaymessage.setText(message.getReplyMessage());
                }
            });
            if (message.getReplyType() != null && !message.getReplyType().equals("")) {
                if (Integer.parseInt(message.getReplyType()) == MessageFactory.audio) {
                    vh2.replaymessagemedio.setVisibility(View.VISIBLE);
                    vh2.replaymessage.setVisibility(View.GONE);
                    vh2.cameraphoto.setVisibility(View.VISIBLE);
                    vh2.cameraphoto.setImageResource(R.drawable.audio);
                    vh2.replaymessagemedio.setText("Audio");
                }
                if (Integer.parseInt(message.getReplyType()) == MessageFactory.video) {
                    vh2.sentimage.setVisibility(View.VISIBLE);
                    vh2.replaymessagemedio.setVisibility(View.VISIBLE);
                    vh2.replaymessage.setVisibility(View.GONE);
                    vh2.replaymessagemedio.setText(message.getReplyMessage());
                    vh2.cameraphoto.setVisibility(View.VISIBLE);
                    vh2.cameraphoto.setImageResource(R.drawable.video);
                    Bitmap photo = ConvertToImage(message.getreplyimagebase64());
                    vh2.sentimage.setImageBitmap(photo);
                }
                if (Integer.parseInt(message.getReplyType()) == MessageFactory.document) {
                    vh2.replaymessagemedio.setVisibility(View.VISIBLE);
                    vh2.replaymessage.setVisibility(View.GONE);
                    vh2.cameraphoto.setVisibility(View.VISIBLE);
                    vh2.cameraphoto.setImageResource(R.drawable.document);
                    vh2.replaymessagemedio.setText(message.getReplyMessage());
                }
                if (Integer.parseInt(message.getReplyType()) == MessageFactory.contact) {
                    vh2.replaymessagemedio.setVisibility(View.VISIBLE);
                    vh2.replaymessage.setVisibility(View.GONE);
                    vh2.cameraphoto.setVisibility(View.VISIBLE);
                    vh2.cameraphoto.setImageResource(R.drawable.add_participant);
                    vh2.replaymessagemedio.setText(message.getReplyMessage());
                }
            }
        }

        if (message.getreplyimgpath() != null && !message.getreplyimgpath().equals("")) {
            vh2.replaymessagemedio.setVisibility(View.VISIBLE);
            vh2.replaylayout.setVisibility(View.VISIBLE);
            vh2.cameraphoto.setVisibility(View.VISIBLE);
            vh2.cameraphoto.setImageResource(R.mipmap.chat_attachment_camera_grey_icon_off);
            vh2.sentimage.setVisibility(View.VISIBLE);
            vh2.lblMsgFrom2.setText(message.getReplySenser());
            vh2.replaymessagemedio.setText("Photo");
            final String imgpath = message.getreplyimgpath();
            Uri imgUri = Uri.parse(imgpath);
            vh2.sentimage.setImageURI(imgUri);

            vh2.sentimage.setImageBitmap(SmarttyImageUtils.decodeBitmapFromFile(imgpath, 50, 50));

            vh2.sentimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ImageZoom.class);
                    intent.putExtra("image", imgpath);
                    mContext.startActivity(intent);
                }
            });
        }


        if (message != null) {

            if (message.isSelected())
                vh2.selection_layout.setBackgroundColor(selectedItemColor);
                // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
            else
                vh2.selection_layout.setBackgroundColor(unSelectedItemColor);
            String textmessage = message.getTextMessage();
            if (!textmessage.contains("\n") && textmessage.length() <= 15) {
                if (!replymsg) {
                    vh2.time.setVisibility(View.VISIBLE);
                    if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                        vh2.starred.setVisibility(View.VISIBLE);

                    } else {
                        vh2.starred.setVisibility(View.GONE);


                    }

                    // vh2.senderName.setText(message.getSenderName());
                    String ts = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
                    ts = ts.replace(".", "");
                    vh2.time.setText(ts);

                    try {
                        vh2.message.post(new Runnable() {
                            @Override
                            public void run() {
                                vh2.message.setText(message.getTextMessage());
                            }
                        });
                    } catch (OutOfMemoryError e) {
                        MyLog.e(TAG, "", e);
                    }

                    String status = message.getDeliveryStatus();
                    if (status == null) {
                        status = MessageFactory.DELIVERY_STATUS_SENT;
                    }
                    switch (status) {
                        case "3":
                            vh2.clock.setVisibility(View.GONE);
                            vh2.doubleTickBlue.setVisibility(View.VISIBLE);
                            vh2.doubleTickBlue.setImageResource(R.drawable.message_deliver_tick);


                            break;
                        case "2":
                            vh2.clock.setVisibility(View.GONE);

                            vh2.doubleTickBlue.setVisibility(View.VISIBLE);
                            vh2.doubleTickBlue.setImageResource(R.mipmap.ic_double_tick);


                            break;
                        case "1":

                            vh2.clock.setVisibility(View.GONE);
                            vh2.doubleTickBlue.setVisibility(View.VISIBLE);
                            vh2.doubleTickBlue.setImageResource(R.mipmap.ic_single_tick);


                            break;
                        default:
                            vh2.clock.setVisibility(View.VISIBLE);
                            vh2.doubleTickBlue.setVisibility(View.GONE);


                            break;
                    }
                } else {
                    vh2.layout_reply_tsalign_above.setVisibility(View.VISIBLE);
                    vh2.ts_reply_above.setVisibility(View.VISIBLE);
                    if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                        vh2.starredindicator_reply_above.setVisibility(View.VISIBLE);
                    } else {
                        vh2.starredindicator_reply_above.setVisibility(View.GONE);
                    }

                    String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
                    mydate = mydate.replace(".", "");
                    vh2.ts_reply_above.setText(mydate);
                    try {
                        vh2.message.post(new Runnable() {
                            @Override
                            public void run() {
                                vh2.message.setText(message.getTextMessage());

                            }
                        });
                    } catch (Exception e) {
                        MyLog.e(TAG, "configureViewHolderMessageSent: ", e);
                    }

                    String status = message.getDeliveryStatus();
                    if (status == null) {
                        status = MessageFactory.DELIVERY_STATUS_SENT;
                    }
                    switch (status) {
                        case "3":
                            vh2.clock_reply_above.setVisibility(View.GONE);
                            vh2.single_tick_green_above_reply.setVisibility(View.VISIBLE);
                            vh2.single_tick_green_above_reply.setImageResource(R.drawable.message_deliver_tick);


                            break;
                        case "2":
                            vh2.clock_reply_above.setVisibility(View.GONE);
                            vh2.single_tick_green_above_reply.setVisibility(View.VISIBLE);
                            vh2.single_tick_green_above_reply.setImageResource(R.mipmap.ic_double_tick);

                            break;
                        case "1":
                            vh2.clock_reply_above.setVisibility(View.GONE);
                            vh2.single_tick_green_above_reply.setVisibility(View.VISIBLE);
                            vh2.single_tick_green_above_reply.setImageResource(R.mipmap.ic_single_tick);

                            break;
                        default:
                            vh2.clock_reply_above.setVisibility(View.VISIBLE);
                            vh2.single_tick_green_above_reply.setVisibility(View.GONE);

                            break;
                    }
                }
            } else {
                if (!replymsg) {
                    vh2.timebelow.setVisibility(View.VISIBLE);
                    if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                        vh2.starredbelow.setVisibility(View.VISIBLE);
                    } else {
                        vh2.starredbelow.setVisibility(View.GONE);
                    }

                    String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
                    mydate = mydate.replace(".", "");
                    vh2.timebelow.setText(mydate);
                    try {
                        vh2.message.post(new Runnable() {
                            @Override
                            public void run() {
                                vh2.message.setText(message.getTextMessage());

                            }
                        });
                    } catch (Exception e) {
                        MyLog.e(TAG, "configureViewHolderMessageSent: ", e);
                    }

                    String status = message.getDeliveryStatus();
                    if (status == null) {
                        status = MessageFactory.DELIVERY_STATUS_SENT;
                    }
                    switch (status) {
                        case "3":

                            vh2.clockbelow.setVisibility(View.GONE);
                            vh2.ivTickBelow.setImageResource(R.drawable.message_deliver_tick);
                            vh2.ivTickBelow.setVisibility(View.VISIBLE);

                            break;
                        case "2":
                            vh2.clockbelow.setVisibility(View.GONE);
                            vh2.ivTickBelow.setImageResource(R.mipmap.ic_double_tick);
                            vh2.ivTickBelow.setVisibility(View.VISIBLE);
                            break;
                        case "1":

                            vh2.clockbelow.setVisibility(View.GONE);
                            vh2.ivTickBelow.setImageResource(R.mipmap.ic_single_tick);
                            vh2.ivTickBelow.setVisibility(View.VISIBLE);
                            break;
                        default:

                            vh2.clockbelow.setVisibility(View.VISIBLE);
                            vh2.ivTickBelow.setVisibility(View.GONE);
                            break;
                    }
                } else {
                    vh2.layout_reply_tsalign.setVisibility(View.VISIBLE);
                    vh2.ts_reply.setVisibility(View.VISIBLE);
                    if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                        vh2.starredindicator_reply.setVisibility(View.VISIBLE);
                    } else {
                        vh2.starredindicator_reply.setVisibility(View.GONE);
                    }

                    String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
                    mydate = mydate.replace(".", "");
                    vh2.ts_reply.setText(mydate);
                    try {
                        vh2.message.post(new Runnable() {
                            @Override
                            public void run() {
                                vh2.message.setText(message.getTextMessage());

                            }
                        });
                    } catch (Exception e) {
                        MyLog.e(TAG, "", e);
                    }

                    String status = message.getDeliveryStatus();
                    if (status == null) {
                        status = MessageFactory.DELIVERY_STATUS_SENT;
                    }
                    switch (status) {
                        case "3":
                            vh2.clock_reply.setVisibility(View.GONE);
                            vh2.single_tick_green_below_reply.setVisibility(View.VISIBLE);
                            vh2.single_tick_green_below_reply.setImageResource(R.drawable.message_deliver_tick);


                            break;
                        case "2":
                            vh2.clock_reply.setVisibility(View.GONE);
                            vh2.single_tick_green_below_reply.setVisibility(View.VISIBLE);
                            vh2.single_tick_green_below_reply.setImageResource(R.mipmap.ic_double_tick);

                            break;
                        case "1":
                            vh2.clock_reply.setVisibility(View.GONE);
                            vh2.single_tick_green_below_reply.setVisibility(View.VISIBLE);
                            vh2.single_tick_green_below_reply.setImageResource(R.mipmap.ic_single_tick);

                            break;
                        default:
                            vh2.clock_reply.setVisibility(View.VISIBLE);
                            vh2.single_tick_green_below_reply.setVisibility(View.GONE);

                            break;
                    }
                }
            }


        }
    }

    private void configureViewHolderImageSent(final VHImageSent vh2, final int position) {
        //     vh2.relative_layout_message.setBackgroundResource(R.drawable.secret_msg_sent_bg);

        final MessageItemChat message = mListData.get(position);
        Boolean caption = false;
        if (message != null) {
            String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            mydate = mydate.replace(".", "");
            if (message.getTextMessage() != null && !message.getTextMessage().trim().equalsIgnoreCase("")) {
                vh2.captiontext.setVisibility(View.VISIBLE);
                vh2.ts_abovecaption.setVisibility(View.GONE);
                vh2.time_layout.setVisibility(View.VISIBLE);
                vh2.captiontext.post(new Runnable() {
                    @Override
                    public void run() {
                        vh2.captiontext.setText(message.getTextMessage());
                    }
                });
                caption = true;
                vh2.time.setText(mydate);
                if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                    vh2.starredindicator_below.setVisibility(View.VISIBLE);
                } else {
                    vh2.starredindicator_below.setVisibility(View.GONE);
                }
            } else {
                vh2.captiontext.setVisibility(View.GONE);
                vh2.ts_abovecaption.setVisibility(View.VISIBLE);
                vh2.time_layout.setVisibility(View.GONE);
                vh2.ts_abovecaption.setText(mydate);
                caption = false;
                if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                    vh2.starredindicator_above.setVisibility(View.VISIBLE);
                } else {
                    vh2.starredindicator_above.setVisibility(View.GONE);
                }
            }

            // vh2.senderName.setText(message.getSenderName());


            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                vh2.starredindicator_below.setVisibility(View.VISIBLE);
            } else {
                vh2.starredindicator_below.setVisibility(View.GONE);
            }
            if (message.isSelected())
                vh2.selection_layout.setBackgroundColor(selectedItemColor);
                // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
            else
                vh2.selection_layout.setBackgroundColor(unSelectedItemColor);

            // vh2.imageViewindicatior.setImageResource(R.drawable.ic_secret_sent_corner);

            if (position == 0) {
                if (message.isInfoMsg() == false && message.isSelf()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                }
            } else {

                MessageItemChat prevmsg = mListData.get(position - 1);
                if (message.isSelf() == !prevmsg.isSelf() || prevmsg.isDate()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vh2.imageViewindicatior.setVisibility(View.GONE);
                }

            }

            if ((message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_UPLOADING &&
                    message.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_NOT_SENT))
                    || message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_DOWNLOADING) {
                try {
                    vh2.pbUpload.setVisibility(View.VISIBLE);
                    vh2.pbUpload.setProgress(message.getUploadDownloadProgress());
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            } else {
                vh2.pbUpload.setVisibility(View.GONE);
            }


            try {
                String imgPath = message.getChatFileLocalPath();
                File file = new File(imgPath);
                if (file.exists()) {
                    vh2.imageView.setImageBitmap(SmarttyImageUtils.decodeBitmapFromFile(imgPath, 220, 200));
                } else {
                    String thumbData = message.getThumbnailData();
                    String imageDataBytes = thumbData.substring(thumbData.indexOf(",") + 1);
                    byte[] decodedString = Base64.decode(imageDataBytes, Base64.DEFAULT);
                    Bitmap thumbBmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    vh2.imageView.setImageBitmap(thumbBmp);
                }


                if (message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_COMPLETED
                        && message.isSelf()) {
                    vh2.forward_image.setVisibility(View.GONE);
                    final String image = message.getChatFileLocalPath();
                    File imgFile = new File(image);

                    if (imgFile.exists()) {
                        vh2.imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!FirstItemSelected) {
                                    String image = message.getChatFileLocalPath();
                                    Intent intent = new Intent(mContext, ImageZoom.class);
                                    intent.putExtra("image", image);
                                    mContext.startActivity(intent);
                                }
                            }
                        });

                        vh2.forward_image.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                                selectedChatItems.add(message);

                                Bundle fwdBundle = new Bundle();
                                //  fwdBundle.putSerializable("MsgItemList", selectedChatItems);
                                fwdBundle.putBoolean("FromSmartty", true);
                                CommonData.setForwardedItems(selectedChatItems);

                                Intent intent = new Intent(mContext, ForwardContact.class);
                                intent.putExtras(fwdBundle);
                                mContext.startActivity(intent);
                            }
                        });
                    }
                }

            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }

            String status = message.getDeliveryStatus();

            if (status == null) {
                status = MessageFactory.DELIVERY_STATUS_SENT;
            }
            if (!caption) {
                vh2.ts_abovecaption_layout.setVisibility(View.VISIBLE);
                vh2.rlMsgStatus_above.setVisibility(View.VISIBLE);
                vh2.time_layout.setVisibility(View.GONE);
                vh2.rlMsgStatus.setVisibility(View.GONE);

                switch (status) {
                    case "3":
                        vh2.clock_above.setVisibility(View.GONE);
                        vh2.ivTickAbove.setVisibility(View.VISIBLE);
                        vh2.ivTickAbove.setImageResource(R.drawable.message_deliver_tick);
                        break;
                    case "2":
                        vh2.clock_above.setVisibility(View.GONE);
                        vh2.ivTickAbove.setVisibility(View.VISIBLE);
                        vh2.ivTickAbove.setImageResource(R.mipmap.ic_double_tick);

                        break;
                    case "1":

                        vh2.clock_above.setVisibility(View.GONE);
                        vh2.ivTickAbove.setVisibility(View.VISIBLE);
                        vh2.ivTickAbove.setImageResource(R.mipmap.ic_single_tick);
                        break;
                    default:
                        vh2.clock_above.setVisibility(View.VISIBLE);
                        vh2.ivTickAbove.setVisibility(View.GONE);
                        break;
                }
            } else {
                vh2.ts_abovecaption_layout.setVisibility(View.GONE);
                vh2.time_layout.setVisibility(View.VISIBLE);
                vh2.rlMsgStatus_above.setVisibility(View.GONE);
                vh2.rlMsgStatus.setVisibility(View.VISIBLE);


                switch (status) {
                    case "3":

                        vh2.clock.setVisibility(View.GONE);
                        vh2.ivTick.setVisibility(View.VISIBLE);
                        vh2.ivTick.setImageResource(R.drawable.message_deliver_tick);


                        break;
                    case "2":
                        vh2.clock.setVisibility(View.GONE);
                        vh2.ivTick.setVisibility(View.VISIBLE);
                        vh2.ivTick.setImageResource(R.mipmap.ic_double_tick);

                        break;
                    case "1":

                        vh2.clock.setVisibility(View.GONE);
                        vh2.ivTick.setVisibility(View.VISIBLE);
                        vh2.ivTick.setImageResource(R.mipmap.ic_single_tick);
                        break;
                    default:
                        vh2.clock.setVisibility(View.VISIBLE);
                        vh2.ivTick.setVisibility(View.GONE);
                        break;
                }
            }
        }
    }

    private void configureViewHolderVideoSent(final VHVideoSent vh2, final int position) {
        //    vh2.rlVideoMsg.setBackgroundResource(R.drawable.secret_msg_sent_bg);
        Boolean caption = false;
        final MessageItemChat message = mListData.get(position);
        if (message != null) {

            String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            mydate = mydate.replace(".", "");
            vh2.time.setText(mydate);

            String surationasec = "";
            MediaMetadataRetriever mdr = new MediaMetadataRetriever();
            if (message.getChatFileLocalPath() != null) {
                mdr.setDataSource(message.getChatFileLocalPath());
                String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                surationasec = getTimeString(AppUtils.parseLong(duration));

            }

            if (message.getTextMessage() != null && !message.getTextMessage().trim().equalsIgnoreCase("")) {
                caption = true;
                vh2.videoabove_layout.setVisibility(View.GONE);
                vh2.video_belowlayout.setVisibility(View.VISIBLE);
                vh2.captiontext.setVisibility(View.VISIBLE);
                vh2.captiontext.post(new Runnable() {
                    @Override
                    public void run() {
                        vh2.captiontext.setText(message.getTextMessage());

                    }
                });
                vh2.time.setText(mydate);
                if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                    vh2.starredindicator_below.setVisibility(View.VISIBLE);
                } else {
                    vh2.starredindicator_below.setVisibility(View.GONE);
                }
                vh2.duration.setText(surationasec);

            } else {

                vh2.videoabove_layout.setVisibility(View.VISIBLE);
                vh2.video_belowlayout.setVisibility(View.GONE);
                vh2.captiontext.setVisibility(View.GONE);
                vh2.ts_abovecaption.setText(mydate);
                caption = false;
                if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                    vh2.starredindicator_above.setVisibility(View.VISIBLE);
                } else {
                    vh2.starredindicator_above.setVisibility(View.GONE);
                }
                vh2.duration_above.setText(surationasec);
            }


            // vh2.senderName.setText(message.getSenderName());

            //vh2.imageViewindicatior.setImageResource(R.drawable.ic_secret_sent_corner);
            if (position == 0) {
                if (!message.isInfoMsg() && message.isSelf()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                }
            } else {

                MessageItemChat prevmsg = mListData.get(position - 1);
                if (message.isSelf() == !prevmsg.isSelf() || prevmsg.isDate()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vh2.imageViewindicatior.setVisibility(View.GONE);
                }

            }
            if (message.isSelected())
                vh2.selection_layout.setBackgroundColor(selectedItemColor);
                // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
            else
                vh2.selection_layout.setBackgroundColor(unSelectedItemColor);

            Glide.clear(vh2.thumbnail);

            // vh2.thumbnail.setImageDrawable (null);
            AppUtils.loadVideoThumbnail(mContext, message.getChatFileLocalPath(), vh2.thumbnail, 0.6f, 140);


            if (message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_UPLOADING) {
                try {
                    vh2.ivPlay.setVisibility(View.GONE);
                    vh2.pbUpload.setVisibility(View.VISIBLE);
                    vh2.pbUpload.setProgress(message.getUploadDownloadProgress());
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            } else {
                vh2.pbUpload.setVisibility(View.GONE);
                vh2.ivPlay.setVisibility(View.VISIBLE);
            }

            vh2.thumbnail.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(message.getChatFileLocalPath()));
                        String path = "file://" + message.getChatFileLocalPath();
                        intent.setDataAndType(Uri.parse(path), "video/*");
                        mContext.startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(mContext, "No app installed to play this video", Toast.LENGTH_LONG).show();
                    }
                }
            });


            String status = message.getDeliveryStatus();

            if (!caption) {
                vh2.videoabove_layout.setVisibility(View.VISIBLE);
                vh2.video_belowlayout.setVisibility(View.GONE);
                switch (status) {
                    case "3":
                        vh2.clock_above.setVisibility(View.GONE);
                        vh2.ivTickAbove.setVisibility(View.VISIBLE);
                        vh2.ivTickAbove.setImageResource(R.drawable.message_deliver_tick);
                        break;
                    case "2":
                        vh2.clock_above.setVisibility(View.GONE);
                        vh2.ivTickAbove.setVisibility(View.VISIBLE);
                        vh2.ivTickAbove.setImageResource(R.mipmap.ic_double_tick);
                        break;
                    case "1":

                        vh2.clock_above.setVisibility(View.GONE);
                        vh2.ivTickAbove.setVisibility(View.VISIBLE);
                        vh2.ivTickAbove.setImageResource(R.mipmap.ic_single_tick);
                        break;
                    default:
                        vh2.clock_above.setVisibility(View.VISIBLE);
                        vh2.ivTickAbove.setVisibility(View.GONE);
                        break;
                }
            } else {

                vh2.videoabove_layout.setVisibility(View.GONE);
                vh2.video_belowlayout.setVisibility(View.VISIBLE);
                switch (status) {
                    case "3":
                        vh2.clock.setVisibility(View.GONE);
                        vh2.ivTick.setVisibility(View.VISIBLE);
                        vh2.ivTick.setImageResource(R.drawable.message_deliver_tick);
                        break;
                    case "2":
                        vh2.clock.setVisibility(View.GONE);
                        vh2.ivTick.setVisibility(View.VISIBLE);
                        vh2.ivTick.setImageResource(R.mipmap.ic_double_tick);
                        break;
                    case "1":

                        vh2.clock.setVisibility(View.GONE);
                        vh2.ivTick.setVisibility(View.VISIBLE);
                        vh2.ivTick.setImageResource(R.mipmap.ic_single_tick);
                        break;
                    default:
                        vh2.clock.setVisibility(View.VISIBLE);
                        vh2.ivTick.setVisibility(View.GONE);
                        break;
                }

            }
        }
    }

    private void configureViewHolderLocationSent(final VHLocationSent vh2, final int position) {
        //   vh2.relative_layout_message.setBackgroundResource(R.drawable.secret_msg_sent_bg);

        final MessageItemChat message = mListData.get(position);
        if (message != null) {

            if (message.isSelected())
                vh2.selection_layout.setBackgroundColor(selectedItemColor);
                // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
            else
                vh2.selection_layout.setBackgroundColor(unSelectedItemColor);

            // vh2.senderName.setText(message.getSenderName());
            String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            mydate = mydate.replace(".", "");
            vh2.time.setText(mydate);
            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                vh2.starredindicator_below.setVisibility(View.VISIBLE);
            } else {
                vh2.starredindicator_below.setVisibility(View.GONE);
            }

            // vh2.imageViewindicatior.setImageResource(R.drawable.ic_secret_sent_corner);
            if (position == 0) {
                if (message.isInfoMsg() == false && message.isSelf()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                }
            } else {

                MessageItemChat prevmsg = mListData.get(position - 1);
                if (message.isSelf() == !prevmsg.isSelf() || prevmsg.isDate()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vh2.imageViewindicatior.setVisibility(View.GONE);
                }

            }
            if (message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_UPLOADING) {
                try {
                    vh2.pbUpload.setVisibility(View.VISIBLE);
                    vh2.pbUpload.setProgress(message.getUploadDownloadProgress());
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            } else {
                vh2.pbUpload.setVisibility(View.GONE);
            }
            /*byte[] decodedString = Base64.decode(message.getThumbnailData(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            vh2.ivMap.setImageBitmap(decodedByte); // image for loading...*/
//            Picasso.with(mContext).load(message.getWebLinkImgUrl()).error(R.drawable.ic_add_user).into(vh2.ivMap);

            if (message.getWebLinkImgThumb() != null) {
                String thumbData = message.getWebLinkImgThumb();
                if (thumbData != null) {
                    if (thumbData.startsWith("data:image/jpeg;base64,")) {
                        thumbData = thumbData.replace("data:image/jpeg;base64,", "");
                    }
                    byte[] decodedString = Base64.decode(thumbData, Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    vh2.ivMap.setImageBitmap(decodedByte);
                } else {
                    vh2.ivMap.setImageBitmap(null);
                }
            } else {
                vh2.ivMap.setImageBitmap(null);
            }

            ImageLoader imageLoader = CoreController.getInstance().getImageLoader();
            imageLoader.get(message.getWebLinkImgUrl(), new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    if (response.getBitmap() != null) {
                        vh2.ivMap.setImageBitmap(response.getBitmap());
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (message.getWebLinkImgThumb() != null) {
                        String thumbData = message.getWebLinkImgThumb();
                        if (thumbData != null) {
                            if (thumbData.startsWith("data:image/jpeg;base64,")) {
                                thumbData = thumbData.replace("data:image/jpeg;base64,", "");
                            }
                            byte[] decodedString = Base64.decode(thumbData, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            vh2.ivMap.setImageBitmap(decodedByte);
                        } else {
                            vh2.ivMap.setImageBitmap(null);
                        }
                    } else {
                        vh2.ivMap.setImageBitmap(null);
                    }
                }
            });

            String status = message.getDeliveryStatus();

            switch (status) {
                case "3":

                    vh2.clock.setVisibility(View.GONE);
                    vh2.singleTick.setVisibility(View.GONE);
                    vh2.doubleTickGreen.setVisibility(View.GONE);
                    vh2.doubleTickBlue.setVisibility(View.VISIBLE);

                    break;
                case "2":
                    vh2.clock.setVisibility(View.GONE);
                    vh2.singleTick.setVisibility(View.GONE);

                    vh2.doubleTickGreen.setVisibility(View.VISIBLE);
                    vh2.doubleTickBlue.setVisibility(View.GONE);
                    break;
                case "1":

                    vh2.clock.setVisibility(View.GONE);
                    vh2.singleTick.setVisibility(View.VISIBLE);

                    vh2.doubleTickGreen.setVisibility(View.GONE);
                    vh2.doubleTickBlue.setVisibility(View.GONE);
                    break;
                default:


                    vh2.clock.setVisibility(View.VISIBLE);
                    vh2.singleTick.setVisibility(View.GONE);

                    vh2.doubleTickGreen.setVisibility(View.GONE);
                    vh2.doubleTickBlue.setVisibility(View.GONE);
                    break;
            }
        }
    }

    private void configureViewHolderContactSent(VHContactSent vh2, final int position) {
        final MessageItemChat message = mListData.get(position);

        //   vh2.imageViewindicatior.setImageResource(R.drawable.ic_secret_sent_corner);
        //  vh2.relative_layout_message.setBackgroundResource(R.drawable.secret_msg_sent_bg);
        // vh2.v1.setBackgroundColor(ContextCompat.getColor(mContext, R.color.secret_viewcolor));

        if (message != null) {

            if (message.isSelected()) {
                vh2.selection_layout.setBackgroundColor(selectedItemColor);
                // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
            } else {
                vh2.selection_layout.setBackgroundColor(unSelectedItemColor);
            }
            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                vh2.starredindicator_below.setVisibility(View.VISIBLE);
            } else {
                vh2.starredindicator_below.setVisibility(View.GONE);
            }

            // vh2.senderName.setText(message.getSenderName());
            String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            mydate = mydate.replace(".", "");
            vh2.time.setText(mydate);

            final String contactName = message.getContactName();
            final String contactNumber = message.getContactNumber();

            final String contactSmarttyID = message.getContactSmarttyId();
            final String contactDetails = message.getDetailedContacts();

            if (position == 0) {
                if (message.isInfoMsg() == false && message.isSelf()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                }
            } else {
                MessageItemChat prevmsg = mListData.get(position - 1);
                if (message.isSelf() == !prevmsg.isSelf()) {
                    vh2.imageViewindicatior.setVisibility(View.VISIBLE);
                } else {
                    vh2.imageViewindicatior.setVisibility(View.GONE);
                }

            }
            if (contactNumber != null) {
                vh2.contactName.setText(contactName);
            } else {
                vh2.contactName.setText("");
            }

            getcontactname.configProfilepic(vh2.contactImage, contactSmarttyID, false, true, R.mipmap.contact_secret);

            if (contactNumber != null) {
                if (contactNumber.equals("")) {
                    vh2.add.setVisibility(View.GONE);
                    vh2.contact_add_invite.setVisibility(View.GONE);
                    vh2.v1.setVisibility(View.GONE);
                    vh2.invite.setVisibility(View.GONE);


                } else {
                    if (contactSmarttyID != null) {
                        if (contactSmarttyID.equals("")) {
                            vh2.add.setVisibility(View.GONE);
                            vh2.add1.setVisibility(View.GONE);
                            vh2.invite1.setVisibility(View.GONE);
                            vh2.invite.setVisibility(View.VISIBLE);
                            vh2.v1.setVisibility(View.VISIBLE);


                        } else if (!contactSmarttyID.equals("")) {
                            vh2.add.setVisibility(View.GONE);
                            vh2.add1.setVisibility(View.GONE);
                            vh2.invite1.setVisibility(View.GONE);
                            vh2.invite.setVisibility(View.GONE);

                            vh2.v1.setVisibility(View.VISIBLE);
                        }
                    }

                }

            }


            String status = message.getDeliveryStatus();

            switch (status) {
                case "3":
                    vh2.clock.setVisibility(View.GONE);
                    vh2.ivTick.setVisibility(View.VISIBLE);
                    vh2.ivTick.setImageResource(R.drawable.message_deliver_tick);
                    break;
                case "2":
                    vh2.clock.setVisibility(View.GONE);
                    vh2.ivTick.setVisibility(View.VISIBLE);
                    vh2.ivTick.setImageResource(R.mipmap.ic_double_tick);
                    break;
                case "1":

                    vh2.clock.setVisibility(View.GONE);
                    vh2.ivTick.setVisibility(View.VISIBLE);
                    vh2.ivTick.setImageResource(R.mipmap.ic_single_tick);
                    break;
                default:
                    vh2.clock.setVisibility(View.VISIBLE);
                    vh2.ivTick.setVisibility(View.GONE);
                    break;
            }

            vh2.invite.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    performUserInvite(contactName, contactDetails);
                    return false;
                }
            });
            vh2.invite1.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    performUserInvite(contactName, contactDetails);
                    return false;
                }
            });
            vh2.add.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Intent intent = new Intent(mContext, Savecontact.class);
                    intent.putExtra("name", contactName);
                    intent.putExtra("number", contactNumber);
                    intent.putExtra("contactList", contactDetails);
                    mContext.startActivity(intent);
                    return false;
                }
            });
            vh2.add1.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Intent intent = new Intent(mContext, Savecontact.class);
                    intent.putExtra("name", contactName);
                    intent.putExtra("number", contactNumber);
                    intent.putExtra("contactList", contactDetails);
                    mContext.startActivity(intent);
                    return false;
                }
            });
            vh2.contactName.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Intent intent = new Intent(mContext, Savecontact.class);
                    intent.putExtra("name", contactName);
                    intent.putExtra("number", contactNumber);
                    intent.putExtra("contactList", contactDetails);
                    mContext.startActivity(intent);
                    return false;
                }
            });

        }
    }

    private void performUserInvite(String contactName, String contactDetails) {

        ArrayList<String> aDetails = null;

        try {
            aDetails = new ArrayList<String>();
            JSONObject data = new JSONObject(contactDetails);
            JSONArray contactmail_List = null;
            JSONArray contactph_List = data.getJSONArray("phone_number");
            if (contactph_List.length() > 0) {
                for (int i = 0; i < contactph_List.length(); i++) {

                    JSONObject phone = contactph_List.getJSONObject(i);
                    String value = phone.getString("value");
                    aDetails.add(value);
                }

                if (data.has("email")) {
                    contactmail_List = data.getJSONArray("email");
                    if (contactmail_List.length() > 0) {
                        for (int i = 0; i < contactmail_List.length(); i++) {

                            JSONObject mail = contactmail_List.getJSONObject(i);
                            String value = mail.getString("value");
                            aDetails.add(value);
                        }
                    }
                }

            }


        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        ShowInviteAlertForBoth(aDetails, contactName);

    }

//    private void playAudio(final int position, MessageItemChat message, final SeekBar sbDuration) {
//        Uri uri = Uri.parse(message.getChatFileLocalPath());
//        mPlayer = MediaPlayer.create(mContext, uri);
//        mTimer = new Timer();
//
//        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mediaPlayer) {
//                mPlayer.start();
//                if (mListData.size() > position) {
//                    if (mListData.get(position).getPlayerCurrentPosition() < mPlayer.getDuration()) {
//                        mPlayer.seekTo(mListData.get(position).getPlayerCurrentPosition() * 1000);
//                    } else {
//                        mPlayer.seekTo((mListData.get(position).getPlayerCurrentPosition() - 1) * 1000);
//                    }
//                    lastPlayedAt = position;
//
//                    final int duration = mPlayer.getDuration();
//                    final int amongToupdate = 1000;
//                    mListData.get(position).setIsMediaPlaying(true);
//                    mListData.get(position).setPlayerMaxDuration(mPlayer.getDuration());
//                    final int max = mPlayer.getDuration() / 1000;
//                    sbDuration.setMax(max);
//
//                    mTimer.schedule(new TimerTask() {
//
//                        @Override
//                        public void run() {
//                            try {
//                                ((Activity) mContext).runOnUiThread(new Runnable() {
//
//                                    @Override
//                                    public void run() {
//                                        if (!(amongToupdate * mListData.get(position).getPlayerCurrentPosition() >= duration)) {
//                                            int progress = mListData.get(position).getPlayerCurrentPosition();
//                                            progress += 1;
//
//                                            if (max >= progress) {
//                                                mListData.get(position).setPlayerCurrentPosition(progress);
//                                            }
//                                        }
//                                        notifyDataSetChanged();
//                                    }
//                                });
//                            } catch (ArrayIndexOutOfBoundsException e) {
//                                Log.e(TAG,"",e);
//                            }
//                        }
//
//                    }, 0, amongToupdate);
//
//                    mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                        @Override
//                        public void onCompletion(MediaPlayer mediaPlayer) {
//                            mPlayer.release();
//                            mTimer.cancel();
//                            mListData.get(position).setPlayerCurrentPosition(0);
//                            mListData.get(position).setIsMediaPlaying(false);
//                            lastPlayedAt = -1;
//                            Log.d("SeekProgressEnd", "called");
//                            notifyDataSetChanged();
//                        }
//                    });
//                }
//            }
//        });
//    }

    private void configureViewHolderAudioSent(final VHAudioSent vh2, final int position) {
        //   vh2.relative_layout_message.setBackgroundResource(R.drawable.secret_msg_sent_bg);

        final MessageItemChat message = mListData.get(position);
        if (message != null) {

            if (message.isSelected())
                vh2.selection_layout.setBackgroundColor(selectedItemColor);
                // vh2.mainSent.setBackgroundResource(R.drawable.background_selector);
            else
                vh2.selection_layout.setBackgroundColor(unSelectedItemColor);

            // vh2.senderName.setText(message.getSenderName());
            String mydate = TimeStampUtils.get12HrTimeFormat(mContext, message.getTS());
            mydate = mydate.replace(".", "");
            vh2.time.setText(mydate);

            if (message.getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                vh2.audiotrack_layout.setVisibility(View.GONE);
                vh2.record_image.setVisibility(View.VISIBLE);
                vh2.record_icon.setVisibility(View.VISIBLE);
                vh2.recodingduration.setVisibility(View.VISIBLE);
                if (message.getDuration() != null) {
                    //  String duration = message.getDuration();
                    //String surationasec = getTimeString(AppUtils.parseLong(duration));
                    vh2.recodingduration.setText(message.getDuration());
                }
                SessionManager sessionmanager = SessionManager.getInstance(mContext);
                String userprofilepic = sessionmanager.getUserProfilePic();
                Picasso.with(mContext).load(Constants.SOCKET_IP + userprofilepic).error(
                        R.drawable.personprofile)
                        .into(vh2.record_image);

            } else {
                vh2.audiotrack_layout.setVisibility(View.VISIBLE);
                vh2.record_image.setVisibility(View.GONE);
                vh2.headset.setImageResource(R.drawable.headsetsecret);
                vh2.record_icon.setVisibility(View.GONE);
                vh2.recodingduration.setVisibility(View.GONE);
                if (message.getDuration() != null) {
                    String duration = message.getDuration();
                    vh2.duration.setTextColor(ContextCompat.getColor(mContext, R.color.black));
//                    String surationasec = getTimeString(AppUtils.parseLong(duration));
                    vh2.duration.setText(duration);
                }
            }

        }

        //vh2.imageViewindicatior.setImageResource(R.drawable.ic_secret_sent_corner);
        if (position == 0) {
            if (message.isInfoMsg() == false && message.isSelf()) {
                vh2.imageViewindicatior.setVisibility(View.VISIBLE);
            }
        } else {

            MessageItemChat prevmsg = mListData.get(position - 1);
            if (message.isSelf() == !prevmsg.isSelf() || prevmsg.isDate()) {
                vh2.imageViewindicatior.setVisibility(View.VISIBLE);
            } else {
                vh2.imageViewindicatior.setVisibility(View.GONE);
            }

        }
        if (message.getChatFileLocalPath() == null) {
            message.setChatFileLocalPath("");
        }


        if (message.isMediaPlaying()) {
            long value = message.getPlayerCurrentPosition() * 1000;
            if (message.getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                vh2.recodingduration.setText(getTimeString(value));
            } else {
                vh2.duration.setText(getTimeString(value));
            }
            vh2.playButton.setBackgroundResource(R.drawable.ic_pausesent);
        } else {
            vh2.playButton.setBackgroundResource(R.drawable.audio_playsent);
            if (message.getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                vh2.recodingduration.setText(message.getDuration());
            } else {
                vh2.duration.setText(message.getDuration());
            }
        }

        vh2.sbDuration.setProgress(message.getPlayerCurrentPosition());

        vh2.playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (lastPlayedAt > -1) {
                    mListData.get(lastPlayedAt).setIsMediaPlaying(false);
                    mTimer.cancel();
                    mPlayer.release();
                }

                if (lastPlayedAt != position) {
                    playAudio(position, message, vh2.sbDuration);
                } else {
                    lastPlayedAt = -1;
                }

                notifyDataSetChanged();
            }
        });

        vh2.sbDuration.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                if (mListData.size() > position && mListData.get(position).isMediaPlaying()) {
                    mTimer.cancel();
                    mPlayer.release();
                    playAudio(position, message, vh2.sbDuration);
                }

                try {
                    notifyDataSetChanged();
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub
                try {
                    if (position != RecyclerView.NO_POSITION && fromUser) {
                        mListData.get(position).setPlayerCurrentPosition(progress);
                    }
                } catch (Exception e) {

                }
            }
        });

        if (message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_UPLOADING) {
            try {
                vh2.playButton.setVisibility(View.INVISIBLE);
                vh2.sbDuration.setVisibility(View.INVISIBLE);
                vh2.pbUpload.setVisibility(View.VISIBLE);
                vh2.pbUpload.setProgress(message.getUploadDownloadProgress());
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
        } else {
            vh2.pbUpload.setVisibility(View.GONE);
            vh2.playButton.setVisibility(View.VISIBLE);
            vh2.sbDuration.setVisibility(View.VISIBLE);
        }

        String status = message.getDeliveryStatus();
        if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
            vh2.starredindicator.setVisibility(View.VISIBLE);
        } else {
            vh2.starredindicator.setVisibility(View.GONE);
        }

        switch (status) {
            case "3":
                vh2.clock.setVisibility(View.GONE);
                vh2.ivTick.setVisibility(View.VISIBLE);
                vh2.ivTick.setImageResource(R.drawable.message_deliver_tick);
                break;
            case "2":
                vh2.clock.setVisibility(View.GONE);
                vh2.ivTick.setVisibility(View.VISIBLE);
                vh2.ivTick.setImageResource(R.mipmap.ic_double_tick);
                break;
            case "1":

                vh2.clock.setVisibility(View.GONE);
                vh2.ivTick.setVisibility(View.VISIBLE);
                vh2.ivTick.setImageResource(R.mipmap.ic_single_tick);
                break;
            default:
                vh2.clock.setVisibility(View.VISIBLE);
                vh2.ivTick.setVisibility(View.GONE);
                break;
        }

    }

    private void playAudio(final int position, MessageItemChat message, final SeekBar sbDuration) {
        File file = new File(message.getChatFileLocalPath());
        String audioPath = message.getChatFileLocalPath();
        if (!file.exists()) {
            try {
                String[] filePathSplited = message.getChatFileLocalPath().split(File.separator);
                String fileName = filePathSplited[filePathSplited.length - 1];

                String publicDirPath = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_MUSIC).getAbsolutePath();
                audioPath = publicDirPath + File.separator + fileName;
            } catch (Exception e) {
                MyLog.e(TAG, "configureViewHolderImageReceived: ", e);
            }
        }

        Uri uri = Uri.parse(audioPath);
        mPlayer = MediaPlayer.create(mContext, uri);
        mTimer = new Timer();

        try {
            mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mPlayer.start();
                    if (mListData.get(position).getPlayerCurrentPosition() < mPlayer.getDuration()) {
                        mPlayer.seekTo(mListData.get(position).getPlayerCurrentPosition() * 1000);

                    } else {
                        mPlayer.seekTo((mListData.get(position).getPlayerCurrentPosition() - 1) * 1000);
                    }
                    lastPlayedAt = position;

                    final int duration = mPlayer.getDuration();

                    final int amongToupdate = 1000;
                    mListData.get(position).setIsMediaPlaying(true);
                    mListData.get(position).setPlayerMaxDuration(mPlayer.getDuration());
                    final int max = mPlayer.getDuration() / 1000;
                    sbDuration.setMax(max);

                    mTimer.schedule(new TimerTask() {

                        @Override
                        public void run() {
                            ((Activity) mContext).runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    if (!(amongToupdate * mListData.get(position).getPlayerCurrentPosition() >= duration)) {
                                        int progress = mListData.get(position).getPlayerCurrentPosition();
                                        progress += 1;

                                        if (max >= progress) {
                                            mListData.get(position).setPlayerCurrentPosition(progress);
                                        }
                                    }
                                    notifyDataSetChanged();
                                }
                            });
                        }

                    }, 0, amongToupdate);

                    mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {
                            mPlayer.release();
                            mTimer.cancel();
                            mListData.get(position).setPlayerCurrentPosition(0);
                            mListData.get(position).setIsMediaPlaying(false);
                            lastPlayedAt = -1;
                            notifyDataSetChanged();
                        }
                    });
                }
            });
        } catch (Exception e) {
            MyLog.e(TAG, "Error", e);
        }
    }

    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private void configureDateLabel(TextView tvDateLbl, int position) {

        tvDateLbl.setVisibility(View.GONE);
        tvDateLbl.setBackgroundResource(0);
        tvDateLbl.setTextColor(Color.WHITE);

        MessageItemChat item = mListData.get(position);
        String currentItemTS = item.getTS();
        Date currentItemDate = TimeStampUtils.getMessageTStoDate(mContext, currentItemTS);

        if (position == 0 && currentItemDate != null) {
            tvDateLbl.setVisibility(View.VISIBLE);
            setDateText(tvDateLbl, currentItemDate);
        } else {
            MessageItemChat prevItem = mListData.get(position - 1);
            String prevItemTS = prevItem.getTS();
            Date prevItemDate = TimeStampUtils.getMessageTStoDate(mContext, prevItemTS);

            if (!currentItemDate.equals(prevItemDate)) {
                tvDateLbl.setVisibility(View.VISIBLE);
                setDateText(tvDateLbl, currentItemDate);
            }
        }
    }

    private void configureScreenShotTaken(TextView tvScreenShotTaken, int position) {
        MessageItemChat item = mListData.get(position);
        tvScreenShotTaken.setText(item.getTextMessage());
    }

    private void configureSecretTimerLabel(TextView tvSecretLbl, int position) {
        tvSecretLbl.setVisibility(View.GONE);
        MessageItemChat currentMsgItem = mListData.get(position);

        if (position == 0) {
            tvSecretLbl.setVisibility(View.VISIBLE);
        } else {
            MessageItemChat prevMsgItem = mListData.get(position - 1);
            if (!currentMsgItem.getSecretTimer().equals(prevMsgItem.getSecretTimer())) {
                tvSecretLbl.setVisibility(View.VISIBLE);
            }
        }

        if (tvSecretLbl.getVisibility() == View.VISIBLE) {
            String createdBy = mListData.get(position).getSecretTimeCreatedBy();
            String name = null;
            if (createdBy.equals(mCurrentUserId)) {
                name = mContext.getString(R.string.you);
            } else {
                name = getcontactname.getSendername(createdBy, mListData.get(position).getSenderMsisdn());
            }

            int time = Integer.parseInt(currentMsgItem.getSecretTimer());
            String timeStr = "";
            switch (time) {
                case 5 * 1000:
                    timeStr = mContext.getResources().getString(R.string.time_5sec);
                    break;

                case 10 * 1000:
                    timeStr = mContext.getResources().getString(R.string.time_10sec);
                    break;

                case 30 * 1000:
                    timeStr = mContext.getResources().getString(R.string.time_30sec);
                    break;

                case 60 * 1000:
                    timeStr = mContext.getResources().getString(R.string.time_1min);
                    break;

                case 60 * 60 * 1000:
                    timeStr = mContext.getResources().getString(R.string.time_1hr);
                    break;

                case 24 * 60 * 60 * 1000:
                    timeStr = mContext.getResources().getString(R.string.time_1day);
                    break;

                case 7 * 24 * 60 * 60 * 1000:
                    timeStr = mContext.getResources().getString(R.string.time_1week);
                    break;
            }

            if (name != null) {
                String msg = name + " " + mContext.getString(R.string.set_expiration_time_to) + timeStr;
                tvSecretLbl.setText(msg);
            }
        }

    }

    private void setDateText(TextView tvDateLbl, Date currentItemDate) {
        Calendar calendar = Calendar.getInstance();
        Date today = TimeStampUtils.getDateFormat(calendar.getTimeInMillis());
        Date yesterday = TimeStampUtils.getYesterdayDate(today);

        if (currentItemDate.equals(today)) {
            tvDateLbl.setText(mContext.getString(R.string.today));
        } else if (currentItemDate.equals(yesterday)) {
            tvDateLbl.setText(mContext.getString(R.string.yesterday));
        } else {
            DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
            String formatDate = df.format(currentItemDate);
            tvDateLbl.setText(formatDate);
        }
    }

    public void updateInfo(ArrayList<MessageItemChat> aitem) {
        this.mListData = aitem;
        notifyDataSetChanged();
    }

    public long getItemId(int position) {
        return position;
    }

    public Filter getFilter() {
        return mFilter;
    }

    public void setfirstItemSelected(boolean isFirstItemSelected) {

        FirstItemSelected = isFirstItemSelected;

    }

    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        int hours = (int) (millis / (1000 * 60 * 60));
        int minutes = (int) ((millis % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (int) (((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000);

        buf.append(String.format(Locale.ENGLISH, "%02d", minutes))
                .append(":")
                .append(String.format(Locale.ENGLISH, "%02d", seconds));

        return buf.toString();
    }

    public String convertDuration(long duration) {
        String out = null;
        long hours = 0;
        try {
            hours = (duration / 3600000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            MyLog.e(TAG, "", e);
            return out;
        }
        long remaining_minutes = (duration - (hours * 3600000)) / 60000;
        String minutes = String.valueOf(remaining_minutes);
        if (minutes.equals(0)) {
            minutes = "00";
        }
        long remaining_seconds = (duration - (hours * 3600000) - (remaining_minutes * 60000));
        String seconds = String.valueOf(remaining_seconds);
        if (seconds.length() < 2) {
            seconds = "00";
        } else {
            seconds = seconds.substring(0, 2);
        }

        if (hours > 0) {
            out = hours + ":" + minutes + ":" + seconds;
        } else {
            out = minutes + ":" + seconds;
        }

        return out;

    }

    public void stopAudioOnMessageDelete(int position) {
        if (position > -1 && position == lastPlayedAt) {
            mTimer.cancel();
            mPlayer.release();
        }
    }

    public void stopAudioOnClearChat() {
        lastPlayedAt = -1;
        mTimer.cancel();
        mPlayer.release();
    }


    private void checKForChatViewNavigation(String msisdn, String contactName, String image, String perID) {
        ChatLockPojo lockPojo = getChatLockdetailfromDB();

        if (SessionManager.getInstance(mContext).getLockChatEnabled().equals("1") && lockPojo != null) {
            String stat = lockPojo.getStatus();
            String pwd = lockPojo.getPassword();
            String documentid = mCurrentUserId + "-" + perID;
            if (stat.equals("1")) {
                openUnlockChatDialog(documentid, stat, pwd, contactName, image, msisdn);
            } else {
                navigateTochatviewpage(contactName, msisdn, image, perID);
            }
        } else {
            navigateTochatviewpage(contactName, msisdn, image, perID);
        }
    }


    private void navigateTochatviewpage(String contactName, String contactNumber, String image, String perSonID) {
        Intent intent = new Intent(mContext, ChatPageActivity.class);
        intent.putExtra("receiverUid", "");
        intent.putExtra("receiverName", "");
        intent.putExtra("documentId", perSonID);
        intent.putExtra("type", 0);
        intent.putExtra("backfrom", true);
        intent.putExtra("Username", contactName);
        intent.putExtra("msisdn", contactNumber);
        intent.putExtra("Image", image);
        mContext.startActivity(intent);
    }


    private ChatLockPojo getChatLockdetailfromDB() {
        String id = mCurrentUserId.concat("-").concat(smarttycontactid);
        MessageDbController dbController = CoreController.getDBInstance(mContext);
        String convId = userInfoSession.getChatConvId(id);
        String receiverId = userInfoSession.getReceiverIdByConvId(convId);
        ChatLockPojo pojo = dbController.getChatLockData(receiverId, MessageFactory.CHAT_TYPE_SINGLE);
        return pojo;
    }


    public void openUnlockChatDialog(String documentid, String stat, String pwd, String contactname, String image, String msisdn) {

        String convId = userInfoSession.getChatConvId(documentid);
        ChatLockPwdDialog dialog = new ChatLockPwdDialog();
        dialog.setTextLabel1("Enter your Password");
        dialog.setEditTextdata("Enter new Password");
        dialog.setforgotpwdlabel("Forgot Password");
        dialog.setHeader("Unlock Chat");
        dialog.setButtonText("Unlock");
        Bundle bundle = new Bundle();
        bundle.putString("convID", convId);
        bundle.putString("status", "1");
        bundle.putString("pwd", pwd);
        bundle.putString("contactName", contactname);
        bundle.putString("avatar", image);
        bundle.putString("msisdn", msisdn);
        String uid = documentid.split("-")[1];
        bundle.putString("docid", uid);
        bundle.putString("page", "chatlist");
        bundle.putString("type", "single");
        bundle.putString("from", mCurrentUserId);
        dialog.setArguments(bundle);
        dialog.show(fragmentManager, "chatunLock");
    }

    public String size(long size) {
        String hrSize = "";
        double k = size / 1024.0;
        double m = size / 1048576.0;
        double g = size / 1073741824.0;
        double t = size / (1073741824.0 * 1024.0);
        DecimalFormat form = new DecimalFormat("0.00");
        if (t > 1) {
            t = round(t, 1);
            hrSize = (t + "").concat(" TB");
        } else if (g > 1) {
            g = round(g, 1);
            hrSize = (g + "").concat(" GB");
        } else if (m > 1) {
            m = round(m, 1);
            hrSize = (m + "").concat(" MB");
        } else if (k > 1) {
            k = round(k, 1);
            hrSize = (k + "").concat(" KB");
        } else {
            hrSize = (size + "").concat(" Bytes");
        }

        return hrSize;
    }

    private void ShowInviteAlertForBoth(final ArrayList<String> detail, String name) {

        final List<MultiTextDialogPojo> labelsList = new ArrayList<>();
        MultiTextDialogPojo pojo = new MultiTextDialogPojo();
        pojo.setLabelText(mContext.getResources().getString(R.string.invite_contacts) + " " + name + " " + "via");
        labelsList.add(pojo);
        for (int i = 0; i < detail.size(); i++) {
            pojo = new MultiTextDialogPojo();
            pojo.setLabelText(detail.get(i));
            labelsList.add(pojo);
        }

        CustomMultiTextItemsDialog dialog = new CustomMultiTextItemsDialog();
        dialog.setLabelsList(labelsList);
        dialog.setDialogItemClickListener(new CustomMultiTextItemsDialog.DialogItemClickListener() {
            @Override
            public void onDialogItemClick(int position) {
                String appname = mContext.getResources().getString(R.string.app_name);
                if (!Getcontactname.isEmailValid(labelsList.get(position).getLabelText())) {
                    SMSintent(labelsList.get(position).getLabelText(), appname);

                } else {
                    performInvite(labelsList.get(position).getLabelText(), appname);

                }

            }


        });
        dialog.show(fragmentManager, "Invite contact");
    }

    private void SMSintent(String labelText, String appname) {
        Intent smsIntent;

        try {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
                /*String defaultApplication = Settings.Secure.getString(mContext.getContentResolver(), "sms_default_application");
                PackageManager pm = mContext.getPackageManager();
                smsIntent = pm.getLaunchIntentForPackage(defaultApplication);*/
                smsIntent = new Intent(Intent.ACTION_SEND);
                String packageName = Telephony.Sms.getDefaultSmsPackage(mContext);
                smsIntent.setPackage(packageName);
                smsIntent.setType("text/plain");
                smsIntent.putExtra("address", labelText);
//                smsIntent.setAction(Intent.ACTION_SEND);
                smsIntent.putExtra(Intent.EXTRA_TEXT, Constants.getAppStoreLink(mContext));
            } else {
                smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address", labelText);
                smsIntent.putExtra("sms_body", Constants.getAppStoreLink(mContext));
            }

            if (smsIntent == null) {
                Toast.makeText(mContext, "No app installed to handle this event", Toast.LENGTH_SHORT).show();
            } else {
                mContext.startActivity(smsIntent);
            }
        } catch (ActivityNotFoundException e) {
            Toast.makeText(mContext, "No app installed to handle this event", Toast.LENGTH_SHORT).show();
        }
    }

    private void performInvite(String labelText, String appname) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_TEXT, Constants.getAppStoreLink(mContext));
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{labelText});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, appname + " : Android");
        mContext.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();

            final ArrayList<MessageItemChat> nlist = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                results.values = mListData;
                results.count = mListData.size();
                //System.out.println("nlist size in if part" + results.count);
            } else {
                for (int i = 0; i < mListData.size(); i++) {
                    MessageItemChat chat_message_item = mListData.get(i);
                    if (chat_message_item.getTextMessage() != null && chat_message_item.getTextMessage().toLowerCase().contains(filterString)) {
                        nlist.add(chat_message_item);
                    }
                }
                results.values = nlist;
                results.count = nlist.size();
                //System.out.println("nlist size in else part" + results.count);
            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mListData = (ArrayList<MessageItemChat>) results.values;
            notifyDataSetChanged();
               /* ArrayList<MessageItemChat> filtered = (ArrayList<MessageItemChat>) results.values;
                notifyDataSetChanged();*/
        }
    }
}

