package com.app.Smarttys.app.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.At_InfoAdapter;
import com.app.Smarttys.app.adapter.ChatMessageAdapter;
import com.app.Smarttys.app.calls.CallMessage;
import com.app.Smarttys.app.calls.CallsActivity;
import com.app.Smarttys.app.dialog.ChatLockPwdDialog;
import com.app.Smarttys.app.dialog.CustomAlertClearDialog;
import com.app.Smarttys.app.dialog.CustomAlertDialog;
import com.app.Smarttys.app.dialog.CustomDeleteDialog;
import com.app.Smarttys.app.dialog.CustomMultiTextItemsDialog;
import com.app.Smarttys.app.dialog.MuteAlertDialog;
import com.app.Smarttys.app.model.GroupMemberFetched;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.BlockUserUtils;
import com.app.Smarttys.app.utils.CommonData;
import com.app.Smarttys.app.utils.ConnectivityInfo;
import com.app.Smarttys.app.utils.EmailChatHistoryUtils;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.GroupInfoSession;
import com.app.Smarttys.app.utils.MuteUnmute;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.SharedPreference;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.app.widget.AvnNextLTProRegEditText;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.ShortcutBadgeManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.AudioMessage;
import com.app.Smarttys.core.message.ContactMessage;
import com.app.Smarttys.core.message.DocumentMessage;
import com.app.Smarttys.core.message.GroupEventInfoMessage;
import com.app.Smarttys.core.message.IncomingMessage;
import com.app.Smarttys.core.message.LocationMessage;
import com.app.Smarttys.core.message.MessageAck;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.message.PictureMessage;
import com.app.Smarttys.core.message.TextMessage;
import com.app.Smarttys.core.message.VideoMessage;
import com.app.Smarttys.core.message.WebLinkMessage;
import com.app.Smarttys.core.model.ChatLockPojo;
import com.app.Smarttys.core.model.ContactToSend;
import com.app.Smarttys.core.model.GroupInfoPojo;
import com.app.Smarttys.core.model.GroupMembersPojo;
import com.app.Smarttys.core.model.Imagepath_caption;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.MultiTextDialogPojo;
import com.app.Smarttys.core.model.MuteStatusPojo;
import com.app.Smarttys.core.model.MuteUserPojo;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.service.ContactsSync;
import com.app.Smarttys.core.smarttyhelperclass.SmarttyImageUtils;
import com.app.Smarttys.core.smarttyhelperclass.SmarttyRegularExp;
import com.app.Smarttys.core.smarttyhelperclass.SmarttyUtilities;
import com.app.Smarttys.core.socket.MessageService;
import com.app.Smarttys.core.socket.NotificationUtil;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.core.uploadtoserver.FetchDownloadManager;
import com.app.Smarttys.core.uploadtoserver.FileDownloadListener;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;
import com.bumptech.glide.BitmapTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import org.appspot.apprtc.CallActivity;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import droidninja.filepicker.models.sort.SortingTypes;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import io.codetail.animation.SupportAnimator;
import me.leolin.shortcutbadger.ShortcutBadger;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ChatPageActivity extends CoreActivity implements View.OnClickListener, AdapterView.OnItemLongClickListener, View.OnLongClickListener, MuteAlertDialog.MuteAlertCloseListener,
        AdapterView.OnItemClickListener, ItemClickListener, ScreenShotDetector.ScreenShotListener, FileDownloadListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final String INTENT_KEY_FINISH_ACTIVITY_ON_SAVE_COMPLETED = "finishActivityOnSaveCompleted";
    public static final String HTML_FRONT_TAG = "<font color=\"#01a9e5\">";
    public static final String HTML_END_TAG = "</font>  ";
    private static final int RESULT_LOAD_IMAGE = 1;
    private static final int RESULT_LOAD_VIDEO = 2;
    private static final int RESULT_CAPTURE_VIDEO = 3;
    private static final int REQUEST_CODE_CONTACTS = 4;
    private static final int RESULT_SHARE_LOCATION = 5;
    private static final int CHECKING_LOCATION = 6;
    private static final int REQUEST_SELECT_AUDIO = 7;
    private static final int CAMERA_REQUEST = 1888;
    private static final int RESULT_WALLPAPER = 8;
    private static final int REQUEST_CODE_DOCUMENT = 9;
    private static final int PICK_CONTACT_REQUEST = 10;
    private static final String TAG = ChatPageActivity.class.getSimpleName() + ">>>@@@@@@";
    public static String LAST_SEEN_TEXT = "last seen ";
    public static Activity Chat_Activity;
    public static String to;
    public static String Chat_to;
    public static boolean isGroupChat = false;
    public static boolean isChatPage, isKilled;
    public static int progressglobal = 0;
    public static ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
    public static List<GroupMembersPojo> allMembersList;
    public static List<GroupMembersPojo> at_memberlist = new ArrayList<GroupMembersPojo>();
    public static String Activity_GroupId = "";
    public static boolean isFirstItemSelected = false;
    public static String receiverAvatar;
    final Context context = this;
    final Handler handler = new Handler(Looper.getMainLooper());
    private final int EXIT_GROUP_REQUEST_CODE = 11;
    private final int AUDIO_RECORD_PERMISSION_REQUEST_CODE = 14;
    private final int MUTE_ACTIVITY = 18;
    private final int ADD_CONTACT = 21;
    private final int REQUEST_CODE_FORWARD_MSG = 15;
    public Boolean reply = false, isSelectedWithUnStarMsg;
    public boolean hasGroupInfo;
    public double latitude;
    public double longitude;
    //    public LocationManager locationManager;
    public Criteria criteria;
    public String bestProvider;
    boolean mContactSaved;
    boolean mListviewScrollBottom;
    int lastvisibleitempostion = 0;
    int unreadmsgcount = 0;
    String Message_id = "";
    String Group_Message_id = "";
    Getcontactname getcontactname;
    String convId, docId = "", mFirstVisibleMsgId;
    String chatType, mypath, audioRecordPath;
    String receiverUid;
    boolean backfrom, canShowLastSeen, isLastSeenCalled;
    GroupMembersPojo mCurrentUserData;
    int totla_progress = 3;
    Session session;
    ArrayList<ContactToSend> contacts;
    ShortcutBadgeManager sharedprf_video_uploadprogress;
    long fromLastTypedAt = 0, toLastTypedAt = 0, lastViewStatusSentAt = 0;
    Handler toTypingHandler = new Handler(Looper.getMainLooper());
    String ContactString;
    String name, number;
    String date;
    RelativeLayout r1messagetoreplay, RelativeLayout_group_delete, text_lay_out, rlSend;
    ImageView cameraphoto, videoimage, audioimage, personimage;
    ImageView cameraimage, sentimage;
    String replytype;
    String value, ReplySender = "";
    String messageold = "", mDataId, mRawContactId;
    boolean isAlrdychatlocked = false;
    ImageView emai1send, gmailsend;
    Bitmap myTemp = null;
    String emailChatlock, recemailChatlock, recPhoneChatlock;
    ContactDB_Sqlite contactDB_sqlite;
    Chronometer myChronometer;
    ImageView image_to;
    int avoid_twotimescall = 0;
    TextView slidetocencel;
    View rootView;
    String apiKey = "AIzaSyAXBjfzFWhK0pu-6bTl5NuSbqKsistosfU";

    EmojIconActions emojIcon;
    String imgDecodableString;
    AvnNextLTProRegEditText Search1;
    TextView add_contact, block_contact, report_spam;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    TextView unreadcount;
    int setscrolllastpoition = 0;
    StringBuilder sb;
    SoftKeyboard softKeyboard;
    Receiver receiver;
    boolean isMassChat;
    boolean is_telpon_chat;
    //    int REQUEST_LOCATION_PERMISSIONS = 1003;
    GoogleApiClient googleApiClient;
    LocationManager locationManager;
    LocationRequest locationRequest;
    LocationSettingsRequest.Builder locationSettingsRequest;
    PendingResult<LocationSettingsResult> pendingResult;
    Intent intentThatCalled;
    Imagepath_caption imagepath_caption;
    private String mSelectedPath = null;
    private String mSelectedType = null;
    private boolean isGroupMemebersNotLoaded = false;
    private boolean noNeedRefresh = false;
    private LinearLayout mLnrcallStatus;
    private ListView chat_list;
    private String Delete_Type = "";
    private MessageDbController db;
    private String from;
    private ArrayList<MessageItemChat> mChatData;
    // private NewLayoutChangeChatAdapter madapter;
    private SessionManager sessionManager;
    private String receiverMsisdn;
    private String mReceiverName = "";
    private String msgid;
    private String receiverName;
    private String mGroupId, mCurrentUserId;
    private UserInfoSession userInfoSession;
    private String contactname, mConvId = "";
    private FileUploadDownloadManager uploadDownloadManager;
    private ImageView attachment_icon;
    private boolean isHidden = true;
    private LinearLayout attachmentLayout;
    private LinearLayout image_choose;
    private LinearLayout video_choose;
    private LinearLayout audio_choose;
    private LinearLayout document_choose;
    private LinearLayout location_choose;
    private LinearLayout contact_choose;
    private Toolbar include;
    private ImageView mBackImage;
    private String mReceiverId;
    private boolean enableGroupChat, gmailintent = false, isMenuBtnClick;
    private RelativeLayout layout_new;
    private long longPressMillis = 0;
    private At_InfoAdapter atAdapter;
    private ImageView capture_image, selKeybord, ivVoiceCall, ivVideoCall;
    private ImageButton sendButton;
    private TextView groupUsername, tvWebLink, tvWebLinkDesc, tvWebTitle, message_old;
    private ImageView Documentimage;
    private TextView dateView, Ifname, messagesetmedio, tvBlock, tvAddToContact;
    private String mymsgid = "", locationName;
    private String imgpath;
    private String imageAsBytes;
    private ImageView close;
    private EmojiconEditText sendMessage;
    private boolean deletestarred = false, audioRecordStarted = false, mClearChat;
    private boolean mCheckedDelete;
//    Context context;
    private RelativeLayout emailgmail;
    private ImageButton record;
    private GroupInfoSession groupInfoSession;
    private List<GroupMembersPojo> savedMembersList, unsavedMembersList;
    private RecyclerView rvGroupMembers;
    TextWatcher watch = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable arg0) {

            //checkLinkDetails(sendMessage.getText().toString().trim());
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence cs, int a, int b, int c) {
            // TODO Auto-generated method stub
            MyLog.d(TAG, "onTextChanged: ");
            handleTypingEvent(cs.length());
            if (cs.length() > 0) {
                hideMenu();
                if (isGroupChat) {
                    if (cs.length() == 1) {
                        String value = cs.toString();
                        if (value.equalsIgnoreCase("@")) {
                            if (allMembersList.size() > 0) {
                                rvGroupMembers.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        String value = cs.toString();
                        String[] splittedText = value.split(" ");
                        String value1 = value;
                        if (splittedText.length > 0)
                            value1 = splittedText[splittedText.length - 1];
                        MyLog.d(TAG, "onTextChanged: " + value1);
                        if (value1.contains(" @")) {
                            boolean haveMembersText = false;
                            for (GroupMembersPojo groupMembersPojo : allMembersList) {
                                String name = groupMembersPojo.getName();
                                String number = groupMembersPojo.getMsisdn();
                                if (name != null && name.contains(cs)) {
                                    haveMembersText = true;
                                    break;
                                } else if (number != null && number.contains(cs)) {
                                    haveMembersText = true;
                                    break;
                                }
                            }
                            if (haveMembersText)
                                rvGroupMembers.setVisibility(View.VISIBLE);
                            else
                                rvGroupMembers.setVisibility(View.GONE);
                        }
                    }
                }
                capture_image.setVisibility(View.GONE);
                attachment_icon.setVisibility(View.GONE);
                if (capture_image.getVisibility() == View.GONE) {
                }
                sendButton.setVisibility(View.VISIBLE);
                record.setVisibility(View.GONE);
            } else {
                if (isGroupChat) {
//                    AppUtils.slideDown(rvGroupMembers);
                    rvGroupMembers.setVisibility(View.GONE);
//                    rvGroupMembers.setVisibility(View.GONE);
                }
                capture_image.setVisibility(View.VISIBLE);
                attachment_icon.setVisibility(View.VISIBLE);
                sendButton.setVisibility(View.GONE);
                record.setVisibility(View.VISIBLE);
            }

        }
    };
    private MyReceiver myReceiver = null;
    //    AvnNextLTProDemiTextView Send_photo;
    private Uri cameraImageUri;
    private int membersCount;
    private String webLink, webLinkTitle, webLinkDesc, webLinkImgUrl;
    private ImageView ivWebLink;
    private RelativeLayout rlWebLink;
    private boolean hasLinkInfo;
    private MediaRecorder audioRecorder;
    private ImageView selEmoji;
    private LinearLayout llAddBlockContact;
    private ImageView background;
    private Menu chatMenu;
    private String Badge_count_id = "";
    private boolean loadingMore = false;
    private LinearLayoutManager mLayoutManager;
    private ImageButton iBtnScroll;
    private GPSTracker gpsTracker;

    private boolean isMessageLoadedOnScroll = true;
    private Animation loaderAnimation;
    private FrameLayout frameL;
    private String starred_msgid = "";
    //--------------------------Header Intialize------------------------
    private ArrayList<String> newMsgIds = new ArrayList<>();
    private LinearLayout relativeLayout;
    private RelativeLayout group_left_layout;
    // private RelativeLayout back_layout;
    private CircleImageView user_profile_image;
    private TextView user_name;
    private TextView status_textview;
    Runnable toTypingRunnable = new Runnable() {
        @Override
        public void run() {
            long currentTime = Calendar.getInstance().getTimeInMillis();
            if (currentTime > toLastTypedAt) {

                if (!isGroupChat) {

                    status_textview.setText("");
                    status_textview.setText("Online");
                    status_textview.setVisibility(View.VISIBLE);

                } else {
                    status_textview.setText("");
                    if (sb != null) {
                        status_textview.setText(sb);
                    }

                }


            }
        }
    };
    /*  private RippleView Ripple_Video;
      private RippleView Ripple_call;
  */
    //------------------------Chat Action Intialize-------------------------
    private ImageView overflow;
    private RelativeLayout name_status_layout;
    private RelativeLayout menu_layout;
    private RelativeLayout call_layout;
    private RelativeLayout rlChatActions;
    private ImageView iBtnBack2;
    private ImageView longpressback;
    private ImageView replymess;
    private ImageView copychat;
    private ImageView starred;
    private ImageView info;
    private ImageView delete;

    private boolean isFirstTimeLoad = true;
    private ImageView forward, share;

    //------------------------------New Record Button---------------------------

    //  private RecordButton record_button;
    // private RecordView record_view;
    private List<String> messageIds = new ArrayList<>();
    private RelativeLayout edit_text_layout;
    private ChatMessageAdapter madapter;
    private View.OnTouchListener audioRecordTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View pView, MotionEvent pEvent) {
            pView.onTouchEvent(pEvent);

            if (pEvent.getAction() == MotionEvent.ACTION_UP) {
                try {

                    if (audioRecordStarted) {
                        String sendAudioPath = audioRecordPath;
                        audioRecordPath = "";
                        record.setBackground(ContextCompat.getDrawable(context, R.drawable.circle_primary_icon));
                        record.setImageResource(R.drawable.recv_ic_mic_white);
                        selEmoji.setImageResource(R.drawable.smile);
                        sendMessage.setVisibility(View.VISIBLE);
                        myChronometer.setVisibility(View.GONE);
                        image_to.setVisibility(View.GONE);
                        slidetocencel.setVisibility(View.GONE);
                        capture_image.setVisibility(View.VISIBLE);
                        attachment_icon.setVisibility(View.VISIBLE);
//                    audioRecorder.stop();
                        audioRecorder.release();
                        myChronometer.stop();

                        File file = new File(sendAudioPath);
                        if (file.exists()) {
                            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                            mmr.setDataSource(ChatPageActivity.this, Uri.parse(sendAudioPath));
                            String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                            //durationStr = getTimeString(Long.parseLong(durationStr));
                            //durationStr = ""+Long.parseLong(durationStr);
                            if (AppUtils.parseLong(durationStr) < 1000) {
                                Toast.makeText(ChatPageActivity.this, "Record atleast one second!", Toast.LENGTH_SHORT).show();
                            } else
                                showAudioRecordSentAlert(sendAudioPath, durationStr);
                        }
                    }
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
                audioRecordStarted = false;
                selEmoji.setEnabled(true);
            }
            return false;
        }
    };
    private Comparator<MessageItemChat> msgComparator = new Comparator<MessageItemChat>() {

        @Override
        public int compare(MessageItemChat lhs, MessageItemChat rhs) {

            Long lhsMsgTS = AppUtils.parseLong(lhs.getTS());
            Long rhsMsgTS = AppUtils.parseLong(rhs.getTS());

            return lhsMsgTS.compareTo(rhsMsgTS);
/*            if (lhsMsgTS > rhsMsgTS) {
                return 1;
            } else {
                return -1;
            }*/
        }
    };
    private boolean isAckNotSend;
    //    private AdView bottom_adView;
    private RelativeLayout bottom_ad_layout;
    private ImageView bottom_ad_close_icon;

    private int Chat_Received_Count = 0;
    private int requestedMemebers = 0;


   /* private void MobileAdintegration() {

        try {

            bottom_adView = findViewById(R.id.bottom_adView);
            bottom_ad_layout = findViewById(R.id.bottom_ad_layout);
            bottom_ad_close_icon = findViewById(R.id.bottom_ad_close_icon);

//            MobileAds.initialize(this, Constants.Ad_Key);


            bottom_adView.setAdListener(new AdListener() {

                @Override
                public void onAdLoaded() {
                    bottom_ad_layout.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {


                }

                @Override
                public void onAdOpened() {

                }

                @Override
                public void onAdClosed() {

                }

                @Override
                public void onAdLeftApplication() {

                }
            });


           *//* AdRequest adRequest = new AdRequest.Builder().build();
            bottom_adView.loadAd(adRequest);*//*

            bottom_ad_close_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    bottom_adView.removeAllViews();
                    bottom_ad_layout.setVisibility(View.GONE);
                }
            });

        } catch (Exception e) {

            Log.e("Exception", e.toString());
        }
    }*/

    public static boolean checkPlayServices(Context context) {
        final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int resultCode = api.isGooglePlayServicesAvailable(context);


        switch (resultCode) {
            case ConnectionResult.API_UNAVAILABLE:
                //API is not available

                break;
            case ConnectionResult.NETWORK_ERROR:
                //Network error while connection

                break;
            case ConnectionResult.RESTRICTED_PROFILE:
                //Profile is restricted by google so can not be used for play services

                break;
            case ConnectionResult.SERVICE_MISSING:
                //service is missing

                break;
            case ConnectionResult.SIGN_IN_REQUIRED:
                //service available but user not signed in

                break;
            case ConnectionResult.SUCCESS:
                //service SUCCESS but  signed in

                break;
        }

        if (resultCode != ConnectionResult.SUCCESS) {
            if (api.isUserResolvableError(resultCode)) {
                api.getErrorDialog(((Activity) context), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(context, context.getString(R.string.devicenotsupport), Toast.LENGTH_SHORT).show();
                ((Activity) context).finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nlc_new_chat_layout_activity);
        MyLog.d(TAG, "onCreate: chatmove");
        Chat_Activity = ChatPageActivity.this;
        isGroupChat = false;
        isFirstItemSelected = false;
        isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
        is_telpon_chat = getResources().getBoolean(R.bool.is_telpon_chat);
        imagepath_caption = new Imagepath_caption();
        //getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.whatsapp_background));

//        MobileAdintegration();
        initializeactivity();
    }

    private void initializeactivity() {

        AppUtils.startService(this, ScreenShotDetector.class);
        // startService(new Intent(this, ScreenShotDetector.class));
        FetchDownloadManager.getInstance().normalChatListener(this);

        sessionManager = SessionManager.getInstance(ChatPageActivity.this);
        sharedprf_video_uploadprogress = new ShortcutBadgeManager(ChatPageActivity.this);
        contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        mChatData = new ArrayList<>();
        uploadDownloadManager = new FileUploadDownloadManager(ChatPageActivity.this);
        userInfoSession = new UserInfoSession(ChatPageActivity.this);
        getcontactname = new Getcontactname(ChatPageActivity.this);
        db = CoreController.getDBInstance(ChatPageActivity.this);
        session = new Session(ChatPageActivity.this);
        groupInfoSession = new GroupInfoSession(ChatPageActivity.this);
        savedMembersList = new ArrayList<>();
        unsavedMembersList = new ArrayList<>();
        allMembersList = new ArrayList<>();
        include = findViewById(R.id.chatheaderinclude);
        setSupportActionBar(include);
        mBackImage = findViewById(R.id.back_image);
        chat_list = findViewById(R.id.chat_list);
        layout_new = findViewById(R.id.layout_new);
        mLnrcallStatus = findViewById(R.id.lnrcallStatus);
        user_name = include.findViewById(R.id.usernamechatsceen);
        r1messagetoreplay = findViewById(R.id.r1messagetoreplay);
        videoimage = findViewById(R.id.videoimage);
        cameraphoto = findViewById(R.id.cameraphoto);
        audioimage = findViewById(R.id.audioimage);
        personimage = findViewById(R.id.personimage);
        Documentimage = findViewById(R.id.Documentimage);
        sentimage = findViewById(R.id.sentimage);
        messagesetmedio = findViewById(R.id.messagesetmedio);
        message_old = findViewById(R.id.message);
        message_old.setTextColor(getResources().getColor(R.color.title));
        Ifname = findViewById(R.id.Ifname);
        close = findViewById(R.id.close);
        sendMessage = findViewById(R.id.chat_edit_text1);
        overflow = findViewById(R.id.overflow);
        emailgmail = findViewById(R.id.email_gmail);
        emai1send = findViewById(R.id.emai1send);
        gmailsend = findViewById(R.id.gmailsend);
        tvBlock = findViewById(R.id.tvBlock);
        tvAddToContact = findViewById(R.id.tvAddToContact);
        block_contact = findViewById(R.id.block_contact);
        rvGroupMembers = findViewById(R.id.rvGroupMembers);
        ivWebLink = findViewById(R.id.ivWebLink);
        tvWebTitle = findViewById(R.id.tvWebTitle);
        tvWebLink = findViewById(R.id.tvWebLink);
        rlWebLink = findViewById(R.id.rlWebLink);
        sendButton = findViewById(R.id.enter_chat1);
        record = findViewById(R.id.record);
        myChronometer = findViewById(R.id.chronometer);
        image_to = findViewById(R.id.image_to);
        slidetocencel = findViewById(R.id.slidetocencel);
        selEmoji = findViewById(R.id.emojiButton);
        rootView = findViewById(R.id.mainRelativeLayout);
        emojIcon = new EmojIconActions(this, rootView, sendMessage, selEmoji);
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smile);
        sendMessage.setVisibility(View.GONE);
        llAddBlockContact = findViewById(R.id.llAddBlockContact);
        background = findViewById(R.id.background);
        Search1 = findViewById(R.id.etSearch);
        add_contact = findViewById(R.id.add_contact);
        block_contact = findViewById(R.id.block_contact);
        report_spam = findViewById(R.id.report_spam);
        iBtnScroll = findViewById(R.id.iBtnScroll);
        unreadcount = findViewById(R.id.unreadcount);
        frameL = findViewById(R.id.frame);

        RelativeLayout_group_delete = findViewById(R.id.RelativeLayout_group_delete);


        relativeLayout = findViewById(R.id.bottomlayout);

        rlSend = findViewById(R.id.rlSend);
        group_left_layout = findViewById(R.id.group_left_layout);

        //------------attachment Intialize----------------------------

        attachment_icon = findViewById(R.id.attachment_icon);
        attachmentLayout = findViewById(R.id.menu_attachments);
        image_choose = findViewById(R.id.image_choose);
        video_choose = findViewById(R.id.video_choose);
        audio_choose = findViewById(R.id.audio_choose);
        document_choose = findViewById(R.id.document_choose);
        location_choose = findViewById(R.id.location_choose);
        contact_choose = findViewById(R.id.contact_choose);

        //---------------------EditText Inside Send Layout Intialize--------------------

        capture_image = findViewById(R.id.capture_image);
        from = sessionManager.getCurrentUserID();
        mCurrentUserId = sessionManager.getCurrentUserID();

        //---------------------Header Initialize--------------------------------
//
        //   back_layout = findViewById(R.id.back_layout);
        user_profile_image = findViewById(R.id.user_profile_image);
        user_name = findViewById(R.id.user_name);
        status_textview = findViewById(R.id.status_textview);
      /*  Ripple_Video = (RippleView) findViewById(R.id.Ripple_Video);
        Ripple_call = (RippleView) findViewById(R.id.Ripple_call);
     */
        ivVideoCall = findViewById(R.id.ivVideoCall);
        ivVoiceCall = findViewById(R.id.ivVoiceCall);

        menu_layout = findViewById(R.id.menu_layout);
        call_layout = findViewById(R.id.call_layout);
        name_status_layout = findViewById(R.id.name_status_layout);

        //-------------------------Chat Action Intialize--------------------------

        rlChatActions = findViewById(R.id.rlChatActions);
        iBtnBack2 = findViewById(R.id.iBtnBack2);
        replymess = findViewById(R.id.replymess);
        copychat = findViewById(R.id.copychat);
        starred = findViewById(R.id.starred);
        info = findViewById(R.id.info);
        delete = findViewById(R.id.delete);
        forward = findViewById(R.id.forward);
        longpressback = findViewById(R.id.iBtnBack3);
        share = findViewById(R.id.share);


        rlChatActions.setVisibility(View.GONE);
        include.setVisibility(View.VISIBLE);

        sendMessage.addTextChangedListener(watch);

        receiver = new ChatPageActivity.Receiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.groupname.change");
        filter.addAction("com.groupprofile.update");
        filter.addAction("com.group.delete.members");
        filter.addAction("com.group.makeadmin");
        registerReceiver(receiver, filter);
        MyLog.d(TAG, "initializeactivity: chatmove");
        getintent();

        //loadFromDB();
        checkPlayServices(context);

        OnclickMethod();
        checkAndOpenChatUnlockDialog();
        OnItemScrollListener();

        initAtAdapter();
        MyLog.d(TAG, "initializeactivity: chatmove");
        loaderAnimation = AnimationUtils.loadAnimation(ChatPageActivity.this, R.anim.fast_fade_out);
        //use this to make it longer:  animation.setDuration(1000);
        loaderAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                frameL.setVisibility(View.GONE);

            }
        });
//call this method to create new object
        emojIcon.ShowEmojIcon();
        MyLog.d(TAG, "initializeactivity end: chatmove ");

    }

    private void getGroupDetails() {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_GROUP_DETAILS);

        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            object.put("convId", mGroupId);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }


    //--------------------------------------------New Contact Added-----------------------------------------------

    private void initAtAdapter() {
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(RecyclerView.VERTICAL);
        rvGroupMembers.setLayoutManager(llm);
        atAdapter = new At_InfoAdapter(ChatPageActivity.this, allMembersList);
        rvGroupMembers.setAdapter(atAdapter);
        if (isGroupChat) {
            getGroupDetails();
        }

        atAdapter.setChatListItemClickListener(new At_InfoAdapter.AtInfoAdapterItemClickListener() {
            @Override
            public void onItemClick(GroupMembersPojo member, int position) {

                MyLog.d(TAG, "onItemClick: " + position);
                at_memberlist.add(member);
                sendMessage.append(AppUtils.getHtmlText("" + HTML_FRONT_TAG + member.getContactName() + HTML_END_TAG));
                rvGroupMembers.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        rvGroupMembers.setVisibility(View.GONE);

                    }
                }, 200);
                //AppUtils.slideDown(rvGroupMembers);
            }
        });
    }

    private void OnItemScrollListener() {

        chat_list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                madapter.setConvertviewBoolean(true);
                int threshold = 1;
                int count = chat_list.getCount();
                ArrayList<MessageItemChat> items = new ArrayList<>();
                int lastscrollposition = 0;


                if (scrollState == SCROLL_STATE_IDLE) {
                    try {

                        visibleItemCount = chat_list.getChildCount();
                        totalItemCount = chat_list.getCount();
                        pastVisiblesItems = chat_list.getFirstVisiblePosition();
                        lastvisibleitempostion = chat_list.getLastVisiblePosition();

                        if (pastVisiblesItems > -1 && lastvisibleitempostion > -1) {
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                iBtnScroll.setVisibility(View.GONE);
                                unreadcount.setVisibility(View.GONE);
                                unreadmsgcount = 0;
                                changeBadgeCount(mConvId);
                            } else {
                                iBtnScroll.setVisibility(View.VISIBLE);
                            }
                        }

                        //    MessageItemChat scrolledMsgItem = mChatData.get(pastVisiblesItems);
                        //  final MessageItemChat lastMsgItem = mChatData.get(lastvisibleitempostion);
                        //  String scrolledMsgId = scrolledMsgItem.getMessageId();

                        if (chat_list.getLastVisiblePosition() >= count - threshold) {
                            Log.e(TAG, "finalItems: chat_list.getLastVisiblePosition() >= count - threshold");

                            int lastposition = chat_list.getLastVisiblePosition();
                            //System.out.println("LastPosition" + " " + " " + lastposition);


                            // down scroll
                            String tss = mChatData.get(lastposition).getTS();
                            items = db.selectAllMessagesWithLimit_again(docId, chatType,
                                    tss, MessageDbController.MESSAGE_PAGE_LOADED_LIMIT);
                            Collections.sort(items, msgComparator);
                            int previousSize = mChatData.size();

                            if (items.size() > 0) {

                                Collections.sort(items, msgComparator);

                                for (int i = 0; i < items.size(); i++) {
                                    mChatData.add(previousSize + i, items.get(i));
                                }
                                //   madapter.notifyDataSetChanged();
                                Log.e(TAG, "finalItems: notifyDataSetChanged" + mListviewScrollBottom);
                                //    madapter.updateInfo(mChatData);

                                if (mListviewScrollBottom) {
                                    madapter.updateInfo(mChatData);
                                }
                            }


                        } else if (chat_list.getFirstVisiblePosition() == 0 && chat_list.getChildAt(0).getTop() == 0) {
                            Log.e(TAG, "finalItems: chat_list.getFirstVisiblePosition() == 0  chat_list.getChildAt(0).getTop() == 0");

                            int first_pos = chat_list.getFirstVisiblePosition();

                            //System.out.println("FirstPosition" + " " + " " + first_pos);


                            String ts = mChatData.get(first_pos).getTS();
                            // up scroll
                            items = db.selectAllMessagesWithLimit(docId, chatType,
                                    ts, MessageDbController.MESSAGE_PAGE_LOADED_LIMIT);

                            Collections.sort(items, msgComparator);

                            if (items.size() > 0) {

                                lastscrollposition = items.size() - 1;
                                setscrolllastpoition = lastscrollposition - 1;

                                for (int i = 0; i < items.size(); i++) {
                                    if (!isAlreadyExist(items.get(i)))
                                        mChatData.add(i, items.get(i));
                                }

                                final ArrayList<MessageItemChat> finalItems = items;
                                chat_list.post(new Runnable() {
                                    @Override
                                    public void run() {

                                        if (finalItems.size() > 0)
                                            Log.e(TAG, "finalItems: notifyDataSetChanged" + mListviewScrollBottom);
//Commented to fix the issues
                                        if (mListviewScrollBottom) {
                                            madapter.updateInfo(mChatData);
                                        }

                                        //       madapter.notifyDataSetChanged();

                                    }
                                });
                            }


                        }
                    } catch (Exception e) {
                        MyLog.e(TAG, "onScrollStateChanged: ", e);
                    }
                }


                if (chat_list.getLastVisiblePosition() == chat_list.getAdapter().getCount() - 1 &&
                        chat_list.getChildAt(chat_list.getChildCount() - 1).getBottom() <= chat_list.getHeight()) {
                    //It is scrolled all the way down here
                    Log.e(TAG, "mListviewScrollBottom It is scrolled all the way down here: ");

                    mListviewScrollBottom = false;

                }
                if (chat_list.getFirstVisiblePosition() == 0 &&
                        chat_list.getChildAt(0).getTop() >= 0) {
                    //It is scrolled all the way up here
                    mListviewScrollBottom = false;
                    Log.e(TAG, "mListviewScrollBottom It is scrolled all the way up here: ");

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (chat_list.getChildAt(0) != null) {

                    //  refresh_suggestion_list.setEnabled(rv_newsfeed_list.getFirstVisiblePosition() == 0 && rv_newsfeed_list.getChildAt(0).getTop() == 0);
                }

                final int lastItem = firstVisibleItem
                        + visibleItemCount;

                if ((lastItem == totalItemCount)
                        & mListviewScrollBottom == false) {

//this to prevent the list to make more than one request in time
                    mListviewScrollBottom = true;
                    //you can put the index for your next page here
                    //  loadMore(int page);

                    Log.e(TAG, "mListviewScrollBottom onScroll all the way up here: ");

                }
            }
        });

    }

    //----------------------------------------Report Spam---------------------------------------------

    private void OnclickMethod() {

        attachment_icon.setOnClickListener(this);
        image_choose.setOnClickListener(this);
        video_choose.setOnClickListener(this);
        audio_choose.setOnClickListener(this);
        document_choose.setOnClickListener(this);
        location_choose.setOnClickListener(this);
        contact_choose.setOnClickListener(this);
        capture_image.setOnClickListener(this);
        mBackImage.setOnClickListener(this);
        ivVideoCall.setOnClickListener(this);
        ivVoiceCall.setOnClickListener(this);
        mLnrcallStatus.setOnClickListener(this);
        //     Ripple_Video.setOnClickListener(this);
        //     Ripple_call.setOnClickListener(this);
        chat_list.setOnItemLongClickListener(this);
        chat_list.setOnItemClickListener(this);
        close.setOnClickListener(this);
        iBtnBack2.setOnClickListener(this);
        replymess.setOnClickListener(this);
        copychat.setOnClickListener(this);
        starred.setOnClickListener(this);
        delete.setOnClickListener(this);
        forward.setOnClickListener(this);
        share.setOnClickListener(this);
        overflow.setOnClickListener(this);
        longpressback.setOnClickListener(this);
        sendButton.setOnClickListener(this);
        name_status_layout.setOnClickListener(this);
        menu_layout.setOnClickListener(this);
        selEmoji.setOnClickListener(this);
        tvBlock.setOnClickListener(this);
        tvAddToContact.setOnClickListener(this);
        iBtnScroll.setOnClickListener(this);

        add_contact.setOnClickListener(this);
        block_contact.setOnClickListener(this);
        report_spam.setOnClickListener(this);

        record.setOnLongClickListener(this);
        record.setOnTouchListener(audioRecordTouchListener);


    }

    @Override
    public void onBackPressed() {
        ChatMessageAdapter.lastPlayedAt = -1;

        Chat_Activity = null;
        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskList = null;
        if (mngr != null) {
            taskList = mngr.getRunningTasks(10);
        } else {
            return;
        }
        int backstackActivitiesCount = taskList.get(0).numActivities;
        MyLog.d(TAG, "onBackPressed: " + backstackActivitiesCount);
        if (backstackActivitiesCount == 1 && taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            MyLog.e(TAG, "This is last activity in the stackkkk");
            Intent intent = new Intent(ChatPageActivity.this, NewHomeScreenActivty.class);
            startActivity(intent);
        } else if (backstackActivitiesCount == 0) {
            MyLog.e(TAG, "There is no activity in the stack");
        } else if (backstackActivitiesCount == 1) {
            Intent intent = new Intent(ChatPageActivity.this, NewHomeScreenActivty.class);
            startActivity(intent);
        } else if (backstackActivitiesCount == 3 || backstackActivitiesCount > 3) {
            Intent intent = new Intent(ChatPageActivity.this, NewHomeScreenActivty.class);
            startActivity(intent);
        }
        showUnSelectedActions();
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.lnrcallStatus:
                Redirecttocall();
                break;
            case R.id.back_image:
                onBackPressed();
                break;

            case R.id.attachment_icon:

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    showMenuBelowLollipop();
                else
                    showMenu();

                break;

            case R.id.image_choose:
                hideMenu();
                Intent i = new Intent(ChatPageActivity.this, ImagecaptionActivity.class);
                i.putExtra("phoneno", mReceiverName);
                i.putExtra("from", "Gallary");
                startActivityForResult(i, RESULT_LOAD_IMAGE);

                break;

            case R.id.video_choose:
                hideMenu();
                Intent video = new Intent(ChatPageActivity.this, ImagecaptionActivity.class);
                video.putExtra("phoneno", mReceiverName);
                video.putExtra("from", "Video");
                video.putExtra("group", "value");
                startActivityForResult(video, RESULT_LOAD_VIDEO);
                break;

            case R.id.audio_choose:
                hideMenu();
                Intent audioIntent = new Intent(ChatPageActivity.this, AudioFilesListActivity.class);
                startActivityForResult(audioIntent, REQUEST_SELECT_AUDIO);
                break;

            case R.id.document_choose:

                hideMenu();
                FilePickerBuilder.Companion.getInstance().setMaxCount(1)
                        .setSelectedFiles(new ArrayList<String>())
                        //.setActivityTheme(R.style.AppTheme)
                        .sortDocumentsBy(SortingTypes.name)
                        .pickFile(ChatPageActivity.this);

                break;

            case R.id.location_choose:

                hideMenu();
                if (ActivityCompat.checkSelfPermission(ChatPageActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ChatPageActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                        ActivityCompat.requestPermissions(ChatPageActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 11);
                    } else {
                        System.out.println("print: ask permission");
                        ActivityCompat.requestPermissions(ChatPageActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 11);
                    }
                } else {
                    mEnableGps();
                    checkLocationEnabled();
                }
               /* PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(ChatPageActivity.this), RESULT_SHARE_LOCATION);
                } catch (GooglePlayServicesRepairableException e) {

                    Toast.makeText(this, "Please update Google Play services!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();

                } catch (GooglePlayServicesNotAvailableException e) {

                    Toast.makeText(this, "Google Play services not available!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }*/
                break;

            case R.id.contact_choose:
                hideMenu();
                Intent intentContact = new Intent(ChatPageActivity.this, SendContact.class);
                startActivityForResult(intentContact, REQUEST_CODE_CONTACTS);
                break;

            case R.id.capture_image:

                if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
                    DisplayAlert("Unblock" + " " + mReceiverName + " " + "to send message?");
                } else {
                    Intent camera = new Intent(ChatPageActivity.this, ImagecaptionActivity.class);
                    camera.putExtra("phoneno", mReceiverName);
                    camera.putExtra("from", "Camera");
                    startActivityForResult(camera, CAMERA_REQUEST);


                    /*File parentDir = new File(MessageFactory.BASE_STORAGE_PATH);
                    File imgDir = new File(MessageFactory.IMAGE_STORAGE_PATH);
//                    Send_photo.setText("Send Photo");
                    if (!parentDir.exists()) {
                        try {
                            parentDir.createNewFile();
                            if (!imgDir.exists()) {
                                imgDir.createNewFile();
                            }
                        } catch (IOException e) {
                            MyLog.e(TAG,"",e);
                        }
                    }

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File cameraImageOutputFile = new File(
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                            createCameraImageFileName());
                    cameraImageUri = Uri.fromFile(cameraImageOutputFile);
//            cameraImageUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", cameraImageOutputFile);

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri);
                    intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                    startActivityForResult(intent, CAMERA_REQUEST);*/
                }
                break;

            case R.id.menu_layout:

                openMenu();

                break;

            //--------------------Chat Actions Click Function-----------------------

            case R.id.replymess:

                ReplayMessageClick();

                break;

            case R.id.starred:

                StartMessageClick();

                break;

            case R.id.copychat:

                CopyClick();

                break;

            case R.id.delete:

                DeleteMessageClick();

                break;

            case R.id.forward:

                ForwardClick();

                break;
            case R.id.share:
                showUnSelectedActions();
                Uri mFile = Uri.fromFile(new File(mSelectedPath));

                ShareClick(mFile);

                break;
            case R.id.close:

                sendMessage.getText().clear();
                r1messagetoreplay.setVisibility(View.GONE);
                reply = false;
                showUnSelectedActions();

                break;

            case R.id.iBtnBack3:

                showUnSelectedActions();

                break;

            //---Call Layout-------------

            // case R.id.Ripple_call:
/*

                if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
                    DisplayAlert("Unblock" + " " + mReceiverName + " " + "to place a "
                            + getString(R.string.app_name) + " call");
                } else {
                    Activecall(false);
                }

                break;*/

          /*  case R.id.Ripple_Video:


                if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
                    DisplayAlert("Unblock" + " " + mReceiverName + " " + "to place a "
                            + getString(R.string.app_name) + " call");
                } else {
                    Activecall(true);
                }

                break;
*/

            case R.id.ivVoiceCall:
                if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
                    DisplayAlert("Unblock" + " " + mReceiverName + " " + "to place a "
                            + getString(R.string.app_name) + " call");
                } else {

                    Activecall(false);
                }

                break;
            case R.id.ivVideoCall:
                if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
                    DisplayAlert("Unblock" + " " + mReceiverName + " " + "to place a "
                            + getString(R.string.app_name) + " call");
                } else {
                    Activecall(true);
                }

                break;


            case R.id.enter_chat1:

                SendMessage();

                break;

            case R.id.name_status_layout:

                goInfoPage();

                break;

            case R.id.emojiButton:

                emojIcon.ShowEmojIcon();

                break;

            case R.id.add_contact:

                addnewcontact(receiverMsisdn);

                break;

            case R.id.block_contact:

                performMenuBlock();

                break;

            case R.id.report_spam:

                performreportspam();

                break;

            case R.id.tvBlock:

                performMenuBlock();

                break;

            case R.id.tvAddToContact:
                AppUtils.startService(this, ContactsSync.class);
                Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                intent.putExtra(INTENT_KEY_FINISH_ACTIVITY_ON_SAVE_COMPLETED, true);
                intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                intent.putExtra(ContactsContract.Intents.Insert.PHONE, user_name.getText().toString());
                startActivityForResult(intent, ADD_CONTACT);

                break;
            case R.id.iBtnScroll:

                if (mChatData.size() > 0)
                    chat_list.setSelection(mChatData.size() - 1);
                iBtnScroll.setVisibility(View.GONE);
                unreadcount.setVisibility(View.GONE);

                break;


        }

    }
    //---------------------------------------------Going User Information Page-----------------------------

    public double getLatitude() {
        if (gpsTracker == null)
            gpsTracker = new GPSTracker(this);
        if (gpsTracker.getLocation() != null && gpsTracker.getLatitude() > 0) {
            return gpsTracker.getLatitude();
        }
        return 0;
    }


    //----------------------------------------------Record Start Audio-----------------------------------------

    public double getLongitude() {
        if (gpsTracker == null)
            gpsTracker = new GPSTracker(this);
        if (gpsTracker.getLocation() != null && gpsTracker.getLongitude() > 0) {
            return gpsTracker.getLongitude();
        }
        return 0;
    }

    //-----------------------------------------------Record Touch Stop Listener---------------------------------------

    //------------------------------------Record Button Long Click-----------------------------
    @Override
    public boolean onLongClick(View view) {

        if (view.getId() == R.id.record) {

            attachmentLayout.setVisibility(View.GONE);
            if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {

                avoid_twotimescall++;
                if (avoid_twotimescall == 1) {
                    DisplayAlert("Unblock " + mReceiverName + " to send message?");
                } else if (avoid_twotimescall == 2) {
                    avoid_twotimescall = 0;
                }

            } else {
                if (checkAudioRecordPermission()) {

                    //  record.setImageResource(R.drawable.record_hold);
                    //record.setBackground(ContextCompat.getDrawable(context, R.drawable.record_hold));
                    record.setBackground(ContextCompat.getDrawable(context, R.drawable.circle_record_icon));
                    record.setImageResource(R.drawable.recv_ic_mic_white);
                    sendMessage.setVisibility(View.GONE);
                    selEmoji.setImageResource(R.drawable.record_usericon);
                    myChronometer.setVisibility(View.VISIBLE);
                    image_to.setVisibility(View.VISIBLE);
                    slidetocencel.setVisibility(View.VISIBLE);
                    capture_image.setVisibility(View.GONE);
                    attachment_icon.setVisibility(View.GONE);
                    // myChronometer.setTypeface(face);
                    selEmoji.setEnabled(false);
                    startAudioRecord();
                } else {
                    requestAudioRecordPermission();
                }
            }
        }

        return true;
    }

    public void addnewcontact(final String number) {
        CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(getResources().getString(R.string.new_exitcontact));
        dialog.setPositiveButtonText("NEW");
        dialog.setNegativeButtonText("EXISTING");

        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                // Sets the MIME type to match the Contacts Provider
                intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                intent.putExtra(ContactsContract.Intents.Insert.NAME, name);
                intent.putExtra(ContactsContract.Intents.Insert.PHONE, number);
                startActivityForResult(intent, ADD_CONTACT);
                //finish();
            }

            @Override
            public void onNegativeButtonClick() {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, 1);
                finish();
            }
        });

        dialog.show(getSupportFragmentManager(), "Save contact");

    }

    private void performreportspam() {
        new AlertDialog.Builder(context)
                .setMessage("Are you sure you want to Report this message?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SendMessageEvent messageEvent = new SendMessageEvent();
                        messageEvent.setEventName(SocketManager.EVENT_REPORT_SPAM_USER);
                        try {
                            JSONObject myobj = new JSONObject();
                            myobj.put("from", from);
                            myobj.put("to", to);

                            messageEvent.setMessageObject(myobj);
                            EventBus.getDefault().post(messageEvent);
                        } catch (JSONException e) {
                            MyLog.e(TAG, "", e);
                        }

                    }
                })

                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();

    }


    //-------------------------------------------------Send Message--------------------------------------

    private void goInfoPage() {

        showUnSelectedActions();
        selectedChatItems.clear();

        if (isGroupChat) {
            Intent infoIntent = new Intent(ChatPageActivity.this, GroupInfo.class);
            infoIntent.putExtra("GroupId", mGroupId);
            infoIntent.putExtra("GroupName", user_name.getText().toString());
            startActivityForResult(infoIntent, EXIT_GROUP_REQUEST_CODE);
        } else {
            Intent infoIntent = new Intent(ChatPageActivity.this, UserInfo.class);
            infoIntent.putExtra("UserId", to);
            infoIntent.putExtra("UserName", user_name.getText().toString());
            infoIntent.putExtra("UserAvatar", receiverAvatar);
            infoIntent.putExtra("UserNumber", receiverMsisdn);
            infoIntent.putExtra("FromSecretChat", false);
            startActivityForResult(infoIntent, MUTE_ACTIVITY);
        }
    }

    private void startAudioRecord() {

        try {
            if (!audioRecordStarted) {
                audioRecordStarted = true;
                File audioDir = new File(MessageFactory.AUDIO_STORAGE_PATH);
                if (!audioDir.exists()) {
                    audioDir.mkdirs();
                }

                audioRecordPath = MessageFactory.AUDIO_STORAGE_PATH + MessageFactory.getMessageFileName(MessageFactory.audio,
                        Calendar.getInstance().getTimeInMillis() + "rc", ".mp3");
                File file = new File(audioRecordPath);
                try {
                    file.createNewFile();
                } catch (Exception e) {
                    audioRecordPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getAbsolutePath() +
                            MessageFactory.getMessageFileName(MessageFactory.audio,
                                    Calendar.getInstance().getTimeInMillis() + "rc", ".mp3");
                    file = new File(audioRecordPath);
                    try {
                        file.createNewFile();
                    } catch (Exception e1) {
                        MyLog.e(TAG, "startAudioRecord: ", e1);
                    }
                }
                audioRecorder = new MediaRecorder();
                audioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                audioRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                audioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                audioRecorder.setOutputFile(audioRecordPath);
                audioRecorder.prepare();
                audioRecorder.start();
                myChronometer.start();
                myChronometer.setBase(SystemClock.elapsedRealtime());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //----------------------------------------Send Particular Message Replay------------------------------------------------------

    private void showAudioRecordSentAlert(final String audioRecordPath, final String durationStr) {
        CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(getString(R.string.send_recorded_audio));
        dialog.setPositiveButtonText(getString(R.string.send));
        dialog.setNegativeButtonText(getString(R.string.cancel));
        dialog.setCancelable(false);

        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                sendAudioMessage(audioRecordPath, durationStr, MessageFactory.AUDIO_FROM_RECORD, false);
            }

            @Override
            public void onNegativeButtonClick() {

            }
        });
        dialog.show(getSupportFragmentManager(), "Record Alert");
    }


    //---------------------------------------------------------Send WebLink Messages------------------------------------------------------

    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        int hours = (int) (millis / (1000 * 60 * 60));
        int minutes = (int) ((millis % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (int) (((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000);

        buf.append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }


    //-----------------------------------------------------Call Functionality Navigate------------------------------------------

    private void SendMessage() {


        if (contactDB_sqlite.getBlockedStatus(to, false).equals("1")) {
            DisplayAlert("Unblock" + " " + mReceiverName + " " + "to send message?");
        } else {
            if ((!hasLinkInfo && !reply)) {
                reply = false;
                sendTextMessage();
            } else if (reply) {
                r1messagetoreplay.setVisibility(View.GONE);
                sendparticularmsgreply();
                if (at_memberlist != null) {
                    at_memberlist.clear();
                }
            } else {
                sendWebLinkMessage();
                if (at_memberlist != null) {
                    at_memberlist.clear();
                }
            }
        }
    }

//-------------------------------------------------Call Audio Permission-------------------------------------------

    //-------------------------------------------------SEND TEXT MESSAGE --------------------------------------------------------------
    private void sendTextMessage() {
        String data = sendMessage.getText().toString().trim();
        String data_at = data;
        String htmlText = data;
        int isTagapplied = 0;
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> userID = new ArrayList<>();

        if (at_memberlist != null && at_memberlist.size() >= 0)
            for (GroupMembersPojo groupMembersPojo : at_memberlist) {
                String userName = "@" + groupMembersPojo.getContactName();
                names.add(userName);
                userID.add(groupMembersPojo.getUserId());
                if (data_at.contains(userName)) {
                    data_at = data_at.replace(userName, TextMessage.TAG_KEY + groupMembersPojo.getUserId() + TextMessage.TAG_KEY);
                    isTagapplied = 1;
                }
            }

        if (!data.equalsIgnoreCase("")) {
            if (session.getarchivecount() != 0) {
                if (session.getarchive(from + "-" + to))
                    session.removearchive(from + "-" + to);
            }
            if (session.getarchivecountgroup() != 0) {
                if (session.getarchivegroup(from + "-" + to + "-g"))
                    session.removearchivegroup(from + "-" + to + "-g");
            }

            SendMessageEvent messageEvent = new SendMessageEvent();
            TextMessage message = (TextMessage) MessageFactory.getMessage(MessageFactory.text, this);
            JSONObject msgObj;
            if (isGroupChat) {
                messageEvent.setEventName(SocketManager.EVENT_GROUP);
                msgObj = (JSONObject) message.getGroupMessageObject(to, data_at, user_name.getText().toString());

                try {
                    msgObj.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE);
                    msgObj.put("userName", user_name.getText().toString());
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            } else {
                msgObj = (JSONObject) message.getMessageObject(to, data, false);
                messageEvent.setEventName(SocketManager.EVENT_MESSAGE);
            }
            ///data_at

            Log.e(TAG, "mReceiverId" + mReceiverId);

            Log.e(TAG, "user_name.getText().toString()" + user_name.getText().toString());

            MessageItemChat item = message.createMessageItem(true, htmlText, MessageFactory.DELIVERY_STATUS_NOT_SENT, mReceiverId, user_name.getText().toString());

            //Check if it blocked and check it is to value and receiver no


            if (contactDB_sqlite.getBlockedMineStatus(to, false).equals(ContactDB_Sqlite.BLOCKED_STATUS)) {
                item.setisBlocked(true);
            }
            messageEvent.setMessageObject(msgObj);


            if (isTagapplied == 1) {
                item.setTagapplied(true);
                item.setSetArrayTagnames(names);
                item.setEditTextmsg(data);
                item.setUserId_tag(userID);
            }

            if (isGroupChat) {
                if (enableGroupChat) {
                    item.setGroupName(user_name.getText().toString());
                    db.updateChatMessage(item, chatType);
                    if (!isAlreadyExist(item))
                        mChatData.add(item);
                    EventBus.getDefault().post(messageEvent);
                    notifyDatasetChange();
                    sendMessage.getText().clear();
                    sendMessage.setText("");
                } else {
                    Toast.makeText(ChatPageActivity.this, "You are not a member in this group", Toast.LENGTH_SHORT).show();
                }
            } else {
                item.setSenderMsisdn(receiverMsisdn);
                item.setSenderName(user_name.getText().toString());
                if (!isAlreadyExist(item))
                    mChatData.add(item);
                EventBus.getDefault().post(messageEvent);
                Log.e(TAG, "item" + item.toString());
                db.updateChatMessage(item, chatType);
                notifyDatasetChange();
                sendMessage.getText().clear();
                sendMessage.setText("");

            }

        }
        if (at_memberlist != null) {
            at_memberlist.clear();
        }

        if (Chat_Received_Count == 20) {

            Chat_Received_Count = 0;
            bottom_ad_layout.setVisibility(View.VISIBLE);
        }

        Chat_Received_Count++;


    }


    //--------------------------------------------------Send Text Message Watcher-----------------------------------------------

    private void sendparticularmsgreply() {
        String data = sendMessage.getText().toString().trim();
        String data_at = data;
        int isTagapplied = 0;
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> userID = new ArrayList<>();

        if (isGroupChat) {
            if (enableGroupChat) {
                if (at_memberlist != null && at_memberlist.size() >= 0)
                    for (GroupMembersPojo groupMembersPojo : at_memberlist) {
                        String userName = "@" + groupMembersPojo.getContactName();
                        names.add(userName);
                        userID.add(groupMembersPojo.getUserId());
                        if (data_at.contains(userName)) {
                            data_at = data.replace(userName, TextMessage.TAG_KEY + groupMembersPojo.getUserId() + TextMessage.TAG_KEY);
                            isTagapplied = 1;

                        }
                    }
            }
        }

        if (!data.equalsIgnoreCase("")) {
            if (session.getarchivecount() != 0) {
                if (session.getarchive(from + "-" + to))
                    session.removearchive(from + "-" + to);
            }
            if (session.getarchivecountgroup() != 0) {
                if (session.getarchivegroup(from + "-" + to + "-g"))
                    session.removearchivegroup(from + "-" + to + "-g");
            }
            SendMessageEvent messageEvent = new SendMessageEvent();
            TextMessage message = (TextMessage) MessageFactory.getMessage(MessageFactory.text, this);
            JSONObject msgObj;
            if (isGroupChat) {
                messageEvent.setEventName(SocketManager.EVENT_GROUP);
                msgObj = (JSONObject) message.getGroupMessageObject(to, data_at, user_name.getText().toString());
                try {
                    msgObj.put("groupType", SocketManager.ACTION_EVENT_GROUP_REPlY_MESSAGE);
                    msgObj.put("userName", user_name.getText().toString());
                    msgObj.put("recordId", mymsgid);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            } else {
                messageEvent.setEventName(SocketManager.EVENT_REPLY_MESSAGE);
                msgObj = (JSONObject) message.getMessageObject(to, data, false);
                try {
                    msgObj.put("recordId", mymsgid);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
            messageEvent.setMessageObject(msgObj);
            MessageItemChat item = message.createMessageItem(true, data,
                    MessageFactory.DELIVERY_STATUS_NOT_SENT, mReceiverId, user_name.getText().toString());
            item.setReplyType(replytype);

            if (isTagapplied == 1) {
                item.setTagapplied(true);
                item.setSetArrayTagnames(names);
                item.setEditTextmsg(data);
                item.setUserId_tag(userID);
            }

            if (Integer.parseInt(replytype) == MessageFactory.text) {
                item.setReplyMessage(messageold);
            } else if (Integer.parseInt(replytype) == MessageFactory.picture) {
                item.setreplyimagepath(imgpath);
                item.setreplyimagebase64(imageAsBytes);
            } else if (Integer.parseInt(replytype) == MessageFactory.audio) {
                item.setReplyMessage("Audio");
            } else if (Integer.parseInt(replytype) == MessageFactory.video) {
                item.setReplyMessage("Video");
                item.setreplyimagebase64(imageAsBytes);
            } else if (Integer.parseInt(replytype) == MessageFactory.document) {
                item.setReplyMessage(messageold);
            } else if (Integer.parseInt(replytype) == MessageFactory.web_link) {
                item.setReplyMessage(messageold);
            } else if (Integer.parseInt(replytype) == MessageFactory.contact) {
                item.setReplyMessage(contactname);
            } else if (Integer.parseInt(replytype) == MessageFactory.location) {
                if (locationName != null && !locationName.equals("")) {
                    item.setReplyMessage(locationName);
                } else {
                    item.setReplyMessage("Location");
                }
                item.setreplyimagebase64(imageAsBytes);
            }

            item.setSenderMsisdn(receiverMsisdn);
            item.setSenderName(mReceiverName);
            item.setReplySender(ReplySender);
            reply = false;


            if (isGroupChat) {
                if (enableGroupChat) {
                    item.setReplyType(replytype);
                    item.setreplyimagebase64(imageAsBytes);
                    if (Integer.parseInt(replytype) == MessageFactory.text) {
                        item.setReplyMessage(messageold);
                    } else if (Integer.parseInt(replytype) == MessageFactory.picture) {
                        item.setreplyimagepath(imgpath);
                    } else if (Integer.parseInt(replytype) == MessageFactory.audio) {
                        item.setReplyMessage("Audio");
                    } else if (Integer.parseInt(replytype) == MessageFactory.document) {
                        item.setReplyMessage(messageold);
                    } else if (Integer.parseInt(replytype) == MessageFactory.web_link) {
                        item.setReplyMessage(messageold);
                    } else if (Integer.parseInt(replytype) == MessageFactory.contact) {
                        item.setReplyMessage(contactname);
                    }
                    item.setGroupName(user_name.getText().toString());
                    db.updateChatMessage(item, chatType);
                    if (!isAlreadyExist(item))
                        mChatData.add(item);
                    EventBus.getDefault().post(messageEvent);
                    notifyDatasetChange();
                    sendMessage.getText().clear();
                } else {
                    Toast.makeText(ChatPageActivity.this, "You are not a member in this group", Toast.LENGTH_SHORT).show();
                }
            } else {
                item.setSenderMsisdn(receiverMsisdn);
                item.setSenderName(user_name.getText().toString());
                db.updateChatMessage(item, chatType);
                if (!isAlreadyExist(item))
                    mChatData.add(item);
                EventBus.getDefault().post(messageEvent);
                notifyDatasetChange();
                sendMessage.getText().clear();
            }

        }

    }

    private void sendWebLinkMessage() {


        if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
            DisplayAlert("Unblock " + mReceiverName + " to send message?");
        } else {
            String data = sendMessage.getText().toString().trim();

            if (!data.equalsIgnoreCase("")) {
                if (session.getarchivecount() != 0) {
                    if (session.getarchive(from + "-" + to))
                        session.removearchive(from + "-" + to);
                }
                if (session.getarchivecountgroup() != 0) {
                    if (session.getarchivegroup(from + "-" + to + "-g"))
                        session.removearchivegroup(from + "-" + to + "-g");
                }

                SendMessageEvent messageEvent = new SendMessageEvent();
                WebLinkMessage message = (WebLinkMessage) MessageFactory.getMessage(MessageFactory.web_link, this);

                String webLinkThumb = null;
                if (ivWebLink.getDrawable() != null) {
                    Bitmap linkBmp = null;
                    try {
                        linkBmp = ((BitmapDrawable) ivWebLink.getDrawable().getCurrent()).getBitmap();
                    } catch (Exception e) {
                        linkBmp = ((GlideBitmapDrawable) ivWebLink.getDrawable().getCurrent()).getBitmap();
                    }

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    linkBmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    webLinkThumb = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    try {
                        byteArrayOutputStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                JSONObject msgObj;
                String receiverName = user_name.getText().toString();
                if (isGroupChat) {
                    messageEvent.setEventName(SocketManager.EVENT_GROUP);
                    msgObj = (JSONObject) message.getGroupMessageObject(to, data, receiverName);
                } else {
                    msgObj = (JSONObject) message.getMessageObject(to, data, false);
                    messageEvent.setEventName(SocketManager.EVENT_MESSAGE);
                }

                MessageItemChat item = message.createMessageItem(true, data, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                        mReceiverId, receiverName, webLink, webLinkTitle, webLinkDesc, webLinkImgUrl, webLinkThumb);
                msgObj = (JSONObject) message.getWebLinkObject(msgObj, webLink, webLinkTitle, webLinkDesc, webLinkImgUrl, webLinkThumb);
                messageEvent.setMessageObject(msgObj);

                if (isGroupChat) {
                    if (enableGroupChat) {
                        item.setGroupName(user_name.getText().toString());
                        db.updateChatMessage(item, chatType);
                        if (!isAlreadyExist(item))
                            mChatData.add(item);
                        EventBus.getDefault().post(messageEvent);
                        notifyDatasetChange();
                    } else {
                        Toast.makeText(ChatPageActivity.this, "You are not a member in this group", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    item.setSenderMsisdn(receiverMsisdn);
                    item.setSenderName(user_name.getText().toString());
                    db.updateChatMessage(item, chatType);
                    if (!isAlreadyExist(item))
                        mChatData.add(item);
                    EventBus.getDefault().post(messageEvent);
                    notifyDatasetChange();
                }

                sendMessage.getText().clear();
                hasLinkInfo = false;
                rlWebLink.setVisibility(View.GONE);
                webLink = "";
                webLinkTitle = "";
                webLinkImgUrl = "";
                webLinkDesc = "";
            }
        }


    }

    private void Activecall(boolean isVideoCall) {
        if (ConnectivityInfo.isInternetConnected(this))
            if (checkAudioRecordPermission()) {
                CallMessage message = new CallMessage(ChatPageActivity.this);
                boolean isOutgoingCall = true;

                String roomid = message.getroomid();
                String timestamp = message.getroomid();
                String callid = mCurrentUserId + "-" + mReceiverId + "-" + timestamp;
                if (!CallsActivity.isStarted) {
                    if (isOutgoingCall) {
                        CallsActivity.opponentUserId = to;

                    }
                    PreferenceManager.setDefaultValues(context, org.appspot.apprtc.R.xml.preferences, false);
                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
                    String keyprefRoomServerUrl = context.getString(org.appspot.apprtc.R.string.pref_room_server_url_key);
                    String roomUrl = sharedPref.getString(
                            keyprefRoomServerUrl, context.getString(org.appspot.apprtc.R.string.pref_room_server_url_default));
                    int videoWidth = 0;
                    int videoHeight = 0;
                    String resolution = context.getString(org.appspot.apprtc.R.string.pref_resolution_default);
                    String[] dimensions = resolution.split("[ x]+");
                    if (dimensions.length == 2) {
                        try {
                            videoWidth = Integer.parseInt(dimensions[0]);
                            videoHeight = Integer.parseInt(dimensions[1]);
                        } catch (NumberFormatException e) {
                            videoWidth = 0;
                            videoHeight = 0;
                        }
                    }
                    Uri uri = Uri.parse(roomUrl);
                    Intent intent = new Intent(context, CallsActivity.class);
                    intent.setData(uri);
                    intent.putExtra(CallsActivity.EXTRA_IS_OUTGOING_CALL, isOutgoingCall);
                    intent.putExtra(CallsActivity.EXTRA_DOC_ID, callid);
                    intent.putExtra(CallsActivity.EXTRA_FROM_USER_ID, from);
                    intent.putExtra(CallsActivity.EXTRA_TO_USER_ID, to);
                    intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, receiverMsisdn);
                    intent.putExtra(CallsActivity.EXTRA_OPPONENT_PROFILE_PIC, "");
                    intent.putExtra(CallsActivity.EXTRA_NAVIGATE_FROM, context.getClass().getSimpleName()); // For navigating from call activity
                    intent.putExtra(CallsActivity.EXTRA_CALL_CONNECT_STATUS, "0");
                    intent.putExtra(CallsActivity.EXTRA_CALL_TIME_STAMP, timestamp);
                    intent.putExtra(CallsActivity.EXTRA_ROOMID, roomid);


                    Log.e(TAG, "EXTRA_IS_OUTGOING_CALL" + isOutgoingCall);

                    Log.e(TAG, "EXTRA_DOC_ID" + callid);

                    Log.e(TAG, "EXTRA_FROM_USER_ID" + from);

                    Log.e(TAG, "EXTRA_TO_USER_ID" + to);
                    Log.e(TAG, "EXTRA_CALL_TIME_STAMP" + timestamp);

                    Log.e(TAG, "EXTRA_USER_MSISDN" + receiverMsisdn);
                    Log.e(TAG, "EXTRA_CALL_CONNECT_STATUS" + "0");

                    Log.e(TAG, "EXTRA_ROOMID" + roomid);

                    intent.putExtra(CallsActivity.EXTRA_LOOPBACK, false);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_CALL, isVideoCall);
                    intent.putExtra(CallsActivity.EXTRA_SCREENCAPTURE, false);
                    intent.putExtra(CallsActivity.EXTRA_CAMERA2, true);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_WIDTH, videoWidth);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_HEIGHT, videoHeight);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_FPS, 0);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_VIDEO_BITRATE, 0);
                    intent.putExtra(CallsActivity.EXTRA_VIDEOCODEC, context.getString(org.appspot.apprtc.R.string.pref_videocodec_default));
                    intent.putExtra(CallsActivity.EXTRA_HWCODEC_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_CAPTURETOTEXTURE_ENABLED, true);
                    intent.putExtra(CallsActivity.EXTRA_FLEXFEC_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_NOAUDIOPROCESSING_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_AECDUMP_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_OPENSLES_ENABLED, false);
                    intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AEC, false);
                    intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AGC, false);
                    intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_NS, false);
                    intent.putExtra(CallsActivity.EXTRA_ENABLE_LEVEL_CONTROL, false);
                    intent.putExtra(CallsActivity.EXTRA_AUDIO_BITRATE, 0);
                    intent.putExtra(CallsActivity.EXTRA_AUDIOCODEC, context.getString(org.appspot.apprtc.R.string.pref_audiocodec_default));
                    intent.putExtra(CallsActivity.EXTRA_DISPLAY_HUD, false);
                    intent.putExtra(CallsActivity.EXTRA_TRACING, false);
                    intent.putExtra(CallsActivity.EXTRA_CMDLINE, false);
                    intent.putExtra(CallsActivity.EXTRA_RUNTIME, 0);
                    intent.putExtra(CallActivity.EXTRA_DATA_CHANNEL_ENABLED, true);
                    intent.putExtra(CallActivity.EXTRA_ORDERED, true);
                    intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS_MS, -1);
                    intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS, -1);
                    intent.putExtra(CallActivity.EXTRA_PROTOCOL, context.getString(org.appspot.apprtc.R.string.pref_data_protocol_default));
                    intent.putExtra(CallActivity.EXTRA_NEGOTIATED, false);
                    intent.putExtra(CallActivity.EXTRA_ID, -1);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            } else {
                requestAudioRecordPermission();
            }
    }

    private void Redirecttocall() {
        if (ConnectivityInfo.isInternetConnected(this)) {
            //      Log.e(TAG,"isStarted"+CallsActivity.isStarted);
            boolean mCall = SharedPreference.getInstance().getBool(context, "isStarted");
            Log.e("mCall", "mCall" + mCall);
            if (mCall) {
                Intent callIntent = new Intent(context, CallsActivity.class);
                callIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                context.startActivity(callIntent);
            } else {
                //    Activecall(false);
                Intent callIntent = new Intent(context, CallsActivity.class);
                callIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                context.startActivity(callIntent);
            }
          /*  if (CallsActivity.isStarted) {
                Intent callIntent = new Intent(context, CallsActivity.class);
                callIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                context.startActivity(callIntent);
            } else {
                Activecall(false);
            }*/
        }
    }

    private void requestAudioRecordPermission() {
        ActivityCompat.requestPermissions(ChatPageActivity.this, new String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, AUDIO_RECORD_PERMISSION_REQUEST_CODE);
    }


    //-----------------------------------------------Menu Popup-------------------------------------------------------------

    private void handleTypingEvent(int length) {
        if (length > 0) {
            long currentTime = Calendar.getInstance().getTimeInMillis();
            long timeDiff = currentTime - fromLastTypedAt;
            if (timeDiff > MessageFactory.TYING_MESSAGE_MIN_TIME_DIFFERENCE) {
                fromLastTypedAt = currentTime;
                if (isGroupChat) {
                    if (enableGroupChat) {
                        sendTypeEvent();
                    }
                } else {
                    sendTypeEvent();
                }
            }
        }
    }

    private void checkLinkDetails(String text) {

        text = " " + text;
        String[] arrSplitText = text.split(" ");
        for (final String splitText : arrSplitText) {

            if (Patterns.WEB_URL.matcher(splitText).matches()) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String url = splitText;
                            if (!splitText.startsWith("http")) {
                                url = "http://" + splitText;
                            }
                            final Document doc = Jsoup.connect(url).get();

                            Elements metaElems = doc.select("meta");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    webLinkTitle = doc.title();
                                    webLink = doc.baseUri();
                                    webLinkImgUrl = webLink + "/favicon.ico";
                                    tvWebTitle.setText(webLinkTitle);
                                    tvWebLink.setText(webLink);
                                    Glide.with(ChatPageActivity.this).load(webLinkImgUrl).into(ivWebLink);

                                    if (rlWebLink.getVisibility() == View.GONE) {
                                        rlWebLink.setVisibility(View.VISIBLE);
                                    }
                                }
                            });

                            hasLinkInfo = true;

                        } catch (IOException ex) {
                            ex.printStackTrace();
                        } catch (NullPointerException nullEx) {
                            hasLinkInfo = false;
                            nullEx.printStackTrace();
                        } catch (Exception e) {
                            hasLinkInfo = false;
                            e.printStackTrace();
                        }
                    }
                }).start();
                break;
            } else {
                hasLinkInfo = false;
            }

        }


    }

    private void sendTypeEvent() {

        boolean blockedStatus = contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1");
        if (!blockedStatus) {
            JSONObject msgObj = new JSONObject();
            SendMessageEvent messageEvent = new SendMessageEvent();
            messageEvent.setEventName(SocketManager.EVENT_TYPING);
            try {
                msgObj.put("from", from);
                msgObj.put("to", to);

                if (!isGroupChat) {
                    msgObj.put("convId", mConvId);
                    msgObj.put("type", "single");
                } else {
                    msgObj.put("convId", mGroupId);
                    msgObj.put("type", "group");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            messageEvent.setMessageObject(msgObj);
            EventBus.getDefault().post(messageEvent);
        }
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        isMenuBtnClick = false;
        super.onOptionsMenuClosed(menu);
    }

    private void openMenu() {

        View popupView = findViewById(R.id.popup);

        if (isMenuBtnClick) {
            List<MultiTextDialogPojo> labelsList = new ArrayList<>();
            String strBlock, strChatlock;

            if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
                strBlock = "Unblock";
            } else {
                strBlock = "Block";
            }


            MultiTextDialogPojo pojo = new MultiTextDialogPojo();
            if (!isGroupChat) {
                pojo.setLabelText(strBlock);
                labelsList.add(pojo);
            }

            pojo = new MultiTextDialogPojo();
            pojo.setLabelText("Clear Chat");
            labelsList.add(pojo);

            /*pojo = new MultiTextDialogPojo();
            pojo.setLabelText("Email Chat");
            labelsList.add(pojo);*/
            pojo = new MultiTextDialogPojo();
            pojo.setLabelText("Add Shortcut");
            labelsList.add(pojo);

            /*if (sessionManager.getLockChatEnabled().equals("1")) {
                if (!checkIsAlreadyLocked(docId)) {
                    strChatlock = "Lock Chat";
                } else {
                    strChatlock = "Unlock Chat";
                }
                pojo = new MultiTextDialogPojo();
                pojo.setLabelText(strChatlock);
                labelsList.add(pojo);
            }*/

            CustomMultiTextItemsDialog dialog = new CustomMultiTextItemsDialog();
            dialog.setLabelsList(labelsList);
            if (!isMassChat) {
                dialog.setDialogItemClickListener(new CustomMultiTextItemsDialog.DialogItemClickListener() {
                    @Override
                    public void onDialogItemClick(int position) {
                        switch (position) {

                            case 0:
                                performMenuBlock();
                                break;

                            case 1:
                                performMenuClearChat();
                                break;

                            case 2:
                                performMenuEmailChat();
                                break;

                            case 3:
                                addShortcutConfirmationDialog();
                                break;

                            case 4:
                                performChatlock();
                                break;

                        }
                    }
                });
            } else {
                dialog.setDialogItemClickListener(new CustomMultiTextItemsDialog.DialogItemClickListener() {
                    @Override
                    public void onDialogItemClick(int position) {
                        switch (position) {

                            case 0:
                                performMenuBlock();
                                break;

                            case 1:
                                performMenuClearChat();
                                break;

                            case 2:
                                performMenuDeleteChat();
                                break;

                            case 3:
                                performMenuEmailChat();

                                break;

                            case 4:
                                addShortcutConfirmationDialog();

                                break;
                            case 5:
                                performChatlock();

                                break;
                        }
                    }
                });
            }

            dialog.show(getSupportFragmentManager(), "custom multi");
        } else {
            PopupMenu popup = new PopupMenu(this, popupView);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.activity_chat_view, popup.getMenu());
            popup.getMenu().findItem(R.id.menuSearch).setVisible(false);
            popup.getMenu().findItem(R.id.menuMute).setVisible(false);
            popup.getMenu().findItem(R.id.menuWallpaper).setVisible(false);
            popup.getMenu().findItem(R.id.menuMore).setVisible(false);
            if (!isGroupChat) {
                popup.getMenu().findItem(R.id.menuSecretChat).setVisible(false);
                popup.getMenu().findItem(R.id.menuBlock).setVisible(true);
            } else {
                popup.getMenu().findItem(R.id.menuSecretChat).setVisible(false);
            }

            popup.getMenu().findItem(R.id.menuClearChat).setVisible(true);
            if (isMassChat) {
                popup.getMenu().findItem(R.id.menudeletechat).setVisible(true);
            }

//            popup.getMenu().findItem(R.id.menuEmailChat).setVisible(true);
            popup.getMenu().findItem(R.id.menuAddShortcut).setVisible(true);
            popup.getMenu().findItem(R.id.menuChatLock).setVisible(false);
            if (sessionManager.getLockChatEnabled().equals("1")) {


                /*if (!checkIsAlreadyLocked(docId)) {
                    popup.getMenu().findItem(R.id.menuChatLock).setTitle("Lock Chat");
                } else {
                    popup.getMenu().findItem(R.id.menuChatLock).setTitle("Unlock Chat");
                }*/
            } else {
                popup.getMenu().findItem(R.id.menuChatLock).setVisible(false);
            }


            if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
                popup.getMenu().findItem(R.id.menuBlock).setTitle("Unblock");
            } else {
                popup.getMenu().findItem(R.id.menuBlock).setTitle("Block");
            }


           /* if (!checkIsAlreadyLocked(docId)) {
                popup.getMenu().findItem(R.id.menuChatLock).setTitle("Lock Chat");
            } else {
                popup.getMenu().findItem(R.id.menuChatLock).setTitle("Unlock Chat");
            }*/

            popup.getMenu().findItem(R.id.menuBlock).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    performMenuBlock();
                    return false;
                }
            });

            /*popup.getMenu().findItem(R.id.menuEmailChat).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    performMenuEmailChat();
                    return false;
                }
            });*/

            popup.getMenu().findItem(R.id.menuClearChat).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    performMenuClearChat();
                    return false;
                }
            });

            if (isMassChat) {

                popup.getMenu().findItem(R.id.menudeletechat).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        performMenuDeleteChat();
                        return false;
                    }
                });
            }
            popup.getMenu().findItem(R.id.menuAddShortcut).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    addShortcutConfirmationDialog();
                    return false;
                }
            });


            popup.getMenu().findItem(R.id.menuChatLock).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    performChatlock();
                    return false;
                }
            });

            popup.show();
        }
    }

    private void performMenuBlock() {

        String msg;

        if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
            msg = "Do you want to Unblock " + mReceiverName + "?";
        } else {
            msg = "Block " + mReceiverName + "? Blocked contacts will no longer be able to call you or send you messages.";
        }

        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(msg);
        dialog.setPositiveButtonText("Ok");
        dialog.setNegativeButtonText("Cancel");
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                dialog.dismiss();

                putBlockUser();
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });

        dialog.show(getSupportFragmentManager(), "Block alert");

    }

    private void performMenuClearChat() {

        final String msg = "Are you sure you want to clear chat messages in this chat?";

        final CustomAlertClearDialog dialog = new CustomAlertClearDialog();
        dialog.setNegativeButtonText("Cancel");
        dialog.setPositiveButtonText("Clear");
        dialog.setCheckBoxtext("Keep Starred Messages");
        boolean isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
        if (isMassChat) {
            dialog.setCheckBoxtext2("Also Clear Chat  for " + mReceiverName);
        }
        dialog.setMessage(msg);
        final String docId;
        deletestarred = false;
        if (isGroupChat) {
            docId = mCurrentUserId + "-" + to + "-g";
        } else {
            docId = mCurrentUserId + "-" + to;
        }

        dialog.setCheckBoxCheckedChangeListener(new CustomAlertClearDialog.OnDialogCheckBoxCheckedChangeListener() {
            @Override
            public void onCheckedChange(boolean isChecked) {
                MyLog.e("Star", "Star" + isChecked);
                deletestarred = isChecked;
            }
        });

        dialog.setCheckBox2CheckedChangeListener(new CustomAlertClearDialog.OnDialogCheckBoxCheckedChangeListener() {
            @Override
            public void onCheckedChange(boolean isChecked) {
                // deletestarred = isChecked;
                // MyLog.e("delete for","Star"+isChecked);

                mClearChat = isChecked;
            }
        });
        dialog.setCustomDialogCloseListener(new CustomAlertClearDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                int star_status = 0;
                if (deletestarred) {
                    star_status = 1;
                    // db.clearUnStarredMessage(docId);
                }
                if (!isGroupChat) {
                    if (userInfoSession.hasChatConvId(docId)) {
                        if (mChatData.size() > 0) {
                            try {
                                String[] array = mChatData.get(mChatData.size() - 1).getMessageId().split("-");

                                String convId = userInfoSession.getChatConvId(docId);
                                JSONObject object = new JSONObject();
                                object.put("convId", convId);
                                object.put("from", mCurrentUserId);
                                //   boolean isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
                                //  if (isMassChat) {
                                if (mClearChat) {
                                    object.put("delete_opponent", "1");
                                } else {
                                    object.put("delete_opponent", "0");
                                }

                                object.put("lastId", array[2]);
                                // }

                                object.put("from", mCurrentUserId);
                                object.put("MessageId", mChatData.get(mChatData.size() - 1).getMessageId());
                                object.put("star_status", star_status);
                                object.put("type", MessageFactory.CHAT_TYPE_SINGLE);
                                SendMessageEvent event = new SendMessageEvent();
                                event.setEventName(SocketManager.EVENT_CLEAR_CHAT);
                                event.setMessageObject(object);
                                deletestarred = false;
                                mClearChat = false;
                                Log.e(TAG, "EVENT_CLEAR_CHAT" + object);
                                EventBus.getDefault().post(event);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        // db.clearAllSingleChatMessage(docId);
                    }
                } else {

                    try {
                        JSONObject object = new JSONObject();
                        object.put("convId", to);
                        object.put("from", mCurrentUserId);
                        object.put("star_status", star_status);
                        object.put("type", MessageFactory.CHAT_TYPE_GROUP);
                        SendMessageEvent event = new SendMessageEvent();
                        event.setEventName(SocketManager.EVENT_CLEAR_CHAT);
                        event.setMessageObject(object);
                        deletestarred = false;
                        EventBus.getDefault().post(event);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
    /*    dialog.setCustomDialogCloseListener2(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                int star_status = 0;
                if (deletestarred) {
                    star_status = 1;
                    // db.clearUnStarredMessage(docId);
                }
                if (!isGroupChat) {
                    if (userInfoSession.hasChatConvId(docId)) {
                        try {
                            String convId = userInfoSession.getChatConvId(docId);
                            JSONObject object = new JSONObject();
                            object.put("convId", convId);
                            object.put("from", mCurrentUserId);


                            object.put("from", mCurrentUserId);

                            object.put("star_status", star_status);
                            object.put("type", MessageFactory.CHAT_TYPE_SINGLE);
                            SendMessageEvent event = new SendMessageEvent();
                            event.setEventName(SocketManager.EVENT_CLEAR_CHAT);
                            event.setMessageObject(object);
                            EventBus.getDefault().post(event);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // db.clearAllSingleChatMessage(docId);
                    }
                } else {

                    try {
                        JSONObject object = new JSONObject();
                        object.put("convId", to);
                        object.put("from", mCurrentUserId);

                        object.put("from", mCurrentUserId);




                        object.put("star_status", star_status);
                        object.put("type", MessageFactory.CHAT_TYPE_GROUP);
                        SendMessageEvent event = new SendMessageEvent();
                        event.setEventName(SocketManager.EVENT_CLEAR_CHAT);
                        event.setMessageObject(object);
                        EventBus.getDefault().post(event);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });

    */
        dialog.show(getSupportFragmentManager(), "Delete member alert");
    }

    private void performMenuDeleteChat() {

        final String msg = "Are you sure you want to Delete chat with " + mReceiverName + "?";

        final CustomAlertDialog dialogg = new CustomAlertDialog();
        //  dialogg.setNegativeButtonText("Delete Chat");
        // dialogg.setPositiveButtonText("Cancel");
        dialogg.setNegativeButtonText("Cancel");
        dialogg.setPositiveButtonText("Delete Chat");
        String mMessage = "Also delete  for " + mReceiverName;
        dialogg.setCheckBoxtext(mMessage);
        dialogg.setMessage(msg);
        final String docId;
        // deletestarred = false;

        if (isGroupChat) {
            docId = mCurrentUserId + "-" + to + "-g";
        } else {
            docId = mCurrentUserId + "-" + to;
        }

        dialogg.setCheckBoxCheckedChangeListener(new CustomAlertDialog.OnDialogCheckBoxCheckedChangeListener() {
            @Override
            public void onCheckedChange(boolean isChecked) {
                //       deletestarred = isChecked;
                mCheckedDelete = isChecked;
            }
        });

        dialogg.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {

                //Check it is group or single

                try {
                    JSONObject object = new JSONObject();

                    if (mCheckedDelete) {
                        object.put("delete_opponent", "1");
                    } else {
                        object.put("delete_opponent", "0");
                    }
                    String docId = from + "-" + to;


                    if (mChatData.size() > 0) {
                        String[] array = mChatData.get(mChatData.size() - 1).getMessageId().split("-");
                        Log.e("lastId", "lastId" + mChatData.get(mChatData.size() - 1).getMessageId());
                        Log.e("lastId", "lastId" + array[2]);
                        String mConvId = mChatData.get(mChatData.size() - 1).getConvId();
                        if (AppUtils.isEmpty(mConvId)) {
                            mConvId = userInfoSession.getChatConvId(docId);
                        }
                        object.put("lastId", array[2]);

                        Log.e("mConvId", "mConvId" + mConvId);

                        object.put("convId", mConvId);
                        object.put("MessageId", mChatData.get(mChatData.size() - 1).getMessageId());

                    } else {
                        object.put("lastId", "0");
                        String mConvId = "";
                        if (AppUtils.isEmpty(mConvId)) {
                            mConvId = userInfoSession.getChatConvId(docId);
                        }
                        object.put("convId", mConvId);
                        object.put("MessageId", docId + "-" + "0");

                    }
                    object.put("from", mCurrentUserId);

                    object.put("type", MessageFactory.CHAT_TYPE_SINGLE);
                    // object.put("MessageId", mChatData.get(mChatData.size() - 1).getMessageId());

                    SendMessageEvent event = new SendMessageEvent();
                    event.setEventName(SocketManager.EVENT_DELETE_CHAT);
                    MyLog.e("EVENT_DELETE_CHAT", "EVENT_DELETE_CHAT" + object);
                    event.setMessageObject(object);
                    EventBus.getDefault().post(event);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                //   }

            }

            @Override
            public void onNegativeButtonClick() {
//                Log.e("onNegativeButtonClick", "onNegativeButtonClick" + mChatData.get(mChatData.size() - 1).getMessageId());

                dialogg.dismiss();
            }
        });
        dialogg.show(getSupportFragmentManager(), "Delete member alert");

    }

    private void performMenuEmailChat() {
        final String msg = "Attaching media will generate a large email message.";

        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setNegativeButtonText("Without media");
        dialog.setPositiveButtonText("Attach media");
        dialog.setMessage(msg);
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                dialog.dismiss();
                final ProgressDialog dialogpr = ProgressDialog.show(ChatPageActivity.this, "",
                        "Loading. Please wait...", true);
                final Timer timer2 = new Timer();
                timer2.schedule(new TimerTask() {
                    public void run() {
                        dialogpr.dismiss();
                        timer2.cancel(); //this will cancel the timer of the system
                    }
                }, 2500); // the timer will count 2.5 seconds....


                emailgmail.setVisibility(View.VISIBLE);
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.bottom_up);
                emailgmail.setAnimation(animation);
                dialogpr.dismiss();
                emai1send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gmailintent = false;

                        String docId;
                        String chatType = MessageFactory.CHAT_TYPE_SINGLE;
                        if (isGroupChat) {
                            docId = mCurrentUserId.concat("-").concat(to).concat("-g");
                            chatType = MessageFactory.CHAT_TYPE_GROUP;
                        } else {
                            docId = mCurrentUserId.concat("-").concat(to);
                        }

                        EmailChatHistoryUtils emailChat = new EmailChatHistoryUtils(ChatPageActivity.this);
                        emailChat.send(docId, user_name.getText().toString(), true, false, chatType);

                        emailgmail.setVisibility(View.GONE);

                    }
                });
                gmailsend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //  sendgmail();
                        // textfilesend();
                        gmailintent = true;

                        String docId;
                        String chatType = MessageFactory.CHAT_TYPE_SINGLE;
                        if (isGroupChat) {
                            docId = mCurrentUserId.concat("-").concat(to).concat("-g");
                            chatType = MessageFactory.CHAT_TYPE_GROUP;
                        } else {
                            docId = mCurrentUserId.concat("-").concat(to);
                        }

                        EmailChatHistoryUtils emailChat = new EmailChatHistoryUtils(ChatPageActivity.this);
                        emailChat.send(docId, user_name.getText().toString(), true, true, chatType);

                        emailgmail.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
                emailgmail.setVisibility(View.VISIBLE);
                final ProgressDialog dialogpr = ProgressDialog.show(ChatPageActivity.this, "",
                        "Loading. Please wait...", true);
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.bottom_up);
                emailgmail.setAnimation(animation);
                dialogpr.dismiss();
                emai1send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String docId;
                        String chatType = MessageFactory.CHAT_TYPE_SINGLE;
                        if (isGroupChat) {
                            docId = mCurrentUserId.concat("-").concat(to).concat("-g");
                            chatType = MessageFactory.CHAT_TYPE_GROUP;
                        } else {
                            docId = mCurrentUserId.concat("-").concat(to);
                        }

                        EmailChatHistoryUtils emailChat = new EmailChatHistoryUtils(ChatPageActivity.this);
                        emailChat.send(docId, user_name.getText().toString(), false, false, chatType);

                        //sendgmailmedia();
                        emailgmail.setVisibility(View.GONE);


                    }
                });
                gmailsend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String docId;
                        String chatType = MessageFactory.CHAT_TYPE_SINGLE;
                        if (isGroupChat) {
                            docId = mCurrentUserId.concat("-").concat(to).concat("-g");
                            chatType = MessageFactory.CHAT_TYPE_GROUP;
                        } else {
                            docId = mCurrentUserId.concat("-").concat(to);
                        }

                        EmailChatHistoryUtils emailChat = new EmailChatHistoryUtils(ChatPageActivity.this);
                        emailChat.send(docId, user_name.getText().toString(), false, true, chatType);

                        //   sendgmailmedia();
                        emailgmail.setVisibility(View.GONE);
                    }
                });
            }
        });
        dialog.show(getSupportFragmentManager(), "Delete member alert");
    }

    private void addShortcutConfirmationDialog() {
        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage("Do you want to  create shortcut?");
        dialog.setPositiveButtonText("Ok");
        dialog.setNegativeButtonText("Cancel");
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                String receiverName = user_name.getText().toString();

                Bitmap bitmap = null;
                if (receiverAvatar != null && !receiverAvatar.equals("")) {

                    addShortcut(receiverAvatar, isGroupChat);
                } else {
                    Drawable drawable = user_profile_image.getDrawable();
                    if (drawable != null) {
                        try {
                            myTemp = ((BitmapDrawable) drawable).getBitmap();
                            // bitmap = Bitmap.createScaledBitmap(myTemp, 128, 128, true);
                        } catch (ClassCastException ex) {
                            try {
                                myTemp = ((GlideBitmapDrawable) user_profile_image.getDrawable().getCurrent()).getBitmap();
                                //     bitmap = Bitmap.createScaledBitmap(myTemp, 128, 128, true);

                            } catch (Exception e) {
                                MyLog.e(TAG, "onPositiveButtonClick: ", e);
                            }
                        } catch (Exception ex2) {
                            MyLog.e(TAG, "onPositiveButtonClick: ", ex2);
                        }
                    }
                    ShortcutBadgeManager.addChatShortcut(ChatPageActivity.this, isGroupChat, mReceiverId, receiverName,
                            receiverAvatar, receiverMsisdn, myTemp);
                }


            }

            @Override
            public void onNegativeButtonClick() {

            }
        });
        dialog.show(getSupportFragmentManager(), "CustomAlert");

    }

    private void addShortcut(String path, final boolean isGroupChat) {
        try {
            String receiverId = to;
            if (isGroupChat)
                receiverId = mGroupId;
            MyLog.d(TAG, "loadImage imagetest1: " + path);
            if (isGroupChat && path.isEmpty() || AppUtils.isEmptyImage(path)) {
                MyLog.d(TAG, "loadImage imagetest2 empty image: ");
                ShortcutBadgeManager.addChatShortcut(this, isGroupChat, receiverId, receiverName,
                        receiverAvatar, receiverMsisdn, null);
            } else {
                if (isGroupChat)
                    path = AppUtils.getValidGroupPath(path);
                else {
                    String userImagePath = contactDB_sqlite.getSingleData(receiverId, ContactDB_Sqlite.AVATARIMAGEURL);
                    path = AppUtils.getValidProfilePath(userImagePath);
                }
                BitmapTypeRequest glideRequestmgr = Glide.with(this).load(AppUtils.getGlideURL(path, this)).asBitmap();


                final String finalReceiverId = receiverId;
                glideRequestmgr.diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontTransform()
                        .dontAnimate()
                        .into(new SimpleTarget<Bitmap>() {

                            @Override
                            public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                ShortcutBadgeManager.addChatShortcut(context, isGroupChat, finalReceiverId, receiverName,
                                        receiverAvatar, receiverMsisdn, arg0);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                ShortcutBadgeManager.addChatShortcut(context, isGroupChat, finalReceiverId, receiverName,
                                        receiverAvatar, receiverMsisdn, null);
                            }
                        });
            }
        } catch (Exception e) {
            MyLog.e(TAG, "LoadImage: ", e);
        }


    }

    private void checkAndOpenChatUnlockDialog() {
        boolean isLockChat = getIntent().getBooleanExtra("isLockChat", false);
        if (isLockChat) {
            final String docId;
            if (isGroupChat) {
                chatType = MessageFactory.CHAT_TYPE_GROUP;
                docId = mCurrentUserId + "-" + to + "-g";
            } else {
                chatType = MessageFactory.CHAT_TYPE_SINGLE;
                docId = mCurrentUserId + "-" + to;
            }
            String convId = userInfoSession.getChatConvId(docId);
            String receiverId = userInfoSession.getReceiverIdByConvId(convId);
            ChatLockPojo lockPojo = db.getChatLockData(receiverId, chatType);
            if (lockPojo != null) {
                String stat = lockPojo.getStatus();
                String pwd = lockPojo.getPassword();
                unlockChat(docId, pwd, 0);
            }
        }
    }

    private void unlockChat(String docId, String pwd, int position) {

        String convId = userInfoSession.getChatConvId(docId);
        ChatLockPwdDialog dialog = new ChatLockPwdDialog();
        dialog.setTextLabel1("Enter your Password");
        dialog.setEditTextdata("Enter new Password");
        dialog.setforgotpwdlabel("Forgot Password");
        dialog.setHeader("Unlock Chat");
        dialog.setButtonText("Unlock");
        dialog.setCancelable(false);

        //  Objects.requireNonNull(dialog.getDialog().getWindow()).setDimAmount(1f);
        Bundle bundle = new Bundle();
        // bundle.putSerializable("MessageItem", mChatDat);
        bundle.putString("convID", convId);
        bundle.putString("status", "1");
        bundle.putString("pwd", pwd);
        bundle.putString("page", "chatlist");
        bundle.putString("type", "single");
        bundle.putString("from", mCurrentUserId);
        bundle.putBoolean("isShortCutPasswordCheck", true);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "chatunLock");


    }

    private void performChatlock() {
        if (sessionManager.getLockChatEnabled().equals("1")) {
            emailChatlock = sessionManager.getUserEmailId();
            recemailChatlock = sessionManager.getRecoveryEMailId();
            recPhoneChatlock = sessionManager.getRecoveryPhoneNo();
            String emailVerifyStatus = sessionManager.getChatLockEmailIdVerifyStatus();

            if (!emailChatlock.equals("") && !recemailChatlock.equals("") && !recPhoneChatlock.equals("")
                    && emailVerifyStatus.equalsIgnoreCase("yes")) {

                final String docId;
                if (isGroupChat) {
                    docId = mCurrentUserId + "-" + to + "-g";
                } else {
                    docId = mCurrentUserId + "-" + to;
                }
                String convId = userInfoSession.getChatConvId(docId);
                String receiverId = userInfoSession.getReceiverIdByConvId(convId);
                ChatLockPojo lockPojo = db.getChatLockData(receiverId, chatType);

                if (lockPojo != null) {
                    String stat = lockPojo.getStatus();
                    String pwd = lockPojo.getPassword();
                    if (stat.equals("0")) {
                        if (ConnectivityInfo.isInternetConnected(ChatPageActivity.this)) {
                            openChatLockDialog(docId);
                        } else {
                            showInternetAlert();
                        }
                    } else {
                        openUnlockChatDialog(docId, stat, pwd, true);
                    }
                } else {
                    if (ConnectivityInfo.isInternetConnected(ChatPageActivity.this)) {
                        openChatLockDialog(docId);
                    } else {
                        showInternetAlert();
                    }
                }

            } else {
                Intent intent = new Intent(ChatPageActivity.this, EmailSettings.class);
                startActivity(intent);
            }
        }
    }

    private void openUnlockChatDialog(String docId, String status, String pwd, boolean isCancelable) {

        String convId = userInfoSession.getChatConvId(docId);
        ChatLockPwdDialog dialog = new ChatLockPwdDialog();
        dialog.setTextLabel1("Enter your Password");
        dialog.setEditTextdata("Enter new Password");
        dialog.setforgotpwdlabel("Forgot Password");
        dialog.setHeader("Unlock Chat");
        dialog.setButtonText("Unlock");
        dialog.setCancelable(isCancelable);
        //dialog.getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        Bundle bundle = new Bundle();
        if (docId.contains("-g")) {
            bundle.putString("convID", to);
        } else {
            bundle.putString("convID", convId);
        }
        bundle.putString("status", "1");
        bundle.putString("pwd", pwd);
        bundle.putString("page", "chatview");
        if (docId.contains("-g")) {
            bundle.putString("type", "group");
        } else {
            bundle.putString("type", "single");
        }
        bundle.putString("from", mCurrentUserId);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "chatunLock");

    }

    private void showInternetAlert() {
        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage("Check Your Internet Connection");
        dialog.setNegativeButtonText("cancel");
        dialog.setPositiveButtonText("ok");
        dialog.setCancelable(false);
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                dialog.dismiss();
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();

            }
        });
        dialog.show(getSupportFragmentManager(), "Delete member alert");
    }

/*
    private void itemClicked(int position) {
        if (isFirstItemSelected) {

            MyLog.d(TAG, "@@@@ onItemClick: ");

            long currentMillis = System.currentTimeMillis();
            long difference = (currentMillis - longPressMillis);
            MyLog.d(TAG, "onItemClick: " + difference);

            if (difference < 300)
                return;

            if (isEncryptionInfo(position)) {
                return;
            }


            performSelection(position);
            if (selectedChatItems.size() == 0) {
                showUnSelectedActions();
            }

        }
    }
*/

    private void openChatLockDialog(String docId) {
        convId = userInfoSession.getChatConvId(docId);
        ChatLockPwdDialog dialog = new ChatLockPwdDialog();
        dialog.setTextLabel1("New password");
        dialog.setTextLabel2("Confirm password");
        dialog.setEditTextdata("Enter new Password");
        dialog.setEditTextdata2("Enter Confirm Password");
        dialog.setHeader("Set chat password");
        dialog.setButtonText("Lock");
        Bundle bundle = new Bundle();
        bundle.putString("from", mCurrentUserId);
        if (docId.contains("-g")) {
            bundle.putString("type", "group");
        } else {
            bundle.putString("type", "single");
        }
        bundle.putString("page", "chatview");
        if (docId.contains("-g")) {
            bundle.putString("convID", to);
        } else {
            bundle.putString("convID", convId);
        }

        bundle.putString("status", "0");
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "chatLock");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //  MyLog.d(TAG, "@@@@ onItemClick: ");
        //Hide if any view is visible
        hideMenu();
        itemClicked(position);
    }

    private void itemClicked(int position) {
        if (isFirstItemSelected) {

            MyLog.d(TAG, "@@@@ onItemClick: ");

            long currentMillis = System.currentTimeMillis();
            long difference = (currentMillis - longPressMillis);
            MyLog.d(TAG, "onItemClick: " + difference);

            if (difference < 300)
                return;

            if (isEncryptionInfo(position)) {
                return;
            }


            performSelection(position);
            if (selectedChatItems.size() == 0) {
                showUnSelectedActions();
            }

        }
    }

    private boolean checkIsAlreadyLocked(String docId) {
        String convId = userInfoSession.getChatConvId(docId);
        String receiverId = userInfoSession.getReceiverIdByConvId(convId);
        ChatLockPojo lockPojo = db.getChatLockData(receiverId, chatType);
        if (sessionManager.getLockChatEnabled().equals("1") && lockPojo != null) {
            String stat = lockPojo.getStatus();
            String pwd = lockPojo.getPassword();
            isAlrdychatlocked = stat.equals("1");
        } else {
            isAlrdychatlocked = false;
        }

        return isAlrdychatlocked;
    }

    private void CopyClick() {

        Search1.setText("");

        String finaltext = "";

        if (selectedChatItems.size() == 1) {
            finaltext = selectedChatItems.get(0).getTextMessage();
        } else {

            for (int i = 0; i < selectedChatItems.size(); i++) {
                String mesgid = selectedChatItems.get(i).getMessageId();
                final String[] array = mesgid.split("-");
                if (!isGroupChat) {
                    date = array[2];
                } else {
                    if (array.length > 3) {
                        date = array[3];
                    }
                }

                long l = Long.parseLong(date);

                Date d = new Date(l);
                DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String myDate = sdf.format(d);
                Date dateObj = new Date(l);
                String timeStamp = new SimpleDateFormat("h:mm a", Locale.ENGLISH).format(dateObj);
                timeStamp = timeStamp.replace(".", "");
                String sendername = selectedChatItems.get(i).getSenderName();
                String textmessage = String.valueOf("\n" + "[" + myDate + "," + timeStamp + "]" + sendername + ":" + selectedChatItems.get(i).getTextMessage());
                finaltext = finaltext + textmessage;
            }
        }
        ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("text", finaltext);
        cm.setPrimaryClip(clip);

        Toast.makeText(ChatPageActivity.this, "Message copied", Toast.LENGTH_SHORT).show();

        showUnSelectedActions();

    }

    private void ForwardClick() {

        Bundle bundle = new Bundle();
        //bundle.putSerializable("MsgItemList", selectedChatItems);
        CommonData.setForwardedItems(selectedChatItems);
        bundle.putBoolean("FromSmartty", true);

        Intent intent = new Intent(context, ForwardContact.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_CODE_FORWARD_MSG);
    }

    private void ShareClick(Uri tempUri) {

        String[] blacklist = new String[]{"com.any.package", "net.other.package"};
        Intent intent = new Intent(Intent.ACTION_SEND);
        if (mSelectedType != null) {
            if (mSelectedType.equals("" + MessageFactory.picture)) {
                intent.setType("image/*");
            } else if (mSelectedType.equals("" + MessageFactory.video)) {
                intent.setType("video/*");
            } else if (mSelectedType.equals("" + MessageFactory.audio)) {
                intent.setType("audio/*");
            } else if (mSelectedType.equals("" + MessageFactory.document)) {
                intent.setType("application/*");
            }
        } else
            intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_STREAM, tempUri);
        intent.putExtra(Intent.EXTRA_TEXT, "");
        Intent mIntent = null;
        mIntent = generateCustomChooserIntent(intent, blacklist);
        if (mIntent != null) {
            startActivity(mIntent);
        }
        noNeedRefresh = true;
        //     startActivity(generateCustomChooserIntent(intent, blacklist));


    }

    private Intent generateCustomChooserIntent(Intent prototype, String[] forbiddenChoices) {
        List<Intent> targetedShareIntents = new ArrayList<Intent>();
        List<HashMap<String, String>> intentMetaInfo = new ArrayList<HashMap<String, String>>();
        Intent chooserIntent;

        Intent dummy = new Intent(prototype.getAction());
        dummy.setType(prototype.getType());
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(dummy, 0);

        if (!resInfo.isEmpty()) {
            for (ResolveInfo resolveInfo : resInfo) {
                if (resolveInfo.activityInfo == null || Arrays.asList(forbiddenChoices).contains(resolveInfo.activityInfo.packageName))
                    continue;
                HashMap<String, String> info = new HashMap<String, String>();
                info.put("packageName", resolveInfo.activityInfo.packageName);
                info.put("className", resolveInfo.activityInfo.name);
                info.put("simpleName", String.valueOf(resolveInfo.activityInfo.loadLabel(getPackageManager())));
                intentMetaInfo.add(info);
            }

            if (!intentMetaInfo.isEmpty()) {
                // sorting for nice readability
                Collections.sort(intentMetaInfo, new Comparator<HashMap<String, String>>() {
                    @Override
                    public int compare(HashMap<String, String> map, HashMap<String, String> map2) {
                        return map.get("simpleName").compareTo(map2.get("simpleName"));
                    }
                });

                // create the custom intent list
                for (HashMap<String, String> metaInfo : intentMetaInfo) {
                    Intent targetedShareIntent = (Intent) prototype.clone();
                    targetedShareIntent.setPackage(metaInfo.get("packageName"));
                    targetedShareIntent.setClassName(metaInfo.get("packageName"), metaInfo.get("className"));
                    targetedShareIntents.add(targetedShareIntent);
                }


                chooserIntent = Intent.createChooser(targetedShareIntents.remove(targetedShareIntents.size() - 1), "share");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
                return chooserIntent;
            }
        }

        return Intent.createChooser(prototype, "Share it");
    }

    private void DeleteMessageClick() {

        Search1.setText("");

        if (Delete_Type.equalsIgnoreCase("delete received message")) {
            final CustomAlertDialog dialog = new CustomAlertDialog();
            dialog.setMessage("Are you sure you want to delete this message?");
            dialog.setNegativeButtonText("No");
            dialog.setPositiveButtonText("Yes");
            dialog.setCancelable(false);
            dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
                @Override
                public void onPositiveButtonClick() {


                    deleteMsgs();
                }

                @Override
                public void onNegativeButtonClick() {
                    dialog.dismiss();

                }
            });
            dialog.show(getSupportFragmentManager(), "Delete member alert");
        } else if (Delete_Type.equalsIgnoreCase("delete sent message")) {
            final CustomDeleteDialog dialog = new CustomDeleteDialog();
            dialog.setMessage("Are you sure you want to delete this message?");
            dialog.setForMeButtonText("Delete for me");
            dialog.setEveryOneButtonText("Delete for everyone");
            dialog.setCancelButtonText("Cancel");
            dialog.setCancelable(false);
            dialog.setDeleteDialogCloseListener(new CustomDeleteDialog.OnDeleteDialogCloseListener() {
                @Override
                public void onForMeButtonClick() {
                    deleteMsgs();
                }

                @Override
                public void onEveryOneButtonClick() {
                    for (int i = 0; i < selectedChatItems.size(); i++) {
                        MessageItemChat msgItem = selectedChatItems.get(i);

                        if (msgItem.getRecordId() != null && !msgItem.getRecordId().equalsIgnoreCase("")) {

                            if (msgItem.isMediaPlaying()) {
                                int chatIndex = mChatData.indexOf(msgItem);
                                madapter.stopAudioOnMessageDelete(chatIndex);
                            }
                            String lastMsgStatus;
                            if (mChatData.size() == 1) {
                                lastMsgStatus = "1";
                            } else {
                                lastMsgStatus = "0";
                            }
                            String docId = from + "-" + to;
                            if (isGroupChat) {
                                docId = docId + "-g";
                                mConvId = mGroupId;
                            } else {
                                if (mConvId == null) {
                                    mConvId = msgItem.getConvId();
                                }
                            }
                            //     SendMessageEvent messageEvent = new SendMessageEvent();
                            //      messageEvent.setEventName(SocketManager.EVENT_REMOVE_MESSAGE);

                            String type = msgItem.getMessageType();

                            if (type.equalsIgnoreCase("1") || type.equalsIgnoreCase("2")
                                    || type.equalsIgnoreCase("3") || type.equalsIgnoreCase("6")) {


                                try {

                                    db.DeleteFileStatusUpdate(msgItem.getMessageId(), "delete");

                                } catch (Exception e) {

                                }


                            }


                            try {
                                JSONObject deleteMsgObj = new JSONObject();
                                deleteMsgObj.put("from", from);
                                deleteMsgObj.put("type", chatType);
                                deleteMsgObj.put("convId", mConvId);
                                deleteMsgObj.put("status", "1");
                                deleteMsgObj.put("recordId", msgItem.getRecordId());
                                deleteMsgObj.put("last_msg", lastMsgStatus);
                                //  messageEvent.setMessageObject(deleteMsgObj);
                                // EventBus.getDefault().post(messageEvent);
                                int index = mChatData.indexOf(selectedChatItems.get(i));

                                //--------------Delete Chat--------------
                                SendMessageEvent Deletemessage = new SendMessageEvent();
                                Deletemessage.setEventName(SocketManager.EVENT_DELETE_MESSAGE);
                                try {
                                    JSONObject deleteMsg = new JSONObject();
                                    deleteMsg.put("from", from);
                                    deleteMsg.put("recordId", msgItem.getRecordId());
                                    deleteMsg.put("convId", mConvId);
                                    if (isGroupChat) {
                                        deleteMsg.put("type", "group");
                                    } else {
                                        deleteMsg.put("type", "single");
                                    }
                                    Deletemessage.setMessageObject(deleteMsg);
                                    EventBus.getDefault().post(Deletemessage);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }


                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }

                        } else {

                            DeleteSenderSide(msgItem, i);
                        }

                    }

                    showUnSelectedActions();
                }

                @Override
                public void onCancelButtonClick() {
                    dialog.dismiss();
                }
            });
            dialog.show(getSupportFragmentManager(), "Delete member alert");
        }

        //view.setSelected(false);

    }

    private void deleteMsgs() {
        for (int i = 0; i < selectedChatItems.size(); i++) {

            MessageItemChat msgItem = selectedChatItems.get(i);

            if (msgItem.isMediaPlaying()) {
                int chatIndex = mChatData.indexOf(msgItem);
                madapter.stopAudioOnMessageDelete(chatIndex);
            }

            String lastMsgStatus;
            if (mChatData.size() == 1) {
                lastMsgStatus = "1";
            } else {
                lastMsgStatus = "0";
            }

            String docId = from + "-" + to;
            if (isGroupChat) {
                docId = docId + "-g";
                mConvId = mGroupId;
            } else {
                if (mConvId == null) {
                    mConvId = msgItem.getConvId();
                }
            }

            SendMessageEvent messageEvent = new SendMessageEvent();
            messageEvent.setEventName(SocketManager.EVENT_REMOVE_MESSAGE);
            try {
                JSONObject deleteMsgObj = new JSONObject();
                deleteMsgObj.put("from", from);
                deleteMsgObj.put("type", chatType);
                deleteMsgObj.put("convId", mConvId);
                deleteMsgObj.put("status", "1");
                deleteMsgObj.put("recordId", msgItem.getRecordId());
                deleteMsgObj.put("last_msg", lastMsgStatus);
                messageEvent.setMessageObject(deleteMsgObj);
                EventBus.getDefault().post(messageEvent);

                db.deleteChatMessage(docId, msgItem.getMessageId(), chatType);
                db.updateTempDeletedMessage(msgItem.getRecordId(), deleteMsgObj);

                int index = mChatData.indexOf(selectedChatItems.get(i));
                if (index > -1) {
                    mChatData.remove(index);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        Log.e(TAG, "deleteMsgs: notifyDataSetChanged");

        madapter.notifyDataSetChanged();
        showUnSelectedActions();
    }

    //-----------------Every one delete record id null scenario----------------
    private void DeleteSenderSide(MessageItemChat msgItem, int i) {

        if (msgItem.isMediaPlaying()) {
            int chatIndex = mChatData.indexOf(msgItem);
            madapter.stopAudioOnMessageDelete(chatIndex);
        }
        String lastMsgStatus;
        if (mChatData.size() == 1) {
            lastMsgStatus = "1";
        } else {
            lastMsgStatus = "0";
        }
        String docId = from + "-" + to;
        if (isGroupChat) {
            docId = docId + "-g";
            mConvId = mGroupId;
        } else {
            if (mConvId == null) {
                mConvId = msgItem.getConvId();
            }
        }

        String type = msgItem.getMessageType();

        if (type.equalsIgnoreCase("1") || type.equalsIgnoreCase("2")
                || type.equalsIgnoreCase("3") || type.equalsIgnoreCase("6")) {


            try {

                db.DeleteFileStatusUpdate(msgItem.getMessageId(), "delete");

            } catch (Exception e) {

            }


        }

        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_REMOVE_MESSAGE);
        try {
            JSONObject deleteMsgObj = new JSONObject();
            deleteMsgObj.put("from", from);
            deleteMsgObj.put("type", chatType);
            deleteMsgObj.put("convId", mConvId);
            deleteMsgObj.put("status", "1");
            deleteMsgObj.put("recordId", msgItem.getRecordId());
            deleteMsgObj.put("last_msg", lastMsgStatus);
            messageEvent.setMessageObject(deleteMsgObj);
            EventBus.getDefault().post(messageEvent);
            int index = mChatData.indexOf(selectedChatItems.get(i));
            mChatData.get(index).setMessageType(MessageFactory.DELETE_SELF + "");
            mChatData.get(index).setIsSelf(true);


            if (isGroupChat) {
                String str_ids = msgItem.getMessageId();
                String[] ids = str_ids.split("-");
                String groupAndMsgId = ids[1] + "-g-" + ids[3];
                Log.e(TAG, "DeleteSenderSide: notifyDataSetChanged");

                madapter.notifyDataSetChanged();
                db.deleteSingleMessage(groupAndMsgId, str_ids, chatType, "self");
                db.deleteChatListPage(groupAndMsgId, str_ids, chatType, "self");
            } else {
                Log.e(TAG, "DeleteSenderSide: notifyDataSetChanged");

                madapter.notifyDataSetChanged();
                db.deleteSingleMessage(docId, msgItem.getMessageId(), chatType, "self");
                db.deleteChatListPage(docId, msgItem.getMessageId(), chatType, "self");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void StartMessageClick() {

        Search1.setText("");

        for (int i = 0; i < selectedChatItems.size(); i++) {
            MessageItemChat msgItem = selectedChatItems.get(i);

            // Add only un-starNextLine if don't have selected starNextLine messages
            //Check it is downloaded or not
            //  Log.e("msgItem.getDownloadStatus()", "msgItem.getDownloadStatus()" + msgItem.getDownloadStatus());
            // Log.e("msgItem.getMessageType()", "msgItem.getMessageType()" + msgItem.getMessageType());

            //    Log.e("msgItem.getDownloadStatus()", "msgItem.getDownloadStatus()" + msgItem.toString());
            if (is_telpon_chat) {
                if (msgItem.getDownloadStatus() != 0 || Integer.parseInt(msgItem.getMessageType()) == 0) {


                    String starStatus;
                    if (isSelectedWithUnStarMsg) {
                        starStatus = MessageFactory.MESSAGE_STARRED;
                    } else {
                        if (msgItem.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                            starStatus = MessageFactory.MESSAGE_UN_STARRED;
                        } else {
                            starStatus = MessageFactory.MESSAGE_STARRED;
                        }
                    }

                    SendMessageEvent messageEvent = new SendMessageEvent();
                    messageEvent.setEventName(SocketManager.EVENT_STAR_MESSAGE);
                    try {
                        JSONObject starMsgObj = new JSONObject();
                        starMsgObj.put("from", from);
                        starMsgObj.put("type", chatType);
                        starMsgObj.put("status", starStatus);
                        starMsgObj.put("recordId", msgItem.getRecordId());
                        starMsgObj.put("doc_id", msgItem.getMessageId());
                        messageEvent.setMessageObject(starMsgObj);

                        if (isGroupChat) {
                            starMsgObj.put("convId", mGroupId);
                            EventBus.getDefault().post(messageEvent);

                            db.updateStarredMessage(msgItem.getMessageId(), starStatus, MessageFactory.CHAT_TYPE_GROUP);
                            db.updateTempStarredMessage(msgItem.getRecordId(), starMsgObj);
                        } else {
                            if (mConvId != null && !mConvId.equals("")) {
                                starMsgObj.put("convId", mConvId);
                                EventBus.getDefault().post(messageEvent);

                                db.updateStarredMessage(msgItem.getMessageId(), starStatus, MessageFactory.CHAT_TYPE_SINGLE);
                                db.updateTempStarredMessage(msgItem.getRecordId(), starMsgObj);
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    String docId = from + "-" + to;
                    if (isGroupChat) {
                        docId = docId + "-g";
                    }


                    int index = mChatData.indexOf(selectedChatItems.get(i));
                    if (index > -1) {
                        mChatData.get(index).setStarredStatus(starStatus);
                        mChatData.get(index).setSelected(false);
                    }
                }
            } else {
                if (msgItem.getDownloadStatus() != 0 || Integer.parseInt(msgItem.getMessageType()) == 0) {


                    String starStatus;
                    if (isSelectedWithUnStarMsg) {
                        starStatus = MessageFactory.MESSAGE_STARRED;
                    } else {
                        if (msgItem.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                            starStatus = MessageFactory.MESSAGE_UN_STARRED;
                        } else {
                            starStatus = MessageFactory.MESSAGE_STARRED;
                        }
                    }

                    SendMessageEvent messageEvent = new SendMessageEvent();
                    messageEvent.setEventName(SocketManager.EVENT_STAR_MESSAGE);
                    try {
                        JSONObject starMsgObj = new JSONObject();
                        starMsgObj.put("from", from);
                        starMsgObj.put("type", chatType);
                        starMsgObj.put("status", starStatus);
                        starMsgObj.put("recordId", msgItem.getRecordId());
                        starMsgObj.put("doc_id", msgItem.getMessageId());
                        messageEvent.setMessageObject(starMsgObj);

                        if (isGroupChat) {
                            starMsgObj.put("convId", mGroupId);
                            EventBus.getDefault().post(messageEvent);

                            db.updateStarredMessage(msgItem.getMessageId(), starStatus, MessageFactory.CHAT_TYPE_GROUP);
                            db.updateTempStarredMessage(msgItem.getRecordId(), starMsgObj);
                        } else {
                            if (mConvId != null && !mConvId.equals("")) {
                                starMsgObj.put("convId", mConvId);
                                EventBus.getDefault().post(messageEvent);

                                db.updateStarredMessage(msgItem.getMessageId(), starStatus, MessageFactory.CHAT_TYPE_SINGLE);
                                db.updateTempStarredMessage(msgItem.getRecordId(), starMsgObj);
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    String docId = from + "-" + to;
                    if (isGroupChat) {
                        docId = docId + "-g";
                    }


                    int index = mChatData.indexOf(selectedChatItems.get(i));
                    if (index > -1) {
                        mChatData.get(index).setStarredStatus(starStatus);
                        mChatData.get(index).setSelected(false);
                    }
                }
            }


            showUnSelectedActions();
        }
    }

    //------------------------------Chat List Long Item Click Listener--------------------------------------------

 /*   @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {

        boolean selection = mChatData.get(position).isSelected();

        boolean isDeletedMsgItem = performSelection(position);
        if (isEncryptionInfo(position)) {
            return false;
        }
        if (selectedChatItems.size() <= 0) {
            showUnSelectedActions();

        } else {
            isFirstItemSelected = true;
            iBtnBack2.setVisibility(View.GONE);
            Search1.setVisibility(View.GONE);
        }

        MyLog.d(TAG, "@@@@ onItemLongClick: long press");
        longPressMillis = System.currentTimeMillis();
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MessageItemChat messageItemChat = mChatData.get(position);

                if (!isGroupChat) {
                    Intent intent = new Intent(context, SingleMessageInfoActivity.class);
                    intent.putExtra("selectedData", messageItemChat);
                    startActivity(intent);

                } else {

                    Intent groupinfointent = new Intent(context, GroupMessageInfoActivity.class);
                    groupinfointent.putExtra("selectedData", messageItemChat);
                    startActivity(groupinfointent);
                }

                mChatData.get(0).setSelected(false);
                madapter.notifyDataSetChanged();
                showUnSelectedActions();
            }
        });


        return true;
    }
*/

    private void ReplayMessageClick() {

        Search1.setText("");

        if (selectedChatItems.size() == 1) {

            r1messagetoreplay.setVisibility(View.VISIBLE);
            reply = true;
            MessageItemChat msgItem = selectedChatItems.get(0);
            int type = Integer.parseInt(msgItem.getMessageType());
            mymsgid = msgItem.getRecordId();
            replytype = msgItem.getMessageType();

            //session.putposition(postionreplay);
            personimage.setVisibility(View.GONE);
            Documentimage.setVisibility(View.GONE);
            cameraphoto.setVisibility(View.GONE);
            audioimage.setVisibility(View.GONE);
            sentimage.setVisibility(View.GONE);
            videoimage.setVisibility(View.GONE);
            messagesetmedio.setVisibility(View.GONE);
            ImageView ivLocation = findViewById(R.id.ivLocation);
            ivLocation.setVisibility(View.GONE);

            if (msgItem.isSelf() && !isGroupChat) {
                ReplySender = "you";
                Ifname.setText("You");
                if (MessageFactory.picture == type) {
                    messagesetmedio.setVisibility(View.VISIBLE);
                    sentimage.setVisibility(View.VISIBLE);
                    cameraphoto.setVisibility(View.VISIBLE);
                    message_old.setVisibility(View.GONE);
                    messagesetmedio.setText("Photo");
                    sentimage.setVisibility(View.VISIBLE);
                    try {
                        imgpath = msgItem.getImagePath();
                        imageAsBytes = msgItem.getThumbnailData();
                        sentimage.setImageBitmap(SmarttyImageUtils.decodeBitmapFromBase64(
                                imageAsBytes, 50, 50));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (MessageFactory.text == type) {
                    message_old.setVisibility(View.VISIBLE);
                    messageold = msgItem.getTextMessage();
                    message_old.setText(messageold);
                    message_old.setTextColor(getResources().getColor(R.color.title));
                } else if (MessageFactory.web_link == type) {
                    message_old.setVisibility(View.VISIBLE);
                    messageold = msgItem.getWebLinkTitle();
                    message_old.setText(messageold);
                } else if (MessageFactory.document == type) {
                    messagesetmedio.setVisibility(View.VISIBLE);
                    Documentimage.setVisibility(View.VISIBLE);
                    message_old.setVisibility(View.GONE);
                    messageold = msgItem.getTextMessage();
                    messagesetmedio.setText(messageold);
                } else if (MessageFactory.video == type) {
                    messagesetmedio.setVisibility(View.VISIBLE);
                    videoimage.setVisibility(View.VISIBLE);
                    message_old.setVisibility(View.GONE);
                    messagesetmedio.setText("Video");
                    sentimage.setVisibility(View.VISIBLE);
                    try {
                        imageAsBytes = msgItem.getThumbnailData().replace("data:image/jpeg;base64,", "");
                        Bitmap photo = ConvertToImage(imageAsBytes);
                        sentimage.setImageBitmap(photo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (MessageFactory.audio == type) {
                    messagesetmedio.setVisibility(View.VISIBLE);
                    audioimage.setVisibility(View.VISIBLE);
                    message_old.setVisibility(View.GONE);
                    messagesetmedio.setText("Audio");
                } else if (MessageFactory.contact == type) {
                    contactname = "" + msgItem.getContactName();
                    messagesetmedio.setVisibility(View.VISIBLE);
                    personimage.setVisibility(View.VISIBLE);
                    message_old.setVisibility(View.GONE);
                    messagesetmedio.setText(contactname);
                } else if (MessageFactory.location == type) {
                    messagesetmedio.setVisibility(View.VISIBLE);
                    ivLocation.setVisibility(View.VISIBLE);
                    message_old.setVisibility(View.GONE);
                    messagesetmedio.setText(msgItem.getWebLinkTitle());
                    sentimage.setVisibility(View.VISIBLE);
                    locationName = msgItem.getWebLinkTitle();
                    try {
                        imageAsBytes = msgItem.getWebLinkImgThumb().replace("data:image/jpeg;base64,", "");
                        Bitmap photo = ConvertToImage(imageAsBytes);
                        sentimage.setImageBitmap(photo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //  msgItem.setReplySender("you");
            } else if (!msgItem.isSelf() && !isGroupChat) {
                if (MessageFactory.picture == type) {
                    messagesetmedio.setVisibility(View.VISIBLE);
                    cameraphoto.setVisibility(View.VISIBLE);
                    message_old.setVisibility(View.GONE);
                    messagesetmedio.setText("Photo");
                    sentimage.setVisibility(View.VISIBLE);
                    try {
                        imgpath = msgItem.getChatFileLocalPath();
                        imageAsBytes = msgItem.getThumbnailData();
                        sentimage.setImageBitmap(SmarttyImageUtils.decodeBitmapFromBase64(
                                imageAsBytes, 50, 50));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (MessageFactory.text == type) {
                    message_old.setVisibility(View.VISIBLE);
                    messageold = msgItem.getTextMessage();
                    message_old.setTextColor(getResources().getColor(R.color.title));
                    message_old.setText(messageold);

                } else if (MessageFactory.web_link == type) {
                    message_old.setVisibility(View.VISIBLE);
                    messageold = msgItem.getTextMessage();
                    message_old.setText(messageold);
                } else if (MessageFactory.document == type) {
                    messagesetmedio.setVisibility(View.VISIBLE);
                    Documentimage.setVisibility(View.VISIBLE);
                    message_old.setVisibility(View.GONE);
                    messageold = msgItem.getTextMessage();
                    messagesetmedio.setText(messageold);
                } else if (MessageFactory.video == type) {
                    messagesetmedio.setVisibility(View.VISIBLE);
                    videoimage.setVisibility(View.VISIBLE);
                    message_old.setVisibility(View.GONE);
                    messagesetmedio.setText("Video");
                    sentimage.setVisibility(View.VISIBLE);
                    try {
                        imageAsBytes = msgItem.getThumbnailData().replace("data:image/jpeg;base64,", "");
                        Bitmap photo = ConvertToImage(imageAsBytes);
                        sentimage.setImageBitmap(photo);
//                Picasso.with(mContext).load(imgPath).into(vh2.imageView);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (MessageFactory.audio == type) {
                    messagesetmedio.setVisibility(View.VISIBLE);
                    audioimage.setVisibility(View.VISIBLE);
                    message_old.setVisibility(View.GONE);
                    messagesetmedio.setText("Audio");
                } else if (MessageFactory.contact == type) {
                    contactname = "Contact:" + msgItem.getContactName();
                    messagesetmedio.setVisibility(View.VISIBLE);
                    personimage.setVisibility(View.VISIBLE);
                    message_old.setVisibility(View.GONE);
                    messagesetmedio.setText(contactname);
                } else if (MessageFactory.location == type) {
                    messagesetmedio.setVisibility(View.VISIBLE);
                    ivLocation.setVisibility(View.VISIBLE);
                    message_old.setVisibility(View.GONE);
                    messagesetmedio.setText(msgItem.getWebLinkTitle());
                    sentimage.setVisibility(View.VISIBLE);
                    locationName = msgItem.getWebLinkTitle();
                    try {
                        imageAsBytes = msgItem.getWebLinkImgThumb().replace("data:image/jpeg;base64,", "");
                        Bitmap photo = ConvertToImage(imageAsBytes);
                        sentimage.setImageBitmap(photo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
//                Picasso.with(mContext).load(imgPath).into(vh2.imageView);
                ReplySender = mReceiverName;
                Ifname.setText(mReceiverName);
            } else if (isGroupChat) {
                if (msgItem.isSelf()) {
                    ReplySender = "You";
                    Ifname.setText(ReplySender);
                    if (MessageFactory.picture == type) {
                        messagesetmedio.setVisibility(View.VISIBLE);
                        cameraphoto.setVisibility(View.VISIBLE);
                        message_old.setVisibility(View.GONE);
                        messagesetmedio.setText("Photo");
                        sentimage.setVisibility(View.VISIBLE);
                        try {
                            imgpath = msgItem.getImagePath();
                            imageAsBytes = msgItem.getThumbnailData();
                            sentimage.setImageBitmap(SmarttyImageUtils.decodeBitmapFromBase64(
                                    imageAsBytes, 50, 50));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (MessageFactory.text == type) {
                        message_old.setVisibility(View.VISIBLE);
                        messageold = msgItem.getTextMessage();
                        message_old.setText(messageold);
                    } else if (MessageFactory.web_link == type) {
                        message_old.setVisibility(View.VISIBLE);
                        messageold = msgItem.getTextMessage();
                        message_old.setText(messageold);
                    } else if (MessageFactory.document == type) {
                        messagesetmedio.setVisibility(View.VISIBLE);
                        Documentimage.setVisibility(View.VISIBLE);
                        message_old.setVisibility(View.GONE);
                        messageold = msgItem.getTextMessage();
                        messagesetmedio.setText(messageold);
                    } else if (MessageFactory.video == type) {
                        messagesetmedio.setVisibility(View.VISIBLE);
                        videoimage.setVisibility(View.VISIBLE);
                        message_old.setVisibility(View.GONE);
                        messagesetmedio.setText("Video");
                        sentimage.setVisibility(View.VISIBLE);
                        try {
                            imageAsBytes = msgItem.getThumbnailData().replace("data:image/jpeg;base64,", "");
                            Bitmap photo = ConvertToImage(imageAsBytes);
                            sentimage.setImageBitmap(photo);
//                Picasso.with(mContext).load(imgPath).into(vh2.imageView);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (MessageFactory.audio == type) {
                        messagesetmedio.setVisibility(View.VISIBLE);
                        audioimage.setVisibility(View.VISIBLE);
                        message_old.setVisibility(View.GONE);
                        messagesetmedio.setText("Audio");
                    } else if (MessageFactory.contact == type) {
                        contactname = "Contact:" + msgItem.getContactName();
                        messagesetmedio.setVisibility(View.VISIBLE);
                        personimage.setVisibility(View.VISIBLE);
                        message_old.setVisibility(View.GONE);
                        messagesetmedio.setText(contactname);
                    } else if (MessageFactory.location == type) {
                        messagesetmedio.setVisibility(View.VISIBLE);
                        ivLocation.setVisibility(View.VISIBLE);
                        message_old.setVisibility(View.GONE);
                        messagesetmedio.setText(msgItem.getWebLinkTitle());
                        sentimage.setVisibility(View.VISIBLE);
                        locationName = msgItem.getWebLinkTitle();
                        try {
                            imageAsBytes = msgItem.getWebLinkImgThumb().replace("data:image/jpeg;base64,", "");
                            Bitmap photo = ConvertToImage(imageAsBytes);
                            sentimage.setImageBitmap(photo);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    ReplySender = getcontactname.getSendername(msgItem.getGroupMsgFrom(), msgItem.getSenderName());
                    Ifname.setText(ReplySender);
                    if (MessageFactory.picture == type) {
                        cameraphoto.setVisibility(View.VISIBLE);
                        message_old.setVisibility(View.GONE);
                        messagesetmedio.setVisibility(View.VISIBLE);
                        messagesetmedio.setText("Photo");
                        sentimage.setVisibility(View.VISIBLE);
                        try {
                            imgpath = msgItem.getChatFileLocalPath();
                            imageAsBytes = msgItem.getThumbnailData();
                            sentimage.setImageBitmap(SmarttyImageUtils.decodeBitmapFromBase64(
                                    imageAsBytes, 50, 50));

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (MessageFactory.text == type) {
                        message_old.setVisibility(View.VISIBLE);
                        messageold = msgItem.getTextMessage();
                        message_old.setText(messageold);
                    } else if (MessageFactory.web_link == type) {
                        message_old.setVisibility(View.VISIBLE);
                        messageold = msgItem.getTextMessage();
                        message_old.setText(messageold);
                    } else if (MessageFactory.document == type) {
                        messagesetmedio.setVisibility(View.VISIBLE);
                        Documentimage.setVisibility(View.VISIBLE);
                        message_old.setVisibility(View.GONE);
                        messageold = msgItem.getTextMessage();
                    } else if (MessageFactory.video == type) {
                        messagesetmedio.setVisibility(View.VISIBLE);
                        videoimage.setVisibility(View.VISIBLE);
                        message_old.setVisibility(View.GONE);
                        messagesetmedio.setText("Video");
                        sentimage.setVisibility(View.VISIBLE);
                        try {
                            imageAsBytes = msgItem.getThumbnailData().replace("data:image/jpeg;base64,", "");
                            Bitmap photo = ConvertToImage(imageAsBytes);
                            sentimage.setImageBitmap(photo);
//                Picasso.with(mContext).load(imgPath).into(vh2.imageView);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
//                Picasso.with(mContext).load(imgPath).into(vh2.imageView);
                    else if (MessageFactory.audio == type) {
                        audioimage.setVisibility(View.VISIBLE);
                        messagesetmedio.setVisibility(View.VISIBLE);
                        message_old.setVisibility(View.GONE);
                        messagesetmedio.setText("Photo");
                    } else if (MessageFactory.contact == type) {
                        contactname = "Contact:" + msgItem.getContactName();
                        messagesetmedio.setVisibility(View.VISIBLE);
                        personimage.setVisibility(View.VISIBLE);
                        message_old.setVisibility(View.GONE);
                        messagesetmedio.setText(contactname);
                    } else if (MessageFactory.location == type) {
                        messagesetmedio.setVisibility(View.VISIBLE);
                        ivLocation.setVisibility(View.VISIBLE);
                        message_old.setVisibility(View.GONE);
                        messagesetmedio.setText(msgItem.getWebLinkTitle());
                        sentimage.setVisibility(View.VISIBLE);
                        locationName = msgItem.getWebLinkTitle();
                        try {
                            imageAsBytes = msgItem.getWebLinkImgThumb().replace("data:image/jpeg;base64,", "");
                            Bitmap photo = ConvertToImage(imageAsBytes);
                            sentimage.setImageBitmap(photo);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            }

            //msgItem.setReplySender(username);

            showUnSelectedActions();
        }
    }

/*

    private void showUnSelectedActions() {
        include.setVisibility(View.VISIBLE);
        rlChatActions.setVisibility(View.GONE);
        //   share.setVisibility(View.GONE);
        isFirstItemSelected = false;
        selectedChatItems.clear();

        for (int i = 0; i < mChatData.size(); i++) {
            mChatData.get(i).setSelected(false);
        }
        if (null != madapter)
            madapter.notifyDataSetChanged();
    }
*/

    public Bitmap ConvertToImage(String image) {
        byte[] imageAsBytes = Base64.decode(image.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {

        boolean selection = mChatData.get(position).isSelected();

        boolean isDeletedMsgItem = performSelection(position);
        if (isEncryptionInfo(position)) {
            return false;
        }
        if (selectedChatItems.size() <= 0) {
            showUnSelectedActions();

        } else {
            isFirstItemSelected = true;
            iBtnBack2.setVisibility(View.GONE);
            Search1.setVisibility(View.GONE);

            //CheckMessageItem

         /*   String msgType = mChatData.get(position).getMessageType();
            int type = Integer.parseInt(msgType);
            if (mChatData.get(position).getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {


                if (type == MessageFactory.audio ||
                        type == MessageFactory.document ||
                        type == MessageFactory.video ||
                        type == MessageFactory.picture) {
                    mSelectedPath = mChatData.get(position).getChatFileLocalPath();
                    share.setVisibility(View.VISIBLE);
                } else {
                    share.setVisibility(View.GONE);
                }
            } else {
                share.setVisibility(View.GONE);

            }*/

        }

        MyLog.d(TAG, "@@@@ onItemLongClick: long press");
        longPressMillis = System.currentTimeMillis();
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MessageItemChat messageItemChat = mChatData.get(position);

                if (!isGroupChat) {
                    Intent intent = new Intent(context, SingleMessageInfoActivity.class);
                    intent.putExtra("selectedData", messageItemChat);
                    startActivity(intent);

                } else {

                    Intent groupinfointent = new Intent(context, GroupMessageInfoActivity.class);
                    groupinfointent.putExtra("selectedData", messageItemChat);
                    startActivity(groupinfointent);
                }

                mChatData.get(0).setSelected(false);

                Log.e(TAG, "onItemLongClick: notifyDataSetChanged");

                madapter.notifyDataSetChanged();
                showUnSelectedActions();
            }
        });


        return true;
    }

    //------------------------------Perform Click Action---------------------------------------------------------------

    private void showUnSelectedActions() {
        include.setVisibility(View.VISIBLE);
        rlChatActions.setVisibility(View.GONE);
        //   share.setVisibility(View.GONE);
        isFirstItemSelected = false;
        selectedChatItems.clear();

        for (int i = 0; i < mChatData.size(); i++) {
            mChatData.get(i).setSelected(false);
        }
        if (null != madapter) {
            Log.e(TAG, "showUnSelectedActions: notifyDataSetChanged");

            madapter.notifyDataSetChanged();
        }

    }

    private boolean isEncryptionInfo(int position) {
        try {
            MessageItemChat msgItem = mChatData.get(position);
            String msgType = msgItem.getMessageType();
            if (msgType.equals("" + MessageFactory.ENCRYPTION_INFO)) {
                return true;
            }
        } catch (Exception e) {
            MyLog.e(TAG, "isEncryptionInfo: ", e);
        }
        return false;
    }

    private boolean performSelection(int position) {

        MessageItemChat msgItem = mChatData.get(position);
        // MessageItemChat msgItem = (MessageItemChat) madapter.getItem(position);
        String msgType = msgItem.getMessageType();
        boolean SelfType = msgItem.isSelf();

        if (msgType.equalsIgnoreCase(MessageFactory.DELETE_SELF + "") || msgType.equalsIgnoreCase(MessageFactory.DELETE_OTHER + "")) {
            Delete_Type = "delete received message";
//            return true;
        } else if ((!msgType.equalsIgnoreCase(MessageFactory.DELETE_SELF + "") || !msgType.equalsIgnoreCase(MessageFactory.DELETE_OTHER + "")) && !SelfType) {
            Delete_Type = "delete received message";
        } else {
            Delete_Type = "delete sent message";
        }

        if (!msgItem.isInfoMsg() && !msgItem.getMessageType().equals(MessageFactory.missed_call + "")) {
            mypath = msgItem.getChatFileLocalPath();
            //int index = mChatData.indexOf(msgItem);
            mChatData.get(position).setSelected(!msgItem.isSelected());
            if (selectedChatItems.contains(msgItem)) {
                selectedChatItems.remove(msgItem);
            } else {
                selectedChatItems.add(msgItem);
            }
            for (MessageItemChat chat : selectedChatItems) {
                if (chat.getMessageType().equals(MessageFactory.text + "")
                        || (chat.getMessageType().equals(MessageFactory.contact + ""))
                        || (chat.getMessageType().equals(MessageFactory.web_link + ""))
                        || (chat.getMessageType().equals(MessageFactory.location + ""))) {
                    forward.setVisibility(View.VISIBLE);

                } else {
                    if (chat.isSelf()) {
                        if (chat.getUploadStatus() == MessageFactory.UPLOAD_STATUS_COMPLETED) {
                            forward.setVisibility(View.VISIBLE);
                        } else {
                            forward.setVisibility(View.GONE);
                            break;
                        }
                    } else {
                        if (chat.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                            forward.setVisibility(View.VISIBLE);
                        } else {
                            forward.setVisibility(View.GONE);

                            break;
                        }
                    }
                }
            }
            // Add copy action only if all selected messages as text
            boolean allTextMsg = true;
            boolean isDeletedMsg = false;
            for (MessageItemChat selectedItem : selectedChatItems) {
                if (selectedItem.getMessageType().equals(MessageFactory.DELETE_SELF + "") ||
                        selectedItem.getMessageType().equals(MessageFactory.DELETE_OTHER + "")) {
                    isDeletedMsg = true;
                    break;
                }
            }

            for (MessageItemChat selectedItem : selectedChatItems) {
                if (!selectedItem.getMessageType().equals(MessageFactory.text + "")
                        && !selectedItem.getMessageType().equals(MessageFactory.web_link + "")) {
                    allTextMsg = false;
                    break;
                }

            }
            if (allTextMsg) {
                copychat.setVisibility(View.VISIBLE);
            } else {
                copychat.setVisibility(View.GONE);

            }

            if (selectedChatItems.size() > 0) {
                isFirstItemSelected = true;
                if (selectedChatItems.size() == 1) {
                    replymess.setVisibility(View.VISIBLE);

                    if (selectedChatItems.get(0).isSelf() && !isDeletedMsg) {
                        info.setVisibility(View.VISIBLE);
                    } else {
                        info.setVisibility(View.GONE);
                    }
                    String msgTypee = selectedChatItems.get(0).getMessageType();
                    int type = Integer.parseInt(msgTypee);
                    boolean canShowShareImage = false;
                    if (selectedChatItems.get(0).isSelf()) {
                        if (type == MessageFactory.audio ||
                                type == MessageFactory.document ||
                                type == MessageFactory.video ||
                                type == MessageFactory.picture) {
                            canShowShareImage = true;

                        }
                    } else {
                        if (selectedChatItems.get(0).getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                            if (type == MessageFactory.audio ||
                                    type == MessageFactory.document ||
                                    type == MessageFactory.video ||
                                    type == MessageFactory.picture) {
                                canShowShareImage = true;

                            }
                        }


                    }


                    if (canShowShareImage) {
                        mSelectedPath = selectedChatItems.get(0).getChatFileLocalPath();
                        mSelectedType = selectedChatItems.get(0).getMessageType();
                        share.setVisibility(View.VISIBLE);

                    } else {

                        share.setVisibility(View.GONE);

                    }


                } else {
                    replymess.setVisibility(View.GONE);
                    share.setVisibility(View.GONE);

                    reply = false;
                    info.setVisibility(View.GONE);
                    //Check whether the
                }

                isSelectedWithUnStarMsg = false;
                for (MessageItemChat selectedItem : selectedChatItems) {
                    if (selectedItem.getStarredStatus().equals(MessageFactory.MESSAGE_UN_STARRED)) {
                        isSelectedWithUnStarMsg = true;
                        break;
                    }
                }

                if (isSelectedWithUnStarMsg) {
                    starred.setImageResource(R.drawable.ic_starred);
                } else {
                    starred.setImageResource(R.drawable.ic_unstarred);
                }

                if (isDeletedMsg) {
                    showDeleteActions();
                } else {
                    showBaseActions();
                }
            }

            madapter.setfirstItemSelected(isFirstItemSelected);
            Log.e(TAG, "performSelection: notifyDataSetChanged");

            madapter.notifyDataSetChanged();
        }
        return false;
    }

    private void showDeleteActions() {
        include.setVisibility(View.GONE);
        rlChatActions.setVisibility(View.VISIBLE);
        starred.setVisibility(View.GONE);
        forward.setVisibility(View.GONE);
        replymess.setVisibility(View.GONE);

        //  share.setVisibility(View.GONE);

        info.setVisibility(View.GONE);
        delete.setVisibility(View.VISIBLE);
        longpressback.setVisibility(View.VISIBLE);
    }

    private void showBaseActions() {
        include.setVisibility(View.GONE);
        rlChatActions.setVisibility(View.VISIBLE);
        starred.setVisibility(View.VISIBLE);
        delete.setVisibility(View.VISIBLE);
        longpressback.setVisibility(View.VISIBLE);
    }

    void showMenuBelowLollipop() {

        int cx = (attachmentLayout.getLeft() + attachmentLayout.getRight());
        int cy = attachmentLayout.getTop();
        int radius = Math.max(attachmentLayout.getWidth(), attachmentLayout.getHeight());

        try {

            SupportAnimator animator = io.codetail.animation.ViewAnimationUtils.createCircularReveal(attachmentLayout, cx, cy, 0, radius);

            animator.setInterpolator(new AccelerateDecelerateInterpolator());

            animator.setDuration(300);

            if (isHidden) {
                //MyLog.e(getClass().getSimpleName(), "showMenuBelowLollipop");

                attachmentLayout.setVisibility(View.VISIBLE);

                animator.start();

                isHidden = false;

            } else {
                SupportAnimator animatorReverse = animator.reverse();

                animatorReverse.start();

                animatorReverse.addListener(new SupportAnimator.AnimatorListener() {
                    @Override
                    public void onAnimationStart() {

                    }

                    @Override
                    public void onAnimationEnd() {
                        //MyLog.e("MainActivity", "onAnimationEnd");

                        isHidden = true;
                        attachmentLayout.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationCancel() {

                    }

                    @Override
                    public void onAnimationRepeat() {

                    }
                });
            }
        } catch (Exception e) {
            //MyLog.e(getClass().getSimpleName(), "try catch");

            isHidden = true;

            attachmentLayout.setVisibility(View.INVISIBLE);

        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    void showMenu() {
        int cx = (attachmentLayout.getLeft() + attachmentLayout.getRight());
        int cy = attachmentLayout.getTop();
        int radius = Math.max(attachmentLayout.getWidth(), attachmentLayout.getHeight());

        if (isHidden) {
            Animator anim = android.view.ViewAnimationUtils.createCircularReveal(attachmentLayout, cx, cy, 0, radius);
            attachmentLayout.setVisibility(View.VISIBLE);
            anim.start();
            isHidden = false;
        } else {
            Animator anim = android.view.ViewAnimationUtils.createCircularReveal(attachmentLayout, cx, cy, radius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    attachmentLayout.setVisibility(View.INVISIBLE);
                    isHidden = true;
                }
            });
            anim.start();
        }
    }

    private void hideMenu() {
        attachmentLayout.setVisibility(View.GONE);
        isHidden = true;
    }

    private void getintent() {

        if (getIntent().getData() != null) {
            Cursor cursor = managedQuery(getIntent().getData(), null, null, null, null);
            if (cursor.moveToNext()) {
                receiverUid = cursor.getString(cursor.getColumnIndex("DATA1"));
                to = cursor.getString(cursor.getColumnIndex("DATA1"));
                receiverMsisdn = cursor.getString(cursor.getColumnIndex("DATA2"));
                backfrom = true;
                mReceiverName = getcontactname.getSendername(receiverUid, receiverMsisdn);

                mReceiverId = to;
            }

        } else {

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                backfrom = bundle.getBoolean("backfrom");
                receiverUid = bundle.getString("receiverUid");
                msgid = bundle.getString("msgid", "");
                receiverName = bundle.getString("receiverName");
                to = bundle.getString("documentId");
                starred_msgid = bundle.getString("starred", "");
                mReceiverName = bundle.getString("Username");
                receiverAvatar = bundle.getString("Image");
                receiverMsisdn = bundle.getString("msisdn");

                mReceiverId = to;
            }
        }

        Chat_to = mReceiverId;


        try {

            getcontactname.configCircleProfilepic(user_profile_image, to, true, true, R.drawable.avatar_contact);

        } catch (Exception e) {


        }


    }

    private void updateName() {
        if (mReceiverName == null || mReceiverName.equalsIgnoreCase("")) {
            mReceiverName = getcontactname.getSendername(receiverUid, receiverMsisdn);
        }

        if (mReceiverName != null && !mReceiverName.equalsIgnoreCase("")) {

            user_name.setText(mReceiverName);
            SharedPreference.getInstance().save(context, "userName", mReceiverName);
        } else {

            SharedPreference.getInstance().save(context, "userName", mReceiverName);
            user_name.setText(receiverMsisdn);
        }
        if (SmarttyContactsService.savedName != null && !SmarttyContactsService.savedName.isEmpty()) {
            user_name.setText(SmarttyContactsService.savedName);

            SharedPreference.getInstance().save(context, "userName", SmarttyContactsService.savedName);
        }

    }

    private void initAdapter() {
        madapter = new ChatMessageAdapter(false, this, mChatData, this.getSupportFragmentManager(), this);
        madapter.setIsGroupChat(isGroupChat);
        chat_list.setAdapter(madapter);
        Log.e(TAG, "initAdapter" + "chat_list.setSelection");
        chat_list.setSelection(mChatData.size() - 1);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void loadFromDB() {
        MyLog.d(TAG, "loadFromDB chatmove: ");
        new LoadDBAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        MyLog.d(TAG, "loadFromDB: end  chatmove");
    }

    private void loadDbAsync() {
        docId = from + "-" + to + "-g";

        ArrayList<MessageItemChat> items = new ArrayList<>();

        if (db.isGroupId(docId)) {
            call_layout.setVisibility(View.GONE);
            isGroupChat = true;
            mGroupId = to;
            mConvId = to;
            Activity_GroupId = mGroupId;
            chatType = MessageFactory.CHAT_TYPE_GROUP;

            Badge_count_id = from + "-" + to + "-g";

            items = new ArrayList<>();

            items.addAll(db.selectAllMessagesWithLimit(docId, chatType,
                    "", MessageDbController.MESSAGE_SELECTION_LIMIT_FIRST_TIME));

            Collections.sort(items, msgComparator);
            mChatData.clear();
            mChatData.addAll(items);
            isGroupChat = true;

            hasGroupInfo = groupInfoSession.hasGroupInfo(docId);

            setGroupMemmbersName();
        } else {


            call_layout.setVisibility(View.VISIBLE);
            isGroupChat = false;
            chatType = MessageFactory.CHAT_TYPE_SINGLE;

            docId = from + "-" + to;
            Badge_count_id = from + "-" + to;
            MessageItemChat encryptionMsgInfo = db.getFirstMsgTimeStamp(chatType, docId);
            items = new ArrayList<>();

            items.addAll(db.selectAllMessagesWithLimit(docId, chatType, "", MessageDbController.MESSAGE_SELECTION_LIMIT_FIRST_TIME));

            if (items.size() > 0) {
                try {
                    Collections.sort(items, msgComparator);
                } catch (Exception e) {
                    Log.e(TAG, "loadDbAsync: ", e);
                }
                long firstItemTs = AppUtils.parseLong(items.get(0).getTS());
                encryptionMsgInfo.setTS("" + (firstItemTs - 10L));
            }
            // items.add(encryptionMsgInfo);
            Collections.sort(items, msgComparator);
            if (items.size() > 0) {
                mFirstVisibleMsgId = items.get(0).getMessageId();
            }

            if (userInfoSession.hasChatConvId(docId)) {
                mConvId = userInfoSession.getChatConvId(docId);
            }

            //getUserDetails(to);

            //sendAllAcksToServer(items);
            mChatData.clear();
            mChatData.addAll(items);

            MyLog.d(TAG, "loadFromDB: finished");
        }


        changeBadgeCount(Badge_count_id);

        sendAllAcksToServer(items);

        if (isFirstTimeLoad) {
            isFirstTimeLoad = false;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    profilepicupdation();
                }
            });
        }

        final ArrayList<MessageItemChat> finalItems = items;
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (isGroupChat) {

                    CheckGroupExitUser(finalItems);
                } else {
                    group_left_layout.setVisibility(View.GONE);
                    relativeLayout.setVisibility(View.VISIBLE);
                    rlSend.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void setLastItemSelection() {
        chat_list.post(new Runnable() {
            @Override
            public void run() {
                if (mChatData.size() > 0)
                    chat_list.setSelection(mChatData.size() - 1);
            }
        });

    }

    //--------------------------Group Exit CurrentUser Check-------------------------

    private String createCameraImageFileName() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return timeStamp + ".jpg";
    }

    private void CheckGroupExitUser(ArrayList<MessageItemChat> items) {

        String createdBy = "";
        String createdTo = "";
        for (int i = 0; i < items.size(); i++) {
            final MessageItemChat message = items.get(i);

            if (message.getCreatedByUserId() != null) {

                createdBy = message.getCreatedByUserId();
            }

            if (message.getCreatedToUserId() != null) {

                createdTo = message.getCreatedToUserId();
            }


            if (message.getGroupEventType() != null) {

                switch (message.getGroupEventType()) {

                    case "" + MessageFactory.exit_group:
                        if (createdBy.equalsIgnoreCase(mCurrentUserId)) {

                            relativeLayout.setVisibility(View.INVISIBLE);
                            rlSend.setVisibility(View.INVISIBLE);
                            group_left_layout.setVisibility(View.VISIBLE);
                        } else {
                            // createdByName = getContactNameIfExists(createdBy);
                        }
                        break;

                    case "" + MessageFactory.delete_member_by_admin:
                        if (createdTo.equalsIgnoreCase(mCurrentUserId)) {

                            relativeLayout.setVisibility(View.GONE);
                            rlSend.setVisibility(View.GONE);
                            group_left_layout.setVisibility(View.VISIBLE);

                        }
                      /*  else if(createdTo.equalsIgnoreCase(mCurrentUserId)) {

                            relativeLayout.setVisibility(View.GONE);
                            rlSend.setVisibility(View.GONE);
                            group_left_layout.setVisibility(View.VISIBLE);
                        }*/

                        break;

                    case "" + MessageFactory.add_group_member:
                        if (createdTo.equalsIgnoreCase(mCurrentUserId)) {

                            relativeLayout.setVisibility(View.VISIBLE);
                            rlSend.setVisibility(View.VISIBLE);
                            group_left_layout.setVisibility(View.GONE);

                        }
                      /*  else if(createdTo.equalsIgnoreCase(mCurrentUserId)) {

                            relativeLayout.setVisibility(View.GONE);
                            rlSend.setVisibility(View.GONE);
                            group_left_layout.setVisibility(View.VISIBLE);
                        }*/

                        break;


                }

            }
        }


    }

    private void initDataBase() {

        if (starred_msgid != null && !starred_msgid.equals("")) {
            Starredmsg_test();
        }
        layout_new.setVisibility(View.GONE);
        if (!isGroupChat) {
            if (!getcontactname.isContactExists(mReceiverId) && (!sessionManager.isFirstMessage(to + "firstmsg"))) {
                //layout_new.setVisibility(View.VISIBLE);
                if (mContactSaved) {
                    llAddBlockContact.setVisibility(View.GONE);
                } else {
                    llAddBlockContact.setVisibility(View.VISIBLE);
                }
                //          llAddBlockContact.setVisibility(View.VISIBLE);
            } else if (!getcontactname.isContactExists(mReceiverId)) {
                llAddBlockContact.setVisibility(View.VISIBLE);
                setTopLayoutBlockText();
            }


            String blockStatus = contactDB_sqlite.getBlockedStatus(to, false);
            if (blockStatus.equals("1")) {
                block_contact.setText("Unblock");
            } else {
                block_contact.setText("Block");
            }
        }

        getgroupmemberdetails();
    }

    //----------------------------------Select the Star Position Chat Message----------------------------------
    private void Starredmsg() {
        String msgidstarred = "";
        if (msgid != null || !msgid.equals("")) {
            final DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            for (int i = 0; i < mChatData.size(); i++) {
                try {
                    String[] array = mChatData.get(i).getMessageId().split("-");
                    if (mChatData.get(i).getMessageId().contains("-g")) {
                        msgidstarred = array[3];
                    } else {
                        msgidstarred = array[2];
                    }
                    if (msgid.equalsIgnoreCase(msgidstarred)) {
                        chat_list.setSelection(i);
                        mChatData.get(i).setSelected(true);
                    }

                } catch (Exception e) {

                }
            }
            final Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < mChatData.size(); i++) {
                        mChatData.get(i).setSelected(false);
                    }
                    Log.e(TAG, "Starredmsg: notifyDataSetChanged");

                    madapter.notifyDataSetChanged();
                }
            }, 2500);

        }

    }

    //------------------------------------------------------------Group Member Details Get----------------------------------------

    private void Starredmsg_test() {
        String msgidstarred = "";
        int starredpostion = 0;
        if (starred_msgid != null && !starred_msgid.equals("")) {
            MessageItemChat message = db.getParticularMessage(starred_msgid);

            ArrayList<MessageItemChat> items = db.selectAllMessagesWithLimit_again(docId, chatType,
                    message.getTS(), MessageDbController.MESSAGE_SELECTION_LIMIT_FIRST_TIME);
            items.add(message);
            Collections.sort(items, msgComparator);
            String starredMsg = message.getTextMessage();
            MyLog.d(TAG, "Starredmsg_test: " + starredMsg);
            mChatData.clear();
            mChatData.addAll(items);
            Log.e(TAG, "Starredmsg_test: notifyDataSetChanged");

            madapter.notifyDataSetChanged();

/*            chat_list.post(new Runnable() {
                @Override
                public void run() {
                    chat_list.setSelection(0);
                }
            });*/
            mChatData.get(0).setSelected(true);
            final Handler handler = new Handler(Looper.getMainLooper());
            //final int finalStarredpostion = starredpostion;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mChatData.get(0).setSelected(false);
                    Log.e(TAG, "Starredmsg_test: notifyDataSetChanged");

                    madapter.notifyDataSetChanged();
                }
            }, 2500);

        }

    }

    private void getgroupmemberdetails() {
        try {
            String docId = mCurrentUserId.concat("-").concat(to).concat("-g");
            hasGroupInfo = groupInfoSession.hasGroupInfo(docId);
        } catch (Exception e) {
            Log.e(TAG, "getgroupmemberdetails: ", e);
        }
        if (hasGroupInfo) {
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);
            if (infoPojo != null && infoPojo.getGroupMembers() != null) {
                String[] membersId = infoPojo.getGroupMembers().split(",");
                for (int i = 0; i < membersId.length; i++) {
                    getUserDetails(membersId[i]);
                }

            } else {
                Toast.makeText(this, "You are not member of this group!", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    public void onStart() {
        //overridePendingTransition(0, 0);
        super.onStart();

        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyLog.d(TAG, "onResume: chatmove");
        ScreenShotDetector.setListener(this);
        isChatPage = true;
        isKilled = false;
        //sendAllAcksToServer(mChatData);


        if (!noNeedRefresh) {
            boolean isLockChat = getIntent().getBooleanExtra("isLockChat", false);
            if (!isLockChat) {

                loadFromDB();
            }
        }
        noNeedRefresh = false;
        /*if (!isFirstTimeLoad) {
            isFirstTimeLoad = false;
        loadFromDB();
        }*/
        if (isGroupChat) {
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(from + "-" + to + "-g");
            if (infoPojo != null) {
                enableGroupChat = infoPojo.isLiveGroup();
            }
        }
        session.puttoid(docId);

        NotificationUtil.clearNotificationData();
        wallpaperdisplay();
        NotificationManager notifManager = (NotificationManager) getApplication().getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.cancelAll();
        if (!isFirstTimeLoad)
            profilepicupdation();
        try {
            ShortcutBadger.removeCountOrThrow(this); //for 1.1.4+
        } catch (Exception e) {
            //  e.printStackTrace();
        }

        //----------Delete Chat---------------
        /*if (isGroupChat) {
            sendGroupOffline();
        } else {
            sendSingleOffline();
        }
        sendNormalOffline();*/
        updateName();
        checkcallstatus();
        MyLog.d(TAG, "onResume end: chatmove");
        if (null != sendMessage && sendMessage.getVisibility() == View.GONE)
            sendMessage.postDelayed(new Runnable() {
                @Override
                public void run() {
                    sendMessage.setVisibility(View.VISIBLE);
                }
            }, 500);

    }

    public void checkcallstatus() {
        boolean mCallOngoing = SharedPreference.getInstance().getBool(CoreController.mcontext, "callongoing");
        if (mCallOngoing) {
            //Register you receiver

            registerCallReceiver();

            if (mLnrcallStatus.getVisibility() == View.GONE) {
                mLnrcallStatus.setVisibility(View.VISIBLE);
            }
        } else {
            if (mLnrcallStatus.getVisibility() == View.VISIBLE) {

                mLnrcallStatus.setVisibility(View.GONE);
            }
        }
    }

    private void registerCallReceiver() {
        //Register BroadcastReceiver
        //to receive event from our service
        if (myReceiver == null) {
            myReceiver = new MyReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(MessageService.SENDMESAGGE);
            registerReceiver(myReceiver, intentFilter);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //overridePendingTransition(0, 0);
        session.puttoid("");
        isChatPage = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //  EventBus.getDefault().unregister(this);

        if (receiver != null) {
            unregisterReceiver(receiver);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (myReceiver != null) {
            unregisterReceiver(myReceiver);
        }
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(GroupMemberFetched event) {
        Log.d(TAG, "onMessageEvent:commaIssue ");
        if (isGroupMemebersNotLoaded) {
            isGroupMemebersNotLoaded = false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setGroupMemmbersName();
                }
            }, 3000);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {

        if (!isGroupChat) {

            if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_START_FILE_DOWNLOAD)) {
                try {
                    Object[] response = event.getObjectsArray();
                    JSONObject jsonObject = (JSONObject) response[1];
                    writeBufferToFile(jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_FILE_RECEIVED)) {
                try {
                    Object[] obj = event.getObjectsArray();
                    JSONObject object = new JSONObject(obj[0].toString());
                    new ChatPageActivity.loadFileUploaded(object).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_MESSAGE)) {

                loadMessage(event);

            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_MESSAGE_RES)) {

                loadMessageRes(event);

            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_PRIVACY_SETTINGS)) {

                loadPrivacySetting(event);

            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CLEAR_CHAT)) {
                Object[] obj = event.getObjectsArray();
                load_clear_chat(obj[0].toString());
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_DELETE_CHAT)) {
                Object[] obj = event.getObjectsArray();
                boolean isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
                if (isMassChat) {
                    loadFromDB();
                    //load_delete_chat(obj[0].toString());
                }
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_TYPING)) {
                loadTypingStatus(event);
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CAHNGE_ONLINE_STATUS)) {
                loadOnlineStatus(event);
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GET_CURRENT_TIME_STATUS)) {
                loadCurrentTimeMessage(event);
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_STAR_MESSAGE)) {
                loadStarredMessage(event);
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_REMOVE_MESSAGE)) {
                loadDeleteMessage(event);
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_MUTE)) {
                Object[] response = event.getObjectsArray();
                loadMuteMessage(response[0].toString());
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GET_MESSAGE_DETAILS)) {
                loadReplyMessageDetails(event.getObjectsArray()[0].toString());
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_DELETE_MESSAGE)) {
                deleteSingleMessage(event);
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_SINGLE_OFFLINE_MSG)) {
                getSingleOffline(event);
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_IMAGE_UPLOAD)) {
                updateProfileImage(event);
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_MESSAGE_STATUS_UPDATE)) {
                loadMessageStatusupdate(event);
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GET_MESSAGE)) {
                loadMessage(event);
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_BLOCK_USER)) {
                blockunblockcontact(event);
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_REPORT_SPAM_USER)) {
                try {
                    Object[] obj = event.getObjectsArray();
                    JSONObject object = new JSONObject(obj[0].toString());
                    Toast.makeText(this, "Contact is reported as spam ", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else {

            if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GROUP)) {

                try {
                    Object[] obj = event.getObjectsArray();
                    JSONObject object = new JSONObject(obj[0].toString());

                    String error = object.getString("err");

                    if (error.equalsIgnoreCase("0") || error.equalsIgnoreCase("")) {


                        String groupAction = object.getString("groupType");

                        if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EVENT_GROUP_MESSAGE)) {
                            String from = object.getString("from");
                            //  if (!from.equalsIgnoreCase(mCurrentUserId)) {
                            //getUserDetails(from);
                            handleGroupMessage(event);
                            //   }
                            //   handleGroupMessage(event);
                        } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_ACK_GROUP_MESSAGE)) {
//                    Toast.makeText(ChatViewActivity.this, "".concat(String.valueOf(object)), Toast.LENGTH_LONG).show();
                            updateGroupMsgStatus(object);
                        } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_CHANGE_GROUP_NAME)) {
                            loadChangeGroupNameMessage(object);
                        } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EXIT_GROUP)) {
                            loadExitMessage(object);
                        } else if (groupAction.equals(SocketManager.ACTION_CHANGE_GROUP_DP)) {
                            performGroupChangeDp(object);
                        } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_DELETE_GROUP_MEMBER)) {
                            loadDeleteMemberMessage(object);
                        } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_ADD_GROUP_MEMBER)) {
                            loadAddMemberMessage(object);
                        } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_MAKE_GROUP_ADMIN)) {
                            loadMakeAdminMessage(object);
                        } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EVENT_GROUP_MSG_DELETE)) {
                            deleteGroupMessage(event);
                        } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EVENT_GROUP_OFFLINE)) {
                            getGroupOffline(event);
                        } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_JOIN_NEW_GROUP)) {

                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    loadFromDB();
                                }
                            }, 5000);
                        }


                    } else {

                        String message = object.getString("msg");

                        String messageid = object.getString("doc_id");

                        for (int i = 0; i < mChatData.size(); i++) {

                            String msg_id = mChatData.get(i).getMessageId();

                            if (msg_id.equalsIgnoreCase(messageid)) {

                                mChatData.get(i).setUploadStatus(1000);
                                Log.e(TAG, "EVENT_GROUP: notifyDataSetChanged");

                                madapter.notifyDataSetChanged();
                                break;

                            }

                        }

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GROUP_DETAILS)) {
                loadGroupDetails(event);
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_FILE_RECEIVED)) {

                try {
                    Object[] objs = event.getObjectsArray();
                    JSONObject objects = new JSONObject(objs[0].toString());
                    new ChatPageActivity.loadFileUploaded(objects).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_START_FILE_DOWNLOAD)) {
                try {
                    Object[] response = event.getObjectsArray();
                    JSONObject jsonObject = (JSONObject) response[1];
                    writeBufferToFile(jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CLEAR_CHAT)) {
                Object[] obj = event.getObjectsArray();
                load_clear_chat(obj[0].toString());
            }

        }
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_USER_AUTHENTICATED)) {
            if (!isGroupChat) {
                if (!isLastSeenCalled) {
                    getReceiverOnlineTimeStatus();
                }
            }
            if (mChatData.size() > 0) {
                sendAllAcksToServer(mChatData);
            }
        }

    }

    //--------------------------------------Message Receiver Events------------------------------------

    private void setGroupMemmbersName() {
        if (hasGroupInfo) {
            Log.d(TAG, "setGroupMemmbersName: commaIssue");
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);
            sb = new StringBuilder();
            String memername;
            if (infoPojo != null && infoPojo.getGroupMembers() != null) {
                if (infoPojo.getGroupMembers().contains("[")) {
                    infoPojo.getGroupMembers().replace("[", "");
                    infoPojo.getGroupMembers().replace("]", "");
                }
                String[] contacts = infoPojo.getGroupMembers().split(",");

//Check keyword and dont update group memebers
                for (int i = 0; i < contacts.length; i++) {
                    if (!contacts[i].equalsIgnoreCase(from)) {


                        String msisdn = contactDB_sqlite.getSingleData(contacts[i], ContactDB_Sqlite.MSISDN);

                        if (msisdn != null) {
                            memername = getcontactname.getSendername(contacts[i], msisdn);
                            if (memername == null || memername.isEmpty()) {
                                memername = msisdn;
                            }
                            Log.d(TAG, "setGroupMemmbersName: commaIssue memeberName=" + memername);
                            if (AppUtils.isEmpty(memername)) {
                                isGroupMemebersNotLoaded = true;
                                break;
                            }
                            sb.append(memername);
                            if (contacts.length - 1 != i) {
                                sb.append(", ");
                            }
                        }
                    } else {
                        memername = "You";
                        sb.append(memername);
                        if (contacts.length - 1 != i) {
                            sb.append(", ");
                        }
                    }
                }
                MyLog.d(TAG, "loadFromDB>>>>: " + sb);
                status_textview.post(new Runnable() {
                    @Override
                    public void run() {
                        status_textview.setText(sb);
                    }
                });

                enableGroupChat = infoPojo.isLiveGroup();
            }
        }
    }

    private void DeleteGroupMessage(JSONObject objects) {

        try {
            int errorState = objects.getInt("err");
            if (errorState == 0) {
                String fromId = objects.getString("from");
                String docId, msgId, type, recId, lastMsg_Status, convId;
//                String deleteStatus = objects.getString("status");
                String chat_id = (String) objects.get("doc_id");
                String[] ids = chat_id.split("-");
                type = objects.getString("type");
//                recId = objects.getString("recordId");
//                convId = objects.getString("convId");
                docId = ids[1] + "-" + ids[0];
                msgId = docId + "-" + ids[2];

                if (fromId.equalsIgnoreCase(from)) {
                    for (MessageItemChat items : mChatData) {
                        if (items != null && items.getMessageId().equalsIgnoreCase(chat_id)) {
                            int index = mChatData.indexOf(items);
                            if (index > -1 && mChatData.get(index).isMediaPlaying()) {
                                madapter.stopAudioOnMessageDelete(index);
                            }

                            mChatData.get(index).setMessageType(MessageFactory.DELETE_SELF + "");
                            mChatData.get(index).setIsSelf(true);
                            Log.e(TAG, "DeleteGroupMessage: notifyDataSetChanged");

                            madapter.notifyDataSetChanged();
                            db.deleteSingleMessage(docId, items.getMessageId(), chatType, "self");
                            db.deleteChatListPage(docId, items.getMessageId(), chatType, "self");

                            break;
                        }
                    }
                }

                if (!fromId.equalsIgnoreCase(from) && fromId.equalsIgnoreCase(Chat_to)) {

                    for (MessageItemChat items : mChatData) {
                        if (items != null && items.getMessageId().equalsIgnoreCase(msgId)) {
                            int index = mChatData.indexOf(items);
                            if (index > -1 && mChatData.get(index).isMediaPlaying()) {
                                madapter.stopAudioOnMessageDelete(index);
                            }

                            mChatData.get(index).setMessageType(MessageFactory.DELETE_OTHER + "");
                            mChatData.get(index).setIsSelf(false);
                            Log.e(TAG, "DeleteGroupMessage: notifyDataSetChanged");

                            madapter.notifyDataSetChanged();

                            db.deleteSingleMessage(docId, msgId, type, "other");
                            db.deleteChatListPage(docId, msgId, type, "other");

                            break;
                        }
                    }
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }

    private void writeBufferToFile(JSONObject object) {

        try {
            String fileName = object.getString("ImageName");
            String localPath = object.getString("LocalPath");
//          String localFileName = object.getString("LocalFileName");
            String end = object.getString("end");
            int bytesRead = object.getInt("bytesRead");
            int fileSize = object.getInt("filesize");
            String msgId = object.getString("MsgId");
            String Start = object.getString("start");

            boolean isFilePaused = uploadDownloadManager.isDownloadFilePaused(msgId);

            MyLog.e("DownloadFileStatus", "DownloadingGetStreaming" + object);

            MyLog.e("DownloadFileStatus", "DownloadingPauseStatus" + " " + "" + isFilePaused);

            if (!isFilePaused) {

                //  int progress = (bytesRead / fileSize) * 100;

                int start = Integer.parseInt(Start);

                final int progress = (start * 100) / fileSize;

                for (int i = 0; i < mChatData.size(); i++) {
                    if (msgId.equalsIgnoreCase(mChatData.get(i).getMessageId())) {
                        //   progress = (start * 100) / fileSize;
                        if (progress == 0) {

                            mChatData.get(i).setUploadDownloadProgress(2);

                            // mChatData.get(i).setVideoPath(localPath);

                        } else {

                            mChatData.get(i).setUploadDownloadProgress(progress);
                        }


                        if (end.equalsIgnoreCase("1")) {
                            mChatData.get(i).setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_COMPLETED);
                            // db.updateMessageDownloadStatus(docId, msgId, MessageFactory.DOWNLOAD_STATUS_COMPLETED);
                        }
                        break;
                    }
                }
                Log.e(TAG, "writeBufferToFile: notifyDataSetChanged");

                madapter.notifyDataSetChanged();

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void loadMessageRes(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            MyLog.e("loadMessageRes", "loadMessageRes" + objects);
            int errorState = objects.getInt("err");
            if (errorState == 0) {
                String chat_id = (String) objects.get("doc_id");
                String[] ids = chat_id.split("-");
                String doc_id = ids[0] + "-" + ids[1];
                int delivery = (int) objects.get("deliver");
                String to = ids[1];

                JSONObject msgData = objects.getJSONObject("data");
                String recordId = msgData.getString("recordId");
                String convId = msgData.getString("convId");
                //msgid is null not coming
                if (msgData.has("msgId")) {

                    String id = msgData.getString("msgId");

                    if (newMsgIds.contains(id)) {
                        return;
                    }
                    newMsgIds.add(id);
                } else {
                    return;
                }/*
                if (newMsgIds.contains(id)) {
                    return;
                }
    */         //   newMsgIds.add(id);


                String secretType = msgData.getString("secret_type");
                if (to.equalsIgnoreCase(mReceiverId) && secretType.equalsIgnoreCase("no")) {

                    layout_new.setVisibility(View.GONE);

                    mConvId = convId;
                    MessageItemChat item = null;

                    for (MessageItemChat msgItemChat : mChatData) {
                        if (msgItemChat.getMessageId().equalsIgnoreCase(chat_id)) {
                            item = msgItemChat;
                            break;
                        }
                    }


                    if (item != null) {
                        int msgIndex = mChatData.indexOf(item);
                        mChatData.get(msgIndex).setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);
                        mChatData.get(msgIndex).setDeliveryStatus("" + delivery);
                        mChatData.get(msgIndex).setRecordId(recordId);
                        mChatData.get(msgIndex).setConvId(convId);

                        Log.e(TAG, "loadMessageRes: notifyDataSetChanged");

                        madapter.notifyDataSetChanged();

                    } else {
                        String toUserId = msgData.getString("to");

                        if (toUserId.equalsIgnoreCase(mReceiverId)) {
                            IncomingMessage incomingMessage = new IncomingMessage(ChatPageActivity.this);
                            MessageItemChat newMsgItem = incomingMessage.loadSingleMessageFromWeb(objects);
                            if (newMsgItem != null) {
                                if (!isAlreadyExist(newMsgItem))
                                    mChatData.add(newMsgItem);

                                Log.e(TAG, "loadMessageRes: notifyDataSetChanged");

                                notifyDatasetChange();
                            }
                        }

                        MyLog.e("CheckEventConnection", "ChatIDNotMatch Error");
                    }

                    if (session.getPrefsNameintoneouttone()) {

                        try {
/*                            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                            r.play();*/
                            MediaPlayer.create(this, R.raw.send_message).start();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } /*else if (errorState == 1) {
                SessionManager.getInstance(ChatViewActivity.this).logoutUser();
            }*/


            } else if (errorState == 1) {

                String chat_id = (String) objects.get("doc_id");

                MessageItemChat item = null;

                for (MessageItemChat msgItemChat : mChatData) {
                    if (msgItemChat.getMessageId().equalsIgnoreCase(chat_id)) {
                        item = msgItemChat;
                        break;
                    }
                }


                if (item != null) {

                    int msgIndex = mChatData.indexOf(item);
                    mChatData.get(msgIndex).setUploadStatus(MessageFactory.UPLOAD_STATUS_UPLOADING);
                    mChatData.get(msgIndex).setUploadDownloadProgress(0);
                    Log.e(TAG, "loadMessageRes: notifyDataSetChanged");

                    madapter.notifyDataSetChanged();

                }


            }
        } catch (Exception e) {
            MyLog.e(TAG, "loadMessageRes: ", e);
        }
    }

    private void loadMessage(ReceviceMessageEvent event) {


        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            String type = "";
            int normal_Offline = 0;
            if (objects.has("is_deleted_everyone")) {
                normal_Offline = objects.getInt("is_deleted_everyone");
            }
            if (objects.has("type")) {
                type = String.valueOf(objects.getString("type"));
                if (Integer.parseInt(objects.optString("type")) == 23) {
                    return;
                }
            }
            String secretType = objects.getString("secret_type");
            if (secretType.equalsIgnoreCase("no")) {
                IncomingMessage incomingMsg = new IncomingMessage(ChatPageActivity.this);
                MessageItemChat item = incomingMsg.loadSingleMessage(objects);
                String from = objects.getString("from");
                String to = objects.getString("to");

                session.Removemark(from);

                String id = objects.getString("msgId");
                String convId = objects.getString("convId");

                String doc_id;
                if (objects.has("docId")) {
                    doc_id = objects.getString("docId");
                } else {
                    doc_id = objects.getString("doc_id");
                }
                String uniqueID = "";
                if (from.equalsIgnoreCase(mCurrentUserId)) {
                    uniqueID = from + "-" + to;
                    item.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_SENT);
                } else if (to.equalsIgnoreCase(mCurrentUserId)) {
                    uniqueID = to + "-" + from;

                    if (sessionManager.canSendReadReceipt()) {
                        uniqueID = to + "-" + from;
                        if (from.equalsIgnoreCase(mReceiverId)) {
                            sendAckToServer(from, doc_id, "" + id, convId);
                        }
                    }
                    item.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_READ);

                    getUserDetails(from);
                }

                item.setMessageId(uniqueID + "-" + id);
                if (secretType.equalsIgnoreCase("yes")) {
                    uniqueID = uniqueID + "-" + MessageFactory.CHAT_TYPE_SECRET;
                    String timer = objects.getString("incognito_timer");
                    item.setSecretTimer(timer);
                    item.setSecretTimeCreatedBy(from);
                }

                if (session.getPrefsNameintoneouttone()) {
                    try {
/*                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                        r.play();*/
                        MediaPlayer.create(this, R.raw.send_message).start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (from.equalsIgnoreCase(mReceiverId)) {
                    mConvId = convId;

                    if (isChatPage) {
                        changeBadgeCount(uniqueID);
                    }
                    sendChatViewedStatus();
                    if (lastvisibleitempostion < mChatData.size() - 1) {
                        unreadmsgcount++;
                        unreadmessage();

                        if (!id.equalsIgnoreCase(Message_id)) {
                            if (!isAlreadyExist(item))
                                mChatData.add(item);
                            Message_id = id;

                            Log.e(TAG, "loadMessage: notifyDataSetChanged");

                            madapter.notifyDataSetChanged();
                            //Check if the last item is not visible move to it

                            int last = chat_list.getLastVisiblePosition();
                            //  Log.e(TAG, "getLastVisiblePosition: last"+last);

                            if (!mListviewScrollBottom) {
                                mListviewScrollBottom = true;
                                setLastItemSelection();
                            }
                        }

                    } else {

                        if (!id.equalsIgnoreCase(Message_id)) {
                            if (!isAlreadyExist(item))
                                mChatData.add(item);
                            Message_id = id;

                            Log.e(TAG, "loadMessage: notifyDataSetChanged");

                            notifyDatasetChange();
                        }

                    }


                    //-------------Delete Chat-----------------

                    if (normal_Offline == 1) {
                        String new_docId, new_msgId, new_type, new_recId, new_convId;
                        try {
                            String fromId = objects.getString("from");
                            if (!fromId.equalsIgnoreCase(mCurrentUserId)) {
                                String chat_id = (String) objects.get("docId");
                                String[] ids = chat_id.split("-");

                                new_type = objects.getString("chat_type");
//                                new_recId = objects.getString("recordId");
//                                new_convId = objects.getString("convId");

                                if (new_type.equalsIgnoreCase("single")) {
                                    new_docId = ids[1] + "-" + ids[0];
                                    new_msgId = new_docId + "-" + ids[2];

                                    for (int i = 0; i < mChatData.size(); i++) {
                                        if (mChatData.get(i).getMessageId().equalsIgnoreCase(new_msgId)) {

//                                            mChatData.get(i).setMessageType("Delete Others");
                                            mChatData.get(i).setMessageType(MessageFactory.DELETE_OTHER + "");
                                            mChatData.get(i).setIsSelf(false);

                                            Log.e(TAG, "loadMessage: notifyDataSetChanged");

                                            madapter.notifyDataSetChanged();

                                            db.deleteSingleMessage(new_docId, new_msgId, new_type, "other");
                                            db.deleteChatListPage(new_docId, new_msgId, new_type, "other");
                                            break;
                                        }
                                    }

                                } else {

                                    new_docId = fromId + "-" + ids[1] + "-g";
                                    new_msgId = docId + "-" + ids[3];

                                    String groupAndMsgId = ids[1] + "-g-" + ids[3];


                                    if (mChatData.size() > 0) {
                                        for (int i = 0; i < mChatData.size(); i++) {
                                            if (mChatData.get(i).getMessageId().contains(groupAndMsgId)) {
                                                if (i > -1 && mChatData.get(i).isMediaPlaying()) {
                                                    madapter.stopAudioOnMessageDelete(i);
                                                }

//                                                mChatData.get(i).setMessageType("Delete Others");
                                                mChatData.get(i).setMessageType(MessageFactory.DELETE_OTHER + "");
                                                mChatData.get(i).setIsSelf(false);
                                                Log.e(TAG, "loadMessage: notifyDataSetChanged");

                                                madapter.notifyDataSetChanged();

                                                db.deleteSingleMessage(groupAndMsgId, new_msgId, type, "other");
                                                db.deleteChatListPage(groupAndMsgId, new_msgId, type, "other");
                                                break;
                                            }
                                        }
                                    }


                                }
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void unreadmessage() {
       /* int lastvisibleitempostion = mLayoutManager.findLastVisibleItemPosition();

        if (lastvisibleitempostion == mChatData.size() - 1) {
            iBtnScroll.setVisibility(View.GONE);
            unreadcount.setVisibility(View.GONE);
            unreadmsgcount = 0;
        } else {
            unreadcount.setVisibility(View.VISIBLE);
            iBtnScroll.setVisibility(View.VISIBLE);
            unreadcount.setText(String.valueOf(unreadmsgcount));
        }*/

    }

    private void sendAckToServer(String to, String doc_id, String id, String convId) {
        MyLog.d(TAG, "$$$$$$ sendAckToServer(): ");
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_MESSAGE_ACK);
        MessageAck ack = (MessageAck) MessageFactory.getMessage(MessageFactory.message_ack, this);

        messageEvent.setMessageObject((JSONObject) ack.getMessageObject(to, doc_id, MessageFactory.DELIVERY_STATUS_READ, id, false, convId));
        EventBus.getDefault().post(messageEvent);
    }

    public void getUserDetails(String userId) {


        if (!contactDB_sqlite.isUserAvailableInDB(userId)) {
            try {
                if (requestedMemebers < 5) {
                    requestedMemebers++;
                    JSONObject eventObj = new JSONObject();
                    eventObj.put("userId", userId);
                    SendMessageEvent event = new SendMessageEvent();
                    event.setEventName(SocketManager.EVENT_GET_USER_DETAILS);
                    event.setMessageObject(eventObj);
                    EventBus.getDefault().post(event);
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

    }

    private void sendChatViewedStatus() {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        long timeDiff = currentTime - lastViewStatusSentAt;

        if (timeDiff > 4000) {
            lastViewStatusSentAt = currentTime;
            if (mConvId != null && !mConvId.equals("")) {
                sendViewedStatusToWeb(mConvId);
            }
        }
    }

    private void sendViewedStatusToWeb(String convId) {
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            object.put("convId", convId);
            if (isGroupChat) {
                object.put("type", "group");
            } else {
                object.put("type", "single");
            }
            object.put("mode", "phone");

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_VIEW_CHAT);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setEncryptionMsg() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (mChatData != null && mChatData.size() == 0) {
                    findViewById(R.id.encryptionlabel).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.encryptionlabel).setVisibility(View.GONE);
                }
            }
        });

    }

    //----------------------------------Image Download----------------------------
    public void imagedownloadmethod(int position) {


        MessageItemChat item = mChatData.get(position);
        if (item.getUploadDownloadProgress() == 0 && !item.isSelf() && item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START) {
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        } else if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START
                && item.isSelf()) {
            /*Download option for sent documents from web chat*/
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        }

        if (FileUploadDownloadManager.FETCH_DOWNLOAD_ENABLED) {
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        }

    }


    public void VideoDownload(int position) {

        MessageItemChat item = mChatData.get(position);
        if (item.getUploadDownloadProgress() == 0 && !item.isSelf() &&
                item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START) {
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);


            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);

           /* String fullPath = item.getChatFileServerPath();

            String message_id=item.getMessageId();

            DownloadServiceClass.DownloadFileFromURL download=new DownloadServiceClass.DownloadFileFromURL(this);
            download.execute(fullPath,message_id);*/

            // download.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,fullPath,message_id);


        } else if (!item.isSelf() && item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
            try {
                String videoPath = item.getChatFileLocalPath();
                File file = new File(item.getChatFileLocalPath());
                if (!file.exists()) {
                    try {

                        String[] filePathSplited = item.getChatFileLocalPath().split(File.separator);
                        String fileName = filePathSplited[filePathSplited.length - 1];
                        String publicDirPath = Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();

                        videoPath = publicDirPath + File.separator + fileName;
                    } catch (Exception e) {
                        MyLog.e(TAG, "configureViewHolderImageReceived: ", e);
                    }
                }

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoPath));
                String path = "file://" + videoPath;
                intent.setDataAndType(Uri.parse(path), "video/*");
                startActivity(intent);

            } catch (ActivityNotFoundException e) {
                Toast.makeText(ChatPageActivity.this, "No app installed to play this video", Toast.LENGTH_LONG).show();
            }
        } else if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START
                && item.isSelf()) {
            /*Download option for sent documents from web chat*/
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        }
        if (FileUploadDownloadManager.FETCH_DOWNLOAD_ENABLED) {
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        }

    }


    //------------------------------Video Download-------------------------------------------

    public void AudioDownload(int position) {

        MessageItemChat item = mChatData.get(position);
        if (item.getUploadDownloadProgress() == 0 && !item.isSelf() && item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START) {
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        } else if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START
                && item.isSelf()) {
            /*Download option for sent documents from web chat*/
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        }
        if (FileUploadDownloadManager.FETCH_DOWNLOAD_ENABLED) {
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        }
    }


    //-------------------------------------Audio Download---------------------------------------

    public void DocumentDownload(int position) {

        MessageItemChat item = mChatData.get(position);
        int downloadStatus = item.getDownloadStatus();
        MyLog.d(TAG, "DocumentDownload downloadStatus: " + downloadStatus);

        if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START && !item.isSelf()) {
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        } else if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START
                && item.isSelf()) {
            /*Download option for sent documents from web chat*/
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        } else if (FileUploadDownloadManager.FETCH_DOWNLOAD_ENABLED) {
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        } else {
            String extension = MimeTypeMap.getFileExtensionFromUrl(item.getChatFileLocalPath());
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

            PackageManager packageManager = getPackageManager();
            Intent testIntent = new Intent(Intent.ACTION_VIEW);
            testIntent.setType(mimeType);
            try {

                List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
                if (list.size() > 0) {
                    File file = new File(item.getChatFileLocalPath());
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(file), mimeType);
                    startActivity(intent);
                } else {
                    Toast.makeText(ChatPageActivity.this, "No app installed to view this document", Toast.LENGTH_LONG).show();
                }
            } catch (ActivityNotFoundException e) {
                Toast.makeText(ChatPageActivity.this, "No app installed to view this document", Toast.LENGTH_LONG).show();
            }


        }

    }


    private void checkLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            GPSTracker tracker = new GPSTracker(this);

            Location gg = tracker.getLocation();
            if (gg != null) {
                LatLng location = new LatLng(gg.getLatitude(), gg.getLongitude());
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(gg.getLatitude(), gg.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();
                    sendLocationMessage(location, name, address);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("TAG: Location is null");
            }



            /*PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            try {
                startActivityForResult(builder.build(ChatPageActivity.this), RESULT_SHARE_LOCATION);
            } catch (GooglePlayServicesRepairableException e) {

                Toast.makeText(this, "Please update Google Play services!", Toast.LENGTH_SHORT).show();
                e.printStackTrace();

            } catch (GooglePlayServicesNotAvailableException e) {

                Toast.makeText(this, "Google Play services not available!", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }*/

        } else {
            mEnableGps();
//            }
        }
    }

    public void mEnableGps() {
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();
        mLocationSetting();
    }

    public void mLocationSetting() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(1 * 1000);
        locationRequest.setFastestInterval(1 * 1000);

        locationSettingsRequest = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

//        mResult();

        pendingResult = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, locationSettingsRequest.build());
        pendingResult.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        System.out.println("TAG: tps turned on by user todo here");
                        // All location settings are satisfied. The client can initialize location
                        // requests here.

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                        try {

                            status.startResolutionForResult(ChatPageActivity.this, RESULT_SHARE_LOCATION);
                        } catch (IntentSender.SendIntentException e) {

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.


                        break;
                }
            }

//            }
        });
//            @Override

    }


//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                                           int[] grantResults){
//        switch (requestCode){
//            case 11: {
//                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
//                    if (ContextCompat.checkSelfPermission(ChatPageActivity.this,
//                            Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED){
//                        System.out.println("print: onRequest Location Permission granted");
//                        checkLocationEnabled();
//                    }
//                }else{
//                    System.out.println("print: onRequest Location Permission denied");
//                }
//                return;
//            }
//        }
//    }


    //-------------------------------------Document Download----------------------------------------

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            try {
                noNeedRefresh = true;
                Bundle bundle = data.getExtras();
                try {
                    ArrayList<Imagepath_caption> pathlist = (ArrayList<Imagepath_caption>) bundle.getSerializable("pathlist");
                    for (int i = 0; i < pathlist.size(); i++) {
                        String path = pathlist.get(i).getPath();
                        String caption = pathlist.get(i).getCaption();

                        if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
                            DisplayAlert("Unblock " + mReceiverName + " to send message?");
                        } else {
                            sendImageChatMessage(path, caption, false);
                        }
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == RESULT_LOAD_VIDEO && resultCode == RESULT_OK) {
            noNeedRefresh = true;
            ArrayList<Imagepath_caption> pathlist = new ArrayList<>();
            Bundle bundle = data.getExtras();
            try {
                pathlist = (ArrayList<Imagepath_caption>) bundle.getSerializable("pathlist");
                String path = pathlist.get(0).getPath();
                String caption = pathlist.get(0).getCaption();
                sendVideoChatMessage(path, caption, false);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else if (requestCode == REQUEST_SELECT_AUDIO && resultCode == RESULT_OK) {
            noNeedRefresh = true;
            String fileName = data.getStringExtra("FileName");
            String filePath = data.getStringExtra("FilePath");
            String duration = data.getStringExtra("Duration");
            sendAudioMessage(filePath, duration, MessageFactory.AUDIO_FROM_ATTACHMENT, false);

        } else if (requestCode == FilePickerConst.REQUEST_CODE_DOC) {
            noNeedRefresh = true;
            if (resultCode == Activity.RESULT_OK && data != null) {
                ArrayList<String> docPaths = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS);
                if (docPaths.size() > 0) {
                    sendDocumentMessage(docPaths.get(0), false);
                }
            }
        } else if (requestCode == RESULT_SHARE_LOCATION && resultCode == RESULT_OK && null != data) {
            System.out.println("TAG: onActivityResult RESULT_SHARE_LOCATION");
            noNeedRefresh = true;
            Place place = PlacePicker.getPlace(ChatPageActivity.this, data);
            if (place != null) {
                LatLng latlng = place.getLatLng();
                if (place.getAddress() != null) {
                    String address = place.getAddress().toString().trim();
                    String name = place.getName().toString();
                    if (name.length() > 0 && name.charAt(0) == '(') {
                        String[] parts = name.split(",");
                        name = parts[0].trim() + "," + parts[1].trim();
                    }

                    if (latlng != null) {

                        sendLocationMessage(latlng, name, address);
                    }
                }
            }

        } else if (requestCode == REQUEST_CODE_CONTACTS && resultCode == RESULT_OK && data != null) {
            noNeedRefresh = true;
            Bundle bundle = data.getExtras();
            try {
                contacts = (ArrayList<ContactToSend>) bundle.getSerializable("ContactToSend");
                String Uname = bundle.getString("name");
                String Title = bundle.getString("title");
                JSONArray phone = new JSONArray();
                JSONArray email = new JSONArray();
                JSONArray address = new JSONArray();
                JSONArray IM = new JSONArray();
                JSONArray Organisation = new JSONArray();
                JSONArray name = new JSONArray();
                String contactSmarttyId = "";


                ArrayList<SmarttyContactModel> savedSmarttyContacts = contactDB_sqlite.getSavedSmarttyContacts();

                if (contacts.size() > 0) {
                    for (int i = 0; i < contacts.size(); i++) {
                        if (contacts.get(i).getType().equalsIgnoreCase("Phone")) {

                            try {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("type", contacts.get(i).getSubType());
                                jsonObject.put("value", contacts.get(i).getNumber());
                                phone.put(jsonObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (contacts.get(i).getType().equalsIgnoreCase("Email")) {

                            try {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("type", contacts.get(i).getSubType());
                                jsonObject.put("value", contacts.get(i).getNumber());
                                email.put(jsonObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (contacts.get(i).getType().equalsIgnoreCase("Address")) {

                            try {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("type", contacts.get(i).getSubType());
                                jsonObject.put("value", contacts.get(i).getNumber());
                                address.put(jsonObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (contacts.get(i).getType().equalsIgnoreCase("Instant Messenger")) {
                            try {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("type", contacts.get(i).getSubType());
                                jsonObject.put("value", contacts.get(i).getNumber());
                                IM.put(jsonObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (contacts.get(i).getType().equalsIgnoreCase("Organisation")) {

                            try {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("type", contacts.get(i).getSubType());
                                jsonObject.put("value", contacts.get(i).getNumber());
                                Organisation.put(jsonObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (contacts.get(i).getType().equalsIgnoreCase("Name")) {

                            try {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("type", contacts.get(i).getSubType());
                                jsonObject.put("value", contacts.get(i).getNumber());
                                name.put(jsonObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    MessageItemChat itemChat = new MessageItemChat();
                    JSONObject finalObj = new JSONObject();
                    try {
                        finalObj.put("phone_number", phone);
                        finalObj.put("email", email);
                        finalObj.put("address", address);
                        finalObj.put("im", IM);
                        finalObj.put("organisation", Organisation);
                        finalObj.put("name", name);
                        ContactString = finalObj.toString();
                        itemChat.setDetailedContacts(ContactString);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (contacts.size() >= 1) {
                        number = contacts.get(0).getNumber();
                        number = number.replace(" ", "").replace("(", "").replace(")", "").replace("-", "");
                    } else {
                        number = "";
                    }

                    for (SmarttyContactModel smarttyModel : savedSmarttyContacts) {

                        if (number.equalsIgnoreCase(smarttyModel.getNumberInDevice())
                                || number.equalsIgnoreCase(smarttyModel.getMsisdn())) {
                            contactSmarttyId = smarttyModel.get_id();
                            break;
                        }
                    }

                }

                // Check whether contact is current user details
                String phNo = number.replace(" ", "").replace("(", "").replace(")", "").replace("-", "");
                if (phNo.equalsIgnoreCase(sessionManager.getPhoneNumberOfCurrentUser()) ||
                        phNo.equalsIgnoreCase(sessionManager.getUserMobileNoWithoutCountryCode())) {
                    contactSmarttyId = mCurrentUserId;
                }

                sendContactMessage("", contactSmarttyId, Uname, number, ContactString);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            try {
                ArrayList<Imagepath_caption> pathlist = new ArrayList<>();
                Bundle bundle = data.getExtras();
                try {
                    pathlist = (ArrayList<Imagepath_caption>) bundle.getSerializable("pathlist");

                    String path = pathlist.get(0).getPath();
                    String caption = pathlist.get(0).getCaption();

                    sendImageChatMessage(path, caption, false);


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

           /* try {
                String path = cameraImageUri.getPath();
                if (path == null) {
                    path = getRealFilePath(data);
                }
                MyLog.d("CameraResult", path);
                imagepath_caption = new Imagepath_caption();
                imagepath_caption.setPath(path);
                imagepath_caption.setCaption("");
                File imgFile = new  File(path);
                ImageView selectimage = new ImageView(this);;

                if(imgFile.exists()){

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

//                    selectimage = (ImageView) findViewById(R.id.imageviewTest);

                    selectimage.setImageBitmap(myBitmap);

                }
//                pathList.add(imagepath_caption);
                AppUtils.loadLocalImage(this,path,selectimage);


//                sendImageChatMessage(path);
            } catch (Exception e) {
                MyLog.e(TAG,"",e);
            }*/

        } else if (requestCode == RESULT_WALLPAPER && resultCode == RESULT_OK && null != data) {
            try {
                Uri selectedImageUri = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgDecodableString = cursor.getString(columnIndex);
                if (imgDecodableString == null) {
                    imgDecodableString = getRealFilePath(data);
                }
                cursor.close();

                session.putgalleryPrefs(imgDecodableString);
                Bitmap bitmap = BitmapFactory.decodeFile(imgDecodableString);
                background.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == EXIT_GROUP_REQUEST_CODE) { // Finish activity when exit group by user
            if (resultCode == RESULT_OK && data != null) {
                boolean isExitFromGroup = data.getBooleanExtra("exitFromGroup", false);
                boolean ismuteGroupchange = data.getBooleanExtra("ismutechange", false);
                boolean isgroupemty = data.getBooleanExtra("isgroupempty", false);
                if (isExitFromGroup) {
                    finish();
                }
                if (ismuteGroupchange) {
                    finish();
                }
                if (isgroupemty) {

                    rlSend.setVisibility(View.GONE);
                    RelativeLayout_group_delete.setVisibility(View.VISIBLE);
                }
            }
        } else if (requestCode == MUTE_ACTIVITY) { // Finish activity when exit group by user
            if (resultCode == RESULT_OK && data != null) {
                boolean ismutechange = data.getBooleanExtra("muteactivity", false);
                if (ismutechange) {
                    finish();
                }
            }
        } else if (requestCode == REQUEST_CODE_FORWARD_MSG) {
            if (resultCode == RESULT_OK) {
                boolean isMultiForward = data.getBooleanExtra("MultiForward", false);
                if (isMultiForward) {
                    reloadAdapter();
                    showUnSelectedActions();
                }
            } else {
                if (selectedChatItems != null && selectedChatItems.size() > 0) {
                    for (MessageItemChat msgItem : selectedChatItems) {
                        int index = mChatData.indexOf(msgItem);
                        if (index > -1) {
                            mChatData.get(index).setSelected(true);
                        }
                    }

                    Log.e(TAG, "REQUEST_CODE_FORWARD_MSG: notifyDataSetChanged");

                    madapter.notifyDataSetChanged();
                } else {
                    showUnSelectedActions();
                }
            }
        } else if (requestCode == ADD_CONTACT && resultCode == RESULT_OK) {
            mContactSaved = true;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    llAddBlockContact.setVisibility(View.GONE);
                    SmarttyContactsService.savedNumber = user_name.getText().toString();
                    SmarttyContactsService.bindContactService(ChatPageActivity.this, false);
                    SmarttyContactsService.setBroadCastSavedName(new SmarttyContactsService.BroadCastSavedName() {
                        @Override
                        public void savedName(final String name) {
                            user_name.post(new Runnable() {
                                @Override
                                public void run() {
                                    user_name.setText(name);
                                    SharedPreference.getInstance().save(context, "userName", name);
                                    llAddBlockContact.setVisibility(View.GONE);

                                }
                            });


                        }
                    });
                }
            });
            //      llAddBlockContact.setVisibility(View.GONE);
        }

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private String getRealFilePath(Intent data) {
        Uri selectedImage = data.getData();
        String wholeID = DocumentsContract.getDocumentId(selectedImage);
        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];
        String[] column = {MediaStore.Images.Media.DATA};
        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = getContentResolver().
                query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);
        String filePath = "";
        int columnIndex = 0;
        if (cursor != null) {
            columnIndex = cursor.getColumnIndex(column[0]);
        }
        if (cursor != null && cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    private synchronized void reloadAdapter() {

        final ArrayList<MessageItemChat> dbItems = db.selectAllMessagesWithLimit(docId, chatType,
                "", MessageDbController.MESSAGE_SELECTION_LIMIT);
        Collections.sort(dbItems, msgComparator);

        mChatData.clear();
        mChatData.addAll(dbItems);
        sendAllAcksToServer(dbItems);
        notifyDatasetChange();
    }

    //-------------------------------------Send Image Chat-----------------------------------------
    public void sendImageChatMessage(String imgPath, String caption, boolean isRetry) {


        if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
            DisplayAlert("Unblock " + mReceiverName + " to send message?");
        } else {
            int isTagapplied = 0;
            ArrayList<String> names = new ArrayList<>();
            ArrayList<String> userID = new ArrayList<>();

            if (imgPath != null) {
                String data_at = caption;
                if (ImagecaptionActivity.at_memberlist != null && ImagecaptionActivity.at_memberlist.size() >= 0)
                    for (GroupMembersPojo groupMembersPojo : ImagecaptionActivity.at_memberlist) {
                        String userName = "@" + groupMembersPojo.getContactName();
                        names.add(userName);
                        userID.add(groupMembersPojo.getUserId());
                        if (data_at.contains(userName)) {
                            data_at = data_at.replace(userName, TextMessage.TAG_KEY + groupMembersPojo.getUserId() + TextMessage.TAG_KEY);
                            isTagapplied = 1;

                        }

                    }
                PictureMessage message = (PictureMessage) MessageFactory.getMessage(MessageFactory.picture, this);

                MessageItemChat item = null;
                boolean canSent = false;

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imgPath, options);
                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;

                if (isGroupChat) {
                    if (enableGroupChat) {
                        canSent = true;
                        message.getGroupMessageObject(to, imgPath, user_name.getText().toString());
                        item = message.createMessageItem(true, caption, imgPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                                mReceiverId, user_name.getText().toString(), imageWidth, imageHeight);
                        item.setGroupName(user_name.getText().toString());
                    } else {
                        Toast.makeText(ChatPageActivity.this, "You are not a member in this group", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    canSent = true;
                    message.getMessageObject(to, imgPath, false);
                    item = message.createMessageItem(true, caption, imgPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                            mReceiverId, user_name.getText().toString(), imageWidth, imageHeight);
                    item.setSenderMsisdn(receiverMsisdn);
                }

                if (isTagapplied == 1) {
                    item.setTagapplied(true);
                    item.setSetArrayTagnames(names);
                    item.setEditTextmsg(caption);
                    item.setUserId_tag(userID);
                }

                if (item != null) {
                    item.setCaption(caption);
                }
                if (canSent) {
                    if (!isRetry) {

                        db.updateChatMessage(item, chatType);
                        if (!isAlreadyExist(item))
                            mChatData.add(item);
                        notifyDatasetChange();
                    }
                    String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(imgPath);
                    String imgName = item.getMessageId() + fileExtension;
                    String docId;
                    if (isGroupChat) {
                        docId = mCurrentUserId + "-" + to + "-g";
                    } else {
                        docId = mCurrentUserId + "-" + to;
                    }

                    JSONObject uploadObj = (JSONObject) message.createImageUploadObject(item.getMessageId(), docId, imgName, imgPath,
                            user_name.getText().toString(), data_at, chatType, false);
                    uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);

                    if (ImagecaptionActivity.at_memberlist != null) {
                        ImagecaptionActivity.at_memberlist.clear();
                    }

                }
            }
        }

    }

    public void sendVideoChatMessage(String videoPath, String caption, boolean isRetry) {


        if (ImagecaptionActivity.CompressFilePath != null && !ImagecaptionActivity.CompressFilePath.equalsIgnoreCase("")) {

            // videoPath=ImagecaptionActivity.CompressFilePath;
            MyLog.e("CheckEventConnection", "CompressVideoPathSetChatActivity" + videoPath);
        }


        if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
            DisplayAlert("Unblock " + mReceiverName + " to send message?");
        } else {
            int isTagapplied = 0;
            ArrayList<String> names = new ArrayList<>();
            ArrayList<String> userID = new ArrayList<>();

            if (videoPath != null) {
                String data_at = caption;
                if (ImagecaptionActivity.at_memberlist != null && ImagecaptionActivity.at_memberlist.size() >= 0)
                    for (GroupMembersPojo groupMembersPojo : ImagecaptionActivity.at_memberlist) {
                        String userName = "@" + groupMembersPojo.getContactName();
                        names.add(userName);
                        userID.add(groupMembersPojo.getUserId());
                        if (data_at.contains(userName)) {
                            data_at = data_at.replace(userName, TextMessage.TAG_KEY + groupMembersPojo.getUserId() + TextMessage.TAG_KEY);
                            isTagapplied = 1;

                        }
                    }
                VideoMessage message = (VideoMessage) MessageFactory.getMessage(MessageFactory.video, this);

                MessageItemChat item = null;
                boolean canSent = false;

                if (isGroupChat) {
                    if (enableGroupChat) {
                        canSent = true;
                        message.getGroupMessageObject(to, user_name.getText().toString(), caption);
                        item = message.createMessageItem(true, videoPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                                mReceiverId, user_name.getText().toString(), caption);
                        item.setGroupName(user_name.getText().toString());
                    } else {
                        Toast.makeText(ChatPageActivity.this, "You are not a member in this group", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    canSent = true;
                    message.getMessageObject(to, videoPath, false);
                    item = message.createMessageItem(true, videoPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                            mReceiverId, user_name.getText().toString(), caption);
                    item.setSenderMsisdn(receiverMsisdn);
                }

                if (isTagapplied == 1) {
                    item.setTagapplied(true);
                    item.setSetArrayTagnames(names);
                    item.setEditTextmsg(caption);
                    item.setUserId_tag(userID);
                }


                if (canSent) {

                    try {

                        Bitmap thumbBmp = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MICRO_KIND);
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        thumbBmp.compress(Bitmap.CompressFormat.PNG, 10, out);
                        byte[] thumbArray = out.toByteArray();

                        try {
                            out.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT);
                        if (thumbData != null) {
                            item.setThumbnailData(thumbData);

                            item.setThumb_bitmap_image(thumbBmp);

                        }
                        String bitmapstring = String.valueOf(thumbBmp);
                        item.setVideobitmap(bitmapstring);
                        item.setCaption(caption);

                    } catch (Exception e) {

                    }

                    if (!isRetry) {

                        db.updateChatMessage(item, chatType);
                        if (!isAlreadyExist(item))
                            mChatData.add(item);
                        notifyDatasetChange();
                    }
                    String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(videoPath);
                    String videoName = item.getMessageId() + fileExtension;
                    String docId;
                    if (isGroupChat) {
                        docId = mCurrentUserId + "-" + to + "-g";
                    } else {
                        docId = mCurrentUserId + "-" + to;
                    }

                    JSONObject uploadObj = (JSONObject) message.createVideoUploadObject(item.getMessageId(), docId,
                            videoName, videoPath, user_name.getText().toString(), data_at, chatType, false);
                    uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);


                    if (ImagecaptionActivity.at_memberlist != null) {
                        ImagecaptionActivity.at_memberlist.clear();
                    }
                }
            }
        }


    }

    //----------------------------------------Send Video Chat--------------------------------------------------------

    public void sendAudioMessage(String filePath, String duration, int audioFrom, boolean retry) {


        if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
            DisplayAlert("Unblock " + mReceiverName + " to send message?");
        } else {
            AudioMessage message = (AudioMessage) MessageFactory.getMessage(MessageFactory.audio, this);
            MessageItemChat item = null;
            boolean canSent = false;

            if (isGroupChat) {
                if (enableGroupChat) {
                    canSent = true;
                    message.getGroupMessageObject(to, filePath, user_name.getText().toString());
                    item = message.createMessageItem(true, filePath, duration, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                            mReceiverId, user_name.getText().toString(), audioFrom);
                    item.setGroupName(user_name.getText().toString());
                } else {
                    Toast.makeText(ChatPageActivity.this, "You are not a member in this group", Toast.LENGTH_SHORT).show();
                }
            } else {
                canSent = true;
                message.getMessageObject(to, filePath, false);
                item = message.createMessageItem(true, filePath, duration, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                        mReceiverId, user_name.getText().toString(), audioFrom);
                item.setSenderMsisdn(receiverMsisdn);
                item.setaudiotype(audioFrom);
            }

            if (canSent) {

                if (!retry) {

                    db.updateChatMessage(item, chatType);
                    if (!isAlreadyExist(item))
                        mChatData.add(item);
                    notifyDatasetChange();
                }


                String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(filePath);
                String audioName = item.getMessageId() + fileExtension;
                String docId;
                if (isGroupChat) {
                    docId = mCurrentUserId + "-" + to + "-g";
                } else {
                    docId = mCurrentUserId + "-" + to;
                }

                JSONObject uploadObj = (JSONObject) message.createAudioUploadObject(item.getMessageId(), docId,
                        audioName, filePath, duration, user_name.getText().toString(), audioFrom, chatType, false);


                uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);
            }
        }

    }

    //-----------------------------------------------------Send Audio Chat-------------------------------------------------------

    public void sendDocumentMessage(String filePath, boolean Retry) {


        if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
            DisplayAlert("Unblock " + mReceiverName + " to send message?");
        } else {
            DocumentMessage message = (DocumentMessage) MessageFactory.getMessage(MessageFactory.document, this);

            MessageItemChat item = null;
            boolean canSent = false;

            if (isGroupChat) {
                if (enableGroupChat) {
                    canSent = true;
                    message.getGroupMessageObject(to, filePath, user_name.getText().toString());
                    item = message.createMessageItem(true, filePath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                            mReceiverId, user_name.getText().toString());
                    item.setGroupName(user_name.getText().toString());
                } else {
                    Toast.makeText(ChatPageActivity.this, "You are not a member in this group", Toast.LENGTH_SHORT).show();
                }
            } else {
                canSent = true;
                message.getMessageObject(to, filePath, false);
                item = message.createMessageItem(true, filePath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                        mReceiverId, user_name.getText().toString());
                item.setSenderMsisdn(receiverMsisdn);
            }

            if (canSent) {

                if (!Retry) {

                    db.updateChatMessage(item, chatType);
                    if (!isAlreadyExist(item))
                        mChatData.add(item);
                    notifyDatasetChange();
                }


                String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(filePath);
                String docName = item.getMessageId() + fileExtension;
                String docId;
                if (isGroupChat) {
                    docId = mCurrentUserId + "-" + to + "-g";
                } else {
                    docId = mCurrentUserId + "-" + to;
                }

                JSONObject uploadObj = (JSONObject) message.createDocUploadObject(item.getMessageId(), docId,
                        docName, filePath, user_name.getText().toString(), chatType, false);
                uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);
            }

        }

    }


    //-------------------------------------------------Send Document Chat---------------------------------------------------------

    public void sendLocationMessage(final LatLng latlng, final String addressName, final String address) {


        if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
            DisplayAlert("Unblock " + mReceiverName + " to send message?");
        } else {
//            String apiKey = getString(R.string.google_api_key);
            if (getResources().getBoolean(R.bool.is_base)) {
            }
            final String thumbUrl = "https://maps.googleapis.com/maps/api/staticmap?center=" + latlng.latitude + "," + latlng.longitude
                    + "&zoom=10&size=180x180&maptype=roadmap&markers=color:red%7Clabel:N%7C" + latlng.latitude
                    + "," + latlng.longitude + "&key=" + apiKey;


            //   https:
//maps.googleapis.com/maps/api/staticmap?center=Chennai&zoom=13&size=600x300&key=AIzaSyCR7bhMU4qZncunudEsvt6M3riu0P5_rEs
            initProgress(getString(R.string.loading_address), true);
            showProgressDialog();

            ImageLoader imageLoader = CoreController.getInstance().getImageLoader();
            imageLoader.get(thumbUrl, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer imageContainer, boolean b) {
                    Bitmap bitmap = imageContainer.getBitmap();
                    //use bitmap
                    if (bitmap != null) {

                        hideProgressDialog();

                        Bitmap newBmp = Bitmap.createScaledBitmap(bitmap, 100, 100, true);

                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        newBmp.compress(Bitmap.CompressFormat.JPEG, 50, out);
                        byte[] thumbArray = out.toByteArray();
                        try {
                            out.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT);
                        if (thumbData != null) {
                            thumbData = thumbData.replace("\n", "");
                            if (!thumbData.startsWith("data:image/jpeg;base64,")) {
                                thumbData = "data:image/jpeg;base64," + thumbData;
                            }

                            String point = latlng.latitude + "," + latlng.longitude;

                            String url = "https://maps.google.com/maps?q=" + point
                                    + " (" + addressName + ")&amp;z=15&amp;hl=en";

                            //point= addressName;

                            SendMessageEvent messageEvent = new SendMessageEvent();
                            LocationMessage message = (LocationMessage) MessageFactory.getMessage(MessageFactory.location, ChatPageActivity.this);

                            JSONObject msgObj;
                            String receiverName = user_name.getText().toString();
                            if (isGroupChat) {
                                messageEvent.setEventName(SocketManager.EVENT_GROUP);
                                msgObj = (JSONObject) message.getGroupMessageObject(to, point, receiverName);
                            } else {
                                msgObj = (JSONObject) message.getMessageObject(to, point, false);
                                messageEvent.setEventName(SocketManager.EVENT_MESSAGE);
                            }

                            MessageItemChat item = message.createMessageItem(true, point, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                                    mReceiverId, receiverName, addressName, address, url, thumbUrl, thumbData);
                            msgObj = (JSONObject) message.getLocationObject(msgObj, addressName, address, url, thumbUrl, thumbData);
                            messageEvent.setMessageObject(msgObj);

                            if (isGroupChat) {
                                if (enableGroupChat) {
                                    item.setGroupName(user_name.getText().toString());
                                    db.updateChatMessage(item, chatType);
                                    if (!isAlreadyExist(item))
                                        mChatData.add(item);
                                    EventBus.getDefault().post(messageEvent);

                                    notifyDatasetChange();
                                } else {
                                    Toast.makeText(ChatPageActivity.this, "You are not a member in this group", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                item.setSenderMsisdn(receiverMsisdn);
                                item.setSenderName(user_name.getText().toString());
                                db.updateChatMessage(item, chatType);
                                if (!isAlreadyExist(item))
                                    mChatData.add(item);
                                EventBus.getDefault().post(messageEvent);
                                notifyDatasetChange();
                            }
                        }
                    }
                }

                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    hideProgressDialog();
                    String json = null;

                    NetworkResponse response = volleyError.networkResponse;
                    if (response != null && response.data != null) {
                        switch (response.statusCode) {
                            case 403:
                                //     MyLog.e(TAG,"statuscode"+response.statusCode);

                                json = new String(response.data);
                                Toast.makeText(ChatPageActivity.this, getString(R.string.enablepayment), Toast.LENGTH_SHORT).show();

                              /*  json = trimMessage(json, "errors");
                                if (json != null) displayMessage(json,context);*/
                                break;
                        }


                    } else {
                        //  Log.i("Volley Error", error.toString());
                        /*Toast.makeText(getApplicationContext(),
                                "Error: " + error.toString(),
                                Toast.LENGTH_LONG).show();*/
                        Toast.makeText(ChatPageActivity.this, getString(R.string.networkerror), Toast.LENGTH_SHORT).show();


                    }
                }
            });
        }


    }

    //Somewhere that has access to a context
    public void displayMessage(String toastString, Context mContext) {
        Toast.makeText(mContext, toastString, Toast.LENGTH_LONG).show();
    }

    public String trimMessage(String json, String key) {
        String trimmedString = null;

        try {
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return trimmedString;
    }

    //--------------------------------------------------Send Location Chat-----------------------------------------------------------------------

    public void sendContactMessage(String data, String contactSmarttyId, String contactName, String contactNumber, String contactDetail) {


        if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
            DisplayAlert("Unblock " + mReceiverName + " to send message?");
        } else {
            SendMessageEvent messageEvent = new SendMessageEvent();
            ContactMessage message = (ContactMessage) MessageFactory.getMessage(MessageFactory.contact, this);

            JSONObject msgObj;
            if (isGroupChat) {
                messageEvent.setEventName(SocketManager.EVENT_GROUP);
                msgObj = (JSONObject) message.getGroupMessageObject(to, data, user_name.getText().toString(), contactSmarttyId, contactName, contactNumber, contactDetail);

            } else {
                messageEvent.setEventName(SocketManager.EVENT_MESSAGE);
                msgObj = (JSONObject) message.getMessageObject(to, data, contactSmarttyId, contactName, contactNumber, contactDetail, false);
            }

            messageEvent.setMessageObject(msgObj);
            MessageItemChat item = message.createMessageItem(true, data, MessageFactory.DELIVERY_STATUS_NOT_SENT, mReceiverId,
                    user_name.getText().toString(), contactName, contactNumber, contactSmarttyId, contactDetail);
            item.setGroupName(user_name.getText().toString());
            item.setSenderMsisdn(receiverMsisdn);
            item.setContactSmarttyId(contactSmarttyId);
      /*  String image = ("uploads/users/").concat(item.get_id()).concat(".jpg");
        String path =  Constants.SOCKET_IP.concat(image);*/
            item.setAvatarImageUrl(receiverAvatar);
            item.setDetailedContacts(contactDetail);

            if (isGroupChat && enableGroupChat) {
                db.updateChatMessage(item, chatType);
                if (!isAlreadyExist(item))
                    mChatData.add(item);
                notifyDatasetChange();
                EventBus.getDefault().post(messageEvent);
            } else if (!isGroupChat) {
                db.updateChatMessage(item, chatType);
                if (!isAlreadyExist(item))
                    mChatData.add(item);
                notifyDatasetChange();
                EventBus.getDefault().post(messageEvent);
            }
        }

    }

    //-------------------------------------------------Send Contact Message--------------------------------------------------------

    private boolean isAlreadyExist(MessageItemChat messageItemChat) {
        String msgId = messageItemChat.getMessageId();
        if (messageIds.contains(msgId)) {
            return true;
        }
        messageIds.add(msgId);
        return false;
    }

    private void notifyDatasetChange() {
        setEncryptionMsg();
        chat_list.post(new Runnable() {
            @Override
            public void run() {
                madapter.setIsGroupChat(isGroupChat);

                Log.e(TAG, "notifyDatasetChange: notifyDataSetChanged");

                madapter.notifyDataSetChanged();
                if (mChatData.size() > 0)
                    chat_list.setSelection(mChatData.size() - 1);
            }
        });

    }

    private void DisplayAlert(final String txt) {
        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(txt);
        dialog.setNegativeButtonText("Cancel");
        dialog.setPositiveButtonText("Unblock");
        dialog.setCancelable(false);
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                putBlockUser();
                dialog.dismiss();
            }

            @Override

            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getSupportFragmentManager(), "Unblock a person");

    }

    private void putBlockUser() {
        BlockUserUtils.changeUserBlockedStatus(ChatPageActivity.this, EventBus.getDefault(),
                mCurrentUserId, mReceiverId, false);
/*        BlockUserUtils.changeUserBlockedStatus(ChatPageActivity.this, EventBus.getDefault(),
                mCurrentUserId, mReceiverId, true);*/

    }

    private void loadFileUploaded(JSONObject object) {

        try {
            int uploadedSize = object.getInt("UploadedSize");
            int totalSize = object.getInt("size");
            String msgId = object.getString("id");
            final int progress = (uploadedSize * 100) / totalSize;
            String Upload_status = db.fileuploadStatusget(msgId);
            MyLog.e("CheckEventConnection", "UPLOAD STATUS CHAT CLASS" + "" + Upload_status);
            if (Upload_status.equalsIgnoreCase("uploading")) {
                for (MessageItemChat msgItem : mChatData) {
                    if (msgItem.getMessageId().equalsIgnoreCase(msgId)) {
                        final int index = mChatData.indexOf(msgItem);
                        if (progress >= mChatData.get(index).getUploadDownloadProgress()) {
                            sharedprf_video_uploadprogress.setfileuploadingprogress(progress, msgId);
                            if (progress == 0) {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        mChatData.get(index).setUploadDownloadProgress(2);

                                        Log.e(TAG, "loadFileUploaded: notifyDataSetChanged");

                                        madapter.notifyDataSetChanged();
                                    }
                                });
                            } else {
                                runOnUiThread(
                                        new Runnable() {
                                            public void run() {
                                                mChatData.get(index).setUploadDownloadProgress(progress);

                                                Log.e(TAG, "loadFileUploaded: notifyDataSetChanged");

                                                madapter.notifyDataSetChanged();
                                            }
                                        });
                            }
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    progressglobal = progress;
                                }
                            });
                        }
                        break;
                    }
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadPrivacySetting(ReceviceMessageEvent event) {
        Object[] object = event.getObjectsArray();

        try {
            JSONObject jsonObject = new JSONObject(object[0].toString());
            String from = jsonObject.getString("from");
            String status = (String) jsonObject.get("status");
            String lastseen = String.valueOf(jsonObject.get("last_seen"));
            String profile = String.valueOf(jsonObject.get("profile_photo"));
            JSONArray contactUserList = jsonObject.getJSONArray("contactUserList");
            if (from.equalsIgnoreCase(mReceiverId)) {
                Boolean iscontact = false;
                if (contactUserList != null) {
                    iscontact = contactUserList.toString().contains(mCurrentUserId);
                }
                if (!isGroupChat) {
                    if (lastseen.equalsIgnoreCase("nobody")) {
                        canShowLastSeen = false;
                        if (!status_textview.getText().toString().equalsIgnoreCase("online")) {
                            status_textview.setVisibility(View.GONE);
                        }
                    } else if (lastseen.equalsIgnoreCase("everyone")) {
                        status_textview.setVisibility(View.VISIBLE);
                        canShowLastSeen = true;
                    } else if (lastseen.equalsIgnoreCase("mycontacts") && iscontact) {
                        canShowLastSeen = true;
                        status_textview.setVisibility(View.VISIBLE);
                    } else {
                        canShowLastSeen = false;
                        if (!status_textview.getText().toString().equalsIgnoreCase("online")) {
                            status_textview.setVisibility(View.GONE);
                        }
                    }

                    if (contactDB_sqlite.getBlockedMineStatus(to, false).equals(ContactDB_Sqlite.BLOCKED_STATUS)) {
                        canShowLastSeen = false;
                        status_textview.setVisibility(View.GONE);
                    }


                }
                profilepicupdation();
            }

        } catch (Exception e) {

        }
    }

    private void profilepicupdation() {
        Log.d(TAG, "profilepicupdation:11");
        //  if (receiverAvatar != null) {
        if (!isGroupChat) {
            getcontactname.configCircleProfilepic(user_profile_image, to, true, true, R.drawable.avatar_contact);
        } else {

            groupInfoSession = new GroupInfoSession(this);
            mCurrentUserId = SessionManager.getInstance(this).getCurrentUserID();
            Log.d(TAG, "profilepicupdation: to " + to);
            Log.d(TAG, "profilepicupdation: mCurrentUserId " + mCurrentUserId);
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(mCurrentUserId.concat("-").concat(to).concat("-g"));
            receiverAvatar = infoPojo.getAvatarPath();
            receiverAvatar = AppUtils.getValidProfilePath(receiverAvatar);
            Log.d(TAG, "profilepicupdation: " + receiverAvatar);
            if (!AppUtils.isEmptyImage(receiverAvatar)) {

                AppUtils.loadImage(this, receiverAvatar, user_profile_image, 100, R.drawable.avatar_group);

            } else {
                user_profile_image.setImageResource(R.drawable.avatar_group);
            }

        }
        //  }

    }

    //----------------------------------------------Load Settings Details------------------------------------------------------

    private void load_clear_chat(String data) {
        try {
            JSONObject object = new JSONObject(data);
            MyLog.e(TAG, "loadClearChat" + object);

            try {
                String from = object.getString("from");
                String convId = object.getString("convId");
                String type = object.getString("type");

                if (from.equalsIgnoreCase(mCurrentUserId) && convId.equalsIgnoreCase(mConvId)) {
                    int star_status;
                    Boolean starred = false;
                    if (object.has("star_status")) {
                        star_status = object.getInt("star_status");
                        starred = star_status != 0;
                    }

                    if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {

                        if (starred) {
                            ArrayList<MessageItemChat> value = new ArrayList<>();
                            for (int i = 0; i < mChatData.size(); i++) {
                                if (mChatData.get(i).getStarredStatus().equalsIgnoreCase(MessageFactory.MESSAGE_UN_STARRED)) {
                                    value.add(mChatData.get(i));
                                }
                            }
                            for (int i = 0; i < value.size(); i++) {
                                if (value.get(i).isMediaPlaying()) {
                                    int chatIndex = mChatData.indexOf(value.get(i));
                                    if (chatIndex > -1 && mChatData.get(chatIndex).isMediaPlaying()) {
                                        madapter.stopAudioOnMessageDelete(chatIndex);
                                    }
                                }
                                mChatData.remove(value.get(i));
                            }

                        } else {
                            madapter.stopAudioOnClearChat();
                            mChatData.clear();

                        }
                    } else {

                        if (starred) {
                            ArrayList<MessageItemChat> value = new ArrayList<>();
                            for (int i = 0; i < mChatData.size(); i++) {
                                if (mChatData.get(i).getStarredStatus().equalsIgnoreCase(MessageFactory.MESSAGE_UN_STARRED)) {
                                    value.add(mChatData.get(i));
                                }
                            }

                            for (int i = 0; i < value.size(); i++) {
                                if (value.get(i).isMediaPlaying()) {
                                    int chatIndex = mChatData.indexOf(value.get(i));
                                    if (chatIndex > -1 && mChatData.get(chatIndex).isMediaPlaying()) {
                                        madapter.stopAudioOnMessageDelete(chatIndex);
                                    }
                                }
                                mChatData.remove(value.get(i));
                            }

                        } else {
                            mChatData.clear();

                        }
                    }
                    Log.e(TAG, "load_clear_chat: notifyDataSetChanged");


                    madapter.notifyDataSetChanged();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void load_delete_chat(String data) {
        try {
            JSONObject object = new JSONObject(data);
            MyLog.e(TAG, "load_delete_chat" + object);
            try {
                String from = object.getString("from");
                String convId = object.getString("convId");
                String type = object.getString("type");
                String lastId = object.getString("lastId");

                long mLong = 0L;
                if (object.has("delete_opponent")) {
                    //Add 2000 millisec
                    mLong = Long.parseLong(lastId) + 2000;
                    String delete_opponent = object.getString("delete_opponent");
                    if (Integer.valueOf(delete_opponent) == 0) {
                        for (int i = 0; i < mChatData.size(); i++) {
                            String groupid[] = mChatData.get(i).getMessageId().split("-");
                            if (Long.parseLong(groupid[2]) <= mLong) {
                                mChatData.remove(mChatData.get(i));
                                MyLog.e("reemoved", "removed" + groupid[2] + "groupid[2]" + "mLong" + mLong + mChatData.size());
                            } else {
                                // mChatData.remove(mChatData.get(i));
                                MyLog.e("reemoved", "not removed" + groupid[2] + "groupid[2]" + "mLong" + mLong);
                                //    break;
                            }
                        }
                        madapter.notifyDataSetChanged();
                    } else if (Integer.valueOf(delete_opponent) == 1) {
                        // lastId=lastId+2000;
                        mLong = Long.parseLong(lastId) + 2000;
                        for (int i = 0; i < mChatData.size(); i++) {
                            String groupid[] = mChatData.get(i).getMessageId().split("-");
                            if (Long.parseLong(groupid[2]) <= mLong) {
                                mChatData.remove(mChatData.get(i));
                                MyLog.e("reemoved", "mChatData" + mChatData.size());
                            } else {
                                break;
                            }
                        }
                        madapter.notifyDataSetChanged();
                    }
                } else {
                    if (!from.equalsIgnoreCase(mCurrentUserId)) {
                        mChatData.clear();
                        madapter.notifyDataSetChanged();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //-----------------------------------Profile Picture Updation-------------------------------------------

    private void loadTypingStatus(ReceviceMessageEvent event) {
        Object[] object = event.getObjectsArray();
        try {
            JSONObject jsonObject = new JSONObject(object[0].toString());
            String from_typing = (String) jsonObject.get("from");
            String to_typing = (String) jsonObject.get("to");
            String convId = (String) jsonObject.get("convId");
            String type = (String) jsonObject.get("type");
            String typingPerson = "";
            if (!isGroupChat) {

                boolean blockedStatus = contactDB_sqlite.getBlockedStatus(from_typing, false).equals("1");

                if (from_typing.equalsIgnoreCase(to) && type.equalsIgnoreCase("single") &&
                        convId.equals(mConvId) && !blockedStatus) {
                    status_textview.setText("typing...");
                    handleReceiverTypingEvent();
                }
            } else {
                if (!from_typing.equalsIgnoreCase(from) && to_typing.equalsIgnoreCase(to)) {

                    if (!from_typing.equalsIgnoreCase(from)) {

                        String msisdn = contactDB_sqlite.getSingleData(from_typing, ContactDB_Sqlite.MSISDN);
                        if (msisdn != null) {
                            typingPerson = msisdn;
                            typingPerson = getcontactname.getSendername(from_typing, typingPerson);

                        }

                        for (int i = 0; i < mChatData.size(); i++) {
                            MessageItemChat item = mChatData.get(i);
                            String groupid[] = item.getMessageId().split("-");
                            if (convId.equalsIgnoreCase(groupid[1])) {
                                status_textview.setText(typingPerson + " is typing....");
                                handleReceiverTypingEvent();
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //-----------------------------------------------Clear Chat-------------------------------------------------------------------

    private void handleReceiverTypingEvent() {
        toLastTypedAt = Calendar.getInstance().getTimeInMillis() + MessageFactory.TYING_MESSAGE_MIN_TIME_DIFFERENCE;
        toTypingHandler.postDelayed(toTypingRunnable, MessageFactory.TYING_MESSAGE_TIMEOUT);
    }

    //-------------------------------------------------Typing Status-------------------------------------------------------------

    private void loadOnlineStatus(ReceviceMessageEvent event) {
        Object[] object = event.getObjectsArray();
        try {
            JSONObject jsonObject = new JSONObject(object[0].toString());
            String id = (String) jsonObject.get("_id");
            String status = String.valueOf(jsonObject.get("Status"));
            if (id.equalsIgnoreCase(to)) {
                if (status.equalsIgnoreCase("1")) {
                    status_textview.setText("Online");
                } else {
                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
                    if (canShowLastSeen && !sessionManager.getLastSeenVisibleTo().equalsIgnoreCase("nobody")) {
                        String lastSeen = jsonObject.getString("DateTime");
                        setOnlineStatusText(lastSeen);

                    } else if (!canShowLastSeen /*&& contactDB_sqlite.getLastSeenVisibility(id).equalsIgnoreCase("nobody")*/) {
                        Log.e(TAG, "nobody" + contactDB_sqlite.getLastSeenVisibility(id).equalsIgnoreCase("nobody"));

                        status_textview.setText("");
                    } else {
                        //      status_textview.setText("");
                        String lastSeen = jsonObject.getString("DateTime");
                        setOnlineStatusText(lastSeen);

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOnlineStatusText(String lastSeen) {
        try {
            Long lastSeenTime = Long.parseLong(lastSeen);
            Long serverDiff = sessionManager.getServerTimeDifference();
            lastSeenTime = lastSeenTime + serverDiff;

            Date lastSeenAt = new Date(lastSeenTime);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
            Date date2 = new Date();
            String currentDate = sdf.format(date2);
            currentDate = currentDate.substring(0, 10);

            String strLastSeenAt = sdf.format(lastSeenAt);
            String onlineStatus;
            //  status_textview.setInitialDelay(2000);
            // Remove a character every 150ms
            // status_textview.setCharacterDelay(250);

            if (lastSeen != null) {
                if (currentDate.equals(strLastSeenAt.substring(0, 10))) {
                    lastSeen = SmarttyUtilities.convert24to12hourformat(strLastSeenAt.substring(11, 19));
                    lastSeen = lastSeen.replace(".", "");
                    //     onlineStatus = "last seen at " + lastSeen;
                    onlineStatus = lastSeen;
                } else {
                    String last = SmarttyUtilities.convert24to12hourformat(strLastSeenAt.substring(11, 19));
                    String[] separated = strLastSeenAt.substring(0, 10).split("-");
                    last = last.replace(".", "");
                    String date = separated[2] + "-" + separated[1] + "-" + separated[0];
                    //   onlineStatus = "last seen " + date + " " + last;
                    onlineStatus = date + " " + last;
                }
                //  status_textview.animateText(LAST_SEEN_TEXT, LAST_SEEN_TEXT + onlineStatus);

                status_textview.setText(LAST_SEEN_TEXT + onlineStatus);

            }
        } catch (Exception e) {
            status_textview.setText("");
        }
    }

    private void loadCurrentTimeMessage(ReceviceMessageEvent event) {
        Object[] object = event.getObjectsArray();
        try {
            JSONObject jsonObject = new JSONObject(object[0].toString());
            String id = (String) jsonObject.get("_id");
            String status = String.valueOf(jsonObject.get("Status"));

            if (id.equalsIgnoreCase(to)) {
                isLastSeenCalled = true;
                canShowLastSeen = true;


                JSONObject privacyObj = jsonObject.getJSONObject("Privacy");
                if (privacyObj.has("last_seen")) {

                    String showLastSeen = privacyObj.getString("last_seen");
                    if (showLastSeen.equalsIgnoreCase(ContactDB_Sqlite.PRIVACY_TO_NOBODY)) {
                        canShowLastSeen = false;
                        contactDB_sqlite.updateLastSeenVisibility(to, ContactDB_Sqlite.PRIVACY_STATUS_NOBODY);
                    } else if (showLastSeen.equalsIgnoreCase(ContactDB_Sqlite.PRIVACY_TO_MY_CONTACTS)) {
                        String isContactUser = jsonObject.getString("is_contact_user");

                        contactDB_sqlite.updateLastSeenVisibility(to, ContactDB_Sqlite.PRIVACY_STATUS_MY_CONTACTS);
                        contactDB_sqlite.updateMyContactStatus(to, isContactUser);

                        if (isContactUser.equals("0")) {
                            canShowLastSeen = false;
                        }
                    } else {
                        contactDB_sqlite.updateLastSeenVisibility(to, ContactDB_Sqlite.PRIVACY_STATUS_EVERYONE);
                    }
                }
//Check it is blocked or not
                if (contactDB_sqlite.getBlockedMineStatus(to, false).equals(ContactDB_Sqlite.BLOCKED_STATUS)) {
                    canShowLastSeen = false;
                    status_textview.setVisibility(View.GONE);
                }

                if ("1".equalsIgnoreCase(status)) {
                    status_textview.setText("Online");
                } else {
                    if (canShowLastSeen && !sessionManager.getLastSeenVisibleTo().equalsIgnoreCase("nobody")) {
                        String lastSeen = jsonObject.getString("DateTime");
                        if (!lastSeen.equals("0") && !lastSeen.equalsIgnoreCase("null")) {
                            setOnlineStatusText(lastSeen);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //------------------------------------------------------Load Online Status-------------------------------------------------

    private void loadStarredMessage(ReceviceMessageEvent event) {

        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            int errorState = objects.getInt("err");
            if (errorState == 0) {
                String fromId = objects.getString("from");
                if (fromId.equalsIgnoreCase(from)) {
                    String chat_id = objects.getString("doc_id");
                    String[] ids = chat_id.split("-");
                    String docId;
                    if (fromId.equalsIgnoreCase(ids[0])) {
                        docId = ids[0] + "-" + ids[1];
                    } else {
                        docId = ids[1] + "-" + ids[0];
                    }

                    if (chat_id.contains("-g-")) {
                        docId = docId + "-g";
                    }

                    String starred = objects.getString("status");
                    String msgId = docId + "-" + ids[2];


                    for (MessageItemChat items : mChatData) {
                        if (items != null && items.getMessageId().equalsIgnoreCase(msgId)) {
                            int index = mChatData.indexOf(items);
                            mChatData.get(index).setStarredStatus(starred);
                            mChatData.get(index).setSelected(false);
                            madapter.notifyDataSetChanged();
                            break;
                        }
                    }
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void loadDeleteMessage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            int errorState = objects.getInt("err");
            if (errorState == 0) {
                String fromId = objects.getString("from");

                if (fromId.equalsIgnoreCase(from)) {
                    String deleteStatus = objects.getString("status");
                    String chat_id = (String) objects.get("doc_id");
                    String[] ids = chat_id.split("-");

                    String docId;
                    String msgId;
                    if (chat_id.contains("-g-")) {
                        docId = fromId + "-" + ids[1] + "-g";
                        msgId = docId + "-" + ids[3];
                    } else {
                        if (fromId.equalsIgnoreCase(ids[0])) {
                            docId = ids[0] + "-" + ids[1];
                        } else {
                            docId = ids[1] + "-" + ids[0];
                        }
                        msgId = docId + "-" + ids[2];
                    }

                    //  String groupAndMsgId = ids[1] + "-g-" + ids[3];

                    if (deleteStatus.equalsIgnoreCase("1")) {
                        for (MessageItemChat items : mChatData) {
                            if (items != null && items.getMessageId().equalsIgnoreCase(msgId)) {
                                int index = mChatData.indexOf(items);
                                if (index > -1 && mChatData.get(index).isMediaPlaying()) {
                                    madapter.stopAudioOnMessageDelete(index);
                                }

                                madapter.notifyDataSetChanged();
                                break;
                            }
                        }

                    }
                }
            }
        } catch (Exception ex) {
            MyLog.e(TAG, "loadDeleteMessage: ", ex);
        }
    }


    //---------------------------------------------------Load Current Time Message------------------------------------------------

    private void loadMuteMessage(String data) {
        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");
            if (from.equalsIgnoreCase(mCurrentUserId)) {
                String convId = object.getString("convId");
                if (convId.equalsIgnoreCase(mConvId)) {
//                    onCreateOptionsMenu(chatMenu);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //--------------------------------------------------------Load Stared Message---------------------------------------------------

    private void loadReplyMessageDetails(String data) {
        try {
            JSONObject object = new JSONObject(data);
            String err = object.getString("err");
            JSONObject dataObj = object.getJSONObject("data");
            String from = dataObj.getString("from");
            String convId = dataObj.getString("convId");

            if (err.equals("0") && convId.equalsIgnoreCase(mConvId)) {

                String to = dataObj.getString("to");
                String requestMsgId = dataObj.getString("requestMsgId");
                String chatType = dataObj.getString("type");

                String docId = from + "-" + to;
                if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                    docId = docId + "-g";
                } else {
                    String secretType = dataObj.getString("secret_type");
                    if (secretType.equalsIgnoreCase("yes")) {
                        docId = docId + "-secret";
                    }
                }

                try {
                    for (int i = 0; i < mChatData.size(); i++) {
                        MessageItemChat msgItem = mChatData.get(i);

                        if (msgItem.getMessageId().equalsIgnoreCase(requestMsgId)) {
                            MessageItemChat modifiedItem = db.getParticularMessage(requestMsgId);
                            modifiedItem.setSelected(msgItem.isSelected());
                            mChatData.set(i, modifiedItem);
                            madapter.notifyDataSetChanged();
                            break;
                        }
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    //--------------------------------------------------------Load Deleted Messages---------------------------------------------

    private void deleteSingleMessage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            int errorState = objects.getInt("err");
            if (errorState == 0) {
                String fromId = objects.getString("from");
                String docId, msgId, type, recId, lastMsg_Status, convId;
//                String deleteStatus = objects.getString("status");
                String chat_id = (String) objects.get("doc_id");
                String[] ids = chat_id.split("-");
                type = objects.getString("type");
//                recId = objects.getString("recordId");
//                convId = objects.getString("convId");
                docId = ids[1] + "-" + ids[0];
                msgId = docId + "-" + ids[2];

                if (fromId.equalsIgnoreCase(from)) {
                    for (MessageItemChat items : mChatData) {
                        if (items != null && items.getMessageId().equalsIgnoreCase(chat_id)) {
                            int index = mChatData.indexOf(items);
                            if (index > -1 && mChatData.get(index).isMediaPlaying()) {
                                madapter.stopAudioOnMessageDelete(index);
                            }

                            mChatData.get(index).setMessageType(MessageFactory.DELETE_SELF + "");
                            mChatData.get(index).setIsSelf(true);
                            madapter.notifyDataSetChanged();
                            db.deleteSingleMessage(docId, items.getMessageId(), chatType, "self");
                            db.deleteChatListPage(docId, items.getMessageId(), chatType, "self");

                            break;
                        }
                    }
                }

                if (!fromId.equalsIgnoreCase(from) && fromId.equalsIgnoreCase(Chat_to)) {

                    for (MessageItemChat items : mChatData) {
                        if (items != null && items.getMessageId().equalsIgnoreCase(msgId)) {
                            int index = mChatData.indexOf(items);
                            if (index > -1 && mChatData.get(index).isMediaPlaying()) {
                                madapter.stopAudioOnMessageDelete(index);
                            }

                            mChatData.get(index).setMessageType(MessageFactory.DELETE_OTHER + "");
                            mChatData.get(index).setIsSelf(false);

                            madapter.notifyDataSetChanged();

                            db.deleteSingleMessage(docId, msgId, type, "other");
                            db.deleteChatListPage(docId, msgId, type, "other");

                            break;
                        }
                    }
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    //---------------------------------------------------------------Load Mute Messages------------------------------------------

    private void getSingleOffline(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            int is_everyone = objects.getInt("is_deleted_everyone");
            if (is_everyone == 1) {
                String fromId = objects.getString("from");
                if (!fromId.equalsIgnoreCase(mCurrentUserId)) {
                    String chat_id = objects.getString("docId");
                    String[] ids = chat_id.split("-");
                    String docId, msgId, chat_type, recId, lastMsg_Status, convId, deleteStatus;

                    chat_type = objects.getString("chat_type");
//                    recId = objects.getString("recordId");
//                    convId = objects.getString("convId");
//                    deleteStatus = objects.getString("is_deleted_everyone");

                    docId = ids[1] + "-" + ids[0];
                    msgId = docId + "-" + ids[2];

                    if (mChatData.size() > 0) {
                        for (int i = 0; i < mChatData.size(); i++) {
                            if (mChatData.get(i).getMessageId().equalsIgnoreCase(msgId)) {
                                if (i > -1 && mChatData.get(i).isMediaPlaying()) {
                                    madapter.stopAudioOnMessageDelete(i);
                                }

                                mChatData.get(i).setMessageType(MessageFactory.DELETE_OTHER + "");
                                mChatData.get(i).setIsSelf(false);

                                madapter.notifyDataSetChanged();

                                db.deleteSingleMessage(docId, msgId, chat_type, "other");
                                db.deleteChatListPage(docId, msgId, chat_type, "other");

                                break;
                            }
                        }
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //--------------------------------------------------------------Load Reply Message Details--------------------------

    private void updateProfileImage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            String from = objects.getString("from");
            String type = objects.getString("type");

            if (type.equalsIgnoreCase("single") && to.equalsIgnoreCase(from)) {
                String path = objects.getString("file") + "?id=" + Calendar.getInstance().getTimeInMillis();
                profilepicupdation();
            }
        } catch (Exception e) {
            MyLog.e(TAG, "updateProfileImage: ", e);
        }
    }

    //-------------------------------------------Delete Single Message---------------------------------------------------------

    private void loadMessageStatusupdate(ReceviceMessageEvent event) {
        Object[] object = event.getObjectsArray();

        try {
            JSONObject jsonObject = new JSONObject(object[0].toString());
            String from = jsonObject.getString("from");
            String to = jsonObject.getString("to");
            String msgIds = jsonObject.getString("msgIds");
            String doc_id = jsonObject.getString("doc_id");
            String status = jsonObject.getString("status");
            String secretType = jsonObject.getString("secret_type");
            if (from.equalsIgnoreCase(mReceiverId) && secretType.equalsIgnoreCase("no")) {

                if (sessionManager.canSendReadReceipt() || !(status.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ))) {

                    for (int i = 0; i < mChatData.size(); i++) {
                        MessageItemChat items = mChatData.get(i);
                        if (items.isBlockedMsg())
                            continue;
                        if (items != null && items.getMessageId().equalsIgnoreCase(doc_id)) {
                            items.setDeliveryStatus("" + status);
                            mChatData.get(i).setDeliveryStatus(status);
                            break;
                        } else if (status.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_DELIVERED) && mChatData.get(i).isSelf()) {
                            if (mChatData.get(i).getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_SENT)) {
                                mChatData.get(i).setDeliveryStatus(status);
                            }
                        } else if (mChatData.get(i).isSelf() && status.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ) &&
                                (mChatData.get(i).getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_SENT)
                                        || (mChatData.get(i).getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_DELIVERED)))) {
                            mChatData.get(i).setDeliveryStatus(status);
                        }
                    }
                }
                madapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    //--------------------------------------------------------Get Single Offline-----------------------------------------------

    private void blockunblockcontact(ReceviceMessageEvent event) {
        String toid = "", fromid = "";
        try {
            Object[] obj = event.getObjectsArray();
            JSONObject object = new JSONObject(obj[0].toString());

            String stat = object.getString("status");
            toid = object.getString("to");
            fromid = object.getString("from");

            if (mCurrentUserId.equalsIgnoreCase(fromid) && toid.equalsIgnoreCase(mReceiverId)) {
                setTopLayoutBlockText();
                if (stat.equalsIgnoreCase("1")) {
                    block_contact.setText("Unblock");
                    Toast.makeText(this, "Number is blocked", Toast.LENGTH_SHORT).show();
                } else {
                    block_contact.setText("Block");
                    Toast.makeText(this, "Number is Unblocked", Toast.LENGTH_SHORT).show();
                }
            } else if (mCurrentUserId.equalsIgnoreCase(toid) && fromid.equalsIgnoreCase(mReceiverId)) {
                getcontactname.configCircleProfilepic(user_profile_image, to, true, false, R.drawable.avatar_contact);

                if (stat.equalsIgnoreCase("1")) {
                    status_textview.setVisibility(View.GONE);

                } else {
                    status_textview.setVisibility(View.VISIBLE);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //----------------------------------------Update Profile Image---------------------------------------

    private void setTopLayoutBlockText() {

        if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
            tvBlock.setText("Unblock");
        } else {
            tvBlock.setText("Block");
        }
    }

    //---------------------------------------------------Load Message Status Updated--------------------------------------------

    private void getReceiverOnlineTimeStatus() {
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            object.put("to", to);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GET_CURRENT_TIME_STATUS);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //----------------------------------------------------------Block Unblock Contact---------------------------------------------------

    private void handleGroupMessage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            Object object = objects.get("type");
            if (objects.has("id")) {
                String msgId = objects.getString("id");
                if (msgId != null && !msgId.isEmpty()) {
                    if (messageIds.contains(msgId)) {
                        return;
                    }
                    messageIds.add(msgId);
                }

            }
            Integer type = 0;
            if (object instanceof String) {
                type = Integer.valueOf((String) objects.get("type"));
            } else if (object instanceof Integer) {
                type = Integer.valueOf((Integer) objects.get("type"));
            }

            switch (type) {

                case MessageFactory.delete_member_by_admin:
                    loadDeleteMemberMessage(objects);
                    break;

                default:
                    loadGroupMessage(objects);
                    break;
            }


            //-------------Delete Chat----------------
            if (isGroupChat) {
                int group_type, is_deleted;
                String new_msgId;
                if (objects.has("groupType")) {
                    group_type = objects.getInt("groupType");
                    if (group_type == 9) {
                        if (objects.has("is_deleted_everyone")) {
                            is_deleted = objects.getInt("is_deleted_everyone");
                            if (is_deleted == 1) {
                                String chat_id = (String) objects.get("toDocOId");
                                String[] ids = chat_id.split("-");
                                new_msgId = mCurrentUserId + "-" + ids[1] + "-g-" + ids[3];
                                String groupAndMsgId = ids[1] + "-g-" + ids[3];
                                try {
                                    String fromId = objects.getString("from");

                                    if (fromId.equalsIgnoreCase(from)) {
                                        if (mChatData.size() > 0) {
                                            for (int i = 0; i < mChatData.size(); i++) {
                                                if (mChatData.get(i).getMessageId().contains(chat_id)) {
                                                    if (i > -1 && mChatData.get(i).isMediaPlaying()) {
                                                        madapter.stopAudioOnMessageDelete(i);
                                                    }

                                                    mChatData.get(i).setMessageType(MessageFactory.DELETE_OTHER + "");
                                                    mChatData.get(i).setIsSelf(false);


                                                    madapter.notifyDataSetChanged();
                                                    db.deleteSingleMessage(groupAndMsgId, chat_id, chatType, "self");
                                                    db.deleteChatListPage(groupAndMsgId, chat_id, chatType, "self");

                                                    break;
                                                }
                                            }
                                        }
                                    }


                                    if (!fromId.equalsIgnoreCase(mCurrentUserId)) {
                                        if (mChatData.size() > 0) {
                                            for (int i = 0; i < mChatData.size(); i++) {
                                                if (mChatData.get(i).getMessageId().contains(groupAndMsgId)) {
                                                    if (i > -1 && mChatData.get(i).isMediaPlaying()) {
                                                        madapter.stopAudioOnMessageDelete(i);
                                                    }

                                                    mChatData.get(i).setMessageType(MessageFactory.DELETE_OTHER + "");
                                                    mChatData.get(i).setIsSelf(false);

                                                    madapter.notifyDataSetChanged();

                                                    db.deleteSingleMessage(groupAndMsgId, new_msgId, "group", "other");
                                                    db.deleteChatListPage(groupAndMsgId, new_msgId, "group", "other");
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void loadGroupMessage(JSONObject objects) {

        String msgId = "";
        try {
            String from = objects.getString("from");
            String groupId = objects.getString("groupId");

            int msgIndex = -1;

            if (Integer.parseInt(objects.optString("type")) == 23) {
                return;
            }
            if (from.equalsIgnoreCase(mCurrentUserId)) {
                if (mGroupId.equalsIgnoreCase(groupId)) {
                    msgId = objects.getString("toDocId");

                    for (int i = 0; i < mChatData.size(); i++) {
                        if (mChatData.get(i).getMessageId().equalsIgnoreCase(msgId)) {
                            msgIndex = i;
                            break;
                        }
                    }

                    if (msgIndex > -1) {
                        groupMessageRes(objects, msgIndex);
                    } else {
                        IncomingMessage incomingMessage = new IncomingMessage(ChatPageActivity.this);
                        MessageItemChat msgItem = incomingMessage.loadGroupMessageFromWeb(objects);
                        if (msgItem != null) {
                            mChatData.add(msgItem);
                            notifyDatasetChange();
                        }
                    }
                }
            } else {
                if (groupId.equalsIgnoreCase(mGroupId)) {
                    IncomingMessage incomingMessage = new IncomingMessage(ChatPageActivity.this);
                    MessageItemChat msgItem = incomingMessage.loadGroupMessage(objects);
                    String senderName = getcontactname.getSendername(msgItem.getReceiverID(), msgItem.getSenderName());
                    msgItem.setSenderName(senderName);

                    msgId = objects.getString("toDocId");

                    if (msgItem != null) {

                        if (!from.equalsIgnoreCase(mCurrentUserId)) {
                            if (lastvisibleitempostion == mChatData.size() - 1) {

                                if (!msgId.equalsIgnoreCase(Group_Message_id)) {

                                    Group_Message_id = msgId;
                                    mChatData.add(msgItem);
                                    notifyDatasetChange();
                                }


                            } else {

                                if (!msgId.equalsIgnoreCase(Group_Message_id)) {

                                    unreadmsgcount++;
                                    unreadmessage();
                                    mChatData.add(msgItem);
                                    madapter.notifyDataSetChanged();

                                }


                            }
                        } else {

                            notifyDatasetChange();
                        }
                    }

                    String id = objects.getString("id");
                    String uniqueID = mCurrentUserId + "-" + to + "-g";

                    if (!from.equalsIgnoreCase(mCurrentUserId)) {
                        sendGroupAckToServer(mCurrentUserId, to, id, MessageFactory.GROUP_MSG_READ_ACK);
                    }

                    if (isChatPage) {
                        changeBadgeCount(uniqueID);
                    }

                    sendChatViewedStatus();

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //-----------------------------------------Get Receiver Online Time Status------------------------------

    private void sendGroupAckToServer(String from, String groupId, String id, String status) {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_GROUP);
        try {
            JSONObject groupAckObj = new JSONObject();
            groupAckObj.put("groupType", SocketManager.ACTION_ACK_GROUP_MESSAGE);
            groupAckObj.put("from", from);
            groupAckObj.put("groupId", groupId);
            groupAckObj.put("status", status);
            groupAckObj.put("msgId", id);
            messageEvent.setMessageObject(groupAckObj);
            EventBus.getDefault().post(messageEvent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //-----------------------------------------------------------------GROUP MESSAGE DETAILS------------------------------------------

    private void groupMessageRes(JSONObject jsonObject, int msgIndex) {

        try {
            String deliver = jsonObject.getString("deliver");
            String recordId = jsonObject.getString("recordId");

            mChatData.get(msgIndex).setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);
            mChatData.get(msgIndex).setDeliveryStatus(deliver);
            mChatData.get(msgIndex).setRecordId(recordId);
            madapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//----------------------------------Load Group Message---------------------------------------------

    public void loadDeleteMemberMessage(JSONObject object) {
        try {
            String err = object.getString("err");

            if (err.equalsIgnoreCase("0")) {
                String from = object.getString("from");
                String groupId = object.getString("groupId");

                if (groupId.equalsIgnoreCase(mGroupId)) {

                    String msgId = object.getString("id");

                    String timeStamp;
                    if (object.has("timeStamp")) {
                        timeStamp = object.getString("timeStamp");
                    } else {
                        timeStamp = object.getString("timestamp");
                    }

                    String removeId;
                    if (object.has("removeId")) {
                        removeId = object.getString("removeId");
                    } else {
                        removeId = object.getString("createdTo");
                    }

                    GroupInfoPojo groupInfoPojo = groupInfoSession.getGroupInfo(docId);
                    String groupName = groupInfoPojo.getGroupName();

                    GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                    MessageItemChat item = message.createMessageItem(MessageFactory.delete_member_by_admin, false, null, MessageFactory.DELIVERY_STATUS_READ,
                            groupId, groupName, from, removeId);
                    item.setMessageId(docId.concat("-").concat(msgId));
                    item.setTS(timeStamp);


                    boolean fromUserContact = contactDB_sqlite.isUserAvailableInDB(from);
                    boolean removedContact = contactDB_sqlite.isUserAvailableInDB(removeId);
//

                    if (removeId.equalsIgnoreCase(mCurrentUserId)) {
                        enableGroupChat = false;
                        if (fromUserContact) {
                            mChatData.add(item);
                            madapter.notifyDataSetChanged();
                            relativeLayout.setVisibility(View.GONE);
                            group_left_layout.setVisibility(View.VISIBLE);
                        }
                    } else if (from.equalsIgnoreCase(mCurrentUserId)) {
                        if (removedContact) {
                            mChatData.add(item);
                            madapter.notifyDataSetChanged();
                        }
                    } else {
                        if (fromUserContact && removedContact) {
                            mChatData.add(item);
                            madapter.notifyDataSetChanged();
                        }
                    }

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void updateGroupMsgStatus(JSONObject objects) {
        try {
            //   Log.e("updateGroupMsgStatus ", "objects " + objects);

            String err = objects.getString("err");
            if (err.equalsIgnoreCase("0")) {
                String from = objects.getString("from");
                String groupId = objects.getString("groupId");
                String msgId = objects.getString("msgId");
                String deliverStatus = objects.getString("status");
//                String recordId = objects.getString("recordId");
                String docId = sessionManager.getCurrentUserID().concat("-").concat(groupId).concat("-g");

                String resMsgId = docId.concat("-").concat(msgId);

                for (int i = 0; i < mChatData.size(); i++) {
                    MessageItemChat dbItem = mChatData.get(i);

                    if (dbItem != null && dbItem.getMessageId().equalsIgnoreCase(resMsgId)) {
                        //   Log.e("updateGroupMsgStatus equalsIgnoreCase", "resMsgId " + resMsgId + "dbItem.getMessageId()." + dbItem.getMessageId());
                        dbItem.setDeliveryStatus("" + deliverStatus);//test it here
                        //   sendAllAcksToServer(mChatData);
                        //  sendGroupACK(groupId, msgId, uniqueCurrentID);
                        //   sendGroupAckToServer(mCurrentUserId, to, msgId, MessageFactory.GROUP_MSG_READ_ACK);

                        //     loadFromDB();

                        mChatData.get(i).setDeliveryStatus(deliverStatus);

                        if (deliverStatus.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_DELIVERED)) {
                            //       loadFromDB();
                        }
                        //Check if the status is 2

                        break;
                    } else if (deliverStatus.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_DELIVERED) && mChatData.get(i).isSelf()) {

                        // Log.e("updateGroupMsgStatus equals", "DELIVERY_STATUS_DELIVERED " + deliverStatus + " mChatData.get(i).isSelf()" + mChatData.get(i).isSelf());
                        if (mChatData.get(i).getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_SENT)) {
                            mChatData.get(i).setDeliveryStatus(deliverStatus);
                        }
                    } else if (deliverStatus.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ) && mChatData.get(i).isSelf() &&
                            !mChatData.get(i).getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_READ)) {
                        //   Log.e("updateGroupMsgStatus equals", "DELIVERY_STATUS_READ " + deliverStatus + " mChatData.get(i).isSelf()" + mChatData.get(i).isSelf());

                        mChatData.get(i).setDeliveryStatus(deliverStatus);
                    }
                }
                // sendAllAcksToServer(mChatData);

                //   initAdapter();
                //   madapter.updateInfo(mChatData);
                //notifyDataSetChanged();
                madapter.notifyDataSetChanged();


                // madapter.notifyDataSetChanged();

                //  loadFromDB();
            }

        } catch (JSONException e) {
            madapter.updateInfo(mChatData);//notifyDataSetChanged();

            e.printStackTrace();
        }
    }


    public void loadChangeGroupNameMessage(JSONObject object) {
        try {
            String from = object.getString("from");
            String id = object.getString("id");
            String msg = object.getString("message");
            String groupId = object.getString("groupId");
            String timeStamp = object.getString("timeStamp");
            String groupNewName = object.getString("groupName");
            String groupPrevName = object.getString("prev_name");

            String docId = sessionManager.getCurrentUserID().concat("-").concat(groupId).concat("-g");
            GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
            MessageItemChat item = message.createMessageItem(MessageFactory.change_group_name, false, null,
                    MessageFactory.DELIVERY_STATUS_READ, groupId, groupNewName, from, null);
            item.setPrevGroupName(groupPrevName);
            item.setMessageId(docId.concat("-").concat(id));
            item.setTS(timeStamp);
            if (from.equalsIgnoreCase(mCurrentUserId)) {
                mChatData.add(item);
                madapter.notifyDataSetChanged();
            } else {


                if (contactDB_sqlite.isUserAvailableInDB(from)) {
                    mChatData.add(item);
                    madapter.notifyDataSetChanged();
                }
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

//---------------------------------------------Load Delete Members------------------------------------

    public void loadExitMessage(JSONObject object) {
        try {
            String from = object.getString("from");
            String groupId = object.getString("groupId");
            String timeStamp = object.getString("timeStamp");
            String id = object.getString("id");

            if (groupId.equalsIgnoreCase(mGroupId)) {

                String docId = sessionManager.getCurrentUserID().concat("-").concat(groupId).concat("-g");
                GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);
                String groupName = infoPojo.getGroupName();

                GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                MessageItemChat item = message.createMessageItem(MessageFactory.exit_group, false, null,
                        MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, null);

                item.setMessageId(docId.concat("-").concat(id));
                item.setTS(timeStamp);
                if (from.equalsIgnoreCase(mCurrentUserId)) {
                    mChatData.add(item);
                    madapter.notifyDataSetChanged();
                } else {


                    if (contactDB_sqlite.isUserAvailableInDB(from)) {
                        mChatData.add(item);
                        madapter.notifyDataSetChanged();
                    }
                }
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    //--------------------------------------------Update Group Message Status-------------------------------------------------

    private void performGroupChangeDp(JSONObject object) {
        try {
            String err = object.getString("err");

            if (err.equalsIgnoreCase("0")) {
                String from = object.getString("from");
                String groupId = object.getString("groupId");
                String avatar = object.getString("avatar");
                String groupName = object.getString("groupName");

                String timeStamp;
                if (object.has("timeStamp")) {
                    timeStamp = object.getString("timeStamp");
                } else {
                    timeStamp = object.getString("timestamp");
                }

                if (groupId.equalsIgnoreCase(to)) {
                    receiverAvatar = avatar;
/*                    Glide.with(ChatViewActivity.this).load(Constants.SOCKET_IP.concat(receiverAvatar))

                            .error(R.mipmap.group_chat_attachment_profile_icon).into(ivProfilePic);*/

                    if (receiverAvatar != null && !receiverAvatar.isEmpty()) {
                        AppUtils.loadImage(ChatPageActivity.this, AppUtils.getValidGroupPath(receiverAvatar),
                                user_profile_image, 100, R.drawable.avatar_group);
                    } else {
                        user_profile_image.setImageResource(R.drawable.avatar_group);
                    }

                    GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                    MessageItemChat item = message.createMessageItem(MessageFactory.change_group_icon, false, null,
                            MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, null);
                    item.setAvatarImageUrl(avatar);
                    item.setTS(timeStamp);

                    String id;
                    if (object.has("id")) {
                        id = object.getString("id");
                    } else {
                        id = Calendar.getInstance().getTimeInMillis() + "";
                    }
                    item.setMessageId(docId.concat("-").concat(id));

                    if (!from.equalsIgnoreCase(mCurrentUserId)) {


                        if (contactDB_sqlite.isUserAvailableInDB(from)) {
                            mChatData.add(item);
                            madapter.notifyDataSetChanged();
                        }
                    } else {
                        mChatData.add(item);
                        madapter.notifyDataSetChanged();
                    }
                }

            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

//--------------------------------------------Load Change Group Name---------------------------------------------------

    private void loadAddMemberMessage(JSONObject object) {
        try {
            String groupId = object.getString("groupId");
            if (groupId.equalsIgnoreCase(mGroupId)) {
                String msgId = object.getString("id");
                String timeStamp = object.getString("timeStamp");
                String from = object.getString("from");
                String groupName = "";
                if (object.has("groupName")) {
                    groupName = object.getString("groupName");
                }
                JSONObject newUserObj = object.getJSONObject("newUser");
                String newUserId = newUserObj.getString("_id");

                String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");
                GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);
                if (infoPojo != null) {
                    groupName = infoPojo.getGroupName();
                }

                GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                MessageItemChat item = message.createMessageItem(MessageFactory.add_group_member, false, null,
                        MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, newUserId);
                item.setMessageId(docId.concat("-").concat(msgId));
                item.setTS(timeStamp);

                boolean fromUserContact = contactDB_sqlite.isUserAvailableInDB(from);
                boolean newUserContact = contactDB_sqlite.isUserAvailableInDB(newUserId);

                if (from.equalsIgnoreCase(mCurrentUserId)) {
                    if (newUserContact) {
                        mChatData.add(item);
                        madapter.notifyDataSetChanged();
                    }
                } else {
                    if (newUserId.equalsIgnoreCase(mCurrentUserId)) {
                        if (fromUserContact) {
                            mChatData.add(item);
                            madapter.notifyDataSetChanged();
                        }
                    } else {
                        if (fromUserContact && newUserContact) {
                            mChatData.add(item);
                            madapter.notifyDataSetChanged();
                        }
                    }
                }
                //Check visiblity if send message is hidden
                if (RelativeLayout_group_delete.getVisibility() == View.GONE) {

                    rlSend.setVisibility(View.VISIBLE);
                    RelativeLayout_group_delete.setVisibility(View.GONE);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    //--------------------------------------------Load Exit Message--------------------------------------------------

    private void loadMakeAdminMessage(JSONObject object) {
        try {
            String groupId = object.getString("groupId");

            if (groupId.equalsIgnoreCase(mGroupId)) {
                String msgId = object.getString("id");
                String timeStamp = object.getString("timeStamp");
//                    String toDocId = object.getString("toDocId");
                String from = object.getString("from");

                String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");
                GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);
                String groupName;
                if (object.has("groupName")) {
                    groupName = object.getString("groupName");
                } else {
                    groupName = infoPojo.getGroupName();
                }
                String newAdminUserId = object.getString("adminuser");

                GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                MessageItemChat item = message.createMessageItem(MessageFactory.make_admin_member, false, null,
                        MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, newAdminUserId);
                item.setMessageId(docId.concat("-").concat(msgId));
                item.setTS(timeStamp);


                if (newAdminUserId.equalsIgnoreCase(mCurrentUserId)) {
                    mChatData.add(item);
                    madapter.notifyDataSetChanged();
                } else if (contactDB_sqlite.isUserAvailableInDB(newAdminUserId)) {
                    mChatData.add(item);
                    madapter.notifyDataSetChanged();
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //-------------------------------------------------Group Change DB----------------------------------------------------

    private void deleteGroupMessage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());

            String fromId = objects.getString("from");
//            String deleteStatus = objects.getString("status");
            String chat_id = (String) objects.get("doc_id");
            String[] ids = chat_id.split("-");

            String docId, msgId, type, recId, convId;

            type = objects.getString("type");
//            recId = objects.getString("recordId");
//            convId = objects.getString("convId");
            docId = fromId + "-" + ids[1] + "-g";
            msgId = docId + "-" + ids[3];
            String groupAndMsgId = ids[1] + "-g-" + ids[3];


            if (fromId.equalsIgnoreCase(from)) {
                if (mChatData.size() > 0) {
                    for (int i = 0; i < mChatData.size(); i++) {
                        if (mChatData.get(i).getMessageId().contains(chat_id)) {
                            if (i > -1 && mChatData.get(i).isMediaPlaying()) {
                                madapter.stopAudioOnMessageDelete(i);
                            }

                            mChatData.get(i).setMessageType(MessageFactory.DELETE_SELF + "");
                            mChatData.get(i).setIsSelf(true);

                            String[] from_ids = chat_id.split("-");
                            String from_groupAndMsgId = from_ids[1] + "-g-" + from_ids[3];

                            madapter.notifyDataSetChanged();
                            db.deleteSingleMessage(from_groupAndMsgId, chat_id, chatType, "self");
                            db.deleteChatListPage(from_groupAndMsgId, chat_id, chatType, "self");

                            break;
                        }
                    }
                }
            }


            if (!fromId.equalsIgnoreCase(from)) {
                if (mChatData.size() > 0) {
                    for (int i = 0; i < mChatData.size(); i++) {
                        if (mChatData.get(i).getMessageId().contains(groupAndMsgId)) {
                            if (i > -1 && mChatData.get(i).isMediaPlaying()) {
                                madapter.stopAudioOnMessageDelete(i);
                            }

                            mChatData.get(i).setMessageType(MessageFactory.DELETE_OTHER + "");
                            mChatData.get(i).setIsSelf(false);

                            madapter.notifyDataSetChanged();

                            db.deleteSingleMessage(groupAndMsgId, msgId, type, "other");
                            db.deleteChatListPage(groupAndMsgId, msgId, type, "other");

                            break;
                        }
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//---------------------------------------------Add Members Details in Group Message---------------------------------

    private void getGroupOffline(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            String status = objects.getString("err");
            String fromID, toDocId, type;
            if (status.equalsIgnoreCase("0")) {
                int groupType = objects.getInt("groupType");
                if (groupType == 20) {
                    int is_deleted_everyone = objects.getInt("is_deleted_everyone");
                    if (is_deleted_everyone == 1) {
                        fromID = objects.getString("from");
                        toDocId = objects.getString("toDocId");
                        type = objects.getString("groupName");
                        String[] ids = toDocId.split("-");


                        String groupAndMsgId = ids[1] + "-g-" + ids[3];


                        if (!fromID.equalsIgnoreCase(from)) {
                            if (mChatData.size() > 0) {
                                for (int i = 0; i < mChatData.size(); i++) {
                                    if (mChatData.get(i).getMessageId().contains(groupAndMsgId)) {
                                        if (i > -1 && mChatData.get(i).isMediaPlaying()) {
                                            madapter.stopAudioOnMessageDelete(i);
                                        }

                                        mChatData.get(i).setMessageType(MessageFactory.DELETE_OTHER + "");
                                        mChatData.get(i).setIsSelf(false);

                                        madapter.notifyDataSetChanged();

                                        db.deleteSingleMessage(groupAndMsgId, toDocId, type, "other");
                                        db.deleteChatListPage(groupAndMsgId, toDocId, type, "other");

                                        break;
                                    }
                                }
                            }
                        }

                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    //---------------------------------------------------Admin Message ---------------------------------------------------

    private void loadGroupDetails(ReceviceMessageEvent event) {
        try {
            Object[] array = event.getObjectsArray();
            JSONObject objects = new JSONObject(array[0].toString());

            String groupId = objects.getString("_id");

            if (groupId.equalsIgnoreCase(mGroupId)) {

                allMembersList.clear();
                savedMembersList.clear();
                unsavedMembersList.clear();
                JSONArray arrMembers = objects.getJSONArray("GroupUsers");
                membersCount = arrMembers.length();
                for (int i = 0; i < arrMembers.length(); i++) {
                    JSONObject userObj = arrMembers.getJSONObject(i);
                    String userId = userObj.getString("id");
                    String active = userObj.getString("active");
                    String isDeleted = userObj.getString("isDeleted");
                    String msisdn = userObj.getString("msisdn");
                    String phNumber = userObj.getString("PhNumber");
                    String name = userObj.getString("ContactName");
                    String status = userObj.getString("Status");
                    String userDp = userObj.getString("avatar");
                    String adminUser = userObj.getString("isAdmin");
                    String contactmsisdn = userObj.getString("ContactName");
                    String isExitsContact = userObj.getString("isExitsContact");
                    try {
                        if (SmarttyRegularExp.isEncodedBase64String(status)) {
                            byte[] arrStatus = Base64.decode(status, Base64.DEFAULT);
                            status = new String(arrStatus, StandardCharsets.UTF_8);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        status = "";
                    }


                    String contactName = getcontactname.getSendername(userId, msisdn);

                    if (mCurrentUserId.equalsIgnoreCase(userId)) {
                        contactName = "You";
                        status = SessionManager.getInstance(this).getcurrentUserstatus();
                    }

                    GroupMembersPojo data = new GroupMembersPojo();
                    data.setUserId(userId);
                    data.setActive(active);
                    data.setIsDeleted(isDeleted);
                    data.setMsisdn(msisdn);
                    data.setPhNumber(phNumber);
                    data.setName(name);
                    data.setStatus(status);
                    data.setUserDp(userDp);
                    data.setIsAdminUser(adminUser);
                    data.setContactName(contactName);

                    if (userId.equalsIgnoreCase(mCurrentUserId)) {
                        mCurrentUserData = data;
                    } else {
                        if (msisdn.equalsIgnoreCase(contactName)) {
                            unsavedMembersList.add(data);
                        } else {
                            savedMembersList.add(data);
                        }
                    }

                }
                Collections.sort(savedMembersList, Getcontactname.groupMemberAsc);
                Collections.sort(unsavedMembersList, Getcontactname.groupMemberAsc);
                allMembersList.addAll(savedMembersList);
                if (membersCount > 0)
                    allMembersList.addAll(unsavedMembersList);

//                if (mCurrentUserData != null) {
//                    allMembersList.add(mCurrentUserData);
//                }

                atAdapter.notifyDataSetChanged();

            }
        } catch (JSONException e) {

        }
    }

    private void sendAllAcksToServer(ArrayList<MessageItemChat> items) {
        Log.d(TAG, "sendAllAcksToServer: ");
        new SendAckAsync(items).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        //sendAck(items);

    }

    //---------------------------------------------------Delete Group Message-------------------------------------------

    private void sendAck(ArrayList<MessageItemChat> items) {
        //check blocked or not
        // For sent & receive acks
        Log.e(TAG, "sendAcktest: ");
        MessageItemChat lastDeliveredItem = null;
        MessageItemChat lastSentItem = null;
        JSONArray arrSentMsgRecordIds = new JSONArray();

        String chatType = MessageFactory.CHAT_TYPE_SINGLE;
        if (isGroupChat) {
            chatType = MessageFactory.CHAT_TYPE_GROUP;
        }

        for (MessageItemChat msgItem : items) {
            String msgStatus = msgItem.getDeliveryStatus();
            lastDeliveredItem = null;
            if (msgItem.getConvId() != null && !msgItem.getConvId().equals("")) {
                mConvId = msgItem.getConvId();
            }

            if (msgStatus != null) {
                if (!msgItem.isSelf() && !msgStatus.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ)) {
                    if (isGroupChat && msgItem.getGroupMsgFrom() != null) {
                        lastDeliveredItem = msgItem;
                    } else if (!isGroupChat) {
                        lastDeliveredItem = msgItem;
                    }
                } else if (msgItem.isSelf() && (msgStatus.equals(MessageFactory.DELIVERY_STATUS_SENT)
                        || msgStatus.equals(MessageFactory.DELIVERY_STATUS_DELIVERED))) {
                    if (msgItem.getRecordId() != null && arrSentMsgRecordIds.length() < 100) {
                        arrSentMsgRecordIds.put(msgItem.getRecordId());
                    }
                }

                if (msgItem.isSelf() && !msgStatus.equals(MessageFactory.DELIVERY_STATUS_NOT_SENT)) {
                    lastSentItem = msgItem;
                }

                // For getting reply message details from server
                if (!msgItem.isSelf() && msgItem.getReplyId() != null && !msgItem.getReplyId().equals("")
                        && (msgItem.getReplyType() == null || msgItem.getReplyType().equals(""))) {
                    getReplyMessageDetails(mReceiverId, msgItem.getReplyId(), chatType, "no", msgItem.getMessageId());
                }
            }

            if (lastDeliveredItem != null) {
                if (sessionManager.canSendReadReceipt()) {
                    if (isGroupChat) {
                        String msgId = lastDeliveredItem.getMessageId().split("-")[3];
                        sendGroupAckToServer(mCurrentUserId, mGroupId, msgId, MessageFactory.GROUP_MSG_READ_ACK);
                    } else {
                        String msgId = lastDeliveredItem.getMessageId().split("-")[2];
                        String ackDocId = to.concat("-").concat(mCurrentUserId).concat("-").concat(msgId);
                        if (SocketManager.getInstance().isConnected()) {
                            sendAckToServer(to, ackDocId, msgId, lastDeliveredItem.getConvId());
                        }
                        resendAck(to, ackDocId, msgId, lastDeliveredItem.getConvId());
                    }

                    sendViewedStatusToWeb(lastDeliveredItem.getConvId());
                }
            } else if (lastSentItem != null && lastSentItem.getConvId() != null) {
                sendViewedStatusToWeb(lastSentItem.getConvId());

            }
        }

        if (arrSentMsgRecordIds.length() > 0) {
            getMessageInfo(arrSentMsgRecordIds);
        }

        if (!isGroupChat) {
            String docId = mCurrentUserId + "-" + mReceiverId;
            if (userInfoSession.hasChatConvId(docId)) {
                mConvId = userInfoSession.getChatConvId(docId);
            }
        }
    }

    //for socket connection delay
    private void resendAck(final String to, final String ackDocId, final String msgId, final String convid) {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                sendAckToServer(to, ackDocId, msgId, convid);
            }
        }, 4000);
    }

    private void getMessageInfo(JSONArray arrSentMsgRecordIds) {

        try {
            JSONObject object = new JSONObject();
            object.put("recordId", arrSentMsgRecordIds);
            object.put("from", mCurrentUserId);
            if (isGroupChat) {
                object.put("type", MessageFactory.CHAT_TYPE_GROUP);
            } else {
                object.put("type", MessageFactory.CHAT_TYPE_SINGLE);
            }

            SendMessageEvent infoEvent = new SendMessageEvent();
            infoEvent.setEventName(SocketManager.EVENT_GET_MESSAGE_INFO);
            infoEvent.setMessageObject(object);
            // EventBus.getDefault().post(infoEvent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//---------------------------------------------------------------------Load Group Offline ------------------------------------

    public void getReplyMessageDetails(String toUserId, String recordId, String chatType,
                                       String secretType, String msgId) {
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            object.put("to", toUserId);
            object.put("recordId", recordId);
            object.put("requestMsgId", msgId);
            object.put("type", chatType);
            object.put("secret_type", secretType);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GET_MESSAGE_DETAILS);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    //----------------------------------------------------------Load Group Details------------------------------------------------------

    //-----------------------------------------WallPaper Set-------------------------------------
    public void wallpaperdisplay() {
        if (session.getgalleryPrefsName().equals("def")) {
            background.setImageResource(R.drawable.whatsapp_background);
        } else if (session.getgalleryPrefsName().equals("no")) {
            Bitmap bmp = Bitmap.createBitmap(128, 128, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawColor(Color.parseColor("#f0f0f0"));
            background.setImageBitmap(bmp);
        } else if (!session.getgalleryPrefsName().equals("")) {
            session.putgalleryPrefs(session.getgalleryPrefsName());
            String gallery_string = session.getgalleryPrefsName();
            Bitmap bitmap = BitmapFactory.decodeFile(gallery_string);
            background.setImageBitmap(bitmap);
        } else if (!session.getColor().equals("")) {
            try {
                Bitmap bmp = Bitmap.createBitmap(128, 128, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bmp);
                canvas.drawColor(Color.parseColor(session.getColor()));
                background.setImageBitmap(bmp);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Bitmap bmp = Bitmap.createBitmap(128, 128, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawColor(Color.parseColor("#f0f0f0"));
            background.setImageBitmap(bmp);
        }
    }


    //-----------------------------------------------Send Acknowledgement To server All Messages--------------------------------------

    private void sendGroupOffline() {
        SendMessageEvent groupMsgEvent = new SendMessageEvent();
        groupMsgEvent.setEventName(SocketManager.EVENT_GROUP);
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            object.put("groupType", SocketManager.ACTION_EVENT_GROUP_OFFLINE);
            groupMsgEvent.setMessageObject(object);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        EventBus.getDefault().post(groupMsgEvent);
    }

    private void sendSingleOffline() {
        SendMessageEvent groupMsgEvent = new SendMessageEvent();
        groupMsgEvent.setEventName(SocketManager.EVENT_SINGLE_OFFLINE_MSG);
        try {
            JSONObject object = new JSONObject();
            object.put("msg_to", mCurrentUserId);
            groupMsgEvent.setMessageObject(object);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        EventBus.getDefault().post(groupMsgEvent);
    }

    private void sendNormalOffline() {
        SendMessageEvent groupMsgEvent = new SendMessageEvent();
        groupMsgEvent.setEventName(SocketManager.EVENT_GET_MESSAGE);
        try {
            JSONObject object = new JSONObject();
            object.put("msg_to", mCurrentUserId);
            groupMsgEvent.setMessageObject(object);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        EventBus.getDefault().post(groupMsgEvent);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        try {

            MuteStatusPojo muteData = null;
            if (isGroupChat) {
                chatMenu.findItem(R.id.menuSecretChat).setVisible(false);
                muteData = contactDB_sqlite.getMuteStatus(mCurrentUserId, null, mGroupId, false);
            } else {
                chatMenu.findItem(R.id.menuSecretChat).setVisible(false);
                muteData = contactDB_sqlite.getMuteStatus(mCurrentUserId, mReceiverId, mConvId, false);
            }

            if (muteData != null && muteData.getMuteStatus().equals("1")) {
                chatMenu.findItem(R.id.menuMute).setTitle("Unmute");
            } else {
                chatMenu.findItem(R.id.menuMute).setTitle("Mute");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.onPrepareOptionsMenu(menu);
    }

    //----------------------------------------Group Offline-----------------------------------------------------------

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        chatMenu = menu;
        getMenuInflater().inflate(R.menu.activity_chat_view, menu);

        return super.onCreateOptionsMenu(chatMenu);
    }

    //------------------------------Single Offline--------------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.menuSearch:
                performMenuSearch();
                break;
            case R.id.menuSecretChat:
                startSecretChat();
                break;


            case R.id.menuMute:
                performMenuMute();
                break;

            case R.id.menuWallpaper:
                performMenuWallpaper();
                break;

            case R.id.menuBlock:
                performMenuBlock();
                break;

            case R.id.menuClearChat:
                performMenuClearChat();
                break;

           /* case R.id.menuEmailChat:
                performMenuEmailChat();
                break;*/

            case R.id.menuAddShortcut:
                addShortcutConfirmationDialog();

                break;

            case R.id.menuMore:
//                showMoreMenu();
                openMenu();
                break;

            case R.id.menuChatLock:
                /*Intent intent = new Intent(ChatViewActivity.this, EmailSettings.class);
                startActivity(intent);*/
                performChatlock();
                break;
        }

        return true;
    }

    private void performMenuSearch() {

        showSearchActions();
        showProgressDialog();
        ArrayList<MessageItemChat> allMessages = db.selectAllChatMessages(docId, chatType);
        Collections.sort(allMessages, msgComparator);
        mChatData.clear();
        mChatData.addAll(allMessages);
        hideProgressDialog();

        Search1.getBackground().clearColorFilter();
        Search1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (cs.length() > 0) {
                    madapter.getFilter().filter(cs);
                } else {
                    madapter.updateInfo(mChatData);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }
        });

        iBtnBack2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Search1.setText("");
                madapter.updateInfo(mChatData);
                showUnSelectedActions();

                InputMethodManager inputMethodManager =
                        (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                if (ChatPageActivity.this.getCurrentFocus() != null) {
                    inputMethodManager.hideSoftInputFromWindow(
                            ChatPageActivity.this.getCurrentFocus().getWindowToken(), 0);
                }

            }
        });
    }


    //----------------------------------------------------------------Menu Button Click---------------------------------------------

    private void showSearchActions() {
        // Make all selected items to unselect
        include.setVisibility(View.GONE);
        rlChatActions.setVisibility(View.VISIBLE);
        //    share.setVisibility(View.GONE);
        replymess.setVisibility(View.GONE);

        copychat.setVisibility(View.GONE);
        starred.setVisibility(View.GONE);
        info.setVisibility(View.GONE);
        delete.setVisibility(View.GONE);
        forward.setVisibility(View.GONE);
        longpressback.setVisibility(View.GONE);
        iBtnBack2.setVisibility(View.VISIBLE);
        Search1.setVisibility(View.VISIBLE);
    }

    private void startSecretChat() {


        Intent intent = new Intent(ChatPageActivity.this, SecretChatViewActivity.class);
        intent.putExtra("receiverUid", receiverUid);
        intent.putExtra("receiverName", receiverName);
        intent.putExtra("documentId", to);
        intent.putExtra("Username", mReceiverName);
        intent.putExtra("Image", receiverAvatar);
        intent.putExtra("msisdn", receiverMsisdn);
        intent.putExtra("type", 0);
        startActivity(intent);
        //overridePendingTransition(R.anim.right_in, R.anim.left_out);

    }

    private void performMenuMute() {
        if (ConnectivityInfo.isInternetConnected(ChatPageActivity.this)) {


            MuteStatusPojo muteData = null;
            if (isGroupChat) {
                muteData = contactDB_sqlite.getMuteStatus(mCurrentUserId, null, mGroupId, false);
            } else {
                muteData = contactDB_sqlite.getMuteStatus(mCurrentUserId, mReceiverId, mConvId, false);
            }

            if (muteData != null && muteData.getMuteStatus().equals("1")) {
                if (!isGroupChat) {
                    MuteUnmute.performUnMute(ChatPageActivity.this, EventBus.getDefault(), mReceiverId,
                            MessageFactory.CHAT_TYPE_SINGLE, "no");
                } else {
                    MuteUnmute.performUnMute(ChatPageActivity.this, EventBus.getDefault(), mReceiverId, MessageFactory.CHAT_TYPE_GROUP, "no");
                }
            } else {
                MuteUserPojo muteUserPojo = new MuteUserPojo();
                muteUserPojo.setReceiverId(mReceiverId);
                muteUserPojo.setSecretType("no");
                if (isGroupChat) {
                    muteUserPojo.setChatType(MessageFactory.CHAT_TYPE_GROUP);
                } else {
                    muteUserPojo.setChatType(MessageFactory.CHAT_TYPE_SINGLE);
                }

                ArrayList<MuteUserPojo> muteUserList = new ArrayList<>();
                muteUserList.add(muteUserPojo);

                Bundle putBundle = new Bundle();
                putBundle.putSerializable("MuteUserList", muteUserList);

                MuteAlertDialog dialog = new MuteAlertDialog();
                dialog.setArguments(putBundle);
                dialog.setCancelable(false);
                dialog.setMuteAlertCloseListener(ChatPageActivity.this);
                dialog.show(getSupportFragmentManager(), "Mute");
            }
        } else {
            Toast.makeText(ChatPageActivity.this, "Check Your Network Connection", Toast.LENGTH_SHORT).show();
        }
    }


    //-------------------------------------Menu Search-------------------------------

    @Override
    public void onMuteDialogClosed(boolean isMuted) {

    }

    private void performMenuWallpaper() {
        List<MultiTextDialogPojo> labelsList = new ArrayList<>();
        MultiTextDialogPojo label = new MultiTextDialogPojo();
        label.setImageResource(R.drawable.gallery_ic);
        label.setLabelText(getString(R.string.gallery));
        labelsList.add(label);

        label = new MultiTextDialogPojo();
        label.setImageResource(R.drawable.solidcolor_ic);
        label.setLabelText(getString(R.string.solid_color));
        labelsList.add(label);

        label = new MultiTextDialogPojo();
        label.setImageResource(R.drawable.default_ic);
        label.setLabelText(getString(R.string.Default_value));
        labelsList.add(label);

        label = new MultiTextDialogPojo();
        label.setImageResource(R.drawable.nowallpaper_ic);
        label.setLabelText(getString(R.string.no_wallpaper));
        labelsList.add(label);

        CustomMultiTextItemsDialog dialog = new CustomMultiTextItemsDialog();
        dialog.setTitleText(getString(R.string.wallpaper));
        dialog.setLabelsList(labelsList);
        dialog.setDialogItemClickListener(new CustomMultiTextItemsDialog.DialogItemClickListener() {
            @Override
            public void onDialogItemClick(int position) {
                switch (position) {

                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(photoPickerIntent, RESULT_WALLPAPER);
                        } else {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(intent, RESULT_WALLPAPER);
                        }
                        break;

                    case 1:
                        Intent intent = new Intent(context, WallpaperColor.class);
                        startActivity(intent);
                        break;
                    case 2:
                        String setbgdef = "def";
                        background.setImageResource(R.drawable.whatsapp_background);
                        session.putgalleryPrefs(setbgdef);
                        break;
                    case 3:
                        String setbg = "no";
                        Bitmap bmp = Bitmap.createBitmap(128, 128, Bitmap.Config.ARGB_8888);
                        Canvas canvas = new Canvas(bmp);
                        canvas.drawColor(Color.parseColor("#f0f0f0"));
                        background.setImageBitmap(bmp);
                        session.putgalleryPrefs(setbg);
                        break;

                }
            }
        });
        dialog.show(getSupportFragmentManager(), "Profile Pic");
    }

    private void sendScreenShotMsg() {
        String data = " took a screenshot!";

        SendMessageEvent messageEvent = new SendMessageEvent();
        TextMessage message = (TextMessage) MessageFactory.getMessage(MessageFactory.text, this);
        JSONObject msgObj = (JSONObject) message.getMessageObject(to, data, false);
        try {
            msgObj.put("type", MessageFactory.SCREEN_SHOT_TAKEN);
        } catch (Exception e) {
            MyLog.e(TAG, "sendScreenShotMsg: ", e);
        }
        messageEvent.setEventName(SocketManager.EVENT_MESSAGE);

        MessageItemChat item = message.createMessageItem(true, "You" + data, MessageFactory.DELIVERY_STATUS_NOT_SENT, mReceiverId, mReceiverName);
        messageEvent.setMessageObject(msgObj);


        item.setSenderMsisdn(receiverMsisdn);
        item.setSenderName(mReceiverName);
        item.setMessageType(MessageFactory.SCREEN_SHOT_TAKEN + "");
        db.updateChatMessage(item, chatType);
        if (!isAlreadyExist(item))
            mChatData.add(item);
        EventBus.getDefault().post(messageEvent);
        chat_list.postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyDatasetChange();

            }
        }, 100);

    }

    private void changeBadgeCount(String docId) {

        session.Removemark(to);
        if (mConvId != null && !mConvId.equals("")) {
            //ShortcutBadgeManager shortcutBadgeMgnr = new ShortcutBadgeManager(ChatPageActivity.this);
            sharedprf_video_uploadprogress.removeMessageCount(mConvId, msgid);
            int totalCount = sharedprf_video_uploadprogress.getTotalCount();
            // Badge working if supported devices
            if (totalCount > 0) {
                try {
                    ShortcutBadger.applyCount(context, totalCount);
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            }
        }
    }
    //------------------------------------------------------Mute click Menu-----------------------------------------------------

    @Override
    public boolean onKeyDown(int keycode, KeyEvent e) {

        switch (keycode) {
            case KeyEvent.KEYCODE_MENU:
                isMenuBtnClick = true;
                return super.onKeyDown(keycode, e);

            case KeyEvent.KEYCODE_BACK:

                if (selectedChatItems.size() > 0) {

                    for (int i = 0; i < mChatData.size(); i++) {

                        mChatData.get(i).setSelected(false);
                    }
                    madapter.notifyDataSetChanged();

                    include.setVisibility(View.VISIBLE);
                    rlChatActions.setVisibility(View.GONE);

                    selectedChatItems.clear();

                } else if (rlChatActions.getVisibility() == View.VISIBLE) {

                    Search1.setText("");
                    madapter.updateInfo(mChatData);
                    showUnSelectedActions();

                    InputMethodManager inputMethodManager =
                            (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    if (ChatPageActivity.this.getCurrentFocus() != null) {
                        inputMethodManager.hideSoftInputFromWindow(
                                ChatPageActivity.this.getCurrentFocus().getWindowToken(), 0);
                    }

                } else if (attachmentLayout.getVisibility() == View.VISIBLE) {

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)

                        showMenuBelowLollipop();
                    else
                        showMenu();

                } else {

                    if (ChatMessageAdapter.mPlayer != null) {

                        for (int i = 0; i < mChatData.size(); i++) {

                            mChatData.get(i).setIsMediaPlaying(false);
                            ChatMessageAdapter.mTimer.cancel();
                            ChatMessageAdapter.mPlayer.release();
                        }
                        madapter.notifyDataSetChanged();
                    }

                    onBackPressed();
                }

                return true;

        }
        return false;

    }

    public void VideoDownloadComplete(String Docid, String Msgid, String localpath) {


        for (int i = 0; i < mChatData.size(); i++) {
            if (Msgid.equalsIgnoreCase(mChatData.get(i).getMessageId())) {
                mChatData.get(i).setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_COMPLETED);
                db.updateMessageDownloadStatus(Docid, Msgid, MessageFactory.DOWNLOAD_STATUS_COMPLETED, localpath);
                madapter.notifyDataSetChanged();

                break;
            }
        }


    }

    @Override
    public void itemClick(int position) {
        itemClicked(position);
    }

    @Override
    public void screenShotTaken() {
        boolean isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
        if (isMassChat) {
            if (!chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                if (chatType.equalsIgnoreCase("single")) {
                    if (!getResources().getString(R.string.app_name).contains("Neo"))
                        sendScreenShotMsg();
                }
            }
        }
    }

    @Override
    public void downloadCompleted(String msgId, String path) {
        VideoDownloadComplete("", msgId, path);
    }

    @Override
    public void progress(int progress, String msgId) {
        for (int i = 0; i < mChatData.size(); i++) {
            if (msgId.equalsIgnoreCase(mChatData.get(i).getMessageId())) {
                mChatData.get(i).setUploadDownloadProgress(progress);
                madapter.notifyDataSetChanged();
                break;
            }
        }
    }

    @Override
    public void DownloadError(int progress, String msgId) {
        for (int i = 0; i < mChatData.size(); i++) {
            if (msgId.equalsIgnoreCase(mChatData.get(i).getMessageId())) {
                //  mChatData.get(i).setDownloadError(true);
                mChatData.get(i).setUploadDownloadProgress(0);

                madapter.notifyDataSetChanged();
                break;
            }
        }
    }

    private boolean isBlockedUser(String to) {
        try {
            ContactDB_Sqlite contactDB_sqlite = new ContactDB_Sqlite(this);
            boolean isBlockedInSecret = contactDB_sqlite.getBlockedStatus(to, true).equals("1");
            boolean isBlockedInNormal = contactDB_sqlite.getBlockedStatus(to, false).equals("1");
            if (isBlockedInNormal || isBlockedInSecret)
                return true;
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    private class LoadDBAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            loadDbAsync();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            initAdapter();
            getReceiverOnlineTimeStatus();
            //notifyDatasetChange();
            setEncryptionMsg();
            initDataBase();
        }
    }


    //------------------------------------------------------Default Back Press Function---------------------------------------------

    private class SendAckAsync extends AsyncTask<Void, Void, Void> {
        ArrayList<MessageItemChat> items;

        public SendAckAsync(ArrayList<MessageItemChat> items) {
            this.items = items;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            sendAck(items);
            return null;
        }
    }


    //----------------------------------------Video Download Complete Update------------------

    class Receiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equalsIgnoreCase("com.groupname.change")) {

                String data = intent.getStringExtra("object");
                JSONObject object = null;
                try {
                    object = new JSONObject(data);

                    String groupId = object.getString("groupId");
                    String id = object.getString("id");
                    String groupPrevName = object.getString("prev_name");
                    String groupNewName = object.getString("groupName");
                    String from = object.getString("from");
                    String timeStamp;
                    if (object.has("timeStamp")) {
                        timeStamp = object.getString("timeStamp");
                    } else {
                        timeStamp = object.getString("timestamp");
                    }

                    if (object.has("changed_name")) {
                        groupNewName = object.getString("changed_name");
                    }

                    String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");

                    GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                    MessageItemChat item = message.createMessageItem(MessageFactory.change_group_name, false, null,
                            MessageFactory.DELIVERY_STATUS_READ, groupId, groupNewName, from, null);
                    item.setPrevGroupName(groupPrevName);
                    item.setMessageId(docId.concat("-").concat(id));
                    item.setTS(timeStamp);
                    mChatData.add(item);
                    notifyDatasetChange();

                    user_name.setText(groupNewName);
                    SharedPreference.getInstance().save(context, "userName", groupNewName);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (intent.getAction().equalsIgnoreCase("com.groupprofile.update")) {

                String data = intent.getStringExtra("object");
                String image_path = intent.getStringExtra("image");
                JSONObject object = null;
                try {
                    object = new JSONObject(data);

                    String from = object.getString("from");
                    String groupId = object.getString("groupId");
                    String groupDp;
                    if (object.has("avatar")) {
                        groupDp = object.getString("avatar");
                    } else {
                        groupDp = object.getString("thumbnail");
                    }
                    String groupName = object.getString("groupName");

                    String timeStamp;
                    if (object.has("timeStamp")) {
                        timeStamp = object.getString("timeStamp");
                    } else {
                        timeStamp = object.getString("timestamp");
                    }

                    String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");

                    GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                    MessageItemChat item = message.createMessageItem(MessageFactory.change_group_icon, false, null,
                            MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, null);
                    item.setAvatarImageUrl(groupDp);
                    item.setTS(timeStamp);
                    String id;
                    if (object.has("id")) {
                        id = object.getString("id");
                    } else {
                        id = Calendar.getInstance().getTimeInMillis() + "";
                    }
                    item.setMessageId(docId.concat("-").concat(id));

                    mChatData.add(item);
                    notifyDatasetChange();

                    if (user_profile_image != null) {
                        receiverAvatar = AppUtils.getValidProfilePath(image_path);
                        AppUtils.loadImage(ChatPageActivity.this, AppUtils.getValidProfilePath(image_path), user_profile_image, 100, R.drawable.avatar_contact);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (intent.getAction().equalsIgnoreCase("com.group.delete.members")) {

                String data = intent.getStringExtra("object");

                try {
                    JSONObject object = new JSONObject(data);
                    loadDeleteMemberMessage(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (intent.getAction().equalsIgnoreCase("com.group.makeadmin")) {

                String data = intent.getStringExtra("object");

                try {
                    JSONObject object = new JSONObject(data);
                    loadMakeAdminMessage(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // DownloadImage AsyncTask
    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {

        String imageURL;

        public DownloadImage(String receiverAvatar) {
            imageURL = receiverAvatar;
        }

        @Override
        protected Bitmap doInBackground(String... URL) {

            Bitmap bitmap = null;
            try {
                // Download Image from URL
                InputStream input = new java.net.URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            ShortcutBadgeManager.addChatShortcut(ChatPageActivity.this, isGroupChat, to, user_name.getText().toString(),
                    receiverAvatar, receiverMsisdn, result);
        }
    }

    private class loadFileUploaded extends AsyncTask<String, Void, String> {
        JSONObject jsonObject_bg;

        public loadFileUploaded(JSONObject object) {
            jsonObject_bg = object;
        }

        @Override
        protected String doInBackground(String... params) {
            loadFileUploaded(jsonObject_bg);
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            //UI thread
        }
    }

    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            if (arg1.hasExtra("message")) {
                checkcallstatus();
                //  System.out.println(arg1.getStringExtra("message"));
            }
        }
    }
}
