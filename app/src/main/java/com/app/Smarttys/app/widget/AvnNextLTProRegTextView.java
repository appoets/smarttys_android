package com.app.Smarttys.app.widget;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by CAS60 on 2/17/2017.
 */
public class AvnNextLTProRegTextView extends CustomEmojiTextView {

    public AvnNextLTProRegTextView(Context context) {
        super(context);
        init();
    }

    public AvnNextLTProRegTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AvnNextLTProRegTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        /*Typeface face = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        setTypeface(face);*/
        //setUseSystemDefault(false);
    }
}
