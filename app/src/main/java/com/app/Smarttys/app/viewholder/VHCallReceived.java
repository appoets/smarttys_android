package com.app.Smarttys.app.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;


public class VHCallReceived extends RecyclerView.ViewHolder {

    public TextView tvCallLbl, tvDateLbl;


    public VHCallReceived(View itemView) {
        super(itemView);

        tvCallLbl = itemView.findViewById(R.id.tvCallLbl);
        tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
    }
}
