package com.app.Smarttys.app.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.ChatPageActivity;
import com.app.Smarttys.app.activity.SecretChatViewActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.model.GroupMembersPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.service.Constants;
import com.bumptech.glide.BitmapTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by CAS56 on 3/30/2017.
 */
public class Getcontactname {

    private static final String TAG = Getcontactname.class.getSimpleName();
    public static Comparator<SmarttyContactModel> nameAscComparator = new Comparator<SmarttyContactModel>() {

        @Override
        public int compare(SmarttyContactModel lhs, SmarttyContactModel rhs) {
            try {
                if (lhs != null && rhs != null) {
                    if (lhs.getFirstName() != null && rhs.getFirstName() != null) {
                        String name1 = lhs.getFirstName().toUpperCase();
                        String name2 = rhs.getFirstName().toUpperCase();

                        //ascending order
                        return name1.compareTo(name2);
                    }
                }
            } catch (Exception e) {
                MyLog.e(TAG, "compare: ", e);
            }
            return 0;
        }

    };
    public static Comparator<GroupMembersPojo> groupMemberAsc = new Comparator<GroupMembersPojo>() {

        @Override
        public int compare(GroupMembersPojo lhs, GroupMembersPojo rhs) {
            String name1 = lhs.getContactName().toUpperCase();
            String name2 = rhs.getContactName().toUpperCase();

            //ascending order
            return name1.compareTo(name2);
        }

    };
    public static Comparator<String> stringAsc = new Comparator<String>() {

        @Override
        public int compare(String lhs, String rhs) {
            String name1 = lhs.toUpperCase();
            String name2 = rhs.toUpperCase();

            //ascending order
            return name1.compareTo(name2);
        }

    };
    public Session session;
    Context context;
    String uniqueCurrentID, receiverDocumentID;
    ContactDB_Sqlite contactDB_sqlite;

    public Getcontactname(Context context) {
        this.context = context;
        session = new Session(context);
        uniqueCurrentID = SessionManager.getInstance(context).getCurrentUserID();
        contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
    }

    public static void CircleloadImage(Context context, String path, final ImageView imageView, int resize, int placeHolderIcon) {
        try {
            if (path == null || path.isEmpty())
                return;
            BitmapTypeRequest glideRequestmgr = Glide.with(context).load(path).asBitmap();
            if (resize > 0)
                glideRequestmgr.override(resize, resize);

            if (placeHolderIcon > 0)
                glideRequestmgr.placeholder(placeHolderIcon);
            glideRequestmgr.error(placeHolderIcon);
            glideRequestmgr.diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .dontAnimate()

                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(imageView.getContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            imageView.setImageDrawable(circularBitmapDrawable);
                            imageView.setImageBitmap(resource);
                        }

                    });
        } catch (Exception e) {
            MyLog.e(TAG, "LoadImage: ", e);
        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static JSONObject getUserDetailsObject(Context context, String userId) {

        JSONObject eventObj = new JSONObject();
        try {
            eventObj.put("userId", userId);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return eventObj;
    }

    public static String getAppVersion(Context context) {
        PackageManager manager = context.getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(
                    context.getPackageName(), 0);
            return info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            MyLog.e(TAG, "", e);
            return "";
        }

    }
//old
  /*  private void loadPic(final ImageView image, final String userId, final boolean needTransform,
                         final boolean isListItem, final int errorResId) {

        if (userId == null || userId.equals("")) {
            image.setImageResource(errorResId);
        } else {

            if (contactDB_sqlite.getBlockedMineStatus(userId, false).equals(ContactDB_Sqlite.BLOCKED_STATUS)) {
                image.setImageResource(errorResId);
            } else {

                String profilePicVisibility = contactDB_sqlite.getProfilePicVisibility(userId);

                if (profilePicVisibility.equals(ContactDB_Sqlite.PRIVACY_STATUS_NOBODY)) {
                    image.setImageResource(errorResId);
                } else {

                    boolean canShow = false;
                    if (profilePicVisibility.equals(ContactDB_Sqlite.PRIVACY_STATUS_EVERYONE)) {
                        canShow = true;
                    } else {
                        if (contactDB_sqlite.getMyContactStatus(userId).equals("1")) {
                            canShow = true;
                        }
                    }
                    Log.e(TAG, "configProfilepic: canshow " + canShow);

                    if (canShow) {
                        String imageTS = contactDB_sqlite.getDpUpdatedTime(userId);
                        if (imageTS == null || imageTS.isEmpty() || imageTS.equalsIgnoreCase("0"))
                            imageTS = "1";
                        //  final String avatar = Constants.USER_PROFILE_URL + userId + ".jpg?id=" + imageTS;
                        String avatar = "";
                        String Ios_image = contactDB_sqlite.getSingleData(userId, ContactDB_Sqlite.AVATARIMAGEURL);
                        if (!AppUtils.isEmpty(Ios_image)) {
                            if (!Ios_image.equalsIgnoreCase("")) {
                                avatar = AppUtils.getValidProfilePath(Ios_image);
                                //System.out.println("ChangedAvater" + " " + " " + avatar);
                            } else {
                                avatar = Constants.USER_PROFILE_URL + userId + "?id=" + imageTS;
                            }
                            //  final String avatar=Constants.USER_PROFILE_URL +contactDB_sqlite.getUserOpponenetDetails(userId).getAvatarImageUrl()+ "?id=" + imageTS;
                            Log.e(TAG, "configProfilepic: canshow " + avatar);
                        } else {
                            avatar = Constants.USER_PROFILE_URL + userId + "?id=" + imageTS;
                        }
*//*                        Picasso picasso = Picasso.with(context);
                        RequestCreator requestCreator = picasso.load(avatar).error(errorResId);*//*
                        Log.e(TAG, "configProfilepic: isListItem " + isListItem);

                        if (isListItem) {
                            Log.e(TAG, "loadImage: " + isListItem);
//Check Enxryption is enable or not
                            if(AppUtils.isEncryptionEnabled(context)){
//                                AppUtils.loadImage(context, avatar, image, 100, errorResId);
                                AppUtils.loadImageSmooth(context, avatar, image, 100, errorResId);

                            }else {
                            //    AppUtils.loadImage(context, avatar, image, 100, errorResId);
                                AppUtils.loadImageSmooth(context, avatar, image, 100, errorResId);

                            }
          //                  AppUtils.loadImage(context, avatar, image, 100, errorResId);
                        } else {
*//*                            Glide.with(context).load(avatar)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .dontAnimate()
                                    .dontTransform()
                                    .error(errorResId)
                                    .into(image);*//*
                            //contacts load image
                            if (errorResId == R.mipmap.contact_off) {
                                //dont use it in RecyclerView
                                Log.e(TAG, "loadImageSmooth: " + isListItem);

                                AppUtils.loadImageSmooth(context, avatar, image, 100, errorResId);
                            } else {
                                Log.e(TAG, "loadImageSmooth: " + isListItem);

                                AppUtils.loadImageSmooth(context, avatar, image, 100, errorResId);
                                //AppUtils.loadImage(context, avatar, image, 0, errorResId);
                            }
                        }
                    } else {
                        MyLog.d(TAG, "configProfilepic: not show");

                        image.setImageResource(errorResId);
                    }
                }
            }
        }


    }*/

    public String getAvatarUrl(String userId) {
        String avatar = "";
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);

        if (contactDB_sqlite.getBlockedMineStatus(userId, false).equals(ContactDB_Sqlite.UN_BLOCKED_STATUS)) {

            String profilePicVisibility = contactDB_sqlite.getProfilePicVisibility(userId);

            if (!profilePicVisibility.equals(ContactDB_Sqlite.PRIVACY_STATUS_NOBODY)) {

                boolean canShow = false;
                if (profilePicVisibility.equals(ContactDB_Sqlite.PRIVACY_STATUS_EVERYONE)) {
                    canShow = true;
                } else {
                    if (contactDB_sqlite.getMyContactStatus(userId).equals("1")) {
                        canShow = true;
                    }
                }

                if (canShow) {
                    String imageTS = contactDB_sqlite.getDpUpdatedTime(userId);
                    if (imageTS == null || imageTS.isEmpty())
                        imageTS = "1";

                    String Ios_image = contactDB_sqlite.getSingleData(userId, ContactDB_Sqlite.AVATARIMAGEURL);
                    if (!AppUtils.isEmpty(Ios_image)) {
                        if (!Ios_image.equalsIgnoreCase("")) {
                        /*avatar = Ios_image.replace("./uploads/users/", Constants.USER_PROFILE_URL);
                        avatar = avatar + "?id=" + imageTS;*/
                            avatar = AppUtils.getValidProfilePath(Ios_image);
                        } else {
                            avatar = Constants.USER_PROFILE_URL + userId + "?id=" + imageTS;
                        }
                    }


                    // avatar = Constants.USER_PROFILE_URL + userId + ".jpg?id=" + imageTS;
                }
            }
        }
        return avatar;
    }


//----------------------------------------Round Image Set Method-------------------------------------------------------------

    public String getSendername(String userid, String msisdn) {
        String sendername = msisdn;

        if (userid != null) {
            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
            long contactRevision = contactDB_sqlite.getOpponenet_UserDetails_savedRevision(userid);
            long syncedRevision = SessionManager.getInstance(context).getContactSavedRevision();

            if (contactRevision >= syncedRevision) {
                String firstNameInDB = contactDB_sqlite.getSingleData(userid, ContactDB_Sqlite.FIRSTNAME);
                if (firstNameInDB != null) {
                    String firstName = "";
                    if (firstNameInDB.length() > 0)
                        firstName = firstNameInDB;
                    sendername = firstName;
                }
            }
        }

        return sendername;
    }

    public String getGroupMemberName(String userid, String msisdn) {

        String name = getSendername(userid, msisdn);
        if (name.equals(msisdn)) {
            String contactName = getSendername(msisdn);
            if (contactName != null && contactName.length() > 0 && !contactName.equals(msisdn))
                name = name + " (" + contactName + ")";
        }
        return name;
    }

    public String getSendername(String msisdn) {
        String sendername = msisdn;
        String firstNameInDB = contactDB_sqlite.getNameByMobile(msisdn);
        if (firstNameInDB != null) {
            String firstName = "";
            if (firstNameInDB.length() > 0) {
                firstName = firstNameInDB;
                sendername = firstName;
            }
        }


        return sendername;
    }

    public String getSendername(String userid, String msisdn, Context mContext) {
        String sendername = msisdn;

        if (userid != null) {
            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
            long contactRevision = contactDB_sqlite.getOpponenet_UserDetails_savedRevision(userid);
            long syncedRevision = SessionManager.getInstance(mContext).getContactSavedRevision();

            if (contactRevision >= syncedRevision) {
                String firstNameInDB = contactDB_sqlite.getSingleData(userid, ContactDB_Sqlite.FIRSTNAME);
                if (firstNameInDB != null) {
                    String firstName = "";
                    if (firstNameInDB.length() > 0) {
                        firstName = firstNameInDB;
                        sendername = firstName;
                    }
                }
            }
        }

        return sendername;
    }

    public void configProfilepic(final ImageView image, final String userId, final boolean needTransform,
                                 final boolean isListItem, final int errorResId) {

        // new LoadPic(image,userId,needTransform,isListItem,errorResId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        loadPic(image, userId, needTransform, isListItem, errorResId);

    }

    private void loadPic(final ImageView image, final String userId, final boolean needTransform,
                         final boolean isListItem, final int errorResId) {

        if (userId == null || userId.equals("")) {
            image.setImageResource(errorResId);
        } else {

            if (contactDB_sqlite.getBlockedMineStatus(userId, false).equals(ContactDB_Sqlite.BLOCKED_STATUS)) {
                image.setImageResource(errorResId);
            } else {

                String profilePicVisibility = contactDB_sqlite.getProfilePicVisibility(userId);

                if (profilePicVisibility.equals(ContactDB_Sqlite.PRIVACY_STATUS_NOBODY)) {
                    image.setImageResource(errorResId);
                } else {

                    boolean canShow = false;
                    if (profilePicVisibility.equals(ContactDB_Sqlite.PRIVACY_STATUS_EVERYONE)) {
                        canShow = true;
                    } else {
                        if (contactDB_sqlite.getMyContactStatus(userId).equals("1")) {
                            canShow = true;
                        }
                    }
                    Log.e(TAG, "configProfilepic: canshow " + canShow);

                    if (canShow) {
                        String imageTS = contactDB_sqlite.getDpUpdatedTime(userId);
                        if (imageTS == null || imageTS.isEmpty() || imageTS.equalsIgnoreCase("0"))
                            imageTS = "1";
                        //  final String avatar = Constants.USER_PROFILE_URL + userId + ".jpg?id=" + imageTS;
                        String avatar = "";
                        String Ios_image = contactDB_sqlite.getSingleData(userId, ContactDB_Sqlite.AVATARIMAGEURL);
                        if (!AppUtils.isEmpty(Ios_image)) {
                            if (!Ios_image.equalsIgnoreCase("")) {
                                avatar = AppUtils.getValidProfilePath(Ios_image);
                                //System.out.println("ChangedAvater" + " " + " " + avatar);
                            } else {
                                avatar = Constants.USER_PROFILE_URL + userId + "?id=" + imageTS;
                            }
                            //  final String avatar=Constants.USER_PROFILE_URL +contactDB_sqlite.getUserOpponenetDetails(userId).getAvatarImageUrl()+ "?id=" + imageTS;
                            Log.e(TAG, "configProfilepic: canshow " + avatar);
                        } else {
                            //  avatar = Constants.USER_PROFILE_URL + userId + "?id=" + imageTS;
                            avatar = Constants.USER_PROFILE_URL + userId;

                        }
/*                        Picasso picasso = Picasso.with(context);
                        RequestCreator requestCreator = picasso.load(avatar).error(errorResId);*/
                        Log.e(TAG, "configProfilepic: isListItem " + isListItem);

                        if (isListItem) {
                            Log.e(TAG, "loadImage: " + isListItem);
//Check Enxryption is enable or not
                            if (AppUtils.isEncryptionEnabled(context)) {
//                                AppUtils.loadImage(context, avatar, image, 100, errorResId);
                                AppUtils.loadImageSmooth(context, avatar, image, 100, errorResId);

                            } else {
                                //    AppUtils.loadImage(context, avatar, image, 100, errorResId);
                                AppUtils.loadImageSmooth(context, avatar, image, 100, errorResId);

                            }
                            //                  AppUtils.loadImage(context, avatar, image, 100, errorResId);
                        } else {
/*                            Glide.with(context).load(avatar)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .dontAnimate()
                                    .dontTransform()
                                    .error(errorResId)
                                    .into(image);*/
                            //contacts load image
                            if (errorResId == R.mipmap.contact_off) {
                                //dont use it in RecyclerView
                                Log.e(TAG, "loadImageSmooth: " + isListItem);

                                AppUtils.loadImageSmooth(context, avatar, image, 100, errorResId);
                            } else {
                                Log.e(TAG, "loadImageSmooth: " + isListItem);

                                AppUtils.loadImageSmooth(context, avatar, image, 100, errorResId);
                                //AppUtils.loadImage(context, avatar, image, 0, errorResId);
                            }
                        }
                    } else {
                        MyLog.d(TAG, "configProfilepic: not show");

                        image.setImageResource(errorResId);
                    }
                }
            }
        }


    }

    public void configCircleProfilepic(final ImageView image, final String userId, final boolean needTransform,
                                       boolean isListItem, final int errorResId) {

        if (userId == null || userId.equals("")) {

            image.setImageResource(errorResId);
        } else {

            if (contactDB_sqlite.getBlockedMineStatus(userId, false).equals(ContactDB_Sqlite.BLOCKED_STATUS)) {
                image.setImageResource(errorResId);
            } else {

                String profilePicVisibility = contactDB_sqlite.getProfilePicVisibility(userId);

                if (profilePicVisibility.equals(ContactDB_Sqlite.PRIVACY_STATUS_NOBODY)) {
                    image.setImageResource(errorResId);

                } else {

                    boolean canShow = false;
                    if (profilePicVisibility.equals(ContactDB_Sqlite.PRIVACY_STATUS_EVERYONE)) {
                        canShow = true;
                    } else {
                        if (contactDB_sqlite.getMyContactStatus(userId).equals("1")) {
                            canShow = true;
                        }
                    }

                    if (canShow) {
                        String imageTS = contactDB_sqlite.getDpUpdatedTime(userId);
                        if (imageTS == null || imageTS.isEmpty() || imageTS.equalsIgnoreCase("0"))
                            imageTS = "1";
                        //  final String avatar = Constants.USER_PROFILE_URL + userId + ".jpg?id=" + imageTS;
                        String avatar = "";

                        String Ios_image = contactDB_sqlite.getSingleData(userId, ContactDB_Sqlite.AVATARIMAGEURL);

                        if (!AppUtils.isEmpty(Ios_image)) {
                            if (!Ios_image.equalsIgnoreCase("")) {
                                avatar = AppUtils.getValidProfilePath(Ios_image);
                                //System.out.println("ChangedAvater" + " " + " " + avatar);
                            } else {
                                avatar = Constants.USER_PROFILE_URL + userId + "?id=" + imageTS;
                            }
                        }
                        //  final String avatar=Constants.USER_PROFILE_URL +contactDB_sqlite.getUserOpponenetDetails(userId).getAvatarImageUrl()+ "?id=" + imageTS;
                        MyLog.d(TAG, "configProfilepic: canshow " + avatar);

/*                        Picasso picasso = Picasso.with(context);
                        RequestCreator requestCreator = picasso.load(avatar).error(errorResId);*/
                        if (isListItem) {
                            AppUtils.CircleloadImage(context, avatar, image, 100, errorResId);
                        } else {
/*                            Glide.with(context).load(avatar)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .dontAnimate()
                                    .dontTransform()
                                    .error(errorResId)
                                    .into(image);*/
                            //contacts load image
                            if (errorResId == R.mipmap.contact_off) {
                                //dont use it in RecyclerView
                                AppUtils.loadImageSmooth(context, avatar, image, 100, errorResId);
                            } else {
                                AppUtils.CircleloadImage(context, avatar, image, 0, errorResId);
                            }
                        }
                    } else {
                        MyLog.d(TAG, "configProfilepic: not show");

                        image.setImageResource(errorResId);
                    }
                }
            }
        }

    }

    public boolean setProfileStatusText(TextView tvStatus, String userId, String status, boolean isSecretChat) {
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
        String statusVisibility = contactDB_sqlite.getProfileStatusVisibility(userId);

        boolean isStatusDisplayed = true;

        switch (statusVisibility) {

            case ContactDB_Sqlite.PRIVACY_STATUS_NOBODY:
                tvStatus.setText("");
                isStatusDisplayed = false;
                break;

            case ContactDB_Sqlite.PRIVACY_STATUS_MY_CONTACTS:
                if (contactDB_sqlite.getMyContactStatus(userId).equals("1")) {
                    if (status.equalsIgnoreCase("Hey there! I am using Smarttys")) {
                        status.equalsIgnoreCase("Hey there! I am using Smarttys");
                    } else {
                        tvStatus.setText(status);
                    }

                } else {
                    tvStatus.setText("");
                    isStatusDisplayed = false;
                }
                break;

            default:
                tvStatus.setText("Hey there! I am using Smarttys");
                break;
        }

        if (contactDB_sqlite.getBlockedMineStatus(userId, isSecretChat).equals("1")) {
            tvStatus.setText("");
            isStatusDisplayed = false;
        }

        return isStatusDisplayed;
    }

    public void navigateToChatviewPageforSmarttyModel(SmarttyContactModel e) {

        Intent intent = new Intent(context, ChatPageActivity.class);
        intent.putExtra("receiverUid", e.getNumberInDevice());
        String firstName = "";
        if (e.getFirstName() != null)
            firstName = e.getFirstName();
        intent.putExtra("receiverName", firstName);
        intent.putExtra("documentId", e.get_id());
        intent.putExtra("Username", firstName);
        intent.putExtra("Image", e.getAvatarImageUrl());
        intent.putExtra("type", 0);
        String msisdn = e.getMsisdn();
        intent.putExtra("msisdn", msisdn);
        context.startActivity(intent);

//        ((Activity) context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void navigateToChatViewpagewithmessageitems(MessageItemChat e, String from) {

        String[] array = e.getMessageId().split("-");
        String msgid = "";
        Intent intent = new Intent(context, ChatPageActivity.class);
        if (e.isSecretChat())
            intent = new Intent(context, SecretChatViewActivity.class);
        intent.putExtra("receiverUid", e.getNumberInDevice());
        intent.putExtra("receiverName", e.getSenderName());
        if (array[0].equalsIgnoreCase(uniqueCurrentID)) {
            receiverDocumentID = array[1];
        } else if (array[1].equalsIgnoreCase(uniqueCurrentID)) {
            receiverDocumentID = array[0];
        }
        if (e.getMessageId().contains("-g")) {
            msgid = array[3];
        } else {
            msgid = array[2];
        }
        intent.putExtra("documentId", receiverDocumentID);
        intent.putExtra("Username", e.getSenderName());
        intent.putExtra("Image", e.getAvatarImageUrl());
        intent.putExtra("msisdn", e.getSenderMsisdn());
        if (from.equalsIgnoreCase("star") || from.equalsIgnoreCase("webLink")) {
            intent.putExtra("msgid", msgid);
            intent.putExtra("starred", e.getMessageId());
        }
        intent.putExtra("type", 0);

        //System.out.println("Type:" + "" + e.getNumberInDevice());
        //System.out.println("Type:" + "" + e.getSenderName());
        //System.out.println("Type:" + "" + receiverDocumentID);
        //System.out.println("Type:" + "" + e.getSenderName());
        //System.out.println("Type:" + "" + e.getSenderMsisdn());
        //System.out.println("Type:" + "" + msgid);
        //System.out.println("Type:" + "" + e.getMessageId());

        context.startActivity(intent);
        ((Activity) context).overridePendingTransition(R.anim.fast_enter, R.anim.fast_exit);
//        ((Activity) context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public boolean isContactExists(String userId) {
        long savedRevision = SessionManager.getInstance(context).getContactSavedRevision();
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
        long contactRevision = contactDB_sqlite.getOpponenet_UserDetails_savedRevision(userId);
        return contactRevision >= savedRevision;
    }

    public class LoadPic extends AsyncTask<Void, Void, Void> {

        ImageView image;
        String userId;
        boolean needTransform, isListItem;
        int errorResId;

        LoadPic(final ImageView image, final String userId, final boolean needTransform,
                boolean isListItem, final int errorResId) {
            this.image = image;
            this.userId = userId;
            this.needTransform = needTransform;
            this.isListItem = isListItem;
            this.errorResId = errorResId;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            loadPic(image, userId, needTransform, isListItem, errorResId);
            return null;
        }


    }
}






