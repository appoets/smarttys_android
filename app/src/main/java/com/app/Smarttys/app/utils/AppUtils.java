package com.app.Smarttys.app.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.CursorWindow;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.About_contactus;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.message.TextMessage;
import com.app.Smarttys.core.service.Constants;
import com.app.Smarttys.core.socket.MessageService;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;
import com.app.Smarttys.status.model.StatusModel;
import com.bumptech.glide.BitmapTypeRequest;
import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.load.resource.bitmap.FileDescriptorBitmapDecoder;
import com.bumptech.glide.load.resource.bitmap.VideoBitmapDecoder;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.common.base.Strings;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by user12 on 3/7/2018.
 */

public class AppUtils {


  /*  public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }*/

    private static final String TAG = "AppUtils";

    public static String getTimeString(long millis) {
        try {
            StringBuffer buf = new StringBuffer();

            int hours = (int) (millis / (1000 * 60 * 60));
            int minutes = (int) ((millis % (1000 * 60 * 60)) / (1000 * 60));
            int seconds = (int) (((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000);

            buf.append(String.format("%02d", minutes))
                    .append(":")
                    .append(String.format("%02d", seconds));

            return buf.toString();
        } catch (Exception e) {
            Log.e(TAG, "getTimeString: ", e);
        }
        return "";
    }

    public static void clearNotification(Context activity) {
        try {
            NotificationManager notificationManager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(1);
        } catch (Exception ex) {
            MyLog.e(TAG, "clearNotification: ", ex);
        }
    }

    public static boolean isEmptyString(String text) {
        return (text == null || text.trim().equals("null") || text.trim()
                .length() <= 0);
    }

    public static Long parseLong(String value) {
        long result = System.currentTimeMillis();
        try {
            if (isEmpty(value))
                return result;
            result = Long.parseLong(value);
        } catch (Exception e) {
            MyLog.e(TAG, "parse: ", e);
            return System.currentTimeMillis();
        }
        return result;
    }

    public static Long parseLongForFile(String value) {
        long result = 0L;
        try {
            if (isEmpty(value))
                return result;
            result = Long.parseLong(value);
        } catch (Exception e) {
            MyLog.e(TAG, "parse: ", e);
            return result;
        }
        return result;
    }

    public static boolean isEmpty(final String s) {
        // Null-safe, short-circuit evaluation.
        return s == null || s.trim().isEmpty() || s.equals("null");
    }

    public static int parseInt(String value) {
        try {
            if (value == null || value.isEmpty())
                return 0;

            if (value.contains(".")) {
                return (int) Float.parseFloat(value);
            }
            return Integer.parseInt(value);
        } catch (Exception e) {
            MyLog.e(TAG, "parse: ", e);
        }
        return 0;
    }

    public static Spanned getHtmlText(CharSequence msg) {
        if (msg == null)
            return Html.fromHtml("");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(msg.toString(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(msg.toString());
        }
    }

    public static boolean isEncryptionEnabled(Context context) {
        if (CoreController.isRaad)
            return true;
        return SessionManager.getInstance(context).getIsEncryptionEnabled();
    }

    public static void loadProfilePic(Context context, ImageView imageView) {

        //  Picasso.with(context).load(filePath).error(R.drawable.personprofile).noPlaceholder().into(imageView);
        String path = getProfileFilePath(context);
        if (path != null && !path.isEmpty()) {
/*                        Picasso.with(context).load(path).error(
                                R.drawable.personprofile)
                                .transform(new CircleTransform()).into(imageView);*/
            loadImage(context, path, imageView, 0, R.drawable.personprofile);
        } else {
            imageView.setImageResource(R.drawable.personprofile);
        }
    }

    public static GlideUrl getUrlWithHeaders(String url, Context context) {
        return new GlideUrl(url, new LazyHeaders.Builder()
                .addHeader("authorization", SessionManager.getInstance(context).getSecurityToken())
                .addHeader("requesttype", "site")
                .addHeader("userid", SessionManager.getInstance(context).getCurrentUserID())
                .addHeader("referer", url)
                .build());
    }

    public static void loadOppenentPic(Context context, String path, ImageView imageView, int resize) {

        //  Picasso.with(context).load(filePath).error(R.drawable.personprofile).noPlaceholder().into(imageView);
        if (path != null && !path.isEmpty()) {
/*                        Picasso.with(context).load(path).error(
                                R.drawable.personprofile)
                                .resize(resize, resize)
                                .transform(new CircleTransform()).into(imageView);*/
            GlideUrl glideUrl = new GlideUrl(path,
                    new LazyHeaders.Builder()
                            .addHeader("authorization", SessionManager.getInstance(context).getSecurityToken())
                            .addHeader("requesttype", "site")
                            .addHeader("userid", SessionManager.getInstance(context).getCurrentUserID())
                            .addHeader("referer", path)
                            .build());

            Log.e(TAG, "GlideUrl" + glideUrl.toString());
            try {
                if (isEmptyImage(path))
                    return;
                if (path == null || path.isEmpty())
                    return;
                DrawableTypeRequest glideRequestmgr = Glide.with(context).load((glideUrl));
                if (resize > 0)
                    glideRequestmgr.override(resize, resize);

                if (R.drawable.personprofile > 0)
                    glideRequestmgr.placeholder(R.drawable.personprofile);
                glideRequestmgr.error(R.drawable.personprofile);
                glideRequestmgr.diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontTransform()
                        .dontAnimate()
                        .into(imageView);
            } catch (Exception e) {
                MyLog.e(TAG, "LoadImage: ", e);
            }

        } else {
            imageView.setImageResource(R.drawable.personprofile);
        }
    }

    public static void loadProfilePic(Context context, ImageView imageView, int resize) {

        //  Picasso.with(context).load(filePath).error(R.drawable.personprofile).noPlaceholder().into(imageView);
        String path = getProfileFilePath(context);
        if (path != null && !path.isEmpty()) {
/*                        Picasso.with(context).load(path).error(
                                R.drawable.personprofile)
                                .resize(resize, resize)
                                .transform(new CircleTransform()).into(imageView);*/
            GlideUrl glideUrl = new GlideUrl(path,
                    new LazyHeaders.Builder()
                            .addHeader("authorization", SessionManager.getInstance(context).getSecurityToken())
                            .addHeader("requesttype", "site")
                            .addHeader("userid", SessionManager.getInstance(context).getCurrentUserID())
                            .addHeader("referer", path)
                            .build());

            try {
                if (isEmptyImage(path))
                    return;
                if (path == null || path.isEmpty())
                    return;
                DrawableTypeRequest glideRequestmgr = Glide.with(context).load((glideUrl));
                if (resize > 0)
                    glideRequestmgr.override(resize, resize);

                if (R.drawable.personprofile > 0)
                    glideRequestmgr.placeholder(R.drawable.personprofile);
                glideRequestmgr.error(R.drawable.personprofile);
                glideRequestmgr.diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontTransform()
                        .dontAnimate()
                        .into(imageView);
            } catch (Exception e) {
                MyLog.e(TAG, "LoadImage: ", e);
            }
            // loadImage(context,path,imageView,resize,R.drawable.personprofile);

             /*           GlideUrl glideUrl = new GlideUrl(path,
                                new LazyHeaders.Builder()
                                        .addHeader("authorization", SessionManager.getInstance(context).getSecurityToken())
                                        .addHeader("requesttype", "site")
                                        .addHeader("userid", SessionManager.getInstance(context).getCurrentUserID())
                                        .addHeader("referer", path)
                                        .build());

                        Glide.with(context).load(glideUrl).into(imageView);*/

        } else {
            imageView.setImageResource(R.drawable.personprofile);
        }
    }

    public static String getValidProfilePath(String path) {
        if (path.startsWith("./")) {
            path = path.replaceFirst("./", Constants.SOCKET_IP);
        } else if (!path.startsWith(Constants.SOCKET_IP)) {
            path = Constants.SOCKET_IP.concat(path);
        }
        //change
//        if (!path.contains("?id")) {
//            path = path + "?id=" + eodMillis();
//        }

        return path;
    }

    public static boolean isDeviceLockEnabled(Context context) {
        boolean isEnabled = SessionManager.getInstance(context).isDeviceLockEnabled();
        MyLog.d(TAG, "isDeviceLockEnabled: " + isEnabled);
        return isEnabled;
    }

    public static String getValidGroupPath(String path) {
        if (path.startsWith("./")) {
            path = path.replaceFirst("./", Constants.SOCKET_IP);
        } else if (!path.startsWith(Constants.SOCKET_IP)) {
            path = Constants.SOCKET_IP.concat(path);
        }
        if (!path.contains("?id")) {
            path = path + "?id=1";
        }
        return path;
    }

    public static String getValidServerPath(String path) {
        if (path.startsWith("./")) {
            path = path.replaceFirst("./", Constants.SOCKET_IP);
        } else if (!path.startsWith(Constants.SOCKET_IP)) {
            path = Constants.SOCKET_IP.concat(path);
        }
        return path;
    }

    //we set the end of the day millis -> for avoid image reloading
    public static long eodMillis() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime().getTime();
    }

    public static String getOtherUserProfilePicPath(String userId, Context context) {
        String path = null;
        ContactDB_Sqlite contactDB_sqlite = new ContactDB_Sqlite(context);
        String imagePath = contactDB_sqlite.getSingleData(userId, ContactDB_Sqlite.AVATARIMAGEURL);
        if (imagePath != null && !imagePath.isEmpty())
            path = getValidProfilePath(imagePath);
        return path;
    }

    //old function update
    public static String getProfileFilePath(Context context) {
        String prefPath = SessionManager.getInstance(context).getUserProfilePic();
        String filePath = "";

        if (prefPath != null && !prefPath.isEmpty()) {
            if (prefPath.startsWith(Constants.SOCKET_IP)) {
                filePath = prefPath;

            } else {
                if (prefPath.startsWith("./")) {
                    filePath = prefPath.replaceFirst("./", Constants.SOCKET_IP);
                } else
                    filePath = Constants.SOCKET_IP + prefPath;
            }
        }

        String token = SessionManager.getInstance(context).getSecurityToken();
        String tokenPredic = SessionManager.getInstance(context).getCurrentUserID();

        String mainUrl = filePath + "?timestamp=1625834233028&atoken=" + token + "&au=" + tokenPredic + "&at=site";
        Log.e("Test", "mainurl" + mainUrl);


        return mainUrl;
    }

    public static String getTaggedMsgs(String mCurrentUserId, String msg, Context context) {

        try {
            ContactDB_Sqlite contactDB_sqlite = new ContactDB_Sqlite(context);

            String[] splitedTexts = msg.split(" ");
            String newPayLoad = "";
            if (splitedTexts != null && splitedTexts.length > 0) {
                for (int i = 0; i < splitedTexts.length; i++) {
                    String text = splitedTexts[i];
                    if (text.contains(TextMessage.TAG_KEY)) {
                        String userName = "";
                        String userId = text.replace(TextMessage.TAG_KEY, "");
                        if (userId != null && userId.equals(mCurrentUserId)) {
                            userName = "You";
                        } else {
                            userName = contactDB_sqlite.getSingleData(userId, ContactDB_Sqlite.FIRSTNAME);

                            if (userName == null || userName.isEmpty()) {
                                userName = contactDB_sqlite.getSingleData(userId, ContactDB_Sqlite.MSISDN);
                            }
                        }
                        userName = "@" + userName;
                        newPayLoad = newPayLoad + userName + " ";
                    } else {
                        newPayLoad = newPayLoad + text + " ";
                    }

                }
            }
            return newPayLoad;
        } catch (Exception e) {
            MyLog.e(TAG, "getTaggedMsgs: ", e);
        }
        return msg;
    }







   /* public static String getProfileFilePath(Context context) {
        String prefPath = SessionManager.getInstance(context).getUserProfilePic();
        Log.e("ProfilePath","prefPath "+prefPath);
        //String filePath = " http://www.smarttysapp.com:8080/uploads/users/60ed769fe2ab405f1708f8ee.jpg?";
        // String filePath = " http://www.smarttysapp.com:8080/uploads/users/60ed769fe2ab405f1708f8ee.jpg?"+""+"1626460199999";

        try {
            URL url=new URL(prefPath);
           String id= url.getQuery().replaceFirst("id=","");
            String urlPath =url.getHost()+url.getPath();
            Log.e("ProfilePath","id "+id);
            Log.e("ProfilePath","urlPath "+urlPath);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if (prefPath != null && !prefPath.isEmpty()) {
            if (prefPath.startsWith(Constants.SOCKET_IP)) {
                //   filePath = prefPath;
            } else {
                if (prefPath.startsWith("./")) {
                    return prefPath.replaceFirst("./", Constants.SOCKET_IP);
                } else
                    return Constants.SOCKET_IP + prefPath;
            }
        }
        return null;
    }*/

    public static void shareApp(Context context) {

        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        String appname = context.getResources().getString(R.string.app_name);
        //emailIntent.setType("application/image");
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_TEXT, Constants.getAppStoreLink(context));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, appname + " : Android");
        context.startActivity(Intent.createChooser(emailIntent, "Send via..."));
    }

    public static Bitmap getThumbnailFromVideo(String path) {
        try {

            // MINI_KIND, size: 512 x 384 thumbnail
            return ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
        } catch (Exception e) {
            MyLog.e(TAG, "getThumbnailFromVideo: ", e);
            return null;
        }
    }

    public static Bitmap getThumbnailFromVideo(Context context, Uri uri) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            String picturePath = "";
            Cursor cursor = context.getContentResolver().query(uri, filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();
            }
            // MINI_KIND, size: 512 x 384 thumbnail
            return ThumbnailUtils.createVideoThumbnail(picturePath, MediaStore.Video.Thumbnails.MINI_KIND);
        } catch (Exception e) {
            MyLog.e(TAG, "getThumbnailFromVideo: ", e);
            return null;
        }
    }

    public static Bitmap getBitmapFromImageUri(Context context, Uri uri) {
        if (uri == null)
            return null;
        try {
            InputStream image_stream = context.getContentResolver().openInputStream(uri);
            return BitmapFactory.decodeStream(image_stream);
        } catch (Exception e) {
            return null;
        }
    }

    public static GlideUrl getGlideURL(String url, Context context) {
        return new GlideUrl(url, new LazyHeaders.Builder()
                .addHeader("authorization", SessionManager.getInstance(context).getSecurityToken())
                .addHeader("requesttype", "site")
                .addHeader("userid", SessionManager.getInstance(context).getCurrentUserID())
                .addHeader("referer", url)
                .build());
    }

    public static OkHttpClient getPicassoHeader(final Context context) {
        return new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("authorization", "" + SessionManager.getInstance(context).getSecurityToken())
                                .addHeader("requesttype", "site")
                                .addHeader("userid", "" + SessionManager.getInstance(context).getCurrentUserID())
                                .build();
                        return chain.proceed(newRequest);
                    }
                })
                .build();
    }

    public static void LoadImage(Context context, String url, ImageView imageView, int placeHolderIconId) {
        try {


            if (url != null && !url.isEmpty() && context != null)

                Glide.with(context).load(getGlideURL(url, context)).placeholder(placeHolderIconId).dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontTransform()
                        .override(100, 100)
                        .into(imageView);
        } catch (Exception e) {
            MyLog.e(TAG, "LoadImage: ", e);
        }
    }

    public static String getFileNameFromPath(String imgPath) {
        String fileName = "";
        try {
            fileName = imgPath.substring(imgPath.lastIndexOf("/") + 1);
        } catch (Exception e) {
            MyLog.e(TAG, "getFileNameFromPath: ", e);
        }
        return fileName;
    }

    public static void loadLocalImage(Context context, String path, ImageView imageView) {
        try {
            Uri uri = Uri.fromFile(new File(path));

            Glide.with(context).load(getValidLocalImageFile(path))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .dontAnimate()
                    .into(imageView);
        } catch (Exception e) {
            MyLog.e(TAG, "LoadImage: ", e);
        }
    }

    public static String getValidLocalImageFile(String path) {
        try {
            File file = new File(path);
            if (file.exists()) {
                return path;
            }
            String publicDirPath = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES).getAbsolutePath();

            String[] splited = path.split("/");

            File publicDirFIle = new File(publicDirPath + "/" + splited[splited.length - 1]);
            if (publicDirFIle.exists())
                return publicDirFIle.getPath();
        } catch (Exception e) {
            return path;
        }
        Log.e("path", "path" + path);
        return path;
    }

    public static void AdapterloadLocalImage(Context context, String path, ImageView imageView) {
        try {
            // File localFile = new File(path);
            Uri uri = Uri.fromFile(new File(path));
            Glide.with(context).load(getValidLocalImageFile(path))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .dontAnimate()
                    .into(imageView);
        } catch (Exception e) {
            MyLog.e(TAG, "LoadImage: ", e);
        }
    }

    public static void loadLocalImage(Context context, String path, ImageView imageView, int width, int height) {
        try {
            // File localFile = new File(path);
            Uri uri = Uri.fromFile(new File(path));
            Glide.with(context).load(uri)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .dontAnimate()
                    .override(width, height)
                    .into(imageView);
        } catch (Exception e) {
            MyLog.e(TAG, "LoadImage: ", e);
        }
    }

    public static void loadVideoThumbnail(Context context, String path, ImageView imageView, float quality, int resize) {
        try {
            // File localFile = new File(path);
            Uri uri = Uri.fromFile(new File(path));
            Glide.with(context).load(uri)
                    // .diskCacheStrategy(DiskCacheStrategy.ALL)
                    //.dontTransform()
                    .dontAnimate()
                    // .thumbnail(quality)
                    // .override(resize,resize)
                    .into(imageView);
        } catch (Exception e) {
            MyLog.e(TAG, "LoadImage: ", e);
        }
    }

    public static void loadglideimage(Context mContext, GlideUrl glideUrl, final ImageView imageView) {
        Glide
                .with(mContext)
                .load(glideUrl)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .error(R.mipmap.chat_attachment_profile_default_image_frame)
                .into(new SimpleTarget<Bitmap>() {

                    @Override
                    public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                        // TODO Auto-generated method stub
                        imageView.setImageBitmap(arg0);

                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    }
                });
    }

    public static void loadLocalVideoThumbanail(Context context, String path, final ImageView imageView) {
        try {
            if (path == null || path.isEmpty())
                return;

            BitmapPool bitmapPool = Glide.get(context).getBitmapPool();
            int microSecond = 1000000;// 6th second as an example
            VideoBitmapDecoder videoBitmapDecoder = new VideoBitmapDecoder(microSecond);
            FileDescriptorBitmapDecoder fileDescriptorBitmapDecoder = new FileDescriptorBitmapDecoder(videoBitmapDecoder, bitmapPool, DecodeFormat.PREFER_ARGB_8888);

            File file = new File(getValidLocalVideoPath(path));
            Glide.with(context).load(path).asBitmap().thumbnail(0.6f)
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .dontAnimate()
                    .videoDecoder(fileDescriptorBitmapDecoder)
                    .override(200, 200)
                    .into(imageView);
        } catch (Exception e) {
            MyLog.e(TAG, "LoadImage: ", e);
        }
    }

    public static String getValidLocalVideoPath(String path) {
        try {
            File videoFile = new File(path);
            if (!videoFile.exists()) {
                String[] filePathSplited = path.split(File.separator);
                String fileName = filePathSplited[filePathSplited.length - 1];

                String publicDirPath = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
                String filePath = publicDirPath + File.separator + fileName;
                videoFile = new File(filePath);
                if (videoFile.exists())
                    return videoFile.getPath();
            }
        } catch (Exception e) {
            Log.e(TAG, "getValidLocalVideoPath: ", e);
        }

        return path;
    }

    public static void loadLiveVideoThumbanail(Context context, String path, final ImageView imageView) {
        try {
            if (path == null || path.isEmpty())
                return;

            path = getValidServerPath(path);
            Glide
                    .with(context)
                    .load(getGlideURL(path, context))
                    .asBitmap()
                    .thumbnail(0.6f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .into(imageView);
        } catch (Exception e) {
            MyLog.e(TAG, "LoadImage: ", e);
        }
    }

    public static boolean isEmptyImage(String path) {
        String emptyImage = Constants.IMAGE_BASE_URL + "/?id";
        return path.contains(emptyImage);

    }

    public static void fixFor10SecMemoryException() {
        try {
            Class clazz = Class.forName("java.lang.Daemons$FinalizerWatchdogDaemon");

            Method method = clazz.getSuperclass().getDeclaredMethod("stop");
            method.setAccessible(true);

            Field field = clazz.getDeclaredField("INSTANCE");
            field.setAccessible(true);

            method.invoke(field.get(null));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dbCursorFix() {
        try {
            Field field = CursorWindow.class.getDeclaredField("sCursorWindowSize");
            field.setAccessible(true);
            field.set(null, 1024000 * 1024); //the 102400 is the new size added
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadImage(Context context, String path, final ImageView imageView, int resize, int placeHolderIcon) {
        try {
            MyLog.d(TAG, "loadImage imagetest1: " + path);
            if (path == null || path.isEmpty() || isEmptyImage(path)) {
                MyLog.d(TAG, "loadImage imagetest2 empty image: ");
                if (placeHolderIcon > 0) {
                    MyLog.d(TAG, "loadImage imagetest3 empty image reource setted: ");
                    imageView.setImageResource(placeHolderIcon);
                }
                return;
            }

            // BitmapTypeRequest glideRequestmgr = Glide.with(context).load(getGlideURL(path, context)).asBitmap();

            BitmapTypeRequest glideRequestmgr = Glide.with(context).load(path).asBitmap();
            if (resize > 0)
                glideRequestmgr.override(resize, resize);

            if (placeHolderIcon > 0)
                glideRequestmgr.placeholder(placeHolderIcon);
            glideRequestmgr.error(placeHolderIcon);

            glideRequestmgr.diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .dontAnimate()
                    .into(new SimpleTarget<Bitmap>() {

                        @Override
                        public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                            if (imageView != null)
                                imageView.setImageBitmap(arg0);
                        }

                    });
        } catch (Exception e) {
            MyLog.e(TAG, "LoadImage: ", e);
        }
    }

    public static void loadBase64Image(int size, Context context, String base64Image, final ImageView image) {
        try {

            if (base64Image.startsWith("data:image/jpeg;base64,")) {
                base64Image = base64Image.replace("data:image/jpeg;base64,", "");
            }
            Glide.with(context)
                    .load(Base64.decode(base64Image, Base64.DEFAULT))
                    .asBitmap()
                    .dontTransform()
                    .dontAnimate()
                    .override(size, size)
                    .into(new SimpleTarget<Bitmap>() {

                        @Override
                        public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                            image.setImageBitmap(arg0);
                        }
                    });
        } catch (Exception e) {
            Log.e(TAG, "loadBase64Image: ", e);
        }
    }

    public static void CircleloadImage(Context context, String path, final ImageView imageView, int resize, int placeHolderIcon) {
        try {
            if (path == null || path.isEmpty())
                return;
            BitmapTypeRequest glideRequestmgr = Glide.with(context).load(getGlideURL(path, context)).asBitmap();
            if (resize > 0)
                glideRequestmgr.override(resize, resize);

            if (placeHolderIcon > 0)
                glideRequestmgr.placeholder(placeHolderIcon);
            glideRequestmgr.error(placeHolderIcon);
            glideRequestmgr.diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .dontAnimate()

                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(imageView.getContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            imageView.setImageDrawable(circularBitmapDrawable);
                            imageView.setImageBitmap(resource);
                        }

                    });
        } catch (Exception e) {
            MyLog.e(TAG, "LoadImage: ", e);
        }
    }

    public static boolean isEmoji(String input) {
        boolean isEmoji = false;
        try {
            String EMOJI_RANGE_REGEX =
                    "[\uD83C\uDF00-\uD83D\uDDFF]|[\uD83D\uDE00-\uD83D\uDE4F]|[\uD83D\uDE80-\uD83D\uDEFF]|[\u2600-\u26FF]|[\u2700-\u27BF]";
            Pattern PATTERN = Pattern.compile(EMOJI_RANGE_REGEX);


            if (Strings.isNullOrEmpty(input)) {
                return false;
            }
            Matcher matcher = PATTERN.matcher(input);

            return matcher.find();

        } catch (Exception e) {
            MyLog.e(TAG, "isEmoji: ", e);
        }
        return false;
    }

    public static void startService(Context context, Class className) {
        try {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
                Intent restartServiceIntent = new Intent(context, className);
                restartServiceIntent.setPackage(context.getPackageName());
                PendingIntent restartServicePendingIntent = PendingIntent.getService(context, 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
                AlarmManager alarmService = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                if (alarmService != null) {
                    alarmService.set(
                            AlarmManager.ELAPSED_REALTIME,
                            SystemClock.elapsedRealtime() + 500,
                            restartServicePendingIntent);
                }
            } else {
                Intent i = new Intent(context, className);
                context.startService(i);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "startService: ", e);
        }
    }

    public static void DownloadstartService(Context context, Class className) {
        try {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
                Intent restartServiceIntent = new Intent(context, className);
                restartServiceIntent.setPackage(context.getPackageName());
                PendingIntent restartServicePendingIntent = PendingIntent.getService(context, 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
                AlarmManager alarmService = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                if (alarmService != null) {
                    alarmService.set(
                            AlarmManager.ELAPSED_REALTIME,
                            SystemClock.elapsedRealtime() + 500,
                            restartServicePendingIntent);
                }
            } else {
                Intent i = new Intent(context, className);
                context.startService(i);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "startService: ", e);
        }
    }

    public static String encrypt(String key, String value) {
        try {
            SecretKey secretKey = new SecretKeySpec(Base64.decode(key.getBytes(), Base64.NO_WRAP), "AES");
            //  AlgorithmParameterSpec iv = new IvParameterSpec(Base64.decode(key.getBytes(), Base64.NO_WRAP));
            Random rand = new SecureRandom();
            byte[] bytes = new byte[16];
            rand.nextBytes(bytes);
            IvParameterSpec ivSpec = new IvParameterSpec(bytes);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            //cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);

            return new String(Base64.encode(cipher.doFinal(value.getBytes(StandardCharsets.UTF_8)), Base64.NO_WRAP));

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
        return null;
    }

    public static String decrypt(String key, String value) {
        try {
            SecretKey secretKey = new SecretKeySpec(Base64.decode(key.getBytes(), Base64.NO_WRAP), "AES");
            //  AlgorithmParameterSpec iv = new IvParameterSpec(Base64.decode(key.getBytes(), Base64.NO_WRAP));
            Random rand = new SecureRandom();
            byte[] bytes = new byte[16];
            rand.nextBytes(bytes);
            IvParameterSpec ivSpec = new IvParameterSpec(bytes);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            // cipher.init(Cipher.DECRYPT_MODE, secretKey, ivSpec);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] decode = Base64.decode(value, Base64.NO_WRAP);
            return new String(cipher.doFinal(decode), StandardCharsets.UTF_8);

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
        return null;
    }

    // ram custom
    public static String getProfileFilePath(Context context, String filePathGet) {

        String filePath = filePathGet;


        String token = SessionManager.getInstance(context).getSecurityToken();
        String tokenPredic = SessionManager.getInstance(context).getCurrentUserID();

        String mainUrl = filePath + "?timestamp=1625834233028&atoken=" + token + "&au=" + tokenPredic + "&at=site";
        Log.e("Test", "mainurl" + mainUrl);


        return mainUrl;
    }

    // ***** Warning ******* --> don't use it in RecyclerView. It will produce problems in Recyclerview
    public static void loadImageSmooth(Context context, String path, final ImageView imageView, int resize, int placeHolderIcon) {
        try {
            if (path == null || path.isEmpty())
                return;
            if (imageView != null)
                Glide.clear(imageView);

            Log.e("Path", "here  " + path);

            String customPath = getProfileFilePath(context, path);

            //  DrawableTypeRequest glideRequestmgr = Glide.with(context).load(getGlideURL(path, context));
            DrawableTypeRequest glideRequestmgr = Glide.with(context).load(customPath);
            if (resize > 0)
                glideRequestmgr.override(resize, resize);

            if (placeHolderIcon > 0)
                glideRequestmgr.placeholder(placeHolderIcon);

            glideRequestmgr.diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .dontAnimate()
                    .into(new SimpleTarget<GlideDrawable>() {
                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                            try {
                                if (imageView != null) {
                                    imageView.setImageDrawable(resource.getCurrent());
                                }
                            } catch (Exception e) {
                                MyLog.e(TAG, "onResourceReady: ", e);
                            }
                        }
                    });
        } catch (Exception e) {
            MyLog.e(TAG, "LoadImage: ", e);
        }
    }

    public static LinearLayoutManager getHorizontalLayoutManger(Context context) {
        if (context != null)
            return new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        return null;
    }

    public static LinearLayoutManager getDefaultLayoutManger(Context context) {
        if (context != null)
            return new LinearLayoutManager(context);
        return null;
    }

    public static LinearLayoutManager getGridLayoutManger(Context context, int spanCount) {
        if (context != null)
            return new GridLayoutManager(context, spanCount);
        return null;
    }

    public static boolean isNetworkAvailable(Context context) {
        int[] networkTypes = {ConnectivityManager.TYPE_MOBILE,
                ConnectivityManager.TYPE_WIFI};
        try {
/*
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                context.registerReceiver(new ConnectivityReceiver(), new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            }
*/

            ConnectivityManager connectivityManager =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            for (int networkType : networkTypes) {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null &&
                        activeNetworkInfo.getType() == networkType)
                    return true;
            }
        } catch (Exception e) {
            MyLog.e(TAG, "isNetworkAvailable: ", e);
            return false;
        }
        return false;
    }

    public static void restartService(Context context) {
        MyLog.d(TAG, "restartService: ");
        Intent restartServiceIntent = new Intent(context, MessageService.class);
        restartServiceIntent.setPackage(context.getPackageName());
        restartServiceIntent.setFlags(Intent.FLAG_RECEIVER_FOREGROUND);

        PendingIntent restartServicePendingIntent =
                PendingIntent.getService(context, 1, restartServiceIntent,
                        PendingIntent.FLAG_ONE_SHOT);

        AlarmManager alarmService =
                (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 2000,
                restartServicePendingIntent);
    }

    public static String SHA256(String text) {
        String msg;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(text.getBytes());
            byte[] digest = md.digest();
            msg = Base64.encodeToString(digest, Base64.NO_WRAP);
            msg = msg.replace("\n", "");
        } catch (Exception e) {
            return null;
        }
        return msg;
    }

    public static String SHA128(String text) {
        String msg;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-128");
            md.update(text.getBytes());
            byte[] digest = md.digest();
            msg = Base64.encodeToString(digest, Base64.NO_WRAP);
            msg = msg.replace("\n", "");
        } catch (Exception e) {
            return null;
        }
        return msg;
    }

    public static ArrayList<StatusModel> getAllShownImagesPath(Context activity) {
        ArrayList<StatusModel> result = new ArrayList<>();
        ArrayList<String> addedPathList = new ArrayList<>();
        String currentUserId = SessionManager.getInstance(activity).getCurrentUserID();

        try {
            Uri uri;
            Cursor cursor;
            int column_index_data, column_index_folder_name;
            String absolutePath = null;
            uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
/*
            String[] projection = {MediaStore.MediaColumns.DATA,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME};
*/


            String[] projection = {
                    MediaStore.Files.FileColumns._ID,
                    MediaStore.Files.FileColumns.DATA,
                    MediaStore.Files.FileColumns.DATE_ADDED,
                    MediaStore.Files.FileColumns.MEDIA_TYPE,
                    MediaStore.Files.FileColumns.MIME_TYPE,
                    MediaStore.Files.FileColumns.TITLE
            };

// Return only video and image metadata.
            String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
                    + " OR "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

            Uri queryUri = MediaStore.Files.getContentUri("external");


            cursor = activity.getContentResolver().query(queryUri,
                    projection,
                    selection,
                    null, // Selection args (none).
                    MediaStore.Files.FileColumns.DATE_ADDED + " DESC" // Sort order.
            );
/*            cursor = activity.getContentResolver().query(uri, projection, null,
                    null,  orderBy + " DESC");*/

            if (cursor != null) {
                column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                int mediaTypeIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.MEDIA_TYPE);
                while (cursor.moveToNext()) {

                    StatusModel statusModel = new StatusModel();

                    statusModel.setUserId(currentUserId);
                    absolutePath = cursor.getString(column_index_data);
                    if (absolutePath != null && !addedPathList.contains(absolutePath)) {
                        statusModel.setLocalPath(absolutePath);
                        //media type
                        String mediaType = cursor.getString(mediaTypeIndex);
                        // media type == 1 --> image else --> video (we only fetch image & video's
                        if (mediaType != null && mediaType.equalsIgnoreCase("1")) {
                            statusModel.setImage(true);
                        } else {
                            statusModel.setVideo(true);
                        }
                        result.add(statusModel);
                        addedPathList.add(absolutePath);
                    }

                }
            }


        } catch (Exception e) {
            MyLog.e(TAG, "getAllShownImagesPath: ", e);
        }
        return result;
    }

    public static long getVideoDuration(String path, Context context) {
        long duration = 0L;
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//use one of overloaded setDataSource() functions to set your data source
            retriever.setDataSource(context, Uri.parse(new File(path).toString()));
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            duration = Long.parseLong(time);
            retriever.release();
        } catch (Exception e) {
            MyLog.e(TAG, "getVideoDuration: ", e);
        }

        return duration;
    }

    public static boolean isDeviceSupportCamera(Context context) {

        // this DEVICE has a camera
// no camera on this DEVICE
        return context != null && context.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA);
    }

    public static void refreshGalleryImage(String filePath, Context context) {
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(filePath))));
    }

    public static String getDateTimeByTimeStamp(Context context, long timeStamp) {
        String dateTime = "";
        try {

            Date today = TimeStampUtils.getDateFormat(Calendar.getInstance().getTimeInMillis());
            Date yesterday = TimeStampUtils.getYesterdayDate(today);

            Date currentItemDate = TimeStampUtils.getDateFormat(timeStamp);
            String time = TimeStampUtils.get12HrTimeFormat(context, String.valueOf(timeStamp));

            if (currentItemDate != null) {
                if (currentItemDate.equals(today)) {
                    dateTime = "Today ," + time;
                } else if (currentItemDate.equals(yesterday)) {

                    dateTime = "Yesterday ," + time;
                } else {
                    DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
                    String formatDate = df.format(currentItemDate);
                    dateTime = formatDate + " " + time;

                }
            }

        } catch (Exception e) {
            MyLog.e(TAG, "getDateTimeByTimeStamp: ", e);
        }
        return dateTime;
    }

    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);

        for (ActivityManager.RunningServiceInfo runningServiceInfo : services) {
            //  Log.d(Constants.TAG, String.format("Service:%s", runningServiceInfo.service.getClassName()));
            if (runningServiceInfo.service.getClassName().equals(serviceClass.getName())) {
                return true;
            }
        }
        return false;
    }

    public static String getStatusTime(Context context, long timeStamp, Boolean isMyStatus) {
        String dateTime = "";
        try {

            Date today = TimeStampUtils.getDateFormat(Calendar.getInstance().getTimeInMillis());
            Date yesterday = TimeStampUtils.getYesterdayDate(today);
            long serverDiff = SessionManager.getInstance(context).getServerTimeDifference();
            Date currentItemDate = null;
            long differnceMinutes = 0;
            long difference = 0;
            long deviceTS = 0;
            String time = null;
            if (isMyStatus) {
                deviceTS = timeStamp;
            } else {

                deviceTS = timeStamp + serverDiff;
            }
            //    Log.e(TAG, "getStatusTime: deviceTS "+deviceTS);
            currentItemDate = TimeStampUtils.getDateFormat(deviceTS);
            time = TimeStampUtils.get12HrTimeFormat(context, String.valueOf(deviceTS));
            long currentMillis = System.currentTimeMillis();
            //   difference = currentMillis - deviceTS;
//Check difference is minus
            //  long minutes = TimeUnit.MILLISECONDS.toMinutes(1000000);
            difference = System.currentTimeMillis() - deviceTS;
            if (difference < 0) {
                //Server time is
                // difference=difference+ Math.abs(-difference);
                deviceTS = timeStamp;
                currentItemDate = TimeStampUtils.getDateFormat(deviceTS);
                time = TimeStampUtils.get12HrTimeFormat(context, String.valueOf(deviceTS));
                difference = System.currentTimeMillis() - (deviceTS);
                // difference=serverDiff+difference;
            }
            differnceMinutes = TimeUnit.MILLISECONDS.toMinutes(difference);
            //  Log.e(TAG, "getStatusTime: differnceMinutes "+differnceMinutes);
            // Log.e(TAG, "getStatusTime: difference "+difference);

            if (currentItemDate != null) {
                if (differnceMinutes < 60) {
                    if (differnceMinutes == 0 || differnceMinutes == 1) {
                        dateTime = context.getString(R.string.just_now);
                    } else {
                        String timeInMinutes = null;
                        if (context.getResources().getBoolean(R.bool.is_arabic) && differnceMinutes == 2) {
                            timeInMinutes = "منذ دقيقتين";
                        } else {
                            timeInMinutes = differnceMinutes + context.getString(R.string.minutes_age);
                        }

                        MyLog.d(TAG, "getStatusTime: " + timeInMinutes);
                        dateTime = timeInMinutes;
                    }
                } else if (currentItemDate.equals(today)) {
                    dateTime = "Today ," + time;
                } else if (currentItemDate.equals(yesterday)) {
                    dateTime = "Yesterday ," + time;
                } else {
                    DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
                    String formatDate = df.format(currentItemDate);
                    dateTime = formatDate + " " + time;

                }
            }

        } catch (Exception e) {
            //    Log.e(TAG, "getDateTimeByTimeStamp: ", e);
        }
        return dateTime;
    }

    public static int setCameraDisplayOrientation(Activity activity,
                                                  int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }

        camera.setDisplayOrientation(result);

        return result;
    }

    public static void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(600);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }
/*


        public static String getStatusTime(Context context, long timeStamp) {
                String dateTime = "";
                try {

                        Date today = TimeStampUtils.getDateFormat(Calendar.getInstance().getTimeInMillis());
                        Date yesterday = TimeStampUtils.getYesterdayDate(today);

                        Date currentItemDate = TimeStampUtils.getDateFormat(timeStamp);
                        String time = TimeStampUtils.get12HrTimeFormat(context, String.valueOf(timeStamp));
                        long currentMillis = System.currentTimeMillis();
                        long difference = currentMillis - timeStamp;
                        long differnceMinutes = TimeUnit.MILLISECONDS.toMinutes(difference);
                        Log.d(TAG, "getStatusTime: differnceMinutes " + differnceMinutes);
                        if (currentItemDate != null) {
                                if (differnceMinutes < 60) {
                                        if (differnceMinutes == 0 || differnceMinutes == 1) {
                                                dateTime = "Just now";
                                        } else {
                                                String timeInMinutes = differnceMinutes + " minutes ago";
                                                Log.d(TAG, "getStatusTime: " + timeInMinutes);
                                                dateTime = timeInMinutes;
                                        }
                                } else if (currentItemDate.equals(today)) {
                                        dateTime = "Today ," + time;
                                } else if (currentItemDate.equals(yesterday)) {
                                        dateTime = "Yesterday ," + time;
                                } else {
                                        DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
                                        String formatDate = df.format(currentItemDate);
                                        dateTime = formatDate + " " + time;

                                }
                        }

                } catch (Exception e) {
                        Log.e(TAG, "getDateTimeByTimeStamp: ", e);
                }
                return dateTime;
        }
*/

    // slide the view from its current position to below itself
    public static void slideDown(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(600);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        try {
            ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "isMyServiceRunning: ", e);
        }
        return false;
    }

    public static boolean DownloadFileService(Context context, Class<?> serviceClass) {
        try {
            ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "isMyServiceRunning: ", e);
        }
        return false;
    }

//------------------------------------Service Check-----------------------------------------

    private String getDeviceInfo(Context context) {
        Locale current = context.getResources().getConfiguration().locale;
        MyLog.d("Deviceinfo_country", current.getCountry());
        MyLog.d("Deviceinfo_code", current.getLanguage());

        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String carrierName = manager.getNetworkOperatorName();
        MyLog.d("Deviceinfo_opera", carrierName + "-test");


        for (DeviceInfo.Device data : DeviceInfo.Device.values()) {
            MyLog.d("Deviceinfo_" + data, DeviceInfo.getDeviceInfo(context, data));
        }

        MyLog.d("StorageTT_interfree", FileUploadDownloadManager.getAvailableInternalMemorySize());
        MyLog.d("StorageTT_intertot", FileUploadDownloadManager.getTotalInternalMemorySize());
        MyLog.d("StorageTT_extfree", FileUploadDownloadManager.getAvailableExternalMemorySize());
        MyLog.d("StorageTT_exttot", FileUploadDownloadManager.getTotalExternalMemorySize());

        MyLog.d("DeviceBuildInfo_produ", Build.PRODUCT);
        MyLog.d("DeviceBuildInfo_device", Build.DEVICE);
        MyLog.d("DeviceBuildInfo_release", Build.VERSION.RELEASE);
        MyLog.d("DeviceBuildInfo_board", Build.BOARD);
        MyLog.d("DeviceBuildInfo_arch", System.getProperty("os.arch"));
        MyLog.d("DeviceBuildInfo_kernal", System.getProperty("os.version"));

        MyLog.d("RoamStauts", "" + About_contactus.isCallRoamActivated(context));

        return "";
    }

    private boolean isNetworkConnected(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


}
