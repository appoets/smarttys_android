package com.app.Smarttys.app.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.app.Smarttys.core.SessionManager;

import java.util.Calendar;

/**
 * Created by CAS60 on 11/10/2017.
 */
public class AccountCompleteReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        SessionManager.getInstance(context).setAccountSyncCompletedTS(Calendar.getInstance().getTimeInMillis());
        MyLog.d("Myaccountcompleted", getClass().getSimpleName());
    }
}
