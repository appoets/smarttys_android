package com.app.Smarttys.app.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.WebviewModule.PolicyWebview;
import com.app.Smarttys.app.adapter.AboutHelpAdapter;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.ActivityLauncher;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.service.Constants;

public class AboutHelp extends CoreActivity {
    /**
     * Called when the activity is first created.
     */
    private static final String TAG = "AboutHelp";
    ListView lview;
    AboutHelpAdapter lviewAdapter;
    ImageView backnavigate;
    Context mContext;
    /*   private final static String value[] = {"FAQ", "Contact Us", "System Status", "Terms and Privacy Policy", "About",
       };
   */
  /*  private final static String subvalue[] = {"", "Questions? Need Help?", "",
            "", ""};
*/
    private String[] value = null, subvalue = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_help);
        setTitle(getString(R.string.about_help));
        //getSupportActionBar().hide();

        mContext = AboutHelp.this;

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
     /*   backnavigate = (ImageView) findViewById(R.id.backnavigate);
        backnavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
   */
        if (CoreController.isRaad) {
            value = new String[]{mContext.getResources().getString(R.string.systemstatus), mContext.getResources().getString(R.string.about)};
            subvalue = new String[]{"", ""};
        } else {
            value = new String[]{mContext.getResources().getString(R.string.faq), mContext.getResources().getString(R.string.contactus), mContext.getResources().getString(R.string.systemstatus), mContext.getResources().getString(R.string.termspolicy), mContext.getResources().getString(R.string.about),};
            subvalue = new String[]{"", mContext.getResources().getString(R.string.questionsneed), "", "", ""};
        }


        lview = findViewById(R.id.listViewHelp);
        lviewAdapter = new AboutHelpAdapter(this, value, subvalue);
        lview.setAdapter(lviewAdapter);

        lview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            // Intent intent = null;

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                if (CoreController.isRaad) {
                    switch (position) {
                        case 0:
                            ActivityLauncher.launchSystemstatus(AboutHelp.this);
                            break;
                        case 1:
                            ActivityLauncher.launchAboutnew(AboutHelp.this);
                            break;
                    }
                } else {
                    switch (position) {
                        case 0:
                            Uri uri = Uri.parse(Constants.FAQ);
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                            break;
                        case 1:
                            MyLog.e("enter", "true");
                            //System.out.println("am in contactus");
                            ActivityLauncher.launchAbout_contactus(AboutHelp.this);
                            break;
                        case 2:
                            ActivityLauncher.launchSystemstatus(AboutHelp.this);
                            break;
                        case 4:
                            ActivityLauncher.launchAboutnew(AboutHelp.this);
                            break;
                        case 3:

                            Intent web = new Intent(AboutHelp.this, PolicyWebview.class);
                            web.putExtra("web_url", Constants.pricvacy_policy);
                            startActivity(web);
                            break;

//                            Uri uriterms = Uri.parse(Constants.pricvacy_policy); // missing 'http://' will cause crashed
//                            Intent intentterms = new Intent(Intent.ACTION_VIEW, uriterms);
//                            startActivity(intentterms);
                    }
                }

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
