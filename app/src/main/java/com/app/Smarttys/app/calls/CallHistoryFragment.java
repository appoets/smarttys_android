package com.app.Smarttys.app.calls;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.SmarttySettings;
import com.app.Smarttys.app.dialog.CustomAlertDialog;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.BlockUserUtils;
import com.app.Smarttys.app.utils.ConnectivityInfo;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.RecyclerViewItemClickListener;
import com.app.Smarttys.app.utils.TimeStampUtils;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.IncomingMessage;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.CallItemChat;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by CAS60 on 7/28/2017.
 */
public class CallHistoryFragment extends Fragment implements View.OnClickListener, RecyclerViewItemClickListener {
    private static final String TAG = CallHistoryFragment.class.getSimpleName() + ">>>@@";
    private final int AUDIO_RECORD_PERMISSION_REQUEST_CODE = 14;
    private final int REQUEST_CODE_REMOVE_CALL_LOG = 1;
    private View view;
    private RecyclerView rvCalls;
    private ImageButton ibNewCall;
    private TextView tvNoCalls;
    private SearchView searchView;
    private Getcontactname getcontactname;
    private CallHistoryAdapter adapter;
    private ArrayList<CallItemChat> callsList;
    private String mCurrentUserId, unBlockedUserId = "";
    private boolean needRefresh;
    private Gson gson;
    private CoreActivity mActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MyLog.d(TAG, "onCreateView: performance 1");
        view = inflater.inflate(R.layout.activity_call_history, container, false);
        MyLog.d(TAG, "onCreateView: 2");
        MyLog.d(TAG, "onCreateView: 3");
        initData();
        MyLog.d(TAG, "onCreateView: performance 4");
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();


    }

    private void initView() {
        rvCalls = view.findViewById(R.id.rvCalls);
        ibNewCall = view.findViewById(R.id.ibNewCall);
        tvNoCalls = view.findViewById(R.id.tvNoCalls);

        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        rvCalls.setLayoutManager(manager);

        mActivity = ((CoreActivity) getActivity());
        mActivity.initProgress(getString(R.string.loading_in), true);

        ibNewCall.setOnClickListener(this);

        view.findViewById(R.id.btnCall).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CallsActivity.isStarted) {
                    CallMessage.resumeCall(getActivity());
                }
            }
        });

        setHasOptionsMenu(true);
    }

    private void initData() {
        mCurrentUserId = SessionManager.getInstance(getActivity()).getCurrentUserID();

        getcontactname = new Getcontactname(getActivity());

        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();


    }

    private void loadCallHistoryFromDB() {

        try {

            if (getActivity() != null) {
                MessageDbController db = CoreController.getDBInstance(getActivity());
                callsList = db.selectAllCalls(mCurrentUserId);
                callsList = removeInvalidTs();
                Collections.sort(callsList, new CallsListSorter());
                callsList = removeArrivedStatusItems();
                callsList = filterCallsList();
                rvCalls.post(new Runnable() {
                    public void run() {
                        // UI code goes here
                        if (callsList.size() == 0) {
                            tvNoCalls.setVisibility(View.VISIBLE);
                        } else {
                            tvNoCalls.setVisibility(View.GONE);
                        }

                /*    for(int i=0;i<callsList.size();i++){
                    String current_user_phone=SessionManager.getInstance(getActivity()).getPhoneNumberOfCurrentUser();
                    String CallerName=callsList.get(i).getCallerName();
                        if(current_user_phone.equalsIgnoreCase(CallerName)){
                            CallItemChat callItem = callsList.get(i);
                            callsList.remove(callItem);
                        }
                    }*/

                        adapter = new CallHistoryAdapter(getActivity(), callsList);
                        adapter.setOnItemClickListener(CallHistoryFragment.this);
                        rvCalls.setAdapter(adapter);
                    }
                });

            }

        } catch (Exception e) {

            MyLog.e(TAG, "loadCallHistoryFromDB: ", e);
        }

    }

    private ArrayList<CallItemChat> removeArrivedStatusItems() {
        ArrayList<CallItemChat> removedItems = new ArrayList<>();
        for (int i = 0; i < callsList.size(); i++) {
            CallItemChat callItem = callsList.get(i);
            if (!callItem.getCallStatus().equals(MessageFactory.CALL_STATUS_ARRIVED + "")) {
                removedItems.add(callItem);
            }
        }
        return removedItems;
    }

    private ArrayList<CallItemChat> removeInvalidTs() {
        ArrayList<CallItemChat> removedItems = new ArrayList<>();
        for (int i = 0; i < callsList.size(); i++) {
            CallItemChat callItem = callsList.get(i);
            try {
                long item1Time = Long.parseLong(callItem.getTS());
                //Log.d(TAG, "removeInvalidTs: "+item1Time);
                removedItems.add(callItem);
            } catch (Exception e) {
            }

        }
        return removedItems;
    }

    private ArrayList<CallItemChat> filterCallsList() {
        ArrayList<CallItemChat> tempList = new ArrayList<>();

        CallItemChat prevItem = null;
        for (int i = 0; i < callsList.size(); i++) {

            CallItemChat currentItem = callsList.get(i);
            if (prevItem != null && prevItem.isSelf() == currentItem.isSelf()
                    && prevItem.getOpponentUserId().equalsIgnoreCase(currentItem.getOpponentUserId())
                    && prevItem.getCallType().equals(currentItem.getCallType())
                    && getCallStatus(prevItem, currentItem) && isDateEqual(prevItem, currentItem)) {
                tempList.add(currentItem);
            } else {
                prevItem = currentItem;
                callsList.set(i, prevItem);
            }
            prevItem = mergeCallItems(prevItem, currentItem);
        }

        for (int i = 0; i < tempList.size(); i++) {
            callsList.remove(tempList.get(i));
        }

        return callsList;
    }

    private boolean isDateEqual(CallItemChat prevItem, CallItemChat currentItem) {
        Date prevItemDate = TimeStampUtils.getMessageTStoDate(getActivity(), prevItem.getTS());
        Date currentItemDate = TimeStampUtils.getMessageTStoDate(getActivity(), currentItem.getTS());
        return prevItemDate != null && currentItemDate != null && prevItemDate.equals(currentItemDate);
    }

    private CallItemChat mergeCallItems(CallItemChat prevItem, CallItemChat currentItem) {
        try {
            String callAtObj = prevItem.getCalledAtObj();

            // Add call details from current item to old item
            JSONArray arrTimes;
            if (callAtObj == null || callAtObj.equals("")) {
                arrTimes = new JSONArray();
            } else {
                arrTimes = new JSONArray(callAtObj);
            }
            String strObj = gson.toJson(currentItem);
            arrTimes.put(strObj);

            prevItem.setCallCount(arrTimes.length());
            prevItem.setCalledAtObj(arrTimes.toString());
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        return prevItem;
    }

    private boolean getCallStatus(CallItemChat prevItem, CallItemChat currentItem) {
        if (prevItem.isSelf() && currentItem.isSelf()) {
            return true;
        } else {
            boolean con1 = prevItem.getCallStatus().equals(MessageFactory.CALL_STATUS_REJECTED + "");
            boolean con2 = prevItem.getCallStatus().equals(MessageFactory.CALL_STATUS_MISSED + "");
            boolean con3 = con1 || con2;

            boolean con4 = currentItem.getCallStatus().equals(MessageFactory.CALL_STATUS_REJECTED + "");
            boolean con5 = currentItem.getCallStatus().equals(MessageFactory.CALL_STATUS_MISSED + "");
            boolean con6 = con4 || con5;

            return con3 == con6;
        }
    }

    public void refreshCallsList() {
        new loadCallHistoryFromDBAsyntask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ibNewCall: {
                Intent intent = new Intent(getActivity(), CallsContactActivity.class);
                startActivity(intent);
                needRefresh = true;
            }
            break;
        }
    }

    @Override
    public void onRVItemClick(View parentView, int position) {

        switch (parentView.getId()) {

            case R.id.ibCall: {
                MyLog.d(TAG, "on call icon Click: ");
                CallItemChat callItem;
                try {
                    callItem = (CallItemChat) parentView.getTag(R.string.data);
                } catch (Exception e) {
                    callItem = callsList.get(position);
                }
                String toUserId = callItem.getOpponentUserId();
                String receiverName = getcontactname.getSendername(toUserId, callItem.getOpponentUserMsisdn());

                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(getActivity());

                if (contactDB_sqlite.getBlockedStatus(toUserId, false).equals("1")) {
                    String msg = "Unblock " + receiverName + " to place a " +
                            getString(R.string.app_name) + " call";
                    displayAlert(msg, toUserId);
                } else {
                    //performCall(callItem);
                    toUserId = callItem.getOpponentUserId();
                    String receiverMsisdn = callItem.getOpponentUserMsisdn();
                    new CallUtil().performCall(getActivity(), isVideoCall(callItem), toUserId, receiverMsisdn);
                }
            }
            break;

            case R.id.rlParent: {
                CallItemChat callItem;
                try {
                    callItem = (CallItemChat) parentView.getTag(R.string.data);
                } catch (Exception e) {
                    callItem = callsList.get(position);
                }
                Intent intent = new Intent(getActivity(), CallInfoActivity.class);
                intent.putExtra(CallInfoActivity.KEY_CALL_ITEM, callItem);
                startActivityForResult(intent, REQUEST_CODE_REMOVE_CALL_LOG);
            }
            break;

        }
    }

    private boolean isVideoCall(CallItemChat callItem) {
        boolean isVideoCall = false;
        if (!callItem.getCallType().isEmpty() && callItem.getCallType() != null) {
            if (Integer.parseInt(callItem.getCallType()) == MessageFactory.video_call) {
                isVideoCall = true;
            }
        } else {
            isVideoCall = false;
        }

        return isVideoCall;
    }

    private void performCall(CallItemChat callItem) {
        MyLog.d(TAG, "performCall: ");
        if (AppUtils.isNetworkAvailable(getActivity())) {
            if (checkAudioRecordPermission()) {
                if (!CallMessage.isAlreadyCallClick) {

                    String toUserId = callItem.getOpponentUserId();
                    CallMessage message = new CallMessage(getActivity());
                    int callType = 0;
                    try {
                        if (!callItem.getCallType().isEmpty() && callItem.getCallType() != null) {
                            callType = 0;
                        } else {
                            callType = Integer.parseInt(callItem.getCallType());
                        }
                        //  callType=Integer.parseInt(callItem.getCallType());
                    } catch (Exception e) {
                        MyLog.e(TAG, "performCall: ", e);
                    }

                    JSONObject object = (JSONObject) message.getMessageObject(toUserId, callType);

                    if (object != null) {
                        SendMessageEvent callEvent = new SendMessageEvent();
                        callEvent.setEventName(SocketManager.EVENT_CALL);
                        callEvent.setMessageObject(object);
                        EventBus.getDefault().post(callEvent);
                    }

                    CallMessage.setCallClickTimeout();
                    //open call page immediately
                    boolean isVideoCall = false;
                    if (callItem.getCallType().equals(MessageFactory.video_call + "")) {
                        isVideoCall = true;
                    }
                    try {
                        String to = object.getString("to");
                        String toUserAvatar = "";

                        MessageDbController db = CoreController.getDBInstance(getActivity());
                        db.updateCallLogs(callItem);

                        String ts = "";
                        CallMessage.openCallScreen(getActivity(), mCurrentUserId, to, callItem.getCallId(),
                                callItem.getRecordId(), toUserAvatar, callItem.getOpponentUserMsisdn(),
                                MessageFactory.CALL_IN_FREE + "", isVideoCall, true, ts, "");
                    } catch (Exception e) {
                        MyLog.e(TAG, "performCall: ", e);
                    }
                } else {
                    Toast.makeText(getActivity(), "Call in progress", Toast.LENGTH_SHORT).show();
                }
            } else {
                requestAudioRecordPermission();
            }

            MyLog.d(TAG, "performCall: end");
        } else {
            Toast.makeText(getActivity(), getString(R.string.check_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }

    private void requestAudioRecordPermission() {
        ActivityCompat.requestPermissions(getActivity(), new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, AUDIO_RECORD_PERMISSION_REQUEST_CODE);
    }

    public boolean checkAudioRecordPermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getActivity(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void loadCallResMessage(ReceviceMessageEvent event) {
        Object[] obj = event.getObjectsArray();
        try {
            MyLog.d(TAG, "loadCallResMessage: ");
            JSONObject object = new JSONObject(obj[0].toString());
            JSONObject callObj = object.getJSONObject("data");

            String from = callObj.getString("from");
            String callStatus = callObj.getString("call_status");
            if (from.equalsIgnoreCase(mCurrentUserId) && callStatus.equals(MessageFactory.CALL_STATUS_CALLING + "")) {
                String to = callObj.getString("to");

                IncomingMessage incomingMsg = new IncomingMessage(getActivity());
                CallItemChat callItem = incomingMsg.loadOutgoingCall(callObj);

                boolean isVideoCall = false;
                if (callItem.getCallType().equals(MessageFactory.video_call + "")) {
                    isVideoCall = true;
                }

                String toUserAvatar = callObj.getString("To_avatar") + "?=id" + Calendar.getInstance().getTimeInMillis();

                MessageDbController db = CoreController.getDBInstance(getActivity());
                db.updateCallLogs(callItem);

                String ts = callObj.getString("timestamp");

                CallMessage.openCallScreen(getActivity(), mCurrentUserId, to, callItem.getCallId(),
                        callItem.getRecordId(), toUserAvatar, callItem.getOpponentUserMsisdn(),
                        MessageFactory.CALL_IN_FREE + "", isVideoCall, true, ts, "");
                needRefresh = true;
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        switch (event.getEventName()) {

            case SocketManager.EVENT_CALL_RESPONSE:
                loadCallResMessage(event);
                break;

            case SocketManager.EVENT_BLOCK_USER:
                loadBlockEventMessage(event);
                break;

            case SocketManager.EVENT_REMOVE_ALL_CALLS:
                loadRemoveCallHistory(event);
                break;

            case SocketManager.EVENT_CALL:
                loadIncomingCallMessage(event);
                break;
        }

    }

    private void loadIncomingCallMessage(ReceviceMessageEvent event) {
        try {
            String data = event.getObjectsArray()[0].toString();
            JSONObject callObj = new JSONObject(data);

            String to = callObj.getString("to");
            if (to.equalsIgnoreCase(mCurrentUserId)) {
                needRefresh = true;
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadRemoveCallHistory(ReceviceMessageEvent event) {

        String data = event.getObjectsArray()[0].toString();

        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");

            if (from.equalsIgnoreCase(mCurrentUserId)) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.hideProgressDialog();
                        loadCallHistoryFromDB();
                    }
                }, 1000);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadBlockEventMessage(ReceviceMessageEvent event) {

        try {
            Object[] obj = event.getObjectsArray();
            JSONObject object = new JSONObject(obj[0].toString());

            String status = object.getString("status");
            String to = object.getString("to");
            String from = object.getString("from");

            if (mCurrentUserId.equalsIgnoreCase(from) && to.equalsIgnoreCase(unBlockedUserId)) {
                mActivity.hideProgressDialog();

                if (status.equalsIgnoreCase("1")) {
                    Toast.makeText(getActivity(), "Number is blocked", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Number is Unblocked", Toast.LENGTH_SHORT).show();
                }
            }

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void displayAlert(String msg, final String toUserId) {
        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(msg);
        dialog.setNegativeButtonText("Cancel");
        dialog.setPositiveButtonText("Unblock");
        dialog.setCancelable(false);
        unBlockedUserId = toUserId;

        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                mActivity.hideProgressDialog();

                BlockUserUtils.changeUserBlockedStatus(getActivity(), EventBus.getDefault(),
                        mCurrentUserId, toUserId, false);
                dialog.dismiss();
            }

            @Override

            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getFragmentManager(), "UnBlock a person");

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        checkAudioRecordPermission();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_REMOVE_CALL_LOG) {
            loadCallHistoryFromDB();
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.activity_call_history, menu);

        MenuItem searchItem = menu.findItem(R.id.menuSearch);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.setIconifiedByDefault(true);
                searchView.setIconified(true);
                searchView.setQuery("", false);
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.equals("") && newText.isEmpty()) {
                    searchView.clearFocus();
                }
                if (adapter != null) {

                    adapter.getFilter().filter(newText);
                }

                return false;
            }
        });

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView.setIconifiedByDefault(true);
        searchView.setQuery("", false);
        searchView.clearFocus();
        searchView.setIconified(true);

        AutoCompleteTextView searchTextView = searchView.findViewById(R.id.search_src_text);
        searchTextView.setTextColor(Color.WHITE);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, 0); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.menuClearCallLog: {
                if (ConnectivityInfo.isInternetConnected(getActivity())) {
                    if (callsList != null && callsList.size() > 0) {
                        performClearCallLogs();
                    }
                } else {
                    Toast.makeText(getActivity(), "Check your internet connection", Toast.LENGTH_SHORT).show();
                }
            }
            break;

            case R.id.menuSettings: {
                Intent intent = new Intent(getActivity(), SmarttySettings.class);
                startActivity(intent);
            }
            break;

        }

        return super.onOptionsItemSelected(item);
    }

    private void performClearCallLogs() {
        mActivity.showProgressDialog();

        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_REMOVE_ALL_CALLS);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MyLog.d(TAG, "onResume: performance 1");
        //if (needRefresh) {
        needRefresh = false;
        //}


        refreshCallsList();
        MyLog.d(TAG, "onResume: performance 2");
    }

    private class loadCallHistoryFromDBAsyntask extends AsyncTask<String, Void, String> {
        public loadCallHistoryFromDBAsyntask() {
        }

        @Override
        protected String doInBackground(String... URL) {
            try {
                loadCallHistoryFromDB();
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
        }
    }

    private class CallsListSorter implements Comparator<CallItemChat> {

        @Override
        public int compare(CallItemChat item1, CallItemChat item2) {
            try {
                long item1Time = Long.parseLong(item1.getTS());
                long item2Time = Long.parseLong(item2.getTS());

                if (item1Time < item2Time) {
                    return 1;
                } else {
                    return -1;
                }
            } catch (Exception e) {
                return -1;
            }
        }

    }
}
