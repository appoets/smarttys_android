package com.app.Smarttys.app.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.ForwardContactAdapter;
import com.app.Smarttys.app.adapter.RItemAdapter;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.message.PictureMessage;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 */
public class Forward_avater extends CoreActivity {
    private static final String TAG = "Forward_avater";
    RecyclerView lvContacts;
    ForwardContactAdapter adapter;
    InputMethodManager inputMethodManager;
    ProgressDialog dialog;
    AvnNextLTProRegTextView resevernameforward;
    ImageView sendmessage;
    Uri imagefromvendor;
    RelativeLayout Sendlayout;
    String mCurrentUserId, textMsgFromVendor;
    List<MessageItemChat> aSelectedMessageInfo;
    Uri uri;
    private SessionManager sessionManager;
    private List<SmarttyContactModel> selectedContactsList;
    private FileUploadDownloadManager uploadDownloadManager;
    private SearchView searchView;
    private boolean forwardFromSmartty;
    private String contact;
    private ArrayList<SmarttyContactModel> smarttyEntries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forward_contact);

        sendmessage = findViewById(R.id.overlapImage);
        resevernameforward = findViewById(R.id.chat_text_view);
        uploadDownloadManager = new FileUploadDownloadManager(Forward_avater.this);
        mCurrentUserId = SessionManager.getInstance(this).getCurrentUserID();
        setTitle("Forward to..");
        lvContacts = findViewById(R.id.listContacts);
        LinearLayoutManager mediaManager = new LinearLayoutManager(Forward_avater.this, LinearLayoutManager.VERTICAL, false);
        lvContacts.setLayoutManager(mediaManager);

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        smarttyEntries = contactDB_sqlite.getSavedSmarttyContacts();

        if (smarttyEntries == null) {
            syncContacts();
        } else {
            Collections.sort(smarttyEntries, Getcontactname.nameAscComparator);
            adapter = new ForwardContactAdapter(Forward_avater.this, smarttyEntries);
            lvContacts.setAdapter(adapter);
        }

        selectedContactsList = new ArrayList<>();

        lvContacts.addOnItemTouchListener(new RItemAdapter(this, lvContacts, new RItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                SmarttyContactModel userData = smarttyEntries.get(position);
                userData.setSelected(!userData.isSelected());

//        if(!userData.getNumberInDevice().startsWith("+")) {
//            String formattedNumber = PhoneNumberUtils.formatNumber("+" + userData.getNumberInDevice(), "");
//            Log.d("formattedNumber", formattedNumber);
//        } else {
//            String formattedNumber = PhoneNumberUtils.formatNumber(userData.getNumberInDevice(), "");
//            Log.d("formattedNumber", formattedNumber);
//        }

                if (userData.isSelected()) {
                    selectedContactsList.add(userData);
                } else {
                    selectedContactsList.remove(userData);
                }

                smarttyEntries.set(position, userData);
                adapter.notifyDataSetChanged();

                if (selectedContactsList.size() == 0) {
                    Sendlayout.setVisibility(View.GONE);
                    sendmessage.setVisibility(View.GONE);
                } else {

                    Sendlayout.setVisibility(View.VISIBLE);
                    sendmessage.setVisibility(View.VISIBLE);

                    Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                            R.anim.bottom_up);
                    Sendlayout.setAnimation(animation);

                    StringBuilder sb = new StringBuilder();
                    int nameIndex = 0;
                    for (SmarttyContactModel contact : selectedContactsList) {
                        sb.append(contact.getFirstName());
                        nameIndex++;
                        if (selectedContactsList.size() > nameIndex) {
                            sb.append(",");
                        }
                    }

                    resevernameforward.setText(sb);

                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        final Intent intent = getIntent();

        String shareAction = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(shareAction) && type != null) {
            if ("text/plain".equals(type)) {
                textMsgFromVendor = intent.getExtras().getString(Intent.EXTRA_TEXT, "");
                uri = (Uri) intent.getExtras().get(Intent.EXTRA_STREAM);
            }
        }

        Sendlayout = findViewById(R.id.sendlayout);

        forwardFromSmartty = getIntent().getBooleanExtra("FromSmartty", false);

        if (getIntent() != null) {
            aSelectedMessageInfo = (List<MessageItemChat>) intent.getSerializableExtra("MsgItemList");
        }
        sendmessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MessageDbController db = CoreController.getDBInstance(Forward_avater.this);

                String path = getFilePath(uri);
                if (path != null) {
                    File imageFile = new File(path);
                    //  String path = uri.getPath();// "file:///mnt/sdcard/FileName.mp3"

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imageFile.toString(), options);
                    int imageHeight = options.outHeight;
                    int imageWidth = options.outWidth;

                    for (int contactIndex = 0; contactIndex < selectedContactsList.size(); contactIndex++) {
                        SmarttyContactModel userData = selectedContactsList.get(contactIndex);

                        PictureMessage message = (PictureMessage) MessageFactory.getMessage(MessageFactory.picture, Forward_avater.this);
                        message.getMessageObject(userData.get_id(), imageFile.toString(), false);

                        MessageItemChat item = message.createMessageItem(true, "", imageFile.toString(), MessageFactory.DELIVERY_STATUS_NOT_SENT,
                                userData.get_id(), userData.getFirstName(), imageWidth, imageHeight);

                        String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(imageFile.toString());
                        String imgName = item.getMessageId() + fileExtension;
                        String docId = mCurrentUserId + "-" + userData.get_id();

                        JSONObject uploadObj = (JSONObject) message.createImageUploadObject(item.getMessageId(), docId,
                                imgName, imageFile.toString(), userData.getFirstName(), "", MessageFactory.CHAT_TYPE_SINGLE, false);
                        uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);

                        item.setSenderMsisdn(userData.getNumberInDevice());
                        item.setSenderName(userData.getFirstName());

                        db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SINGLE);
                    }

                    if (selectedContactsList.size() == 1) {
                        Intent intent = new Intent(Forward_avater.this, ChatPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                        SmarttyContactModel userData = selectedContactsList.get(0);
                        intent.putExtra("receiverId", userData.get_id());
                        intent.putExtra("documentId", userData.get_id());
                        intent.putExtra("receiverName", userData.getFirstName());
                        intent.putExtra("Username", userData.getFirstName());
                        intent.putExtra("Image", userData.getAvatarImageUrl());
                        intent.putExtra("type", 0);
                        intent.putExtra("msisdn", userData.getNumberInDevice());
                        startActivity(intent);

                    }

                    finish();
                } else {
                    Toast.makeText(Forward_avater.this, "Please try again", Toast.LENGTH_SHORT).show();
                }
            }
        });
        /* Variables for serch */
        sessionManager = SessionManager.getInstance(this);
    }

    private void syncContacts() {
        contactsFromCursor();
    }

    private void contactsFromCursor() {
        SmarttyContactsService.contactEntries = new ArrayList<>();
        showProgressDialog();

        Uri contactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI; // The content URI of the phone contacts
        String[] projection = {                                  // The columns to return for each row
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER
        };
        String selection = null;                                 //Selection criteria
        String[] selectionArgs = {};                             //Selection criteria
        String sortOrder = null;                                 //The sort order for the returned rows

        Cursor cursor = getContentResolver().query(contactsUri, projection, selection, selectionArgs, sortOrder);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            JSONArray arrContacts = new JSONArray();
            do {
                SmarttyContactModel d = new SmarttyContactModel();
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                d.setFirstName(name);
//                contact += phNumber.trim() + ",";

                try {
                    String phNo = phNumber.replace(" ", "").replace("(", "").replace(")", "").replace("-", "");
                    d.setNumberInDevice(phNo);
                    Log.e("Forwardavatar", "setNumberInDevice" + phNo);

                    JSONObject contactObj = new JSONObject();
                    contactObj.put("Phno", phNo);
                    contactObj.put("Name", name);
                    arrContacts.put(contactObj);
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }

                SmarttyContactsService.contactEntries.add(d);

            } while (cursor.moveToNext());
            contact = arrContacts.toString();
        }
        updateDataToTheServer();
    }

    private void updateDataToTheServer() {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_GET_FAVORITE);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("indexAt", "0");
            jsonObject.put("msisdn", SessionManager.getInstance(this).getPhoneNumberOfCurrentUser());
            jsonObject.put("Contacts", contact);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        messageEvent.setMessageObject(jsonObject);
        EventBus.getDefault().post(messageEvent);
    }

    private void storeContact(ReceviceMessageEvent event) throws JSONException {
        Object[] args = event.getObjectsArray();
        JSONObject data = new JSONObject(args[0].toString());
        JSONArray array = data.getJSONArray("Favorites");

        smarttyEntries = new ArrayList<>();

        for (int contactIndex = 0; contactIndex < SmarttyContactsService.contactEntries.size(); contactIndex++) {
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = new JSONObject(array.get(i).toString());
                String msisdn = obj.getString("msisdn");
                String id = obj.getString("_id");
                String profilePic = obj.getString("ProfilePic");

                SmarttyContactModel entry = SmarttyContactsService.contactEntries.get(contactIndex);
                if (msisdn.contains(entry.getNumberInDevice())) {
                    smarttyEntries.add(entry);

                }
            }
        }

        smarttyEntries = new ArrayList<>();
        for (SmarttyContactModel contact : smarttyEntries) {
            contact.setSelected(false);
            smarttyEntries.add(contact);
        }

        Collections.sort(smarttyEntries, Getcontactname.nameAscComparator);
        adapter = new ForwardContactAdapter(Forward_avater.this, smarttyEntries);
        lvContacts.setAdapter(adapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (SocketManager.EVENT_GET_CONTACTS.equalsIgnoreCase(event.getEventName())) {
            try {
                storeContact(event);
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } else if (SocketManager.EVENT_GET_CONTACTS.equalsIgnoreCase(event.getEventName())) {
            try {
                storeContact(event);
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private String getFilePath(Uri contentUri) {
        String path = null;
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(contentUri, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            path = cursor.getString(columnIndex);
            cursor.close();

            if (path == null) {
                path = getRealFilePath(contentUri);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

        return path;
    }

    private String getRealFilePath(Uri contentUri) {
        String wholeID = DocumentsContract.getDocumentId(contentUri);
        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];
        String[] column = {MediaStore.Images.Media.DATA};
        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = getContentResolver().
                query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);
        String filePath = "";
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_forward_contact, menu);
        MenuItem searchItem = menu.findItem(R.id.chats_searchIcon);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.setIconifiedByDefault(true);
                searchView.setIconified(true);
                searchView.setQuery("", false);
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.equals("") && newText.isEmpty()) {
                    searchView.clearFocus();
                }
//                adapter.getFilter().filter(newText);
                return false;
            }
        });

        searchView.setIconifiedByDefault(true);
        searchView.setQuery("", false);
        searchView.clearFocus();
        searchView.setIconified(true);

        AutoCompleteTextView searchTextView = searchView.findViewById(R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, 0); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
