package com.app.Smarttys.app.encryption;

import android.content.Context;

import com.app.Smarttys.app.utils.CryptLib;
import com.app.Smarttys.app.utils.MyLog;


/**
 * Created by user134 on 6/19/2018.
 */

public class AESHelper {
    private static final String TAG = "EncryptionUtils";

    public static String simpleMsgEncrypt(String msg, String token, Context context) {
        String originalMsg = msg;
        try {
            String iv = "chat";
            String encodedToken = token;
            encodedToken = encodedToken.replace("\n", "");
            //userToken=convertHexToBase64String(userToken);
            MyLog.d(TAG, "simpleMsgEncrypt: " + encodedToken);
            CryptLib cryptLib = new CryptLib();
            return cryptLib.encryptSimple(msg, encodedToken, iv);


        } catch (Exception e) {
            MyLog.e(TAG, "getScMessageResponseDecrypted: ", e);
        }
        return originalMsg;
    }


    public static String simpleMsgDecrypt(String msg, String token, Context context) {
        String originalMsg = msg;
        try {
            String iv = "chat";
            String encodedToken = token;
            encodedToken = encodedToken.replace("\n", "");
            MyLog.d(TAG, "getScMessageResponseDecrypted: " + encodedToken);
            CryptLib cryptLib = new CryptLib();
            return cryptLib.decryptCipherText(msg, encodedToken, iv);
        } catch (Exception e) {
            MyLog.e(TAG, "getScMessageResponseDecrypted: ", e);
        }
        return originalMsg;
    }


}
