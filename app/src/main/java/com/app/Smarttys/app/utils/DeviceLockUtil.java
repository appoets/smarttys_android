package com.app.Smarttys.app.utils;

import android.content.Context;

import com.app.Smarttys.core.SessionManager;


public class DeviceLockUtil {

    public static final int IMMEDIATELY = 0;
    public static final int AFTER_ONE_MIN = 1;
    public static final int AFTER_FIFTEEN_MIN = 2;
    public static final int AFTER_ONE_HOUR = 3;

    private static final int oneMinMillis = 1000 * 60;

    public static long getDeviceLockDuration(Context context) {

        int type = SessionManager.getInstance(context).getDeviceLockType();
        switch (type) {

            case IMMEDIATELY:
                return 1000;


            case AFTER_ONE_MIN:
                return oneMinMillis;

            case AFTER_FIFTEEN_MIN:
                return oneMinMillis * 15;

            case AFTER_ONE_HOUR:
                return oneMinMillis * 60;

        }
        return 0;
    }


}
