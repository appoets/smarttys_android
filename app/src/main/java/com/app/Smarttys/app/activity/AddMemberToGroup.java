package com.app.Smarttys.app.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.AddGroupMemberAdapter;
import com.app.Smarttys.app.adapter.RItemAdapter;
import com.app.Smarttys.app.dialog.CustomAlertDialog;
import com.app.Smarttys.app.utils.BlockUserUtils;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.message.GroupEventInfoMessage;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Created by CAS60 on 11/30/2016.
 */
public class AddMemberToGroup extends CoreActivity implements RItemAdapter.OnItemClickListener, View.OnClickListener {

    private static final String TAG = "AddMemberToGroup";
    private RecyclerView rvContacts;
    private ImageView ibBack;
    private TextView tvNoContacts;
    private List<SmarttyContactModel> smarttyContacts, filterContacts;
    private String mCurrentUserId, mGroupId, mGroupName, mGroupUserIds;
    private AddGroupMemberAdapter adapter;
    private ImageView search;
    private EditText etSearch;
    private TextView add_participant_headertext;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member_group);

        context = AddMemberToGroup.this;

        initView();
        initData();
    }

    private void initData() {

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        smarttyContacts = contactDB_sqlite.getSavedSmarttyContacts();


        mCurrentUserId = SessionManager.getInstance(AddMemberToGroup.this).getCurrentUserID();

        Bundle bundle = getIntent().getExtras();
        mGroupId = bundle.getString("GroupId", "");
        mGroupName = bundle.getString("GroupName", "");
        mGroupUserIds = bundle.getString("GroupUserIds", "");

        initProgress("Loading ...", false);
        filterContacts = new ArrayList<>();

        if (smarttyContacts != null && smarttyContacts.size() > 0) {

            for (SmarttyContactModel item : smarttyContacts) {
                String userId = item.get_id();
                if (!mGroupUserIds.contains(userId) && !contactDB_sqlite.getBlockedMineStatus(userId, false).equals("1")) {
//                if (!mGroupUserIds.contains(userId) && !contactsDB.getBlockedMineStatus(userId, false).equals("1")) {
                    filterContacts.add(item);
                }
            }

            Collections.sort(filterContacts, Getcontactname.nameAscComparator);

            adapter = new AddGroupMemberAdapter(AddMemberToGroup.this, filterContacts);
            rvContacts.setAdapter(adapter);
            rvContacts.addOnItemTouchListener(new RItemAdapter(AddMemberToGroup.this,
                    rvContacts, AddMemberToGroup.this));
        } else {
            Toast.makeText(AddMemberToGroup.this, "Your contacts are not available in " + getResources().getString(R.string.app_name), Toast.LENGTH_SHORT).show();
        }

        if (filterContacts.size() == 0) {
            rvContacts.setVisibility(View.GONE);
            tvNoContacts.setVisibility(View.VISIBLE);
        }
    }

    private void initView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ibBack = findViewById(R.id.ibBack);
        ibBack.setOnClickListener(AddMemberToGroup.this);

        rvContacts = findViewById(R.id.rvContacts);

        LinearLayoutManager manager = new LinearLayoutManager(AddMemberToGroup.this);
        rvContacts.setLayoutManager(manager);

        tvNoContacts = findViewById(R.id.tvNoContacts);
        search = findViewById(R.id.search);
        etSearch = findViewById(R.id.etSearch);
        add_participant_headertext = findViewById(R.id.add_participant_headertext);

        Onclick();
    }


    private void Onclick() {

        search.setOnClickListener(this);

    }

    @Override
    public void onItemClick(View view, int position) {
        showAddAlert(position);
    }

    @Override
    public void onItemLongClick(View view, int position) {

    }

    private void showAddAlert(final int position) {

        final SmarttyContactModel e = adapter.getItem(position);

        String userId = e.get_id();
        String name = e.getMsisdn();

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);

        if (!contactDB_sqlite.getBlockedStatus(userId, false).equals("1")) {
            final CustomAlertDialog dialog = new CustomAlertDialog();
            String msg = "Add " + e.getFirstName() + " to \"" + mGroupName + "\" group";
            dialog.setMessage(msg);

            dialog.setPositiveButtonText("Ok");
            dialog.setNegativeButtonText("Cancel");

            dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
                @Override
                public void onPositiveButtonClick() {
                    if (internetcheck()) {
                        performAddMemberGroup(e.get_id());
                    } else {
                        Toast.makeText(AddMemberToGroup.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onNegativeButtonClick() {
                    dialog.dismiss();
                }
            });
            dialog.show(getSupportFragmentManager(), "Add member");
        } else {
            Getcontactname getcontactname = new Getcontactname(this);
            String message = "Unblock" + " " + getcontactname.getSendername(userId, name) + " " + "to add group?";
            displayAlert(message, userId);
        }
    }

    private void displayAlert(final String txt, final String userId) {
        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(txt);
        dialog.setNegativeButtonText("Cancel");
        dialog.setPositiveButtonText("Unblock");
        dialog.setCancelable(false);
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                BlockUserUtils.changeUserBlockedStatus(AddMemberToGroup.this, EventBus.getDefault(),
                        mCurrentUserId, userId, false);
                dialog.dismiss();
            }

            @Override

            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getSupportFragmentManager(), "Unblock a person");

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {

        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GROUP)) {
            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String groupAction = object.getString("groupType");

                if (groupAction.equalsIgnoreCase(SocketManager.ACTION_ADD_GROUP_MEMBER)) {
                    loadAddMemberMessage(object);
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_BLOCK_USER)) {
            loadBlockUserMessage(event);
        }
    }

    private void loadBlockUserMessage(ReceviceMessageEvent event) {
        try {
            Object[] obj = event.getObjectsArray();
            JSONObject object = new JSONObject(obj[0].toString());

            String from = object.getString("from");
            String to = object.getString("to");

            if (mCurrentUserId.equalsIgnoreCase(from)) {
                for (int i = 0; i < filterContacts.size(); i++) {
                    String userId = filterContacts.get(i).get_id();
                    if (userId.equals(to)) {
                        String stat = object.getString("status");
                        if (stat.equalsIgnoreCase("1")) {
                            Toast.makeText(this, "Number is blocked", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this, "Number is Unblocked", Toast.LENGTH_SHORT).show();
                        }
                        adapter.notifyDataSetChanged();
                        break;
                    }
                }
            }

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private Boolean internetcheck() {
        ConnectivityManager cm =
                (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;

    }

    private void loadAddMemberMessage(JSONObject object) {

        try {
            String msg = object.getString("message");
            String err = object.getString("err");
            String groupId = object.getString("groupId");
            String msgId = object.getString("id");
            String timeStamp = object.getString("timeStamp");
            String from = object.getString("from");

            JSONObject newUserObj = object.getJSONObject("newUser");
            String newUserId = newUserObj.getString("_id");
            String newUserMsisdn = newUserObj.getString("msisdn");
            String newUserPhNo = newUserObj.getString("PhNumber");
//            String newUserName = newUserObj.getString("Name");
            if (object.has("Status")) {
                String newUserStatus = newUserObj.getString("Status");
            }

            GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, this);
            MessageItemChat item = message.createMessageItem(MessageFactory.add_group_member, false, msg, MessageFactory.DELIVERY_STATUS_READ,
                    mGroupId, mGroupName, from, newUserId);
            String docId = mCurrentUserId.concat("-").concat(mGroupId).concat("-g");
            item.setSenderName(mGroupName);
            item.setGroupName(mGroupName);
            item.setIsInfoMsg(true);
            item.setMessageId(docId.concat("-").concat(timeStamp));
//            db.updateChatMessage(docId, item);

            Intent exitIntent = new Intent();
            exitIntent.putExtra("MemberAdded", true);
            setResult(RESULT_OK, exitIntent);
            hideProgressDialog();
            finish();

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }

    private void performAddMemberGroup(String newUserId) {
        long ts = Calendar.getInstance().getTimeInMillis();
        String msgId = mCurrentUserId + "-" + mGroupId + "-g-" + ts;

        try {
            JSONObject object = new JSONObject();
            object.put("groupType", SocketManager.ACTION_ADD_GROUP_MEMBER);
            object.put("from", SessionManager.getInstance(AddMemberToGroup.this).getCurrentUserID());
            object.put("id", ts);
            object.put("toDocId", msgId);
            object.put("groupId", mGroupId);
            object.put("newuser", newUserId);
            object.put("add_new_group_name", true);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GROUP);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);

            showProgressDialog();
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(AddMemberToGroup.this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(AddMemberToGroup.this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ibBack:
                BackButtonPush();
                break;

            case R.id.search:
                SearchClickFunction();
                break;

        }

    }


    private void SearchClickFunction() {


        add_participant_headertext.setVisibility(View.GONE);
        search.setVisibility(View.GONE);
        etSearch.setVisibility(View.VISIBLE);
        etSearch.requestFocus();

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });


    }


    private void BackButtonPush() {

        if (etSearch.getVisibility() == View.VISIBLE) {

            etSearch.setText("");
            etSearch.setVisibility(View.GONE);
            search.setVisibility(View.VISIBLE);
            add_participant_headertext.setVisibility(View.VISIBLE);
            adapter.updateInfo(filterContacts);
            if (AddMemberToGroup.this.getCurrentFocus() != null) {
                InputMethodManager inputMethodManager =
                        (InputMethodManager) getApplicationContext().getSystemService(
                                Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(
                        AddMemberToGroup.this.getCurrentFocus().getWindowToken(), 0);

            }
        } else {

            finish();
        }
    }
}
