package com.app.Smarttys.app.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import androidx.appcompat.widget.Toolbar;

import com.app.Smarttys.R;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;

/**
 * Created by CAS63 on 11/18/2016.
 */
public class DataUsage extends CoreActivity {
    final Context context = this;
    //  ImageView backimg;
    LinearLayout l1, l2, l3;
    AvnNextLTProRegTextView text_media, textm_data, text_wifi, text_roam, content, call_setting, text_call_setting, text_data;
    AvnNextLTProRegTextView mobiledata, wifi, roaming;
    Session session;
    String sIncludedOn = "";
    String sIncludedOn1 = "";
    String sIncludedOn2 = "";
    private AvnNextLTProDemiTextView /*text_actionbar_1,*/textnetwork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_usage);
        session = new Session(DataUsage.this);
        textnetwork = findViewById(R.id.textnetwork);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        setToolbar();


        call_setting = findViewById(R.id.call_setting);
        text_call_setting = findViewById(R.id.text_call_setting);

        text_data = findViewById(R.id.text_data);
        //   text_actionbar_1 = (AvnNextLTProDemiTextView) findViewById(R.id.text_actionbar_1);

        l1 = findViewById(R.id.Linear1);
        l2 = findViewById(R.id.Linear2);
        l3 = findViewById(R.id.Linear3);
        mobiledata = findViewById(R.id.mobiledata);

        wifi = findViewById(R.id.wifi);
        roaming = findViewById(R.id.roaming);
        if (!session.getmobiledataPrefsName().equals("")) {
            mobiledata.setText(session.getmobiledataPrefsName());
        } else {
            mobiledata.setText(R.string.no_media);
        }
        if (!session.getwifiPrefsName().equals("")) {
            wifi.setText(session.getwifiPrefsName());
        } else {
            wifi.setText(R.string.no_media);
        }
        if (!session.getromingPrefsName().equals("")) {
            roaming.setText(session.getromingPrefsName());
        } else {
            roaming.setText(R.string.no_media);
        }
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //backimg = (ImageView) findViewById(R.id.backarrow_datausage);
        //  getSupportActionBar().hide();
       /* backimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityLauncher.launchHomeScreen(DataUsage.this);

            }
        });
       */
        textnetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DataUsage.this, Networkusage.class);
                startActivity(i);

            }
        });
        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);

                final CheckBox c1, c2, c3, c4;
                AvnNextLTProDemiTextView cancel, ok;
                AvnNextLTProRegTextView text;
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_mobile_data);
                text = dialog.findViewById(R.id.texthead);
                c1 = dialog.findViewById(R.id.check1);
                c2 = dialog.findViewById(R.id.check2);
                c3 = dialog.findViewById(R.id.check3);
                c4 = dialog.findViewById(R.id.check4);
                cancel = dialog.findViewById(R.id.cancel);
                ok = dialog.findViewById(R.id.ok);
                Typeface typeface = CoreController.getInstance().getAvnNextLTProRegularTypeface();
                c1.setTypeface(typeface);
                c2.setTypeface(typeface);
                c3.setTypeface(typeface);
                c4.setTypeface(typeface);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                text.setText(getString(R.string.When_using_mobile_data));
                String[] values = session.getmobiledataPrefsName().split(",");
                //System.out.println("==================================values" + values);
                for (int i = 0; i < values.length; i++) {
                    if (values[i].equalsIgnoreCase(getString(R.string.photo))) {
                        c1.setChecked(true);
                    }
                    if (values[i].equalsIgnoreCase(getString(R.string.audio))) {
                        c2.setChecked(true);
                    }
                    if (values[i].equalsIgnoreCase(getString(R.string.video))) {
                        c3.setChecked(true);
                    }
                    if (values[i].equalsIgnoreCase(getString(R.string.doc))) {
                        c4.setChecked(true);
                    }
                }


                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (c1.isChecked()) {
                            sIncludedOn = sIncludedOn + getString(R.string.photo) + ",";
                        }


                        if (c2.isChecked()) {
                            sIncludedOn = sIncludedOn + getString(R.string.audio) + ",";
                        }

                        if (c3.isChecked()) {
                            sIncludedOn = sIncludedOn + getString(R.string.video) + ",";
                        }

                        if (c4.isChecked()) {
                            sIncludedOn = sIncludedOn + getString(R.string.doc) + ",";
                        }


                        if (sIncludedOn.equalsIgnoreCase("")) {
                            sIncludedOn = getString(R.string.no_media);
                            session.putmobiledataPrefs(sIncludedOn);
                            mobiledata.setText(session.getmobiledataPrefsName());
                            sIncludedOn = "";
                            dialog.dismiss();
                            //  Toast.makeText(context, "Please select any one option?", Toast.LENGTH_LONG).show();
                        } else {
                            sIncludedOn = sIncludedOn.substring(0, sIncludedOn.length() - 1);
                            dialog.dismiss();
                            session.putmobiledataPrefs(sIncludedOn);
                            mobiledata.setText(session.getmobiledataPrefsName());
                            sIncludedOn = "";

                            //System.out.println("-----------sIncludedOn----------" + sIncludedOn);
                            //System.out.println("-----------session.getmobiledataPrefsName()----------" + session.getmobiledataPrefsName());
                        }

                    }

                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }

        });

        l2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);

                final CheckBox c1, c2, c3, c4;
                AvnNextLTProDemiTextView cancel, ok;
                AvnNextLTProRegTextView text;
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_mobile_data);
                text = dialog.findViewById(R.id.texthead);
                c1 = dialog.findViewById(R.id.check1);
                c2 = dialog.findViewById(R.id.check2);
                c3 = dialog.findViewById(R.id.check3);
                c4 = dialog.findViewById(R.id.check4);
                cancel = dialog.findViewById(R.id.cancel);
                ok = dialog.findViewById(R.id.ok);

                Typeface typeface = CoreController.getInstance().getAvnNextLTProRegularTypeface();
                c1.setTypeface(typeface);
                c2.setTypeface(typeface);
                c3.setTypeface(typeface);
                c4.setTypeface(typeface);

                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                text.setText(R.string.When_Connected_On_Wifi);
                String[] values = session.getwifiPrefsName().split(",");
                //System.out.println("==================================values" + values);
                for (int i = 0; i < values.length; i++) {
                    if (values[i].equalsIgnoreCase(getString(R.string.photo))) {
                        c1.setChecked(true);
                    }
                    if (values[i].equalsIgnoreCase(getString(R.string.audio))) {
                        c2.setChecked(true);
                    }
                    if (values[i].equalsIgnoreCase(getString(R.string.video))) {
                        c3.setChecked(true);
                    }
                    if (values[i].equalsIgnoreCase(getString(R.string.doc))) {
                        c4.setChecked(true);
                    }
                }


                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (c1.isChecked()) {
                            sIncludedOn1 = sIncludedOn1 + getString(R.string.photo) + ",";
                        }

                        if (c2.isChecked()) {
                            sIncludedOn1 = sIncludedOn1 + getString(R.string.audio) + ",";
                        }

                        if (c3.isChecked()) {
                            sIncludedOn1 = sIncludedOn1 + getString(R.string.video) + ",";
                        }

                        if (c4.isChecked()) {
                            sIncludedOn1 = sIncludedOn1 + getString(R.string.doc) + ",";
                        }


                        if (sIncludedOn1.equalsIgnoreCase("")) {
                            sIncludedOn1 = getString(R.string.no_media);
                            session.putwifiPrefs(sIncludedOn1);
                            wifi.setText(session.getwifiPrefsName());
                            sIncludedOn1 = "";
                            dialog.dismiss();
                        } else {
                            sIncludedOn1 = sIncludedOn1.substring(0, sIncludedOn1.length() - 1);
                            dialog.dismiss();
                            session.putwifiPrefs(sIncludedOn1);
                            wifi.setText(session.getwifiPrefsName());
                            sIncludedOn1 = "";
                            //System.out.println("-----------sIncludedOn----------" + sIncludedOn1);
                            //System.out.println("-----------session.getmobiledataPrefsName()----------" + session.getmobiledataPrefsName());
                        }

                    }

                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }

        });
        l3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);

                final CheckBox c1, c2, c3, c4;
                AvnNextLTProDemiTextView cancel, ok;
                AvnNextLTProRegTextView text;
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_mobile_data);
                text = dialog.findViewById(R.id.texthead);
                c1 = dialog.findViewById(R.id.check1);
                c2 = dialog.findViewById(R.id.check2);
                c3 = dialog.findViewById(R.id.check3);
                c4 = dialog.findViewById(R.id.check4);
                cancel = dialog.findViewById(R.id.cancel);
                ok = dialog.findViewById(R.id.ok);

                Typeface typeface = CoreController.getInstance().getAvnNextLTProRegularTypeface();
                c1.setTypeface(typeface);
                c2.setTypeface(typeface);
                c3.setTypeface(typeface);
                c4.setTypeface(typeface);

                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                text.setText(getString(R.string.when_roaming));
                String[] values = session.getromingPrefsName().split(",");
                //System.out.println("==================================values" + values);
                for (int i = 0; i < values.length; i++) {
                    if (values[i].equalsIgnoreCase(getString(R.string.photo))) {
                        c1.setChecked(true);
                    }
                    if (values[i].equalsIgnoreCase(getString(R.string.audio))) {
                        c2.setChecked(true);
                    }
                    if (values[i].equalsIgnoreCase(getString(R.string.video))) {
                        c3.setChecked(true);
                    }
                    if (values[i].equalsIgnoreCase(getString(R.string.doc))) {
                        c4.setChecked(true);
                    }
                }


                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (c1.isChecked()) {
                            sIncludedOn2 = sIncludedOn2 + getString(R.string.photo) + ",";
                        }

                        if (c2.isChecked()) {
                            sIncludedOn2 = sIncludedOn2 + getString(R.string.audio) + ",";
                        }

                        if (c3.isChecked()) {
                            sIncludedOn2 = sIncludedOn2 + getString(R.string.video) + ",";
                        }

                        if (c4.isChecked()) {
                            sIncludedOn2 = sIncludedOn2 + getString(R.string.doc) + ",";
                        }


                        if (sIncludedOn2.equalsIgnoreCase("")) {
                            sIncludedOn2 = getString(R.string.no_media);
                            session.putromingPrefs(sIncludedOn2);
                            roaming.setText(session.getromingPrefsName());
                            sIncludedOn2 = "";
                            dialog.dismiss();

                        } else {
                            sIncludedOn2 = sIncludedOn2.substring(0, sIncludedOn2.length() - 1);
                            dialog.dismiss();

                            session.putromingPrefs(sIncludedOn2);
                            roaming.setText(session.getromingPrefsName());
                            sIncludedOn2 = "";

                            //System.out.println("-----------sIncludedOn----------" + sIncludedOn);
                            //System.out.println("-----------session.getmobiledataPrefsName()----------" + session.getmobiledataPrefsName());
                        }

                    }

                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setToolbar() {
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        // actionBar.setTitle("Notifications");
        //  actionBar.sett(getResources().getColor(R.color.someColor));
        getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>" + context.getResources().getString(R.string.Data_Usage) + "</font>"));

        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}



