package com.app.Smarttys.app.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.ChatMessageAdapter;
import com.app.Smarttys.app.dialog.CustomAlertDialog;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.BlockUserUtils;
import com.app.Smarttys.app.utils.ConnectivityInfo;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.TimeStampUtils;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.app.widget.AvnNextLTProDemiButton;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.app.widget.CircleImageView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.ShortcutBadgeManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.AudioMessage;
import com.app.Smarttys.core.message.ContactMessage;
import com.app.Smarttys.core.message.DocumentMessage;
import com.app.Smarttys.core.message.IncomingMessage;
import com.app.Smarttys.core.message.LocationMessage;
import com.app.Smarttys.core.message.MessageAck;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.message.PictureMessage;
import com.app.Smarttys.core.message.TextMessage;
import com.app.Smarttys.core.message.TimerChangeMessage;
import com.app.Smarttys.core.message.VideoMessage;
import com.app.Smarttys.core.message.WebLinkMessage;
import com.app.Smarttys.core.model.ContactToSend;
import com.app.Smarttys.core.model.Imagepath_caption;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.smarttyhelperclass.SmarttyUtilities;
import com.app.Smarttys.core.socket.NotificationUtil;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.core.uploadtoserver.FetchDownloadManager;
import com.app.Smarttys.core.uploadtoserver.FileDownloadListener;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import droidninja.filepicker.models.sort.SortingTypes;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import io.socket.client.Socket;
import me.leolin.shortcutbadger.ShortcutBadger;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.text.InputType.TYPE_TEXT_FLAG_MULTI_LINE;
import static android.text.InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD;

/**
 *
 */
public class SecretChatViewActivity extends CoreActivity implements View.OnClickListener,
        View.OnLongClickListener, NumberPicker.OnValueChangeListener, ScreenShotDetector.ScreenShotListener, ItemClickListener, FileDownloadListener {
    private static final int RESULT_LOAD_IMAGE = 1;
    private static final int RESULT_LOAD_VIDEO = 2;
    private static final int RESULT_CAPTURE_VIDEO = 3;
    private static final int REQUEST_CODE_CONTACTS = 4;
    private static final int RESULT_SHARE_LOCATION = 5;
    private static final int CHECKING_LOCATION = 6;
    private static final int REQUEST_SELECT_AUDIO = 7;
    private static final int REQUEST_CODE_DOCUMENT = 9;
    private static final int PICK_CONTACT_REQUEST = 10;
    private static final int CAMERA_REQUEST = 1888;
    private static final String TAG = "SecretChatViewActivity";
    public static boolean isSecretChatPage;
    public static String receiverMsisdn;
    final Context context = this;
    private final int AUDIO_RECORD_PERMISSION_REQUEST_CODE = 14;
    private final int MUTE_ACTIVITY = 18;
    private final int REQUEST_CODE_FORWARD_MSG = 15;
    public Boolean reply = false, isSelectedWithUnStarMsg;
    public String to, mConvId = "";
    int lastvisibleitempostion = 0;
    int unreadmsgcount = 0;
    Getcontactname getcontactname;
    ArrayList<ContactToSend> contacts;
    int timervalue = 0;
    String ContactString, secretMsgTimer, secretMsgTimerMode, secretMsgTimerId;
    ImageView image_to, msgTimer;
    long fromLastTypedAt = 0, toLastTypedAt = 0;
    ImageView cameraphoto, videoimage, audioimage, personimage;
    String mesgid;
    ImageView cameraimage, sentimage;
    RelativeLayout cameralayout;
    RelativeLayout overflowlayout, Relative_body;
    View rootView;
    String[] array;
    String messageold = "", mDataId, mRawContactId;
    RelativeLayout r1messagetoreplay, RelativeLayout_group_delete, text_lay_out, rlSend;
    ImageView emai1send, gmailsend, record;
    boolean isFirstItemSelected = false;
    String mypath, audioRecordPath;
    RelativeLayout imageLayout;
    RelativeLayout emailgmail;
    String value, ReplySender = "";
    String date;
    Bitmap bitmap;
    Drawable d;
    String name, number;
    boolean backfrom, canShowLastSeen, isLastSeenCalled;
    String replytype;
    String receiverUid;
    Session session;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    Chronometer myChronometer;
    ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
    ArrayList<MessageItemChat> removeChatItems = new ArrayList<>();
    AvnNextLTProDemiTextView slidetocencel;
    //    ArrayList<String> selected_data = new ArrayList<String>();
    AvnNextLTProRegTextView unreadcount;
    EmojIconActions emojIcon;
    ContactDB_Sqlite contactDB_sqlite;
    private List<String> messageIds = new ArrayList<>();
    private String mReceiverId;
    private ImageView sendButton, captureImage, selEmoji;
    private ListView recyclerView_chat;
    private EmojiconEditText sendMessage;
    private CircleImageView ivProfilePic;
    private TextView groupUsername, tvWebLink, tvWebLinkDesc, tvWebTitle, message_old;
    private AvnNextLTProDemiTextView receiverNameText, tvSecretTimer;
    private AvnNextLTProRegTextView statusTextView, tvTyping, startwith_name;
    Runnable toTypingRunnable = new Runnable() {
        @Override
        public void run() {
            long currentTime = Calendar.getInstance().getTimeInMillis();
            if (currentTime > toLastTypedAt) {
                tvTyping.setVisibility(View.GONE);
                statusTextView.setVisibility(View.VISIBLE);
            }
        }
    };
    private ImageView history, ivWebLink, ivWebLinkClose;
    private LinearLayout atlas_actionbar_call;
    private RelativeLayout backButton, hearderdate, rlWebLink;//,root;
    private TextView dateView, Ifname, messagesetmedio;
    private RelativeLayout nameMAincontainer;
    //    private LinearLayout bottomlayoutinner;
    private TextView groupleftmess;
    private Handler msgDeleteHandler;
    private Runnable msgDeleteRunnable;
    private Toolbar include;
    private RelativeLayout rlChatActions;
    private Handler toTypingHandler = new Handler();
    private SessionManager mSessionManager;
    private UserInfoSession userInfoSession;
    //    private CoordinatorLayout root;
    private ImageView iBtnBack, overflow, backnavigate;
    private ImageButton iBtnScroll;
    private ImageView delete, attachment, copy, forward, starred, info, replymess, close, Documentimage;
    private String mReceiverName = "";
    private String mymsgid = "";
    private String msgid;
    private String from;
    private ArrayList<MessageItemChat> mChatData;
    private ChatMessageAdapter mAdapter;
    private ImageView longpressback;
    private String mCurrentUserId, receiverAvatar;
    private FileUploadDownloadManager uploadDownloadManager;
    private String imgpath;
    private boolean loading = true;
    private LinearLayoutManager mLayoutManager;
    private int removeCount = 0;
    private boolean hasLinkInfo;
    private String webLink, webLinkTitle, webLinkDesc, webLinkImgUrl;
    TextWatcher watch = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub
            checkLinkDetails(sendMessage.getText().toString().trim());
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence cs, int a, int b, int c) {
            // TODO Auto-generated method stub

            handleTypingEvent(cs.length());

            if (cs.length() > 0) {
                captureImage.setVisibility(View.GONE);
                sendButton.setVisibility(View.VISIBLE);
                record.setVisibility(View.GONE);


            } else {
                captureImage.setVisibility(View.VISIBLE);
                sendButton.setVisibility(View.GONE);
                record.setVisibility(View.VISIBLE);
            }

        }
    };
    private String imageAsBytes;
    private String contactname;
    private boolean deletestarred = false, audioRecordStarted = false;
    private MediaRecorder audioRecorder;
    private String mLocDbDocId;
    private long serverTimeDiff;
    private View.OnKeyListener enterKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

            if (keyCode == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                if (session.isEnterKeyPressToSend()) {
                    if (isBlockedUser()) {
                        DisplayAlert(to);
                    } else {
                        if (!hasLinkInfo && !reply) {
                            sendTextMessage();
                        } else if (reply) {
                            r1messagetoreplay.setVisibility(View.GONE);
                            sendparticularmsgreply();
                        } else {
                            sendWebLinkMessage();
                        }
                    }
                    return true;
                }
            }

            return false;
        }
    };
    private View.OnTouchListener audioRecordTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View pView, MotionEvent pEvent) {
            pView.onTouchEvent(pEvent);

            if (pEvent.getAction() == MotionEvent.ACTION_UP) {
                try {

                    if (audioRecordStarted) {

                        MyLog.d("AudioRecordStop", "called");
                        String sendAudioPath = audioRecordPath;
                        audioRecordPath = "";
                        record.setImageResource(R.drawable.record_secret_normal);
                        selEmoji.setImageResource(R.drawable.smile);
                        sendMessage.setVisibility(View.VISIBLE);
                        myChronometer.setVisibility(View.GONE);
                        image_to.setVisibility(View.GONE);
                        captureImage.setVisibility(View.VISIBLE);
                        slidetocencel.setVisibility(View.GONE);

//                    audioRecorder.stop();
                        audioRecorder.release();
                        myChronometer.stop();

                        File file = new File(sendAudioPath);
                        if (file.exists()) {
                            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                            mmr.setDataSource(SecretChatViewActivity.this, Uri.parse(sendAudioPath));
                            String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                            //durationStr = getTimeString(AppUtils.parseLong(durationStr));
                            if (AppUtils.parseLong(durationStr) < 1000) {
                                Toast.makeText(SecretChatViewActivity.this, "Record atleast one second!", Toast.LENGTH_SHORT).show();
                            } else
                                showAudioRecordSentAlert(sendAudioPath, durationStr);
                        }
                    }
                } catch (RuntimeException e) {
                    MyLog.e(TAG, "", e);
                }
                audioRecordStarted = false;
                selEmoji.setEnabled(true);
            }
            return false;
        }
    };

    private static String getCursorString(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if (index != -1) return cursor.getString(index);
        return null;
    }

    public static boolean setNumberPickerTextColor(NumberPicker numberPicker, int color) {
        final int count = numberPicker.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = numberPicker.getChildAt(i);
            if (child instanceof EditText) {
                try {
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText) child).setTextColor(color);
                    numberPicker.invalidate();
                    return true;
                } catch (NoSuchFieldException e) {
                    MyLog.e(TAG, "", e);
                    //  Log.w("setNumberPickerTextColor", e);
                } catch (IllegalAccessException e) {
                    MyLog.e(TAG, "", e);
                    //  Log.w("setNumberPickerTextColor", e);
                } catch (IllegalArgumentException e) {
                    MyLog.e(TAG, "", e);
                    //Log.w("setNumberPickerTextColor", e);
                }
            }
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secret_chat_message_screen_new);
        //   startService(new Intent(this, ScreenShotDetector.class));
        AppUtils.startService(this, ScreenShotDetector.class);

        ScreenShotDetector.setListener(this);
//        getSupportActionBar().hide();
        session = new Session(SecretChatViewActivity.this);
        userInfoSession = new UserInfoSession(SecretChatViewActivity.this);
        // mservice = new MessageService(ChatViewActivity.this);
        getcontactname = new Getcontactname(SecretChatViewActivity.this);
        initView();
        deleteMsgHandler();
    }

    private void initDataBase() {
        loadFromDB();

        if (msgid != null || !msgid.equals("")) {
            Starredmsg();
        }

    }

    private boolean isAlreadyExist(MessageItemChat messageItemChat) {
        String msgId = messageItemChat.getMessageId();
        if (messageIds.contains(msgId)) {
            return true;
        }
        messageIds.add(msgId);
        return false;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void initView() {
//        root = (CoordinatorLayout) findViewById(R.id.typing);
//        bottomlayoutinner = (LinearLayout) findViewById(R.id.bottomlayoutinner);
        FetchDownloadManager.getInstance().secretChatListener(this);
        contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        groupleftmess = findViewById(R.id.groupleftmess);
        sendButton = findViewById(R.id.enter_chat1);
        record = findViewById(R.id.record);
        myChronometer = findViewById(R.id.chronometer);
        include = findViewById(R.id.chatheaderinclude);
        setSupportActionBar(include);

        rlChatActions = findViewById(R.id.rlChatActions);
        rlChatActions.setVisibility(View.GONE);

        msgTimer = findViewById(R.id.timer);
        //rlChatSeen = findViewById(R.id.rlChatSeen);
        unreadcount = findViewById(R.id.unreadcount);
        personimage = findViewById(R.id.personimage);
        Documentimage = findViewById(R.id.Documentimage);
        ivProfilePic = include.findViewById(R.id.profileImageChatScreen);
        nameMAincontainer = include.findViewById(R.id.nameMAincontainer);
        statusTextView = include.findViewById(R.id.statuschatsceen);
        tvTyping = include.findViewById(R.id.tvTyping);
        tvSecretTimer = include.findViewById(R.id.tvSecretTimer);
        startwith_name = findViewById(R.id.startwith_name);
        messagesetmedio = findViewById(R.id.messagesetmedio);
        rlWebLink = findViewById(R.id.rlWebLink);
        tvWebTitle = findViewById(R.id.tvWebTitle);
        tvWebLink = findViewById(R.id.tvWebLink);
        tvWebLinkDesc = findViewById(R.id.tvWebLinkDesc);
        ivWebLink = findViewById(R.id.ivWebLink);
        ivWebLinkClose = findViewById(R.id.ivWebLinkClose);
        ivWebLinkClose.setOnClickListener(this);
        image_to = findViewById(R.id.image_to);
        slidetocencel = findViewById(R.id.slidetocencel);
        nameMAincontainer.setOnClickListener(this);
        replymess = rlChatActions.findViewById(R.id.replymess);
        close = findViewById(R.id.close);
        r1messagetoreplay = findViewById(R.id.r1messagetoreplay);
        text_lay_out = findViewById(R.id.text_lay_out);
        rlSend = findViewById(R.id.rlSend);
        Ifname = findViewById(R.id.Ifname);
        message_old = findViewById(R.id.message);
        dateView = findViewById(R.id.dateView);
        dateView.setText("Today");
        receiverNameText = include.findViewById(R.id.usernamechatsceen);
        backButton = include.findViewById(R.id.backButton);
        RelativeLayout_group_delete = include.findViewById(R.id.RelativeLayout_group_delete);
        delete = rlChatActions.findViewById(R.id.delete);
        attachment = include.findViewById(R.id.attachment);

        hearderdate = findViewById(R.id.hearderdate);
        sendMessage = findViewById(R.id.chat_edit_text1);
        recyclerView_chat = findViewById(R.id.list_view_messages);
        iBtnBack = findViewById(R.id.iBtnBack);
        iBtnScroll = findViewById(R.id.iBtnScroll);
        backnavigate = rlChatActions.findViewById(R.id.iBtnBack2);
        imageLayout = findViewById(R.id.Relative_recycler);
        overflow = include.findViewById(R.id.overflow);
        overflowlayout = include.findViewById(R.id.overflowLayout);
        overflowlayout.setOnClickListener(this);
        iBtnBack.setOnClickListener(this);
        ivProfilePic.setOnClickListener(this);

        iBtnScroll.setOnClickListener(this);
        videoimage = findViewById(R.id.videoimage);
        cameraphoto = findViewById(R.id.cameraphoto);
        audioimage = findViewById(R.id.audioimage);
        sentimage = findViewById(R.id.sentimage);
        cameraimage = findViewById(R.id.imagecamera);
        cameralayout = findViewById(R.id.cameralayout);
        Relative_body = findViewById(R.id.Relative_body);
        emailgmail = findViewById(R.id.email_gmail);
        emai1send = findViewById(R.id.emai1send);
        gmailsend = findViewById(R.id.gmailsend);
        copy = rlChatActions.findViewById(R.id.copychat);
        forward = rlChatActions.findViewById(R.id.forward);
        info = rlChatActions.findViewById(R.id.info);
        starred = rlChatActions.findViewById(R.id.starred);
        longpressback = rlChatActions.findViewById(R.id.iBtnBack3);

        captureImage = findViewById(R.id.capture_image);
        selEmoji = findViewById(R.id.emojiButton);
        rootView = findViewById(R.id.mainRelativeLayout);

        emojIcon = new EmojIconActions(this, rootView, sendMessage, selEmoji);
        //  emojIcon.ShowEmojIcon();

        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smile);
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {

                MyLog.e("", "Keyboard opened!");
            }

            @Override
            public void onKeyboardClose() {
                MyLog.e("", "Keyboard Closed!");

            }
        });
        emojIcon.setUseSystemEmoji(false);
        // popup = new EmojiconsPopup(rootView, this);

        sendMessage.setOnKeyListener(enterKeyListener);
        Typeface typeface = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        sendMessage.setTypeface(typeface);
        if (Build.MANUFACTURER.equalsIgnoreCase("samsung")) {
            sendMessage.setInputType(TYPE_TEXT_FLAG_MULTI_LINE |
                    TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        }
        msgTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show();
            }
        });


        record.setOnLongClickListener(SecretChatViewActivity.this);
        record.setOnTouchListener(audioRecordTouchListener);
        recyclerView_chat.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                emailgmail.setVisibility(View.GONE);
                return false;
            }
        });

/*
        recyclerView_chat.addOnItemTouchListener(new RItemAdapter(this, recyclerView_chat, new RItemAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {

                if (position >= 0) {

                    String type = mChatData.get(position).getMessageType();

                    if (type != null) {
                        int messagetype = Integer.parseInt(type);

                        if (MessageFactory.picture == messagetype) {

                            MessageItemChat item = mChatData.get(position);
                            if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START
                                    && item.isSelf()) {
                                item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
                                uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, true);
                            } else if (!item.isSelf() && item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START &&
                                    item.getUploadDownloadProgress() == 0) {
                                */
        /*Download option for sent images from web chat*//*

                                item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
                                uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, true);
                            }
                        } else if (MessageFactory.audio == messagetype) {

                            MessageItemChat item = mChatData.get(position);
                            if (item.getUploadDownloadProgress() == 0 && !item.isSelf() && item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START) {
                                item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
                                uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, true);
                            } else if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START
                                    && item.isSelf()) {
                                */
        /*Download option for sent documents from web chat*//*

                                item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
                                uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, true);
                            }
                        } else if (MessageFactory.document == messagetype) {

                            MessageItemChat item = mChatData.get(position);
                            if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START && !item.isSelf()) {
                                item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
                                uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, true);
                            } else if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START
                                    && item.isSelf()) {
                                */
        /*Download option for sent documents from web chat*//*

                                item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
                                uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, true);
                            } else {
                                String extension = MimeTypeMap.getFileExtensionFromUrl(item.getChatFileLocalPath());
                                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

                                PackageManager packageManager = getPackageManager();
                                Intent testIntent = new Intent(Intent.ACTION_VIEW);
                                testIntent.setType(mimeType);
                                try {

                                    List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
                                    if (list.size() > 0) {
                                        File file = new File(item.getChatFileLocalPath());
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setDataAndType(Uri.fromFile(file), mimeType);
                                        startActivity(intent);
                                    } else {
                                        Toast.makeText(SecretChatViewActivity.this, "No app installed to view this document", Toast.LENGTH_LONG).show();
                                    }
                                } catch (ActivityNotFoundException e) {
                                    Toast.makeText(SecretChatViewActivity.this, "No app installed to view this document", Toast.LENGTH_LONG).show();
                                }


                            }
                        } else if (MessageFactory.video == messagetype) {

                            MessageItemChat item = mChatData.get(position);
                            if (item.getUploadDownloadProgress() == 0 && !item.isSelf() &&
                                    item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START) {
                                item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
                                uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, true);
                            } else if (!item.isSelf() && item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                                try {
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getChatFileLocalPath()));
                                    String path = "file://" + item.getChatFileLocalPath();
                                    intent.setDataAndType(Uri.parse(path), "video/*");
                                    startActivity(intent);
                                } catch (ActivityNotFoundException e) {
                                    Toast.makeText(SecretChatViewActivity.this, "No app installed to play this video", Toast.LENGTH_LONG).show();
                                }
                            } else if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START
                                    && item.isSelf()) {
                                */
        /*Download option for sent documents from web chat*//*

                                item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
                                uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, true);
                            }
                        } else if (MessageFactory.location == messagetype) {
                            MessageItemChat item = mChatData.get(position);
                            String path = "geo:37.7749,-122.4194";
                            Uri gmmIntentUri = Uri.parse(item.getWebLink());
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            startActivity(mapIntent);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            if (mapIntent.resolveActivity(getPackageManager()) != null) {
                                startActivity(mapIntent);
                            }
                        }
                    }

                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onItemLongClick(final View view, final int position) {

            }
        }));
*/

/*        recyclerView_chat.addOnScrollListener(new RecyclerView.OnScrollListener()

                                              {
                                                  @Override
                                                  public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                      super.onScrolled(recyclerView, dx, dy);

                                                      visibleItemCount = mLayoutManager.getChildCount();
                                                      totalItemCount = mLayoutManager.getItemCount();
                                                      pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                                                      lastvisibleitempostion = mLayoutManager.findLastVisibleItemPosition();
                                                      if (loading) {
                                                          if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                                              unreadmsgcount = 0;
                                                              unreadcount.setVisibility(View.GONE);
                                                              iBtnScroll.setVisibility(View.GONE);
                                                          } else {
                                                              iBtnScroll.setVisibility(View.VISIBLE);
                                                          }
                                                      }

                                                  }
                                              }

        );*/


      /*  popup.setSizeForSoftKeyboard();
        popup.setOnDismissListener(new PopupWindow.OnDismissListener()

                                   {
                                       @Override
                                       public void onDismiss() {
                                           changeEmojiKeyboardIcon(selEmoji, R.mipmap.ic_msg_panel_smiles);
                                       }
                                   }

        );
        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener()

                                                 {

                                                     @Override
                                                     public void onKeyboardOpen(int keyBoardHeight) {

                                                     }

                                                     @Override
                                                     public void onKeyboardClose() {
                                                         if (popup.isShowing())
                                                             popup.dismiss();
                                                     }
                                                 }

        );
        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener()

                                           {

                                               @Override
                                               public void onEmojiconClicked(Emojicon emojicon) {
                                                   if (sendMessage == null || emojicon == null) {
                                                       return;
                                                   }

                                                   int start = sendMessage.getSelectionStart();
                                                   int end = sendMessage.getSelectionEnd();
                                                   if (start < 0) {
                                                       sendMessage.append(emojicon.getEmoji());
                                                   } else {
                                                       sendMessage.getText().replace(Math.min(start, end),
                                                               Math.max(start, end), emojicon.getEmoji(), 0,
                                                               emojicon.getEmoji().length());
                                                   }
                                               }
                                           }

        );
        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener()

                                                    {

                                                        @Override
                                                        public void onEmojiconBackspaceClicked(View v) {
                                                            KeyEvent event = new KeyEvent(
                                                                    0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                                                            sendMessage.dispatchKeyEvent(event);
                                                        }
                                                    }

        );
*/
        sendMessage.addTextChangedListener(watch);


        initData();


    }

    public void VideoDownloadComplete(String Docid, String Msgid, String localpath) {
        MessageDbController db = CoreController.getDBInstance(this);
        for (int i = 0; i < mChatData.size(); i++) {
            if (Msgid.equalsIgnoreCase(mChatData.get(i).getMessageId())) {
                mChatData.get(i).setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_COMPLETED);
                db.updateMessageDownloadStatus(Docid, Msgid, MessageFactory.DOWNLOAD_STATUS_COMPLETED, localpath);
                mAdapter.notifyDataSetChanged();

                break;
            }
        }
    }

    private void setSecretTimerText() {
        int time = Integer.parseInt(secretMsgTimer);
        String timeStr = "";
        switch (time) {
            case 5 * 1000:
                timeStr = "5 sec";
                timervalue = 0;
                break;

            case 10 * 1000:
                timeStr = "10 sec";
                timervalue = 1;
                break;

            case 30 * 1000:
                timeStr = "30 sec";
                timervalue = 2;
                break;

            case 60 * 1000:
                timeStr = "1 min";
                timervalue = 3;
                break;

            case 60 * 60 * 1000:
                timeStr = "1 hr";
                timervalue = 4;
                break;

            case 24 * 60 * 60 * 1000:
                timeStr = "1 day";
                timervalue = 5;
                break;

            case 7 * 24 * 60 * 60 * 1000:
                timeStr = "1 week";
                timervalue = 6;
                break;
        }
        tvSecretTimer.setText(timeStr);
    }

    private void writeBufferToFile(JSONObject object) {

        try {
            String fileName = object.getString("ImageName");
            String localPath = object.getString("LocalPath");
//            String localFileName = object.getString("LocalFileName");
            String end = object.getString("end");
            int bytesRead = object.getInt("bytesRead");
            int fileSize = object.getInt("filesize");
            String msgId = object.getString("MsgId");

            for (int i = 0; i < mChatData.size(); i++) {
                if (msgId.equalsIgnoreCase(mChatData.get(i).getMessageId())) {
                    int progress = (bytesRead / fileSize) * 100;
                    mChatData.get(i).setUploadDownloadProgress(progress);

                    if (end.equalsIgnoreCase("1")) {
                        mChatData.get(i).setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_COMPLETED);
//                        db.updateMessageDownloadStatus(docId, msgId, MessageFactory.DOWNLOAD_STATUS_COMPLETED);
                    }
                    break;
                }
            }

            mAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }

    private void sentTypeEvent() {
        boolean blockedStatus = isBlockedUser();
        if (!blockedStatus) {
            JSONObject msgObj = new JSONObject();
            SendMessageEvent messageEvent = new SendMessageEvent();
            messageEvent.setEventName(SocketManager.EVENT_TYPING);
            try {
                msgObj.put("from", from);
                msgObj.put("to", to);
                msgObj.put("convId", mConvId);
                msgObj.put("type", "single");
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
            messageEvent.setMessageObject(msgObj);
            EventBus.getDefault().post(messageEvent);
        }
    }

    private void handleTypingEvent(int length) {
        if (length > 0) {
            long currentTime = Calendar.getInstance().getTimeInMillis();
            long timeDiff = currentTime - fromLastTypedAt;
            if (timeDiff > MessageFactory.TYING_MESSAGE_MIN_TIME_DIFFERENCE) {
                fromLastTypedAt = currentTime;
                sentTypeEvent();
            }
        }
    }

    private void checkLinkDetails(String text) {

        text = " " + text;
        String[] arrSplitText = text.split(" ");
        for (final String splitText : arrSplitText) {

            if (Patterns.WEB_URL.matcher(splitText).matches()) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String url = splitText;
                            if (!splitText.startsWith("http")) {
                                url = "http://" + splitText;
                            }
                            final Document doc = Jsoup.connect(url).get();

                            Elements metaElems = doc.select("meta");
//                            webLinkDesc = metaElems.tagName("description").first().attr("content");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    webLinkTitle = doc.title();
                                    webLink = doc.baseUri();
                                    webLinkImgUrl = webLink + "/favicon.ico";
//                                    webLinkImgUrl = webLink.substring(0, webLink.lastIndexOf("/")) + "/favicon.ico";
//                                            webLinkImgUrl = doc.head().select("meta[itemprop=image]").first().attr("content");
                                    tvWebTitle.setText(webLinkTitle);
                                    tvWebLink.setText(webLink);
//                                    tvWebLinkDesc.setText(webLinkDesc);
                                    Picasso.with(SecretChatViewActivity.this).load(webLinkImgUrl).into(ivWebLink);

                                    if (rlWebLink.getVisibility() == View.GONE) {
                                        rlWebLink.setVisibility(View.VISIBLE);
                                    }
                                }
                            });

                            hasLinkInfo = true;
                                    /*String keywords = doc.select("meta[name=keywords]").first().attr("content");
                                    Log.e("Meta keyword : ", keywords);
                                    String description = doc.select("meta[name=description]").first().attr("content");
                                    Log.e("Meta description : ", description);*/
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        } catch (NullPointerException nullEx) {
                            hasLinkInfo = false;
                            nullEx.printStackTrace();
                        } catch (Exception e) {
                            hasLinkInfo = false;
                            MyLog.e(TAG, "", e);
                        }
                    }
                }).start();
                break;
            } else {
                hasLinkInfo = false;
            }

        }


    }

    private void initData() {
        mChatData = new ArrayList<>();
        uploadDownloadManager = new FileUploadDownloadManager(SecretChatViewActivity.this);

        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        //recyclerView_chat.setLayoutManager(mLayoutManager);
        //recyclerView_chat.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new ChatMessageAdapter(true, this, mChatData, SecretChatViewActivity.this.getSupportFragmentManager(), this);
        recyclerView_chat.setAdapter(mAdapter);
        //recyclerView_chat.setHasFixedSize(true);

        mSessionManager = SessionManager.getInstance(this);

        String userId = mSessionManager.getPhoneNumberOfCurrentUser();
        String userName = mSessionManager.getnameOfCurrentUser();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            backfrom = bundle.getBoolean("backfrom");
            receiverUid = bundle.getString("receiverUid");
            msgid = bundle.getString("msgid", "");
            String receiverName = bundle.getString("receiverName");
            to = bundle.getString("documentId");
            mReceiverId = to;
            if (session.getmark(mReceiverId)) {
                session.Removemark(mReceiverId);
            }
            mReceiverName = bundle.getString("Username");
            receiverAvatar = bundle.getString("Image");
            receiverMsisdn = bundle.getString("msisdn");
            mReceiverName = getcontactname.getSendername(mReceiverId, receiverMsisdn);
            startwith_name.setText(getString(R.string.you_started_a_secret_chat_with) + " " + mReceiverName);
            profilepicupdation();
           /* if (receiverAvatar != null) {
                if (!receiverAvatar.startsWith(Constants.SOCKET_IP)) {
                    receiverAvatar = Constants.SOCKET_IP.concat(receiverAvatar);
                }
                if (session.getidprofilepic(to) != 2) {
                    if (session.getidmycontact_profilephoto(to)) {

                        Picasso.with(ChatViewActivity.this).load(receiverAvatar)
                                .into(ivProfilePic, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                    }

                                    @Override
                                    public void onError() {
                                        ivProfilePic.setBackgroundResource(R.mipmap.chat_attachment_profile_default_image_frame);

                                    }
                                });
                    } else {
                        ivProfilePic.setBackgroundResource(R.mipmap.chat_attachment_profile_default_image_frame);
                    }
                } else {
                    ivProfilePic.setBackgroundResource(R.mipmap.chat_attachment_profile_default_image_frame);
                }
            }*/

        }
        if (mReceiverName != null && !mReceiverName.isEmpty())
            receiverNameText.setText(mReceiverName);
        else
            receiverNameText.setText(receiverMsisdn);

        from = mSessionManager.getCurrentUserID();
        mCurrentUserId = mSessionManager.getCurrentUserID();

        sendButton.setOnClickListener(this);
        attachment.setOnClickListener(this);
        captureImage.setOnClickListener(this);
        selEmoji.setOnClickListener(this);
    }

    private void getReceiverOnlineTimeStatus() {
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            object.put("to", to);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GET_CURRENT_TIME_STATUS);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadFromDB() {
        ArrayList<MessageItemChat> items;


        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);


        String timerData = contactDB_sqlite.getSecretMessageTimer(mReceiverId);
        String createdBy = mCurrentUserId;
        secretMsgTimer = String.valueOf(60 * 60 * 1000);
        secretMsgTimerMode = getResources().getString(R.string.time_1hr);
        if (timerData != null) {
            try {
                JSONObject object = new JSONObject(timerData);
                createdBy = object.getString(ContactDB_Sqlite.KEY_SECRET_TIMER_CREATED_BY);
                secretMsgTimer = object.getString(ContactDB_Sqlite.KEY_SECRET_TIMER);
                secretMsgTimerId = object.getString(ContactDB_Sqlite.KEY_SECRET_TIMER_ID);
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        }

        setSecretTimerText();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getReceiverOnlineTimeStatus();
            }
        }, 2000);

        mLocDbDocId = from + "-" + to + "-" + MessageFactory.CHAT_TYPE_SECRET;
        if (userInfoSession.hasChatConvId(mLocDbDocId)) {
            mConvId = userInfoSession.getChatConvId(mLocDbDocId);
        }

        MessageDbController db = CoreController.getDBInstance(this);
        serverTimeDiff = mSessionManager.getServerTimeDifference();
        items = db.selectAllSecretChatMessage(mLocDbDocId, serverTimeDiff);

        if (items.size() == 0) {
            String msgId = mCurrentUserId.concat("-").concat(mReceiverId).concat(Calendar.getInstance().getTimeInMillis() + "");

            MessageItemChat msgItem = new MessageItemChat();
            msgItem.setIsDate(true); // showing server message
            msgItem.setSecretTimer(secretMsgTimer);
            msgItem.setSecretTimeCreatedBy(createdBy);
            msgItem.setMessageType(MessageFactory.timer_change + "");
            msgItem.setIsInfoMsg(true);
            msgItem.setMessageId(msgId);
            msgItem.setTS(TimeStampUtils.getServerTimeStamp(SecretChatViewActivity.this, Calendar.getInstance().getTimeInMillis()));
            items.add(msgItem);
        }

        SmarttyContactModel contactModel = contactDB_sqlite.getUserOpponenetDetails(to);
        if (contactModel == null) {
            getUserDetails(to);
        }

        changeBadgeCount();
        mChatData.clear();
        mChatData.addAll(items);
        sendAllAcksToServer();
        notifyDatasetChange();


    }

    private void reloadAdapter() {
        try {
            ArrayList<MessageItemChat> items;
            MessageDbController db = CoreController.getDBInstance(this);
            serverTimeDiff = mSessionManager.getServerTimeDifference();
            items = db.selectAllSecretChatMessage(mLocDbDocId, serverTimeDiff);
            mChatData.clear();
            mChatData.addAll(items);
            sendAllAcksToServer();
            notifyDatasetChange();
        } catch (Exception e) {
            MyLog.e(TAG, "reloadAdapter: ", e);
        }
    }

    private void handleMessageDelete() {
        long currentTime = Calendar.getInstance().getTimeInMillis() - serverTimeDiff;
        ArrayList<MessageItemChat> removeUIItems = new ArrayList<>();
        String docId = mCurrentUserId + "-" + mReceiverId + "-" + MessageFactory.CHAT_TYPE_SECRET;

        if (removeCount > 10 && removeChatItems.size() > 0) {
            MessageDbController db = CoreController.getDBInstance(this);
            for (MessageItemChat removeDbItem : removeChatItems) {
                if (!removeDbItem.isDate()) {
                    notifyMessageDeleteToServer(removeDbItem.getConvId(), removeDbItem.getRecordId());
                }
                db.deleteChatMessage(docId, removeDbItem.getMessageId(), MessageFactory.CHAT_TYPE_SECRET);
            }

            removeCount = 0;
            removeChatItems.clear();
        } else {
            removeCount++;
        }

        try {

            for (int i = 0; i < mChatData.size(); i++) {
                MessageItemChat msgItem = mChatData.get(i);

                if (msgItem.isDate()) {
                    int nextIndex = i + 1;
                    if (!msgItem.getMessageId().equalsIgnoreCase(secretMsgTimerId) && nextIndex < mChatData.size()) {
                        MessageItemChat nextItem = mChatData.get(nextIndex);
                        if (msgItem.isDate() == nextItem.isDate()) {
                            removeChatItems.add(msgItem);
                            removeUIItems.add(msgItem);
                        }
                    }
                } else {
                    if (!msgItem.isDate() && !msgItem.isSelf() && msgItem.getDeliveryStatus().equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ)) {
                        String strReadAt = msgItem.getSecretMsgReadAt();
                        String strTimer = msgItem.getSecretTimer();

                        long readAt = AppUtils.parseLong(strReadAt);
                        long timer = AppUtils.parseLong(strTimer);
                        long timeDiff = currentTime - readAt;

                        if (timeDiff >= timer) {
                            if (secretMsgTimerId != null && secretMsgTimerId.equalsIgnoreCase(msgItem.getMessageId())) {

                            } else {
                                if (msgItem.isMediaPlaying()) {
                                    mAdapter.stopAudioOnClearChat();
                                }
                                removeUIItems.add(msgItem);
                                removeChatItems.add(msgItem);
                            }
                        }
                    } else if (!msgItem.isDate() && msgItem.isSelf() && !msgItem.getDeliveryStatus().equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_NOT_SENT)) {
                        String strSentAt = msgItem.getMsgSentAt();
                        String strTimer = msgItem.getSecretTimer();
                        long sentAt = AppUtils.parseLong(strSentAt);
                        long timer = AppUtils.parseLong(strTimer);
                        long timeDiff = currentTime - sentAt;

                        if (timeDiff >= timer) {
                            if (secretMsgTimerId != null && secretMsgTimerId.equalsIgnoreCase(msgItem.getMessageId())) {

                            } else {
                                if (msgItem.isMediaPlaying()) {
                                    mAdapter.stopAudioOnClearChat();
                                }

                                removeUIItems.add(msgItem);
                                removeChatItems.add(msgItem);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

        for (MessageItemChat removeItem : removeUIItems) {
            mChatData.remove(removeItem);
        }

        if (removeUIItems.size() > 0) {
            mAdapter.notifyDataSetChanged();
        }
    }

    private void notifyMessageDeleteToServer(String convId, String recordId) {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_REMOVE_MESSAGE);
        try {
            JSONObject deleteMsgObj = new JSONObject();
            deleteMsgObj.put("from", from);
            deleteMsgObj.put("type", MessageFactory.CHAT_TYPE_SINGLE);
            deleteMsgObj.put("convId", convId);
            deleteMsgObj.put("status", "1");
            deleteMsgObj.put("recordId", recordId);
            deleteMsgObj.put("last_msg", "0");
            messageEvent.setMessageObject(deleteMsgObj);
            EventBus.getDefault().post(messageEvent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void sendAllAcksToServer() {
        // For sent & receive acks
        MessageItemChat lastDeliveredItem = null;
        JSONArray arrSentMsgRecordIds = new JSONArray();

        MessageDbController db = CoreController.getDBInstance(this);
        String docId = mCurrentUserId + "-" + mReceiverId + "-" + MessageFactory.CHAT_TYPE_SECRET;
        long serverTS = Calendar.getInstance().getTimeInMillis() - serverTimeDiff;

        for (int i = 0; i < mChatData.size(); i++) {
            MessageItemChat msgItem = mChatData.get(i);
            String msgStatus = msgItem.getDeliveryStatus();

            if (msgItem.getConvId() != null && !msgItem.getConvId().equals("")) {
                mConvId = msgItem.getConvId();
            }

            if (msgStatus != null) {
                if (!msgItem.isSelf() && !msgStatus.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ)) {
                    lastDeliveredItem = msgItem;
                    mChatData.get(i).setDeliveryStatus(MessageFactory.DELIVERY_STATUS_READ);

                    mChatData.get(i).setSecretMsgReadAt(serverTS + "");
                    db.updateSecretMessageReadAt(docId, msgItem.getMessageId(), serverTS);
                    Log.d(TAG, "updateChatMessageFrom8 ");
                    db.updateChatMessage(docId, msgItem.getMessageId(), MessageFactory.DELIVERY_STATUS_READ,
                            serverTS + "", false);
                } else if (msgItem.isSelf() && (msgStatus.equals(MessageFactory.DELIVERY_STATUS_SENT)
                        || msgStatus.equals(MessageFactory.DELIVERY_STATUS_DELIVERED))) {
                    if (msgItem.getRecordId() != null && arrSentMsgRecordIds.length() < 100) {
                        arrSentMsgRecordIds.put(msgItem.getRecordId());
                    }
                }
            }

            // For getting reply message details from server
            if (!msgItem.isSelf() && msgItem.getReplyId() != null && !msgItem.getReplyId().equals("")
                    && (msgItem.getReplyType() == null || msgItem.getReplyType().equals(""))) {
                getReplyMessageDetails(mReceiverId, msgItem.getReplyId(), MessageFactory.CHAT_TYPE_SINGLE, "yes", msgItem.getMessageId());
            }
        }

        if (lastDeliveredItem != null) {
            if (mSessionManager.canSendReadReceipt()) {
                String msgId = lastDeliveredItem.getMessageId().split("-")[2];
                String ackDocId = to.concat("-").concat(mCurrentUserId).concat("-").concat(msgId);
                sendAckToServer(to, ackDocId, msgId);

                sendViewedStatusToWeb(lastDeliveredItem);
            }
            mAdapter.notifyDataSetChanged();
        }

        if (arrSentMsgRecordIds.length() > 0) {
            getMessageInfo(arrSentMsgRecordIds);
        }
    }

    public void getReplyMessageDetails(String toUserId, String recordId, String chatType,
                                       String secretType, String msgId) {
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            object.put("to", toUserId);
            object.put("recordId", recordId);
            object.put("requestMsgId", msgId);
            object.put("type", chatType);
            object.put("secret_type", secretType);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GET_MESSAGE_DETAILS);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    //bug in web - no need now
    private void getMessageInfo(JSONArray arrSentMsgRecordIds) {

        try {
            JSONObject object = new JSONObject();
            object.put("recordId", arrSentMsgRecordIds);
            object.put("from", mCurrentUserId);
            object.put("type", MessageFactory.CHAT_TYPE_SINGLE);

            SendMessageEvent infoEvent = new SendMessageEvent();
            infoEvent.setEventName(SocketManager.EVENT_GET_MESSAGE_INFO);
            infoEvent.setMessageObject(object);
            //EventBus.getDefault().post(infoEvent);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void sendViewedStatusToWeb(MessageItemChat lastDeliveredItem) {
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            object.put("convId", lastDeliveredItem.getConvId());
            object.put("type", MessageFactory.CHAT_TYPE_SINGLE);
            object.put("mode", "phone");
            object.put("chat_type", MessageFactory.CHAT_TYPE_SECRET);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_VIEW_CHAT);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void changeBadgeCount() {
//        session.Removemark(to);

        if (mConvId != null && !mConvId.equals("")) {
            ShortcutBadgeManager shortcutBadgeMgnr = new ShortcutBadgeManager(SecretChatViewActivity.this);
            shortcutBadgeMgnr.removeMessageCount(mConvId, msgid);
            int totalCount = shortcutBadgeMgnr.getTotalCount();

            // Badge working if supported devices
            if (totalCount > 0) {
                try {
                    ShortcutBadger.applyCount(context, totalCount);
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            }

        }
    }

    public void openDialog(View include) {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View inflatedView;
        inflatedView = layoutInflater.inflate(R.layout.custom_dialog_options_menu, null, false);

        LinearLayout ll1Row, ll2Row;
        ll1Row = inflatedView.findViewById(R.id.ll1Row);
        ll2Row = inflatedView.findViewById(R.id.ll2Row);

        int menuBackColor = ContextCompat.getColor(SecretChatViewActivity.this, R.color.secret_title);
        ll1Row.setBackgroundColor(menuBackColor);
        ll2Row.setBackgroundColor(menuBackColor);

        final LinearLayout layoutGallery, layoutDocument, layoutVideo, layoutContacts, layoutAudio, layoutLocation;
        TextView tvGallery, tvDocument, tvVideo, tvContacts, tvAudio, tvLocation;
        tvGallery = inflatedView.findViewById(R.id.tvGallery);
        tvDocument = inflatedView.findViewById(R.id.tvDocument);
        tvVideo = inflatedView.findViewById(R.id.tvVideo);
        tvAudio = inflatedView.findViewById(R.id.tvAudio);
        tvContacts = inflatedView.findViewById(R.id.tvContact);
        tvLocation = inflatedView.findViewById(R.id.tvLocation);

        tvGallery.setTextColor(Color.WHITE);
        tvDocument.setTextColor(Color.WHITE);
        tvVideo.setTextColor(Color.WHITE);
        tvAudio.setTextColor(Color.WHITE);
        tvContacts.setTextColor(Color.WHITE);
        tvLocation.setTextColor(Color.WHITE);
        layoutGallery = inflatedView.findViewById(R.id.layoutGallery);
        layoutGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FloatingView.dismissWindow();
                /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    Intent photoPickerIntent = new Intent();
                    photoPickerIntent.setType("image*//*");
                    photoPickerIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(photoPickerIntent, RESULT_LOAD_IMAGE);
                } else {
                    Intent intent = new Intent();
                    intent.setType("image*//*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    startActivityForResult(intent, RESULT_LOAD_IMAGE);
                }*/

                Intent i = new Intent(SecretChatViewActivity.this, ImagecaptionActivity.class);
                i.putExtra("phoneno", mReceiverName);
                i.putExtra("from", "Gallary");
                startActivityForResult(i, RESULT_LOAD_IMAGE);

            }
        });
        layoutDocument = inflatedView.findViewById(R.id.layoutDocument);
        layoutDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FloatingView.dismissWindow();

                FilePickerBuilder.Companion.getInstance().setMaxCount(1)
                        .setSelectedFiles(new ArrayList<String>())
                        //.setActivityTheme(R.style.AppTheme)
                        .sortDocumentsBy(SortingTypes.name)
                        .pickFile(SecretChatViewActivity.this);

                /*Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);//mms quality video not hd
                intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);//max 120s video
                intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 26214400L);//max 2.5 mb size recording
                startActivityForResult(intent, RESULT_CAPTURE_VIDEO);*/
            }
        });
        layoutVideo = inflatedView.findViewById(R.id.layoutVideo);
        layoutVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FloatingView.dismissWindow();
                Intent i = new Intent(SecretChatViewActivity.this, ImagecaptionActivity.class);
                i.putExtra("phoneno", mReceiverName);
                i.putExtra("from", "Video");
                startActivityForResult(i, RESULT_LOAD_VIDEO);
            }
        });
        layoutContacts = inflatedView.findViewById(R.id.layoutContact);
        layoutContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FloatingView.dismissWindow();
                Intent intentContact = new Intent(SecretChatViewActivity.this, SendContact.class);
                startActivityForResult(intentContact, REQUEST_CODE_CONTACTS);
            }
        });

        layoutAudio = inflatedView.findViewById(R.id.layoutAudio);
        layoutAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FloatingView.dismissWindow();
                /*Intent intent_upload = new Intent();
                intent_upload.setType("audio*//*");
                intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent_upload, REQUEST_SELECT_AUDIO);*/
                Intent audioIntent = new Intent(SecretChatViewActivity.this, AudioFilesListActivity.class);
                startActivityForResult(audioIntent, REQUEST_SELECT_AUDIO);
            }
        });
        layoutLocation = inflatedView.findViewById(R.id.layoutLocation);
        layoutLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FloatingView.dismissWindow();
                //          GPSTracker gps = new GPSTracker(getApplicationContext());
//                if (gps.canGetLocation()) {

                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(SecretChatViewActivity.this), RESULT_SHARE_LOCATION);
                } catch (GooglePlayServicesRepairableException e) {
                    MyLog.e(TAG, "", e);
                } catch (GooglePlayServicesNotAvailableException e) {
                    MyLog.e(TAG, "", e);
                }
                /*} else {
                    View clSnackBar = findViewById(R.id.clSnackBar);
                    if (clSnackBar != null) {
                        Snackbar snackbar = Snackbar.make(clSnackBar, "Please Enable GPS", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        View view2 = snackbar.getView();
                        TextView txtv = (TextView) view2.findViewById(android.support.design.R.id.snackbar_text);
                        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                    }
                }*/
            }
        });
        FloatingView.onShowPopup(this, inflatedView, include);
    }


    //------------------------------Video Download-------------------------------------------

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    //-------------------------------------Audio Download---------------------------------------

    @Override
    public void onStop() {
        super.onStop();

        if (toTypingRunnable != null) {
            toTypingHandler.removeCallbacks(toTypingRunnable);
        }
        ScreenShotDetector.setListener(null);
        EventBus.getDefault().unregister(this);
    }

    //-------------------------------------Document Download----------------------------------------

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.enter_chat1) {


            if (isBlockedUser()) {
                DisplayAlert("Unblock" + " " + mReceiverName + " " + "to send message?");
            } else {
                if ((!hasLinkInfo && !reply)) {
                    reply = false;
                    sendTextMessage();
                } else if (reply) {
                    r1messagetoreplay.setVisibility(View.GONE);
                    sendparticularmsgreply();
                } else {
                    sendWebLinkMessage();
                }
            }

        } else if (view.getId() == R.id.attachment) {
            if (isBlockedUser()) {
                DisplayAlert(to);
            } else {
                openDialog(include);
            }
        } else if (view.getId() == R.id.iBtnBack) {
            goSecrectChatListPage();
        } else if (view.getId() == R.id.nameMAincontainer) {
            goInfoPage();
        } else if (view.getId() == R.id.capture_image) {
            if (isBlockedUser()) {
                DisplayAlert(to);
            } else {
                Intent i = new Intent(SecretChatViewActivity.this, ImagecaptionActivity.class);
                i.putExtra("phoneno", mReceiverName);
                i.putExtra("from", "Camera");
                startActivityForResult(i, CAMERA_REQUEST);
            }

        } else if (view.getId() == R.id.emojiButton) {
            //System.out.println("===================================>simly");
            emojIcon.ShowEmojIcon();
        } else if (view.getId() == R.id.iBtnScroll) {
            if (mChatData.size() > 0)
                recyclerView_chat.setSelection(mChatData.size() - 1);
        } else if (view.getId() == R.id.ivWebLinkClose) {
            if (rlWebLink.getVisibility() == View.VISIBLE) {
                rlWebLink.setVisibility(View.GONE);
            }
        }
    }

    //----------------------------------Image Download----------------------------
    public void imagedownloadmethod(int position) {


        MessageItemChat item = mChatData.get(position);
        if (item.getUploadDownloadProgress() == 0 && !item.isSelf() && item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START) {
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        } else if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START
                && item.isSelf()) {
            /*Download option for sent documents from web chat*/
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        }
    }

    public void VideoDownload(int position) {

        MessageItemChat item = mChatData.get(position);
        if (item.getUploadDownloadProgress() == 0 && !item.isSelf() &&
                item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START) {
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);


            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);

           /* String fullPath = item.getChatFileServerPath();

            String message_id=item.getMessageId();

            DownloadServiceClass.DownloadFileFromURL download=new DownloadServiceClass.DownloadFileFromURL(this);
            download.execute(fullPath,message_id);*/

            // download.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,fullPath,message_id);


        } else if (!item.isSelf() && item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
            try {
                String videoPath = item.getChatFileLocalPath();
                File file = new File(item.getChatFileLocalPath());
                if (!file.exists()) {
                    try {

                        String[] filePathSplited = item.getChatFileLocalPath().split(File.separator);
                        String fileName = filePathSplited[filePathSplited.length - 1];
                        String publicDirPath = Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();

                        videoPath = publicDirPath + File.separator + fileName;
                    } catch (Exception e) {
                        MyLog.e(TAG, "configureViewHolderImageReceived: ", e);
                    }
                }

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoPath));
                String path = "file://" + videoPath;
                intent.setDataAndType(Uri.parse(path), "video/*");
                startActivity(intent);

            } catch (ActivityNotFoundException e) {
                Toast.makeText(this, "No app installed to play this video", Toast.LENGTH_LONG).show();
            }
        } else if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START
                && item.isSelf()) {
            /*Download option for sent documents from web chat*/
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        }

    }

    public void AudioDownload(int position) {

        MessageItemChat item = mChatData.get(position);
        if (item.getUploadDownloadProgress() == 0 && !item.isSelf() && item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START) {
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        } else if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START
                && item.isSelf()) {
            /*Download option for sent documents from web chat*/
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        }
    }

    public void DocumentDownload(int position) {

        MessageItemChat item = mChatData.get(position);
        if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START && !item.isSelf()) {
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        } else if (item.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START
                && item.isSelf()) {
            /*Download option for sent documents from web chat*/
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_DOWNLOADING);
            uploadDownloadManager.startFileDownload(EventBus.getDefault(), item, false);
        } else {
            String extension = MimeTypeMap.getFileExtensionFromUrl(item.getChatFileLocalPath());
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

            PackageManager packageManager = getPackageManager();
            Intent testIntent = new Intent(Intent.ACTION_VIEW);
            testIntent.setType(mimeType);
            try {

                List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
                if (list.size() > 0) {
                    File file = new File(item.getChatFileLocalPath());
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(file), mimeType);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "No app installed to view this document", Toast.LENGTH_LONG).show();
                }
            } catch (ActivityNotFoundException e) {
                Toast.makeText(this, "No app installed to view this document", Toast.LENGTH_LONG).show();
            }


        }

    }

    private void sendTimerChangeMessage(String timeMsg) {
        SendMessageEvent messageEvent = new SendMessageEvent();
        TimerChangeMessage message = (TimerChangeMessage) MessageFactory.getMessage(MessageFactory.timer_change, this);
        JSONObject msgObj = (JSONObject) message.getMessageObject(to, timeMsg, secretMsgTimer, true);
        messageEvent.setEventName(SocketManager.EVENT_CHANGE_SECRET_MSG_TIMER);

        MessageItemChat item = message.createMessageItem(true, timeMsg, MessageFactory.DELIVERY_STATUS_NOT_SENT, mReceiverId,
                receiverNameText.getText().toString(), secretMsgTimer + "");
        messageEvent.setMessageObject(msgObj);
        MessageDbController db = CoreController.getDBInstance(this);

        item.setSenderMsisdn(receiverMsisdn);
        item.setSenderName(receiverNameText.getText().toString());
        secretMsgTimerId = item.getMessageId();

        //new dbsqlite
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        contactDB_sqlite.updateSecretMessageTimer(mReceiverId, secretMsgTimer, mCurrentUserId, item.getMessageId());

        db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SECRET);
        if (!isAlreadyExist(item))
            mChatData.add(item);
        EventBus.getDefault().post(messageEvent);
        notifyDatasetChange();
    }

    private void sendTextMessage() {
        String data = sendMessage.getText().toString().trim();

        if (!data.equalsIgnoreCase("")) {

            SendMessageEvent messageEvent = new SendMessageEvent();
            TextMessage message = (TextMessage) MessageFactory.getMessage(MessageFactory.text, this);
            JSONObject msgObj = (JSONObject) message.getMessageObject(to, data, true);
            messageEvent.setEventName(SocketManager.EVENT_MESSAGE);

            MessageItemChat item = message.createMessageItem(true, data, MessageFactory.DELIVERY_STATUS_NOT_SENT, mReceiverId, receiverNameText.getText().toString());
            //Check if it blocked
            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
            if (contactDB_sqlite.getBlockedMineStatus(to, true).equals(ContactDB_Sqlite.BLOCKED_STATUS)) {
                item.setisBlocked(true);
            }
            messageEvent.setMessageObject(msgObj);
            MessageDbController db = CoreController.getDBInstance(this);

            item.setSecretTimer(secretMsgTimer);
            item.setSecretTimeCreatedBy(mCurrentUserId);
            item.setSenderMsisdn(receiverMsisdn);
            item.setSenderName(receiverNameText.getText().toString());
            db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SECRET);
            if (!isAlreadyExist(item))
                mChatData.add(item);
            EventBus.getDefault().post(messageEvent);
            notifyDatasetChange();
            sendMessage.getText().clear();
        }
    }

    private void sendparticularmsgreply() {
        String data = sendMessage.getText().toString().trim();
        if (!data.equalsIgnoreCase("")) {
            if (session.getarchivecount() != 0) {
                if (session.getarchive(from + "-" + to))
                    session.removearchive(from + "-" + to);
            }
            if (session.getarchivecountgroup() != 0) {
                if (session.getarchivegroup(from + "-" + to + "-g"))
                    session.removearchivegroup(from + "-" + to + "-g");
            }
            SendMessageEvent messageEvent = new SendMessageEvent();
            TextMessage message = (TextMessage) MessageFactory.getMessage(MessageFactory.text, this);
            messageEvent.setEventName(SocketManager.EVENT_REPLY_MESSAGE);
            JSONObject msgObj = (JSONObject) message.getMessageObject(to, data, true);
            try {
                msgObj.put("recordId", mymsgid);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }

            messageEvent.setMessageObject(msgObj);
            MessageItemChat item = message.createMessageItem(true, data,
                    MessageFactory.DELIVERY_STATUS_NOT_SENT, mReceiverId, receiverNameText.getText().toString());
            item.setReplyType(replytype);
            if (Integer.parseInt(replytype) == MessageFactory.text) {
                item.setReplyMessage(messageold);
            } else if (Integer.parseInt(replytype) == MessageFactory.picture) {
                item.setreplyimagepath(imgpath);
            } else if (Integer.parseInt(replytype) == MessageFactory.audio) {
                item.setReplyMessage("Audio");
            } else if (Integer.parseInt(replytype) == MessageFactory.video) {
                item.setReplyMessage("Video");
                item.setreplyimagebase64(imageAsBytes);
            } else if (Integer.parseInt(replytype) == MessageFactory.document) {
                item.setReplyMessage(messageold);
            } else if (Integer.parseInt(replytype) == MessageFactory.web_link) {
                item.setReplyMessage(messageold);
            } else if (Integer.parseInt(replytype) == MessageFactory.contact) {
                item.setReplyMessage(contactname);
            }

            item.setSenderMsisdn(receiverMsisdn);
            item.setSenderName(mReceiverName);
            item.setReplySender(ReplySender);
            reply = false;

            MessageDbController db = CoreController.getDBInstance(this);

            item.setSecretTimer(secretMsgTimer);
            item.setSecretTimeCreatedBy(mCurrentUserId);
            item.setSenderMsisdn(receiverMsisdn);
            item.setSenderName(receiverNameText.getText().toString());
            db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SECRET);
            if (!isAlreadyExist(item))
                mChatData.add(item);
            EventBus.getDefault().post(messageEvent);
            notifyDatasetChange();
            sendMessage.getText().clear();
        }
    }

    private void goInfoPage() {

        showUnSelectedActions();
        selectedChatItems.clear();

        Intent infoIntent = new Intent(SecretChatViewActivity.this, UserInfo.class);
        infoIntent.putExtra("UserId", to);
        infoIntent.putExtra("UserName", receiverNameText.getText().toString());
        infoIntent.putExtra("UserAvatar", receiverAvatar);
        infoIntent.putExtra("UserNumber", receiverMsisdn);
        infoIntent.putExtra("FromSecretChat", true);
        startActivityForResult(infoIntent, MUTE_ACTIVITY);
    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId) {
        iconToBeChanged.setImageResource(drawableResourceId);
    }

    private void notifyDatasetChange() {

        mAdapter.notifyDataSetChanged();
        if (mChatData.size() > 0)
            recyclerView_chat.setSelection(mChatData.size() - 1);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {

        switch (event.getEventName()) {

            case SocketManager.EVENT_MESSAGE_RES:
                loadMessageRes(event);
                break;

            case SocketManager.EVENT_PRIVACY_SETTINGS:
                loadPrivacySetting(event);
                break;

            case SocketManager.EVENT_MESSAGE:
                loadMessage(event);
                break;

            case SocketManager.EVENT_MESSAGE_STATUS_UPDATE:
                loadMessageStatusupdate(event);
                break;

            case SocketManager.EVENT_GET_MESSAGE:
                loadMessage(event);
                break;

            case SocketManager.EVENT_BLOCK_USER:
                blockunblockcontact(event);
                break;

            case SocketManager.EVENT_REPORT_SPAM_USER:
                try {
                    Object[] obj = event.getObjectsArray();
                    JSONObject object = new JSONObject(obj[0].toString());
                    MyLog.e("Report Response---", object.toString());
                    Toast.makeText(this, "Contact is reported as spam ", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
                break;

            case Socket.EVENT_CONNECT:
                if (!isLastSeenCalled) {
                    getReceiverOnlineTimeStatus();
                }
                break;

            case SocketManager.EVENT_CLEAR_CHAT:
                Object[] obj = event.getObjectsArray();
                load_clear_chat(obj[0].toString());
                break;

            case SocketManager.EVENT_TYPING:
                loadTypingStatus(event);
                break;

            case SocketManager.EVENT_CAHNGE_ONLINE_STATUS:
                loadOnlineStatus(event);
                break;

            case SocketManager.EVENT_GET_CURRENT_TIME_STATUS:
                loadCurrentTimeMessage(event);
                break;

            case SocketManager.EVENT_STAR_MESSAGE:
                loadStarredMessage(event);
                break;

            case SocketManager.EVENT_REMOVE_MESSAGE:
                loadDeleteMessage(event);
                break;

            case SocketManager.EVENT_FILE_RECEIVED:
                try {
                    Object[] objects = event.getObjectsArray();
                    JSONObject object = new JSONObject(objects[0].toString());
                    loadFileUploaded(object);
                    MyLog.d("Received Response", object.toString());
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
                break;

            case SocketManager.EVENT_START_FILE_DOWNLOAD:
                try {
                    Object[] response = event.getObjectsArray();
                    JSONObject jsonObject = (JSONObject) response[1];
                    writeBufferToFile(jsonObject);
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
                break;

            case SocketManager.EVENT_GET_MESSAGE_DETAILS:
                loadReplyMessageDetails(event.getObjectsArray()[0].toString());
                break;


            case SocketManager.EVENT_IMAGE_UPLOAD:
                updateProfileImage(event);
                break;

        }

    }

    private void updateProfileImage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            String from = objects.getString("from");
            String type = objects.getString("type");

            if (type.equalsIgnoreCase("single") && to.equalsIgnoreCase(from)) {
                String path = objects.getString("file") + "?id=" + Calendar.getInstance().getTimeInMillis();
                MyLog.d(TAG, "updateProfileImage: " + path);
                profilepicupdation();
            }
        } catch (Exception e) {
            MyLog.e(TAG, "updateProfileImage: ", e);
        }
    }

    private void loadReplyMessageDetails(String data) {
        try {
            JSONObject object = new JSONObject(data);
            String err = object.getString("err");
            JSONObject dataObj = object.getJSONObject("data");
            String from = dataObj.getString("from");
            String convId = dataObj.getString("convId");

            if (err.equals("0") && convId.equalsIgnoreCase(mConvId)) {

                String to = dataObj.getString("to");
                String requestMsgId = dataObj.getString("requestMsgId");
                String chatType = dataObj.getString("type");

                String docId = from + "-" + to;
                if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                    docId = docId + "-g";
                } else {
                    String secretType = dataObj.getString("secret_type");
                    if (secretType.equalsIgnoreCase("yes")) {
                        docId = docId + "-secret";
                    }
                }

                try {
                    for (int i = 0; i < mChatData.size(); i++) {
                        MessageItemChat msgItem = mChatData.get(i);

                        if (msgItem.getMessageId().equalsIgnoreCase(requestMsgId)) {
                            MessageDbController db = CoreController.getDBInstance(this);
                            MessageItemChat modifiedItem = db.getParticularMessage(requestMsgId);
                            modifiedItem.setSelected(msgItem.isSelected());
                            mChatData.set(i, modifiedItem);
                            mAdapter.notifyDataSetChanged();
                            break;
                        }
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    MyLog.e(TAG, "", e);
                }
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void load_clear_chat(String data) {
        try {
            JSONObject object = new JSONObject(data);

            try {
                String from = object.getString("from");
                String convId = object.getString("convId");
                String type = object.getString("type");
                int star_status;
                Boolean starred = false;
                if (object.has("star_status")) {
                    star_status = object.getInt("star_status");
                    starred = star_status != 0;
                }
                if (from.equalsIgnoreCase(mCurrentUserId)) {
                    String receiverId;
                    if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                        receiverId = convId;
                    } else {
                        receiverId = userInfoSession.getReceiverIdByConvId(convId);
                    }

                    if (!receiverId.equals("")) {
                        String docId = from.concat("-").concat(receiverId);
                        if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                            docId = docId.concat("-g");
                        }
                        if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {

                            if (starred) {
                                ArrayList<MessageItemChat> value = new ArrayList<>();
                                for (int i = 0; i < mChatData.size(); i++) {
                                    if (mChatData.get(i).getStarredStatus().equalsIgnoreCase(MessageFactory.MESSAGE_UN_STARRED)) {
                                        value.add(mChatData.get(i));
                                    }
                                }

                                for (int i = 0; i < value.size(); i++) {
                                    if (value.get(i).isMediaPlaying()) {
                                        int chatIndex = mChatData.indexOf(value.get(i));
                                        if (chatIndex > -1 && mChatData.get(chatIndex).isMediaPlaying()) {
                                            mAdapter.stopAudioOnMessageDelete(chatIndex);
                                        }
                                    }
                                    mChatData.remove(value.get(i));
                                }

                            } else {
                                mAdapter.stopAudioOnClearChat();
                                mChatData.clear();

                            }
                        } else {

                            if (starred) {
                                ArrayList<MessageItemChat> value = new ArrayList<>();
                                for (int i = 0; i < mChatData.size(); i++) {
                                    if (mChatData.get(i).getStarredStatus().equalsIgnoreCase(MessageFactory.MESSAGE_UN_STARRED)) {
                                        value.add(mChatData.get(i));
                                    }
                                }

                                for (int i = 0; i < value.size(); i++) {
                                    if (value.get(i).isMediaPlaying()) {
                                        int chatIndex = mChatData.indexOf(value.get(i));
                                        if (chatIndex > -1 && mChatData.get(chatIndex).isMediaPlaying()) {
                                            mAdapter.stopAudioOnMessageDelete(chatIndex);
                                        }
                                    }
                                    mChatData.remove(value.get(i));
                                }

                            } else {
                                mChatData.clear();

                            }
                        }

                    }
                    mAdapter.notifyDataSetChanged();
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadFileUploaded(JSONObject object) {
        try {
            int uploadedSize = object.getInt("UploadedSize");
            int totalSize = object.getInt("size");
            String msgId = object.getString("id");

            int progress = (uploadedSize * 100) / totalSize;

            for (MessageItemChat msgItem : mChatData) {
                if (msgItem.getMessageId().equalsIgnoreCase(msgId)) {
                    int index = mChatData.indexOf(msgItem);
                    if (progress >= mChatData.get(index).getUploadDownloadProgress()) {
                        MyLog.d("Progressupdate", "" + progress);
                        mChatData.get(index).setUploadDownloadProgress(progress);
                        mAdapter.notifyDataSetChanged();
                    }

                    break;
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadDeleteMessage(ReceviceMessageEvent event) {

        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            int errorState = objects.getInt("err");
            if (errorState == 0) {
                String fromId = objects.getString("from");

                if (fromId.equalsIgnoreCase(from)) {
                    String deleteStatus = objects.getString("status");
                    String chat_id = (String) objects.get("doc_id");
                    String[] ids = chat_id.split("-");

                    String docId;
                    String msgId;
                    if (chat_id.contains("-g-")) {
                        docId = fromId + "-" + ids[1] + "-g";
                        msgId = docId + "-" + ids[3];
                    } else {
                        if (fromId.equalsIgnoreCase(ids[0])) {
                            docId = ids[0] + "-" + ids[1];
                        } else {
                            docId = ids[1] + "-" + ids[0];
                        }
                        msgId = docId + "-" + ids[2];
                    }

                    if (deleteStatus.equalsIgnoreCase("1")) {
//                        MessageItemChat item = db.deleteChatMessage(docId, msgId);
                        for (MessageItemChat items : mChatData) {
                            if (items != null && items.getMessageId().equalsIgnoreCase(msgId)) {
                                int index = mChatData.indexOf(items);
                                if (index > -1 && mChatData.get(index).isMediaPlaying()) {
                                    mAdapter.stopAudioOnMessageDelete(index);
                                }
                                removeChatItems.add(mChatData.get(index));
                                mChatData.remove(index);
                                mAdapter.notifyDataSetChanged();
                                break;
                            }
                        }

                    }
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void loadStarredMessage(ReceviceMessageEvent event) {

        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            int errorState = objects.getInt("err");
            if (errorState == 0) {
                String fromId = objects.getString("from");
                if (fromId.equalsIgnoreCase(from)) {
                    String chat_id = objects.getString("doc_id");
                    String[] ids = chat_id.split("-");
                    String docId;
                    if (fromId.equalsIgnoreCase(ids[0])) {
                        docId = ids[0] + "-" + ids[1];
                    } else {
                        docId = ids[1] + "-" + ids[0];
                    }

                    if (chat_id.contains("-g-")) {
                        docId = docId + "-g";
                    }

                    String starred = objects.getString("status");
//                    String msgId = docId + "-" + ids[2];

                    /*MessageDbController db = CoreController.getDBInstance(this);
                    MessageItemChat item = db.updateStarredMessage(docId, chat_id, starNextLine);
                    for (MessageItemChat items : mChatData) {
                        if (items != null && items.getMessageId().equalsIgnoreCase(item.getMessageId())) {
                            int index = mChatData.indexOf(items);
                            mChatData.get(index).setStarredStatus(starNextLine);
                            mChatData.get(index).setSelected(false);
                            mAdapter.notifyDataSetChanged();
                            break;
                        }
                    }*/
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_secret_chat_view, menu);

        if (isBlockedUser()) {
            menu.findItem(R.id.menuBlock).setTitle("Unblock");
        } else {
            menu.findItem(R.id.menuBlock).setTitle("Block");
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.menuBlock: {
                performMenuBlock();
            }
            break;

        }

        return super.onOptionsItemSelected(item);
    }

    private void performMenuBlock() {

        String msg;
        if (isBlockedUser()) {
            msg = "Do you want to Unblock " + mReceiverName + "?";
        } else {
            msg = "Block " + mReceiverName + "? Blocked contacts will no longer be able to call you or send you messages.";
        }

        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(msg);
        dialog.setPositiveButtonText("Ok");
        dialog.setNegativeButtonText("Cancel");
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                dialog.dismiss();

                putBlockUser();
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });

        dialog.show(getSupportFragmentManager(), "Block alert");

    }

    private void loadPrivacySetting(ReceviceMessageEvent event) {
        Object[] object = event.getObjectsArray();

        try {
            JSONObject jsonObject = new JSONObject(object[0].toString());
            String from = jsonObject.getString("from");
            String status = (String) jsonObject.get("status");
            String lastseen = String.valueOf(jsonObject.get("last_seen"));
            String profile = String.valueOf(jsonObject.get("profile_photo"));
            JSONArray contactUserList = jsonObject.getJSONArray("contactUserList");
            if (from.equalsIgnoreCase(mReceiverId)) {
                Boolean iscontact = false;
                if (contactUserList != null) {
                    iscontact = contactUserList.toString().contains(mCurrentUserId);
                }

                if (lastseen.equalsIgnoreCase("nobody")) {
                    canShowLastSeen = false;
                    if (!statusTextView.getText().toString().equalsIgnoreCase("online")) {
                        statusTextView.setVisibility(View.GONE);
                    }
                } else if (lastseen.equalsIgnoreCase("everyone")) {
                    statusTextView.setVisibility(View.VISIBLE);
                    canShowLastSeen = true;
                } else if (lastseen.equalsIgnoreCase("mycontacts") && iscontact) {
                    canShowLastSeen = true;
                    statusTextView.setVisibility(View.VISIBLE);
                } else {
                    canShowLastSeen = false;
                    if (!statusTextView.getText().toString().equalsIgnoreCase("online")) {
                        statusTextView.setVisibility(View.GONE);
                    }
                }

                profilepicupdation();
            }

        } catch (Exception e) {

        }
    }

    private void loadOnlineStatus(ReceviceMessageEvent event) {
        Object[] object = event.getObjectsArray();
        try {
            JSONObject jsonObject = new JSONObject(object[0].toString());
            String id = (String) jsonObject.get("_id");
            String status = String.valueOf(jsonObject.get("Status"));
            if (id.equalsIgnoreCase(to)) {
                if (status.equalsIgnoreCase("1")) {
                    statusTextView.setText("Online");
                } else {
                    if (canShowLastSeen && !mSessionManager.getLastSeenVisibleTo().equalsIgnoreCase("nobody")) {
                        String lastSeen = jsonObject.getString("DateTime");
                        setOnlineStatusText(lastSeen);
                    } else {
                        statusTextView.setText("");
                    }
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadCurrentTimeMessage(ReceviceMessageEvent event) {
        Object[] object = event.getObjectsArray();
        try {
            JSONObject jsonObject = new JSONObject(object[0].toString());
            String id = (String) jsonObject.get("_id");
            String status = String.valueOf(jsonObject.get("Status"));

            if (id.equalsIgnoreCase(to)) {
                isLastSeenCalled = true;
                canShowLastSeen = true;
                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(SecretChatViewActivity.this);
                JSONObject privacyObj = jsonObject.getJSONObject("Privacy");
                if (privacyObj.has("last_seen")) {

                    String showLastSeen = privacyObj.getString("last_seen");
                    if (showLastSeen.equalsIgnoreCase(ContactDB_Sqlite.PRIVACY_TO_NOBODY)) {
                        canShowLastSeen = false;
                        contactDB_sqlite.updateLastSeenVisibility(to, ContactDB_Sqlite.PRIVACY_STATUS_NOBODY);
                    } else if (showLastSeen.equalsIgnoreCase(ContactDB_Sqlite.PRIVACY_TO_MY_CONTACTS)) {
                        String isContactUser = jsonObject.getString("is_contact_user");

                        contactDB_sqlite.updateLastSeenVisibility(to, ContactDB_Sqlite.PRIVACY_STATUS_MY_CONTACTS);
                        contactDB_sqlite.updateMyContactStatus(to, isContactUser);
                        if (isContactUser.equals("0")) {
                            canShowLastSeen = false;
                        }
                    } else {
                        contactDB_sqlite.updateLastSeenVisibility(to, ContactDB_Sqlite.PRIVACY_STATUS_EVERYONE);
                    }
                }

                if (contactDB_sqlite.getBlockedMineStatus(to, false).equals(ContactDB_Sqlite.BLOCKED_STATUS)) {
                    canShowLastSeen = false;
                    statusTextView.setVisibility(View.GONE);
                }

                if ("1".equalsIgnoreCase(status)) {
                    statusTextView.setText("Online");
                } else {
                    if (canShowLastSeen && !mSessionManager.getLastSeenVisibleTo().equalsIgnoreCase("nobody")) {
                        String lastSeen = jsonObject.getString("DateTime");
                        setOnlineStatusText(lastSeen);
                    }
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadTypingStatus(ReceviceMessageEvent event) {
        Object[] object = event.getObjectsArray();
        try {
            JSONObject jsonObject = new JSONObject(object[0].toString());
            String from_typing = (String) jsonObject.get("from");
            String to_typing = (String) jsonObject.get("to");
            String convId = (String) jsonObject.get("convId");
            String type = (String) jsonObject.get("type");

            boolean blockedStatus = isBlockedUser();

            if (from_typing.equalsIgnoreCase(to) && type.equalsIgnoreCase("single") &&
                    convId.equalsIgnoreCase(mConvId) && !blockedStatus) {
                tvTyping.setText("typing...");
                tvTyping.setVisibility(View.VISIBLE);
                statusTextView.setVisibility(View.GONE);
                handleReceiverTypingEvent();
            }

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void handleReceiverTypingEvent() {
        toLastTypedAt = Calendar.getInstance().getTimeInMillis() + MessageFactory.TYING_MESSAGE_MIN_TIME_DIFFERENCE;
        toTypingHandler.postDelayed(toTypingRunnable, MessageFactory.TYING_MESSAGE_TIMEOUT);
    }

    private void getUserDetails(String userId) {
        try {
            JSONObject eventObj = new JSONObject();
            eventObj.put("userId", userId);
            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GET_USER_DETAILS);
            event.setMessageObject(eventObj);
            EventBus.getDefault().post(event);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }

    private void setOnlineStatusText(String lastSeen) {
        try {
            Long lastSeenTime = AppUtils.parseLong(lastSeen);
            Long serverDiff = mSessionManager.getServerTimeDifference();
            lastSeenTime = lastSeenTime + serverDiff;

            Date lastSeenAt = new Date(lastSeenTime);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
            Date date2 = new Date();
            String currentDate = sdf.format(date2);
            currentDate = currentDate.substring(0, 10);

            String strLastSeenAt = sdf.format(lastSeenAt);
            String onlineStatus;
            if (lastSeen != null) {
                if (currentDate.equals(strLastSeenAt.substring(0, 10))) {
                    lastSeen = SmarttyUtilities.convert24to12hourformat(strLastSeenAt.substring(11, 19));
                    onlineStatus = "Last seen at " + lastSeen;
                } else {
                    String last = SmarttyUtilities.convert24to12hourformat(strLastSeenAt.substring(11, 19));
                    String[] separated = strLastSeenAt.substring(0, 10).split("-");
                    String date = separated[2] + "-" + separated[1] + "-" + separated[0];
                    onlineStatus = "Last seen " + date + " " + last;
                }

                statusTextView.setText(onlineStatus);

            }
        } catch (Exception e) {
            statusTextView.setText("");
        }
    }

    private void loadMessageStatusupdate(ReceviceMessageEvent event) {
        Object[] object = event.getObjectsArray();

        try {
            JSONObject jsonObject = new JSONObject(object[0].toString());
            String from = jsonObject.getString("from");
            String to = jsonObject.getString("to");
            String msgIds = jsonObject.getString("msgIds");
            String doc_id = jsonObject.getString("doc_id");
            String status = jsonObject.getString("status");
            String secretType = jsonObject.getString("secret_type");
            if (from.equalsIgnoreCase(mReceiverId) && secretType.equalsIgnoreCase("yes")) {
//                String[] splitIds = doc_id.split("-");
//                doc_id = doc_id + "-secret-" + splitIds[2];

                if (mSessionManager.canSendReadReceipt() || !(status.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ))) {

                    for (int i = 0; i < mChatData.size(); i++) {
                        MessageItemChat items = mChatData.get(i);
                        if (items.isBlockedMsg())
                            continue;
                        if (items != null && items.getMessageId().equalsIgnoreCase(doc_id)) {
                            items.setDeliveryStatus("" + status);
                            mChatData.get(i).setDeliveryStatus(status);
                            break;
                        } else if (status.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_DELIVERED) && mChatData.get(i).isSelf()) {
                            if (mChatData.get(i).getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_SENT)) {
                                mChatData.get(i).setDeliveryStatus(status);
                            }
                        } else if (status.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ) && mChatData.get(i).isSelf() &&
                                !mChatData.get(i).getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_READ)) {
                            mChatData.get(i).setDeliveryStatus(status);
                        }
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CONTACTS && resultCode == RESULT_OK && data != null) {

            Bundle bundle = data.getExtras();
            contacts = (ArrayList<ContactToSend>) bundle.getSerializable("ContactToSend");
            String Uname = bundle.getString("name");
            String Title = bundle.getString("title");
            JSONArray phone = new JSONArray();
            JSONArray email = new JSONArray();
            JSONArray address = new JSONArray();
            JSONArray IM = new JSONArray();
            JSONArray Organisation = new JSONArray();
            JSONArray name = new JSONArray();

            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);

            ArrayList<SmarttyContactModel> smarttyEntries = contactDB_sqlite.getSavedSmarttyContacts();

            for (int i = 0; i < contacts.size(); i++) {
                if (contacts.get(i).getType().equalsIgnoreCase("Phone")) {

                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("type", contacts.get(i).getSubType());
                        jsonObject.put("value", contacts.get(i).getNumber());
                        phone.put(jsonObject);
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                } else if (contacts.get(i).getType().equalsIgnoreCase("Email")) {

                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("type", contacts.get(i).getSubType());
                        jsonObject.put("value", contacts.get(i).getNumber());
                        email.put(jsonObject);
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                } else if (contacts.get(i).getType().equalsIgnoreCase("Address")) {

                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("type", contacts.get(i).getSubType());
                        jsonObject.put("value", contacts.get(i).getNumber());
                        address.put(jsonObject);
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                } else if (contacts.get(i).getType().equalsIgnoreCase("Instant Messenger")) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("type", contacts.get(i).getSubType());
                        jsonObject.put("value", contacts.get(i).getNumber());
                        IM.put(jsonObject);
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                } else if (contacts.get(i).getType().equalsIgnoreCase("Organisation")) {

                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("type", contacts.get(i).getSubType());
                        jsonObject.put("value", contacts.get(i).getNumber());
                        Organisation.put(jsonObject);
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                } else if (contacts.get(i).getType().equalsIgnoreCase("Name")) {

                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("type", contacts.get(i).getSubType());
                        jsonObject.put("value", contacts.get(i).getNumber());
                        name.put(jsonObject);
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                }
            }
            MessageItemChat itemChat = new MessageItemChat();
            JSONObject finalObj = new JSONObject();
            try {
                finalObj.put("phone_number", phone);
                finalObj.put("email", email);
                finalObj.put("address", address);
                finalObj.put("im", IM);
                finalObj.put("organisation", Organisation);
                finalObj.put("name", name);
                ContactString = finalObj.toString();
                itemChat.setDetailedContacts(ContactString);
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }


            if (contacts.size() >= 1) {
                number = contacts.get(0).getNumber();
                number = number.replace(" ", "").replace("(", "").replace(")", "").replace("-", "");
            } else {
                number = "";
            }
            String contactSmarttyId = "";

            for (SmarttyContactModel smarttyModel : smarttyEntries) {
                if ((smarttyModel.getNumberInDevice() != null && smarttyModel.getNumberInDevice().equalsIgnoreCase(number))
                        || (smarttyModel.getMsisdn() != null && smarttyModel.getMsisdn().equalsIgnoreCase(number))) {
                    contactSmarttyId = smarttyModel.get_id();
                    break;
                }
            }

            // Check whether contact is current user details
            String phNo = number.replace(" ", "").replace("(", "").replace(")", "").replace("-", "");

            if (phNo.equalsIgnoreCase(mSessionManager.getPhoneNumberOfCurrentUser()) ||
                    phNo.equalsIgnoreCase(mSessionManager.getUserMobileNoWithoutCountryCode())) {
                contactSmarttyId = mCurrentUserId;
            }

            sendContactMessage("", contactSmarttyId, Uname, number, ContactString);
        } else if (requestCode == RESULT_SHARE_LOCATION && resultCode == RESULT_OK && null != data) {
            Place place = PlacePicker.getPlace(SecretChatViewActivity.this, data);
            if (place != null) {
                LatLng latlng = place.getLatLng();
                if (place.getAddress() != null) {
                    String address = place.getAddress().toString().trim();
                    String name = place.getName().toString();
                    if (name.length() > 0 && name.charAt(0) == '(') {
                        String[] parts = name.split(",");
                        name = parts[0].trim() + "," + parts[1].trim();
                    }

                    if (latlng != null) {
                        sendLocationMessage(latlng, name, address);
                    }
                }
            }

        } else if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            try {
                ArrayList<Imagepath_caption> pathlist = new ArrayList<>();
                Bundle bundle = data.getExtras();
                try {
                    pathlist = (ArrayList<Imagepath_caption>) bundle.getSerializable("pathlist");
                    for (int i = 0; i < pathlist.size(); i++) {
                        String path = pathlist.get(i).getPath();
                        String caption = pathlist.get(i).getCaption();
                        sendImageChatMessage(path, caption);
                    }
                } catch (NullPointerException e) {
                    MyLog.e(TAG, "", e);
                }

            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
        } else if (requestCode == RESULT_LOAD_VIDEO && resultCode == RESULT_OK) {
            ArrayList<Imagepath_caption> pathlist = new ArrayList<>();
            Bundle bundle = data.getExtras();
            try {
                pathlist = (ArrayList<Imagepath_caption>) bundle.getSerializable("pathlist");
                String path = pathlist.get(0).getPath();
                String caption = pathlist.get(0).getCaption();
                sendVideoChatMessage(path, caption);
            } catch (NullPointerException e) {
                MyLog.e(TAG, "", e);
            }
        } else if (requestCode == MUTE_ACTIVITY) { // Finish activity when exit group by user
            if (resultCode == RESULT_OK && data != null) {
                boolean ismutechange = data.getBooleanExtra("muteactivity", false);
                if (ismutechange) {
                    finish();
                }
            }
        } else if (requestCode == REQUEST_CODE_FORWARD_MSG) {
            if (resultCode == RESULT_OK) {
                boolean isMultiForward = data.getBooleanExtra("MultiForward", false);
                if (isMultiForward) {
                    reloadAdapter();
                    showUnSelectedActions();
                }
            } else {
                if (selectedChatItems != null && selectedChatItems.size() > 0) {
                    for (MessageItemChat msgItem : selectedChatItems) {
                        int index = mChatData.indexOf(msgItem);
                        if (index > -1) {
                            mChatData.get(index).setSelected(true);
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                } else {
                    showUnSelectedActions();
                }
            }
        } else if (resultCode == RESULT_CANCELED) {
            mChatData.clear();
            initDataBase();
        } else if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {

            try {
                ArrayList<Imagepath_caption> pathlist = new ArrayList<>();
                Bundle bundle = data.getExtras();
                try {
                    pathlist = (ArrayList<Imagepath_caption>) bundle.getSerializable("pathlist");

                    String path = pathlist.get(0).getPath();
                    String caption = pathlist.get(0).getCaption();
                    sendImageChatMessage(path, caption);

                } catch (NullPointerException e) {
                    MyLog.e(TAG, "", e);
                }

            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }

        } else if (requestCode == REQUEST_CODE_DOCUMENT && resultCode == RESULT_OK) {
            Uri fileUri = data.getData();

            Log.i("Filesss Path", fileUri.toString());
            sendDocumentMessage(fileUri.getPath());
        } else if (requestCode == REQUEST_SELECT_AUDIO && resultCode == RESULT_OK) {
            String fileName = data.getStringExtra("FileName");
            String filePath = data.getStringExtra("FilePath");
            String duration = data.getStringExtra("Duration");
            sendAudioMessage(filePath, duration, MessageFactory.AUDIO_FROM_ATTACHMENT);
        } else if (requestCode == FilePickerConst.REQUEST_CODE_DOC) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                ArrayList<String> docPaths = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS);
                if (docPaths.size() > 0) {
                    sendDocumentMessage(docPaths.get(0));
                }
            }
        }
        if (requestCode == PICK_CONTACT_REQUEST && resultCode == RESULT_OK) {
            loadContactInfo(data.getData());
        }
    }

    private void loadContactInfo(Uri contactUri) {

        /*
         * We should always run database queries on a background thread. The database may be
         * locked by some process for a long tsNextLine.  If we locked up the UI thread while waiting
         * for the query to come back, we might get an "Application Not Responding" dialog.
         */
        AsyncTask<Uri, Void, Boolean> task = new AsyncTask<Uri, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(Uri... uris) {
                Log.v("Retreived ContactURI", uris[0].toString());

                return doesContactContainHomeEmail(uris[0]);
            }

            @Override
            protected void onPostExecute(Boolean exists) {
                if (exists) {
                    Log.v("", "Updating...");
                    updateContact();
                } else {
                    Log.v("", "Inserting...");
                    insertEmailContact();
                }
            }
        };

        task.execute(contactUri);
    }

    private Boolean doesContactContainHomeEmail(Uri contactUri) {
        boolean returnValue = false;
        Cursor mContactCursor = getContentResolver().query(contactUri, null, null, null, null);
        Log.v("Contact", "Got Contact Cursor");

        try {
            if (mContactCursor.moveToFirst()) {
                String mContactId = getCursorString(mContactCursor,
                        ContactsContract.Contacts._ID);

                Cursor mRawContactCursor = getContentResolver().query(
                        ContactsContract.RawContacts.CONTENT_URI,
                        null,
                        ContactsContract.Data.CONTACT_ID + " = ?",
                        new String[]{mContactId},
                        null);

                Log.v("RawContact", "Got RawContact Cursor");

                try {
                    ArrayList<String> mRawContactIds = new ArrayList<String>();
                    while (mRawContactCursor.moveToNext()) {
                        String rawId = getCursorString(mRawContactCursor, ContactsContract.RawContacts._ID);
                        Log.v("RawContact", "ID: " + rawId);
                        mRawContactIds.add(rawId);
                    }

                    for (String rawId : mRawContactIds) {
                        // Make sure the "last checked" RawContactId is set locally for use in insert & update.
                        mRawContactId = rawId;
                        Cursor mDataCursor = getContentResolver().query(
                                ContactsContract.Data.CONTENT_URI,
                                null,
                                ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.CommonDataKinds.Email.TYPE + " = ?",
                                new String[]{mRawContactId, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, String.valueOf(ContactsContract.CommonDataKinds.Phone.TYPE_OTHER)},
                                null);

                        if (mDataCursor.getCount() > 0) {
                            mDataCursor.moveToFirst();
                            mDataId = getCursorString(mDataCursor, ContactsContract.Data._ID);
                            Log.v("Data", "Found data item with MIMETYPE and EMAIL.TYPE");
                            mDataCursor.close();
                            returnValue = true;
                            break;
                        } else {
                            Log.v("Data", "Data doesn't contain MIMETYPE and EMAIL.TYPE");
                            mDataCursor.close();
                        }
                        returnValue = false;
                    }
                } finally {
                    mRawContactCursor.close();
                }
            }
        } catch (Exception e) {
            Log.w("UpdateContact", e.getMessage());
            for (StackTraceElement ste : e.getStackTrace()) {
                Log.w("UpdateContact", "\t" + ste.toString());
            }
            throw new RuntimeException();
        } finally {
            mContactCursor.close();
        }

        return returnValue;
    }

    public void insertEmailContact() {
        try {
            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValue(ContactsContract.Data.RAW_CONTACT_ID, mRawContactId)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, receiverMsisdn)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_OTHER)
                    // .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, "Phone")
                    .build());


            getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);

        } catch (Exception e) {
            // Display warning
            Log.w("UpdateContact", e.getMessage());
            for (StackTraceElement ste : e.getStackTrace()) {
                Log.w("UpdateContact", "\t" + ste.toString());
            }
            Context ctx = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(ctx, "Update failed", duration);
            MyLog.e(TAG, "", e);
            toast.show();
        }
    }

    public void updateContact() {
        try {
            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

            ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                    .withSelection(ContactsContract.Data.RAW_CONTACT_ID + " = ?", new String[]{mRawContactId})
                    .withSelection(ContactsContract.Data._ID + " = ?", new String[]{mDataId})
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, receiverMsisdn)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_OTHER)

                    .build());


            getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);

        } catch (Exception e) {
            // Display warning
            Log.w("UpdateContact", e.getMessage());
            for (StackTraceElement ste : e.getStackTrace()) {
                Log.w("UpdateContact", "\t" + ste.toString());
            }
            Context ctx = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(ctx, "Update failed", duration);
            toast.show();
        }
    }

    public void sendImageChatMessage(String imgPath, String caption) {
        if (imgPath != null) {

            PictureMessage message = (PictureMessage) MessageFactory.getMessage(MessageFactory.picture, this);

            MessageItemChat item = null;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imgPath, options);
            int imageHeight = options.outHeight;
            int imageWidth = options.outWidth;

            message.getMessageObject(to, imgPath, true);
            item = message.createMessageItem(true, caption, imgPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                    mReceiverId, receiverNameText.getText().toString(), imageWidth, imageHeight);
            item.setSenderMsisdn(receiverMsisdn);
            item.setSecretTimer(secretMsgTimer);
            item.setSecretTimeCreatedBy(mCurrentUserId);

            MessageDbController db = CoreController.getDBInstance(this);
            db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SECRET);
            if (!isAlreadyExist(item))
                mChatData.add(item);
            notifyDatasetChange();

            String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(imgPath);
         /*   String[] splitIds = item.getMessageId().split("-");
            String docId = mCurrentUserId + "-" + to;
            String msgId = docId + "-" + splitIds[3];*/
            String imgName = item.getMessageId() + fileExtension;

            JSONObject uploadObj = (JSONObject) message.createImageUploadObject(item.getMessageId(), mLocDbDocId, imgName, imgPath,
                    receiverNameText.getText().toString(), caption, MessageFactory.CHAT_TYPE_SINGLE, true);
            uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);
        }
    }

    private void sendLocationMessage(final LatLng latlng, final String addressName, final String address) {

        final String thumbUrl = "https://maps.googleapis.com/maps/api/staticmap?center=" + latlng.latitude + "," + latlng.longitude
                + "&zoom=15&size=180x180&maptype=roadmap&markers=color:red%7Clabel:S%7C" + latlng.latitude
                + "," + latlng.longitude + "&key=" + "AIzaSyAXBjfzFWhK0pu-6bTl5NuSbqKsistosfU";
//        final String thumbUrl = "https://static.pexels.com/photos/20974/pexels-photo.jpg";

//        final String thumbUrl = "https://maps.googleapis.com/maps/api/staticmap?center=40.714728,-73.998672&zoom=12&size=400x400&key=AIzaSyCm0zmTw3_oQNR6x-gdD1Y3175BOAfUa2o";

        initProgress(getString(R.string.loading_address), true);

        ImageLoader imageLoader = CoreController.getInstance().getImageLoader();
        imageLoader.get(thumbUrl, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer imageContainer, boolean b) {
                Bitmap bitmap = imageContainer.getBitmap();
                //use bitmap
                if (bitmap != null) {

                    hideProgressDialog();

                    Bitmap newBmp = Bitmap.createScaledBitmap(bitmap, 100, 100, true);

                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    newBmp.compress(Bitmap.CompressFormat.JPEG, 50, out);
                    byte[] thumbArray = out.toByteArray();
                    try {
                        out.close();
                    } catch (IOException e) {
                        MyLog.e(TAG, "", e);
                    }
                    String thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT);
                    if (thumbData != null) {
                        thumbData = thumbData.replace("\n", "");
                        if (!thumbData.startsWith("data:image/jpeg;base64,")) {
                            thumbData = "data:image/jpeg;base64," + thumbData;
                        }

                        String url = "https://maps.google.com/maps?q=" + latlng.latitude + "," + latlng.longitude
                                + " (" + addressName + ")&amp;z=15&amp;hl=en";

                        SendMessageEvent messageEvent = new SendMessageEvent();
                        LocationMessage message = (LocationMessage) MessageFactory.getMessage(MessageFactory.location, SecretChatViewActivity.this);

        /*String webLinkThumb = null;
        if (ivWebLink.getDrawable() != null) {
            Bitmap linkBmp = ((BitmapDrawable) ivWebLink.getDrawable()).getBitmap();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            linkBmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            webLinkThumb = Base64.encodeToString(byteArray, Base64.DEFAULT);
        }*/

                        String receiverName = receiverNameText.getText().toString();
                        JSONObject msgObj = (JSONObject) message.getMessageObject(to, "", true);
                        messageEvent.setEventName(SocketManager.EVENT_MESSAGE);

                        MessageItemChat item = message.createMessageItem(true, addressName, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                                mReceiverId, receiverName, addressName, address, url, thumbUrl, thumbData);
                        msgObj = (JSONObject) message.getLocationObject(msgObj, addressName, address, url, thumbUrl, thumbData);
                        messageEvent.setMessageObject(msgObj);

                        MessageDbController db = CoreController.getDBInstance(SecretChatViewActivity.this);

                        item.setSenderMsisdn(receiverMsisdn);
                        item.setSenderName(receiverNameText.getText().toString());
                        item.setSecretTimer(secretMsgTimer);
                        item.setSecretTimeCreatedBy(mCurrentUserId);
                        db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SECRET);
                        if (!isAlreadyExist(item))
                            mChatData.add(item);
                        EventBus.getDefault().post(messageEvent);
                        notifyDatasetChange();

                    }
                }
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                hideProgressDialog();
             /*   Toast.makeText(SecretChatViewActivity.this, "Try again", Toast.LENGTH_SHORT).show();
                Log.d("error", "occue");
           */
                String json = null;
                NetworkResponse response = volleyError.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            //     Log.e(TAG,"statuscode"+response.statusCode);
                            json = new String(response.data);
                            Toast.makeText(SecretChatViewActivity.this, getString(R.string.enablepayment), Toast.LENGTH_SHORT).show();
                              /*  json = trimMessage(json, "errors");
                                if (json != null) displayMessage(json,context);*/
                            break;
                    }
                } else {
                    //  Log.i("Volley Error", error.toString());
                        /*Toast.makeText(getApplicationContext(),
                                "Error: " + error.toString(),
                                Toast.LENGTH_LONG).show();*/
                    Toast.makeText(SecretChatViewActivity.this, getString(R.string.networkerror), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void sendContactMessage(String data, String contactSmarttyId, String contactName, String contactNumber, String contactDetail) {

        SendMessageEvent messageEvent = new SendMessageEvent();
        ContactMessage message = (ContactMessage) MessageFactory.getMessage(MessageFactory.contact, this);

        messageEvent.setEventName(SocketManager.EVENT_MESSAGE);
        JSONObject msgObj = (JSONObject) message.getMessageObject(to, data, contactSmarttyId, contactName,
                contactNumber, contactDetail, true);

        messageEvent.setMessageObject(msgObj);
        MessageItemChat item = message.createMessageItem(true, data, MessageFactory.DELIVERY_STATUS_NOT_SENT, mReceiverId,
                receiverNameText.getText().toString(), contactName, contactNumber, contactSmarttyId, contactDetail);
        item.setSenderMsisdn(receiverMsisdn);
      /*  String image = ("uploads/users/").concat(item.get_id()).concat(".jpg");
        String path =  Constants.SOCKET_IP.concat(image);*/
        item.setAvatarImageUrl(receiverAvatar);
        item.setDetailedContacts(contactDetail);
        item.setSecretTimer(secretMsgTimer);
        item.setSecretTimeCreatedBy(mCurrentUserId);

        MessageDbController db = CoreController.getDBInstance(this);
        db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SECRET);
        if (!isAlreadyExist(item))
            mChatData.add(item);
        notifyDatasetChange();
        EventBus.getDefault().post(messageEvent);

    }

    private void sendAudioMessage(String filePath, String duration, int audioFrom) {

        AudioMessage message = (AudioMessage) MessageFactory.getMessage(MessageFactory.audio, this);
        message.getMessageObject(to, filePath, true);

        MessageItemChat item = message.createMessageItem(true, filePath, duration, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                mReceiverId, receiverNameText.getText().toString(), audioFrom);
        item.setSenderMsisdn(receiverMsisdn);
        item.setaudiotype(audioFrom);
        item.setSecretTimer(secretMsgTimer);
        item.setSecretTimeCreatedBy(mCurrentUserId);

        MessageDbController db = CoreController.getDBInstance(this);
        db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SECRET);
        if (!isAlreadyExist(item))
            mChatData.add(item);
        notifyDatasetChange();

        String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(filePath);
        String audioName = item.getMessageId() + fileExtension;

        JSONObject uploadObj = (JSONObject) message.createAudioUploadObject(item.getMessageId(), mLocDbDocId, audioName, filePath,
                duration, receiverNameText.getText().toString(), audioFrom, MessageFactory.CHAT_TYPE_SINGLE, true);
        uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);
    }

    private void sendVideoChatMessage(String videoPath, String caption) {
        if (videoPath != null) {

            VideoMessage message = (VideoMessage) MessageFactory.getMessage(MessageFactory.video, this);
            message.getMessageObject(to, videoPath, true);

            MessageItemChat item = message.createMessageItem(true, videoPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                    mReceiverId, receiverNameText.getText().toString(), caption);
            item.setSenderMsisdn(receiverMsisdn);

            Bitmap thumbBmp = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MICRO_KIND);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            thumbBmp.compress(Bitmap.CompressFormat.PNG, 10, out);
            byte[] thumbArray = out.toByteArray();
            try {
                out.close();
            } catch (IOException e) {
                MyLog.e(TAG, "", e);
            }
            String thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT);
            if (thumbData != null) {
                item.setThumbnailData(thumbData);
            }
            item.setSecretTimer(secretMsgTimer);
            item.setSecretTimeCreatedBy(mCurrentUserId);

            MessageDbController db = CoreController.getDBInstance(this);
            db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SECRET);
            if (!isAlreadyExist(item))
                mChatData.add(item);
            notifyDatasetChange();

            String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(videoPath);
            String videoName = item.getMessageId() + fileExtension;

            JSONObject uploadObj = (JSONObject) message.createVideoUploadObject(item.getMessageId(), mLocDbDocId, videoName,
                    videoPath, receiverNameText.getText().toString(), caption, MessageFactory.CHAT_TYPE_SINGLE, true);
            uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);
        }
    }

    private void sendDocumentMessage(String filePath) {
        DocumentMessage message = (DocumentMessage) MessageFactory.getMessage(MessageFactory.document, this);

        message.getMessageObject(to, filePath, true);
        MessageItemChat item = message.createMessageItem(true, filePath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                mReceiverId, receiverNameText.getText().toString());
        item.setSenderMsisdn(receiverMsisdn);
        item.setSecretTimer(secretMsgTimer);
        item.setSecretTimeCreatedBy(mCurrentUserId);

        MessageDbController db = CoreController.getDBInstance(this);
        db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SECRET);
        if (!isAlreadyExist(item))
            mChatData.add(item);
        notifyDatasetChange();

        String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(filePath);
        String docName = item.getMessageId() + fileExtension;

        JSONObject uploadObj = (JSONObject) message.createDocUploadObject(item.getMessageId(), mLocDbDocId, docName, filePath,
                receiverNameText.getText().toString(), MessageFactory.CHAT_TYPE_SINGLE, true);
        uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);
    }

    private void loadMessage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            //     MyLog.e(TAG, "loadMessage" + objects);
            //{"type":0,"payload":"hjjk","id":"5808c7f76090cc12841d4b46-5808cb46bf5aa60a70fd5c23","from":"5808c7f76090cc12841d4b46","doc_id":"5808c7f76090cc12841d4b46-5808cb46bf5aa60a70fd5c23-1477902694564","thumbnail":"","dataSize":"","timestamp":1477902830388,"convId":"5816e58855dea4ec14bf7dd7"}
            String type = String.valueOf(objects.getString("type"));
            String secretType = objects.getString("secret_type");
            String resendTo = objects.getString("from");
            if (isBlockedUser(resendTo)) {
                return;
            }
            if (secretType.equalsIgnoreCase("yes") && resendTo.equalsIgnoreCase(mReceiverId)) {

                if (type.equalsIgnoreCase(MessageFactory.timer_change + "")) {
                    loadSecretTimerChangeMessage(objects);
                } else {

                    String payLoad = objects.getString("payload");
                    String id = (String) objects.get("id");

                    String doc_id = (String) objects.get("doc_id");
                    String thumbnail = objects.getString("thumbnail");        //REVERESE LOGIC FOR STORE CHAT ITEM RECEIVED
                    String dataSize = objects.getString("filesize");
                    String convId = objects.getString("convId");
                    mConvId = convId;
                    String audiotype = "";

                    String recordId = objects.getString("recordId");
                    String name = objects.getString("Name");
                    String ts = objects.getString("timestamp");
                    String starredStatus = objects.getString("isStar");
                    MessageItemChat item = new MessageItemChat();
                    item.setTS(ts);
                    item.setConvId(convId);
                    item.setRecordId(recordId);
                    item.setStarredStatus(starredStatus);
                    item.setReceiverID(from);
                    item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_NOT_START);

                    if (objects.has("ContactMsisdn")) {
                        String senderMsisdn = objects.getString("ContactMsisdn");
                        item.setSenderMsisdn(senderMsisdn);
                        item.setSenderName(senderMsisdn);
                    }
//            item.setSenderName("" + new String(Base64.decode(name, Base64.DEFAULT), "UTF-8"));
                    if (objects.has("replyDetails")) {
                        JSONObject aReplyObject = objects.getJSONObject("replyDetails");
                        item.setReplyMsisdn(aReplyObject.getString("From_msisdn"));
                        item.setReplyFrom(aReplyObject.getString("from"));
                        item.setReplyType(aReplyObject.getString("type"));
                        if (aReplyObject.has("message")) {
                            String mess = aReplyObject.getString("message");
                            item.setReplyMessage(aReplyObject.getString("message"));
                        }
                        item.setReplyId(aReplyObject.getString("_id"));
                        item.setReplyServerLoad(aReplyObject.getString("server_load"));
                        if (aReplyObject.has("thumbnail_data")) {
                            String thumbnail_data = aReplyObject.getString("thumbnail_data");
                            item.setreplyimagebase64(thumbnail_data);
                        }

                        if (item.getReplyFrom().equalsIgnoreCase(mCurrentUserId)) {
                            item.setReplySender("you");
                        } else if (!mReceiverName.equals("") && mReceiverName != null) {
                            item.setReplySender(mReceiverName);
                        } else {
                            item.setReplySender(item.getReplyMsisdn());
                        }

                    }

                    //REVERESE LOGIC FOR STORE CHAT ITEM RECEIVED
                    String uniqueID;
                    if (from.equalsIgnoreCase(resendTo)) {
                        uniqueID = mCurrentUserId + "-" + resendTo;
                        item.setIsSelf(true);
                        item.setMessageId(uniqueID.concat("-").concat(id));
                    } else if (to.equalsIgnoreCase(resendTo)) {
                        if (convId != null && convId.equals("")) {
                            mConvId = convId;
                        }
                        if (mSessionManager.canSendReadReceipt()) {
                            sendAckToServer(resendTo, doc_id, "" + id);
                        }
                        uniqueID = from + "-" + resendTo;
                        item.setMessageId(uniqueID.concat("-").concat(id));
                        changeBadgeCount();
                        item.setIsSelf(false);

                    }

                    JSONObject linkObj = objects.getJSONObject("link_details");

                    String recLink = "";
                    if (linkObj.has("url")) {
                        recLink = linkObj.getString("url");
                    }

                    String recLinkTitle = "";
                    if (linkObj.has("title")) {
                        recLinkTitle = linkObj.getString("title");
                    }

                    String recLinkDesc = "";
                    if (linkObj.has("description")) {
                        recLinkDesc = linkObj.getString("description");
                    }

                    String recLinkImgUrl = "";
                    if (linkObj.has("image")) {
                        recLinkImgUrl = linkObj.getString("image");
                    }

                    String recLinkImgThumb = "";
                    if (linkObj.has("thumbnail_data")) {
                        recLinkImgThumb = linkObj.getString("thumbnail_data");
                    }

                    String typeStr = "" + type;
                    item.setMessageType(typeStr);
                    if (typeStr.equalsIgnoreCase("" + MessageFactory.text)) {
                        item.setTextMessage(payLoad);
                    } else if (typeStr.equalsIgnoreCase("" + MessageFactory.contact)) {
//                item.setContactInfo(payLoad);
                        String contactName = objects.getString("contact_name");
                        String contactNumber = objects.getString("createdTomsisdn");
                        String contactDetails = objects.getString("contact_details");
                        if (objects.has("createdTo")) {
                            String contactSmarttyId = objects.getString("createdTo");
                            item.setContactSmarttyId(contactSmarttyId);
                        }
                        item.setContactName(contactName);
                        item.setContactNumber(contactNumber);
                        item.setDetailedContacts(contactDetails);

                    } else if (typeStr.equalsIgnoreCase("" + MessageFactory.picture)) {
                        if (thumbnail != null && !thumbnail.equals("")) {
                            File dir = new File(Environment.getExternalStoragePublicDirectory(
                                    Environment.DIRECTORY_PICTURES), MessageFactory.IMAGE_STORAGE_PATH);
                            if (!dir.exists()) {
                                dir.mkdir();
                            }
                            String localFileName = MessageFactory.getMessageFileName(MessageFactory.picture,
                                    id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                            String filePath = MessageFactory.IMAGE_STORAGE_PATH + localFileName;
                            item.setChatFileLocalPath(filePath);
                        }

                        item.setChatFileServerPath(thumbnail);
                        item.setFileBufferAt(0);
                        item.setUploadDownloadProgress(0);

                        String thumbData = objects.getString("thumbnail_data");
                        item.setThumbnailData(thumbData);
                        item.setFileSize(dataSize);
                    } else if (type.equalsIgnoreCase("" + MessageFactory.audio)) {

                        if (thumbnail != null && !thumbnail.equals("")) {
                            File dir = new File(MessageFactory.AUDIO_STORAGE_PATH);
                            if (!dir.exists()) {
                                dir.mkdir();
                            }
                            String localFileName = MessageFactory.getMessageFileName(
                                    MessageFactory.audio, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                            String filePath = MessageFactory.AUDIO_STORAGE_PATH + localFileName;
                            item.setChatFileLocalPath(filePath);
                        }
                        if (objects.has("audio_type")) {
                            audiotype = objects.getString("audio_type");
                        }
                        item.setaudiotype(Integer.parseInt(audiotype));
                        String duration = objects.getString("duration");
                        item.setDuration(duration);
                        item.setaudiotype(Integer.parseInt(audiotype));
                        item.setChatFileServerPath(thumbnail);
                        item.setFileSize(dataSize);
                    } else if (type.equalsIgnoreCase("" + MessageFactory.video)) {
                        if (thumbnail != null && !thumbnail.equals("")) {
                            File dir = new File(MessageFactory.VIDEO_STORAGE_PATH);
                            if (!dir.exists()) {
                                dir.mkdir();
                            }
                            String localFileName = MessageFactory.getMessageFileName(
                                    MessageFactory.video, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                            String filePath = MessageFactory.VIDEO_STORAGE_PATH + localFileName;
                            item.setChatFileLocalPath(filePath);
                        }

                        item.setChatFileServerPath(thumbnail);

                        String thumbData = objects.getString("thumbnail_data");
                        String duration = objects.getString("duration");
                        item.setThumbnailData(thumbData);
                        item.setFileSize(dataSize);
                    } else if (type.equalsIgnoreCase("" + MessageFactory.document)) {

                        if (thumbnail != null && !thumbnail.equals("")) {
                            File dir = new File(MessageFactory.DOCUMENT_STORAGE_PATH);
                            if (!dir.exists()) {
                                dir.mkdir();
                            }
                            String localFileName = MessageFactory.getMessageFileName(
                                    MessageFactory.document, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                            String filePath = MessageFactory.DOCUMENT_STORAGE_PATH + localFileName;
                            item.setChatFileLocalPath(filePath);
                            //set original file name
                            String originalFileName = objects.getString("original_filename");
                            item.setTextMessage(originalFileName);

                            //  item.setTextMessage(localFileName);

                        }

                        item.setChatFileServerPath(thumbnail);
                        item.setFileBufferAt(0);
                        item.setUploadDownloadProgress(0);
                        item.setFileSize(dataSize);
                    } else if (type.equalsIgnoreCase(MessageFactory.timer_change + "")) {
                        String timerMode = objects.getString("incognito_timer_mode");
                        String timer = objects.getString("incognito_timer");
                        item.setIsDate(true);

                        if (resendTo.equalsIgnoreCase(mReceiverId)) {
                            secretMsgTimerId = item.getMessageId();
                            secretMsgTimerMode = timerMode;
                            secretMsgTimer = timer;
                        }
                    }

                    String timerMode = objects.getString("incognito_timer_mode");
                    String timer = objects.getString("incognito_timer");

                    long serverTS = Calendar.getInstance().getTimeInMillis() - serverTimeDiff;

                    item.setSecretTimeCreatedBy(resendTo);
                    item.setSecretTimer(timer);
                    item.setSecretTimerMode(timerMode);
                    item.setSecretMsgReadAt(serverTS + "");
                    item.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_READ);
                    if (!payLoad.isEmpty())
                        item.setTextMessage(payLoad);
                    item.setWebLink(recLink);
                    item.setWebLinkTitle(recLinkTitle);
                    item.setWebLinkDesc(recLinkDesc);
                    item.setWebLinkImgUrl(recLinkImgUrl);
                    item.setWebLinkImgThumb(recLinkImgThumb);

                    MessageDbController dbController = CoreController.getDBInstance(this);
                    dbController.updateSecretMessageReadAt(mLocDbDocId, item.getMessageId(), serverTS);
                    Log.d(TAG, "updateChatMessageFrom9 ");
                    dbController.updateChatMessage(mLocDbDocId, item.getMessageId(), MessageFactory.DELIVERY_STATUS_READ,
                            serverTS + "", false);

                    if (lastvisibleitempostion < mChatData.size() - 1) {
                        unreadmsgcount++;
                        unreadmessage();
                        if (!isAlreadyExist(item))
                            mChatData.add(item);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        if (!isAlreadyExist(item))
                            mChatData.add(item);
                        notifyDatasetChange();
                    }

                    if (session.getPrefsNameintoneouttone()) {
                        /*Uri path = Uri.parse("android.resource://" + getPackageName() + "/raw/solemn");
                        RingtoneManager.setActualDefaultRingtoneUri(
                                getApplicationContext(), RingtoneManager.TYPE_RINGTONE,
                                path);
                        Log.i("TESTT", "Ringtone Set to Resource: " + path.toString());
                        RingtoneManager.getRingtone(getApplicationContext(), path)
                                .play();*/
                        MediaPlayer.create(this, R.raw.send_message).start();
                    }
                }

            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadSecretTimerChangeMessage(JSONObject objects) {
        try {
            String secretType = objects.getString("secret_type");
            String from = objects.getString("from");
            if (secretType.equalsIgnoreCase("yes") && from.equalsIgnoreCase(mReceiverId)) {

                String to = objects.getString("to");
                String convId = objects.getString("convId");
                String recordId = objects.getString("recordId");
                String timer = objects.getString("incognito_timer");
                String timerMode = objects.getString("incognito_timer_mode");
                String toUserMsgId;
                if (objects.has("doc_id")) {
                    toUserMsgId = objects.getString("doc_id");
                } else {
                    toUserMsgId = objects.getString("docId");
                }
                String msgId = objects.getString("id");
                String timeStamp = objects.getString("timestamp");
                String fromMsisdn = objects.getString("ContactMsisdn");

                sendAckToServer(from, toUserMsgId, msgId);
                String docId = to + "-" + from;

                MessageItemChat item = new MessageItemChat();
                item.setIsSelf(false);
                item.setIsDate(true);
                item.setConvId(convId);
                item.setRecordId(recordId);
                item.setSecretTimer(timer);
                item.setSecretTimeCreatedBy(from);
                item.setSecretTimerMode(timerMode);
                item.setMessageId(docId + "-" + msgId);
                item.setTS(timeStamp);
                item.setSenderMsisdn(fromMsisdn);
                item.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_READ);
                item.setMessageType(MessageFactory.timer_change + "");

                secretMsgTimer = timer;
                secretMsgTimerMode = timerMode;
                secretMsgTimerId = msgId;

                setSecretTimerText();
                if (!isAlreadyExist(item))
                    mChatData.add(item);
                mAdapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void sendAckToServer(String to, String doc_id, String id) {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_MESSAGE_ACK);
        MessageAck ack = (MessageAck) MessageFactory.getMessage(MessageFactory.message_ack, this);
        messageEvent.setMessageObject((JSONObject) ack.getMessageObject(to, doc_id,
                MessageFactory.DELIVERY_STATUS_READ, id, true));
        EventBus.getDefault().post(messageEvent);
    }

    private void loadMessageRes(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            int errorState = objects.getInt("err");
            if (errorState == 0) {
                String chat_id = (String) objects.get("doc_id");
                String[] ids = chat_id.split("-");
                String doc_id = ids[0] + "-" + ids[1];
                int delivery = (int) objects.get("deliver");
                String[] to_id = doc_id.split("-");
                String to = to_id[1];

                mSessionManager.setIsFirstMessage(to, true);

                JSONObject msgData = objects.getJSONObject("data");
                String recordId = msgData.getString("recordId");
                String convId = msgData.getString("convId");

                String secretType = msgData.getString("secret_type");
                if (secretType.equalsIgnoreCase("yes")) {
                    MessageItemChat item = null;

                    for (MessageItemChat msgItemChat : mChatData) {
                        if (msgItemChat.getMessageId().equalsIgnoreCase(chat_id)) {
                            item = msgItemChat;
                            break;
                        }
                    }
                    if (item != null) {
                        mConvId = convId;
                        long serverTS = Calendar.getInstance().getTimeInMillis() - serverTimeDiff;
                        int msgIndex = mChatData.indexOf(item);
                        mChatData.get(msgIndex).setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);
                        mChatData.get(msgIndex).setDeliveryStatus("" + delivery);
                        mChatData.get(msgIndex).setRecordId(recordId);
                        mChatData.get(msgIndex).setConvId(convId);
                        mChatData.get(msgIndex).setMsgSentAt(serverTS + "");
                        mAdapter.notifyDataSetChanged();

                    } else {

                        String toUserId = msgData.getString("to");

                        if (toUserId.equalsIgnoreCase(mReceiverId)) {
                            mConvId = convId;

                            IncomingMessage incomingMessage = new IncomingMessage(SecretChatViewActivity.this);
                            MessageItemChat newMsgItem = incomingMessage.loadSingleMessageFromWeb(objects);

                            if (newMsgItem != null) {
                                long serverTS = Calendar.getInstance().getTimeInMillis() - serverTimeDiff;
                                newMsgItem.setMsgSentAt(serverTS + "");
                                if (!isAlreadyExist(newMsgItem))
                                    mChatData.add(newMsgItem);
                                notifyDatasetChange();
                            }
                        }
                    }
                }

                if (session.getPrefsNameintoneouttone()) {
                   /* Uri path = Uri.parse("android.resource://" + getPackageName() + "/raw/solemn");
                    final MediaPlayer player = new MediaPlayer();
                    try {
                        player.setDataSource(context, path);
                        player.prepare();
                        player.start();
                    } catch (IOException e) {
                        Log.e(TAG,"",e);
                    }

                    player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer) {
                            player.start();
                        }
                    });*/
                    MediaPlayer.create(this, R.raw.send_message).start();
                }

            } /*else if (errorState == 1) {
                SessionManager.getInstance(ChatViewActivity.this).logoutUser();
            }*/

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }

    public Bitmap ConvertToImage(String image) {
        byte[] imageAsBytes = Base64.decode(image.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goSecrectChatListPage();
    }

    private void goSecrectChatListPage() {
        if (backfrom) {
            Intent intent = new Intent(this, SecretChatList.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            finish();
        }
    }

    private boolean isBlockedUser() {
        try {
            boolean isBlockedInSecret = contactDB_sqlite.getBlockedStatus(to, true).equals("1");
            boolean isBlockedInNormal = contactDB_sqlite.getBlockedStatus(to, false).equals("1");
            if (isBlockedInNormal || isBlockedInSecret)
                return true;
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    private void sendWebLinkMessage() {
        String data = sendMessage.getText().toString().trim();

        if (!data.equalsIgnoreCase("")) {
            if (session.getarchivecount() != 0) {
                if (session.getarchive(from + "-" + to))
                    session.removearchive(from + "-" + to);
            }
            if (session.getarchivecountgroup() != 0) {
                if (session.getarchivegroup(from + "-" + to + "-g"))
                    session.removearchivegroup(from + "-" + to + "-g");
            }

            SendMessageEvent messageEvent = new SendMessageEvent();
            WebLinkMessage message = (WebLinkMessage) MessageFactory.getMessage(MessageFactory.web_link, this);

            String webLinkThumb = null;
            if (ivWebLink.getDrawable() != null) {
                Bitmap linkBmp = ((BitmapDrawable) ivWebLink.getDrawable()).getBitmap();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                linkBmp.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                byte[] byteArray = outputStream.toByteArray();
                try {
                    outputStream.close();
                } catch (IOException e) {
                    MyLog.e(TAG, "", e);
                }
                webLinkThumb = Base64.encodeToString(byteArray, Base64.DEFAULT);
            }

            JSONObject msgObj;
            String receiverName = receiverNameText.getText().toString();

            msgObj = (JSONObject) message.getMessageObject(to, data, true);
            messageEvent.setEventName(SocketManager.EVENT_MESSAGE);

            MessageItemChat item = message.createMessageItem(true, data, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                    mReceiverId, receiverName, webLink, webLinkTitle, webLinkDesc, webLinkImgUrl, webLinkThumb);
            msgObj = (JSONObject) message.getWebLinkObject(msgObj, webLink, webLinkTitle, webLinkDesc, webLinkImgUrl, webLinkThumb);
            messageEvent.setMessageObject(msgObj);

            MessageDbController db = CoreController.getDBInstance(this);

            item.setSecretTimer(secretMsgTimer);
            item.setSecretTimeCreatedBy(mCurrentUserId);
            item.setSenderMsisdn(receiverMsisdn);
            item.setSenderName(receiverNameText.getText().toString());
            db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SECRET);
            if (!isAlreadyExist(item))
                mChatData.add(item);
            EventBus.getDefault().post(messageEvent);
            notifyDatasetChange();
        }

        sendMessage.getText().clear();
        hasLinkInfo = false;
        rlWebLink.setVisibility(View.GONE);
        webLink = "";
        webLinkTitle = "";
        webLinkImgUrl = "";
        webLinkDesc = "";

    }

    @Override
    protected void onResume() {
        super.onResume();
        isSecretChatPage = true;
        session.puttoid(to + "-" + mSessionManager.getCurrentUserID());
        msgDeleteHandler.postDelayed(msgDeleteRunnable, 1000);
        initDataBase();
        NotificationUtil.clearNotificationData();

        NotificationManager notifManager = (NotificationManager) getApplication().getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.cancelAll();
        profilepicupdation();
    }

    private void deleteMsgHandler() {
        msgDeleteHandler = new Handler();
        msgDeleteRunnable = new Runnable() {
            @Override
            public void run() {
                handleMessageDelete();
                msgDeleteHandler.postDelayed(msgDeleteRunnable, 1000);
            }
        };
        msgDeleteHandler.postDelayed(msgDeleteRunnable, 2000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        session.puttoid("");
        isSecretChatPage = false;
        msgDeleteHandler.removeCallbacks(msgDeleteRunnable);
    }

    private void DisplayAlert(final String to) {
        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage("Unblock " + mReceiverName + " to send message?");
        dialog.setNegativeButtonText("Cancel");
        dialog.setPositiveButtonText("Unblock");
        dialog.setCancelable(false);

        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                putBlockUser();
                dialog.dismiss();

            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();

            }
        });
        dialog.show(getSupportFragmentManager(), "Unblock a person");
    }

    private void putBlockUser() {
        BlockUserUtils.changeUserBlockedStatus(SecretChatViewActivity.this, EventBus.getDefault(),
                mCurrentUserId, mReceiverId, false);
/*        BlockUserUtils.changeUserBlockedStatus(SecretChatViewActivity.this, EventBus.getDefault(),
                mCurrentUserId, mReceiverId, true);*/
    }

    private void blockunblockcontact(ReceviceMessageEvent event) {
        String toid = "", fromid = "";
        try {
            Object[] obj = event.getObjectsArray();
            JSONObject object = new JSONObject(obj[0].toString());

            String stat = object.getString("status");
            toid = object.getString("to");
            fromid = object.getString("from");

            if (mCurrentUserId.equalsIgnoreCase(fromid) && toid.equalsIgnoreCase(mReceiverId)) {
                String receiverName = mReceiverName;
                if (mReceiverName == null || mReceiverName.isEmpty())
                    receiverName = receiverMsisdn;
                if (stat.equalsIgnoreCase("1")) {
                    Toast.makeText(this, receiverName + " is blocked", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, receiverName + " is Unblocked", Toast.LENGTH_SHORT).show();
                }
            } else if (mCurrentUserId.equalsIgnoreCase(toid) && fromid.equalsIgnoreCase(mReceiverId)) {
                getcontactname.configProfilepic(ivProfilePic, to, true, true, R.mipmap.chat_attachment_profile_default_image_frame);

                if (stat.equalsIgnoreCase("1")) {
                    statusTextView.setVisibility(View.GONE);

                } else {
                    statusTextView.setVisibility(View.VISIBLE);
                }
            }

            invalidateOptionsMenu();
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    @Override
    public boolean onLongClick(View view) {

        if (view.getId() == R.id.record) {
            if (isBlockedUser()) {

                DisplayAlert(to);
            } else {
                if (checkAudioRecordPermission()) {
                    final Typeface face = CoreController.getInstance().getAvnNextLTProRegularTypeface();
                    record.setImageResource(R.drawable.record_secret_hold);
                    sendMessage.setVisibility(View.GONE);
                    selEmoji.setImageResource(R.drawable.record_usericon);
                    myChronometer.setVisibility(View.VISIBLE);
                    image_to.setVisibility(View.VISIBLE);
                    slidetocencel.setVisibility(View.VISIBLE);
                    captureImage.setVisibility(View.GONE);
                    myChronometer.setTypeface(face);

                    startAudioRecord();
                    selEmoji.setEnabled(false);
                } else {
                    requestAudioRecordPermission();
                }
            }
        }


        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case AUDIO_RECORD_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                }
                break;
        }
    }

    private void startAudioRecord() {

        try {
            if (!audioRecordStarted) {
                audioRecordStarted = true;
                File audioDir = new File(MessageFactory.AUDIO_STORAGE_PATH);
                if (!audioDir.exists()) {
                    audioDir.mkdirs();
                }

                audioRecordPath = MessageFactory.AUDIO_STORAGE_PATH + MessageFactory.getMessageFileName(MessageFactory.audio,
                        Calendar.getInstance().getTimeInMillis() + "rc", ".mp3");
                File file = new File(audioRecordPath);
                try {
                    file.createNewFile();
                } catch (Exception e) {
                    audioRecordPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getAbsolutePath() +
                            MessageFactory.getMessageFileName(MessageFactory.audio,
                                    Calendar.getInstance().getTimeInMillis() + "rc", ".mp3");
                    file = new File(audioRecordPath);
                    try {
                        file.createNewFile();
                    } catch (Exception e1) {
                        MyLog.e(TAG, "startAudioRecord: ", e1);
                    }
                }

//                audioRecordPath = MessageFactory.AUDIO_STORAGE_PATH + MessageFactory.getMessageFileName(MessageFactory.audio,
//                        Calendar.getInstance().getTimeInMillis() + "rc", ".mp3");
//                File file = new File(audioRecordPath);
//                file.createNewFile();

                audioRecorder = new MediaRecorder();
                audioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                audioRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                audioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                audioRecorder.setOutputFile(audioRecordPath);
                audioRecorder.prepare();
                audioRecorder.start();
                myChronometer.start();
                myChronometer.setBase(SystemClock.elapsedRealtime());
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            MyLog.e(TAG, "", e);
        }
    }

    public boolean checkAudioRecordPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestAudioRecordPermission() {
        ActivityCompat.requestPermissions(SecretChatViewActivity.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, AUDIO_RECORD_PERMISSION_REQUEST_CODE);
    }

    private void showAudioRecordSentAlert(final String audioRecordPath, final String durationStr) {
        CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage("You want send this recorded audio?");
        dialog.setPositiveButtonText("Send");
        dialog.setNegativeButtonText("Cancel");

        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                sendAudioMessage(audioRecordPath, durationStr, MessageFactory.AUDIO_FROM_RECORD);
            }

            @Override
            public void onNegativeButtonClick() {

            }
        });
        dialog.show(getSupportFragmentManager(), "Record Alert");
    }

    private void showUnSelectedActions() {
       /* rlChatSeen.setVisibility(View.VISIBLE);
        rlChatActions.setVisibility(View.GONE);

        isFirstItemSelected = false;
        selectedChatItems.clear();

        for (int i = 0; i < mChatData.size(); i++) {
            mChatData.get(i).setSelected(false);
        }

        mAdapter.notifyDataSetChanged();*/
    }

    private void performSelection(int position) {
        MessageItemChat msgItem = mChatData.get(position);
        if (!msgItem.isInfoMsg()) {
            mypath = msgItem.getChatFileLocalPath();

            mChatData.get(position).setSelected(!msgItem.isSelected());
            if (!selectedChatItems.contains(msgItem)) {
                selectedChatItems.add(msgItem);
            } else {
                selectedChatItems.remove(msgItem);

            }

            // Add copy action only if all selected messages as text
            boolean allTextMsg = true;
            for (MessageItemChat selectedItem : selectedChatItems) {
                if (!selectedItem.getMessageType().equals(MessageFactory.text + "")) {
                    allTextMsg = false;
                    break;
                }

            }
            if (allTextMsg) {
                copy.setVisibility(View.VISIBLE);
            } else {
                copy.setVisibility(View.GONE);
            }

            if (selectedChatItems.size() > 0) {
                if (selectedChatItems.size() == 1) {
                    replymess.setVisibility(View.VISIBLE);

                    if (selectedChatItems.get(0).isSelf()) {
                        info.setVisibility(View.VISIBLE);
                    } else {
                        info.setVisibility(View.GONE);
                    }

                } else {
                    replymess.setVisibility(View.GONE);
                    reply = false;
                    info.setVisibility(View.GONE);
                }

                isSelectedWithUnStarMsg = false;
                for (MessageItemChat selectedItem : selectedChatItems) {
                    if (selectedItem.getStarredStatus().equals(MessageFactory.MESSAGE_UN_STARRED)) {
                        isSelectedWithUnStarMsg = true;
                        break;
                    }
                }

                if (isSelectedWithUnStarMsg) {
                    starred.setImageResource(R.drawable.ic_starred);
                } else {
                    starred.setImageResource(R.drawable.ic_unstarred);
                }

                showBaseActions();
            }

            mAdapter.notifyDataSetChanged();
        }
    }

    private void showBaseActions() {
        // rlChatSeen.setVisibility(View.GONE);
        rlChatActions.setVisibility(View.VISIBLE);

        starred.setVisibility(View.VISIBLE);
        delete.setVisibility(View.VISIBLE);
        forward.setVisibility(View.VISIBLE);
        longpressback.setVisibility(View.VISIBLE);
    }

    private void showSearchActions() {
        // Make all selected items to unselect

        //rlChatSeen.setVisibility(View.GONE);
        rlChatActions.setVisibility(View.VISIBLE);

        replymess.setVisibility(View.GONE);
        copy.setVisibility(View.GONE);
        starred.setVisibility(View.GONE);
        info.setVisibility(View.GONE);
        delete.setVisibility(View.GONE);
        forward.setVisibility(View.GONE);

        longpressback.setVisibility(View.GONE);

        backnavigate.setVisibility(View.VISIBLE);
    }

    private void Starredmsg() {
        String msgidstarred = "";
        if (msgid != null || !msgid.equals("")) {
            final DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            for (int i = 0; i < mChatData.size(); i++) {
                try {
                    String[] array = mChatData.get(i).getMessageId().split("-");
                    if (mChatData.get(i).getMessageId().contains("-g")) {
                        msgidstarred = array[3];
                    } else {
                        msgidstarred = array[2];
                    }
                    if (msgid.equalsIgnoreCase(msgidstarred)) {
                        recyclerView_chat.smoothScrollToPosition(i);
                        mChatData.get(i).setSelected(true);
                    }

                } catch (Exception e) {

                }
            }
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < mChatData.size(); i++) {
                        mChatData.get(i).setSelected(false);
                    }
                    mAdapter.notifyDataSetChanged();
                }
            }, 1500);

        }

    }

    private void profilepicupdation() {
        if (receiverAvatar != null) {

            receiverAvatar = AppUtils.getValidProfilePath(receiverAvatar);


            getcontactname.configProfilepic(ivProfilePic, to, true, true, R.mipmap.chat_attachment_profile_default_image_frame);
        }

    }

    public void show() {
        final Dialog dialog = new Dialog(SecretChatViewActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_secretchat);
        AvnNextLTProDemiButton b1 = dialog.findViewById(R.id.button1);
        AvnNextLTProDemiButton b2 = dialog.findViewById(R.id.button2);
        final NumberPicker np = dialog.findViewById(R.id.numberPicker1);
        setDividerColor(np, ContextCompat.getColor(this, R.color.numberpic_divdercolor));
        setNumberPickerTextColor(np, ContextCompat.getColor(this, R.color.white));
        String[] data = new String[]{getResources().getString(R.string.time_5sec), getResources().getString(R.string.time_10sec),
                getResources().getString(R.string.time_30sec), getResources().getString(R.string.time_1min),
                getResources().getString(R.string.time_1hr), getResources().getString(R.string.time_1day), getResources().getString(R.string.time_1week)};
        np.setMinValue(0);
        np.setMaxValue(data.length - 1);
        np.setDisplayedValues(data);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        np.setValue(timervalue);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                secretMsgTimerMode = getResources().getString(R.string.time_5sec);
                secretMsgTimer = String.valueOf(5 * 1000);
                switch (timervalue) {

                    case 0:
                        secretMsgTimerMode = getResources().getString(R.string.time_5sec);
                        secretMsgTimer = String.valueOf(5 * 1000); // 5 sec
                        break;

                    case 1:
                        secretMsgTimerMode = getResources().getString(R.string.time_10sec);
                        secretMsgTimer = String.valueOf(10 * 1000); // 10 sec
                        break;

                    case 2:
                        secretMsgTimerMode = getResources().getString(R.string.time_30sec);
                        secretMsgTimer = String.valueOf(30 * 1000);
                        break;

                    case 3:
                        secretMsgTimerMode = getResources().getString(R.string.time_1min);
                        secretMsgTimer = String.valueOf(60 * 1000);
                        break;

                    case 4:
                        secretMsgTimerMode = getResources().getString(R.string.time_1hr);
                        secretMsgTimer = String.valueOf(60 * 60 * 1000);
                        break;

                    case 5:
                        secretMsgTimerMode = getResources().getString(R.string.time_1day);
                        secretMsgTimer = String.valueOf(24 * 60 * 60 * 1000);
                        break;

                    case 6:
                        secretMsgTimerMode = getResources().getString(R.string.time_1week);
                        secretMsgTimer = String.valueOf(7 * 24 * 60 * 60 * 1000);
                        break;

                }
                if (ConnectivityInfo.isInternetConnected(SecretChatViewActivity.this)) {
                    sendTimerChangeMessage(secretMsgTimerMode);
                    setSecretTimerText();
                    dialog.dismiss();
                } else {
                    Toast.makeText(SecretChatViewActivity.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();


    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        Log.i("value is", "" + newVal);
        //System.out.println("New value is" + newVal);
        timervalue = newVal;


    }

    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    MyLog.e(TAG, "", e);
                } catch (Resources.NotFoundException e) {
                    MyLog.e(TAG, "", e);
                } catch (IllegalAccessException e) {
                    MyLog.e(TAG, "", e);
                }
                break;
            }
        }
    }

    private void unreadmessage() {
        if (lastvisibleitempostion == mChatData.size() - 1) {
            unreadmsgcount = 0;
            unreadcount.setVisibility(View.GONE);
        } else {
            //unreadcount.setVisibility(View.VISIBLE);
            unreadcount.setText(String.valueOf(unreadmsgcount));
        }
    }

    private void sendScreenShotMsg() {
        String data = " took a screenshot!";
        SendMessageEvent messageEvent = new SendMessageEvent();
        TextMessage message = (TextMessage) MessageFactory.getMessage(MessageFactory.text, this);
        JSONObject msgObj = (JSONObject) message.getMessageObject(to, data, true);
        try {
            msgObj.put("type", MessageFactory.SCREEN_SHOT_TAKEN);
        } catch (Exception e) {
            MyLog.e(TAG, "sendScreenShotMsg: ", e);
        }
        messageEvent.setEventName(SocketManager.EVENT_MESSAGE);

        MessageItemChat item = message.createMessageItem(true, "You" + data, MessageFactory.DELIVERY_STATUS_NOT_SENT, mReceiverId, receiverNameText.getText().toString());
        messageEvent.setMessageObject(msgObj);
        MessageDbController db = CoreController.getDBInstance(this);

        item.setSecretTimer(secretMsgTimer);
        item.setSecretTimeCreatedBy(mCurrentUserId);
        item.setSenderMsisdn(receiverMsisdn);
        item.setSenderName(receiverNameText.getText().toString());
        item.setMessageType(MessageFactory.SCREEN_SHOT_TAKEN + "");
        db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SECRET);
        if (!isAlreadyExist(item))
            mChatData.add(item);
        EventBus.getDefault().post(messageEvent);
        recyclerView_chat.postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyDatasetChange();

            }
        }, 500);

    }

    @Override
    public void screenShotTaken() {

        if (!getResources().getString(R.string.app_name).contains("Neo")) {
            sendScreenShotMsg();
        }
    }

    @Override
    public void itemClick(int position) {

    }

    private boolean isBlockedUser(String to) {
        try {
            ContactDB_Sqlite contactDB_sqlite = new ContactDB_Sqlite(this);
            boolean isBlockedInSecret = contactDB_sqlite.getBlockedStatus(to, true).equals("1");
            boolean isBlockedInNormal = contactDB_sqlite.getBlockedStatus(to, false).equals("1");
            if (isBlockedInNormal || isBlockedInSecret)
                return true;
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    @Override
    public void downloadCompleted(String msgId, String path) {
        VideoDownloadComplete("", msgId, path);
    }

    @Override
    public void progress(int progress, String msgId) {
        for (int i = 0; i < mChatData.size(); i++) {
            if (msgId.equalsIgnoreCase(mChatData.get(i).getMessageId())) {
                mChatData.get(i).setUploadDownloadProgress(progress);
                mAdapter.notifyDataSetChanged();
                break;
            }
        }
    }

    @Override
    public void DownloadError(int progress, String msgId) {

    }
}
