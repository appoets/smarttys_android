package com.app.Smarttys.app.utils;

import android.content.Context;

import com.app.Smarttys.R;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.MessageItemChat;

public class GroupMessageUtil {
    public static String getGroupEventMsg(MessageItemChat message, Context context, String mCurrentUserId, String createdByName, String createdToName) {
        String msg = null;
        String createdBy = message.getCreatedByUserId();
        String createdTo = message.getCreatedToUserId();
        String groupName = message.getGroupName();
        if (message.getGroupEventType() != null)
            switch (message.getGroupEventType()) {

                case "" + MessageFactory.join_new_group:
                    if (createdBy.equalsIgnoreCase(mCurrentUserId)) {
                        createdByName = context.getString(R.string.you);
                        msg = createdByName + " " + context.getString(R.string.created_group) + " " + groupName;
                    } else {

                        msg = createdByName + " " + context.getString(R.string.created_group_others) + " " + groupName;
                    }

                    break;

                case "" + MessageFactory.add_group_member:
                    if (createdBy.equalsIgnoreCase(mCurrentUserId)) {
                        createdByName = context.getString(R.string.you);
                        msg = createdByName + " " + context.getString(R.string.added);
                    } else {

                        msg = createdByName + " " + context.getString(R.string.added_others);
                    }
                    if (createdTo.equalsIgnoreCase(mCurrentUserId)) {
                        createdToName = context.getString(R.string.you);
                        msg += " " + createdToName;
                    } else {

                        msg += " " + createdToName;
                    }

//                            if(context.getResources().getBoolean(R.bool.is_arabic)) {
//                                msg = createdByName + " " +context.getString(R.string.added) + " " +createdToName;
//                            } else {
//                                msg = createdByName + " " +context.getString(R.string.added_others) + " " +createdToName;
//                            }

                    break;

                case "" + MessageFactory.change_group_icon:
                    if (createdBy.equalsIgnoreCase(mCurrentUserId)) {
                        createdByName = context.getString(R.string.you);
                        msg = createdByName + " " + context.getString(R.string.changed_group_icon);
                    } else {

                        msg = createdByName + " " + context.getString(R.string.changed_group_icon_others);
                    }
                    break;

                case "" + MessageFactory.change_group_name:
                    if (createdBy.equalsIgnoreCase(mCurrentUserId)) {
                        createdByName = context.getString(R.string.you);
                        msg = createdByName + " " + context.getString(R.string.changed_group_name) + " " + message.getPrevGroupName() + " " + context.getString(R.string.to)
                                + " " + message.getGroupName();
                    } else {

                        msg = createdByName + " " + context.getString(R.string.changed_group_name_others) + " " + message.getPrevGroupName() + " " + context.getString(R.string.to)
                                + " " + message.getGroupName();
                    }

                    break;

                case "" + MessageFactory.delete_member_by_admin:

                    if (createdBy.equalsIgnoreCase(mCurrentUserId)) {
                        createdByName = context.getString(R.string.you);
                    }

                    String outputRemoved = "";
                    if (createdTo.equalsIgnoreCase(mCurrentUserId)) {
//                                createdToName = context.getString(R.string.you);
                        outputRemoved = context.getString(R.string.removed_you);
                    } else {

                        outputRemoved = context.getString(R.string.removed) + " " + createdToName;
                    }

//                            msg = createdByName + context.getString(R.string.removed_you) + createdToName;
                    msg = createdByName + " " + outputRemoved;
                    break;

                case "" + MessageFactory.make_admin_member:
                    if (createdTo.equalsIgnoreCase(mCurrentUserId)) {
                        createdToName = context.getString(R.string.you);
                    }
                    msg = createdToName + " " + context.getString(R.string.now_admin);
                    break;

                case "" + MessageFactory.exit_group:
                    if (createdBy.equalsIgnoreCase(mCurrentUserId)) {
                        createdByName = context.getString(R.string.you);
                        msg = createdByName + " " + context.getString(R.string.left);
                    } else {
                        msg = createdByName + " " + context.getString(R.string.left_others);
                    }
                    break;

            }
        return msg;
    }
}
