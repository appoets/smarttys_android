package com.app.Smarttys.app.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.app.Smarttys.app.utils.MyLog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;

public class NewGroup_DB extends SQLiteOpenHelper {

    private final static int DB_Version = 4;

    private final static String TABLE_GROUP = "table_group";
    private final static String KEY_JSON_OBJECT = "jsonObj";
    private final static String KEY_RECORD_ID = "recordId";
    private final static String KEY_ID = "id";
    private final static String KEY_GROUP_TYPE = "groupType";
    private final static String KEY_ERR = "err";
    private final static String KEY_GROUP_ID = "groupId";
    private final static String KEY_CONV_ID = "convId";
    private final static String KEY_CREATED_BY = "createdBy";
    private final static String KEY_ADMIN = "admin";
    private final static String KEY_PROFILE_PIC = "profilePic";
    private final static String KEY_GROUP_NAME = "groupName";
    private final static String KEY_TIMESTAMP = "timestamp";
    private final static String KEY_GROUP_MEMBERS = "groupMembers";
    private final static String KEY_FROM = "from_";
    private final static String KEY_MSISDN = "msisdn";
    private final static String KEY_TYPE = "type";


    private final static String DB_Name = "group_db";

    private static final String TAG = NewGroup_DB.class.getSimpleName();
    private static final String CREATE_TABLE_GROUP = "CREATE TABLE " + TABLE_GROUP + "("
            + KEY_JSON_OBJECT + " TEXT,"
            + KEY_RECORD_ID + " TEXT,"
            + KEY_GROUP_TYPE + " INTEGER,"
            + KEY_ID + " TEXT,"
            + KEY_ERR + " INTEGER,"
            + KEY_GROUP_ID + " TEXT,"
            + KEY_CONV_ID + " TEXT,"
            + KEY_CREATED_BY + " TEXT,"
            + KEY_ADMIN + " TEXT,"
            + KEY_PROFILE_PIC + " TEXT,"
            + KEY_GROUP_NAME + " TEXT,"
            + KEY_TIMESTAMP + " TEXT,"
            + KEY_GROUP_MEMBERS + " TEXT,"
            + KEY_FROM + " TEXT,"
            + KEY_MSISDN + " TEXT,"
            + KEY_TYPE + " TEXT)";
    private String mCurrentUserId;
    private SQLiteDatabase mDatabaseInstance;
    private Gson gson;


    public NewGroup_DB(Context context) {
        super(context, DB_Name, null, DB_Version);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
        getReadableDatabase();
    }

    private SQLiteDatabase getDatabaseInstance() {
        if (mDatabaseInstance == null) {
            mDatabaseInstance = getWritableDatabase();
        }

        if (!mDatabaseInstance.isOpen()) {
            mDatabaseInstance = getWritableDatabase();
        }

        return mDatabaseInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_GROUP);
        //System.out.println("Table Created Successfully");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUP);
        onCreate(db);
    }

    public void close() {
        if (mDatabaseInstance != null && mDatabaseInstance.isOpen()) {
            mDatabaseInstance.close();
        }
    }

    public void insertNewGroupDetails(NewGroupDetails_Model data, JSONObject jsonObject) {
        if (data == null) {
            MyLog.e(TAG, "insertNewStatus: NewGroupDetails_Model null.. check ");
            return;
        }
        try {
            deleteGroupByGroupId(data.getGroupId());
            EventBus.getDefault().post(new GroupInviteModified(data.getGroupId()));
            getDatabaseInstance().beginTransaction();
            ContentValues values = new ContentValues();
            values.put(KEY_JSON_OBJECT, jsonObject.toString());
            values.put(KEY_RECORD_ID, data.getRecordId());
            values.put(KEY_GROUP_TYPE, data.getGroupType());
            values.put(KEY_ID, data.getId());
            values.put(KEY_ERR, data.getErr());
            values.put(KEY_GROUP_ID, data.getGroupId());
            values.put(KEY_CONV_ID, data.getConvId());
            values.put(KEY_CREATED_BY, data.getCreatedBy());
            values.put(KEY_ADMIN, data.getAdmin());
            values.put(KEY_PROFILE_PIC, data.getProfilePic());
            values.put(KEY_GROUP_NAME, data.getGroupName());
            values.put(KEY_TIMESTAMP, data.getTimestamp());
            values.put(KEY_GROUP_MEMBERS, data.getGroupMembers());
            values.put(KEY_FROM, data.getFrom_());
            values.put(KEY_MSISDN, data.getMsisdn());
            values.put(KEY_TYPE, data.getType());
            // Inserting Row
            long result = getDatabaseInstance().insert(TABLE_GROUP, null, values);
            MyLog.d(TAG, "insertNewGroup result of insert: " + result);
            getDatabaseInstance().setTransactionSuccessful();
        } catch (Exception e) {
            MyLog.e(TAG, "insertNewGroup: ", e);
        } finally {
            getDatabaseInstance().endTransaction();
        }
    }

    public ArrayList<NewGroupDetails_Model> getList(String query) {
        ArrayList<NewGroupDetails_Model> result = new ArrayList<>();
        try {
            Cursor cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String data = cursor.getString(cursor.getColumnIndex(KEY_ID));
                    result.add(new NewGroupDetails_Model(
                            cursor.getString(cursor.getColumnIndex("jsonObj")),
                            cursor.getString(cursor.getColumnIndex("recordId"))
                            , cursor.getString(cursor.getColumnIndex("id"))
                            , cursor.getInt(cursor.getColumnIndex("groupType"))
                            , cursor.getInt(cursor.getColumnIndex("err"))
                            , cursor.getString(cursor.getColumnIndex("groupId"))
                            , cursor.getString(cursor.getColumnIndex("convId"))
                            , cursor.getString(cursor.getColumnIndex("createdBy"))
                            , cursor.getString(cursor.getColumnIndex("admin"))
                            , cursor.getString(cursor.getColumnIndex("profilePic"))
                            , cursor.getString(cursor.getColumnIndex("groupName"))
                            , cursor.getString(cursor.getColumnIndex("timestamp"))
                            , cursor.getString(cursor.getColumnIndex("groupMembers"))
                            , cursor.getString(cursor.getColumnIndex("from_"))
                            , cursor.getString(cursor.getColumnIndex("msisdn"))
                            , cursor.getString(cursor.getColumnIndex("type"))));
                }
                cursor.close();
            }
        } catch (Exception e) {
            MyLog.e(TAG, "getMyUnUploadedStatus: ", e);
        }
        return result;
    }

    public NewGroupDetails_Model getSingleData(String groupId) {
        NewGroupDetails_Model newGroupDetails_Model = null;
        String query = "SELECT * FROM table_group where groupId='" + groupId + "'";
        try {
            Cursor cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String data = cursor.getString(cursor.getColumnIndex(KEY_ID));
                    newGroupDetails_Model = new NewGroupDetails_Model(
                            cursor.getString(cursor.getColumnIndex("jsonObj")),
                            cursor.getString(cursor.getColumnIndex("recordId"))
                            , cursor.getString(cursor.getColumnIndex("id"))
                            , cursor.getInt(cursor.getColumnIndex("groupType"))
                            , cursor.getInt(cursor.getColumnIndex("err"))
                            , cursor.getString(cursor.getColumnIndex("groupId"))
                            , cursor.getString(cursor.getColumnIndex("convId"))
                            , cursor.getString(cursor.getColumnIndex("createdBy"))
                            , cursor.getString(cursor.getColumnIndex("admin"))
                            , cursor.getString(cursor.getColumnIndex("profilePic"))
                            , cursor.getString(cursor.getColumnIndex("groupName"))
                            , cursor.getString(cursor.getColumnIndex("timestamp"))
                            , cursor.getString(cursor.getColumnIndex("groupMembers"))
                            , cursor.getString(cursor.getColumnIndex("from_"))
                            , cursor.getString(cursor.getColumnIndex("msisdn"))
                            , cursor.getString(cursor.getColumnIndex("type")));
                }
                cursor.close();
            }
        } catch (Exception e) {
            MyLog.e(TAG, "getMyUnUploadedStatus: ", e);
        }
        return newGroupDetails_Model;
    }

    public void deleteGroupData(String msgId) {
        String query = "SELECT " + KEY_ID + " FROM " + TABLE_GROUP + " WHERE "
                + KEY_ID + "='" + msgId + "'";

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(KEY_ID));
                getDatabaseInstance().delete(TABLE_GROUP, KEY_ID + "=" + msgId, null);
            }
            cursor.close();
        }

    }

    public void deleteGroupByGroupId(String groupId) {
        try {
            String query = "SELECT " + KEY_ID + " FROM " + TABLE_GROUP + " WHERE "
                    + KEY_GROUP_ID + "='" + groupId + "'";

            Cursor cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    // int id = cursor.getInt(cursor.getColumnIndex(KEY_ID));
                    getDatabaseInstance().delete(TABLE_GROUP, KEY_GROUP_ID + "='" + groupId + "'", null);
                }
                cursor.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteGroupByGroupId: ", e);
        }

    }

    public int getRowCount() {
        String countQuery = "SELECT  * FROM " + TABLE_GROUP;
        Cursor cursor = getDatabaseInstance().rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }
}
