package com.app.Smarttys.app.CustomFontTextView;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class NewFontItalic extends AppCompatTextView {

    public NewFontItalic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public NewFontItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NewFontItalic(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Karla-BoldItalic.ttf");
        setTypeface(tf);
    }
}