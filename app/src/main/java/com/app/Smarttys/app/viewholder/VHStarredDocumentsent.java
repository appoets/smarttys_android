package com.app.Smarttys.app.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;


/**
 * Created by CAS60 on 2/2/2017.
 */
public class VHStarredDocumentsent extends RecyclerView.ViewHolder {


    public TextView senderName, message, time, datelbl, fromname, toname;

    public ImageView singleTick, doubleTickGreen, doubleTickBlue, clock, starred, userprofile, ivDoc;
    public ProgressBar pbUpload;
    public RelativeLayout mainSent;

    public VHStarredDocumentsent(View view) {
        super(view);

        toname = view.findViewById(R.id.toname);
        fromname = view.findViewById(R.id.fromname);
        senderName = view.findViewById(R.id.lblMsgFrom);

        message = view.findViewById(R.id.txtMsg);

        time = view.findViewById(R.id.ts);
        userprofile = view.findViewById(R.id.userprofile);
        datelbl = view.findViewById(R.id.datelbl);

        singleTick = view.findViewById(R.id.single_tick_green);

        doubleTickGreen = view.findViewById(R.id.double_tick_green);

        doubleTickBlue = view.findViewById(R.id.double_tick_blue);

        clock = view.findViewById(R.id.clock);
        pbUpload = view.findViewById(R.id.pbUpload);
        mainSent = view.findViewById(R.id.mainSent);

        starred = view.findViewById(R.id.starredindicator);
        ivDoc = view.findViewById(R.id.ivDoc);

    }
}

