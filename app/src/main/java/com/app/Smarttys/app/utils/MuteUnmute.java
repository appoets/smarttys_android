package com.app.Smarttys.app.utils;


import android.content.Context;

import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CAS56 on 5/8/2017.
 */
public class MuteUnmute {

    private static final String TAG = "MuteUnmute";

    public static void muteUnmute(EventBus eventBus, String from, String to, String convId,
                                  String type, String secretType, int status, String option,
                                  int notifyStatus) {
        try {
            JSONObject object = new JSONObject();
            object.put("from", from);
            object.put("status", status);
            object.put("option", option);
            object.put("type", type);
            object.put("notify_status", notifyStatus);

            if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_SINGLE)) {
                object.put("to", to);
                object.put("secret_type", secretType);
            }

            if (convId != null && !convId.isEmpty()) {
                object.put("convId", convId);
            }

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_MUTE);
            event.setMessageObject(object);
            eventBus.post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }

    public static void performUnMute(Context context, EventBus eventBus, String receiverId,
                                     String chatType, String secretType) {
        Session session = new Session(context);
        SessionManager sessionManager = SessionManager.getInstance(context);
        UserInfoSession userInfoSession = new UserInfoSession(context);

        String mCurrentUserId = sessionManager.getCurrentUserID();

        String docId = mCurrentUserId + "-" + receiverId;
        String convId = null;

        if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
            docId = docId + "-g";
            convId = receiverId;
        } else {
            if (secretType.equalsIgnoreCase("yes")) {
                docId = docId + "-secret";
            }
            if (userInfoSession.hasChatConvId(docId)) {
                convId = userInfoSession.getChatConvId(docId);
            }
        }

        MuteUnmute.muteUnmute(eventBus, mCurrentUserId, receiverId, convId,
                chatType, secretType, 0, "", 1);

       /* session.setMuteDuration(docId, "");
        sessionManager.setMuteStatus(docId, false);
        session.setNotificationOnMute(docId, true);
        DateFormat dff = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
        Date cDate = new Date();
        String reportDate = dff.format(cDate);
        session.puttime(receiverId + "time", reportDate);*/
    }

}
