package com.app.Smarttys.app.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


/**
 * Created by Casperon Technologyon 02/04/16.
 */
public class VHLocationReceived extends RecyclerView.ViewHolder implements OnMapReadyCallback {

    public TextView senderName, time, tvDateLbl, tvSecretLbl;
    public ImageView imageViewindicatior, starredindicator_below;
    public ImageView ivMap;
    public RelativeLayout selection_layout;
    private LatLng positionSelected = new LatLng(0, 0);

    public VHLocationReceived(View view) {
        super(view);

        senderName = view.findViewById(R.id.lblMsgFrom);
        time = view.findViewById(R.id.ts);
        tvDateLbl = view.findViewById(R.id.tvDateLbl);
        tvSecretLbl = view.findViewById(R.id.tvSecretLbl);

        ivMap = view.findViewById(R.id.ivMap);
        imageViewindicatior = view.findViewById(R.id.imageView);
        selection_layout = view.findViewById(R.id.selection_layout);
        starredindicator_below = view.findViewById(R.id.starredindicator_below);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setMapToolbarEnabled(true);
        String nameSelected = "";
        googleMap.addMarker(new MarkerOptions().position(positionSelected).title(nameSelected));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(positionSelected, 16.0f));
    }
}
