package com.app.Smarttys.app.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app.Smarttys.R;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CAS56 on 3/10/2017.
 */
public class MediaAcitivity extends CoreActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private AvnNextLTProDemiTextView toolbar_title;
    private ImageView toolbar_back_button;
    private String docid;
    private Typeface avnRegFont, avnDemiFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mediomainactivity);
        toolbar = findViewById(R.id.toolbar);
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_back_button = findViewById(R.id.toolbar_back_button);
        Bundle bundle = getIntent().getExtras();
        docid = bundle.getString("docid");
        String username = bundle.getString("username");
        toolbar_title.setText(username);
        toolbar_title.setTextSize(20);
        setSupportActionBar(toolbar);
        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        avnRegFont = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        avnDemiFont = CoreController.getInstance().getAvnNextLTProDemiTypeface();
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        Session session = new Session(MediaAcitivity.this);
        session.putMediadocid(docid);
// set Fragmentclass Arguments
        createTabIcons();
        toolbar_back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        Bundle bundle_fragment = new Bundle();
        bundle_fragment.putString("docid", docid);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MediaFragment(), getString(R.string.medias));
        adapter.addFragment(new DocumentFragment(), getString(R.string.docss));
        adapter.addFragment(new LinkFragment(), getString(R.string.linkss));
        viewPager.setAdapter(adapter);
    }

    private void createTabIcons() {
        View tab1 = LayoutInflater.from(this).inflate(R.layout.media_tabs_view, null);
        TextView tab1Title = tab1.findViewById(R.id.tvTitle);
        tab1Title.setText(R.string.media);
        tab1Title.setTypeface(avnRegFont);
        tabLayout.getTabAt(0).setCustomView(tab1);

        View tab2 = LayoutInflater.from(this).inflate(R.layout.media_tabs_view, null);
        TextView tab2Title = tab2.findViewById(R.id.tvTitle);
        tab2Title.setText(R.string.docs);
        tab2Title.setTypeface(avnRegFont);
        tabLayout.getTabAt(1).setCustomView(tab2);

        View tab3 = LayoutInflater.from(this).inflate(R.layout.media_tabs_view, null);
        TextView tab3Title = tab3.findViewById(R.id.tvTitle);
        tab3Title.setText(R.string.links);
        tab3Title.setTypeface(avnRegFont);
        tabLayout.getTabAt(2).setCustomView(tab3);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
