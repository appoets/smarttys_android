package com.app.Smarttys.app.utils;

import android.view.View;

/**
 * Created by CAS60 on 5/10/2017.
 */
public interface RecyclerViewItemClickListener {

    void onRVItemClick(View parentView, int position);

}
