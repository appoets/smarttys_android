package com.app.Smarttys.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;

/**
 * Created by CAS60 on 8/2/2017.
 */
public class UserInfoSession {

    private final String USER_INFO_PREF = "UserInfoPref";
    private Context mContext;
    private SharedPreferences userInfoPref;
    private String mCurrentUSerId;

    public UserInfoSession(Context context) {
        if (context == null && CoreController.mcontext != null)
            context = CoreController.mcontext;
        this.mContext = context;
        userInfoPref = mContext.getSharedPreferences(USER_INFO_PREF, Context.MODE_PRIVATE);
        mCurrentUSerId = SessionManager.getInstance(context).getCurrentUserID();
    }

    public void clearData() {
        SharedPreferences.Editor userInfoPrefEditor = userInfoPref.edit();
        userInfoPrefEditor.clear();
        userInfoPrefEditor.apply();
    }

    public void updateChatConvId(String docId, String receiverId, String convId) {
        if (!mCurrentUSerId.equalsIgnoreCase(receiverId)) {
            String hasConvKey = docId.concat("-hasCovId");
            String convKey = docId.concat("-chatCovId");
            String receiverKey = convId.concat("-chatUserId");

            SharedPreferences.Editor userInfoPrefEditor = userInfoPref.edit();
            userInfoPrefEditor.putBoolean(hasConvKey, true);
            userInfoPrefEditor.putString(convKey, convId);
            userInfoPrefEditor.putString(receiverKey, receiverId);
            userInfoPrefEditor.apply();
        }
    }

    public boolean hasChatConvId(String docId) {
        String hasConvKey = docId.concat("-hasCovId");
        return userInfoPref.getBoolean(hasConvKey, false);
    }

    public String getChatConvId(String docId) {
        String convKey = docId.concat("-chatCovId");
        return userInfoPref.getString(convKey, "");
    }

    public String getReceiverIdByConvId(String convId) {
        String receiverKey = convId.concat("-chatUserId");
        return userInfoPref.getString(receiverKey, "");
    }

    public void updateUserMsisdn(String toUserId, String msisdn) {
        String msisdnKey = toUserId.concat("-Msisdn");
        SharedPreferences.Editor userInfoPrefEditor = userInfoPref.edit();
        userInfoPrefEditor.putString(msisdnKey, msisdn);
        userInfoPrefEditor.apply();
    }

    public String getUserMsisdn(String toUserId) {
        String msisdnKey = toUserId.concat("-Msisdn");
        return userInfoPref.getString(msisdnKey, "");
    }

}
