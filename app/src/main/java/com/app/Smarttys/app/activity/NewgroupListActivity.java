package com.app.Smarttys.app.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.NewGroupListAdapter;
import com.app.Smarttys.app.model.GroupInviteModified;
import com.app.Smarttys.app.model.NewGroupDetails_Model;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

//Only for RAAD
public class NewgroupListActivity extends CoreActivity {

    RecyclerView newGrouplists;
    List<NewGroupDetails_Model> newGroupDetails_modelList = new ArrayList<>();
    NewGroupListAdapter adapter;
    private ImageView backButton;
    private AvnNextLTProRegTextView userMessagechat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group_list);
        newGrouplists = findViewById(R.id.new_group_lists);
        userMessagechat = findViewById(R.id.userMessagechat);
        backButton = findViewById(R.id.backarrow_contactsetting);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        initData();
    }

    private void initData() {
        // String userprofilepic = SessionManager.getInstance(this).getUserProfilePic();

        newGroupDetails_modelList = CoreController.getmNewGroup_db(NewgroupListActivity.this).getList("SELECT * FROM table_group");

        if (newGroupDetails_modelList.size() > 0) {
            userMessagechat.setVisibility(View.GONE);
        }

        adapter = new NewGroupListAdapter(NewgroupListActivity.this, newGroupDetails_modelList);
        newGrouplists.setAdapter(adapter);
        newGrouplists.setHasFixedSize(true);
        LinearLayoutManager mediaManager = new LinearLayoutManager(NewgroupListActivity.this, LinearLayoutManager.VERTICAL, false);
        newGrouplists.setLayoutManager(mediaManager);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void groupInviteModified(GroupInviteModified groupInviteModified) {
        initData();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {
        backButton.performClick();
    }
}
