package com.app.Smarttys.app.dialog;

import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.ConnectivityInfo;
import com.app.Smarttys.app.utils.MuteUnmute;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.MuteStatusPojo;
import com.app.Smarttys.core.model.MuteUserPojo;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by CAS60 on 6/23/2017.
 */
public class MuteAlertDialog extends DialogFragment {

    private static final String TAG = "MuteAlertDialog";
    private View view;
    private RadioGroup rg;
    private RadioButton rb1, rb2, rb3;
    private AvnNextLTProDemiTextView cancel, ok, tvTitle;
    private CheckBox check;
    private CoreActivity mActivity;

    private Session session;
    private SessionManager sessionManager;
    private UserInfoSession userInfoSession;
    //    private String mCurrentUserId, mReceiverId, mLocDbDocId, muteDuration, chatType, secretType;
    private String mCurrentUserId, muteDuration, mLastMuteUserId;

    private MuteAlertCloseListener listener;
    private ArrayList<MuteUserPojo> muteUserList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        view = inflater.inflate(R.layout.dialog_mute_chat, container, false);

        rg = view.findViewById(R.id.radiogroupmain);
        rb1 = view.findViewById(R.id.rb1);
        rb2 = view.findViewById(R.id.rb2);
        rb3 = view.findViewById(R.id.rb3);
        check = view.findViewById(R.id.check1);
        cancel = view.findViewById(R.id.cancel);
        ok = view.findViewById(R.id.ok);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText("Mute chat for...");

        Typeface f1 = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        rb1.setTypeface(f1);
        rb2.setTypeface(f1);
        rb3.setTypeface(f1);
        check.setTypeface(f1);

        mActivity = (CoreActivity) getActivity();
        mActivity.initProgress(getString(R.string.loading_in), true);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        session = new Session(getActivity());
        sessionManager = SessionManager.getInstance(getActivity());
        userInfoSession = new UserInfoSession(getActivity());

        mCurrentUserId = sessionManager.getCurrentUserID();

        Bundle getBundle = getArguments();
        muteUserList = (ArrayList<MuteUserPojo>) getBundle.getSerializable("MuteUserList");

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (rb1.isChecked()) {
                    muteDuration = "8 Hours";
                } else if (rb2.isChecked()) {
                    muteDuration = "1 Week";
                } else if (rb3.isChecked()) {
                    muteDuration = "1 Year";
                }
            }
        });

        if (muteUserList != null && muteUserList.size() == 1) {
            MuteUserPojo muteUserItem = muteUserList.get(0);
            String toUserId = muteUserItem.getReceiverId();
            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(getActivity());

            MuteStatusPojo muteData = null;

            if (muteUserItem.getChatType().equalsIgnoreCase("group")) {
                muteData = contactDB_sqlite.getMuteStatus(mCurrentUserId, null, toUserId, false);
            } else {
                String docId = mCurrentUserId + "-" + toUserId;
                String convId = userInfoSession.getChatConvId(docId);
                muteData = contactDB_sqlite.getMuteStatus(mCurrentUserId, toUserId, convId, false);
//                muteData = contactsDB.getMuteStatus(mCurrentUserId, toUserId, convId, false);
            }


            if (muteData != null && muteData.getMuteStatus().equals("1")) {
                if (muteData.getDuration().equalsIgnoreCase("8 Hours")) {
                    rb1.setChecked(true);
                } else if (muteData.getDuration().equalsIgnoreCase("1 Week")) {
                    rb2.setChecked(true);
                } else if (muteData.getDuration().equalsIgnoreCase("1 Year")) {
                    rb3.setChecked(true);
                }
                if (muteData.getNotifyStatus().equals("1")) {
                    check.setChecked(true);
                }
            }
        } else {
            rb1.setChecked(true);
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityInfo.isInternetConnected(getActivity())) {
                    listener.onMuteDialogClosed(false);
                    getDialog().dismiss();
                } else {
                    Toast.makeText(getActivity(), "Check Your Network Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityInfo.isInternetConnected(getActivity())) {
                    if (muteDuration == null || muteDuration.equals("")) {
                        Toast.makeText(getActivity(), "Please choose duration", Toast.LENGTH_SHORT).show();
                    } else {
                        for (MuteUserPojo muteUserItem : muteUserList) {
                            String receiverId = muteUserItem.getReceiverId();
                            String chatType = muteUserItem.getChatType();
                            String secretType = muteUserItem.getSecretType();
                            mLastMuteUserId = receiverId;

                            String locDbDocId = getLocDBDocId(muteUserItem);
//                            session.setMuteDuration(locDbDocId, muteDuration);

                            String convId = null;
                            if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_SINGLE)) {
                                if (userInfoSession.hasChatConvId(locDbDocId)) {
                                    convId = userInfoSession.getChatConvId(locDbDocId);
                                }
                            } else {
                                // For group --- Group id and conversation id are same
                                convId = receiverId;
                            }

                            if (!muteDuration.equals("")) {
                                int notifyStatus = 0;
                                String value = getString(R.string.Default_ringtone);
                                session.putTone(value);
                                session.putgroupTone(value);
                                if (check.isChecked()) {
                                    notifyStatus = 1;
                                    session.putTone("None");
                                    session.putgroupTone("None");
                                }

                                MuteUnmute.muteUnmute(EventBus.getDefault(), mCurrentUserId, receiverId, convId,
                                        chatType, secretType, 1, muteDuration, notifyStatus);

                            } else {
                                listener.onMuteDialogClosed(false);
                            }

                            /*if (!check.isChecked()) {
                                session.setNotificationOnMute(locDbDocId, true);

                            } else {
                                session.setNotificationOnMute(locDbDocId, false);
                            }*/

                            mActivity.showProgressDialog();
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), "Check Your Network Connection", Toast.LENGTH_SHORT).show();
                }
            }

        });
    }

    public void setMuteAlertCloseListener(MuteAlertCloseListener listener) {
        this.listener = listener;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_MUTE)) {
            try {
                JSONObject objects = new JSONObject(event.getObjectsArray()[0].toString());

                String err = objects.getString("err");
                if (err.equalsIgnoreCase("1")) {
                    mActivity.hideProgressDialog();
                }

                String from = objects.getString("from");
                String to;
                if (objects.has("to")) {
                    to = objects.getString("to");
                } else {
                    to = objects.getString("convId");
                }

                if (from.equalsIgnoreCase(mCurrentUserId) && to.equalsIgnoreCase(mLastMuteUserId)) {
                    mActivity.hideProgressDialog();
                    listener.onMuteDialogClosed(true);
                    getDialog().dismiss();
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        }
    }

    private String getLocDBDocId(MuteUserPojo muteUserItem) {
        String docId = mCurrentUserId + "-" + muteUserItem.getReceiverId();

        if (muteUserItem.getChatType().equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
            docId = docId + "-g";
        } else {
            if (muteUserItem.getSecretType().equalsIgnoreCase("yes")) {
                docId = docId + "-secret";
            }
        }

        return docId;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public interface MuteAlertCloseListener {
        void onMuteDialogClosed(boolean isMuted);
    }
}
