package com.app.Smarttys.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.model.AudioFilePojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CAS60 on 2/1/2017.
 */
public class AudioFilesListAdapter extends RecyclerView.Adapter<AudioFilesListAdapter.AudioFilesViewHolder>
        implements Filterable {

    public List<AudioFilePojo> mDisplayedValues;
    private Context mContext;
    private List<AudioFilePojo> mOriginalValues;

    private AudioFileListItemClickListener listener;

    public AudioFilesListAdapter(Context mContext, List<AudioFilePojo> dataList) {
        this.mContext = mContext;
        this.mDisplayedValues = dataList;
        this.mOriginalValues = dataList;
    }

    public void setFileItemClickListener(AudioFileListItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public AudioFilesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_audio_files_list, parent, false);

        AudioFilesViewHolder holder = new AudioFilesViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(AudioFilesViewHolder holder, int position) {
        final AudioFilePojo audioItem = mDisplayedValues.get(position);

       /* long duration = Long.parseLong(audioItem.getDuration()) / 1000;
        long durationQuen = duration / 60;
        long durationRem = duration % 60;
        String strDuration;
        if(durationRem < 10) {
            strDuration = durationQuen + ":0" + durationRem;
        } else {
            strDuration = durationQuen + ":" + durationRem;
        }*/
        holder.tvDuration.setText(audioItem.getDuration());
        holder.tvFileName.setText(audioItem.getFileName());
//        holder.tvDuration.setText(strDuration);

        if (listener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onFileItemClick(audioItem);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mDisplayedValues.size();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mDisplayedValues = (ArrayList<AudioFilePojo>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<AudioFilePojo> FilteredArrList = new ArrayList<>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {


                        String fileName = mOriginalValues.get(i).getFileName();
                        if (fileName.toLowerCase().contains(constraint)) {

                            AudioFilePojo audioItem = new AudioFilePojo();
                            audioItem.setFileName(mOriginalValues.get(i).getFileName());
                            audioItem.setFilePath(mOriginalValues.get(i).getFilePath());
                            audioItem.setDuration(mOriginalValues.get(i).getDuration());

                            FilteredArrList.add(audioItem);
                        }


                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public interface AudioFileListItemClickListener {
        void onFileItemClick(AudioFilePojo item);
    }

    public class AudioFilesViewHolder extends RecyclerView.ViewHolder {

        public AvnNextLTProRegTextView tvFileName, tvDuration;

        public AudioFilesViewHolder(View itemView) {
            super(itemView);

            tvFileName = itemView.findViewById(R.id.tvFileName);
            tvDuration = itemView.findViewById(R.id.tvDuration);
        }
    }

}
