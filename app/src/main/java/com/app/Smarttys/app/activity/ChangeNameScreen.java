package com.app.Smarttys.app.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.ConnectivityInfo;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.widget.AvnNextLTProDemiButton;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

/**
 * Created by Administrator on 10/13/2016.
 */
public class ChangeNameScreen extends CoreActivity implements View.OnClickListener {

    EmojIconActions emojIcon;
    TextView tvFieldName;
    FrameLayout emoji;
    ImageButton happyFace;
    private AvnNextLTProDemiButton okBtn, cancleBtn;
    private EmojiconEditText et;
    private AvnNextLTProRegTextView textSize;
    private final TextWatcher myTextEditorWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            textSize.setText(String.valueOf(139 - s.length()));
        }
    };
    private ImageView ibBack;
    private boolean status;
    private String mCurrentUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_name);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        mCurrentUserId = SessionManager.getInstance(ChangeNameScreen.this).getCurrentUserID();

        // Enabled SoftKey
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE
                        | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        ibBack = findViewById(R.id.ibBack);
        ibBack.setOnClickListener(ChangeNameScreen.this);

        et = findViewById(R.id.editText_addStatus);
        Typeface face = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        et.setTypeface(face);

        et.addTextChangedListener(myTextEditorWatcher);
        et.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard();
                okBtn.performClick();
                return true;
            }
            return false;
        });
        textSize = findViewById(R.id.textSize);
        tvFieldName = findViewById(R.id.tvFieldName);
        Intent i = getIntent();
        if (i.hasExtra("name")) {
            et.setText(i.getStringExtra("name"));
            tvFieldName.setText(String.format(getString(R.string.Add_new_name, "name")));
        }
        if (i.hasExtra("surname")) {
            et.setText(i.getStringExtra("surname"));
            tvFieldName.setText(String.format(getString(R.string.Add_new_name, "surname")));
        }
        if (i.hasExtra("email")) {
            et.setText(i.getStringExtra("email"));
            tvFieldName.setText(String.format(getString(R.string.Add_new_name, "email")));
        }
        okBtn = findViewById(R.id.okAddNewStatus);

        /* Return the status to the Status Class */
        okBtn.setOnClickListener(v -> {
            if (et.getText().toString().trim().length() == 0) {
                Toast.makeText(ChangeNameScreen.this, getResources().getString(R.string.empty_profile_name_alert), Toast.LENGTH_LONG).show();
            } else if (ConnectivityInfo.isInternetConnected(ChangeNameScreen.this)) {
                SendMessageEvent event = new SendMessageEvent();
                event.setEventName(SocketManager.EVENT_CHANGE_USER_NAME);
                String mCurrentuname = et.getText().toString();

                try {
                    JSONObject object = new JSONObject();
                    object.put("from", mCurrentUserId);
/*                    byte[] data = (et.getText().toString()).getBytes();
                 base64 = Base64.encodeToString(data, Base64.DEFAULT);*/
                    if (getIntent().hasExtra("name")) {
                        object.put("first_name", et.getText().toString());
                        object.put("last_name", SessionManager.getInstance(ChangeNameScreen.this).getKeyUserLastName());
                        object.put("email", SessionManager.getInstance(ChangeNameScreen.this).getnameOfEmail());
                    }
                    if (getIntent().hasExtra("surname")) {
                        object.put("first_name", SessionManager.getInstance(ChangeNameScreen.this).getnameOfCurrentUser());
                        object.put("last_name", et.getText().toString());
                        object.put("email", SessionManager.getInstance(ChangeNameScreen.this).getnameOfEmail());
                    }
                    if (getIntent().hasExtra("email")) {
                        object.put("first_name", SessionManager.getInstance(ChangeNameScreen.this).getnameOfCurrentUser());
                        object.put("last_name", SessionManager.getInstance(ChangeNameScreen.this).getKeyUserLastName());
                        object.put("email", et.getText().toString());
                    }
                    object.put("PhNumber", SessionManager.getInstance(ChangeNameScreen.this).getPhoneNumberOfCurrentUser());
                    event.setMessageObject(object);
                    EventBus.getDefault().post(event);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            } else {
                Toast.makeText(ChangeNameScreen.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
        cancleBtn = findViewById(R.id.cancleAddNewStatus);
        cancleBtn.setOnClickListener(v -> finish());

        emoji = findViewById(R.id.emojicons);
        happyFace = findViewById(R.id.happyFace);
        setEmojiconFragment(false);

        et.setOnClickListener(view -> {
            /* remove the emojis in case edit text is clicked */
            status = !status;
            emoji.setVisibility(View.GONE);
        });

        final View rootView = findViewById(R.id.addatatusmainlayout);
        emojIcon = new EmojIconActions(this, rootView, et, happyFace);

        happyFace.setOnClickListener(v -> emojIcon.ShowEmojIcon());

        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                MyLog.e("", "Keyboard opened!");
            }

            @Override
            public void onKeyboardClose() {
                MyLog.e("", "Keyboard Closed!");
            }
        });
        emojIcon.setUseSystemEmoji(false);
        /*final EmojiconsPopup popup = new EmojiconsPopup(rootView, this);

        popup.setSizeForSoftKeyboard();

        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {

            }
        });

        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if (popup.isShowing())
                    popup.dismiss();
            }
        });

        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(github.ankushsachdeva.emojicon.emoji.Emojicon emojicon) {
                if (et == null || emojicon == null) {
                    return;
                }

                int start = et.getSelectionStart();
                int end = et.getSelectionEnd();
                if (start < 0) {
                    et.append(emojicon.getEmoji());
                } else {
                    et.getText().replace(Math.min(start, end),
                            Math.max(start, end), emojicon.getEmoji(), 0,
                            emojicon.getEmoji().length());
                }
            }
        });

        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                et.dispatchKeyEvent(event);
            }
        });

        *//* Enable disable the emoji fragment *//*
        happyFace.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!popup.isShowing()) {
                    if (popup.isKeyBoardOpen()) {
                        popup.showAtBottom();
                    } else {
                        et.setFocusableInTouchMode(true);
                        et.requestFocus();
                        popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
                    }
                } else {
                    popup.dismiss();
                }
            }
        });*/
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CHANGE_USER_NAME)) {
            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String err = object.getString("err");
                if (err.equals("0")) {
                    String from = object.getString("from");
                    if (from.equalsIgnoreCase(mCurrentUserId)) {
                        String name = object.getString("name");
                        SessionManager.getInstance(ChangeNameScreen.this).setnameOfCurrentUser(name);
                        Intent intent = new Intent();
                        intent.putExtra("name", true);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CHANGE_USER_NAME)) {
            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String err = object.getString("err");
                if (err.equals("0")) {
                    String from = object.getString("from");
                    if (from.equalsIgnoreCase(mCurrentUserId)) {
                        String surname = object.getString("surname");
                        SessionManager.getInstance(ChangeNameScreen.this).setKeyUserLastName(surname);
                        Intent intent = new Intent();
                        intent.putExtra("surname", true);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CHANGE_USER_NAME)) {
            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String err = object.getString("err");
                if (err.equals("0")) {
                    String from = object.getString("from");
                    if (from.equalsIgnoreCase(mCurrentUserId)) {
                        String email = object.getString("email");
                        SessionManager.getInstance(ChangeNameScreen.this).setEmail(email);
                        Intent intent = new Intent();
                        intent.putExtra("email", true);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(ChangeNameScreen.this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(ChangeNameScreen.this);
    }

    private void setEmojiconFragment(boolean useSystemDefault) {
        getSupportFragmentManager()
                .beginTransaction()
                // .replace(com.layer.atlas.R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (status) {
            /* Remove the emojicon */
            status = !status;
            emoji.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                finish();
                break;
        }
    }
}