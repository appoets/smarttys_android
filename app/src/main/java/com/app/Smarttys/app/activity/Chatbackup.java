package com.app.Smarttys.app.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;
import com.app.Smarttys.app.adapter.ChatbackupAdapter;
import com.app.Smarttys.app.dialog.CustomAlertDialog;
import com.app.Smarttys.app.utils.ConnectivityInfo;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.RecyclerViewItemClickListener;
import com.app.Smarttys.app.widget.AvnNextLTProDemiButton;
import com.app.Smarttys.app.widget.AvnNextLTProDemiTextView;
import com.app.Smarttys.app.widget.AvnNextLTProRegTextView;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.BackUpMessage;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.GmailAccountPojo;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.MessageService;
import com.app.Smarttys.core.socket.SocketManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.ChangeListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * Created by CAS63 on 12/5/2016.
 */
public class Chatbackup extends CoreActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final int REQUEST_CODE_RESOLUTION = 1;
    public static boolean isChatBackUpPage = false;
    //    Session session;
//    Dialog listDialog;
    RelativeLayout backupmain, accountmain, backupovertitle, rlParent;
    ImageView back_navigate;
    AvnNextLTProRegTextView duration, accountspecification, wifi, incvideos, tvLocalTime, tvDriveTime, tvBackUpSize, chatbackupinfo;
    AvnNextLTProDemiTextView text_title_chatbackup, backupover;
    ChatbackupAdapter adaptor;
    private AvnNextLTProDemiTextView title_head, local, gdrive, drivesize,
            title_head2, backupperiod, accountname;
    private AvnNextLTProDemiButton btnBackup;
    private ArrayList<GmailAccountPojo> list = new ArrayList<>();
    private GoogleApiClient mGoogleApiClient;
    private String mCurrentUserId, mChatDbName, mBackupGmailId, mDriveFileName, mBackUpDuration,
            mBackUpOver, mPrevMailId;
    private DriveId mDriveId;
    private long lastNotifyAt = 0, mDriveBackUpFileSize, mDriveBackUpLastTS;
    private String TAG = "Test";
    private SessionManager mSessionMgnr;
    private boolean isGmailChanged, isFromBackupClk;
    private DriveFile mDriveFile;
    private ChangeListener driveFileChangeListener = new ChangeListener() {
        @Override
        public void onChange(ChangeEvent event) {

            mDriveFile.getMetadata(mGoogleApiClient).setResultCallback(new ResultCallback<DriveResource.MetadataResult>() {
                @Override
                public void onResult(DriveResource.MetadataResult metadataResult) {
                    if (metadataResult != null && metadataResult.getStatus().isSuccess()) {
                        mDriveFileName = metadataResult.getMetadata().getTitle();
                        mDriveId = metadataResult.getMetadata().getDriveId();

                        MyLog.d("Drive_Comp_Finish", mDriveId + "");

                        isFromBackupClk = true;
                        updateDataToServer(Calendar.getInstance().getTimeInMillis());
                    }
                    mDriveFile.removeChangeListener(mGoogleApiClient, driveFileChangeListener);
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_backup);
        MyLog.d(TAG, "onCreate: ");
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        back_navigate = findViewById(R.id.back_navigate);
        backupmain = findViewById(R.id.backupmain);
        accountmain = findViewById(R.id.accountmain);
        backupovertitle = findViewById(R.id.backupovertitle);
        rlParent = findViewById(R.id.rlParent);

        duration = findViewById(R.id.backupduration);
        accountspecification = findViewById(R.id.accountspecification);
        getSupportActionBar().hide();

        text_title_chatbackup = findViewById(R.id.text_title_chatbackup);

        backupover = findViewById(R.id.backupover);
        wifi = findViewById(R.id.wifi);
        incvideos = findViewById(R.id.incvideos);

        local = findViewById(R.id.local);
        tvLocalTime = findViewById(R.id.tvLocalTime);
        gdrive = findViewById(R.id.gdrive);
        tvDriveTime = findViewById(R.id.tvDriveTime);
        drivesize = findViewById(R.id.drivesize);
        tvBackUpSize = findViewById(R.id.tvBackUpSize);
        chatbackupinfo = findViewById(R.id.chatbackupinfo);
        backupperiod = findViewById(R.id.backupperiod);
        accountname = findViewById(R.id.accountname);
        title_head2 = findViewById(R.id.title_head2);
       /* final View view = getLayoutInflater().inflate(R.layout.account_listview, null);
        setContentView(view);*/
        btnBackup = findViewById(R.id.btnBackup);

        btnBackup.setOnClickListener(Chatbackup.this);
        back_navigate.setOnClickListener(Chatbackup.this);
        backupmain.setOnClickListener(Chatbackup.this);
        accountmain.setOnClickListener(Chatbackup.this);
        backupovertitle.setOnClickListener(this);

        initProgress("Backup in progress...", true);

        mSessionMgnr = SessionManager.getInstance(Chatbackup.this);

        mChatDbName = MessageDbController.DB_NAME;
        mCurrentUserId = mSessionMgnr.getCurrentUserID();
        mBackupGmailId = mSessionMgnr.getBackMailAccount();
        mBackUpDuration = mSessionMgnr.getBackUpDuration();
        mBackUpOver = mSessionMgnr.getBackUpOver();
        mDriveBackUpFileSize = mSessionMgnr.getBackUpSize();
        mDriveBackUpLastTS = mSessionMgnr.getBackUpTS();
        mPrevMailId = mBackupGmailId;

        setTextToTextView();
        getDriveSettings();
    }

    private void setTextToTextView() {
        if (mDriveBackUpLastTS != 0) {
            tvLocalTime.setText(getTimeFormat(mDriveBackUpLastTS));
            tvDriveTime.setText(getTimeFormat(mDriveBackUpLastTS));
        }
        accountspecification.setText(mBackupGmailId);
        duration.setText(mBackUpDuration);
        wifi.setText(mBackUpOver);
    }

    private void getDriveSettings() {
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            MyLog.d(TAG, "getDriveSettings: ");
            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GET_GOOGLE_DRIVE_SETTINGS);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private ArrayList<GmailAccountPojo> getData() {
        ArrayList<GmailAccountPojo> accountsList = new ArrayList<>();
        return accountsList;
    }

    private void saveToDrive(final DriveFolder pFldr, final String titl, final String mime, final java.io.File file) {
        if (mGoogleApiClient != null && pFldr != null && titl != null && mime != null && file != null)
            try {
                // create content from file
                Drive.DriveApi.newDriveContents(mGoogleApiClient).setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {
                    @Override
                    public void onResult(DriveApi.DriveContentsResult driveContentsResult) {
                        final DriveContents cont = driveContentsResult != null && driveContentsResult.getStatus().isSuccess() ?
                                driveContentsResult.getDriveContents() : null;

                        // write file to content, chunk by chunk
                        if (cont != null) try {
                            OutputStream oos = cont.getOutputStream();
                            if (oos != null) try {
                                InputStream is = new FileInputStream(file);
                                byte[] buf = new byte[4096];
                                int c;
                                while ((c = is.read(buf, 0, buf.length)) > 0) {
                                    oos.write(buf, 0, c);
                                    oos.flush();
                                }
                                is.close();
                            } finally {
                                oos.close();
                            }

                            // content's COOL, create metadata
                            MetadataChangeSet meta = new MetadataChangeSet.Builder()
                                    .setTitle(titl).setMimeType(mime).build();

                            // now create file on GooDrive
                            pFldr.createFile(mGoogleApiClient, meta, cont).setResultCallback(new ResultCallback<DriveFolder.DriveFileResult>() {
                                @Override
                                public void onResult(DriveFolder.DriveFileResult driveFileResult) {
                                    if (driveFileResult != null && driveFileResult.getStatus().isSuccess()) {

                                        mDriveFile = null;
                                        mDriveFile = driveFileResult.getDriveFile();
                                        if (mDriveFile != null) {

                                            mDriveFile.addChangeListener(mGoogleApiClient, driveFileChangeListener);
                                        }
                                    } else {
                                        hideProgressDialog();
                                        Toast.makeText(Chatbackup.this, "Try again", Toast.LENGTH_SHORT).show();

                                    }
                                }
                            });
                        } catch (Exception e) {
                            MyLog.e(TAG, "", e);
                        }
                    }
                });
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
    }

    private void updateDataToServer(long backUpTime) {
        BackUpMessage message = new BackUpMessage();
        JSONObject object = message.getBackUpMessageObject(mCurrentUserId, mDriveBackUpFileSize, mDriveFileName, mDriveId,
                backUpTime, mBackUpDuration, mBackupGmailId, mBackUpOver);

        try {
            JSONObject driveObj = new JSONObject();
            driveObj.put("from", mCurrentUserId);
            driveObj.put("drive_settings", object);
            SendMessageEvent backupEvent = new SendMessageEvent();
            backupEvent.setEventName(SocketManager.EVENT_UPDATE_GOOGLE_DRIVE_SETTINGS);
            backupEvent.setMessageObject(driveObj);
            EventBus.getDefault().post(backupEvent);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient == null && mBackupGmailId != null && !mBackupGmailId.equals("")) {
            // Create the API client and bind it to an instance variable.
            // We use this instance as the callback for connection and connection
            // failures.
            // Since no account name is passed, the user is prompted to choose.
            connectGoogleApiClient();
        } else if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            connectGoogleApiClient();
        }
    }

    private void connectGoogleApiClient() {
        if (!mBackupGmailId.equals("")) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addScope(Drive.SCOPE_APPFOLDER)
                    .setAccountName(mBackupGmailId)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onPause() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onPause();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == REQUEST_CODE_RESOLUTION && resultCode == RESULT_OK) {
            isGmailChanged = true;
        } else {
            mBackupGmailId = mPrevMailId;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (isGmailChanged) {
            isGmailChanged = false;
        }

        switch (result.getErrorCode()) {
            case ConnectionResult.INVALID_ACCOUNT:
                showGmailLoginAlert("Login with " + mBackupGmailId + " in gmail app and then try to backup your "
                        + " chat history.");
                break;

            case ConnectionResult.INTERNAL_ERROR:
                Toast.makeText(Chatbackup.this, "Internal Error Occurred", Toast.LENGTH_SHORT).show();
                break;

            case ConnectionResult.TIMEOUT:
                Toast.makeText(Chatbackup.this, "Timeout", Toast.LENGTH_SHORT).show();
                break;

            case ConnectionResult.NETWORK_ERROR:
                Toast.makeText(Chatbackup.this, "Network Error", Toast.LENGTH_SHORT).show();
                break;

            default:
                // show the localized error dialog.
                if (result.hasResolution()) {
                    try {
                        long currentTime = Calendar.getInstance().getTimeInMillis();
                        long timeDiff = currentTime - lastNotifyAt;
                        if (timeDiff > 15 * 1000) {
                            lastNotifyAt = currentTime;
                            result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
                        }
                    } catch (IntentSender.SendIntentException e) {
                        MyLog.e(TAG, "Exception while starting resolution activity", e);
                    }
                } else {
                    mBackupGmailId = mPrevMailId;
//                    GoogleApiAvailability.getInstance().getErrorDialog(this, result.getErrorCode(), 0).show();
                    new AlertDialog.Builder(Chatbackup.this)
                            .setTitle("Signin Failed")
                            .setMessage("Try with different account")
                            .setCancelable(false)
                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Whatever...
                                    dialog.dismiss();
                                }
                            }).show();
                    return;
                }
        }
    }

    private void showGmailLoginAlert(String msg) {
        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(msg);
        dialog.setPositiveButtonText("Proceed");
        dialog.setNegativeButtonText("Cancel");
        dialog.setCancelable(false);
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                Intent addAccountIntent = new Intent(android.provider.Settings.ACTION_ADD_ACCOUNT)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    addAccountIntent.putExtra(Settings.EXTRA_ACCOUNT_TYPES, new String[]{"com.google"});
                }
                startActivity(addAccountIntent);
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getSupportFragmentManager(), "Account Alert");
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "API client connected.");
        if (isGmailChanged) {
            isGmailChanged = false;
            performBackup();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        if (isGmailChanged) {
            isGmailChanged = false;
        }
        Log.i(TAG, "GoogleApiClient connection suspended");
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.back_navigate:
                finish();
                break;

            case R.id.backupmain:
                if (ConnectivityInfo.isInternetConnected(Chatbackup.this)) {
                    pickBackupDuration();
                } else {
                    Toast.makeText(Chatbackup.this, "Please check your network connection", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.accountmain:
                if (ConnectivityInfo.isInternetConnected(Chatbackup.this)) {
                    pickGmailAccount();
                } else {
                    Toast.makeText(Chatbackup.this, "Please check your network connection", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btnBackup:
                if (ConnectivityInfo.isInternetConnected(Chatbackup.this)) {
                    if (mBackupGmailId != null && !mBackupGmailId.equals("")) {
                        performBackup();
                    } else {
                        Toast.makeText(Chatbackup.this, "Please choose gmail account and try again.", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(Chatbackup.this, "Please check your network connection", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.backupovertitle:
                if (ConnectivityInfo.isInternetConnected(Chatbackup.this)) {
                    pickNetworkConnection();
                } else {
                    Toast.makeText(Chatbackup.this, "Please check your network connection", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void pickNetworkConnection() {
        final Dialog dialog = new Dialog(Chatbackup.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RadioGroup rgroup;
        final RadioButton rb1, rb2;
        final AvnNextLTProDemiTextView cancel;

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_pick_network_mode);

        rgroup = dialog.findViewById(R.id.radiogroup_chatbackup);
        rb1 = dialog.findViewById(R.id.r1_chatbackup);
        rb2 = dialog.findViewById(R.id.r2_chatbackup);


        Typeface typeface = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        rb1.setTypeface(typeface);
        rb2.setTypeface(typeface);

        cancel = dialog.findViewById(R.id.cancel_chatbackup);

        dialog.setCanceledOnTouchOutside(true);
        dialog.show();

        rgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (rb1.isChecked()) {
                    mBackUpOver = getResources().getString(R.string.wifi);
                } else {
                    mBackUpOver = getResources().getString(R.string.Wifi_or_Cellular);
                }
                dialog.dismiss();
                updateDataToServer(mDriveBackUpLastTS);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void performBackup() {
        if (!mBackUpDuration.equalsIgnoreCase(MessageService.BACK_UP_NEVER)) {
            File file = new File("/data/data/" + getPackageName() + "/databases/"
                    + MessageDbController.DB_NAME);

            File backUpFolder = new File(MessageFactory.DATABASE_PATH);
            if (!backUpFolder.exists()) {
                backUpFolder.mkdirs();
            }
            String backUpFilePath = backUpFolder + "/" + getResources().getString(R.string.app_name) + "_db_lite.zip";

            zip(file.getAbsolutePath(), backUpFilePath);
        }
    }

    public void zip(String files, String zipFileName) {
        try {
            BufferedInputStream origin = null;
            File zipFile = new File(zipFileName);
            if (zipFile.exists()) {
                zipFile.delete();
                zipFile.createNewFile();
            } else {
                zipFile.createNewFile();
            }
            FileOutputStream dest = new FileOutputStream(zipFileName);
            BufferedOutputStream bufferedStream = new BufferedOutputStream(dest);
            ZipOutputStream out = new ZipOutputStream(bufferedStream);
            byte data[] = new byte[1024];

            FileInputStream fi = new FileInputStream(files);
            origin = new BufferedInputStream(fi, 1024);

            ZipEntry entry = new ZipEntry(files);
            out.putNextEntry(entry);
            int count;

            while ((count = origin.read(data, 0, 1024)) != -1) {
                out.write(data, 0, count);
            }

            fi.close();
            origin.close();

            out.close();
            dest.close();
            bufferedStream.close();
            uploadToServer();
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void uploadToServer() {
        showProgressDialog();

        File dbDir = new File(MessageFactory.DATABASE_PATH);
        String backUpFileName = getResources().getString(R.string.app_name) + "_db_lite.zip";
        String filePath = dbDir + "/" + backUpFileName;

        File backUpFile = new File(filePath);
        if (backUpFile.exists()) {
            mDriveBackUpFileSize = backUpFile.length();
            String gdFileName = mCurrentUserId + "/" + backUpFileName;

            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//                saveToDrive(Drive.DriveApi.getAppFolder(mGoogleApiClient), gdFileName,
//                        "*/*", backUpFile);
                saveToDrive(Drive.DriveApi.getAppFolder(mGoogleApiClient), gdFileName,
                        "*/*", backUpFile);
            } else {
                hideProgressDialog();
                connectGoogleApiClient();
                Toast.makeText(Chatbackup.this, "Your gmail session expired. Please select your gmail and try again!", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void pickBackupDuration() {
        final Dialog dialog = new Dialog(Chatbackup.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RadioGroup rgroup;
        final RadioButton rb1, rb2, rb3, rb4, rb5;
        final AvnNextLTProDemiTextView cancel;

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_backup);

        rgroup = dialog.findViewById(R.id.radiogroup_chatbackup);
        rb1 = dialog.findViewById(R.id.r1_chatbackup);
        rb2 = dialog.findViewById(R.id.r2_chatbackup);
        rb3 = dialog.findViewById(R.id.r3_chatbackup);
        rb4 = dialog.findViewById(R.id.r4_chatbackup);
        rb5 = dialog.findViewById(R.id.r5_chatbackup);

        Typeface typeface = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        rb1.setTypeface(typeface);
        rb2.setTypeface(typeface);
        rb3.setTypeface(typeface);
        rb4.setTypeface(typeface);
        rb5.setTypeface(typeface);

        cancel = dialog.findViewById(R.id.cancel_chatbackup);

        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        String backUpDuration = mSessionMgnr.getBackUpDuration();
        if (backUpDuration.equalsIgnoreCase(MessageService.BACK_UP_NEVER)) {
            rb1.setChecked(true);
        } else if (backUpDuration.equalsIgnoreCase(MessageService.BACK_UP_ONLY_I_TAP)) {
            rb2.setChecked(true);
        } else if (backUpDuration.equalsIgnoreCase(MessageService.BACK_UP_DAILY)) {
            rb3.setChecked(true);
        } else if (backUpDuration.equalsIgnoreCase(MessageService.BACK_UP_WEEKLY)) {
            rb4.setChecked(true);
        } else if (backUpDuration.equalsIgnoreCase(MessageService.BACK_UP_MONTHLY)) {
            rb5.setChecked(true);
        }
        rgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (rb1.isChecked()) {
                    mBackUpDuration = MessageService.BACK_UP_NEVER;
                    dialog.dismiss();

                } else if (rb2.isChecked()) {
                    mBackUpDuration = MessageService.BACK_UP_ONLY_I_TAP;
                    dialog.dismiss();

                } else if (rb3.isChecked()) {
                    mBackUpDuration = MessageService.BACK_UP_DAILY;
                    dialog.dismiss();

                } else if (rb4.isChecked()) {
                    mBackUpDuration = MessageService.BACK_UP_WEEKLY;
                    dialog.dismiss();

                } else if (rb5.isChecked()) {
                    mBackUpDuration = MessageService.BACK_UP_MONTHLY;
                    dialog.dismiss();

                }

                isFromBackupClk = false;
                updateDataToServer(mDriveBackUpLastTS);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
    }

    private void pickGmailAccount() {
        final Dialog listDialog = new Dialog(Chatbackup.this);
        listDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        listDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v1 = li.inflate(R.layout.account_listview, null, false);

        listDialog.setContentView(v1);
        listDialog.setCancelable(true);
        list = getData();
        final RecyclerView listView = listDialog.findViewById(R.id.listView1);
        LinearLayoutManager manager = new LinearLayoutManager(Chatbackup.this);
        listView.setLayoutManager(manager);

        adaptor = new ChatbackupAdapter(Chatbackup.this, list);
        listView.setAdapter(adaptor);

        adaptor.setItemClickListener(listView, new RecyclerViewItemClickListener() {
            @Override
            public void onRVItemClick(View parentView, int position) {
                if (!list.get(position).getMailId().equals(mBackupGmailId)) {
                    mGoogleApiClient = null;
                    listDialog.dismiss();

                    mPrevMailId = mBackupGmailId;
                    mBackupGmailId = list.get(position).getMailId();
                    isGmailChanged = true;
                    connectGoogleApiClient();
                }
            }
        });

        listDialog.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_UPDATE_GOOGLE_DRIVE_SETTINGS)) {
            String data = event.getObjectsArray()[0].toString();

            try {
                JSONObject object = new JSONObject(data);
                String from = object.getString("from");

                if (from.equalsIgnoreCase(mCurrentUserId)) {
                    hideProgressDialog();

                    if (isFromBackupClk) {
                        mDriveBackUpLastTS = mSessionMgnr.getBackUpTS();
                        mDriveBackUpFileSize = mSessionMgnr.getBackUpSize();
                        isFromBackupClk = false;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvBackUpSize.setText(getSizeFormat());
                                setTextToTextView();
                            }
                        });
                        Toast.makeText(Chatbackup.this, "Backup completed", Toast.LENGTH_SHORT).show();
                    }
                    mSessionMgnr.setBackUpMailAccount(mBackupGmailId);
                    accountspecification.setText(mBackupGmailId);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mBackUpOver = mSessionMgnr.getBackUpOver();
                            mBackUpDuration = mSessionMgnr.getBackUpDuration();

                            setTextToTextView();
                        }
                    });
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GET_GOOGLE_DRIVE_SETTINGS)) {
            String data = event.getObjectsArray()[0].toString();

            try {
                JSONObject object = new JSONObject(data);
                String from = object.getString("from");

                if (from.equalsIgnoreCase(mCurrentUserId)) {
                    hideProgressDialog();

                    JSONObject driveObj = object.getJSONObject("drive_settings");

                    if (driveObj.length() > 0) {
                        mDriveBackUpFileSize = driveObj.getLong("FileSize");
                        mDriveBackUpLastTS = driveObj.getLong("CreatedTs");
                        mBackupGmailId = driveObj.getString("BackUpGmailId");
                        mBackUpOver = driveObj.getString("BackUpOver");
                        mBackUpDuration = driveObj.getString("BackUpDuration");

                        mDriveFileName = driveObj.getString("FileName");
                        mDriveId = DriveId.decodeFromString(driveObj.getString("DriveId"));

                        mSessionMgnr.setBackUpMailAccount(mBackupGmailId);
                        mSessionMgnr.setBackUpSize(mDriveBackUpFileSize);
                        mSessionMgnr.setBackUpTS(mDriveBackUpLastTS);
                        mSessionMgnr.setBackUpOver(mBackUpOver);
                        mSessionMgnr.setBackUpDuration(mBackUpDuration);
                        mSessionMgnr.setBackUpDriveFileName(mDriveFileName);
                        mSessionMgnr.setBackUpDriveFileId(driveObj.getString("DriveId"));

                        final String backUpTime = getTimeFormat(mDriveBackUpLastTS);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvBackUpSize.setText(getSizeFormat());
                                tvLocalTime.setText(backUpTime);
                                tvDriveTime.setText(backUpTime);
                            }
                        });
                    }
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        }
    }

    private String getTimeFormat(long timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a", Locale.ENGLISH);
        Date dateTs = new Date(timeStamp);
        return format.format(dateTs);
    }

    private String getSizeFormat() {
        long fileSizeInKB = mDriveBackUpFileSize / 1024;
        long fileSizeInMB = fileSizeInKB / (1024 * 1024);

        String size = fileSizeInKB + " KB";
        if (fileSizeInMB > 0) {
            size = fileSizeInMB + " MB";
        }
        return size;
    }

    @Override
    public void onStart() {
        super.onStart();
        isChatBackUpPage = true;
        EventBus.getDefault().register(Chatbackup.this);

    }

    @Override
    public void onStop() {
        super.onStop();
        isChatBackUpPage = false;
        EventBus.getDefault().unregister(Chatbackup.this);
    }

    /*public class BackUpCompleteSvc extends DriveEventService {

        @Override
        public void onCompletion(CompletionEvent completionEvent) {
            super.onCompletion(completionEvent);
            DriveId driveId = completionEvent.getDriveId();
            Log.d("Drive_Comp_File ResourceId: ", "" + driveId.getResourceId());
            DriveId folderDriveId = getParentID(driveId);
            if (folderDriveId != null && folderDriveId.getResourceId() != null)
                Log.d("Drive_Comp_Folder ResourceId: ", "" + folderDriveId.getResourceId());
        }
    }*/

    public DriveId getParentID(DriveId driveId) {
        MetadataBuffer mdb = null;
        DriveApi.MetadataBufferResult mbRslt = driveId.asDriveResource().listParents(mGoogleApiClient).await();
        if (mbRslt.getStatus().isSuccess()) try {
            mdb = mbRslt.getMetadataBuffer();
            if (mdb.getCount() > 0)
                return mdb.get(0).getDriveId();
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        } finally {
            if (mdb != null) mdb.close();
        }
        return null;
    }
}




