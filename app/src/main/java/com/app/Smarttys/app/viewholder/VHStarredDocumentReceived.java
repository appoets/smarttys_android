package com.app.Smarttys.app.viewholder;

import android.text.Html;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;


/**
 * Created by CAS60 on 2/2/2017.
 */
public class VHStarredDocumentReceived extends RecyclerView.ViewHolder {


    public TextView senderName, message, time, datelbl, tvInfoMsg, fromname, toname;
    public RelativeLayout mainReceived;
    public ImageView starred, userprofile, ivDoc;
    public ImageButton iBtnDownload;
    public ProgressBar Pbdownload;
    public RelativeLayout relative_layout_message;


    public VHStarredDocumentReceived(View view) {
        super(view);

        relative_layout_message = view.findViewById(R.id.relative_layout_message);
        toname = view.findViewById(R.id.toname);
        fromname = view.findViewById(R.id.fromname);
        senderName = view.findViewById(R.id.lblMsgFrom);

        message = view.findViewById(R.id.txtMsg);

        time = view.findViewById(R.id.ts);

        datelbl = view.findViewById(R.id.datelbl);

        starred = view.findViewById(R.id.starredindicator);
        userprofile = view.findViewById(R.id.userprofile);
        ivDoc = view.findViewById(R.id.ivDoc);

        mainReceived = view.findViewById(R.id.mainReceived);

        iBtnDownload = view.findViewById(R.id.iBtnDownload);
        Pbdownload = view.findViewById(R.id.pbUpload);
        tvInfoMsg = view.findViewById(R.id.tvInfoMsg);
        message.setText(Html.fromHtml(message.getText().toString() + " &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;")); // 10 spaces

    }
}

