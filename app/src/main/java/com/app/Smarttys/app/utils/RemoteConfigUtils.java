package com.app.Smarttys.app.utils;

/**
 * Created by user134 on 5/4/2018.
 */

/*
public class RemoteConfigUtils {

    private FirebaseRemoteConfig mFirebaseRemoteConfig ;
    public static final String KEY_LIVE_VERSION = "live_version";
    public static final String KEY_IS_FORCE_UPDATE = "is_force_update";
    public static final String KEY_MESSAGE = "msg";
    public static final String KEY_MESSAGE_ARABIC = "msg_arabic";
    private static final String TAG = "RemoteConfigUtils";
    private static long lastDialogShowTime ;


    private static final RemoteConfigUtils ourInstance = new RemoteConfigUtils();

    public static RemoteConfigUtils getInstance() {
        return ourInstance;
    }

    private RemoteConfigUtils() {
    }

    public void init() {
        try{
            mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
            //firebase initialization
            FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                    .setDeveloperModeEnabled(BuildConfig.DEBUG)
                    .build();
            mFirebaseRemoteConfig.setConfigSettings(configSettings);

            //load defaults from xml if not available from cache
            mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_default);
        }
        catch (Exception e){
            MyLog.e(TAG, "init: ",e );
        }


    }

    public void checkUpdate(final Activity context) {
        //display current config data
        init();
        if (!FirebaseApp.getApps(context).isEmpty())
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        long cacheExpiration = 0; // 1 hour in seconds.
        if(null==mFirebaseRemoteConfig){
            return;
        }
        // If in developer mode cacheExpiration is set to 0 so each fetch will retrieve values from
        // the server.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        // [START fetch_config_with_callback]
        // cacheExpirationSeconds is set to cacheExpiration here, indicating that any previously
        // fetched and cached config would be considered expired because it would have been fetched
        // more than cacheExpiration seconds ago. Thus the next fetch would go to the server unless
        // throttling is in progress. The default expiration duration is 43200 (12 hours).
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(context, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            //Toast.makeText(context, "Fetch Succeeded", Toast.LENGTH_SHORT).show();

                            // Once the config is successfully fetched it must be activated before newly fetched
                            // values are returned.
                            mFirebaseRemoteConfig.activateFetched();
                        }
                        fetchRemoteValues(context);
                    }
                }).addOnFailureListener(context, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                MyLog.e(TAG, "onFailure: ",e );
            }
        });
    }

    public boolean shallWeShowPopUp(){
        long currentTimeMillis= System.currentTimeMillis();

        long differentMillis= currentTimeMillis- lastDialogShowTime;
        long minimumTimeDiff= TimeUnit.HOURS.toMillis(24); //one day after we show again
        return differentMillis > minimumTimeDiff;

    }
    private void fetchRemoteValues(Context context) {
        if(mFirebaseRemoteConfig!=null) {
            String latestVersion = mFirebaseRemoteConfig.getString(KEY_LIVE_VERSION);
            String isForceUpdateStr = mFirebaseRemoteConfig.getString(KEY_IS_FORCE_UPDATE);
            String msg = mFirebaseRemoteConfig.getString(KEY_MESSAGE);
            String arabicMsg = mFirebaseRemoteConfig.getString(KEY_MESSAGE_ARABIC);
            MyLog.d(TAG, "fetchRemoteValues: latestVersion  "+latestVersion);
            MyLog.d(TAG, "fetchRemoteValues:  isForceUpdateStr "+isForceUpdateStr);
            boolean isForceUpdate = false;
            if(isForceUpdateStr!=null && isForceUpdateStr.equalsIgnoreCase("yes")){
                isForceUpdate=true;
            }
            int liveVersionNumber=0;
            try {
                SharedPrefUtil.write(KEY_IS_FORCE_UPDATE, isForceUpdate);
                SharedPrefUtil.write(KEY_MESSAGE, msg);
                SharedPrefUtil.write(KEY_MESSAGE_ARABIC, msg);
                liveVersionNumber = Integer.parseInt(latestVersion);
                SharedPrefUtil.writeInt(KEY_LIVE_VERSION, liveVersionNumber);
            }
            catch (Exception e){
                MyLog.e(TAG, "fetchRemoteValues: ",e );
            }
            if(isForceUpdate || shallWeShowPopUp()){
                 try {

                     int currentVersionNumber= context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
                     MyLog.d(TAG, "fetchRemoteValues currentVersionNumber: "+currentVersionNumber);
                     MyLog.d(TAG, "fetchRemoteValues liveVersionNumber: "+liveVersionNumber);
                     if(currentVersionNumber>0 && liveVersionNumber>currentVersionNumber) {
                         showUpdateDialog(context,isForceUpdate,msg);
                     }
                 }
                 catch (Exception e){
                     MyLog.e(TAG, "fetchRemoteValues: ",e );
                 }
             }
        }
    }


    public  void showUpdateDialog(final Context context,boolean isForceUpdate,String remoteMsg){
        try {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyleNew);
            builder.setTitle(R.string.update_available);
            String okTitle=context.getString(R.string.update);
            if(isForceUpdate) {
                builder.setCancelable(false);
            }
             String msg="There is newer version of this application available, click "+okTitle+" to upgrade now";
            if(remoteMsg!=null && !remoteMsg.trim().isEmpty() && remoteMsg.length()>0){
                msg=remoteMsg;
            }
            boolean isArabic=context.getResources().getBoolean(R.bool.is_arabic);

            if(isArabic){
                String arabicMsg = SharedPrefUtil.read(RemoteConfigUtils.KEY_MESSAGE_ARABIC, "");
                if(!arabicMsg.isEmpty())
                    msg=arabicMsg;
            }
            builder.setMessage(msg);
            builder.setPositiveButton(okTitle, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                    final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    } catch (Exception e) {
                        MyLog.e(TAG, "onClick: ", e);
                    }
                }
            });//second parameter used for onclicklistener
            if(!isForceUpdate)
            builder.setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
            lastDialogShowTime=System.currentTimeMillis();
        }
        catch (Exception e){
            MyLog.e(TAG, "showUpdateDialog: ",e );
        }
    }

}
*/
