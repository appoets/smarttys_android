package com.app.Smarttys.app.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.model.MessageItemChat;

import java.io.File;

public class DocOpenUtils {
    private static final String TAG = "DocOpenUtils";

    public static void openDocument(MessageItemChat message, Activity context) {


        String extension = MimeTypeMap.getFileExtensionFromUrl(message.getChatFileLocalPath());
        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

        PackageManager packageManager = context.getPackageManager();
        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        testIntent.setType(mimeType);
        File file = new File(message.getChatFileLocalPath());
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), mimeType);
        if (file.exists()) {
            try {
                context.startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(context, "No app installed to open this document!", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "onClick: ", e);
            }
        } else {
            openDocInBrowser(message, context);
        }

    }

    private static void openDocInBrowser(MessageItemChat message, Activity context) {
        Uri buildUri = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(message.getChatFileLocalPath());
        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
        if (message.isSelf())
            buildUri = Uri.parse(message.getChatFileLocalPath());
        else
            buildUri = Uri.parse(AppUtils.getValidGroupPath(message.getChatFileServerPath()));


        buildUri.buildUpon()
                //        .appendQueryParameter("id", "0")
                .appendQueryParameter("atoken", SessionManager.getInstance(context).getSecurityToken())
                .appendQueryParameter("au", SessionManager.getInstance(context).getCurrentUserID())
                .appendQueryParameter("at", "site")
                .build();
        browserIntent.setDataAndType(buildUri, mimeType);
        Intent chooser = Intent.createChooser(browserIntent, mimeType);
        chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // optional
        context.startActivity(chooser);
    }
}
