package com.app.Smarttys.app.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.Smarttys.R;


/**
 *
 */
public class VHStarredMessageReceived extends RecyclerView.ViewHolder {

    public TextView senderName, message, time, tvInfoMsg, lblMsgFrom2, fromname, toname, datelbl, relpaymessage_recevied, relpaymessage_receviedmedio, ts_below;
    public RelativeLayout mainReceived;
    public ImageView starred, sentimage, cameraphoto, audioimage, pdfimage, personimage, starredindicator_below, userprofile;
    public RelativeLayout relative_layout_message, replaylayout_recevied;


    public VHStarredMessageReceived(View view) {
        super(view);

        datelbl = view.findViewById(R.id.datelbl);
        toname = view.findViewById(R.id.toname);
        fromname = view.findViewById(R.id.fromname);
        // 10 spaces
        pdfimage = view.findViewById(R.id.pdfimage);
        relative_layout_message = view.findViewById(R.id.relative_layout_message);
        lblMsgFrom2 = view.findViewById(R.id.lblMsgFrom2);
        relpaymessage_recevied = view.findViewById(R.id.relpaymessage_recevied);
        relpaymessage_receviedmedio = view.findViewById(R.id.relpaymessage_receviedmedio);
        senderName = view.findViewById(R.id.lblMsgFrom);
        replaylayout_recevied = view.findViewById(R.id.replaylayout_recevied);
        message = view.findViewById(R.id.txtMsg);
        cameraphoto = view.findViewById(R.id.cameraphoto);
        audioimage = view.findViewById(R.id.audioimage);
        sentimage = view.findViewById(R.id.sentimage);
        time = view.findViewById(R.id.ts);
        personimage = view.findViewById(R.id.personimage);
        starred = view.findViewById(R.id.starredindicator);
        starredindicator_below = view.findViewById(R.id.starredindicator_below);
        mainReceived = view.findViewById(R.id.mainReceived);
        ts_below = view.findViewById(R.id.ts_below);
        userprofile = view.findViewById(R.id.userprofile);
        tvInfoMsg = view.findViewById(R.id.tvInfoMsg);

    }
}
