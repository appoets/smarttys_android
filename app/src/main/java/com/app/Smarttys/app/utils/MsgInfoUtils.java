package com.app.Smarttys.app.utils;

import android.util.Log;

import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.MessageItemChat;

import org.json.JSONArray;
import org.json.JSONObject;

public class MsgInfoUtils {
    private static final String TAG = "MsgInfoUtils";

    public static String getMessageStatus(MessageItemChat messageItemChat, String currentUserId) {
        String status = MessageFactory.DELIVERY_STATUS_SENT;
        int deliverPendingCount = 0, readPendingCount = 0;
        int totalMembers = 0;
        Log.d(TAG, "getMessageStatus: ");
        try {
            String infoData = messageItemChat.getGroupMsgDeliverStatus();
            JSONObject object = new JSONObject(infoData);
            JSONArray arrMembers = object.getJSONArray("GroupMessageStatus");
            totalMembers = arrMembers.length();
            for (int i = 0; i < arrMembers.length(); i++) {
                JSONObject userObj = arrMembers.getJSONObject(i);
                String receiverId = userObj.getString("UserId");

                if (!receiverId.equals(currentUserId)) {

                    String deliverStatus = userObj.getString("DeliverStatus");
                    //   String deliverTime = userObj.getString("DeliverTime");
                    //  String readTime = userObj.getString("ReadTime");
                    if (!deliverStatus.equals(MessageFactory.DELIVERY_STATUS_DELIVERED) &&
                            !deliverStatus.equals(MessageFactory.DELIVERY_STATUS_READ)) {
                        deliverPendingCount += 1;
                        readPendingCount += 1;

                    } else if (!deliverStatus.equals(MessageFactory.DELIVERY_STATUS_READ)) {
                        readPendingCount += 1;
                    }

                }

            }
        } catch (Exception e) {
            Log.e(TAG, "getMessageStatus: ", e);
        }

        Log.d(TAG, "getMessageStatus: totalMembers " + totalMembers);
        Log.d(TAG, "getMessageStatus: deliverPendingCount " + deliverPendingCount);
        Log.d(TAG, "getMessageStatus: readPendingCount " + readPendingCount);

        if (readPendingCount == 0 && deliverPendingCount == 0) {
            status = MessageFactory.DELIVERY_STATUS_READ;
        } else if (deliverPendingCount == 0) {
            status = MessageFactory.DELIVERY_STATUS_DELIVERED;
        }

        Log.d(TAG, "getMessageStatus: " + status);
        return status;
    }

}
