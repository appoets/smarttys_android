package com.app.Smarttys.fcm;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.app.Smarttys.R;
import com.app.Smarttys.app.calls.CallAck;
import com.app.Smarttys.app.calls.CallsActivity;
import com.app.Smarttys.app.calls.IncomingCallActivity;
import com.app.Smarttys.app.model.GroupInviteBraodCast;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.AutoDownLoadUtils;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.ShortcutBadgeManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.IncomingMessage;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.message.MesssageObjectReceiver;
import com.app.Smarttys.core.model.CallItemChat;
import com.app.Smarttys.core.model.ChatLockPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.MuteStatusPojo;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.MessageService;
import com.app.Smarttys.core.socket.NotificationUtil;
import com.app.Smarttys.core.socket.SocketManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import me.leolin.shortcutbadger.ShortcutBadger;

import static com.app.Smarttys.core.CoreController.isRaad;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String FCM_PARAM = "picture";
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private static final String CHANNEL_NAME = "FCM";
    private static final String CHANNEL_DESC = "Firebase Cloud Messaging";
    public static int missedCallCount = 0;
    public static String lastMissedCallId = "";
    public static Context mContext;
    public static ArrayList<HashMap<String, Boolean>> chat = new ArrayList<>();
    public static Ringtone ringtone;
    public static Vibrator vibrator;
    public static String token;
    public LinkedList<String> messageIds = new LinkedList<>();
    public SocketManager mSocketManager = null;
    String uniqueCurrentID = "";
    Getcontactname getcontactname;
    boolean secretchat = false;
    boolean singletchat_boolean = false;
    boolean isMuted = false;
    private Handler incomCallBroadcastHandler;
    private String mCurrentUserId;
    private MesssageObjectReceiver objectReceiver;
    private NotificationUtils notificationUtils;
    private Session session;
    private IncomingMessage incomingMsg;
    private UserInfoSession userInfoSession;
    private SessionManager sessionManager;
    private NotificationChannel mChannel;
    private NotificationManager notifManager;
    private int numMessages = 0;

    public static String getFCMToken(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        return prefs.getString(AppConfig.REG_ID, "");
    }

    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        super.onNewToken(refreshedToken);
//        String s = FirebaseInstanceId.getInstance().getToken();
        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);
        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);
        token = refreshedToken;
        System.out.println("token-----------------: " + refreshedToken);
        storeToken(this, token);
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        MyLog.e(TAG, "sendRegistrationToServer: " + token);
    }

    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.apply();
    }

    private void storeToken(Context context, String regId) {
        final SharedPreferences prefs = context.getSharedPreferences(TAG,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConfig.REG_ID, regId);
        editor.apply();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        mContext = MyFirebaseMessagingService.this;
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        incomingMsg = new IncomingMessage(MyFirebaseMessagingService.this);
        userInfoSession = new UserInfoSession(MyFirebaseMessagingService.this);
        getcontactname = new Getcontactname(MyFirebaseMessagingService.this);
        incomingMsg = new IncomingMessage(MyFirebaseMessagingService.this);
        sessionManager = SessionManager.getInstance(MyFirebaseMessagingService.this);
        mCurrentUserId = uniqueCurrentID;
        System.out.println("print unique user id: " + uniqueCurrentID);
        Log.e("TAG", "From: " + remoteMessage.getFrom());
        System.out.println("--------FCM Received-------" + remoteMessage);

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e("TAG", "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            try {
                Map<String, String> data = remoteMessage.getData();
                Log.e("TAG", "Data Payload: " + data);
                JSONObject json = new JSONObject(data);
                System.out.println("jsoon data: -------->>>> " + json);
                if (json.has("roomid")) {
                    handleDataMessage(json);
                } else {
                    System.out.println("---------message-----------");
                    try {
                        String chatType = json.getString("messageType");
                        String Secretchat = json.optString("secret_type");
                        secretchat = Secretchat.equalsIgnoreCase("1");
                        if (chatType.equalsIgnoreCase("single_message")) {
                            singletchat_boolean = true;
                        } else if (chatType.equals("group_invite")) {
                            if (isRaad) {
                                MesssageObjectReceiver.addToDBForApproval(json, this, mCurrentUserId);
                                NotificationUtil.getInstance().newGroupNotification(this, json);
                                EventBus.getDefault().post(new GroupInviteBraodCast());
                            }
                        }

                        try {
                            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
                            if (json.has("convId")) {
                                String convId = json.getString("convId");
                                MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(uniqueCurrentID, null, convId, secretchat);
                                if (muteData != null && muteData.getMuteStatus().equals("1")) {
                                    Date cDate = new Date();
                                    isMuted = cDate.getTime() <= muteData.getExpireTs();
                                }
                            }
                            String stat = "";
                            String pwd;
                            if (json.has("doc_id")) {
                                String docId = json.getString("doc_id");
                                ChatLockPojo lockPojo = getChatLockdetailfromDB(docId, chatType);
                                if (sessionManager.getLockChatEnabled().equals("1") && lockPojo != null) {
                                    stat = lockPojo.getStatus();
                                    pwd = lockPojo.getPassword();
                                }
                            }
                            if (!isMuted) {
                                if (sessionManager.getlogin()) {
                                    //  CustomshowNotification(data_object, json.optJSONObject("data").optString("from"), json.optJSONObject("data").optString("type"), singletchat_boolean, secretchat);
                                    boolean isLockChat = false;
                                    if (stat != null && stat.equals("1")) {
                                        isLockChat = true;
                                    }
//                                    String from=json.optJSONObject("data").optString("from");
                                    //NotificationUtil.getInstance(). CustomshowNotification(this,data_object,from , json.optJSONObject("data").optString("type"), singletchat_boolean, secretchat,isLockChat);
                                    if (!AppUtils.isServiceRunning(this, MessageService.class))
                                        AppUtils.startService(this, MessageService.class);
                                    loadMessage(json, singletchat_boolean, secretchat);
                                }
                            }
                        } catch (Exception exx) {
                            MyLog.e(TAG, "handleDataMessage: ", exx);
                        }
                    } catch (Exception e) {
                        MyLog.e(TAG, "Exception: " + e.getMessage());
                    }
                }
//                handleNotification(json);
            } catch (Exception e) {
                Log.e("TAG", "Exception get data: " + e.getMessage());
            }
        }
    }

    public ChatLockPojo getChatLockdetailfromDB(String documentID, String chatType) {
        MessageDbController dbController = CoreController.getDBInstance(this);
        String convId = userInfoSession.getChatConvId(documentID);
        String receiverId = userInfoSession.getReceiverIdByConvId(convId);
        return dbController.getChatLockData(receiverId, chatType);
    }

    private synchronized void loadMessage(JSONObject objects, boolean singletchat_boolean, boolean secretchat) {
        System.out.println("load message------------------>>>>");
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        Log.d(TAG, "loadMessagefcm: 1");
        try {
            MessageItemChat item = null;
            int normal_Offline = 0;
            String type = objects.optString("type");
            Log.d(TAG, "loadMessagefcm: 2");
            if (type != null && !type.isEmpty() && Integer.parseInt(objects.optString("type")) == 23) {
                return;
            }
            Log.d(TAG, "loadMessagefcm: 3");
            if (objects.has("is_deleted_everyone")) {
                normal_Offline = objects.optInt("is_deleted_everyone");
            }
            if (type != null && type.equalsIgnoreCase("" + MessageFactory.timer_change)) {
                //        loadSecretTimerChangeMessage(objects);
            } else {
                Log.d(TAG, "loadMessagefcm: 4");
                item = incomingMsg.loadSingleMessage(objects);
                String from = objects.optString("from");
                String to = objects.optString("to");
                String secretType = objects.optString("secret_type");
                String msgId = objects.optString("msgId");
                //String status = objects.getString("status");
                //String message_status = objects.getString("message_status");

                MyLog.d(TAG, "loadMessageMS messageIds: " + messageIds);
                if (messageIds.contains(msgId)) {
                    return;
                }
                Log.d(TAG, "loadMessagefcm: 5");
                MyLog.d(TAG, "loadMessageMS msgId: " + msgId);
                messageIds.add(msgId);
                //NotificationUtil.getInstance().CustomshowNotification(this,objects,from,objects.optString("type"),singletchat_boolean,secretchat,false);
                boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                if (is_gossip) {
                    if (Build.VERSION.SDK_INT >= 27) {

                        NotificationUtil.getInstance().CustomshowNotificationWithReply(this, objects, from, objects.optString("type"), singletchat_boolean, secretchat, false);
                    } else {
                        NotificationUtil.getInstance().CustomshowNotification(this, objects, from, objects.optString("type"), singletchat_boolean, secretchat, false);
                    }
                } else {
                    NotificationUtil.getInstance().CustomshowNotification(this, objects, from, objects.optString("type"), singletchat_boolean, secretchat, false);
                }

                //                String id = objects.getString("id");
                Log.d(TAG, "loadMessagefcm: 6");
                String convId = objects.optString("convId");
                String stat = "", pwd = "";
                String doc_id;
                if (objects.has("docId")) {
                    doc_id = objects.optString("docId");
                } else {
                    doc_id = objects.optString("doc_id");
                }
                String uniqueID = to + "-" + from;

                boolean isSecretMsg = false;

                item.setIsSelf(false);
                item.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_DELIVERED);

                String chatType = MessageFactory.CHAT_TYPE_SINGLE;
                if (secretType.equalsIgnoreCase("yes")) {
                    chatType = MessageFactory.CHAT_TYPE_SECRET;
                    uniqueID = uniqueID + "-" + MessageFactory.CHAT_TYPE_SECRET;
                    String timer = objects.getString("incognito_timer");
                    String timerCreatedBy = objects.getString("incognito_user");
                    item.setSecretTimer(timer);
                    item.setSecretTimeCreatedBy(timerCreatedBy);

                    //new dbsqlite
                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
                    contactDB_sqlite.updateSecretMessageTimer(to, timer, timerCreatedBy, item.getMessageId());
                }

                if (session.getarchivecount() != 0) {
                    if (session.getarchive(uniqueCurrentID + "-" + from))
                        session.removearchive(uniqueCurrentID + "-" + from);
                }

                MessageDbController db = CoreController.getDBInstance(this);

                db.updateChatMessage(item, chatType);
                AutoDownLoadUtils.getInstance().checkAndAutoDownload(this, item);
                changeBadgeCount(convId, item.getMessageId());

                if (!userInfoSession.hasChatConvId(uniqueID)) {
                    userInfoSession.updateChatConvId(uniqueID, from, convId);
                }
            }
            Log.d(TAG, "loadMessagefcm: 7");

            //-------------Delete Chat-----------------

            if (normal_Offline == 1) {
                String new_docId, new_msgId, new_type, new_recId, new_convId;
                try {
                    String fromId = objects.getString("from");

                    if (!fromId.equalsIgnoreCase(uniqueCurrentID)) {
                        String chat_id = (String) objects.get("docId");
                        String[] ids = chat_id.split("-");

                        new_type = objects.getString("chat_type");
//                            new_recId = objects.getString("recordId");
//                            new_convId = objects.getString("convId");
                        MessageDbController db = CoreController.getDBInstance(this);

                        if (new_type.equalsIgnoreCase("single")) {
                            new_docId = ids[1] + "-" + ids[0];
                            new_msgId = new_docId + "-" + ids[2];
                            db.deleteSingleMessage(new_docId, new_msgId, "single", "other");
                            db.deleteChatListPage(new_docId, new_msgId, "single", "other");
                        } else {
                            new_docId = ids[1] + "-g-" + ids[0];
                            new_msgId = uniqueCurrentID + "-g-" + ids[1] + "-g-" + ids[3];
                            String groupAndMsgId = ids[1] + "-g-" + ids[3];

                            db.deleteSingleMessage(groupAndMsgId, new_msgId, "group", "other");
                            db.deleteChatListPage(groupAndMsgId, new_msgId, "group", "other");
                        }


                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
        Log.d(TAG, "loadMessagefcm: 9");
/*        if(!MessageUtils.getInstance().isstarted)
            MessageUtils.getInstance().init(this);*/
    }

    private void changeBadgeCount(String convId, String msgId) {
        ShortcutBadgeManager shortcutBadgeMgnr = new ShortcutBadgeManager(this);
        shortcutBadgeMgnr.putMessageCount(convId, msgId);
        int totalCount = shortcutBadgeMgnr.getTotalCount();

        try {
            ShortcutBadger.applyCount(this, totalCount);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /* when your phone is locked screen wakeup method*/
    @SuppressLint("InvalidWakeLockTag")
    private void wakeUpScreen() {
        PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();

        Log.e("screen on......", "" + isScreenOn);
        KeyguardManager km = (KeyguardManager) mContext.getSystemService(KEYGUARD_SERVICE);
        final KeyguardManager.KeyguardLock kl = km.newKeyguardLock("IN");
        kl.disableKeyguard();
        if (isScreenOn == false) {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "MyLock");
            wl.acquire(10000);
//            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyCpuLock");
//            wl_cpu.acquire(10000);
        }
    }

    private void handleNotification(String message) {

    }

    private void playRingtone() {
        AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
        switch (am.getRingerMode()) {
            case AudioManager.RINGER_MODE_SILENT:

                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                long[] pattern = {0, 1000, 1500}; // start, vibrate duration, sleep duration
                vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, 0);
                break;
            case AudioManager.RINGER_MODE_NORMAL:
                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                    ringtone = RingtoneManager.getRingtone(mContext, notification);
                    ringtone.play();
                } catch (Exception e) {
                    MyLog.d("RingtoneException", e.toString());
                    MyLog.e(TAG, "", e);
                }
                break;
        }
    }

    private void handleDataMessage(JSONObject jsonObject) {
        Log.e("json call-------", jsonObject.toString());

        Log.e("json", jsonObject.toString());
        Intent intent = null;
        Bundle bundle = new Bundle();
        String from = "";
        String to = "";
        String callStatus = "";
        String Room_id = "";
        String recordId = "";
        String id = "";
        String type = "";
        boolean isVideoCall = false;
        String callId = "";
        String ts = "";
        String fromUserMsisdn = "";
        String clickAction = "";
        String title = "";
        String body = "";
        String colorCode = "";
        int numberHideStatus = 0;
        String hidednumber = "";
        playRingtone();
        try {
            from = jsonObject.getString("from_id");
            System.out.println("print from_id: " + from);
            to = jsonObject.getString("to");
            callStatus = jsonObject.getString("call_status");
            Room_id = jsonObject.getString("roomid");
            recordId = jsonObject.getString("recordId");
            id = jsonObject.getString("id");
            type = jsonObject.getString("type");
            ts = jsonObject.getString("timestamp");
            fromUserMsisdn = jsonObject.getString("ContactMsisdn");
            clickAction = jsonObject.getString("click_action");
            title = jsonObject.getString("title");
            body = jsonObject.getString("body");

            String name = getcontactname.getSendername(to, fromUserMsisdn);

            callId = to + "-" + from + "-" + id;

            if (to.equalsIgnoreCase(uniqueCurrentID) && callStatus.equals(MessageFactory.CALL_STATUS_CALLING + "")) {
                CallItemChat callItem = incomingMsg.loadIncomingCall(jsonObject);
                MessageDbController db = CoreController.getDBInstance(this);
                db.updateCallLogs(callItem);
                if (type.equals(MessageFactory.video_call + "")) {
                    isVideoCall = true;
                }

             /*   if (!AppUtils.isMyServiceRunning(this, MessageService.class)) {
                    MyLog.e("isMyServiceRunning", "MessageService started");

                    AppUtils.startService(this, MessageService.class);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        this.startForegroundService(new Intent(this, MessageService.class));
                    } else {
                        this.startService(new Intent(this, MessageService.class));
                    }
                }*/

                if (!CallsActivity.isStarted && !IncomingCallActivity.isStarted) {
                    intent = new Intent(this, IncomingCallActivity.class);
                    intent.putExtra(IncomingCallActivity.EXTRA_DOC_ID, callId);
                    intent.putExtra(IncomingCallActivity.EXTRA_FROM_USER_ID, from);
                    intent.putExtra(IncomingCallActivity.EXTRA_TO_USER_ID, to);
                    intent.putExtra(IncomingCallActivity.EXTRA_FROM_USER_MSISDN, fromUserMsisdn);
                    intent.putExtra(IncomingCallActivity.EXTRA_CALL_ROOM_ID, Room_id);
                    intent.putExtra(IncomingCallActivity.EXTRA_CALL_RECORD_ID, recordId);
                    intent.putExtra(IncomingCallActivity.EXTRA_CALL_TYPE, isVideoCall);
                    intent.putExtra(IncomingCallActivity.EXTRA_CALL_TIME_STAMP, ts);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addCategory(Intent.CATEGORY_LAUNCHER);
                    intent.setAction(Intent.ACTION_MAIN);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtras(bundle);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception:---->> " + e.getMessage());
        }

//        boolean isVideoCall = false;

//        if (clickAction.equals("IncomingCallActivity")){
        if (!CallsActivity.isStarted && !IncomingCallActivity.isStarted) {
            intent = new Intent(this, IncomingCallActivity.class);
            intent.putExtra(IncomingCallActivity.EXTRA_DOC_ID, callId);
            intent.putExtra(IncomingCallActivity.EXTRA_FROM_USER_ID, from);
            intent.putExtra(IncomingCallActivity.EXTRA_TO_USER_ID, to);
            intent.putExtra(IncomingCallActivity.EXTRA_FROM_USER_MSISDN, fromUserMsisdn);
            intent.putExtra(IncomingCallActivity.EXTRA_CALL_ROOM_ID, Room_id);
            intent.putExtra(IncomingCallActivity.EXTRA_CALL_RECORD_ID, recordId);
            intent.putExtra(IncomingCallActivity.EXTRA_CALL_TYPE, isVideoCall);
            intent.putExtra(IncomingCallActivity.EXTRA_CALL_TIME_STAMP, ts);

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.setAction(Intent.ACTION_MAIN);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            intent.putExtras(bundle);

               /* if (callItem.getOpponentUserId().equalsIgnoreCase(CallsActivity.opponentUserId)) {
                    sendIncomingCallBroadcast(intent);
                }
                type = "" + MessageFactory.audio_call;
                if (isVideoCall)
                    type = "" + MessageFactory.video_call;
                // Ack to call sender(reverse order)
                JSONObject ackObj = CallMessage.getCallStatusObject(to, callItem.getOpponentUserId(),
                        callItem.getId(), callItem.getCallId(), callItem.getRecordId(), MessageFactory.CALL_STATUS_ARRIVED, type);
                SendMessageEvent event = new SendMessageEvent();
                MyLog.e(TAG, "EVENT_CALL_STATUS" + ackObj);
                event.setEventName(SocketManager.EVENT_CALL_STATUS);
                event.setMessageObject(ackObj);
                EventBus.getDefault().post(event);

                sendCallAckToServer(jsonObject, callItem);*/
        }
        /*}else{
            intent = new Intent(this, ChatPageActivity.class);
            intent.putExtras(bundle);
        }*/

        NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(0);
        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
//        getApplicationContext().startActivity(intent);
        KeyguardManager km = (KeyguardManager) mContext.getSystemService(KEYGUARD_SERVICE);
        final KeyguardManager.KeyguardLock kl = km.newKeyguardLock("IN");
        kl.disableKeyguard();

        PowerManager pm = (PowerManager) mContext.getSystemService(POWER_SERVICE);
        @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.FULL_WAKE_LOCK, "SMOK Komunal");
        wl.acquire();

//        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + mContext.getPackageName() + "/" + R.raw.call_tone);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());

        CharSequence name = "Blink Mobility";
        String desc = "this is notific";
        int imp = NotificationManager.IMPORTANCE_HIGH;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            final String ChannelID = "my_channel_01";
            final int ncode = 101;
            final NotificationManager mNotific =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationChannel mChannel = new NotificationChannel(ChannelID, name,
                    imp);
            mChannel.setDescription(desc);
            mChannel.setLightColor(Color.CYAN);
            mChannel.canShowBadge();
            mChannel.setShowBadge(false);
            mChannel.setSound(sound, attributes);

            mNotific.createNotificationChannel(mChannel);

            @SuppressLint("WrongConstant") Notification n = new Notification.Builder(this, ChannelID)
                    .setContentTitle(title)
                    .setContentText(hidednumber + " calling you")
                    .setBadgeIconType(R.mipmap.app_icon_new)
                    .setColor(getResources().getColor(R.color.colorPrimary))
                    .setContentIntent(contentIntent)
                    .setSmallIcon(R.mipmap.app_icon_new)
                    .setAutoCancel(false)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setCategory(NotificationCompat.CATEGORY_CALL)
                    .setFullScreenIntent(contentIntent, true)
                    .setOngoing(true)
//                    .setAutoCancel(false)
//                    .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(), 0))
                    .build();
            n.flags = Notification.FLAG_NO_CLEAR;
            wl.release();
            mNotific.notify(ncode, n);
            startForeground(0, n);

        } else {
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Resources res = this.getResources();
            builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.mipmap.app_icon_new)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.app_icon_new))
                    .setColor(getResources().getColor(R.color.colorPrimary))
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setSound(soundUri)
                    .setContentTitle(title)
                    .setLights(0xffff0000, 100, 2000)
                    .setPriority(Notification.DEFAULT_SOUND)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setContentText(body)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setCategory(NotificationCompat.CATEGORY_CALL)
                    .setFullScreenIntent(contentIntent, true)
                    .setContentIntent(contentIntent);
            Notification n = builder.getNotification();
            n.defaults |= Notification.DEFAULT_ALL;
            wl.release();
            nm.notify(0, n);
        }
    }

    private void sendCallAckToServer(JSONObject callObj, CallItemChat callItem) {
        try {
            String callId;
            if (callObj.has("docId")) {
                callId = callObj.getString("docId");
            } else {
                callId = callObj.getString("doc_id");
            }
            SendMessageEvent messageEvent = new SendMessageEvent();
            messageEvent.setEventName(SocketManager.EVENT_CALL_ACK);
            CallAck ack = (CallAck) MessageFactory.getMessage(MessageFactory.call_ack, (this));
            messageEvent.setMessageObject((JSONObject) ack.getMessageObject(callItem.getOpponentUserId(),
                    callId, MessageFactory.DELIVERY_STATUS_DELIVERED, callItem.getId()));
            EventBus.getDefault().post(messageEvent);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void sendIncomingCallBroadcast(final Intent dataIntent) {
        Runnable incomCallBroadcastRunnable = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setAction(getPackageName() + ".incoming_call");
                intent.putExtras(dataIntent.getExtras());
                sendBroadcast(intent);
            }
        };
        incomCallBroadcastHandler.postDelayed(incomCallBroadcastRunnable, 4000);
    }

    private void showNotification(String aMessageStr, String aActionStr) {
//        Intent finish_PushNotificationAlert = new Intent();
    }
}