package com.app.Smarttys.video;

import android.os.AsyncTask;

import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.File;


/**
 * Created by user134 on 7/16/2018.
 */

public class VideoCompressor extends AsyncTask<Void, Void, VideoCompressPojo> {
    private static final String TAG = "VideoCompressor";
    private String videoPath;
    private FileUploadDownloadManager fileUploadDownloadManager;
    private EventBus eventBus;
    private JSONObject jsonObject;

    public VideoCompressor(EventBus eventBus, JSONObject object, FileUploadDownloadManager fileUploadDownloadManager, String videoPath) {
        this.fileUploadDownloadManager = fileUploadDownloadManager;
        this.videoPath = videoPath;
        this.jsonObject = object;
        this.eventBus = eventBus;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        MyLog.d(TAG, "Start video compression");
    }

    @Override
    protected VideoCompressPojo doInBackground(Void... voids) {
        File f = new File(Config.VIDEO_COMPRESSOR_COMPRESSED_VIDEOS_DIR);
        if (!f.exists())
            f.mkdirs();
        if (videoPath != null && !videoPath.isEmpty())
            return MediaController.getInstance().convertVideo(videoPath);
        return new VideoCompressPojo();
    }

    @Override
    protected void onPostExecute(VideoCompressPojo videoCompressPojo) {
        super.onPostExecute(videoCompressPojo);
        MyLog.d(TAG, "Compression post execute!");
        try {
            if (videoCompressPojo.isCompressed() && videoCompressPojo.getCompressedFilePath() != null) {
                MyLog.d(TAG, "onPostExecute: compress success");
                MyLog.e("CheckEventConnection", "Video Compress Success");
                jsonObject.put("CompressedPath", videoCompressPojo.getCompressedFilePath());
                //fileUploadDownloadManager.uploadCompressVideoFile(eventBus, jsonObject, new File(videoCompressPojo.getCompressedFilePath()));
            } else {
                MyLog.d(TAG, "onPostExecute: compress fail");
                MyLog.e("CheckEventConnection", "Video Compress Fail");
                if (videoCompressPojo.getOriginalFIlePath() != null && !videoCompressPojo.getOriginalFIlePath().isEmpty()) {
                    //fileUploadDownloadManager.uploadCompressVideoFile(eventBus, jsonObject, new File(videoCompressPojo.getOriginalFIlePath()));
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "onPostExecute: ", e);
            MyLog.e("CheckEventConnection", "Video Compress Fail");
            //fileUploadDownloadManager.uploadCompressVideoFile(eventBus, jsonObject, new File(videoCompressPojo.getOriginalFIlePath()));
        }

    }
}

