package com.app.Smarttys.video;

import com.app.Smarttys.core.message.MessageFactory;

/**
 * Created by user134 on 7/16/2018.
 */

public class Config {
    /*
     * Application folder for video files
     */
    public static final String VIDEO_COMPRESSOR_COMPRESSED_VIDEOS_DIR = MessageFactory.BASE_STORAGE_PATH + "/Compressed_Videos/";
}
