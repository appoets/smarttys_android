package com.app.Smarttys.core.uploadtoserver;

import com.app.Smarttys.app.utils.MyLog;

import org.json.JSONObject;

import java.util.Arrays;

public class FileUploadUtils {
    private static final String TAG = "FileUploadUtils";

    public static ByteSplit divideArray(byte[] source, JSONObject object, int uploadedSize) {
        MyLog.d(TAG, "FileTest divideArray source size: " + source.length);
        int mb = source.length / (1024 * 1024);
        ByteSplit byteSplit = new ByteSplit();


        MyLog.d(TAG, "FileTest divideArray uploadedSize: " + uploadedSize);

        byte[] balanceBytes = Arrays.copyOfRange(source, uploadedSize, source.length);

        int originalChunkSize = source.length / 10;
        int totalParts = 0;
        if (mb < 1) {
            totalParts = 5;
            originalChunkSize = source.length / totalParts;
        } else if (mb < 3) {
            totalParts = 10;
            originalChunkSize = source.length / totalParts;
        } else if (mb < 5) {
            totalParts = 20;
            originalChunkSize = source.length / totalParts;
        } else if (mb < 7) {
            totalParts = 30;
            originalChunkSize = source.length / totalParts;
        } else if (mb < 10) {
            totalParts = 40;
            originalChunkSize = source.length / totalParts;
        } else if (mb < 15) {
            totalParts = 50;
            originalChunkSize = source.length / totalParts;
        } else {
            totalParts = 50;
            originalChunkSize = source.length / totalParts;
        }

/*
    Dynamic speed calculation
        try {
            long startMillis;
            if(object.has("startTime")) {
                startMillis = object.getLong("startTime");
                long endMillis= System.currentTimeMillis();
                long timeTakenMillis = endMillis-startMillis;
                double seconds= timeTakenMillis *0.001;

                int kpBytesUploaded=(originalChunkSize/1024);
                double kbps= ( kpBytesUploaded/ seconds);
                Log.d(TAG, "divideArray:DynamicSpeed seconds "+seconds);
                Log.d(TAG, "divideArray: DynamicSpeed size: "+originalChunkSize);
                Log.d(TAG, "divideArray:DynamicSpeed kbps "+kbps);
            }
            object.put("startTime",System.currentTimeMillis());
        } catch (JSONException e) {
            e.printStackTrace();
        }*/


        if (isLastChunkFile(uploadedSize, totalParts, originalChunkSize, source.length)) {
            MyLog.d(TAG, "filetest@ divideArray: final item" + balanceBytes.length);
            byteSplit.isFinalByte = true;
            byteSplit.bytes = balanceBytes;
        } else {
            byteSplit.bytes = Arrays.copyOfRange(source, uploadedSize, uploadedSize + originalChunkSize);
            byteSplit.isFinalByte = false;
            MyLog.d(TAG, "filetest@ divideArray: byte length " + byteSplit.bytes.length);
        }


        MyLog.d(TAG, "FileTest divideArray end: ");


        return byteSplit;
    }


    private static boolean isLastChunkFile(int uploadedSize, int totalParts, int chunkSize, int sourceLength) {

        int balanceLastBytes = sourceLength - (chunkSize * totalParts);
        MyLog.d(TAG, "filetest divideArray balanceLastBytes: " + balanceLastBytes);
        if (balanceLastBytes > 0) {

            int totalBytes = uploadedSize + balanceLastBytes;

            MyLog.d(TAG, "filetest divideArray isLastChunkFile totalBytes if: " + totalBytes);
            if (totalBytes == sourceLength) {
                MyLog.d(TAG, "filetest isLastChunkFile: true1");
                return true;
            }
        } else {
            int totalbytes = uploadedSize + chunkSize;
            MyLog.d(TAG, "filetest divideArray isLastChunkFile totalBytes: " + totalbytes);
            if (totalbytes == sourceLength) {
                if (balanceLastBytes == 0) {
                    MyLog.d(TAG, "FileTest isLastChunkFile: true2");
                    return true;
                }
            }
        }
        return false;

    }
}
