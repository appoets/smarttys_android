package com.app.Smarttys.core.model;

import com.google.android.gms.drive.DriveId;

/**
 * Created by CAS60 on 5/8/2017.
 */
public class GDBackupPojo {

    String fileName, filePath;
    DriveId driveId;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public DriveId getDriveId() {
        return driveId;
    }

    public void setDriveId(DriveId driveId) {
        this.driveId = driveId;
    }
}
