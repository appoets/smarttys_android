package com.app.Smarttys.core.model;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 */
public class SmarttyContactModel implements Serializable {

    private String FirstName = "", Status = "", AvatarImageUrl = "", noInDevice = "null";
    private String _id, Msisdn = "";
    private String type;
    private boolean isSelected = false;
    private String countryCode;
    private String GroupDocID;
    private boolean isGroup = false;

    public String getGroupDocID() {
        return GroupDocID;
    }

    public void setGroupDocID(String groupDocID) {
        GroupDocID = groupDocID;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getNumberInDevice() {
        return noInDevice;
    }

    public void setNumberInDevice(String noInDevice) {
        this.noInDevice = noInDevice;
    }

    public String getAvatarImageUrl() {
        return AvatarImageUrl;
    }

    public void setAvatarImageUrl(String AvatarImageUrl) {
        this.AvatarImageUrl = AvatarImageUrl;
    }

    public String getMsisdn() {
        return Msisdn;
    }

    public void setMsisdn(String Msisdn) {
        this.Msisdn = Msisdn;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String country_code) {
        this.countryCode = country_code;
    }

    @Override
    public int hashCode() {
        return Objects.hash(_id, FirstName, AvatarImageUrl);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final SmarttyContactModel other = (SmarttyContactModel) obj;
        if (!Objects.equals(this._id, other._id)) return false;
        if (!Objects.equals(this.FirstName, other.FirstName)) return false;
        if (!Objects.equals(this.AvatarImageUrl, other.AvatarImageUrl)) return false;
        return true;
    }
}