package com.app.Smarttys.core;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.ChatPageActivity;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.smarttyhelperclass.SmarttyImageUtils;

import java.util.List;

/**
 * Created by CAS60 on 12/2/2016.
 */
public class ShortcutBadgeManager {
    private static final String TAG = "ShortcutBadgeManager";
    private final String SHORTCUT_BADGE_PREF = "ShorcutBadgePref";
    private final String KEY_BADGE_TOTAL_COUNT = "TotalCount";
    private final String KEY_SINGLE_MSG_COUNT = "SingleMsgCount";
    private final String KEY_GROUP_MSG_COUNT = "GroupMsgCount";
    private final String contactRefresh_time = "contactrefreshtime";
    private final String firsttimeSyncCompleted = "firsttime_sync_complete";
    private final String firsttimetimeloading = "firsttimetimeloading";
    private SharedPreferences badgePref;

    public ShortcutBadgeManager(Context context) {
        badgePref = context.getSharedPreferences(SHORTCUT_BADGE_PREF, Context.MODE_PRIVATE);
    }

    public static void addChatShortcut(Context context, boolean isGroupChat, String receiverId, String receiverName,
                                       String receiverAvatar, String receiverMsisdn, Bitmap avatarBmp) {
        Intent shortcutIntent = new Intent(context, ChatPageActivity.class);

        shortcutIntent.putExtra("backfrom", true);
        shortcutIntent.putExtra("receiverUid", "");
        shortcutIntent.putExtra("receiverName", "");
        shortcutIntent.putExtra("documentId", receiverId);
        shortcutIntent.putExtra("Username", receiverName);
        shortcutIntent.putExtra("Image", receiverAvatar);
        shortcutIntent.putExtra("msisdn", receiverMsisdn);
        shortcutIntent.putExtra("isLockChat", true);
        shortcutIntent.setAction(Intent.ACTION_MAIN);

        Intent addIntent = new Intent();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            Bitmap bitmap = null;
            if (avatarBmp != null || (receiverAvatar != null && !receiverAvatar.equals(""))) {
                if (avatarBmp != null) {
                    bitmap = Bitmap.createScaledBitmap(avatarBmp, 128, 128, true);
                    addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, SmarttyImageUtils.getCroppedBitmap(bitmap));
                } else {
                    if (!isGroupChat) {
                        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                                Intent.ShortcutIconResource.fromContext(context,
                                        R.mipmap.chat_attachment_profile_default_image_frame));
                    } else {
                        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                                Intent.ShortcutIconResource.fromContext(context,
                                        R.mipmap.group_chat_attachment_profile_icon));

                    }
                }
            } else {
                if (!isGroupChat) {
                    addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                            Intent.ShortcutIconResource.fromContext(context,
                                    R.mipmap.chat_attachment_profile_default_image_frame));
                } else {
                    addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                            Intent.ShortcutIconResource.fromContext(context,
                                    R.mipmap.group_chat_attachment_profile_icon));

                }
            }
            addIntent.putExtra("duplicate", false);


            addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, receiverName);
            addIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
            context.sendBroadcast(addIntent);

            addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            context.sendBroadcast(addIntent);

            Toast.makeText(context, "Shortcut created", Toast.LENGTH_SHORT).show();
        } else {
            try {
                postApi26CreateShortcut(receiverId, context, shortcutIntent, receiverName, avatarBmp, isGroupChat);
            } catch (Exception e) {
                Log.e(TAG, "addChatShortcut: ", e);
            }
        }
    }

    private static void postApi26CreateShortcut(String shortcutId, final Context context, Intent intent, String label, Bitmap bitmap, boolean isGroupChat) {
        if (Build.VERSION.SDK_INT >= 26) {
            ShortcutManager sm = context.getSystemService(ShortcutManager.class);
            if (sm != null && sm.isRequestPinShortcutSupported()) {
                boolean shortcutExists = false;
                // We create the shortcut multiple times if given the
                // opportunity.  If the shortcut exists, put up
                // a toast message and exit.
                List<ShortcutInfo> shortcuts = sm.getPinnedShortcuts();
                for (int i = 0; i < shortcuts.size() && !shortcutExists; i++)
                    shortcutExists = shortcuts.get(i).getId().equals(shortcutId);
                if (shortcutExists) {
                    Toast.makeText(context, "Shortcut already exists"
                            , Toast.LENGTH_LONG
                    ).show();
                } else {
                    // this intent is used to wake up the broadcast receiver.
                    // I couldn't get createShortcutResultIntent to work but
                    // just a simple intent as used for a normal broadcast
                    // intent works fine.
                    Intent broadcastIntent
                            = new Intent(shortcutId);
                    broadcastIntent.putExtra("msg", "approve");
                    // wait up to N seconds for user input, then continue
                    // on assuming user's choice was deny.
                    // create an anonymous broadcaster.  Unregister when done.
                    context.registerReceiver(new BroadcastReceiver() {
                                                 @Override
                                                 public void onReceive(Context c, Intent intent) {
                                                     @SuppressWarnings("unused")
                                                     String msg = intent.getStringExtra("msg");
                                                     if (msg == null) msg = "NULL";
                                                     context.unregisterReceiver(this);
                                                     Log.d(TAG, String.format(
                                                             "ShortcutReceiver activity = \"$1%s\" : msg = %s"
                                                             , intent.getAction()
                                                             , msg)
                                                     );

                                                 }
                                             }
                            , new IntentFilter(shortcutId)
                    );

                    // this is the intent that actually creates the shortcut.

                    intent.setAction(shortcutId);
                    ShortcutInfo shortcutInfo;
                    if (bitmap != null) {
                        shortcutInfo = new ShortcutInfo
                                .Builder(context, shortcutId)
                                .setShortLabel(label)
                                .setIcon(Icon.createWithBitmap(bitmap))
                                .setIntent(intent)
                                .build();
                    } else {
                        Icon icon;
                        if (isGroupChat) {
                            icon = Icon.createWithResource(context, R.mipmap.group_chat_attachment_profile_icon);
                        } else {
                            icon = Icon.createWithResource(context, R.mipmap.chat_attachment_profile_default_image_frame);
                        }
                        shortcutInfo = new ShortcutInfo
                                .Builder(context, shortcutId)
                                .setShortLabel(label)
                                .setIcon(icon)
                                .setIntent(intent)
                                .build();
                    }
                    PendingIntent successCallback = PendingIntent.getBroadcast(
                            context, 99
                            , broadcastIntent, 0);
                    // Shortcut gets created here.
                    sm.requestPinShortcut(shortcutInfo
                            , successCallback.getIntentSender());
                }
            }
        }
    }

    public void putMessageCount(String convId, String msgId) {
        String countKey = convId;
        int totCount = getTotalCount() + 1;
        int count = getSingleBadgeCount(countKey) + 1;

        SharedPreferences.Editor badgeEditor = badgePref.edit();
        badgeEditor.putInt(countKey, count);
        badgeEditor.putInt(KEY_BADGE_TOTAL_COUNT, totCount);

        if (countKey.contains("-g")) {  // For adding group message count
            int groupMsgCount = getAllGroupMsgCount() + 1;
            badgeEditor.putInt(KEY_GROUP_MSG_COUNT, groupMsgCount);
        } else {
            int singleMsgCount = getAllSingleMsgCount() + 1;
            badgeEditor.putInt(KEY_SINGLE_MSG_COUNT, singleMsgCount);
        }

        badgeEditor.apply();

    }

    public void setfileuploadingprogress(int value, String msgId) {
        SharedPreferences.Editor badgeEditor = badgePref.edit();
        badgeEditor.putInt(msgId, value);
        badgeEditor.apply();
    }

    public int getfileuploadingprogress(String msgId) {
        int time = 0;
        try {
            time = badgePref.getInt(msgId, 0);
        } catch (Exception e) {

        }

        return time;
    }

    public void removeMessageCount(String docId, String msgId) {
        String countKey = docId;
        int totCount = getTotalCount();
        int singleDocIdCount = getSingleBadgeCount(countKey);
        totCount = totCount - singleDocIdCount;

        String keyAllMsgCount;
        int allMsgCount;
        if (countKey.contains("-g")) {
            keyAllMsgCount = KEY_GROUP_MSG_COUNT;
            allMsgCount = getAllGroupMsgCount() - singleDocIdCount;
        } else {
            keyAllMsgCount = KEY_SINGLE_MSG_COUNT;
            allMsgCount = getAllSingleMsgCount() - singleDocIdCount;
        }

        SharedPreferences.Editor badgeEditor = badgePref.edit();
        badgeEditor.putInt(countKey, 0);
        badgeEditor.putInt(keyAllMsgCount, allMsgCount);
        badgeEditor.putInt(KEY_BADGE_TOTAL_COUNT, totCount);
        badgeEditor.apply();
    }

    public int getTotalCount() {
        int totalCount = badgePref.getInt(KEY_BADGE_TOTAL_COUNT, 0);
        return totalCount;
    }

    public int getSingleBadgeCount(String countKey) {
        int count = badgePref.getInt(countKey, 0);
        return count;
    }

    public int getAllGroupMsgCount() {
        int count = badgePref.getInt(KEY_GROUP_MSG_COUNT, 0);
        return count;
    }

    public int getAllSingleMsgCount() {
        int count = badgePref.getInt(KEY_SINGLE_MSG_COUNT, 0);
        return count;
    }

    public void clearBadgeCount() {
        SharedPreferences.Editor badgeEditor = badgePref.edit();
        badgeEditor.clear();
        badgeEditor.apply();
    }

    public void setContactLastRefreshTime(long value) {
        MyLog.d(TAG, ">>>setContactLastRefreshTime: " + value);
        SharedPreferences.Editor badgeEditor = badgePref.edit();
        badgeEditor.putLong(contactRefresh_time, value);
        badgeEditor.apply();
    }

    public long getlastContact_refreshTime() {
        long time = badgePref.getLong(contactRefresh_time, 0);
        MyLog.d(TAG, "getlastContact_refreshTime: " + time);
        return time;
    }

    public void setfirsttimecontactSyncCompleted(boolean value) {
        SharedPreferences.Editor badgeEditor = badgePref.edit();
        badgeEditor.putBoolean(firsttimeSyncCompleted, value);
        badgeEditor.apply();
    }

    public boolean getfirstTimecontactSyncCompleted() {
        boolean time = badgePref.getBoolean(firsttimeSyncCompleted, false);
        return time;
    }

    public void setfirsttimeloading_newlogin(boolean value) {
        SharedPreferences.Editor badgeEditor = badgePref.edit();
        badgeEditor.putBoolean(firsttimetimeloading, value);
        badgeEditor.apply();
    }

    public boolean getfirsttimeloading_newlogin() {
        boolean time = badgePref.getBoolean(firsttimetimeloading, false);
        return time;
    }

}
