package com.app.Smarttys.core.model;

/**
 * Created by CAS60 on 2/1/2017.
 */
public class AudioFilePojo {

    private String fileName, filePath, duration;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
