package com.app.Smarttys.core.socket;

import android.content.Context;

import com.app.Smarttys.app.utils.MyLog;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Casperon Technologyon 02/04/16.
 */

public abstract class SocketResponseHandler {
    private Context context;

    public SocketResponseHandler(Context ctx) {
        this.context = ctx;
    }

    public abstract void execute(String event, JSONObject jsonObject) throws JSONException;

    public void handleJSONObjectResponse(String event, JSONObject jsonObject) throws Exception {
        MyLog.d("app controller 7", "");
        execute(event, jsonObject);

    }
}