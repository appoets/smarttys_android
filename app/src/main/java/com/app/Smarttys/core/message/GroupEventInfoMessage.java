package com.app.Smarttys.core.message;

import android.content.Context;

import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.model.MessageItemChat;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 10/27/2016.
 */
public class GroupEventInfoMessage extends BaseMessage implements Message {

    private static final String TAG = "GroupEventInfoMessage";
    private Context context;

    public GroupEventInfoMessage(Context context) {
        super(context);
        this.context = context;
    }


    @Override
    public Object getMessageObject(String to, String payload, Boolean isSecretchat) {
        this.to = to;
        setId(from + "-" + to);
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("type", MessageFactory.group_event_info);
            object.put("payload", payload);
            object.put("id", tsForServerEpoch);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return object;
    }

    @Override
    public Object getGroupMessageObject(String to, String payload, String groupName) {
        this.to = to;
        setId(from + "-" + to + "-g");
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("type", MessageFactory.group_event_info);
            object.put("payload", payload);
            object.put("id", tsForServerEpoch);
            object.put("convId", to);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return object;
    }

    public MessageItemChat createMessageItem(int eventType, boolean isSelf, String message, String status, String receiverUid,
                                             String groupName, String createdById, String createdToId) {
        item = new MessageItemChat();
        item.setMessageId(getId() + "-" + tsForServerEpoch);
        item.setIsSelf(isSelf);
        item.setStarredStatus(MessageFactory.MESSAGE_UN_STARRED);
        item.setTextMessage(message);
        item.setDeliveryStatus(status);
        item.setReceiverUid(receiverUid);
        item.setReceiverID(to);
        item.setMessageType("" + type);
        item.setGroupEventType("" + eventType);
        item.setSenderName(groupName);
        item.setGroupName(groupName);
        item.setTS(getShortTimeFormat());
        item.setIsInfoMsg(true);
        item.setCreatedByUserId(createdById);
        item.setCreatedToUserId(createdToId);
        item.setReceiverID(receiverUid);

        return item;
    }

}

