package com.app.Smarttys.core.message;

import android.content.Context;

import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 */
public class ChangeSetController {
    private static final String TAG = "ChangeSetController";
    static Context context;

    public ChangeSetController(Context context) {
        ChangeSetController.context = context;
    }

    public static void setChangeStatus(String status) {

        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_CHANGE_ST);
        JSONObject obj = new JSONObject();
        try {
            obj.put("from", SessionManager.getInstance(context).getCurrentUserID());
            obj.put("status", status);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        MyLog.e(TAG, "setChangeStatus" + obj);
        messageEvent.setMessageObject(obj);
        EventBus.getDefault().post(messageEvent);
    }

}
