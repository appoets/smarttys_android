package com.app.Smarttys.core.model;

/**
 * Created by CAS63 on 4/5/2017.
 */
public class ContactsPojo {
    String number, name, type;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
