package com.app.Smarttys.core.socket;

import com.app.Smarttys.core.model.SendMessageEvent;

public interface EventCallBack {
    void onMessageEvent(SendMessageEvent event);
}
