package com.app.Smarttys.core.service;

import android.app.DownloadManager;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.Smarttys.R;
import com.app.Smarttys.core.SessionManager;

import java.util.Objects;

public class DownloadService extends IntentService {
    private static final String DOWNLOAD_PATH = "download_path";
    private static final String DESTINATION_PATH = "destination_path";

    //private static final String DESTINATION_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    public DownloadService() {
        super("DownloadService");
    }

    public static Intent getDownloadService(final @NonNull Context callingClassContext, final @NonNull String downloadPath) {
        return new Intent(callingClassContext, DownloadService.class)
                .putExtra(DOWNLOAD_PATH, downloadPath);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String serverURL = null;
        String destinationPath = null;
        if (intent != null) {
            serverURL = intent.getStringExtra(DOWNLOAD_PATH);
            destinationPath = intent.getStringExtra(DESTINATION_PATH);
        }
        startDownload(serverURL, destinationPath);

    }

    private void startDownload(String serverPath, String directory) {
        Uri uri = Uri.parse(serverPath); // Path where you want to download file.
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);  // Tell on which network you want to download file.
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);  // This will show notification on top when downloading the file.
        request.setTitle(getString(R.string.app_name) + " file download"); // Title for notification.
        request.setVisibleInDownloadsUi(true);
        request.addRequestHeader("authorization", SessionManager.getInstance(this).getSecurityToken());
        request.addRequestHeader("requesttype", "site");
        request.addRequestHeader("userid", SessionManager.getInstance(this).getCurrentUserID());
        request.addRequestHeader("referer", serverPath);
        request.setDestinationInExternalPublicDir(directory, uri.getLastPathSegment());  // Storage directory path
        DownloadManager downloadManager = ((DownloadManager) Objects.requireNonNull(getSystemService(Context.DOWNLOAD_SERVICE)));
        downloadManager.enqueue(request); // This will start downloading

    }
}