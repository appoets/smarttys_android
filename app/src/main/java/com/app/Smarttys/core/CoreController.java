package com.app.Smarttys.core;

import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.app.Smarttys.ConnectivityReceiver;
import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.ContactRefresh;
import com.app.Smarttys.app.activity.GPSTracker;
import com.app.Smarttys.app.activity.NewHomeScreenActivty;
import com.app.Smarttys.app.activity.SmarttyContactsService;
import com.app.Smarttys.app.model.NewGroup_DB;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.Dialer;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.SharedPreference;
import com.app.Smarttys.core.connectivity.InternetConnectionReceiver;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.ChangeSetController;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.service.Constants;
import com.app.Smarttys.core.service.ContactChangeObserver;
import com.app.Smarttys.core.smarttyhelperclass.SmarttyFontUtils;
import com.app.Smarttys.core.socket.EventCallBack;
import com.app.Smarttys.core.socket.MessageService;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.core.uploadtoserver.FetchDownloadManager;
import com.app.Smarttys.eventnotejob.DemoJobCreator;
import com.app.Smarttys.receiverHelper.DynamicReceiver;
import com.app.Smarttys.status.controller.SharedPrefUtil;
import com.app.Smarttys.status.model.StatusDB;
import com.evernote.android.job.JobManager;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import org.appspot.apprtc.WebrtcConstants;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;
import java.util.Locale;

//import io.fabric.sdk.android.Fabric;

public class CoreController extends MultiDexApplication {
    // private RxBus bus = null;

    public static final String TAG = CoreController.class.getSimpleName();
    public static MessageDbController mMessageDbInstance;
    public static StatusDB mStatusDB;
    public static NewGroup_DB mNewGroup_db;
    public static ContactDB_Sqlite contactDB_sqliteInstance;
    public static Context mcontext;
    public static String mAppName;
    public static boolean isRaad;
    public static boolean isObbip;
    public static boolean isNobelNet;
    public static volatile Handler applicationHandler = null;
    public static GPSTracker gpsTracker;
    private static CoreController mInstance;
    private static EventCallBack mEventCallBack;
    private static ContactChangeObserver contentObserver = new ContactChangeObserver(new Handler());
    private static ContentResolver contentResolver;
    com.app.Smarttys.ConnectivityReceiver mR;
    BroadcastReceiver mybroadcast = new BroadcastReceiver() {

        //When Event is published, onReceive method is called
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            Log.i("[BroadcastReceiver]", "MyReceiver");

            try {
                if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                    Log.i("[BroadcastReceiver]", "Screen ON");
                } else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                    Log.i("[BroadcastReceiver]", "Screen OFF");
                    SessionManager.getInstance(mcontext).setIsDeviceLocked(true);
                }
            } catch (Exception e) {
                MyLog.e(TAG, "onReceive: ", e);
            }

        }
    };
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private Typeface avnNextLTProRegTypeface, avnNextLTProDemiTypeface;
    private Typeface robotoRegularTypeFace;
    private Typeface whatsappRegularTypeFace, whatsappBoldTypeFace;
    private SmarttyContactsService smarttyContactsService;
    private BroadcastReceiver receiver;
    private BroadcastReceiver myReceiverLocale = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String language;
            PackageManager packageManager = context.getPackageManager();
            Resources resources;
            try {
                resources = packageManager.getResourcesForApplication("android");
                language = resources.getConfiguration().locale.getLanguage();

                SharedPreference.getInstance().save(context, "lan", language);
                SharedPreference.getInstance().save(context, "lancontry", resources.getConfiguration().locale.getCountry());

                String mLanguagecode = SharedPreference.getInstance().getValue(context, "lan");
                CheckLanguage(mLanguagecode, resources.getConfiguration().locale.getCountry());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public static void logout(Context context) {
        try {
            if (context != null) {
                Toast.makeText(context, "You login in another device", Toast.LENGTH_SHORT).show();
                MessageDbController db = CoreController.getDBInstance(context);
                db.deleteDatabase();

                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
                contactDB_sqlite.deleteDatabase();

                notifyLogoutToServer();
                ChangeSetController.setChangeStatus("0");
                SessionManager.getInstance(context).logoutUser(false);
            }
        } catch (Exception e) {
            Log.e(TAG, "logout: ", e);
        }
    }

    private static void notifyLogoutToServer() {
        try {
            String userId = SessionManager.getInstance(mcontext).getCurrentUserID();
            JSONObject logoutObj = new JSONObject();
            logoutObj.put("from", userId);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_MOBILE_TO_WEB_LOGOUT);
            event.setMessageObject(logoutObj);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    public static void registerContactObserver() {
        if (ActivityCompat.checkSelfPermission(mcontext,
                Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED) {
            contentResolver.registerContentObserver(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, true, contentObserver);
        }
    }

    public static void setCallback(EventCallBack eventCallBack) {
        mEventCallBack = eventCallBack;
    }

    public static String getAppName() {
        if (mAppName == null || mAppName.isEmpty())
            return "Smarttys";
        return mAppName.replace(" ", "");
    }

    synchronized public static ContactDB_Sqlite getContactSqliteDBintstance(Context context) {
        if (contactDB_sqliteInstance == null) {
            contactDB_sqliteInstance = new ContactDB_Sqlite(context);
        }
        return contactDB_sqliteInstance;
    }

    public static synchronized CoreController getInstance() {
        return mInstance;
    }

    public static synchronized MessageDbController getDBInstance(Context context) {
        if (mMessageDbInstance == null) {
            mMessageDbInstance = new MessageDbController(context);
        }
        return mMessageDbInstance;
    }

    public static synchronized StatusDB getStatusDB(Context context) {
        if (mStatusDB == null) {
            mStatusDB = new StatusDB(context);
        }
        return mStatusDB;
    }

    public static synchronized NewGroup_DB getmNewGroup_db(Context context) {
        if (mNewGroup_db == null) {
            mNewGroup_db = new NewGroup_DB(context);
        }
        return mNewGroup_db;
    }

    public static void getLocation() {
        gpsTracker = new GPSTracker(mcontext);
        gpsTracker.getLocation();
    }

    public static void setDBInstance(Context context) {
        mMessageDbInstance = new MessageDbController(context);
    }

    public static String getBaseFilePath() {
        if (CoreController.isRaad)
            return mcontext.getFilesDir().getPath();
        else
            return Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + CoreController.getAppName() + "";
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "AppponCreate:");
//        Fabric.with(this, new Crashlytics());
        mInstance = this;
        mcontext = this;
        mAppName = getString(R.string.app_name);


        mR = new com.app.Smarttys.ConnectivityReceiver();
        receiver = DynamicReceiver.with(mR).register(this);
        //Check activity is running or not
        SharedPreference.getInstance().saveBool(mcontext, "callongoing", false);
        //   bus = new RxBus();
        //  getRxBusSingleton();
        boolean isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
        if (isMassChat) {
            SessionManager sessionManager = SessionManager.getInstance(mcontext);
            sessionManager.setIsAppSettingsReceived(false);
        }
        isRaad = getResources().getBoolean(R.bool.is_raad);
        isObbip = getResources().getBoolean(R.bool.is_obbip);
        isNobelNet = getResources().getBoolean(R.bool.is_nobelnet);
        contentResolver = getContentResolver();
        initContactChangeListener();
        hideAllFiles();
        initReceivers();
        startContactService();
        initPicasso();
        JobManager.create(this).addJobCreator(new DemoJobCreator());
        setScreenOffReceiver();
        initDB();
        FetchDownloadManager.getInstance().init(this);
        registerDialReceiver();
        if (isRaad)
            getLocation();
        WebrtcConstants.OWN_TURN_SERVER = Constants.BASE_IP;

        smarttyContactsService = new SmarttyContactsService();
        smarttyContactsService.init(this);
        //AppUtils.dbCursorFix();


    }

    public boolean isRunning(Context ctx) {
        ActivityManager activityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (ctx.getPackageName().equalsIgnoreCase(task.baseActivity.getPackageName()))
                return true;
        }

        return false;
    }

    private void initContactChangeListener() {
        contentObserver.init(this);
        registerContactObserver();
    }

    private void initPicasso() {
        final Picasso picasso = new Picasso.Builder(this)
                .downloader(new OkHttp3Downloader(AppUtils.getPicassoHeader(this)))
                .build();
        Picasso.setSingletonInstance(picasso);
    }

    private void initDB() {
        SessionManager sessionmgr = SessionManager.getInstance(mcontext);
        SharedPrefUtil.init(getApplicationContext());
        if (sessionmgr.isLoginKeySent()) {
            mMessageDbInstance = new MessageDbController(mcontext);
            contactDB_sqliteInstance = new ContactDB_Sqlite(mcontext);

        }
        mStatusDB = new StatusDB(mcontext);

        mNewGroup_db = new NewGroup_DB(mcontext);
        applicationHandler = new Handler(getInstance().getMainLooper());

    }

    private void startContactService() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED) {
            SmarttyContactsService.bindContactService(this, true);
        }
        AppUtils.startService(this, MessageService.class);
    }

    private void initReceivers() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(new InternetConnectionReceiver(), new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
        IntentFilter filter = new IntentFilter(Intent.ACTION_LOCALE_CHANGED);
        registerReceiver(myReceiverLocale, filter);
        if (EventBus.getDefault().isRegistered(this)) {
            Log.d(TAG, "EventBus_registered:true ");
        } else {
            Log.d(TAG, "EventBus_registered: false");
        }
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final SendMessageEvent event) {
        if (AppUtils.isServiceRunning(this, MessageService.class) && mEventCallBack != null)
            mEventCallBack.onMessageEvent(event);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        EventBus.getDefault().unregister(this);
    }

    private void hideAllFiles() {
        if (Constants.HIDE_FILE) {
            try {
                MyLog.d(TAG, "onCreate: hidenfile");
                File internalFile = getDir(CoreController.getAppName(), MODE_PRIVATE);
                if (!internalFile.exists()) {
                    internalFile.mkdirs();
                }
                MessageFactory.BASE_STORAGE_PATH = internalFile.getPath();
                MyLog.d(TAG, "onCreate: " + MessageFactory.BASE_STORAGE_PATH);
            } catch (Exception e) {
                MyLog.e(TAG, "onCreate hidenfile: ", e);
            }
        }
    }

    private void setScreenOffReceiver() {
        try {

            registerReceiver(mybroadcast, new IntentFilter(Intent.ACTION_SCREEN_OFF));
            MyLog.d(TAG, "onCreate: ");
        } catch (Exception e) {
            MyLog.e(TAG, "onCreate: ", e);
        }
    }

    public void CheckLanguage(String mLanguagecode, String mCountrycode) {
        Locale myLocale = new Locale(mLanguagecode, mCountrycode);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    public Typeface getAvnNextLTProRegularTypeface() {
        if (avnNextLTProRegTypeface == null) {
            //  avnNextLTProRegTypeface = CoreController.getInstance().getAvnNextLTProRegularTypeface();
            avnNextLTProRegTypeface = Typeface.createFromAsset(getAssets(), SmarttyFontUtils.getWhatsappFontStyle());
        }
        return avnNextLTProRegTypeface;
    }

    public Typeface getAvnNextLTProBoldTypeface() {
        if (avnNextLTProRegTypeface == null) {
            avnNextLTProRegTypeface = Typeface.createFromAsset(getAssets(), SmarttyFontUtils.getWhatsappFontStyle());
        }
        return avnNextLTProRegTypeface;
    }

    public Typeface getAvnNextLTProDemiTypeface() {
        if (avnNextLTProDemiTypeface == null) {
            avnNextLTProDemiTypeface = Typeface.createFromAsset(getAssets(), SmarttyFontUtils.getWhatsappBoldFontStyle());
        }
        return avnNextLTProDemiTypeface;
    }

    public Typeface getRobotoRegularTypeFace() {
        if (robotoRegularTypeFace == null) {
            robotoRegularTypeFace = Typeface.createFromAsset(getAssets(), SmarttyFontUtils.getWhatsappFontStyle());
        }
        return robotoRegularTypeFace;
    }

    public Typeface getWhatsappRegularTypeFace() {
        if (whatsappRegularTypeFace == null) {
            whatsappRegularTypeFace = Typeface.createFromAsset(getAssets(), SmarttyFontUtils.getWhatsappFontStyle());
        }
        return whatsappRegularTypeFace;
    }

    public Typeface getWhatsappBoldTypeFace() {
        if (whatsappBoldTypeFace == null) {
            whatsappBoldTypeFace = Typeface.createFromAsset(getAssets(), SmarttyFontUtils.getWhatsappBoldFontStyle());
        }
        return whatsappBoldTypeFace;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        MultiDex.install(this);
        AppUtils.fixFor10SecMemoryException();
    }

    public void freeMemory() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    private void registerDialReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SECRET_CODE");
        filter.addDataScheme("android_secret_code");
        filter.addDataAuthority("100", null);
        registerReceiver(new Dialer() {
            @Override
            public void onReceive(Context context, Intent intent) {
                super.onReceive(context, intent);
                Log.d(TAG, "onReceive: ");
                Intent intent1 = new Intent(context, NewHomeScreenActivty.class);
                context.startActivity(intent1);
            }
        }, filter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final ContactRefresh event) {
        if (smarttyContactsService != null)
            smarttyContactsService.onMessageEvent(event, this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final ReceviceMessageEvent event) {
        if (smarttyContactsService != null)
            smarttyContactsService.onMessageEvent(event, this);
    }

    public void setConnectivityListener(com.app.Smarttys.ConnectivityReceiver.ConnectivityReceiverListener listener) {

        com.app.Smarttys.ConnectivityReceiver.connectivityReceiverListener = listener;

    }

    public void unsetConnectivityListener() {

        ConnectivityReceiver.connectivityReceiverListener = null;

    }
}
