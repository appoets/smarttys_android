package com.app.Smarttys.core.message;

import android.content.Context;
import android.os.Environment;

import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.GroupInfoSession;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.SharedPreference;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.model.CallItemChat;
import com.app.Smarttys.core.model.GroupInfoPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.socket.MessageService;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;
import com.app.Smarttys.status.model.StatusModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by CAS60 on 2/27/2017.
 */

public class IncomingMessage {
    private static final String TAG = "IncomingMessage";
    private Context mContext;

    private String mCurrentUserId;
    private Session session;
    private Getcontactname getcontactname;
    private MessageService msgSvcCallback;


    public IncomingMessage(Context context) {
        this.mContext = context;
        session = new Session(mContext);
        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        getcontactname = new Getcontactname(mContext);
    }

    public MessageItemChat loadSingleStatusMessage(StatusModel objects) {
        MessageItemChat item = new MessageItemChat();
        String duration = "";


        MyLog.d("loadSingleStatusMessage", objects.toString());
        try {

            String payLoad = objects.getTextstatus_caption();
            String id = objects.getId();
            String from = objects.getUserId();
            item.setStatusReply(false);


            if ("0".equalsIgnoreCase(MessageFactory.text + "")) {
                long messagelength = session.getreceviedmessagelength() + payLoad.length();
                session.putreceivemessagelength(messagelength);
                int receviedmesagecount = session.getreceviedmessagecount();
                receviedmesagecount = receviedmesagecount + 1;
                session.putreceivemessagecount(receviedmesagecount);
                //System.out.println("receviedmesagecount" + receviedmesagecount);
            }


           /*     String name = objects.getString("Name");
                String dataSize = objects.getString("filesize");
                String convId = objects.getString("convId");
                String recordId = objects.getString("recordId");
//            String starredStatus = objects.getString("isStar");
                String ts = objects.getString("timestamp");
*/
              /*  JSONObject linkObj;
                try {
                    linkObj = objects.getJSONObject("link_details");
                } catch (JSONException e) {
                    linkObj = new JSONObject();
                }*/
//            JSONObject linkObj = objects.getJSONObject("link_details");

              /*  String webLink = "";
                if (linkObj.has("url")) {
                    webLink = linkObj.getString("url");
                }

                String webLinkTitle = "";
                if (linkObj.has("title")) {
                    webLinkTitle = linkObj.getString("title");
                }

                String webLinkDesc = "";
                if (linkObj.has("description")) {
                    webLinkDesc = linkObj.getString("description");
                }

                String webLinkImgUrl = "";
                if (linkObj.has("image")) {
                    webLinkImgUrl = linkObj.getString("image");
                }

                String webLinkImgThumb = "";
                if (linkObj.has("thumbnail_data")) {
                    webLinkImgThumb = linkObj.getString("thumbnail_data");
                }
*/
                /*try {
                    if (objects.has("ContactMsisdn")) {
                        String senderMsisdn = objects.getString("ContactMsisdn");
                        String phContactName = senderMsisdn;
                        phContactName = getcontactname.getSendername(from, senderMsisdn);
                        item.setSenderName(phContactName);
                        item.setSenderMsisdn(senderMsisdn);
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }*/

            item.setIsSelf(false);
            item.setTS(String.valueOf(objects.getTimeUploaded()));
            //  item.setConvId(convId);
            //  item.setRecordId(recordId);
            item.setStarredStatus(MessageFactory.MESSAGE_UN_STARRED);

            item.setMessageType("0");
            item.sethasNewMessage(true);
            if (payLoad != null && payLoad.length() > 0)
                item.setTextMessage(payLoad);

            item.setCount(1);

            if ("0".equalsIgnoreCase("" + MessageFactory.text)) {
                item.setTextMessage(payLoad);
            }

            String docId = mCurrentUserId + "-" + from;
            item.setMessageId(docId + "-" + id);
            item.setReceiverID(from);
            item.setFileBufferAt(0);
            item.setUploadDownloadProgress(0);
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_NOT_START);


        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

        return item;
    }


    public MessageItemChat loadGroupMessage(JSONObject objects) {
        try {
            MyLog.d(TAG, "loadGroupMessage" + objects.toString());

            String to = null;
            Integer type = Integer.parseInt(objects.getString("type"));
            String id = objects.getString("id");
            String from = objects.getString("from");
            if (objects.has("to")) {
                to = objects.getString("to");
            }

            //      String to = objects.getString("to");
/*
            String toDocId = (String) objects.get("toDocId");
            String[] splitDocId = toDocId.split("-");
            String msgId = splitDocId[3];
          */
            if (objects.has("to")) {
                to = objects.getString("to");
            }
            String toDocId = (String) objects.get("toDocId");
            String[] splitDocId = toDocId.split("-");
            String msgId = splitDocId[3];
            if (!objects.has("to")) {
                to = splitDocId[1];
            }
            String name = objects.getString("msisdn");
//            String convId = objects.getString("convId");
            String recordId = objects.getString("recordId");
            String starredStatus = objects.getString("isStar");
            String ts = objects.getString("timestamp");
            if (session.getarchivecountgroup() != 0) {
                if (session.getarchivegroup(mCurrentUserId + "-" + to + "-g"))
                    session.removearchivegroup(mCurrentUserId + "-" + to + "-g");
            }
            MessageItemChat item = new MessageItemChat();
//            item.setConvId(convId);
            item.setRecordId(recordId);
            item.set_id(id);
            item.setStarredStatus(starredStatus);
            item.setTS(ts);
            item.setGroupMsgFrom(from);
            item.setSenderName(name);
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_NOT_START);
            if (objects.has("payload")) {
                String payLoad = objects.getString("payload");
                item.setTextMessage(payLoad);
            }
            if (type == MessageFactory.picture || type == MessageFactory.audio || type == MessageFactory.video
                    || type == MessageFactory.group_document_message) {
                long filesize = AppUtils.parseLongForFile(objects.getString("filesize"));
                filesize = session.getreceviedmedialength() + filesize;
                session.putreceivemedialength(filesize);
            }
            if (objects.has("replyDetails")) {
                JSONObject aReplyObject = objects.getJSONObject("replyDetails");
                if (aReplyObject.has("message")) {
                    item.setReplyMsisdn(aReplyObject.getString("From_msisdn"));
                    item.setReplyFrom(aReplyObject.getString("from"));
                    String userID = aReplyObject.getString("from");
                    String replyname = aReplyObject.getString("From_msisdn");
                    item.setReplyType(aReplyObject.getString("type"));
                    if (aReplyObject.has("message")) {
                        item.setReplyMessage(aReplyObject.getString("message"));
                    }
                    if (aReplyObject.has("thumbnail_data")) {
                        String thumbnail_data = aReplyObject.getString("thumbnail_data");
                        item.setreplyimagebase64(thumbnail_data);
                    }
                    item.setReplyId(aReplyObject.getString("_id"));

                    replyname = getcontactname.getSendername(userID, replyname);

                    if (item.getReplyFrom().equalsIgnoreCase(mCurrentUserId)) {
                        item.setReplySender("you");
                    } else {
                        item.setReplySender(replyname);
                    }
                    if (aReplyObject.has("link_details")) {
                        try {
                            JSONObject replyLinkObj = aReplyObject.getJSONObject("link_details");
                            String replyLocTitle = replyLinkObj.getString("title");
                            String replyLocThumbData = replyLinkObj.getString("thumbnail_data");

                            item.setReplyMessage(replyLocTitle);
                            item.setreplyimagebase64(replyLocThumbData);
                        } catch (JSONException e) {
                            MyLog.e(TAG, "", e);
                        }
                    }
                }

            }
            item.setMessageType("" + type);
            String strType = "" + type;
            if (strType.equalsIgnoreCase("" + MessageFactory.text)) {
                String payLoad = objects.getString("payload");
                long messagelength = session.getreceviedmessagelength() + payLoad.length();
                session.putreceivemessagelength(messagelength);
                int receviedmesagecount = session.getreceviedmessagecount();
                receviedmesagecount = receviedmesagecount + 1;
                session.putreceivemessagecount(receviedmesagecount);

            } else if (strType.equalsIgnoreCase("" + MessageFactory.contact)) {
                String contactName = objects.getString("contact_name");
                String contactNumber = "", contactDetails = "";
                if (objects.has("createdTomsisdn")) {
                    contactNumber = objects.getString("createdTomsisdn");
                    item.setContactNumber(contactNumber);
                }
                if (objects.has("contact_details")) {
                    contactDetails = objects.getString("contact_details");
                    item.setDetailedContacts(contactDetails);
                }

                if (objects.has("createdTo")) {
                    String contactSmarttyId = objects.getString("createdTo");
                    item.setContactSmarttyId(contactSmarttyId);
                }


                if (objects.has("createdTo")) {
                    String contactSmarttyId = objects.getString("createdTo");
                    item.setContactSmarttyId(contactSmarttyId);
                }
                item.setContactName(contactName);

            } else if (strType.equalsIgnoreCase("" + MessageFactory.picture)) {
                String thumbnail = objects.getString("thumbnail");
                String fileSize = objects.getString("filesize");
                String thumbData = objects.getString("thumbnail_data");

                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.IMAGE_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.picture, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.IMAGE_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                }

                item.setChatFileServerPath(thumbnail);
                item.setFileBufferAt(0);
                item.setUploadDownloadProgress(0);

                item.setThumbnailData(thumbData);
                item.setFileSize(fileSize);
            } else if (strType.equalsIgnoreCase("" + MessageFactory.group_document_message)) {
                item.setMessageType("" + MessageFactory.document);
                String thumbnail = objects.getString("thumbnail");
                String fileSize = objects.getString("filesize");
                String originalFileName = objects.getString("original_filename");
                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.DOCUMENT_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.document, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.DOCUMENT_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                    item.setTextMessage(originalFileName);
                    item.setThumbnailData(thumbnail);
                }

                item.setChatFileServerPath(thumbnail);
                item.setFileSize(fileSize);

            } else if (strType.equalsIgnoreCase("" + MessageFactory.audio)) {
                String thumbnail = objects.getString("thumbnail");
                String fileSize = objects.getString("filesize");

                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.AUDIO_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.audio, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.AUDIO_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                }

                String duration = objects.getString("duration");
                item.setChatFileServerPath(thumbnail);
                item.setFileSize(fileSize);
                item.setDuration(duration);
            } else if (strType.equalsIgnoreCase("" + MessageFactory.video)) {
                String thumbnail = objects.getString("thumbnail");
                String fileSize = objects.getString("filesize");

                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.VIDEO_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.video, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.VIDEO_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                }

                item.setChatFileServerPath(thumbnail);

                String thumbData = objects.getString("thumbnail_data");
                String duration = objects.getString("duration");
                item.setDuration(duration);
                item.setThumbnailData(thumbData);
                item.setFileSize(fileSize);
            }
            if (objects.has("link_details") && !strType.equalsIgnoreCase("" + MessageFactory.group_document_message)) {
                if (objects.has("payload")) {
                    String payLoad = objects.getString("payload");
                }
                JSONObject linkObj = objects.getJSONObject("link_details");

                String recLink = "";
                if (linkObj.has("url")) {
                    recLink = linkObj.getString("url");
                }

                String recLinkTitle = "";
                if (linkObj.has("title")) {
                    recLinkTitle = linkObj.getString("title");
                }

                String recLinkDesc = "";
                if (linkObj.has("description")) {
                    recLinkDesc = linkObj.getString("description");
                }

                String recLinkImgUrl = "";
                if (linkObj.has("image")) {
                    recLinkImgUrl = linkObj.getString("image");
                }

                String recLinkImgThumb = "";
                if (linkObj.has("thumbnail_data")) {
                    recLinkImgThumb = linkObj.getString("thumbnail_data");
                }


                item.setWebLink(recLink);
                item.setWebLinkTitle(recLinkTitle);
                item.setWebLinkDesc(recLinkDesc);
                item.setWebLinkImgUrl(recLinkImgUrl);
                item.setWebLinkImgThumb(recLinkImgThumb);
            }

            if (objects.has("is_tag_applied")) {
                String istagapply = objects.getString("is_tag_applied");
                if (istagapply.equalsIgnoreCase("1")) {
                    if (objects.has("payload")) {
                        String payLoads = objects.getString("payload");
                        String newPayLoad = AppUtils.getTaggedMsgs(mCurrentUserId, payLoads, mContext);
                        item.setTextMessage(newPayLoad);
                    }
                } else {
                    if (objects.has("payload")) {
                        String payLoads = objects.getString("payload");

                        if (payLoads.length() > 0) {

                            item.setTextMessage(payLoads);
                        }


                    }
                }
            }
            item.setCount(1);

            // For showing Group name in chat list
            String groupName = objects.getString("groupName");
            item.setGroupName(groupName);

            item.sethasNewMessage(true);
            String docId;

            if (from.equalsIgnoreCase(mCurrentUserId)) {
                docId = from + "-" + to + "-g";
                item.setIsSelf(true);
                item.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_SENT);
            } else /*if (to.equalsIgnoreCase(uniqueCurrentID))*/ {
                item.setIsSelf(false);
                item.sethasNewMessage(true);
                docId = mCurrentUserId + "-" + to + "-g";
            }
            item.setReceiverID(to);
            item.setMessageId(docId.concat("-").concat(msgId));

            GroupInfoSession groupInfoSession = new GroupInfoSession(mContext);
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);

            if (infoPojo != null) {
                String[] groupMembers = infoPojo.getGroupMembers().split(",");
                try {
                    JSONArray arrMembers = new JSONArray();
                    for (String member : groupMembers) {
                        JSONObject userObj = new JSONObject();
                        userObj.put("UserId", member);
                        userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_SENT);
                        if (from.equals(member)) {
                            userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_READ);
                        }
                        userObj.put("DeliverTime", "");
                        userObj.put("ReadTime", "");
                        arrMembers.put(userObj);
                    }
                    JSONObject deliverObj = new JSONObject();
                    deliverObj.put("GroupMessageStatus", arrMembers);
                    item.setGroupMsgDeliverStatus(deliverObj.toString());
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }

            if (objects.has("replyId") && !objects.has("replyDetails")) {
                String replyRecordId = objects.getString("replyId");
                if (msgSvcCallback != null) {
                    MessageDbController db = CoreController.getDBInstance(mContext);
                    MessageItemChat replyMsgItem = db.getMessageByRecordId(replyRecordId);
                    if (replyMsgItem == null) {
                        msgSvcCallback.getReplyMessageDetails(to, to, replyRecordId, MessageFactory.CHAT_TYPE_GROUP,
                                "no", item.getMessageId());
                    } else {
                        item = getReplyDetailsByMessageItem(replyMsgItem, item);
                    }
                }
            }

            return item;

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
            return null;
        }
    }

    public MessageItemChat loadSingleMessageFromWeb(JSONObject objects) {

        try {
            MyLog.d(TAG, "loadSingleMessageFromWeb" + objects.toString());

            String id = objects.getString("id");
            int delivery = (int) objects.get("deliver");

            JSONObject msgData = objects.getJSONObject("data");
            String secretType = msgData.getString("secret_type");
            String chat_id = (String) objects.get("doc_id");
            String[] ids = chat_id.split("-");
            String doc_id = ids[0] + "-" + ids[1];

            String recordId = msgData.getString("recordId");
            String convId = msgData.getString("convId");
            String fromUserId = msgData.getString("from");
            String toUserId = msgData.getString("to");

            String toMsisdn = msgData.getString("To_msisdn");
            String ts = msgData.getString("timestamp");

            MessageItemChat newMsgItem = new MessageItemChat();
            boolean isSecretTimerMsg = false;
            if (secretType.equalsIgnoreCase("yes")) {

                String secretMsgTimer = msgData.getString("incognito_timer");
                newMsgItem.setSecretTimer(secretMsgTimer);
                newMsgItem.setSecretTimeCreatedBy(mCurrentUserId);

                if (msgData.getString("type").equals(MessageFactory.timer_change + "")) {
                    isSecretTimerMsg = true;
                }
            }

            if (isSecretTimerMsg) {
                MessageItemChat timerMsgItem = getTimerChangeMessage(msgData, newMsgItem);
                return timerMsgItem;
            } else {
                newMsgItem.setIsSelf(true);
                newMsgItem.setStarredStatus(MessageFactory.MESSAGE_UN_STARRED);
                newMsgItem.setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);
                newMsgItem.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_NOT_START);
                newMsgItem.setDeliveryStatus("" + delivery);
                newMsgItem.setReceiverUid(toUserId);
                newMsgItem.setTS(ts);
                newMsgItem.setSenderMsisdn(toMsisdn);
                newMsgItem.setMessageId(chat_id);
                newMsgItem.setConvId(convId);
                newMsgItem.setRecordId(recordId);
                newMsgItem.setReceiverID(toUserId);
                newMsgItem.setSenderName(toMsisdn);
                newMsgItem.setMsgSentAt(ts);
                String savedName = getcontactname.getSendername(toUserId, toMsisdn);
                newMsgItem.setSenderName(savedName);

                int type = Integer.parseInt(msgData.getString("type"));
                newMsgItem.setMessageType("" + type);
                switch (type) {

                    case MessageFactory.text:
                        String textMsg = msgData.getString("message");
                        newMsgItem.setTextMessage(textMsg);
                        break;

                    case MessageFactory.picture:
                        String caption = msgData.getString("message");
                        String thumbnail = msgData.getString("thumbnail");
                        String thumbData = msgData.getString("thumbnail_data");
                        String locFileId = msgData.getString("id");
                        String fileSize = msgData.getString("filesize");
                        String width = msgData.getString("width");
                        String height = msgData.getString("height");
                        String localFilePath = MessageFactory.IMAGE_STORAGE_PATH + MessageFactory.getMessageFileName(
                                MessageFactory.picture, locFileId, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));

                        newMsgItem.setTextMessage(caption);
                        newMsgItem.setChatFileServerPath(thumbnail);
                        newMsgItem.setThumbnailData(thumbData);
                        newMsgItem.setFileSize(fileSize);
                        newMsgItem.setChatFileWidth(width);
                        newMsgItem.setChatFileHeight(height);
                        newMsgItem.setChatFileLocalPath(localFilePath);
                        break;

                    case MessageFactory.contact:
                        String contactName = msgData.getString("contact_name");
                        String contactNumber = msgData.getString("createdTomsisdn");
                        String contactDetails = msgData.getString("contact_details");

                        if (msgData.has("createdTo")) {
                            String contactSmarttyId = msgData.getString("createdTo");
                            newMsgItem.setContactSmarttyId(contactSmarttyId);
                        }
                        newMsgItem.setContactName(contactName);
                        newMsgItem.setContactNumber(contactNumber);
                        newMsgItem.setDetailedContacts(contactDetails);
                        break;

                    case MessageFactory.web_link:
                        String linkMsg = msgData.getString("message");
                        newMsgItem.setTextMessage(linkMsg);
                        break;

                    case MessageFactory.document:
                        String originalFileName = msgData.getString("original_filename");
                        String docPath = msgData.getString("thumbnail");
                        String thumbnailData = msgData.getString("thumbnail_data");
//                    String docMsg = msgData.getString("message");
                        String dataSize = msgData.getString("filesize");
                        String locDocFileId = msgData.getString("id");

                        if (docPath != null && !docPath.equals("")) {
                            File dir = new File(MessageFactory.DOCUMENT_STORAGE_PATH);
                            if (!dir.exists()) {
                                dir.mkdir();
                            }
                            String localFileName = MessageFactory.getMessageFileName(
                                    MessageFactory.document, locDocFileId, FileUploadDownloadManager.getFileExtnFromPath(docPath));
                            String filePath = MessageFactory.DOCUMENT_STORAGE_PATH + localFileName;
                            newMsgItem.setChatFileLocalPath(filePath);
                            newMsgItem.setTextMessage(originalFileName);
                        }

                        newMsgItem.setChatFileServerPath(docPath);
                        newMsgItem.setFileBufferAt(0);
                        newMsgItem.setUploadDownloadProgress(0);
                        newMsgItem.setFileSize(dataSize);
                        newMsgItem.setThumbnailData(thumbnailData);
                        break;

                    case MessageFactory.video:
                        String videoPath = msgData.getString("thumbnail");
                        String videoSize = msgData.getString("filesize");
                        String locVideoFileId = msgData.getString("id");
                        if (videoPath != null && !videoPath.equals("")) {
                            File dir = new File(MessageFactory.VIDEO_STORAGE_PATH);
                            if (!dir.exists()) {
                                dir.mkdir();
                            }
                            String localFileName = MessageFactory.getMessageFileName(
                                    MessageFactory.video, locVideoFileId, FileUploadDownloadManager.getFileExtnFromPath(videoPath));
                            String filePath = MessageFactory.VIDEO_STORAGE_PATH + localFileName;
                            newMsgItem.setChatFileLocalPath(filePath);
                        }
                        newMsgItem.setChatFileServerPath(videoPath);
                        String videoThumbData = objects.getString("thumbnail_data");
                        String duration = objects.getString("duration");
                        newMsgItem.setDuration(duration);
                        newMsgItem.setThumbnailData(videoThumbData);
                        newMsgItem.setFileSize(videoSize);
                        break;
                }

                if (msgData.has("replyDetails")) {
                    JSONObject aReplyObject = msgData.getJSONObject("replyDetails");

                    if (aReplyObject.has("message")) {
                        newMsgItem.setReplyMsisdn(aReplyObject.getString("From_msisdn"));
                        String replyname = aReplyObject.getString("From_msisdn");
                        newMsgItem.setReplyFrom(aReplyObject.getString("from"));
                        String userId = aReplyObject.getString("from");
                        newMsgItem.setReplyType(aReplyObject.getString("type"));
                        if (aReplyObject.has("message")) {
                            newMsgItem.setReplyMessage(aReplyObject.getString("message"));
                        }
                        newMsgItem.setReplyId(aReplyObject.getString("_id"));
                        newMsgItem.setReplyServerLoad(aReplyObject.getString("server_load"));
                        if (aReplyObject.has("thumbnail_data")) {
                            String thumbnail_data = aReplyObject.getString("thumbnail_data");
                            newMsgItem.setreplyimagebase64(thumbnail_data);
                        }

                        replyname = getcontactname.getSendername(userId, replyname);

                        if (newMsgItem.getReplyFrom().equalsIgnoreCase(mCurrentUserId)) {
                            newMsgItem.setReplySender("you");
                        } else {
                            newMsgItem.setReplySender(replyname);
                        }

                        if (aReplyObject.has("link_details")) {
                            try {
                                JSONObject replyLinkObj = aReplyObject.getJSONObject("link_details");
                                String replyLocTitle = replyLinkObj.getString("title");
                                String replyLocThumbData = replyLinkObj.getString("thumbnail_data");
                                newMsgItem.setReplyMessage(replyLocTitle);
                                newMsgItem.setreplyimagebase64(replyLocThumbData);

                            } catch (JSONException e) {
                                MyLog.e(TAG, "", e);
                            }
                        }
                    }

                }

                // This block for Location and weblink messages
                if (msgData.has("link_details")) {
                    JSONObject linkObj = msgData.getJSONObject("link_details");

                    String recLink = "";
                    if (linkObj.has("url")) {
                        recLink = linkObj.getString("url");
                    }

                    String recLinkTitle = "";
                    if (linkObj.has("title")) {
                        recLinkTitle = linkObj.getString("title");
                    }

                    String recLinkDesc = "";
                    if (linkObj.has("description")) {
                        recLinkDesc = linkObj.getString("description");
                    }

                    String recLinkImgUrl = "";
                    if (linkObj.has("image")) {
                        recLinkImgUrl = linkObj.getString("image");
                    }

                    String recLinkImgThumb = "";
                    if (linkObj.has("thumbnail_data")) {
                        recLinkImgThumb = linkObj.getString("thumbnail_data");
                    }

                    newMsgItem.setWebLink(recLink);
                    newMsgItem.setWebLinkTitle(recLinkTitle);
                    newMsgItem.setWebLinkDesc(recLinkDesc);
                    newMsgItem.setWebLinkImgUrl(recLinkImgUrl);
                    newMsgItem.setWebLinkImgThumb(recLinkImgThumb);
                }

                return newMsgItem;
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        return null;

    }

    private MessageItemChat getTimerChangeMessage(JSONObject msgData, MessageItemChat newMsgItem) {
        try {
            MyLog.d(TAG, "getTimerChangeMessage" + msgData.toString());

            String from = msgData.getString("from");
            String to = msgData.getString("to");
            String convId = msgData.getString("convId");
            String recordId = msgData.getString("recordId");
            String timer = msgData.getString("incognito_timer");
            String timerMode = msgData.getString("incognito_timer_mode");
            String toUserMsgId;
            if (msgData.has("doc_id")) {
                toUserMsgId = msgData.getString("doc_id");
            } else {
                toUserMsgId = msgData.getString("docId");
            }
            String msgId = msgData.getString("id");
            String timeStamp = msgData.getString("timestamp");
            String fromMsisdn = msgData.getString("ContactMsisdn");

            MessageItemChat item = new MessageItemChat();
            String docId;
            if (from.equalsIgnoreCase(mCurrentUserId)) {
                docId = from + "-" + to;
                item.setReceiverID(to);
            } else {
                docId = to + "-" + from;
                item.setReceiverID(from);
            }

            item.setIsSelf(false);
            item.setIsDate(true);
            item.setConvId(convId);
            item.setRecordId(recordId);
            item.setSecretTimer(timer);
            item.setSecretTimeCreatedBy(from);
            item.setSecretTimerMode(timerMode);
            item.setMessageId(docId + "-" + msgId);
            item.setTS(timeStamp);
            item.setSenderMsisdn(fromMsisdn);
            item.setMessageType(MessageFactory.timer_change + "");
            item.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_DELIVERED);

            MessageDbController db = CoreController.getDBInstance(mContext);
            docId = docId + "-" + MessageFactory.CHAT_TYPE_SECRET;
            db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SECRET);

            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
            contactDB_sqlite.updateSecretMessageTimer(to, timer, from, item.getMessageId());

            return item;

        } catch (JSONException e) {
            return null;
        }
    }


    public MessageItemChat loadGroupMessageFromWeb(JSONObject objects) {
        try {
            MyLog.d(TAG, "loadGroupMessageFromWeb" + objects.toString());

            Integer type = Integer.parseInt(objects.getString("type"));

            String id = objects.getString("id");
            String from = objects.getString("from");
            String to = objects.getString("groupId");

            String toDocId = (String) objects.get("toDocId");
            String[] splitDocId = toDocId.split("-");
            String msgId = splitDocId[3];
            String name = objects.getString("msisdn");
//            String convId = objects.getString("convId");
            String recordId = objects.getString("recordId");
            String starredStatus = objects.getString("isStar");

            String ts = objects.getString("timestamp");
            if (session.getarchivecountgroup() != 0) {
                if (session.getarchivegroup(mCurrentUserId + "-" + to + "-g"))
                    session.removearchivegroup(mCurrentUserId + "-" + to + "-g");
            }

            MessageItemChat item = new MessageItemChat();
//            item.setConvId(convId);
            item.setRecordId(recordId);
            item.set_id(id);
            item.setStarredStatus(starredStatus);
            item.setTS(ts);
            item.setGroupMsgFrom(from);
            item.setSenderName(name);
            item.setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_NOT_START);
            item.setMessageId(toDocId);
            String payLoad = objects.getString("payload");
            item.setTextMessage(payLoad);
            if (type == MessageFactory.picture || type == MessageFactory.audio || type == MessageFactory.video) {
                long filesize = AppUtils.parseLongForFile(objects.getString("filesize"));
                filesize = session.getreceviedmedialength() + filesize;
                session.putreceivemedialength(filesize);
            }

            if (objects.has("replyDetails")) {
                JSONObject aReplyObject = objects.getJSONObject("replyDetails");

                if (aReplyObject.has("message")) {
                    item.setReplyMsisdn(aReplyObject.getString("From_msisdn"));
                    item.setReplyFrom(aReplyObject.getString("from"));
                    String userID = aReplyObject.getString("from");
                    String replyname = aReplyObject.getString("From_msisdn");
                    //   item.setReplyType(aReplyObject.getString("type"));

                    if (aReplyObject.has("type")) {
                        item.setReplyType(aReplyObject.getString("type"));
                    } else {
                        item.setReplyType("");

                    }
                    if (aReplyObject.has("message")) {
                        item.setReplyMessage(aReplyObject.getString("message"));
                    }
                    if (aReplyObject.has("thumbnail_data")) {
                        String thumbnail_data = aReplyObject.getString("thumbnail_data");
                        item.setreplyimagebase64(thumbnail_data);
                    }
                    item.setReplyId(aReplyObject.getString("_id"));

                    replyname = getcontactname.getSendername(userID, replyname);

                    if (item.getReplyFrom().equalsIgnoreCase(mCurrentUserId)) {
                        item.setReplySender("you");
                    } else {
                        item.setReplySender(replyname);
                    }

                    if (aReplyObject.has("link_details")) {
                        try {
                            JSONObject replyLinkObj = aReplyObject.getJSONObject("link_details");
                            String replyLocTitle = replyLinkObj.getString("title");
                            String replyLocThumbData = replyLinkObj.getString("thumbnail_data");

                            item.setReplyMessage(replyLocTitle);
                            item.setreplyimagebase64(replyLocThumbData);
                        } catch (JSONException e) {
                            MyLog.e(TAG, "", e);
                        }
                    }
                }

            }
            item.setMessageType("" + type);
            String strType = "" + type;
            if (strType.equalsIgnoreCase("" + MessageFactory.text)) {
                long messagelength = session.getreceviedmessagelength() + payLoad.length();
                session.putreceivemessagelength(messagelength);
                int receviedmesagecount = session.getreceviedmessagecount();
                receviedmesagecount = receviedmesagecount + 1;
                session.putreceivemessagecount(receviedmesagecount);

            } else if (strType.equalsIgnoreCase("" + MessageFactory.contact)) {
                String contactName = objects.getString("contact_name");
                String contactNumber = objects.getString("createdTomsisdn");
                String contactDetails = objects.getString("contact_details");
                if (objects.has("createdTo")) {
                    String contactSmarttyId = objects.getString("createdTo");
                    item.setContactSmarttyId(contactSmarttyId);
                }
                item.setContactName(contactName);
                item.setContactNumber(contactNumber);
                item.setDetailedContacts(contactDetails);
            } else if (strType.equalsIgnoreCase("" + MessageFactory.picture)) {
                String thumbnail = objects.getString("thumbnail");
                String fileSize = objects.getString("filesize");
                String thumbData = objects.getString("thumbnail_data");

                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.IMAGE_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.picture, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.IMAGE_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                }

                item.setChatFileServerPath(thumbnail);
                item.setFileBufferAt(0);
                item.setUploadDownloadProgress(0);

                item.setThumbnailData(thumbData);
                item.setFileSize(fileSize);
            } else if (strType.equalsIgnoreCase("" + MessageFactory.group_document_message)) {
                item.setMessageType("" + MessageFactory.document);

                String thumbnail = objects.getString("thumbnail");
                String thumbnailData = objects.getString("thumbnail_data");
                String originalFileName = objects.getString("original_filename");
                String fileSize = objects.getString("filesize");

                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.DOCUMENT_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.document, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.DOCUMENT_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                    item.setThumbnailData(thumbnailData);
                    item.setTextMessage(originalFileName);
                }

                item.setChatFileServerPath(thumbnail);
                item.setFileSize(fileSize);
            } else if (strType.equalsIgnoreCase("" + MessageFactory.audio)) {
                String thumbnail = objects.getString("thumbnail");
                String fileSize = objects.getString("filesize");

                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.AUDIO_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.audio, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.AUDIO_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                }

                String duration = objects.getString("duration");
                item.setChatFileServerPath(thumbnail);
                item.setFileSize(fileSize);
                item.setDuration(duration);
            } else if (strType.equalsIgnoreCase("" + MessageFactory.video)) {
                String thumbnail = objects.getString("thumbnail");
                String fileSize = objects.getString("filesize");

                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.VIDEO_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.video, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.VIDEO_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                }

                item.setChatFileServerPath(thumbnail);

                String thumbData = objects.getString("thumbnail_data");
                String duration = objects.getString("duration");
                item.setDuration(duration);
                item.setThumbnailData(thumbData);
                item.setFileSize(fileSize);
            }
            if (objects.has("link_details")) {
                JSONObject linkObj = objects.getJSONObject("link_details");

                String recLink = "";
                if (linkObj.has("url")) {
                    recLink = linkObj.getString("url");
                }

                String recLinkTitle = "";
                if (linkObj.has("title")) {
                    recLinkTitle = linkObj.getString("title");
                }

                String recLinkDesc = "";
                if (linkObj.has("description")) {
                    recLinkDesc = linkObj.getString("description");
                }

                String recLinkImgUrl = "";
                if (linkObj.has("image")) {
                    recLinkImgUrl = linkObj.getString("image");
                }

                String recLinkImgThumb = "";
                if (linkObj.has("thumbnail_data")) {
                    recLinkImgThumb = linkObj.getString("thumbnail_data");
                }


                item.setWebLink(recLink);
                item.setWebLinkTitle(recLinkTitle);
                item.setWebLinkDesc(recLinkDesc);
                item.setWebLinkImgUrl(recLinkImgUrl);
                item.setWebLinkImgThumb(recLinkImgThumb);
            }
            item.setCount(1);

            // For showing Group name in chat list
            String groupName = objects.getString("groupName");
            item.setGroupName(groupName);

            item.sethasNewMessage(true);
            String docId;

            if (from.equalsIgnoreCase(mCurrentUserId)) {
                docId = from + "-" + to + "-g";
                item.setIsSelf(true);
                item.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_SENT);
            } else /*if (to.equalsIgnoreCase(uniqueCurrentID))*/ {
                item.setIsSelf(false);
                item.sethasNewMessage(true);
                docId = mCurrentUserId + "-" + to + "-g";
            }
            item.setReceiverID(to);
            item.setMessageId(docId.concat("-").concat(msgId));

            GroupInfoSession groupInfoSession = new GroupInfoSession(mContext);
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);

            if (infoPojo != null) {
                String[] groupMembers = infoPojo.getGroupMembers().split(",");
                try {
                    JSONArray arrMembers = new JSONArray();
                    for (String member : groupMembers) {
                        JSONObject userObj = new JSONObject();
                        userObj.put("UserId", member);
                        userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_SENT);
                        if (from.equals(member)) {
                            userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_READ);
                        }
                        userObj.put("DeliverTime", "");
                        userObj.put("ReadTime", "");
                        arrMembers.put(userObj);
                    }
                    JSONObject deliverObj = new JSONObject();
                    deliverObj.put("GroupMessageStatus", arrMembers);
                    item.setGroupMsgDeliverStatus(deliverObj.toString());
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }

//            JSONArray arrMsgStatus = objects.getJSONArray("status");

            return item;

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
            return null;
        }
    }

    public MessageItemChat loadSingleMessage(JSONObject objects) {
        MessageItemChat item = new MessageItemChat();
        String duration = "";


        MyLog.d(TAG, "loadSingleMessage" + objects.toString());
        try {
            String type = objects.getString("type");
            String convId = "";
            if (type.equals(MessageFactory.missed_call + "")) {
                item = getMissedCallMessage(objects);
            } else {
                String payLoad = "";
                if (objects.has("payload")) {
                    payLoad = objects.getString("payload");
                }
                String id = objects.getString("msgId");
                String from = objects.getString("from");
                if (objects.has("reply_type") && objects.getString("reply_type").equals("status")) {
                    item.setStatusReply(true);
                }

                if (type.equalsIgnoreCase(MessageFactory.text + "")) {
                    long messagelength = session.getreceviedmessagelength() + payLoad.length();
                    session.putreceivemessagelength(messagelength);
                    int receviedmesagecount = session.getreceviedmessagecount();
                    receviedmesagecount = receviedmesagecount + 1;
                    session.putreceivemessagecount(receviedmesagecount);
                    //System.out.println("receviedmesagecount" + receviedmesagecount);
                } else if (type.equalsIgnoreCase(MessageFactory.picture + "") || type.equalsIgnoreCase(MessageFactory.audio + "")
                        || type.equalsIgnoreCase(MessageFactory.video + "") || type.equalsIgnoreCase(MessageFactory.document + "")) {
                    long filesize = AppUtils.parseLongForFile(objects.getString("filesize"));
                    filesize = session.getreceviedmedialength() + filesize;
                    session.putreceivemedialength(filesize);
                }
                String thumbnail = "";
                if (objects.has("thumbnail")) {
                    thumbnail = objects.getString("thumbnail");
                }

                String name = objects.optString("Name");
                String dataSize = "";
                if (objects.has("filesize")) {
                    dataSize = objects.getString("filesize");
                }

                if (objects.has("convId")) {
                    convId = objects.getString("convId");
                    item.setConvId(convId);

                } else {
                    item.setConvId(convId);

                }
                String recordId = objects.getString("recordId");
//            String starredStatus = objects.getString("isStar");
                String ts = objects.getString("timestamp");

                JSONObject linkObj;
                if (objects.has("link_details")) {
                    try {
                        linkObj = objects.getJSONObject("link_details");
                    } catch (JSONException e) {
                        linkObj = new JSONObject();
                    }

                    String webLink = "";
                    if (linkObj.has("url")) {
                        webLink = linkObj.getString("url");
                    }


                    String webLinkTitle = "";
                    if (linkObj.has("title")) {
                        webLinkTitle = linkObj.getString("title");
                    }


                    String webLinkDesc = "";
                    if (linkObj.has("description")) {
                        webLinkDesc = linkObj.getString("description");
                    }

                    String webLinkImgUrl = "";
                    if (linkObj.has("image")) {
                        webLinkImgUrl = linkObj.getString("image");
                    }

                    String webLinkImgThumb = "";
                    if (linkObj.has("thumbnail_data")) {
                        webLinkImgThumb = linkObj.getString("thumbnail_data");
                    }

                    item.setWebLink(webLink);
                    item.setWebLinkTitle(webLinkTitle);
                    item.setWebLinkDesc(webLinkDesc);
                    item.setWebLinkImgUrl(webLinkImgUrl);
                    item.setWebLinkImgThumb(webLinkImgThumb);

                }

//            JSONObject linkObj = objects.getJSONObject("link_details");


                try {
                    if (objects.has("ContactMsisdn")) {
                        String senderMsisdn = objects.getString("ContactMsisdn");
                        String phContactName = senderMsisdn;
                        phContactName = getcontactname.getSendername(from, senderMsisdn);
                        item.setSenderName(phContactName);
                        item.setSenderMsisdn(senderMsisdn);
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }

                item.setIsSelf(false);
                item.setTS(ts);
                item.setRecordId(recordId);
                item.setStarredStatus(MessageFactory.MESSAGE_UN_STARRED);
                if (objects.has("replyDetails")) {
                    JSONObject aReplyObject = objects.getJSONObject("replyDetails");
                    if (aReplyObject.has("message")) {
                        item = getReplyDetailsByObject(objects, aReplyObject, item);
                    }
                }
                item.setMessageType("" + type);
                item.sethasNewMessage(true);
                if (payLoad != null && payLoad.length() > 0)
                    item.setTextMessage(payLoad);

                item.setCount(1);

                if (type.equalsIgnoreCase("" + MessageFactory.text)) {
                    item.setTextMessage(payLoad);
                } else if (type.equalsIgnoreCase("" + MessageFactory.contact)) {
//                item.setContactInfo(payLoad);
                    String contactName = objects.getString("contact_name");
                    if (objects.has("createdTomsisdn")) {
                        String contactNumber = objects.getString("createdTomsisdn");
                        item.setContactNumber(contactNumber);
                    }
                    if (objects.has("contact_details")) {
                        String contactDetails = objects.getString("contact_details");
                        item.setDetailedContacts(contactDetails);
                    }
                    if (objects.has("createdTo")) {
                        String contactSmarttyId = objects.getString("createdTo");
                        item.setContactSmarttyId(contactSmarttyId);
//                        String AvtarImage = Constants.userprofileurl + contactSmarttyId + ".jpg?id=" + Calendar.getInstance().getTimeInMillis();
//                        item.setAvatarImageUrl(AvtarImage);
                    }

                    item.setContactName(contactName);

                } else if (type.equalsIgnoreCase("" + MessageFactory.picture)) {

                    String width = objects.getString("width");
                    String height = objects.getString("height");

                    if (thumbnail != null && !thumbnail.equals("")) {
                        File dir = new File(Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES), MessageFactory.IMAGE_STORAGE_PATH);
                        if (!dir.exists()) {
                            dir.mkdir();
                        }
                        String localFileName = MessageFactory.getMessageFileName(
                                MessageFactory.picture, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                        String filePath = MessageFactory.IMAGE_STORAGE_PATH + localFileName;
                        item.setChatFileLocalPath(filePath);
                        item.setChatFileWidth(width);
                        item.setChatFileHeight(height);
                    }

                    item.setChatFileServerPath(thumbnail);
                    String thumbData = objects.getString("thumbnail_data");
                    item.setThumbnailData(thumbData);
                    item.setFileSize(dataSize);
                } else if (type.equalsIgnoreCase("" + MessageFactory.audio)) {
                    String audiotype = "";
                    if (thumbnail != null && !thumbnail.equals("")) {
                        File dir = new File(MessageFactory.AUDIO_STORAGE_PATH);
                        if (!dir.exists()) {
                            dir.mkdir();
                        }
                        if (objects.has("audio_type")) {
                            audiotype = objects.getString("audio_type");
                        }

                        if (audiotype != null && !audiotype.equalsIgnoreCase("")) {
                            item.setaudiotype(Integer.parseInt(audiotype));
                        }

                        String localFileName = MessageFactory.getMessageFileName(
                                MessageFactory.audio, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                        String filePath = MessageFactory.AUDIO_STORAGE_PATH + localFileName;
                        item.setChatFileLocalPath(filePath);
                    }

                    if (objects.has("duration")) {

                        duration = objects.getString("duration");
                    }


                    item.setDuration(duration);
                    item.setChatFileServerPath(thumbnail);
                    item.setFileSize(dataSize);
                } else if (type.equalsIgnoreCase("" + MessageFactory.document)) {
                    String thumbData = null;
                    String originalFileName = objects.getString("original_filename");
//                    item.setMessageType("" + MessageFactory.document);
                    if (thumbnail != null && !thumbnail.equals("")) {
                        File dir = new File(MessageFactory.DOCUMENT_STORAGE_PATH);
                        if (!dir.exists()) {
                            dir.mkdir();
                        }
                        String localFileName = MessageFactory.getMessageFileName(
                                MessageFactory.document, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                        String filePath = MessageFactory.DOCUMENT_STORAGE_PATH + localFileName;
                        item.setChatFileLocalPath(filePath);
                        item.setTextMessage(originalFileName);
                        item.setChatFileServerPath(thumbnail);
                        item.setFileSize(dataSize);
                        if (objects.has("thumbnail")) {
                            thumbData = objects.getString("thumbnail");
                        } else {
                            thumbData = objects.getString("thumbnail_data");
                        }

                        item.setThumbnailData(thumbData);
                    }
                } else if (type.equalsIgnoreCase("" + MessageFactory.video)) {
                    if (thumbnail != null && !thumbnail.equals("")) {
                        File dir = new File(MessageFactory.VIDEO_STORAGE_PATH);
                        if (!dir.exists()) {
                            dir.mkdir();
                        }
                        String localFileName = MessageFactory.getMessageFileName(
                                MessageFactory.video, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                        String filePath = MessageFactory.VIDEO_STORAGE_PATH + localFileName;
                        item.setChatFileLocalPath(filePath);
                    }

                    item.setChatFileServerPath(thumbnail);


                    if (objects.has("duration")) {

                        duration = objects.getString("duration");
                    }

                    item.setDuration(duration);

                    if (objects.has("thumbnail_data")) {
                        String thumbData = objects.getString("thumbnail_data");
                        item.setThumbnailData(thumbData);
                    } else if (objects.has("thumbnail")) {
                        String thumbData = objects.getString("thumbnail");
                        item.setThumbnailData(thumbData);
                    }
                    item.setFileSize(dataSize);
                }

                String docId = mCurrentUserId + "-" + from;
                item.setMessageId(docId + "-" + id);
                String secretType = "no";
                if (objects.has("secret_type")) {
                    secretType = objects.getString("secret_type");
                    if (secretType.equalsIgnoreCase("yes")) {
                        docId = docId + "-" + MessageFactory.CHAT_TYPE_SECRET;
                    }
                }
                if (objects.has("replyId") && !objects.has("replyDetails")) {
                    String replyRecordId = objects.getString("replyId");
                    item.setReplyId(replyRecordId);

                    MessageDbController db = CoreController.getDBInstance(mContext);
                    MessageItemChat replyMsgItem = db.getMessageByRecordId(replyRecordId);
                    if (replyMsgItem == null) {
                        if (msgSvcCallback != null) {
                            msgSvcCallback.getReplyMessageDetails(from, convId, replyRecordId,
                                    MessageFactory.CHAT_TYPE_SINGLE, secretType, item.getMessageId());
                        }
                    } else {
                        item = getReplyDetailsByMessageItem(replyMsgItem, item);
                    }
                }
                item.setMsgId(id);
             /*   item.setWebLink(webLink);
                item.setWebLinkTitle(webLinkTitle);
                item.setWebLinkDesc(webLinkDesc);
                item.setWebLinkImgUrl(webLinkImgUrl);
                item.setWebLinkImgThumb(webLinkImgThumb);
           */
                item.setReceiverID(from);
                item.setFileBufferAt(0);
                item.setUploadDownloadProgress(0);
                item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_NOT_START);

            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        return item;
    }

    public MessageItemChat getReplyDetailsByObject(JSONObject objects, JSONObject replyObject, MessageItemChat item) {
        MyLog.e("getReplyDetailsByObject", "getReplyDetailsByObject" + replyObject);
        //   Log.e("getReplyDetailsByObject","getReplyDetailsByObject"+item.toString());

        try {
            String fromMsisdn = "";
            if (replyObject.has("DisplayMsisdn")) {
                fromMsisdn = replyObject.getString("DisplayMsisdn");
            } else if (replyObject.has("From_msisdn")) {
                fromMsisdn = replyObject.getString("From_msisdn");
            } else if (replyObject.has("ContactMsisdn")) {
                fromMsisdn = replyObject.getString("ContactMsisdn");
            } else {
                if (replyObject.has("msisdn")) {

                    fromMsisdn = replyObject.getString("msisdn");
                    item.setReplyMsisdn(fromMsisdn);
                }
            }

            //From is null
            if (replyObject.has("from")) {
                item.setReplyFrom(replyObject.getString("from"));
            }
            String userId = "";
            //item.setReplyFrom(replyObject.getString("from"));
            if (replyObject.has("from")) {
                userId = replyObject.getString("from");
            }
            //       String userId = replyObject.getString("from");
            if (replyObject.has("type")) {
                item.setReplyType(replyObject.getString("type"));
            } else {
                item.setReplyType("");

            }
            if (replyObject.has("message")) {
                if (replyObject.has("original_filename")) {

                    String fileName = replyObject.getString("original_filename");
                    if (fileName != null && !fileName.trim().isEmpty())
                        item.setReplyMessage(replyObject.getString("original_filename"));
                    else
                        item.setReplyMessage(replyObject.getString("message"));

                } else {
                    item.setReplyMessage(replyObject.getString("message"));
                }

            }
            if (replyObject.has("_id")) {
                item.setReplyId(replyObject.getString("_id"));
            } else {
                item.setReplyId(objects.getString("_id"));

            }
            if (replyObject.has("server_load")) {
                item.setReplyServerLoad(replyObject.getString("server_load"));
            }
            // item.setReplyServerLoad(replyObject.getString("server_load"));
            if (replyObject.has("thumbnail_data")) {
                String thumbnail_data = replyObject.getString("thumbnail_data");
                item.setreplyimagebase64(thumbnail_data);
            }

            String replyname = getcontactname.getSendername(userId, fromMsisdn);

            if (item.getReplyFrom().equalsIgnoreCase(mCurrentUserId)) {
                item.setReplySender("you");
            } else {
                item.setReplySender(replyname);
            }

            if (replyObject.has("link_details")) {
                try {
                    JSONObject replyLinkObj = replyObject.getJSONObject("link_details");
                    if (replyLinkObj.has("title")) {
                        String replyLocTitle = replyLinkObj.getString("title");
                        item.setReplyMessage(replyLocTitle);
                    }

                        /*String replyLocThumbUrl = replyLinkObj.getString("image");
                        String replyLocDesc = replyLinkObj.getString("description");
                        String replyLocUrl = replyLinkObj.getString("url");*/
                    if (replyLinkObj.has("thumbnail_data")) {

                        String replyLocThumbData = replyLinkObj.getString("thumbnail_data");
                        item.setreplyimagebase64(replyLocThumbData);

                    }
                        /*item.setWebLinkImgUrl(replyLocThumbUrl);
                        item.setWebLinkDesc(replyLocDesc);
                        item.setWebLink(replyLocUrl);*/
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

        return item;
    }

    public MessageItemChat getReplyDetailsByMessageItem(MessageItemChat replyMsgItem, MessageItemChat item) {

        String repliedUserId, repliedMsisdn, replyname;
        if (replyMsgItem.isSelf()) {
            repliedUserId = mCurrentUserId;
            repliedMsisdn = SessionManager.getInstance(mContext).getPhoneNumberOfCurrentUser();
            replyname = "you";
        } else {
            repliedUserId = replyMsgItem.getReceiverID();
            repliedMsisdn = replyMsgItem.getSenderMsisdn();
            replyname = getcontactname.getSendername(repliedUserId, repliedMsisdn);
        }

        item.setReplyMsisdn(repliedMsisdn);
        item.setReplyFrom(repliedUserId);
        item.setReplySender(replyname);
        item.setReplyType(replyMsgItem.getMessageType());
        item.setReplyMessage(replyMsgItem.getTextMessage());
        item.setReplyId(replyMsgItem.getRecordId());
        item.setreplyimagebase64(replyMsgItem.getThumbnailData());

        if (replyMsgItem.getMessageType() != null &&
                (replyMsgItem.getMessageType().equals(MessageFactory.web_link + "") ||
                        replyMsgItem.getMessageType().equals(MessageFactory.location + ""))) {
            item.setReplyMessage(replyMsgItem.getWebLinkTitle());
            item.setreplyimagebase64(replyMsgItem.getWebLinkImgThumb());
        }

//        item.setReplyServerLoad(replyObject.getString("server_load"));

        return item;
    }

    public MessageItemChat getMissedCallMessage(JSONObject object) {

        MessageItemChat msgItem = new MessageItemChat();

        try {
            String type = object.getString("type");
            String from = object.getString("from");
            String to = object.getString("to");
            String id = object.getString("id");
            String recordId = object.getString("recordId");
            String convId = object.getString("convId");
            String senderMsisdn = object.getString("ContactMsisdn");

            String ts = object.getString("timestamp");

            String callId;
            if (object.has("docId")) {
                callId = object.getString("docId");
            } else {
                callId = object.getString("doc_id");
            }

            String docId = mCurrentUserId + "-" + from;
            String msgId = docId + "-" + id;

            msgItem.set_id(id);
            msgItem.setMessageId(msgId);
            msgItem.setConvId(convId);
            msgItem.setRecordId(recordId);
            msgItem.setReceiverID(from);
            msgItem.setMessageType(type);
            msgItem.setIsSelf(false);
            msgItem.setTS(ts);
            msgItem.setSenderMsisdn(senderMsisdn);
            msgItem.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_DELIVERED);
            msgItem.setStarredStatus(MessageFactory.MESSAGE_UN_STARRED);

            // TODO: 8/18/2017 Need call_type for offline call logs from json
            String callType = object.getString("call_type");
            msgItem.setCallType(callType);

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        return msgItem;
    }

    public CallItemChat loadOutgoingCall(JSONObject object) {
        MyLog.e(TAG, "loadOutgoingCall" + object);
        try {
            String type = object.getString("type");
            String from = object.getString("from");
            String to = object.getString("to");
            String id = object.getString("id");
            String recordId = object.getString("recordId");
            String toUserMsisdn = object.getString("To_msisdn");
            String callStatus = object.getString("call_status");
            String callId = object.getString("doc_id");
            String ts = object.getString("timestamp");
            String receiverName = getcontactname.getSendername(to, toUserMsisdn);

            CallItemChat callItem = new CallItemChat();
            callItem.setId(id);
            callItem.setCallType(type);
            callItem.setOpponentUserId(to);
            callItem.setRecordId(recordId);
            callItem.setOpponentUserMsisdn(toUserMsisdn);
            callItem.setCallStatus(callStatus);
            callItem.setCallId(callId);
            callItem.setTS(ts);
            callItem.setIsSelf(true);
            callItem.setCallerName(receiverName);
//            callItem = CallMessage.getCallCount(from, to, callItem);
            return callItem;
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
            return null;
        }
    }

    public CallItemChat loadIncomingCall(JSONObject object) {
        MyLog.e(TAG, "loadIncomingCall" + object);
        try {
            String type = object.getString("type");
            String from = "";
            if (object.has("from")) {
                from = object.getString("from");
            }
            if (object.has("from_id")) {
                from = object.getString("from_id");
            }
            String to = object.getString("to");
            String id = object.getString("id");
            String recordId = object.getString("recordId");
            String fromUserMsisdn = object.getString("ContactMsisdn");
            //String callStatus = object.getString("call_status");
            String ts = object.getString("timestamp");
            String receiverName = getcontactname.getSendername(from, fromUserMsisdn);

            String callId = to + "-" + from + "-" + id;

            CallItemChat callItem = new CallItemChat();
            callItem.setId(id);
            callItem.setCallType(type);
            callItem.setOpponentUserId(from);
            callItem.setRecordId(recordId);

            callItem.setOpponentUserMsisdn(fromUserMsisdn);
            callItem.setCallStatus(MessageFactory.CALL_STATUS_ARRIVED + "");
            callItem.setCallId(callId);
            callItem.setTS(ts);
            callItem.setIsSelf(false);
            callItem.setCallerName(receiverName);
//            callItem = CallMessage.getCallCount(to, from, callItem);
            return callItem;
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
            return null;
        }
    }

    public CallItemChat loadOfflineCall(JSONObject object) {
        CallItemChat callItem = new CallItemChat();

        try {
            String type = "";
            if (object.has("type"))
                type = object.getString("type");
            String from = object.getString("from");
            String to = object.getString("to");

            String id = "";
            if (object.has("msgId")) {
                id = object.getString("msgId");
            } else if (object.has("id")) {
                id = object.getString("id");
            }
            String recordId = object.getString("recordId");
            String fromUserMsisdn = "";
            if (object.has("ContactMsisdn"))
                fromUserMsisdn = object.getString("ContactMsisdn");
            String callStatus = object.getString("call_status");
            String ts = "";
            if (object.has("timestamp")) {
                ts = object.getString("timestamp");
            } else {
                ts = "" + id;
            }
            String receiverName = getcontactname.getSendername(from, fromUserMsisdn);

            String callId = to + "-" + from + "-" + id;

            callItem.setId(id);
            callItem.setCallType(type);
            callItem.setOpponentUserId(from);
            callItem.setRecordId(recordId);
            callItem.setOpponentUserMsisdn(fromUserMsisdn);
            callItem.setCallStatus(callStatus);
            callItem.setCallId(callId);
            callItem.setTS(ts);
            callItem.setIsSelf(false);
            callItem.setCallerName(receiverName);
            callItem.setCallDuration("00:00");
            if (callStatus != null && callStatus.equals("5"))
                callItem.setCallCount(1);
//            callItem = CallMessage.getCallCount(to, from, callItem);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
            return null;
        }
        return callItem;

    }

    public CallItemChat loadfromOfflineCall(JSONObject object) {
        CallItemChat callItem = new CallItemChat();

        try {
            String type = "";
            if (object.has("type"))
                type = object.getString("type");
            String from = object.getString("from");
            String to = object.getString("to");

            String id = "";
            if (object.has("msgId")) {
                id = object.getString("msgId");
            } else if (object.has("id")) {
                id = object.getString("id");
            }
            String recordId = object.getString("recordId");
            String fromUserMsisdn = "";
            if (object.has("ContactMsisdn"))
                fromUserMsisdn = object.getString("ContactMsisdn");
            String callStatus = object.getString("call_status");
            String ts = "";
            if (object.has("timestamp")) {
                ts = object.getString("timestamp");
            } else {
                ts = "" + id;
            }
            String receiverName = getcontactname.getSendername(from, fromUserMsisdn);
            String callId = from + "-" + to + "-" + id;
            callItem.setId(id);
            callItem.setCallType(type);
            callItem.setOpponentUserId(to);
            callItem.setRecordId(recordId);
            callItem.setOpponentUserMsisdn(fromUserMsisdn);
            callItem.setCallStatus(callStatus);
            callItem.setCallId(callId);
            callItem.setTS(ts);
            callItem.setIsSelf(false);
            callItem.setCallerName(receiverName);
            callItem.setCallDuration("00:00");
            if (callStatus != null && callStatus.equals("5"))
                callItem.setCallCount(1);
//            callItem = CallMessage.getCallCount(to, from, callItem);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
            return null;
        }
        return callItem;

    }

    public CallItemChat loadfromOfflineOutgoingCall(JSONObject object) {
        CallItemChat callItem = new CallItemChat();

        try {
            String type = "";
            if (object.has("type"))
                type = object.getString("type");
            String from = object.getString("from");
            String to = object.getString("to");

            String id = "";
            if (object.has("msgId")) {
                id = object.getString("msgId");
            } else if (object.has("id")) {
                id = object.getString("id");
            }
            String recordId = object.getString("recordId");
            String fromUserMsisdn = "";
            if (object.has("ContactMsisdn"))
                fromUserMsisdn = object.getString("ContactMsisdn");
            String callStatus = object.getString("call_status");
            String ts = "";
            if (object.has("timestamp")) {
                ts = object.getString("timestamp");
            } else {
                ts = "" + id;
            }
            String receiverName = getcontactname.getSendername(from, fromUserMsisdn);
            String callId = from + "-" + to + "-" + id;
            callItem.setId(id);
            callItem.setCallType(type);
            callItem.setOpponentUserId(to);
            callItem.setRecordId(recordId);
            callItem.setOpponentUserMsisdn(fromUserMsisdn);
            callItem.setCallStatus(callStatus);
            callItem.setCallId(callId);
            callItem.setTS(ts);
            callItem.setIsSelf(true);
            callItem.setCallerName(receiverName);
            callItem.setCallDuration("00:00");
            if (callStatus != null && callStatus.equals("5"))
                callItem.setCallCount(1);
//            callItem = CallMessage.getCallCount(to, from, callItem);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
            return null;
        }
        return callItem;

    }


    public void setCallback(MessageService msgSvcCallback) {
        this.msgSvcCallback = msgSvcCallback;
    }

    public CallItemChat loadOutgoingCallOppenent(JSONObject object) {
        MyLog.e(TAG, "loadOutgoingCallOppenent" + object);
        CallItemChat callItem = new CallItemChat();
        boolean isAnsweredToUser = SharedPreference.getInstance().getBool(mContext, "isAnsweredToUser");
        try {
            String type = "";
            if (object.has("type"))
                type = object.getString("type");
            String from = object.getString("from");
            String to = object.getString("to");
            String toDocId = object.getString("toDocId");
            String id = "";
            if (object.has("msgId")) {
                id = object.getString("msgId");
            } else if (object.has("id")) {
                id = object.getString("id");
            }
            String recordId = object.getString("recordId");

            String fromUserMsisdn = "";
            if (object.has("ContactMsisdn"))
                fromUserMsisdn = object.getString("ContactMsisdn");
            String callStatus = object.getString("call_status");
            String ts = "";
            if (object.has("timestamp")) {
                ts = object.getString("timestamp");
            } else {
                ts = "" + id;
            }
            String receiverName = getcontactname.getSendername(from, fromUserMsisdn);

            String callId = to + "-" + from + "-" + id;

            callItem.setId(id);
            callItem.setCallType(type);
            callItem.setOpponentUserId(from);
            callItem.setRecordId(recordId);
            callItem.setOpponentUserMsisdn(fromUserMsisdn);
            callItem.setCallStatus(callStatus);
            callItem.setCallId(callId);
            callItem.setTS(ts);
            if (toDocId.startsWith(mCurrentUserId))
                callItem.setIsSelf(true);
            else if (Integer.parseInt(callStatus) == 2) {
                if (!isAnsweredToUser)
                    callItem.setIsSelf(true);
            } else {
                callItem.setIsSelf(false);
            }
            callItem.setCallerName(receiverName);
            callItem.setCallDuration("00:00");
            if (callStatus != null && callStatus.equals("5"))
                callItem.setCallCount(1);
//            callItem = CallMessage.getCallCount(to, from, callItem);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
            return null;
        }
        return callItem;

    }
}
/*
public class IncomingMessage {
    private static final String TAG = "IncomingMessage";
    private Context mContext;

    private String mCurrentUserId;
    private Session session;
    private Getcontactname getcontactname;
    private MessageService msgSvcCallback;


    public IncomingMessage(Context context) {
        this.mContext = context;
        session = new Session(mContext);
        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        getcontactname = new Getcontactname(mContext);
    }

    public MessageItemChat loadSingleStatusMessage(StatusModel objects) {
        MessageItemChat item = new MessageItemChat();
        String duration = "";


        MyLog.d("loadSingleStatusMessage", objects.toString());
        try {

            String payLoad = objects.getTextstatus_caption();
            String id = objects.getId();
            String from = objects.getUserId();
            item.setStatusReply(false);


            if ("0".equalsIgnoreCase(MessageFactory.text + "")) {
                long messagelength = session.getreceviedmessagelength() + payLoad.length();
                session.putreceivemessagelength(messagelength);
                int receviedmesagecount = session.getreceviedmessagecount();
                receviedmesagecount = receviedmesagecount + 1;
                session.putreceivemessagecount(receviedmesagecount);
                //System.out.println("receviedmesagecount" + receviedmesagecount);
            }


           *//*     String name = objects.getString("Name");
                String dataSize = objects.getString("filesize");
                String convId = objects.getString("convId");
                String recordId = objects.getString("recordId");
//            String starredStatus = objects.getString("isStar");
                String ts = objects.getString("timestamp");
*//*
 *//*  JSONObject linkObj;
                try {
                    linkObj = objects.getJSONObject("link_details");
                } catch (JSONException e) {
                    linkObj = new JSONObject();
                }*//*
//            JSONObject linkObj = objects.getJSONObject("link_details");

              *//*  String webLink = "";
                if (linkObj.has("url")) {
                    webLink = linkObj.getString("url");
                }

                String webLinkTitle = "";
                if (linkObj.has("title")) {
                    webLinkTitle = linkObj.getString("title");
                }

                String webLinkDesc = "";
                if (linkObj.has("description")) {
                    webLinkDesc = linkObj.getString("description");
                }

                String webLinkImgUrl = "";
                if (linkObj.has("image")) {
                    webLinkImgUrl = linkObj.getString("image");
                }

                String webLinkImgThumb = "";
                if (linkObj.has("thumbnail_data")) {
                    webLinkImgThumb = linkObj.getString("thumbnail_data");
                }
*//*
 *//*try {
                    if (objects.has("ContactMsisdn")) {
                        String senderMsisdn = objects.getString("ContactMsisdn");
                        String phContactName = senderMsisdn;
                        phContactName = getcontactname.getSendername(from, senderMsisdn);
                        item.setSenderName(phContactName);
                        item.setSenderMsisdn(senderMsisdn);
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }*//*

            item.setIsSelf(false);
            item.setTS(String.valueOf(objects.getTimeUploaded()));
            //  item.setConvId(convId);
            //  item.setRecordId(recordId);
            item.setStarredStatus(MessageFactory.MESSAGE_UN_STARRED);

            item.setMessageType("0");
            item.sethasNewMessage(true);
            if (payLoad != null && payLoad.length() > 0)
                item.setTextMessage(payLoad);

            item.setCount(1);

            if ("0".equalsIgnoreCase("" + MessageFactory.text)) {
                item.setTextMessage(payLoad);
            }

            String docId = mCurrentUserId + "-" + from;
            item.setMessageId(docId + "-" + id);
            item.setReceiverID(from);
            item.setFileBufferAt(0);
            item.setUploadDownloadProgress(0);
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_NOT_START);


        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

        return item;
    }


    public MessageItemChat loadGroupMessage(JSONObject objects) {
        try {
            MyLog.d(TAG, "loadGroupMessage" + objects.toString());

            String to = null;
            Integer type = Integer.parseInt(objects.getString("type"));
            String id = objects.getString("id");
            String from = objects.getString("from");
            if (objects.has("to")) {
                to = objects.getString("to");
            }

            //      String to = objects.getString("to");
*//*
            String toDocId = (String) objects.get("toDocId");
            String[] splitDocId = toDocId.split("-");
            String msgId = splitDocId[3];
          *//*
            if (objects.has("to")) {
                to = objects.getString("to");
            }
            String toDocId = (String) objects.get("toDocId");
            String[] splitDocId = toDocId.split("-");
            String msgId = splitDocId[3];
            if (!objects.has("to")) {
                to = splitDocId[1];
            }
            String name = objects.getString("msisdn");
//            String convId = objects.getString("convId");
            String recordId = objects.getString("recordId");
            String starredStatus = objects.getString("isStar");
            String ts = objects.getString("timestamp");
            if (session.getarchivecountgroup() != 0) {
                if (session.getarchivegroup(mCurrentUserId + "-" + to + "-g"))
                    session.removearchivegroup(mCurrentUserId + "-" + to + "-g");
            }
            MessageItemChat item = new MessageItemChat();
//            item.setConvId(convId);
            item.setRecordId(recordId);
            item.set_id(id);
            item.setStarredStatus(starredStatus);
            item.setTS(ts);
            item.setGroupMsgFrom(from);
            item.setSenderName(name);
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_NOT_START);
            if (objects.has("payload")) {
                String payLoad = objects.getString("payload");
                item.setTextMessage(payLoad);
            }
            if (type == MessageFactory.picture || type == MessageFactory.audio || type == MessageFactory.video
                    || type == MessageFactory.group_document_message) {
                long filesize = AppUtils.parseLongForFile(objects.getString("filesize"));
                filesize = session.getreceviedmedialength() + filesize;
                session.putreceivemedialength(filesize);
            }
            if (objects.has("replyDetails")) {
                JSONObject aReplyObject = objects.getJSONObject("replyDetails");
                item.setReplyMsisdn(aReplyObject.getString("From_msisdn"));
                item.setReplyFrom(aReplyObject.getString("from"));
                String userID = aReplyObject.getString("from");
                String replyname = aReplyObject.getString("From_msisdn");
                item.setReplyType(aReplyObject.getString("type"));
                Log.e(TAG,"loadGroupMessage setReplyType"+aReplyObject.getString("type"));
                if (aReplyObject.has("message")) {
                    item.setReplyMessage(aReplyObject.getString("message"));
                }
                if (aReplyObject.has("thumbnail_data")) {
                    String thumbnail_data = aReplyObject.getString("thumbnail_data");
                    item.setreplyimagebase64(thumbnail_data);
                }
                item.setReplyId(aReplyObject.getString("_id"));

                replyname = getcontactname.getSendername(userID, replyname);

                if (item.getReplyFrom().equalsIgnoreCase(mCurrentUserId)) {
                    item.setReplySender("you");
                } else {
                    item.setReplySender(replyname);
                }
                if (aReplyObject.has("link_details")) {
                    try {
                        JSONObject replyLinkObj = aReplyObject.getJSONObject("link_details");
                        String replyLocTitle = replyLinkObj.getString("title");
                        String replyLocThumbData = replyLinkObj.getString("thumbnail_data");

                        item.setReplyMessage(replyLocTitle);
                        item.setreplyimagebase64(replyLocThumbData);
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                }
            }
            item.setMessageType("" + type);
            String strType = "" + type;
            if (strType.equalsIgnoreCase("" + MessageFactory.text)) {
                String payLoad = objects.getString("payload");
                long messagelength = session.getreceviedmessagelength() + payLoad.length();
                session.putreceivemessagelength(messagelength);
                int receviedmesagecount = session.getreceviedmessagecount();
                receviedmesagecount = receviedmesagecount + 1;
                session.putreceivemessagecount(receviedmesagecount);

            } else if (strType.equalsIgnoreCase("" + MessageFactory.contact)) {
                String contactName = objects.getString("contact_name");
                String contactNumber = "", contactDetails = "";
                if (objects.has("createdTomsisdn")) {
                    contactNumber = objects.getString("createdTomsisdn");
                    item.setContactNumber(contactNumber);
                }
                if (objects.has("contact_details")) {
                    contactDetails = objects.getString("contact_details");
                    item.setDetailedContacts(contactDetails);
                }

                if (objects.has("createdTo")) {
                    String contactSmarttyId = objects.getString("createdTo");
                    item.setcontactSmarttyId(contactSmarttyId);
                }


                if (objects.has("createdTo")) {
                    String contactSmarttyId = objects.getString("createdTo");
                    item.setcontactSmarttyId(contactSmarttyId);
                }
                item.setContactName(contactName);

            } else if (strType.equalsIgnoreCase("" + MessageFactory.picture)) {
                String thumbnail = objects.getString("thumbnail");
                String fileSize = objects.getString("filesize");
                String thumbData = objects.getString("thumbnail_data");

                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.IMAGE_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.picture, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.IMAGE_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                }

                item.setChatFileServerPath(thumbnail);
                item.setFileBufferAt(0);
                item.setUploadDownloadProgress(0);

                item.setThumbnailData(thumbData);
                item.setFileSize(fileSize);
            } else if (strType.equalsIgnoreCase("" + MessageFactory.group_document_message)) {
                item.setMessageType("" + MessageFactory.document);
                String thumbnail = objects.getString("thumbnail");
                String fileSize = objects.getString("filesize");
                String originalFileName = objects.getString("original_filename");
                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.DOCUMENT_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.document, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.DOCUMENT_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                    item.setTextMessage(originalFileName);
                    item.setThumbnailData(thumbnail);
                }

                item.setChatFileServerPath(thumbnail);
                item.setFileSize(fileSize);

            } else if (strType.equalsIgnoreCase("" + MessageFactory.audio)) {
                String thumbnail = objects.getString("thumbnail");
                String fileSize = objects.getString("filesize");

                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.AUDIO_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.audio, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.AUDIO_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                }

                String duration = objects.getString("duration");
                item.setChatFileServerPath(thumbnail);
                item.setFileSize(fileSize);
                item.setDuration(duration);
            } else if (strType.equalsIgnoreCase("" + MessageFactory.video)) {
                String thumbnail = objects.getString("thumbnail");
                String fileSize = objects.getString("filesize");

                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.VIDEO_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.video, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.VIDEO_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                }

                item.setChatFileServerPath(thumbnail);

                String thumbData = objects.getString("thumbnail_data");
                String duration = objects.getString("duration");
                item.setDuration(duration);
                item.setThumbnailData(thumbData);
                item.setFileSize(fileSize);
            }
            if (objects.has("link_details") && !strType.equalsIgnoreCase("" + MessageFactory.group_document_message)) {
                if (objects.has("payload")) {
                    String payLoad = objects.getString("payload");
                }
                JSONObject linkObj = objects.getJSONObject("link_details");

                String recLink = "";
                if (linkObj.has("url")) {
                    recLink = linkObj.getString("url");
                }

                String recLinkTitle = "";
                if (linkObj.has("title")) {
                    recLinkTitle = linkObj.getString("title");
                }

                String recLinkDesc = "";
                if (linkObj.has("description")) {
                    recLinkDesc = linkObj.getString("description");
                }

                String recLinkImgUrl = "";
                if (linkObj.has("image")) {
                    recLinkImgUrl = linkObj.getString("image");
                }

                String recLinkImgThumb = "";
                if (linkObj.has("thumbnail_data")) {
                    recLinkImgThumb = linkObj.getString("thumbnail_data");
                }


                item.setWebLink(recLink);
                item.setWebLinkTitle(recLinkTitle);
                item.setWebLinkDesc(recLinkDesc);
                item.setWebLinkImgUrl(recLinkImgUrl);
                item.setWebLinkImgThumb(recLinkImgThumb);
            }

            if (objects.has("is_tag_applied")) {
                String istagapply = objects.getString("is_tag_applied");
                if (istagapply.equalsIgnoreCase("1")) {
                    if (objects.has("payload")) {
                        String payLoads = objects.getString("payload");
                        String newPayLoad = AppUtils.getTaggedMsgs(mCurrentUserId, payLoads, mContext);
                        item.setTextMessage(newPayLoad);
                    }
                } else {
                    if (objects.has("payload")) {
                        String payLoads = objects.getString("payload");

                        if (payLoads.length() > 0) {

                            item.setTextMessage(payLoads);
                        }


                    }
                }
            }
            item.setCount(1);

            // For showing Group name in chat list
            String groupName = objects.getString("groupName");
            item.setGroupName(groupName);

            item.sethasNewMessage(true);
            String docId;

            if (from.equalsIgnoreCase(mCurrentUserId)) {
                docId = from + "-" + to + "-g";
                item.setIsSelf(true);
                item.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_SENT);
            } else *//*if (to.equalsIgnoreCase(uniqueCurrentID))*//* {
                item.setIsSelf(false);
                item.sethasNewMessage(true);
                docId = mCurrentUserId + "-" + to + "-g";
            }
            item.setReceiverID(to);
            item.setMessageId(docId.concat("-").concat(msgId));

            GroupInfoSession groupInfoSession = new GroupInfoSession(mContext);
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);

            if (infoPojo != null) {
                String[] groupMembers = infoPojo.getGroupMembers().split(",");
                try {
                    JSONArray arrMembers = new JSONArray();
                    for (String member : groupMembers) {
                        JSONObject userObj = new JSONObject();
                        userObj.put("UserId", member);
                        userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_SENT);
                        if (from.equals(member)) {
                            userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_READ);
                        }
                        userObj.put("DeliverTime", "");
                        userObj.put("ReadTime", "");
                        arrMembers.put(userObj);
                    }
                    JSONObject deliverObj = new JSONObject();
                    deliverObj.put("GroupMessageStatus", arrMembers);
                    item.setGroupMsgDeliverStatus(deliverObj.toString());
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }

            if (objects.has("replyId") && !objects.has("replyDetails")) {
                String replyRecordId = objects.getString("replyId");
                if (msgSvcCallback != null) {
                    MessageDbController db = CoreController.getDBInstance(mContext);
                    MessageItemChat replyMsgItem = db.getMessageByRecordId(replyRecordId);
                    if (replyMsgItem == null) {
                        msgSvcCallback.getReplyMessageDetails(to, to, replyRecordId, MessageFactory.CHAT_TYPE_GROUP,
                                "no", item.getMessageId());
                    } else {
                        item = getReplyDetailsByMessageItem(replyMsgItem, item);
                    }
                }
            }

            return item;

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
            return null;
        }
    }

    public MessageItemChat loadSingleMessageFromWeb(JSONObject objects) {

        try {
            MyLog.d(TAG, "loadSingleMessageFromWeb" + objects.toString());

            String id = objects.getString("id");
            int delivery = (int) objects.get("deliver");

            JSONObject msgData = objects.getJSONObject("data");
            String secretType = msgData.getString("secret_type");
            String chat_id = (String) objects.get("doc_id");
            String[] ids = chat_id.split("-");
            String doc_id = ids[0] + "-" + ids[1];

            String recordId = msgData.getString("recordId");
            String convId = msgData.getString("convId");
            String fromUserId = msgData.getString("from");
            String toUserId = msgData.getString("to");

            String toMsisdn = msgData.getString("To_msisdn");
            String ts = msgData.getString("timestamp");

            MessageItemChat newMsgItem = new MessageItemChat();
            boolean isSecretTimerMsg = false;
            if (secretType.equalsIgnoreCase("yes")) {

                String secretMsgTimer = msgData.getString("incognito_timer");
                newMsgItem.setSecretTimer(secretMsgTimer);
                newMsgItem.setSecretTimeCreatedBy(mCurrentUserId);

                if (msgData.getString("type").equals(MessageFactory.timer_change + "")) {
                    isSecretTimerMsg = true;
                }
            }

            if (isSecretTimerMsg) {
                MessageItemChat timerMsgItem = getTimerChangeMessage(msgData, newMsgItem);
                return timerMsgItem;
            } else {
                newMsgItem.setIsSelf(true);
                newMsgItem.setStarredStatus(MessageFactory.MESSAGE_UN_STARRED);
                newMsgItem.setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);
                newMsgItem.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_NOT_START);
                newMsgItem.setDeliveryStatus("" + delivery);
                newMsgItem.setReceiverUid(toUserId);
                newMsgItem.setTS(ts);
                newMsgItem.setSenderMsisdn(toMsisdn);
                newMsgItem.setMessageId(chat_id);
                newMsgItem.setConvId(convId);
                newMsgItem.setRecordId(recordId);
                newMsgItem.setReceiverID(toUserId);
                newMsgItem.setSenderName(toMsisdn);
                newMsgItem.setMsgSentAt(ts);
                String savedName = getcontactname.getSendername(toUserId, toMsisdn);
                newMsgItem.setSenderName(savedName);

                int type = Integer.parseInt(msgData.getString("type"));
                newMsgItem.setMessageType("" + type);
                switch (type) {

                    case MessageFactory.text:
                        String textMsg = msgData.getString("message");
                        newMsgItem.setTextMessage(textMsg);
                        break;

                    case MessageFactory.picture:
                        String caption = msgData.getString("message");
                        String thumbnail = msgData.getString("thumbnail");
                        String thumbData = msgData.getString("thumbnail_data");
                        String locFileId = msgData.getString("id");
                        String fileSize = msgData.getString("filesize");
                        String width = msgData.getString("width");
                        String height = msgData.getString("height");
                        String localFilePath = MessageFactory.IMAGE_STORAGE_PATH + MessageFactory.getMessageFileName(
                                MessageFactory.picture, locFileId, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));

                        newMsgItem.setTextMessage(caption);
                        newMsgItem.setChatFileServerPath(thumbnail);
                        newMsgItem.setThumbnailData(thumbData);
                        newMsgItem.setFileSize(fileSize);
                        newMsgItem.setChatFileWidth(width);
                        newMsgItem.setChatFileHeight(height);
                        newMsgItem.setChatFileLocalPath(localFilePath);
                        break;

                    case MessageFactory.contact:
                        String contactName = msgData.getString("contact_name");
                        String contactNumber = msgData.getString("createdTomsisdn");
                        String contactDetails = msgData.getString("contact_details");

                        if (msgData.has("createdTo")) {
                            String contactSmarttyId = msgData.getString("createdTo");
                            newMsgItem.setcontactSmarttyId(contactSmarttyId);
                        }
                        newMsgItem.setContactName(contactName);
                        newMsgItem.setContactNumber(contactNumber);
                        newMsgItem.setDetailedContacts(contactDetails);
                        break;

                    case MessageFactory.web_link:
                        String linkMsg = msgData.getString("message");
                        newMsgItem.setTextMessage(linkMsg);
                        break;

                    case MessageFactory.document:
                        String originalFileName = msgData.getString("original_filename");
                        String docPath = msgData.getString("thumbnail");
                        String thumbnailData = msgData.getString("thumbnail_data");
//                    String docMsg = msgData.getString("message");
                        String dataSize = msgData.getString("filesize");
                        String locDocFileId = msgData.getString("id");

                        if (docPath != null && !docPath.equals("")) {
                            File dir = new File(MessageFactory.DOCUMENT_STORAGE_PATH);
                            if (!dir.exists()) {
                                dir.mkdir();
                            }
                            String localFileName = MessageFactory.getMessageFileName(
                                    MessageFactory.document, locDocFileId, FileUploadDownloadManager.getFileExtnFromPath(docPath));
                            String filePath = MessageFactory.DOCUMENT_STORAGE_PATH + localFileName;
                            newMsgItem.setChatFileLocalPath(filePath);
                            newMsgItem.setTextMessage(originalFileName);
                        }

                        newMsgItem.setChatFileServerPath(docPath);
                        newMsgItem.setFileBufferAt(0);
                        newMsgItem.setUploadDownloadProgress(0);
                        newMsgItem.setFileSize(dataSize);
                        newMsgItem.setThumbnailData(thumbnailData);
                        break;

                    case MessageFactory.video:
                        String videoPath = msgData.getString("thumbnail");
                        String videoSize = msgData.getString("filesize");
                        String locVideoFileId = msgData.getString("id");
                        if (videoPath != null && !videoPath.equals("")) {
                            File dir = new File(MessageFactory.VIDEO_STORAGE_PATH);
                            if (!dir.exists()) {
                                dir.mkdir();
                            }
                            String localFileName = MessageFactory.getMessageFileName(
                                    MessageFactory.video, locVideoFileId, FileUploadDownloadManager.getFileExtnFromPath(videoPath));
                            String filePath = MessageFactory.VIDEO_STORAGE_PATH + localFileName;
                            newMsgItem.setChatFileLocalPath(filePath);
                        }
                        newMsgItem.setChatFileServerPath(videoPath);
                        String videoThumbData = objects.getString("thumbnail_data");
                        String duration = objects.getString("duration");
                        newMsgItem.setDuration(duration);
                        newMsgItem.setThumbnailData(videoThumbData);
                        newMsgItem.setFileSize(videoSize);
                        break;
                }

                if (msgData.has("replyDetails")) {
                    JSONObject aReplyObject = msgData.getJSONObject("replyDetails");
                    newMsgItem.setReplyMsisdn(aReplyObject.getString("From_msisdn"));
                    String replyname = aReplyObject.getString("From_msisdn");
                    newMsgItem.setReplyFrom(aReplyObject.getString("from"));
                    String userId = aReplyObject.getString("from");
                    newMsgItem.setReplyType(aReplyObject.getString("type"));
                    Log.e(TAG,"setReplyType loadSingleMessageFromWeb"+aReplyObject.getString("type"));

                    if (aReplyObject.has("message")) {
                        newMsgItem.setReplyMessage(aReplyObject.getString("message"));
                    }
                    newMsgItem.setReplyId(aReplyObject.getString("_id"));
                    newMsgItem.setReplyServerLoad(aReplyObject.getString("server_load"));
                    if (aReplyObject.has("thumbnail_data")) {
                        String thumbnail_data = aReplyObject.getString("thumbnail_data");
                        newMsgItem.setreplyimagebase64(thumbnail_data);
                    }

                    replyname = getcontactname.getSendername(userId, replyname);

                    if (newMsgItem.getReplyFrom().equalsIgnoreCase(mCurrentUserId)) {
                        newMsgItem.setReplySender("you");
                    } else {
                        newMsgItem.setReplySender(replyname);
                    }

                    if (aReplyObject.has("link_details")) {
                        try {
                            JSONObject replyLinkObj = aReplyObject.getJSONObject("link_details");
                            String replyLocTitle = replyLinkObj.getString("title");
                            String replyLocThumbData = replyLinkObj.getString("thumbnail_data");
                            newMsgItem.setReplyMessage(replyLocTitle);
                            newMsgItem.setreplyimagebase64(replyLocThumbData);

                        } catch (JSONException e) {
                            MyLog.e(TAG, "", e);
                        }
                    }
                }

                // This block for Location and weblink messages
                if (msgData.has("link_details")) {
                    JSONObject linkObj = msgData.getJSONObject("link_details");

                    String recLink = "";
                    if (linkObj.has("url")) {
                        recLink = linkObj.getString("url");
                    }

                    String recLinkTitle = "";
                    if (linkObj.has("title")) {
                        recLinkTitle = linkObj.getString("title");
                    }

                    String recLinkDesc = "";
                    if (linkObj.has("description")) {
                        recLinkDesc = linkObj.getString("description");
                    }

                    String recLinkImgUrl = "";
                    if (linkObj.has("image")) {
                        recLinkImgUrl = linkObj.getString("image");
                    }

                    String recLinkImgThumb = "";
                    if (linkObj.has("thumbnail_data")) {
                        recLinkImgThumb = linkObj.getString("thumbnail_data");
                    }

                    newMsgItem.setWebLink(recLink);
                    newMsgItem.setWebLinkTitle(recLinkTitle);
                    newMsgItem.setWebLinkDesc(recLinkDesc);
                    newMsgItem.setWebLinkImgUrl(recLinkImgUrl);
                    newMsgItem.setWebLinkImgThumb(recLinkImgThumb);
                }

                return newMsgItem;
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        return null;

    }

    private MessageItemChat getTimerChangeMessage(JSONObject msgData, MessageItemChat newMsgItem) {
        try {
            MyLog.d(TAG, "getTimerChangeMessage" + msgData.toString());

            String from = msgData.getString("from");
            String to = msgData.getString("to");
            String convId = msgData.getString("convId");
            String recordId = msgData.getString("recordId");
            String timer = msgData.getString("incognito_timer");
            String timerMode = msgData.getString("incognito_timer_mode");
            String toUserMsgId;
            if (msgData.has("doc_id")) {
                toUserMsgId = msgData.getString("doc_id");
            } else {
                toUserMsgId = msgData.getString("docId");
            }
            String msgId = msgData.getString("id");
            String timeStamp = msgData.getString("timestamp");
            String fromMsisdn = msgData.getString("ContactMsisdn");

            MessageItemChat item = new MessageItemChat();
            String docId;
            if (from.equalsIgnoreCase(mCurrentUserId)) {
                docId = from + "-" + to;
                item.setReceiverID(to);
            } else {
                docId = to + "-" + from;
                item.setReceiverID(from);
            }

            item.setIsSelf(false);
            item.setIsDate(true);
            item.setConvId(convId);
            item.setRecordId(recordId);
            item.setSecretTimer(timer);
            item.setSecretTimeCreatedBy(from);
            item.setSecretTimerMode(timerMode);
            item.setMessageId(docId + "-" + msgId);
            item.setTS(timeStamp);
            item.setSenderMsisdn(fromMsisdn);
            item.setMessageType(MessageFactory.timer_change + "");
            item.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_DELIVERED);

            MessageDbController db = CoreController.getDBInstance(mContext);
            docId = docId + "-" + MessageFactory.CHAT_TYPE_SECRET;
            db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SECRET);

            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
            contactDB_sqlite.updateSecretMessageTimer(to, timer, from, item.getMessageId());

            return item;

        } catch (JSONException e) {
            return null;
        }
    }


    public MessageItemChat loadGroupMessageFromWeb(JSONObject objects) {
        try {
            MyLog.d(TAG, "loadGroupMessageFromWeb" + objects.toString());

            Integer type = Integer.parseInt(objects.getString("type"));

            String id = objects.getString("id");
            String from = objects.getString("from");
            String to = objects.getString("groupId");

            String toDocId = (String) objects.get("toDocId");
            String[] splitDocId = toDocId.split("-");
            String msgId = splitDocId[3];
            String name = objects.getString("msisdn");
//            String convId = objects.getString("convId");
            String recordId = objects.getString("recordId");
            String starredStatus = objects.getString("isStar");

            String ts = objects.getString("timestamp");
            if (session.getarchivecountgroup() != 0) {
                if (session.getarchivegroup(mCurrentUserId + "-" + to + "-g"))
                    session.removearchivegroup(mCurrentUserId + "-" + to + "-g");
            }

            MessageItemChat item = new MessageItemChat();
//            item.setConvId(convId);
            item.setRecordId(recordId);
            item.set_id(id);
            item.setStarredStatus(starredStatus);
            item.setTS(ts);
            item.setGroupMsgFrom(from);
            item.setSenderName(name);
            item.setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);
            item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_NOT_START);
            item.setMessageId(toDocId);
            String payLoad = objects.getString("payload");
            item.setTextMessage(payLoad);
            if (type == MessageFactory.picture || type == MessageFactory.audio || type == MessageFactory.video) {
                long filesize = AppUtils.parseLongForFile(objects.getString("filesize"));
                filesize = session.getreceviedmedialength() + filesize;
                session.putreceivemedialength(filesize);
            }

            if (objects.has("replyDetails")) {
                JSONObject aReplyObject = objects.getJSONObject("replyDetails");
                item.setReplyMsisdn(aReplyObject.getString("From_msisdn"));
                item.setReplyFrom(aReplyObject.getString("from"));
                String userID = aReplyObject.getString("from");
                String replyname = aReplyObject.getString("From_msisdn");
                item.setReplyType(aReplyObject.getString("type"));
                if (aReplyObject.has("message")) {
                    item.setReplyMessage(aReplyObject.getString("message"));
                }
                if (aReplyObject.has("thumbnail_data")) {
                    String thumbnail_data = aReplyObject.getString("thumbnail_data");
                    item.setreplyimagebase64(thumbnail_data);
                }
                item.setReplyId(aReplyObject.getString("_id"));

                replyname = getcontactname.getSendername(userID, replyname);

                if (item.getReplyFrom().equalsIgnoreCase(mCurrentUserId)) {
                    item.setReplySender("you");
                } else {
                    item.setReplySender(replyname);
                }

                if (aReplyObject.has("link_details")) {
                    try {
                        JSONObject replyLinkObj = aReplyObject.getJSONObject("link_details");
                        String replyLocTitle = replyLinkObj.getString("title");
                        String replyLocThumbData = replyLinkObj.getString("thumbnail_data");

                        item.setReplyMessage(replyLocTitle);
                        item.setreplyimagebase64(replyLocThumbData);
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                }
            }
            item.setMessageType("" + type);
            String strType = "" + type;
            if (strType.equalsIgnoreCase("" + MessageFactory.text)) {
                long messagelength = session.getreceviedmessagelength() + payLoad.length();
                session.putreceivemessagelength(messagelength);
                int receviedmesagecount = session.getreceviedmessagecount();
                receviedmesagecount = receviedmesagecount + 1;
                session.putreceivemessagecount(receviedmesagecount);

            } else if (strType.equalsIgnoreCase("" + MessageFactory.contact)) {
                String contactName = objects.getString("contact_name");
                String contactNumber = objects.getString("createdTomsisdn");
                String contactDetails = objects.getString("contact_details");
                if (objects.has("createdTo")) {
                    String contactSmarttyId = objects.getString("createdTo");
                    item.setcontactSmarttyId(contactSmarttyId);
                }
                item.setContactName(contactName);
                item.setContactNumber(contactNumber);
                item.setDetailedContacts(contactDetails);
            } else if (strType.equalsIgnoreCase("" + MessageFactory.picture)) {
                String thumbnail = objects.getString("thumbnail");
                String fileSize = objects.getString("filesize");
                String thumbData = objects.getString("thumbnail_data");

                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.IMAGE_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.picture, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.IMAGE_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                }

                item.setChatFileServerPath(thumbnail);
                item.setFileBufferAt(0);
                item.setUploadDownloadProgress(0);

                item.setThumbnailData(thumbData);
                item.setFileSize(fileSize);
            } else if (strType.equalsIgnoreCase("" + MessageFactory.group_document_message)) {
                item.setMessageType("" + MessageFactory.document);

                String thumbnail = objects.getString("thumbnail");
                String thumbnailData = objects.getString("thumbnail_data");
                String originalFileName = objects.getString("original_filename");
                String fileSize = objects.getString("filesize");

                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.DOCUMENT_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.document, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.DOCUMENT_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                    item.setThumbnailData(thumbnailData);
                    item.setTextMessage(originalFileName);
                }

                item.setChatFileServerPath(thumbnail);
                item.setFileSize(fileSize);
            } else if (strType.equalsIgnoreCase("" + MessageFactory.audio)) {
                String thumbnail = objects.getString("thumbnail");
                String fileSize = objects.getString("filesize");

                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.AUDIO_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.audio, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.AUDIO_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                }

                String duration = objects.getString("duration");
                item.setChatFileServerPath(thumbnail);
                item.setFileSize(fileSize);
                item.setDuration(duration);
            } else if (strType.equalsIgnoreCase("" + MessageFactory.video)) {
                String thumbnail = objects.getString("thumbnail");
                String fileSize = objects.getString("filesize");

                if (thumbnail != null && !thumbnail.equals("")) {
                    File dir = new File(MessageFactory.VIDEO_STORAGE_PATH);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    String localFileName = MessageFactory.getMessageFileName(
                            MessageFactory.video, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                    String filePath = MessageFactory.VIDEO_STORAGE_PATH + localFileName;
                    item.setChatFileLocalPath(filePath);
                }

                item.setChatFileServerPath(thumbnail);

                String thumbData = objects.getString("thumbnail_data");
                String duration = objects.getString("duration");
                item.setDuration(duration);
                item.setThumbnailData(thumbData);
                item.setFileSize(fileSize);
            }
            if (objects.has("link_details")) {
                JSONObject linkObj = objects.getJSONObject("link_details");

                String recLink = "";
                if (linkObj.has("url")) {
                    recLink = linkObj.getString("url");
                }

                String recLinkTitle = "";
                if (linkObj.has("title")) {
                    recLinkTitle = linkObj.getString("title");
                }

                String recLinkDesc = "";
                if (linkObj.has("description")) {
                    recLinkDesc = linkObj.getString("description");
                }

                String recLinkImgUrl = "";
                if (linkObj.has("image")) {
                    recLinkImgUrl = linkObj.getString("image");
                }

                String recLinkImgThumb = "";
                if (linkObj.has("thumbnail_data")) {
                    recLinkImgThumb = linkObj.getString("thumbnail_data");
                }


                item.setWebLink(recLink);
                item.setWebLinkTitle(recLinkTitle);
                item.setWebLinkDesc(recLinkDesc);
                item.setWebLinkImgUrl(recLinkImgUrl);
                item.setWebLinkImgThumb(recLinkImgThumb);
            }
            item.setCount(1);

            // For showing Group name in chat list
            String groupName = objects.getString("groupName");
            item.setGroupName(groupName);

            item.sethasNewMessage(true);
            String docId;

            if (from.equalsIgnoreCase(mCurrentUserId)) {
                docId = from + "-" + to + "-g";
                item.setIsSelf(true);
                item.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_SENT);
            } else *//*if (to.equalsIgnoreCase(uniqueCurrentID))*//* {
                item.setIsSelf(false);
                item.sethasNewMessage(true);
                docId = mCurrentUserId + "-" + to + "-g";
            }
            item.setReceiverID(to);
            item.setMessageId(docId.concat("-").concat(msgId));

            GroupInfoSession groupInfoSession = new GroupInfoSession(mContext);
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);

            if (infoPojo != null) {
                String[] groupMembers = infoPojo.getGroupMembers().split(",");
                try {
                    JSONArray arrMembers = new JSONArray();
                    for (String member : groupMembers) {
                        JSONObject userObj = new JSONObject();
                        userObj.put("UserId", member);
                        userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_SENT);
                        if (from.equals(member)) {
                            userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_READ);
                        }
                        userObj.put("DeliverTime", "");
                        userObj.put("ReadTime", "");
                        arrMembers.put(userObj);
                    }
                    JSONObject deliverObj = new JSONObject();
                    deliverObj.put("GroupMessageStatus", arrMembers);
                    item.setGroupMsgDeliverStatus(deliverObj.toString());
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }

//            JSONArray arrMsgStatus = objects.getJSONArray("status");

            return item;

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
            return null;
        }
    }

    public MessageItemChat loadSingleMessage(JSONObject objects) {
        MessageItemChat item = new MessageItemChat();
        String duration = "";


        MyLog.d(TAG, "loadSingleMessage" + objects.toString());
        try {
            String type = objects.getString("type");
            String convId ="";
            if (type.equals(MessageFactory.missed_call + "")) {
                item = getMissedCallMessage(objects);
            } else {
                String payLoad = "";
                if (objects.has("payload")) {
                    payLoad = objects.getString("payload");
                }
                String id = objects.getString("msgId");
                String from = objects.getString("from");
                if (objects.has("reply_type") && objects.getString("reply_type").equals("status")) {
                    item.setStatusReply(true);
                }

                if (type.equalsIgnoreCase(MessageFactory.text + "")) {
                    long messagelength = session.getreceviedmessagelength() + payLoad.length();
                    session.putreceivemessagelength(messagelength);
                    int receviedmesagecount = session.getreceviedmessagecount();
                    receviedmesagecount = receviedmesagecount + 1;
                    session.putreceivemessagecount(receviedmesagecount);
                    //System.out.println("receviedmesagecount" + receviedmesagecount);
                } else if (type.equalsIgnoreCase(MessageFactory.picture + "") || type.equalsIgnoreCase(MessageFactory.audio + "")
                        || type.equalsIgnoreCase(MessageFactory.video + "") || type.equalsIgnoreCase(MessageFactory.document + "")) {
                    long filesize = AppUtils.parseLongForFile(objects.getString("filesize"));
                    filesize = session.getreceviedmedialength() + filesize;
                    session.putreceivemedialength(filesize);
                }
                String thumbnail = "";
                if (objects.has("thumbnail")) {
                    thumbnail = objects.getString("thumbnail");
                }

                String name = objects.optString("Name");
                String dataSize = "";
                if (objects.has("filesize")) {
                    dataSize = objects.getString("filesize");
                }

                if (objects.has("convId")) {
                     convId = objects.getString("convId");
                    item.setConvId(convId);

                }else {
                    item.setConvId(convId);

                }
                String recordId = objects.getString("recordId");
//            String starredStatus = objects.getString("isStar");
                String ts = objects.getString("timestamp");

                JSONObject linkObj;
                if (objects.has("link_details")) {
                    try {
                        linkObj = objects.getJSONObject("link_details");
                    } catch (JSONException e) {
                        linkObj = new JSONObject();
                    }

                    String webLink = "";
                    if (linkObj.has("url")) {
                        webLink = linkObj.getString("url");
                    }


                    String webLinkTitle = "";
                    if (linkObj.has("title")) {
                        webLinkTitle = linkObj.getString("title");
                    }


                    String webLinkDesc = "";
                    if (linkObj.has("description")) {
                        webLinkDesc = linkObj.getString("description");
                    }

                    String webLinkImgUrl = "";
                    if (linkObj.has("image")) {
                        webLinkImgUrl = linkObj.getString("image");
                    }

                    String webLinkImgThumb = "";
                    if (linkObj.has("thumbnail_data")) {
                        webLinkImgThumb = linkObj.getString("thumbnail_data");
                    }

                    item.setWebLink(webLink);
                    item.setWebLinkTitle(webLinkTitle);
                    item.setWebLinkDesc(webLinkDesc);
                    item.setWebLinkImgUrl(webLinkImgUrl);
                    item.setWebLinkImgThumb(webLinkImgThumb);

                }

//            JSONObject linkObj = objects.getJSONObject("link_details");





                try {
                    if (objects.has("ContactMsisdn")) {
                        String senderMsisdn = objects.getString("ContactMsisdn");
                        String phContactName = senderMsisdn;
                        phContactName = getcontactname.getSendername(from, senderMsisdn);
                        item.setSenderName(phContactName);
                        item.setSenderMsisdn(senderMsisdn);
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }

                item.setIsSelf(false);
                item.setTS(ts);
                item.setRecordId(recordId);
                item.setStarredStatus(MessageFactory.MESSAGE_UN_STARRED);
                if (objects.has("replyDetails")) {
                    JSONObject aReplyObject = objects.getJSONObject("replyDetails");
                    item = getReplyDetailsByObject(objects, aReplyObject, item);

                }
                item.setMessageType("" + type);
                item.sethasNewMessage(true);
                if (payLoad != null && payLoad.length() > 0)
                    item.setTextMessage(payLoad);

                item.setCount(1);

                if (type.equalsIgnoreCase("" + MessageFactory.text)) {
                    item.setTextMessage(payLoad);
                } else if (type.equalsIgnoreCase("" + MessageFactory.contact)) {
//                item.setContactInfo(payLoad);
                    String contactName = objects.getString("contact_name");
                    if (objects.has("createdTomsisdn")) {
                        String contactNumber = objects.getString("createdTomsisdn");
                        item.setContactNumber(contactNumber);
                    }
                    if (objects.has("contact_details")) {
                        String contactDetails = objects.getString("contact_details");
                        item.setDetailedContacts(contactDetails);
                    }
                    if (objects.has("createdTo")) {
                        String contactSmarttyId = objects.getString("createdTo");
                        item.setcontactSmarttyId(contactSmarttyId);
//                        String AvtarImage = Constants.userprofileurl + contactSmarttyId + ".jpg?id=" + Calendar.getInstance().getTimeInMillis();
//                        item.setAvatarImageUrl(AvtarImage);
                    }

                    item.setContactName(contactName);

                } else if (type.equalsIgnoreCase("" + MessageFactory.picture)) {

                    String width = objects.getString("width");
                    String height = objects.getString("height");

                    if (thumbnail != null && !thumbnail.equals("")) {
                        File dir = new File(Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES), MessageFactory.IMAGE_STORAGE_PATH);
                        if (!dir.exists()) {
                            dir.mkdir();
                        }
                        String localFileName = MessageFactory.getMessageFileName(
                                MessageFactory.picture, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                        String filePath = MessageFactory.IMAGE_STORAGE_PATH + localFileName;
                        item.setChatFileLocalPath(filePath);
                        item.setChatFileWidth(width);
                        item.setChatFileHeight(height);
                    }

                    item.setChatFileServerPath(thumbnail);
//Check thumbnail_data is available
if (objects.has("thumbnail_data")){
    String thumbData = objects.getString("thumbnail_data");
    item.setThumbnailData(thumbData);

}else  if (objects.has("data")){
    String thumbData = objects.getJSONObject("data").getString("thumbnail_data");
    item.setThumbnailData(thumbData);
}
                    item.setFileSize(dataSize);
                } else if (type.equalsIgnoreCase("" + MessageFactory.audio)) {
                    String audiotype = "";
                    if (thumbnail != null && !thumbnail.equals("")) {
                        File dir = new File(MessageFactory.AUDIO_STORAGE_PATH);
                        if (!dir.exists()) {
                            dir.mkdir();
                        }
                        if (objects.has("audio_type")) {
                            audiotype = objects.getString("audio_type");
                        }

                        if (audiotype != null && !audiotype.equalsIgnoreCase("")) {
                            item.setaudiotype(Integer.parseInt(audiotype));
                        }

                        String localFileName = MessageFactory.getMessageFileName(
                                MessageFactory.audio, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                        String filePath = MessageFactory.AUDIO_STORAGE_PATH + localFileName;
                        item.setChatFileLocalPath(filePath);
                    }

                    if (objects.has("duration")) {

                        duration = objects.getString("duration");
                    }


                    item.setDuration(duration);
                    item.setChatFileServerPath(thumbnail);
                    item.setFileSize(dataSize);
                } else if (type.equalsIgnoreCase("" + MessageFactory.document)) {
                    String thumbData = null;
                    String originalFileName = objects.getString("original_filename");
//                    item.setMessageType("" + MessageFactory.document);
                    if (thumbnail != null && !thumbnail.equals("")) {
                        File dir = new File(MessageFactory.DOCUMENT_STORAGE_PATH);
                        if (!dir.exists()) {
                            dir.mkdir();
                        }
                        String localFileName = MessageFactory.getMessageFileName(
                                MessageFactory.document, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                        String filePath = MessageFactory.DOCUMENT_STORAGE_PATH + localFileName;
                        item.setChatFileLocalPath(filePath);
                        item.setTextMessage(originalFileName);
                        item.setChatFileServerPath(thumbnail);
                        item.setFileSize(dataSize);
                        if (objects.has("thumbnail")) {
                            thumbData = objects.getString("thumbnail");
                        } else {
                            thumbData = objects.getString("thumbnail_data");
                        }

                        item.setThumbnailData(thumbData);
                    }
                } else if (type.equalsIgnoreCase("" + MessageFactory.video)) {
                    if (thumbnail != null && !thumbnail.equals("")) {
                        File dir = new File(MessageFactory.VIDEO_STORAGE_PATH);
                        if (!dir.exists()) {
                            dir.mkdir();
                        }
                        String localFileName = MessageFactory.getMessageFileName(
                                MessageFactory.video, id, FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
                        String filePath = MessageFactory.VIDEO_STORAGE_PATH + localFileName;
                        item.setChatFileLocalPath(filePath);
                    }

                    item.setChatFileServerPath(thumbnail);


                    if (objects.has("duration")) {

                        duration = objects.getString("duration");
                    }

                    item.setDuration(duration);

                    if (objects.has("thumbnail_data")) {
                        String thumbData = objects.getString("thumbnail_data");
                        item.setThumbnailData(thumbData);
                    } else if (objects.has("thumbnail")) {
                        String thumbData = objects.getString("thumbnail");
                        item.setThumbnailData(thumbData);
                    }
                    item.setFileSize(dataSize);
                }

                String docId = mCurrentUserId + "-" + from;
                item.setMessageId(docId + "-" + id);

                String secretType = objects.getString("secret_type");
                if (secretType.equalsIgnoreCase("yes")) {
                    docId = docId + "-" + MessageFactory.CHAT_TYPE_SECRET;
                }

                if (objects.has("replyId") && !objects.has("replyDetails")) {
                    String replyRecordId = objects.getString("replyId");
                    item.setReplyId(replyRecordId);

                    MessageDbController db = CoreController.getDBInstance(mContext);
                    MessageItemChat replyMsgItem = db.getMessageByRecordId(replyRecordId);
                    if (replyMsgItem == null) {
                        if (msgSvcCallback != null) {
                            msgSvcCallback.getReplyMessageDetails(from, convId, replyRecordId,
                                    MessageFactory.CHAT_TYPE_SINGLE, secretType, item.getMessageId());
                        }
                    } else {
                        item = getReplyDetailsByMessageItem(replyMsgItem, item);
                    }
                }
                item.setMsgId(id);
             *//*   item.setWebLink(webLink);
                item.setWebLinkTitle(webLinkTitle);
                item.setWebLinkDesc(webLinkDesc);
                item.setWebLinkImgUrl(webLinkImgUrl);
                item.setWebLinkImgThumb(webLinkImgThumb);
           *//*     item.setReceiverID(from);
                item.setFileBufferAt(0);
                item.setUploadDownloadProgress(0);
                item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_NOT_START);

            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        return item;
    }

    public MessageItemChat getReplyDetailsByObject(JSONObject objects, JSONObject replyObject, MessageItemChat item) {
        MyLog.e("getReplyDetailsByObject", "getReplyDetailsByObject" + replyObject);
        //   Log.e("getReplyDetailsByObject","getReplyDetailsByObject"+item.toString());

        try {
            String fromMsisdn = "";
            if (replyObject.has("DisplayMsisdn")) {
                fromMsisdn = replyObject.getString("DisplayMsisdn");
            } else if (replyObject.has("From_msisdn")) {
                fromMsisdn = replyObject.getString("From_msisdn");
            } else if (replyObject.has("ContactMsisdn")) {
                fromMsisdn = replyObject.getString("ContactMsisdn");
            } else {
                if (replyObject.has("msisdn")) {

                    fromMsisdn = replyObject.getString("msisdn");
                    item.setReplyMsisdn(fromMsisdn);
                }
            }

            //From is null
            if (replyObject.has("from")) {
                item.setReplyFrom(replyObject.getString("from"));
            }
            String userId = "";
            //item.setReplyFrom(replyObject.getString("from"));
            if (replyObject.has("from")) {
                userId = replyObject.getString("from");
            }
            //       String userId = replyObject.getString("from");
            if (replyObject.has("type")) {
                item.setReplyType(replyObject.getString("type"));
            } else {
                item.setReplyType("");

            }
            if (replyObject.has("message")) {
                if (replyObject.has("original_filename")) {

                    String fileName = replyObject.getString("original_filename");
                    if (fileName != null && !fileName.trim().isEmpty())
                        item.setReplyMessage(replyObject.getString("original_filename"));
                    else
                        item.setReplyMessage(replyObject.getString("message"));

                } else {
                    item.setReplyMessage(replyObject.getString("message"));
                }

            }
            if (replyObject.has("_id")) {
                item.setReplyId(replyObject.getString("_id"));
            } else {
                item.setReplyId(objects.getString("_id"));

            }
            if (replyObject.has("server_load")) {
                item.setReplyServerLoad(replyObject.getString("server_load"));
            }
            // item.setReplyServerLoad(replyObject.getString("server_load"));
            if (replyObject.has("thumbnail_data")) {
                String thumbnail_data = replyObject.getString("thumbnail_data");
                item.setreplyimagebase64(thumbnail_data);
            }

            String replyname = getcontactname.getSendername(userId, fromMsisdn);

            if (item.getReplyFrom().equalsIgnoreCase(mCurrentUserId)) {
                item.setReplySender("you");
            } else {
                item.setReplySender(replyname);
            }

            if (replyObject.has("link_details")) {
                try {
                    JSONObject replyLinkObj = replyObject.getJSONObject("link_details");
                    if (replyLinkObj.has("title")) {
                        String replyLocTitle = replyLinkObj.getString("title");
                        item.setReplyMessage(replyLocTitle);
                    }

                        *//*String replyLocThumbUrl = replyLinkObj.getString("image");
                        String replyLocDesc = replyLinkObj.getString("description");
                        String replyLocUrl = replyLinkObj.getString("url");*//*
                    if (replyLinkObj.has("thumbnail_data")) {

                        String replyLocThumbData = replyLinkObj.getString("thumbnail_data");
                        item.setreplyimagebase64(replyLocThumbData);

                    }
                        *//*item.setWebLinkImgUrl(replyLocThumbUrl);
                        item.setWebLinkDesc(replyLocDesc);
                        item.setWebLink(replyLocUrl);*//*
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

        return item;
    }

    public MessageItemChat getReplyDetailsByMessageItem(MessageItemChat replyMsgItem, MessageItemChat item) {

        String repliedUserId, repliedMsisdn, replyname;
        if (replyMsgItem.isSelf()) {
            repliedUserId = mCurrentUserId;
            repliedMsisdn = SessionManager.getInstance(mContext).getPhoneNumberOfCurrentUser();
            replyname = "you";
        } else {
            repliedUserId = replyMsgItem.getReceiverID();
            repliedMsisdn = replyMsgItem.getSenderMsisdn();
            replyname = getcontactname.getSendername(repliedUserId, repliedMsisdn);
        }

        item.setReplyMsisdn(repliedMsisdn);
        item.setReplyFrom(repliedUserId);
        item.setReplySender(replyname);
        item.setReplyType(replyMsgItem.getMessageType());
        item.setReplyMessage(replyMsgItem.getTextMessage());
        item.setReplyId(replyMsgItem.getRecordId());
        item.setreplyimagebase64(replyMsgItem.getThumbnailData());

        if (replyMsgItem.getMessageType() != null &&
                (replyMsgItem.getMessageType().equals(MessageFactory.web_link + "") ||
                        replyMsgItem.getMessageType().equals(MessageFactory.location + ""))) {
            item.setReplyMessage(replyMsgItem.getWebLinkTitle());
            item.setreplyimagebase64(replyMsgItem.getWebLinkImgThumb());
        }

//        item.setReplyServerLoad(replyObject.getString("server_load"));

        return item;
    }

    public MessageItemChat getMissedCallMessage(JSONObject object) {

        MessageItemChat msgItem = new MessageItemChat();

        try {
            String type = object.getString("type");
            String from = object.getString("from");
            String to = object.getString("to");
            String id = object.getString("id");
            String recordId = object.getString("recordId");
            String convId = object.getString("convId");
            String senderMsisdn = object.getString("ContactMsisdn");

            String ts = object.getString("timestamp");

            String callId;
            if (object.has("docId")) {
                callId = object.getString("docId");
            } else {
                callId = object.getString("doc_id");
            }

            String docId = mCurrentUserId + "-" + from;
            String msgId = docId + "-" + id;

            msgItem.set_id(id);
            msgItem.setMessageId(msgId);
            msgItem.setConvId(convId);
            msgItem.setRecordId(recordId);
            msgItem.setReceiverID(from);
            msgItem.setMessageType(type);
            msgItem.setIsSelf(false);
            msgItem.setTS(ts);
            msgItem.setSenderMsisdn(senderMsisdn);
            msgItem.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_DELIVERED);
            msgItem.setStarredStatus(MessageFactory.MESSAGE_UN_STARRED);

            // TODO: 8/18/2017 Need call_type for offline call logs from json
            String callType = object.getString("call_type");
            msgItem.setCallType(callType);

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        return msgItem;
    }

    public CallItemChat loadOutgoingCall(JSONObject object) {
        MyLog.e(TAG, "loadOutgoingCall" + object);
        try {
            String type = object.getString("type");
            String from = object.getString("from");
            String to = object.getString("to");
            String id = object.getString("id");
            String recordId = object.getString("recordId");
            String toUserMsisdn = object.getString("To_msisdn");
            String callStatus = object.getString("call_status");
            String callId = object.getString("doc_id");
            String ts = object.getString("timestamp");
            String receiverName = getcontactname.getSendername(to, toUserMsisdn);

            CallItemChat callItem = new CallItemChat();
            callItem.setId(id);
            callItem.setCallType(type);
            callItem.setOpponentUserId(to);
            callItem.setRecordId(recordId);
            callItem.setOpponentUserMsisdn(toUserMsisdn);
            callItem.setCallStatus(callStatus);
            callItem.setCallId(callId);
            callItem.setTS(ts);
            callItem.setIsSelf(true);
            callItem.setCallerName(receiverName);
//            callItem = CallMessage.getCallCount(from, to, callItem);
            return callItem;
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
            return null;
        }
    }

    public CallItemChat loadIncomingCall(JSONObject object) {
        MyLog.e(TAG, "loadIncomingCall" + object);
        try {
            String type = object.getString("type");
            String from = object.getString("from");
            String to = object.getString("to");
            String id = object.getString("id");
            String recordId = object.getString("recordId");
            String fromUserMsisdn = object.getString("ContactMsisdn");
            //String callStatus = object.getString("call_status");
            String ts = object.getString("timestamp");
            String receiverName = getcontactname.getSendername(from, fromUserMsisdn);

            String callId = to + "-" + from + "-" + id;

            CallItemChat callItem = new CallItemChat();
            callItem.setId(id);
            callItem.setCallType(type);
            callItem.setOpponentUserId(from);
            callItem.setRecordId(recordId);

            callItem.setOpponentUserMsisdn(fromUserMsisdn);
            callItem.setCallStatus(MessageFactory.CALL_STATUS_ARRIVED + "");
            callItem.setCallId(callId);
            callItem.setTS(ts);
            callItem.setIsSelf(false);
            callItem.setCallerName(receiverName);
//            callItem = CallMessage.getCallCount(to, from, callItem);
            return callItem;
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
            return null;
        }
    }

    public CallItemChat loadOfflineCall(JSONObject object) {
        CallItemChat callItem = new CallItemChat();

        try {
            String type = "";
            if (object.has("type"))
                type = object.getString("type");
            String from = object.getString("from");
            String to = object.getString("to");

            String id = "";
            if (object.has("msgId")) {
                id = object.getString("msgId");
            } else if (object.has("id")) {
                id = object.getString("id");
            }
            String recordId = object.getString("recordId");
            String fromUserMsisdn = "";
            if (object.has("ContactMsisdn"))
                fromUserMsisdn = object.getString("ContactMsisdn");
            String callStatus = object.getString("call_status");
            String ts = "";
            if (object.has("timestamp")) {
                ts = object.getString("timestamp");
            } else {
                ts = "" + id;
            }
            String receiverName = getcontactname.getSendername(from, fromUserMsisdn);

            String callId = to + "-" + from + "-" + id;

            callItem.setId(id);
            callItem.setCallType(type);
            callItem.setOpponentUserId(from);
            callItem.setRecordId(recordId);
            callItem.setOpponentUserMsisdn(fromUserMsisdn);
            callItem.setCallStatus(callStatus);
            callItem.setCallId(callId);
            callItem.setTS(ts);
            callItem.setIsSelf(false);
            callItem.setCallerName(receiverName);
            callItem.setCallDuration("00:00");
            if (callStatus != null && callStatus.equals("5"))
                callItem.setCallCount(1);
//            callItem = CallMessage.getCallCount(to, from, callItem);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
            return null;
        }
        return callItem;

    }

    public CallItemChat loadfromOfflineCall(JSONObject object) {
        CallItemChat callItem = new CallItemChat();

        try {
            String type = "";
            if (object.has("type"))
                type = object.getString("type");
            String from = object.getString("from");
            String to = object.getString("to");

            String id = "";
            if (object.has("msgId")) {
                id = object.getString("msgId");
            } else if (object.has("id")) {
                id = object.getString("id");
            }
            String recordId = object.getString("recordId");
            String fromUserMsisdn = "";
            if (object.has("ContactMsisdn"))
                fromUserMsisdn = object.getString("ContactMsisdn");
            String callStatus = object.getString("call_status");
            String ts = "";
            if (object.has("timestamp")) {
                ts = object.getString("timestamp");
            } else {
                ts = "" + id;
            }
            String receiverName = getcontactname.getSendername(from, fromUserMsisdn);
            String callId = from + "-" + to + "-" + id;
            callItem.setId(id);
            callItem.setCallType(type);
            callItem.setOpponentUserId(to);
            callItem.setRecordId(recordId);
            callItem.setOpponentUserMsisdn(fromUserMsisdn);
            callItem.setCallStatus(callStatus);
            callItem.setCallId(callId);
            callItem.setTS(ts);
            callItem.setIsSelf(false);
            callItem.setCallerName(receiverName);
            callItem.setCallDuration("00:00");
            if (callStatus != null && callStatus.equals("5"))
                callItem.setCallCount(1);
//            callItem = CallMessage.getCallCount(to, from, callItem);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
            return null;
        }
        return callItem;

    }

    public CallItemChat loadfromOfflineOutgoingCall(JSONObject object) {
        CallItemChat callItem = new CallItemChat();

        try {
            String type = "";
            if (object.has("type"))
                type = object.getString("type");
            String from = object.getString("from");
            String to = object.getString("to");

            String id = "";
            if (object.has("msgId")) {
                id = object.getString("msgId");
            } else if (object.has("id")) {
                id = object.getString("id");
            }
            String recordId = object.getString("recordId");
            String fromUserMsisdn = "";
            if (object.has("ContactMsisdn"))
                fromUserMsisdn = object.getString("ContactMsisdn");
            String callStatus = object.getString("call_status");
            String ts = "";
            if (object.has("timestamp")) {
                ts = object.getString("timestamp");
            } else {
                ts = "" + id;
            }
            String receiverName = getcontactname.getSendername(from, fromUserMsisdn);
            String callId = from + "-" + to + "-" + id;
            callItem.setId(id);
            callItem.setCallType(type);
            callItem.setOpponentUserId(to);
            callItem.setRecordId(recordId);
            callItem.setOpponentUserMsisdn(fromUserMsisdn);
            callItem.setCallStatus(callStatus);
            callItem.setCallId(callId);
            callItem.setTS(ts);
            callItem.setIsSelf(true);
            callItem.setCallerName(receiverName);
            callItem.setCallDuration("00:00");
            if (callStatus != null && callStatus.equals("5"))
                callItem.setCallCount(1);
//            callItem = CallMessage.getCallCount(to, from, callItem);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
            return null;
        }
        return callItem;

    }


    public void setCallback(MessageService msgSvcCallback) {
        this.msgSvcCallback = msgSvcCallback;
    }

    public CallItemChat loadOutgoingCallOppenent(JSONObject object) {
        MyLog.e(TAG, "loadOutgoingCallOppenent" + object);
        CallItemChat callItem = new CallItemChat();
        boolean isAnsweredToUser = SharedPreference.getInstance().getBool(mContext, "isAnsweredToUser");
        try {
            String type = "";
            if (object.has("type"))
                type = object.getString("type");
            String from = object.getString("from");
            String to = object.getString("to");
            String toDocId = object.getString("toDocId");
            String id = "";
            if (object.has("msgId")) {
                id = object.getString("msgId");
            } else if (object.has("id")) {
                id = object.getString("id");
            }
            String recordId = object.getString("recordId");

            String fromUserMsisdn = "";
            if (object.has("ContactMsisdn"))
                fromUserMsisdn = object.getString("ContactMsisdn");
            String callStatus = object.getString("call_status");
            String ts = "";
            if (object.has("timestamp")) {
                ts = object.getString("timestamp");
            } else {
                ts = "" + id;
            }
            String receiverName = getcontactname.getSendername(from, fromUserMsisdn);

            String callId = to + "-" + from + "-" + id;

            callItem.setId(id);
            callItem.setCallType(type);
            callItem.setOpponentUserId(from);
            callItem.setRecordId(recordId);
            callItem.setOpponentUserMsisdn(fromUserMsisdn);
            callItem.setCallStatus(callStatus);
            callItem.setCallId(callId);
            callItem.setTS(ts);
            if (toDocId.startsWith(mCurrentUserId))
                callItem.setIsSelf(true);
            else if (Integer.parseInt(callStatus) == 2) {
                if (!isAnsweredToUser)
                    callItem.setIsSelf(true);
            } else {
                callItem.setIsSelf(false);
            }
            callItem.setCallerName(receiverName);
            callItem.setCallDuration("00:00");
            if (callStatus != null && callStatus.equals("5"))
                callItem.setCallCount(1);
//            callItem = CallMessage.getCallCount(to, from, callItem);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
            return null;
        }
        return callItem;

    }
}
*/