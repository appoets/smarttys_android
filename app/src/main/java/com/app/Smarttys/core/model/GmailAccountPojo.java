package com.app.Smarttys.core.model;

/**
 * Created by CAS60 on 5/10/2017.
 */
public class GmailAccountPojo {
    private String mailId;
    private boolean isSelected;

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
