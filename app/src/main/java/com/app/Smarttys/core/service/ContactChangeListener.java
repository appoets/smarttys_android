package com.app.Smarttys.core.service;

import android.net.Uri;

public interface ContactChangeListener {


    void onContactChange(Uri uri);
}
