package com.app.Smarttys.core.message;

import android.content.Context;

import com.app.Smarttys.app.utils.GroupInfoSession;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.model.GroupInfoPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.socket.SocketManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by CAS60 on 2/2/2017.
 */
public class DocumentMessage extends BaseMessage implements Message {

    private static final String TAG = "DocumentMessage";
    private Context context;

    public DocumentMessage(Context context) {
        super(context);
        setType(MessageFactory.document);
        this.context = context;
    }

    @Override
    public Object getMessageObject(String to, String payload, Boolean isSecretChat) {
        this.to = to;
        setId(from + "-" + to);
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("type", MessageFactory.document);
            object.put("payload", payload);
            object.put("id", tsForServerEpoch);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);

            if (isSecretChat) {
//                setId(getId() + "-secret");
                object.put("chat_type", MessageFactory.CHAT_TYPE_SECRET);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return object;
    }

    @Override
    public Object getGroupMessageObject(String to, String payload, String groupName) {
        this.to = to;
        setId(from + "-" + to + "-g");
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("type", MessageFactory.group_document_message);
            object.put("payload", payload);
            object.put("id", tsForServerEpoch);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);
            object.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE);
            object.put("userName", groupName);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return object;
    }

    public MessageItemChat createMessageItem(boolean isSelf, String filePath, String deliverStatus,
                                             String receiverId, String receiverName) {
        String[] fullPath = filePath.split("/");
        String fileName = fullPath[fullPath.length - 1];

        item = new MessageItemChat();
        item.setMessageId(getId() + "-" + tsForServerEpoch);
        item.setIsSelf(isSelf);
        item.setTextMessage(fileName);
        item.setReceiverID(to);
        item.setChatFileLocalPath(filePath);
        item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_COMPLETED);
        item.setDeliveryStatus(deliverStatus);
        item.setReceiverUid(receiverId);
        item.setMessageType("" + type);
        item.setSenderName(receiverName);
        item.setTS(getShortTimeFormat());
        item.setFileBufferAt(0);

        if (fileName.contains(".pdf")) {
            item.setThumbnailData(getPDFThumbData(filePath));
        }

        if (getId().contains("-g")) {
            GroupInfoSession groupInfoSession = new GroupInfoSession(context);
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(getId());

            if (infoPojo != null) {
                String[] groupMembers = infoPojo.getGroupMembers().split(",");
                try {
                    JSONArray arrMembers = new JSONArray();
                    for (String member : groupMembers) {
                        JSONObject userObj = new JSONObject();
                        userObj.put("UserId", member);
                        userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_SENT);
                        if (from.equals(member)) {
                            userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_READ);
                        }
                        userObj.put("DeliverTime", "");
                        userObj.put("ReadTime", "");
                        arrMembers.put(userObj);
                    }
                    JSONObject deliverObj = new JSONObject();
                    deliverObj.put("GroupMessageStatus", arrMembers);
                    item.setGroupMsgDeliverStatus(deliverObj.toString());
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }
        }

        return item;
    }

    public Object createDocUploadObject(String msgId, String docId, String docName, String filePath,
                                        String receiverName, String chatType, boolean isSecretChat) {
        JSONObject uploadObj = new JSONObject();

        try {
            uploadObj.put("err", 0);
            uploadObj.put("ImageName", docName);
            uploadObj.put("id", msgId);
            uploadObj.put("from", docId.split("-")[0]);
            uploadObj.put("to", docId.split("-")[1]);
            uploadObj.put("toDocId", msgId);
            uploadObj.put("docId", docId);
            uploadObj.put("bufferAt", -1); // for get first index is 0.
            uploadObj.put("LocalPath", filePath);
            uploadObj.put("type", MessageFactory.document);
            uploadObj.put("ReceiverName", receiverName);
            uploadObj.put("mode", "phone");
            uploadObj.put("chat_type", chatType);

            if (isSecretChat) {
                uploadObj.put("secret_type", "yes");
            } else {
                uploadObj.put("secret_type", "no");
            }

            File file = new File(filePath);
            uploadObj.put("size", file.length());
            uploadObj.put("original_filename", file.getName());
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        return uploadObj;
    }

    public Object getDocumentMessageObject(String msgId, int fileSize, String originalFileName, String locFilePath,
                                           String serverFilePath, boolean isSecretChat) {
        JSONObject msgObj = new JSONObject();
        try {
            String[] splitIds = msgId.split("-");
            String fromUserId = splitIds[0];
            String toUserId = splitIds[1];
            String docMsgId;
            if (msgId.contains("-g")) {
                docMsgId = splitIds[3];
            } else {
                docMsgId = splitIds[2];
            }

            msgObj.put("from", fromUserId);
            msgObj.put("to", toUserId);
            msgObj.put("type", MessageFactory.document);
            msgObj.put("payload", "");
            msgObj.put("original_filename", originalFileName);

            msgObj.put("id", docMsgId);
            msgObj.put("toDocId", msgId);
            msgObj.put("filesize", fileSize);
            msgObj.put("thumbnail", serverFilePath);
            if (locFilePath.contains(".pdf")) {
                msgObj.put("thumbnail_data", getPDFThumbData(locFilePath));
            }

            if (isSecretChat) {
                msgObj.put("chat_type", MessageFactory.CHAT_TYPE_SECRET);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return msgObj;
    }

    public Object getGroupDocumentMessageObject(String msgId, int fileSize, String originalFileName, String locFilePath,
                                                String serverFilePath, String groupName) {

        JSONObject msgObj = new JSONObject();
        try {
            String[] splitIds = msgId.split("-");
            String fromUserId = splitIds[0];
            String toUserId = splitIds[1];
            String docMsgId;
            if (msgId.contains("-g")) {
                docMsgId = splitIds[3];
            } else {
                docMsgId = splitIds[2];
            }
            msgObj.put("from", fromUserId);
            msgObj.put("to", toUserId);
            msgObj.put("type", MessageFactory.group_document_message);
            msgObj.put("original_filename", originalFileName);
            msgObj.put("id", docMsgId);
            msgObj.put("toDocId", msgId);
            msgObj.put("filesize", fileSize);
            msgObj.put("thumbnail", serverFilePath);
            msgObj.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE);
            msgObj.put("userName", groupName);
            msgObj.put("payload", "");

            if (locFilePath.contains(".pdf")) {
                msgObj.put("thumbnail_data", getPDFThumbData(locFilePath));
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return msgObj;
    }

    public String getPDFThumbData(String filePath) {
        String thumbData = "";
        int pageNumber = 0;
       /* PdfiumCore pdfiumCore = new PdfiumCore(context);
        try {
            //http://www.programcreek.com/java-api-examples/index.php?api=android.os.ParcelFileDescriptor
            ParcelFileDescriptor fd = ParcelFileDescriptor.open(new File(filePath), ParcelFileDescriptor.MODE_READ_ONLY);
            PdfDocument pdfDocument = pdfiumCore.newDocument(fd);
            pdfiumCore.openPage(pdfDocument, pageNumber);
            *//*int width = pdfiumCore.getPageWidthPoint(pdfDocument, pageNumber);
            int height = pdfiumCore.getPageHeightPoint(pdfDocument, pageNumber);*//*
            Bitmap bmp = Bitmap.createBitmap(300, 300, Bitmap.Config.ARGB_8888);
            pdfiumCore.renderPageBitmap(pdfDocument, bmp, pageNumber, 0, 0, 300, 300);
            pdfiumCore.closeDocument(pdfDocument); // important!

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] thumbBuffer = byteArrayOutputStream .toByteArray();
            thumbData = Base64.encodeToString(thumbBuffer, Base64.DEFAULT);
            if (!thumbData.startsWith("data:image/jpeg;base64,")) {
                thumbData = "data:image/jpeg;base64," + thumbData;
            }
        } catch (Exception e) {
            //todo with exception
            Log.e(TAG,"",e);
        }*/

        return thumbData;
    }
}
