package com.app.Smarttys.core.socket;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.app.Smarttys.app.utils.MyLog;

public class PermanentBackgroundService extends Service {
    private static final int NOTIFICATION_ID = 10;

    private static void startForeground(Service service) {
        Notification notification = new Notification.Builder(service).getNotification();
        service.startForeground(NOTIFICATION_ID, notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (TemporaryForegroundService.instance == null)
            throw new RuntimeException(TemporaryForegroundService.class.getSimpleName() + " not running");

        //Set both services to foreground using the same notification id, resulting in just one notification
        startForeground(TemporaryForegroundService.instance);
        startForeground(this);

        //Cancel this service's notification, resulting in zero notifications
        stopForeground(true);

        //Stop this service so we don't waste RAM.
        //Must only be called *after* doing the work or the notification won't be hidden.
        stopSelf();

        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        MyLog.e("onCreate", "PermanentBackgroundService");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MyLog.e("onDestroy", "PermanentBackgroundService");

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
