package com.app.Smarttys.core.message;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.provider.Settings;

import androidx.core.app.NotificationCompat;

import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.NewHomeScreenActivty;
import com.app.Smarttys.app.model.GroupInviteBraodCast;
import com.app.Smarttys.app.model.GroupInviteModified;
import com.app.Smarttys.app.model.NewGroupDetails_Model;
import com.app.Smarttys.app.model.NewGroup_DB;
import com.app.Smarttys.app.utils.AutoDownLoadUtils;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.GroupInfoSession;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.ShortcutBadgeManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.model.GroupInfoPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.MessageService;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import me.leolin.shortcutbadger.ShortcutBadger;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 *
 */
public class MesssageObjectReceiver {
    private static final String TAG = "MesssageObjectReceiver";
    Context context;
    HashMap<String, MessageItemChat> uniqueQue = new HashMap<>();
    private ActiveSessionDispatcher dispatcher;
    private String uniqueCurrentID;
    private ShortcutBadgeManager shortcutBadgeMgnr;
    private GroupInfoSession groupInfoSession;
    private UserInfoSession userInfoSession;
    private String mCurrentUserId;
    private FileUploadDownloadManager uploadDownloadManager;
    private IncomingMessage incomingMsg;
    private EventBus eventBus;
    private Session session;
    private String connected;
    private Getcontactname getcontactname;

    private boolean wifiEnabled = false;

    private MessageService callBack;


    public MesssageObjectReceiver(MessageService messageService) {
        this.context = CoreController.mcontext;
        this.callBack = messageService;

        session = new Session(context);
        uploadDownloadManager = new FileUploadDownloadManager(this.context);
        dispatcher = new ActiveSessionDispatcher();
        shortcutBadgeMgnr = new ShortcutBadgeManager(this.context);
        groupInfoSession = new GroupInfoSession(this.context);
        userInfoSession = new UserInfoSession(this.context);
        mCurrentUserId = SessionManager.getInstance(this.context).getCurrentUserID();
        incomingMsg = new IncomingMessage(this.context);
        incomingMsg.setCallback(this.callBack);
        //Check App is not running and Closed
        //    incomingMsg.setBackgroundCallback(mCallBack);
        getcontactname = new Getcontactname(this.context);

    }

    public static void addToDBForApproval(JSONObject object, Context context, String mCurrentUserId) {
        NewGroupDetails_Model newGroupDetails_model = new NewGroupDetails_Model();
        try {
            newGroupDetails_model.setAdmin(object.getJSONArray("admin").toString());
            newGroupDetails_model.setConvId(object.getString("convId"));
            newGroupDetails_model.setCreatedBy(object.getString("createdBy"));
            newGroupDetails_model.setErr(object.getInt("err"));
            newGroupDetails_model.setFrom_(object.getString("from"));
            newGroupDetails_model.setGroupId(object.getString("groupId"));
            newGroupDetails_model.setGroupMembers(object.getJSONArray("groupMembers").toString());
            newGroupDetails_model.setGroupName(object.getString("groupName"));
            newGroupDetails_model.setGroupType(object.getInt("groupType"));
            newGroupDetails_model.setId(object.getString("id"));
            newGroupDetails_model.setMsisdn(object.getString("msisdn"));
            newGroupDetails_model.setProfilePic(object.getString("profilePic"));
            newGroupDetails_model.setRecordId(object.getString("recordId"));
            newGroupDetails_model.setTimestamp(object.getString("timeStamp"));
            newGroupDetails_model.setType(object.getString("type"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NewGroup_DB db = CoreController.getmNewGroup_db(context);
        db.insertNewGroupDetails(newGroupDetails_model, object);
        EventBus.getDefault().post(new GroupInviteBraodCast());
        sendGroupAckToServer(mCurrentUserId, newGroupDetails_model.getGroupId(), newGroupDetails_model.getId());
    }

    public static void deleteInvitedGroup(String groupId, Context context) {

        NewGroup_DB newGroup_db = CoreController.getmNewGroup_db(context);
        try {
            NewGroupDetails_Model model = newGroup_db.getSingleData(groupId);

        } catch (Exception e) {
            MyLog.e(TAG, "deleteInvitedGroup: ", e);
        }
        //MessageService.service.denyGroup(groupId);
        newGroup_db.deleteGroupByGroupId(groupId);
        EventBus.getDefault().post(new GroupInviteModified(groupId));
        EventBus.getDefault().post(new GroupInviteBraodCast());
    }

    private static void sendGroupAckToServer(String from, String groupId, String id) {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_GROUP);
        try {
            JSONObject groupAckObj = new JSONObject();
            groupAckObj.put("groupType", SocketManager.ACTION_ACK_GROUP_MESSAGE);
            groupAckObj.put("from", from);
            groupAckObj.put("groupId", groupId);
            groupAckObj.put("status", MessageFactory.GROUP_MSG_DELIVER_ACK);
            groupAckObj.put("msgId", id);
            messageEvent.setMessageObject(groupAckObj);
            EventBus.getDefault().post(messageEvent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setEventBusObject(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    public void storeGroupMsgInDataBase(Object[] response) {
        try {
            JSONObject objects = new JSONObject(response[0].toString());
            Integer type = Integer.parseInt(objects.getString("type"));

            switch (type) {
                case MessageFactory.join_new_group:
                    if (CoreController.isRaad) {
                        String admin_id = objects.getString("createdBy");
                        //we can allow admin. but users not allowed to join directly. they will confirm
                        if (!admin_id.equalsIgnoreCase(mCurrentUserId)) {
                            addToDBForApproval(objects, context, mCurrentUserId);
                            return;
                        }
                    }
                    loadNewGroupMessage(objects);
                    MyLog.d("GroupEvent___new", objects.toString());
                    break;

                case MessageFactory.add_group_member:
                    if (CoreController.isRaad) {
                        String admin_id = objects.getString("createdBy");
                        //we can allow admin. but users not allowed to join directly. they will confirm
                        if (!admin_id.equalsIgnoreCase(mCurrentUserId)) {
                            addToDBForApproval(objects, context, mCurrentUserId);
                            return;
                        }
                    }
                    joinExistingGroupMessage(objects);
                    MyLog.d("GroupEvent___joinexist", objects.toString());
                    break;

                case MessageFactory.change_group_icon:
                    loadGroupDpChangeMessage(objects);
                    MyLog.d("GroupEvent___changeicon", objects.toString());
                    break;

                case MessageFactory.delete_member_by_admin:
                    loadOfflineDeleteMemberMessage(objects);
                    MyLog.d("GroupEvent___deletemem", objects.toString());
                    break;

                case MessageFactory.change_group_name:
                    loadChangeGroupNameMessage(objects);
                    MyLog.d("GroupEvent___changename", objects.toString());
                    break;

                case MessageFactory.make_admin_member:
                    loadOfflineMakeAdminMessage(objects);
                    MyLog.d("GroupEvent___makeadmin", objects.toString());
                    break;

                case MessageFactory.exit_group:
                    loadOfflineExitGroupMessage(objects);
                    MyLog.d("GroupEvent___exitgroup", objects.toString());
                    break;

                //only for raad
                case MessageFactory.GROUP_INVITE_ACCEPT:
                    try {
                        String from = objects.getString("from");
                        String groupId = objects.getString("groupId");
                        String id = objects.getString("id");
                        sendGroupAckToServer(from, groupId, id);
                    } catch (Exception e) {
                        MyLog.e(TAG, "storeGroupMsgInDataBase: ", e);
                    }
                    break;

                default:
                    loadGroupMessage(objects);
                    break;
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void storeGroupMsgInDataBase(JSONObject response, String mType) {
        try {

            switch (Integer.valueOf(mType)) {

                case MessageFactory.join_new_group:
                    if (CoreController.isRaad) {
                        String admin_id = response.getString("createdBy");
                        //we can allow admin. but users not allowed to join directly. they will confirm
                        if (!admin_id.equalsIgnoreCase(mCurrentUserId)) {
                            addToDBForApproval(response, context, mCurrentUserId);
                            return;
                        }
                    }
                    loadNewGroupMessage(response);
                    MyLog.d("GroupEvent___new", response.toString());
                    break;

                case MessageFactory.add_group_member:
                    if (CoreController.isRaad) {
                        String admin_id = response.getString("createdBy");
                        //we can allow admin. but users not allowed to join directly. they will confirm
                        if (!admin_id.equalsIgnoreCase(mCurrentUserId)) {
                            addToDBForApproval(response, context, mCurrentUserId);
                            return;
                        }
                    }
                    joinExistingGroupMessage(response);
                    MyLog.d("GroupEvent___joinexist", response.toString());
                    break;

                case MessageFactory.change_group_icon:
                    loadGroupDpChangeMessage(response);
                    MyLog.d("GroupEvent___changeicon", response.toString());
                    break;

                case MessageFactory.delete_member_by_admin:
                    loadOfflineDeleteMemberMessage(response);
                    MyLog.d("GroupEvent___deletemem", response.toString());
                    break;

                case MessageFactory.change_group_name:
                    loadChangeGroupNameMessage(response);
                    MyLog.d("GroupEvent___changename", response.toString());
                    break;

                case MessageFactory.make_admin_member:
                    loadOfflineMakeAdminMessage(response);
                    MyLog.d("GroupEvent___makeadmin", response.toString());
                    break;

                case MessageFactory.exit_group:
                    loadOfflineExitGroupMessage(response);
                    MyLog.d("GroupEvent___exitgroup", response.toString());
                    break;

                default:
                    loadGroupMessage(response);
                    break;
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadOfflineExitGroupMessage(JSONObject object) {
        try {
            String err = object.getString("err");
            if (err.equals("0")) {
                String from = object.getString("from");
                String msgId = object.getString("id");
                String groupId = object.getString("groupId");
                String groupName = object.getString("groupName");

                GroupInfoPojo infoPojo = new GroupInfoPojo();

                String docId = mCurrentUserId + "-" + groupId + "-g";
                if (groupInfoSession.hasGroupInfo(docId)) {
                    infoPojo = groupInfoSession.getGroupInfo(docId);
                }

                String group_mem = infoPojo.getGroupMembers();
                if (group_mem != null) {
                    group_mem = group_mem.replace(from, "");
                    group_mem = group_mem.replace(",,", ",");
                    infoPojo.setGroupMembers(group_mem);
                }


                if (from.equalsIgnoreCase(mCurrentUserId)) {
                    infoPojo.setLiveGroup(false);
                    deleteInvitedGroup(groupId, context);
                    MessageService.removeUser(groupId);
                } else {
                    getUserDetailsNotExists(from);
                }

                groupInfoSession.updateGroupInfo(docId, infoPojo);
                //For update latest group details
                callBack.getGroupDetails(groupId);

                GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                MessageItemChat item = message.createMessageItem(MessageFactory.exit_group, false, null,
                        MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, null);
                item.setMessageId(docId.concat("-").concat(msgId));

                if (object.has("timestamp")) {
                    String ts = object.getString("timestamp");
                    item.setTS(ts);
                }

                MessageDbController db = CoreController.getDBInstance(context);
                db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);

                sendGroupAckToServer(mCurrentUserId, groupId, msgId);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void joinExistingGroupMessage(JSONObject object) {
        try {
            String err = object.getString("err");
            if (err.equals("0")) {
                String createdTo = "";
                String msgId = object.getString("id");
                String createdBy = object.getString("createdBy");
                String groupId = object.getString("groupId");
                String groupAvatar = object.getString("profilePic");
                String groupName = object.getString("groupName");
                String groupMembers = object.getString("groupMembers");
                String admin = object.getString("admin");
                String timeStamp = object.getString("timeStamp");
                if (object.has("createdTo")) {
                    createdTo = object.getString("createdTo");
                } else if (object.has("createdBy")) {
                    createdTo = object.getString("createdBy");
                }

                try {
                    JSONObject createdToObj = new JSONObject(createdTo);
                    createdTo = createdToObj.getString("_id");
                } catch (JSONException e) {

                }

                // get user details if not available in local db
                getUserDetailsNotExists(createdBy);
                getUserDetailsNotExists(createdTo);

                String docId = mCurrentUserId + "-" + groupId + "-g";

                GroupInfoPojo infoPojo = new GroupInfoPojo();
                infoPojo.setCreatedBy(createdBy);
                infoPojo.setAvatarPath(groupAvatar);
                infoPojo.setGroupName(groupName);
                infoPojo.setGroupMembers(groupMembers);
                infoPojo.setAdminMembers(admin);
                infoPojo.setLiveGroup(true);
                groupInfoSession.updateGroupInfo(docId, infoPojo);

                if (createdTo.equalsIgnoreCase(mCurrentUserId)) {
                    callBack.createGroup(groupId);
                    callBack.getGroupDetails(groupId);

                    SendMessageEvent event = new SendMessageEvent();
                    event.setEventName(SocketManager.EVENT_GROUP);
                    JSONObject groupObj = new JSONObject();
                    groupObj.put("groupType", SocketManager.ACTION_JOIN_NEW_GROUP);
                    groupObj.put("from", mCurrentUserId);
                    groupObj.put("groupId", groupId);
                    groupObj.put("createdBy", createdBy);
                    groupObj.put("groupName", groupName);
                    groupObj.put("timeStamp", msgId);
                    groupObj.put("id", msgId);
                    event.setMessageObject(groupObj);
                    eventBus.post(event);
                }

                /*long deviceTS = Calendar.getInstance().getTimeInMillis();
                long timeDiff = SessionManager.getInstance(context).getServerTimeDifference();
                long localTime = deviceTS - timeDiff;*/

                GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                MessageItemChat item = message.createMessageItem(MessageFactory.add_group_member, false, null,
                        MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, createdBy, createdTo);
                item.setMessageId(docId.concat("-").concat(msgId));
                item.setTS(msgId);

                MessageDbController db = CoreController.getDBInstance(context);
                db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);


                sendGroupAckToServer(mCurrentUserId, groupId, msgId);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void loadNewGroupMessage(JSONObject object) {
        try {
            //if Raad we should not add user to group without user approval.
            String err = object.getString("err");
            if (err.equals("0")) {
                String msgId = object.getString("id");
                String createdBy = object.getString("createdBy");
                String groupId = object.getString("groupId");
                String groupAvatar = object.getString("profilePic");
                String groupName = object.getString("groupName");
                String groupMembers = object.getString("groupMembers");
                String admin = object.getString("admin");
                String timeStamp = object.getString("timeStamp");

                // get user details if not available in local db
                getUserDetailsNotExists(createdBy);

                String docId = mCurrentUserId + "-" + groupId + "-g";
                GroupEventInfoMessage createdMsg = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                MessageItemChat createdMsgItem = createdMsg.createMessageItem(MessageFactory.join_new_group, false,
                        null, MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, createdBy, null);
                String createdTS = String.valueOf(Long.parseLong(timeStamp) - 10);
                createdMsgItem.setTS(createdTS);
                String createdMsgId = String.valueOf(Long.parseLong(msgId) - 10);
                createdMsgItem.setMessageId(docId.concat("-").concat(createdMsgId));

                /*GroupEventInfoMessage addedMsg = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                MessageItemChat addedMsgItem = addedMsg.createMessageItem(MessageFactory.add_group_member, false, null,
                        MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, createdBy, mCurrentUserId);
                addedMsgItem.setMessageId(docId.concat("-").concat(msgId));*/

                MessageDbController db = CoreController.getDBInstance(context);
                db.updateChatMessage(createdMsgItem, MessageFactory.CHAT_TYPE_GROUP);
//                db.updateChatMessage(docId, addedMsgItem);


                if (Integer.parseInt(object.optString("type")) == 6) {
                    String from = object.getString("from");
                    uniqueCurrentID = SessionManager.getInstance(context).getCurrentUserID();

                    if (!from.equalsIgnoreCase(uniqueCurrentID)) {
                        showGroupCreationNotification(object);
                    }
                }
                GroupInfoPojo infoPojo = new GroupInfoPojo();
                infoPojo.setCreatedBy(createdBy);
                infoPojo.setAvatarPath(groupAvatar);
                infoPojo.setGroupName(groupName);
                infoPojo.setGroupMembers(groupMembers);
                infoPojo.setAdminMembers(admin);
                infoPojo.setLiveGroup(true);
                groupInfoSession.updateGroupInfo(docId, infoPojo);

                callBack.createGroup(groupId);
                callBack.getGroupDetails(groupId);

                SendMessageEvent event = new SendMessageEvent();
                event.setEventName(SocketManager.EVENT_GROUP);
                JSONObject groupObj = new JSONObject();
                groupObj.put("groupType", SocketManager.ACTION_JOIN_NEW_GROUP);
                groupObj.put("from", mCurrentUserId);
                groupObj.put("groupId", groupId);
                groupObj.put("createdBy", createdBy);
                groupObj.put("groupName", groupName);
                groupObj.put("timeStamp", timeStamp);
                groupObj.put("id", msgId);
                event.setMessageObject(groupObj);
                eventBus.post(event);

                sendGroupAckToServer(mCurrentUserId, groupId, msgId);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadGroupMessage(JSONObject objects) {
        MessageDbController db = CoreController.getDBInstance(context);

        try {
            String from = objects.getString("from");
            String groupId = objects.getString("groupId");
            String type = objects.optString("type");
            if (type != null && type.equals("23")) {
                MyLog.d(TAG, "loadGroupMessage: security token label");
                return;
            }
            if (from.equalsIgnoreCase(mCurrentUserId)) {
                String msgId = objects.getString("toDocId");

                String tempDocId = from + "-" + groupId + "-g";
                MessageItemChat tempItem = db.getParticularMessage(msgId);
                if (tempItem != null) {
                    groupMessageRes(objects);
                } else {
                    MessageItemChat msgItem = incomingMsg.loadGroupMessageFromWeb(objects);
                    db.updateChatMessage(msgItem, MessageFactory.CHAT_TYPE_GROUP);
                }
            } else {
                MessageItemChat msgItem = incomingMsg.loadGroupMessage(objects);
                String docId = mCurrentUserId + "-" + msgItem.getReceiverID() + "-g";

                if (!msgItem.getGroupMsgFrom().equalsIgnoreCase(mCurrentUserId)) {
                    msgItem.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_DELIVERED);
                    changeBadgeCount(groupId, msgItem.getMessageId());
                    sendGroupAckToServer(mCurrentUserId, msgItem.getReceiverID(), msgItem.get_id());
                }


                db.updateChatMessage(msgItem, MessageFactory.CHAT_TYPE_GROUP);
                AutoDownLoadUtils.getInstance().checkAndAutoDownload(context, msgItem);

            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }

    public Boolean isDataRoamingEnabled() {
        try {
            // return true or false if data roaming is enabled or not
            return Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.DATA_ROAMING) == 1;
        } catch (Settings.SettingNotFoundException e) {
            // return null if no such settings exist (device with no radio data ?)
            return null;
        }
    }

    private void groupMessageRes(JSONObject jsonObject) {

        try {
            String from = jsonObject.getString("from");
            String groupId = jsonObject.getString("groupId");
            String doc_id;
            if (jsonObject.has("toDocId")) {
                doc_id = jsonObject.getString("toDocId");
            } else {
                doc_id = jsonObject.getString("doc_id");
            }
            String deliver = jsonObject.getString("deliver");
            String recordId = jsonObject.getString("recordId");
            String type = jsonObject.getString("type");
            String timeStamp = jsonObject.getString("timestamp");

            if (type.equalsIgnoreCase(MessageFactory.text + "")) {
                String message = jsonObject.getString("message");
                long messagelength = session.getsentmessagelength() + message.length();
                session.putsentmessagelength(messagelength);
                int sentmesagecount = session.getsentmessagecount();
                sentmesagecount = sentmesagecount + 1;
                session.putsentmessagecount(sentmesagecount);
            } else if (type.equalsIgnoreCase(MessageFactory.picture + "") || type.equalsIgnoreCase(MessageFactory.audio + "") || type.equalsIgnoreCase(MessageFactory.video + "")) {
                long filesize = Long.parseLong(jsonObject.getString("filesize"));
                filesize = session.getsentmedialength() + filesize;
                session.putsentmedialength(filesize);
            }
            MessageDbController db = CoreController.getDBInstance(context);
            db.updateChatMessage(from + "-" + groupId + "-g", doc_id, deliver, recordId,
                    groupId, timeStamp);
            db.deleteSendNewMessage(doc_id);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void updateGroupMsgStatus(JSONObject objects) {
        try {
            String err = objects.getString("err");
            if (err.equalsIgnoreCase("0")) {
                String from = objects.getString("from");
                String groupId = objects.getString("groupId");
                String msgId = objects.getString("msgId");
                String deliverStatus = objects.getString("status");
                String timeStamp = objects.getString("currenttime");

                String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");
                MessageDbController db = CoreController.getDBInstance(context);
                db.updateGroupMessageStatus(docId, docId.concat("-").concat(msgId), deliverStatus,
                        timeStamp, from, mCurrentUserId);

            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void loadExitMessage(JSONObject object) {
        try {

            String err = object.getString("err");

            if (err.equalsIgnoreCase("0")) {
                String msgId = object.getString("id");
                String groupId = object.getString("groupId");
                String timeStamp = object.getString("timeStamp");
                String from = object.getString("from");

                String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");
                GroupInfoPojo groupInfoPojo = groupInfoSession.getGroupInfo(docId);

                if (groupInfoPojo == null) {
                    groupInfoPojo = new GroupInfoPojo();
                }

                String group_mem = groupInfoPojo.getGroupMembers();
                if (group_mem != null) {
                    group_mem = group_mem.replace(from, "");
                    group_mem = group_mem.replace(",,", ",");
                    groupInfoPojo.setGroupMembers(group_mem);
                }
                String groupName = groupInfoPojo.getGroupName();


                if (from.equalsIgnoreCase(mCurrentUserId)) {
                    groupInfoPojo.setIsAdminUser("0");
                    groupInfoPojo.setLiveGroup(false);
                    deleteInvitedGroup(groupId, context);
                    groupInfoSession.updateGroupInfo(docId, groupInfoPojo);
                } else {
                    getUserDetailsNotExists(from);
                }

                GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                MessageItemChat item = message.createMessageItem(MessageFactory.exit_group, false, null,
                        MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, null);
                item.setMessageId(docId.concat("-").concat(msgId));
                item.setTS(timeStamp);

                MessageDbController db = CoreController.getDBInstance(context);
                db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);
                sendGroupAckToServer(mCurrentUserId, groupId, msgId);
                callBack.getGroupDetails(groupId);
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void loadChangeGroupNameMessage(JSONObject object) {

        try {
            String groupId = object.getString("groupId");
            String id = object.getString("id");
            String groupPrevName = object.optString("prev_name");
            String groupNewName = object.getString("groupName");
            String from = object.getString("from");
            String timeStamp;
            if (object.has("timeStamp")) {
                timeStamp = object.getString("timeStamp");
            } else {
                timeStamp = object.getString("timestamp");
            }

            if (object.has("changed_name")) {
                groupNewName = object.getString("changed_name");
            }
            getUserDetailsNotExists(from);

            String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");

            GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
            MessageItemChat item = message.createMessageItem(MessageFactory.change_group_name, false, null,
                    MessageFactory.DELIVERY_STATUS_READ, groupId, groupNewName, from, null);
            item.setPrevGroupName(groupPrevName);
            item.setMessageId(docId.concat("-").concat(id));
            item.setTS(timeStamp);

            MessageDbController db = CoreController.getDBInstance(context);
            db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);

            boolean hasGroupInfo = groupInfoSession.hasGroupInfo(docId);
            GroupInfoPojo infoPojo;
            if (hasGroupInfo) {
                infoPojo = groupInfoSession.getGroupInfo(docId);
            } else {
                infoPojo = new GroupInfoPojo();
            }
            infoPojo.setGroupId(groupId);
            infoPojo.setGroupName(groupNewName);
            groupInfoSession.updateGroupInfo(docId, infoPojo);

            sendGroupAckToServer(mCurrentUserId, groupId, id);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void loadMakeAdminMessage(JSONObject object) {

        try {
            String err = object.getString("err");

            if (err.equalsIgnoreCase("0")) {

                String groupId = object.getString("groupId");
                String msgId = object.getString("id");
                String timeStamp = object.getString("timeStamp");
//                    String toDocId = object.getString("toDocId");
                String from = object.getString("from");
                String groupName = "";
                if (object.has("groupName")) {
                    groupName = object.getString("groupName");
                }

                String newAdminUserId = object.getString("adminuser");
                String adminMembers = object.getString("admin");
//                    String newAdminMsisdn = object.getString("newadminmsisdn");

                getUserDetailsNotExists(newAdminUserId);

                String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");
                GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);
                if (infoPojo == null) {
                    infoPojo = new GroupInfoPojo();
                } else {
                    groupName = infoPojo.getGroupName();
                }

                if (adminMembers == null || adminMembers.equals("")) {
                    adminMembers = infoPojo.getAdminMembers().concat(",").concat(newAdminUserId);
                }
                infoPojo.setAdminMembers(adminMembers);
                groupInfoSession.updateGroupInfo(docId, infoPojo);

                if (infoPojo.isLiveGroup()) {
                    GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                    MessageItemChat item = message.createMessageItem(MessageFactory.make_admin_member, false, null,
                            MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, newAdminUserId);
                    item.setMessageId(docId.concat("-").concat(msgId));
                    item.setTS(timeStamp);

                    MessageDbController db = CoreController.getDBInstance(context);
                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);
                    sendGroupAckToServer(mCurrentUserId, groupId, msgId);
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadOfflineMakeAdminMessage(JSONObject object) {
        try {
            String err = object.getString("err");

            if (err.equalsIgnoreCase("0")) {
                String msgId = object.getString("id");
                String groupId = object.getString("groupId");
                String newAdminId = object.getString("createdTo");
                String timeStamp = object.getString("timestamp");
//                String toDocId = object.getString("toDocId");
                String from = object.getString("from");
                String groupName = object.getString("groupName");

                String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");
                GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);

                getUserDetailsNotExists(from);
                getUserDetailsNotExists(newAdminId);

                if (infoPojo == null) {
                    infoPojo = new GroupInfoPojo();
                }
                String adminMembers = infoPojo.getAdminMembers();
                adminMembers = adminMembers + "'" + newAdminId;
                infoPojo.setAdminMembers(adminMembers);
                groupInfoSession.updateGroupInfo(docId, infoPojo);

                if (infoPojo.isLiveGroup()) {
                    GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                    MessageItemChat item = message.createMessageItem(MessageFactory.make_admin_member, false, null,
                            MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, newAdminId);
                    item.setMessageId(docId.concat("-").concat(msgId));
                    item.setTS(timeStamp);
                    MessageDbController db = CoreController.getDBInstance(context);
                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);

                    sendGroupAckToServer(mCurrentUserId, groupId, msgId);
                    callBack.getGroupDetails(groupId);
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void loadDeleteMemberMessage(JSONObject object) {

        try {
            String err = object.getString("err");

            if (err.equalsIgnoreCase("0")) {
//                String msg = object.getString("message");
                String msgId = object.getString("id");
                String groupId = object.getString("groupId");
                String timeStamp = object.getString("timeStamp");
                String toDocId = object.getString("toDocId");
                String from = object.getString("from");
                String removeId = object.getString("removeId");
//                String fromMsisdn = object.getString("from_msisdn");
//                String removeMsisdn = object.getString("remove_msisdn");
                String groupName = "";
                if (object.has("groupName")) {
                    groupName = object.getString("groupName");
                }

                getUserDetailsNotExists(from);
                getUserDetailsNotExists(removeId);

                String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");
                GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);

                if (infoPojo == null) {
                    infoPojo = new GroupInfoPojo();
                    infoPojo.setGroupName(groupName);
                    infoPojo.setGroupId(groupId);

                } else {
                    groupName = infoPojo.getGroupName();
                    String group_mem = infoPojo.getGroupMembers();
                    if (group_mem != null) {
                        group_mem = group_mem.replace(removeId, "");
                        group_mem = group_mem.replace(",,", ",");
                        infoPojo.setGroupMembers(group_mem);
                    }
                }

                if (removeId.equalsIgnoreCase(mCurrentUserId)) {
                    infoPojo.setLiveGroup(false);
                    infoPojo.setIsAdminUser("0");
                    deleteInvitedGroup(groupId, context);
                    MessageService.removeUser(groupId);
                }

                groupInfoSession.updateGroupInfo(docId, infoPojo);

                GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                MessageItemChat item = message.createMessageItem(MessageFactory.delete_member_by_admin, false, null, MessageFactory.DELIVERY_STATUS_READ,
                        groupId, groupName, from, removeId);
                item.setMessageId(docId.concat("-").concat(msgId));
                item.setTS(timeStamp);
                MessageDbController db = CoreController.getDBInstance(context);
                db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);

                sendGroupAckToServer(mCurrentUserId, groupId, msgId);
                callBack.getGroupDetails(groupId);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void loadOfflineDeleteMemberMessage(JSONObject object) {

        try {
            String err = object.getString("err");

            if (err.equalsIgnoreCase("0")) {
                String msgId = object.getString("id");
                String groupId = object.getString("groupId");
                String removeId = object.optString("createdTo");
                String timeStamp = object.getString("timestamp");
//                String toDocId = object.getString("toDocId");
                String from = object.getString("from");
                String groupName = object.getString("groupName");

                String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");
                GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);

                if (infoPojo != null) {
                    groupName = infoPojo.getGroupName();
                } else {
                    infoPojo = new GroupInfoPojo();
                    infoPojo.setGroupId(groupId);
                    infoPojo.setIsAdminUser("0");
                }

                // get user details if not available in local db
                getUserDetailsNotExists(from);
                if (removeId != null)
                    getUserDetailsNotExists(removeId);

                if (removeId != null && removeId.equalsIgnoreCase(mCurrentUserId)) {
                    infoPojo.setIsAdminUser("0");
                    infoPojo.setLiveGroup(false);
                    MessageService.removeUser(groupId);
                    deleteInvitedGroup(groupId, context);
                    groupInfoSession.updateGroupInfo(docId, infoPojo);
                }

                GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                MessageItemChat item = message.createMessageItem(MessageFactory.delete_member_by_admin, false, null,
                        MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, removeId);
                item.setMessageId(docId.concat("-").concat(msgId));
                item.setTS(timeStamp);

                MessageDbController db = CoreController.getDBInstance(context);
                db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);

                sendGroupAckToServer(mCurrentUserId, groupId, msgId);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void loadGroupDpChangeMessage(JSONObject object) {
        try {
            String from = object.getString("from");
            String groupId = object.getString("groupId");
            String groupDp;
            if (object.has("avatar")) {
                groupDp = object.getString("avatar");
            } else {
                groupDp = object.getString("thumbnail");
            }
            String groupName = object.getString("groupName");

            String timeStamp;
            if (object.has("timeStamp")) {
                timeStamp = object.getString("timeStamp");
            } else {
                timeStamp = object.getString("timestamp");
            }

            // get user details if not available in local db
            getUserDetailsNotExists(from);

            String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");

            GroupInfoPojo groupInfo = groupInfoSession.getGroupInfo(docId);
            groupInfo.setAvatarPath(groupDp);
            groupInfoSession.updateGroupInfo(docId, groupInfo);

            GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
            MessageItemChat item = message.createMessageItem(MessageFactory.change_group_icon, false, null,
                    MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, null);
            item.setAvatarImageUrl(groupDp);
            item.setTS(timeStamp);

            String id;
            if (object.has("id")) {
                id = object.getString("id");
                sendGroupAckToServer(mCurrentUserId, groupId, id);
            } else {
                id = Calendar.getInstance().getTimeInMillis() + "";
            }
            item.setMessageId(docId.concat("-").concat(id));

            MessageDbController db = CoreController.getDBInstance(context);
            db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void loadAddMemberMessage(JSONObject object) {
        try {
            String msg;/* = object.getString("message");*/
            String err = object.getString("err");
            String groupId = object.getString("groupId");
            String msgId = object.getString("id");
//            String timeStamp = object.getString("timeStamp");
            String from = object.getString("from");
//            String msisdn = object.getString("msisdn");
            String groupName = "";
            if (object.has("groupName")) {
                groupName = object.getString("groupName");
            }

            JSONObject newUserObj = object.getJSONObject("newUser");
            String newUserId = newUserObj.getString("_id");

            getUserDetailsNotExists(from);
            getUserDetailsNotExists(newUserId);

            String docId = mCurrentUserId.concat("-").concat(groupId).concat("-g");
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);
            if (infoPojo != null) {
                groupName = infoPojo.getGroupName();
            }

            GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
            MessageItemChat item = message.createMessageItem(MessageFactory.add_group_member, false, null,
                    MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, newUserId);
            item.setMessageId(docId.concat("-").concat(msgId));

            MessageDbController db = CoreController.getDBInstance(context);
            db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);

            if (newUserId.equalsIgnoreCase(mCurrentUserId)) {
                callBack.createGroup(groupId);
                callBack.getGroupDetails(groupId);

                SendMessageEvent event = new SendMessageEvent();
                event.setEventName(SocketManager.EVENT_GROUP);
                JSONObject groupObj = new JSONObject();
                groupObj.put("groupType", SocketManager.ACTION_JOIN_NEW_GROUP);
                groupObj.put("from", mCurrentUserId);
                groupObj.put("groupId", groupId);
                groupObj.put("createdBy", from);
                groupObj.put("groupName", groupName);
                groupObj.put("timeStamp", msgId);
                groupObj.put("id", msgId);
                event.setMessageObject(groupObj);
                eventBus.post(event);
            }

            sendGroupAckToServer(mCurrentUserId, groupId, msgId);

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void changeBadgeCount(String convId, String msgId) {

        shortcutBadgeMgnr.putMessageCount(convId, msgId);
        int totalCount = shortcutBadgeMgnr.getTotalCount();

        try {
            ShortcutBadger.applyCount(context, totalCount);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void addNewGroupData(JSONObject object) {
        try {
            //Check group is creating duplicate

            MyLog.e(TAG, "addNewGroupData" + object.toString());
            String groupId = object.getString("groupId");
            String members = object.getString("groupMembers");
            String from = object.getString("from");
            String createdBy = object.getString("createdBy");
            String profilePic = object.getString("profilePic");
            String groupName = object.getString("groupName");
            String ts = object.getString("timeStamp");
            String admin = object.getString("admin");
            String msg = object.getString("message");
            String id;
            if (object.has("id")) {
                id = object.getString("id");
            } else {
                id = "" + Calendar.getInstance().getTimeInMillis();
            }

            getUserDetailsNotExists(from);

            String docId = mCurrentUserId + "-" + groupId + "-g";

            GroupInfoPojo infoPojo = new GroupInfoPojo();
            infoPojo.setCreatedBy(createdBy);
            infoPojo.setAvatarPath(profilePic);
            infoPojo.setGroupName(groupName);
//Pass only ID ,MEMBERS

            // JSONObject jsonObject = new JSONObject(members);

            JSONArray jsonArray = null;
            String mRespones = "";
            jsonArray = new JSONArray(members);
            try {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject curr = jsonArray.getJSONObject(i);
                    String mId = curr.getString("id");
                    //   mRespones += mId + ",";
                    if (jsonArray.length() - 1 != i) {
                        mRespones += mId + ",";
                    } else {
                        mRespones += mId;
                    }
                }
            } catch (Exception e) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    String mId = jsonArray.getString(i);
                    //   mRespones += mId + ",";
                    if (jsonArray.length() - 1 != i) {
                        mRespones += mId + ",";
                    } else {
                        mRespones += mId;
                    }
                }
            }
            infoPojo.setGroupMembers(mRespones);
            infoPojo.setAdminMembers(admin);
            infoPojo.setLiveGroup(true);
            groupInfoSession.updateGroupInfo(docId, infoPojo);

            getGroupDetails(groupId);

            if (!createdBy.equals(mCurrentUserId)) {
                sendGroupAckToServer(mCurrentUserId, groupId, id);
            } else {
                GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, context);
                MessageItemChat item = message.createMessageItem(MessageFactory.join_new_group, false, null,
                        MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, null);
                item.setMessageId(docId.concat("-").concat(id));
                item.setTS(ts);

                MessageDbController db = CoreController.getDBInstance(context);
                db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void getGroupDetails(String groupId) {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_GROUP_DETAILS);

        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            object.put("convId", groupId);
            event.setMessageObject(object);
            eventBus.post(event);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void getUserDetailsNotExists(String userId) {

        if (!userId.equalsIgnoreCase(mCurrentUserId)) {
//
            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
            if (!contactDB_sqlite.isUserAvailableInDB(userId)) {
                callBack.getUserDetails(userId);
            }
        }
    }

    private void showGroupCreationNotification(JSONObject data) {
        String groupName = data.optString("groupName");
        String from = data.optString("from");
        String msisdn = data.optString("msisdn");

        NotificationCompat.Builder builder;
        Intent intent = new Intent(context, NewHomeScreenActivty.class);
        //intent.putExtra(HomeActivity.FROM_MISSED_CALL_NOTIFICATION, true);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        String mMobile = getcontactname.getSendername(from, msisdn);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder =
                    new NotificationCompat.Builder(context, "1")
                            .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.app_icon_new))
                            .setSmallIcon(R.mipmap.app_icon_new)
                            // Set Ticker Message
                            .setTicker("")
                            .setContentTitle(context.getString(R.string.app_name))
                            .setContentText(mMobile + " Created the Group " + groupName)
                            .setAutoCancel(true)
                            // Set PendingIntent into Notification
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setDefaults(Notification.DEFAULT_ALL)
                            .setContentIntent(pIntent)
                            .setContentIntent(pIntent)
                            // Set RemoteViews into Notification
                            .setColor(0xF01a9e5);


        } else {
            builder = new NotificationCompat.Builder(context)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.app_icon_new))
                    .setSmallIcon(R.mipmap.app_icon_new)
                    // Set Ticker Message
                    .setTicker("")
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentText(mMobile + " Created the Group " + groupName)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setDefaults(Notification.DEFAULT_ALL)
                    // Set PendingIntent into Notification
                    .setContentIntent(pIntent)
                    // Set RemoteViews into Notification
                    .setContentIntent(pIntent)
                    .setColor(0xF01a9e5);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int smallIconViewId = context.getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());

          /*  if (smallIconViewId != 0) {
                @SuppressLint("RestrictedApi") RemoteViews view = builder.getContentView();
                if (view != null)
                    view.setViewVisibility(smallIconViewId, View.INVISIBLE);

                if (builder.getHeadsUpContentView() != null)
                    builder.getHeadsUpContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);

                if (builder.getBigContentView() != null)
                    builder.getBigContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);
            }*/
        }


        NotificationCompat.BigTextStyle style =
                new NotificationCompat.BigTextStyle(builder);
       /* if (session.getvibratePrefsName().equals("Default"))
            builder.setVibrate("");
        else if (session.getvibratePrefsName().equals("Short"))
            builder.setVibrate(shortv);
        else if (session.getvibratePrefsName().equals("Long"))
            builder.setVibrate(longv);
*/
        style.bigText("Created the Group " + groupName)
                .setBigContentTitle(context.getString(R.string.app_name));


        builder.setStyle(style);


        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("1",
                    context.getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationmanager.createNotificationChannel(channel);
        }
        notificationmanager.notify(2, builder.build());
    }

    public class ActiveSessionDispatcher {
        private BlockingQueue<Runnable> dispatchQueue
                = new LinkedBlockingQueue<Runnable>();
        public Runnable dispatchRunnable = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        dispatchQueue.take().run();
                    } catch (InterruptedException e) {
                        MyLog.e(TAG, "", e);
                    }
                }
            }
        };
        private Thread mThread;

        public ActiveSessionDispatcher() {
            mThread = new Thread(dispatchRunnable);
            mThread.start();
        }

        private void addWork(Runnable work) {
            try {
                dispatchQueue.put(work);
            } catch (Exception e) {
            }
        }

    }
}

