package com.app.Smarttys.core.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.CallItemChat;
import com.app.Smarttys.core.model.ChatLockPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.OfflineRetryEventPojo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by CAS60 on 12/21/2017.
 */
public class MessageDbController extends SQLiteOpenHelper {

    public static final String DB_NAME = "Chat";
    public static final int MESSAGE_SELECTION_LIMIT = 120;
    public static final int CHAT_PAGE_LIMIT = 1;
    public static final int MESSAGE_SELECTION_LIMIT_FIRST_TIME = 50;
    public static final int MESSAGE_PAGE_LOADED_LIMIT = 100;
    private static final int DB_VERSION = 2;
    private static final String TAG = MessageDbController.class.getSimpleName();
    private final String TABLE_CHAT_LIST = "ChatList";
    private final String TABLE_MESSAGES = "ChatMessages";
    private final String INDEX_MESSAGES = "IndexChatMessages";
    private final String TABLE_CHAT_LOCK = "ChatLock";
    private final String TABLE_TEMP_DELETE_MESSAGES = "TempDeleteMessages";
    private final String TABLE_TEMP_STAR_MESSAGES = "TempStarMessages";
    private final String TABLE_TEMP_SEND_NEW_MESSAGE = "TempSendNewMessage";
    private final String TABLE_OFFLINE_EVENTS = "OfflineEvents";
    private final String TABLE_CALL_LOGS = "CallLogs";

    private final String COLUMN_OID = "OID";
    private final String COLUMN_STATUS = "Status";
    private final String COLUMN_TIME_STAMP = "TimeStamp";

    private final String COLUMN_MESSAGE_ID = "MessageId";
    private final String COLUMN_CHAT_ID = "ChatId";
    private final String COLUMN_RECEIVER_ID = "ReceiverId";
    private final String COLUMN_MESSAGE_DATA = "MessageData";
    private final String COLUMN_DELIVERY_STATUS = "DeliveryStatus";
    private final String COLUMN_IS_SELF = "IsSelf";
    private final String COLUMN_CHAT_TYPE = "ChatType";
    private final String COLUMN_CLEAR_STATUS = "ClearStatus";

    private final String COLUMN_SENDER_ID = "SenderId";
    private final String COLUMN_PASSWORD = "Password";
    private final String COLUMN_LOCK_STATUS = "LockStatus";

    private final String COLUMN_RECORD_ID = "RecordId";
    private final String COLUMN_DELETE_MESSAGE_DATA = "DeleteMessageData";
    private final String COLUMN_STAR_MESSAGE_DATA = "StarMessageData";

    private final String COLUMN_EVENT_ID = "EventId";
    private final String COLUMN_EVENT_NAME = "EventName";
    private final String COLUMN_EVENT_DATA = "EventData";

    private final String COLUMN_CALL_ID = "CallId";
    private final String COLUMN_CALL_DATA = "CallData";


    //--------------------------------Upload First Data Maintain Table------------------------------


    //--------------------------------Upload First Data Maintain Table------------------------------

    private final String FILE_UPLOAD_FIRSTTIME_TABLE = "Firsttimefileupload";
    private final String FILE_UPLOAD_FIRSTTIME_MESSAGE_ID = "Firsttimefileuploadmessageid";
    private final String FILE_UPLOAD_FIRSTTIME_STATUS = "Firsttimefileuploadstatus";
    private final String FILE_UPLOAD_FIRSTTIME_OBJECT = "Firsttimefileuploadobject";
    private final String FILE_UPLOAD_FIRSTTIME_COLUMN_ID = "Firsttimefileuploadcolumnid";
    private final String USERFILE_UPLOAD_TABLE = "userfileupload";

    //--------------------------------Upload First Data Maintain Table------------------------------


    //--------------------------------Upload Maintain Table------------------------------
    private final String USERFILE_UPLOAD_MESSAGE_ID = "userfileuploadmessageid";
    private final String USERFILE_UPLOAD_STATUS = "userfileuploadstatus";
    private final String USERFILE_UPLOAD_OBJECT = "userfileuploadobject";
    private final String USERFILE_UPLOAD_COLUMN_ID = "userfileuploadcolumnid";
    private final String FILE_UPLOAD_TABLE = "fileupload";
    private final String FILE_UPLOAD_MESSAGE_ID = "fileuploadmessageid";
    private final String FILE_UPLOAD_STATUS = "fileuploadstatus";
    private final String FILE_UPLOAD_OBJECT = "fileuploadobject";
    private final String FILE_UPLOAD_COLUMN_ID = "fileuploadcolumnid";
    String CREATE_UPLOAD_FIRSTTIME_TABLE = "CREATE TABLE " + FILE_UPLOAD_FIRSTTIME_TABLE + "(" + FILE_UPLOAD_FIRSTTIME_COLUMN_ID
            + " INTEGER PRIMARY KEY," + FILE_UPLOAD_FIRSTTIME_MESSAGE_ID
            + " TEXT," + FILE_UPLOAD_FIRSTTIME_STATUS + " TEXT," + FILE_UPLOAD_FIRSTTIME_OBJECT + " TEXT)";
    String CREATE_UPLOAD_MAINTAIN = "CREATE TABLE " + FILE_UPLOAD_TABLE + "(" + FILE_UPLOAD_COLUMN_ID
            + " INTEGER PRIMARY KEY," + FILE_UPLOAD_MESSAGE_ID
            + " TEXT," + FILE_UPLOAD_STATUS + " TEXT," + FILE_UPLOAD_OBJECT + " TEXT)";


    String CREATE_USERPIC_MAINTAIN = "CREATE TABLE " + USERFILE_UPLOAD_TABLE + "(" + USERFILE_UPLOAD_COLUMN_ID
            + " INTEGER PRIMARY KEY," + USERFILE_UPLOAD_MESSAGE_ID
            + " TEXT," + USERFILE_UPLOAD_STATUS + " TEXT," + USERFILE_UPLOAD_OBJECT + " TEXT)";


    //--------------------------------Upload Maintain Table------------------------------


    String CREATE_TABLE_MESSAGE = "CREATE TABLE " + TABLE_MESSAGES + "(" + COLUMN_OID
            + " INTEGER PRIMARY KEY," + COLUMN_MESSAGE_ID + " TEXT," + COLUMN_RECEIVER_ID
            + " TEXT," + COLUMN_CHAT_ID + " TEXT," + COLUMN_RECORD_ID + " TEXT," + COLUMN_MESSAGE_DATA
            + " TEXT," + COLUMN_DELIVERY_STATUS + " TEXT," + COLUMN_IS_SELF + " INTEGER,"
            + COLUMN_CHAT_TYPE + " TEXT," + COLUMN_TIME_STAMP + " INTEGER," + COLUMN_STATUS + " INTEGER)";

    String CREATE_MESSAGE_TABLE_INDEX = "CREATE UNIQUE INDEX " + INDEX_MESSAGES + " ON "
            + TABLE_MESSAGES + " (" + COLUMN_MESSAGE_ID + ")";

    String CREATE_TABLE_CHAT_LIST = "CREATE TABLE " + TABLE_CHAT_LIST + "(" + COLUMN_OID
            + " INTEGER PRIMARY KEY," + COLUMN_RECEIVER_ID + " TEXT," + COLUMN_CHAT_ID
            + " TEXT," + COLUMN_MESSAGE_DATA + " TEXT," + COLUMN_CHAT_TYPE + " TEXT,"
            + COLUMN_CLEAR_STATUS + " INTEGER," + COLUMN_TIME_STAMP + " INTEGER," + COLUMN_STATUS
            + " INTEGER)";

    String CREATE_TABLE_CHAT_LOCK = "CREATE TABLE " + TABLE_CHAT_LOCK + "(" + COLUMN_OID
            + " INTEGER PRIMARY KEY," + COLUMN_SENDER_ID + " TEXT," + COLUMN_RECEIVER_ID
            + " TEXT," + COLUMN_CHAT_TYPE + " TEXT," + COLUMN_PASSWORD + " TEXT,"
            + COLUMN_LOCK_STATUS + " TEXT," + COLUMN_STATUS + " INTEGER)";

    String CREATE_TABLE_TEMP_DELETE_MESSAGES = "CREATE TABLE " + TABLE_TEMP_DELETE_MESSAGES
            + "(" + COLUMN_OID + " INTEGER PRIMARY KEY," + COLUMN_RECORD_ID + " TEXT,"
            + COLUMN_DELETE_MESSAGE_DATA + " TEXT," + COLUMN_CHAT_TYPE + " TEXT," + COLUMN_STATUS
            + " INTEGER)";

    String CREATE_TABLE_TEMP_STAR_MESSAGES = "CREATE TABLE " + TABLE_TEMP_STAR_MESSAGES
            + "(" + COLUMN_OID + " INTEGER PRIMARY KEY," + COLUMN_RECORD_ID + " TEXT,"
            + COLUMN_STAR_MESSAGE_DATA + " TEXT," + COLUMN_CHAT_TYPE + " TEXT," + COLUMN_STATUS
            + " INTEGER)";

    String CREATE_TABLE_TEMP_SEND_NEW_MESSAGE = "CREATE TABLE " + TABLE_TEMP_SEND_NEW_MESSAGE + "("
            + COLUMN_OID + " INTEGER PRIMARY KEY," + COLUMN_EVENT_ID + " TEXT," + COLUMN_EVENT_NAME
            + " TEXT," + COLUMN_EVENT_DATA + " TEXT," + COLUMN_STATUS + " INTEGER)";

    String CREATE_TABLE_OFFLINE_EVENTS = "CREATE TABLE " + TABLE_OFFLINE_EVENTS + "("
            + COLUMN_OID + " INTEGER PRIMARY KEY," + COLUMN_EVENT_ID + " TEXT," + COLUMN_EVENT_NAME
            + " TEXT," + COLUMN_EVENT_DATA + " TEXT," + COLUMN_STATUS + " INTEGER)";

    String CREATE_TABLE_CALL_LOGS = "CREATE TABLE " + TABLE_CALL_LOGS + "(" + COLUMN_OID
            + " INTEGER PRIMARY KEY," + COLUMN_CALL_ID + " TEXT," + COLUMN_CALL_DATA + " TEXT,"
            + COLUMN_STATUS + " INTEGER)";


    private Context mContext;
    private Gson gson;
    private GsonBuilder gsonBuilder;
    private String mCurrentUserId;

    private SQLiteDatabase mDatabaseInstance;

    public MessageDbController(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

        this.mContext = context;
        gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    private SQLiteDatabase getDatabaseInstance() {
        if (mDatabaseInstance == null) {
            mDatabaseInstance = getWritableDatabase();
        }

        if (!mDatabaseInstance.isOpen()) {
            mDatabaseInstance = getWritableDatabase();
        }

        return mDatabaseInstance;
    }

    public void close() {
        if (mDatabaseInstance != null && mDatabaseInstance.isOpen()) {
            mDatabaseInstance.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_MESSAGE);
        //db.execSQL(INDEX_MESSAGES);
        db.execSQL(CREATE_TABLE_CHAT_LIST);
        db.execSQL(CREATE_TABLE_CHAT_LOCK);
        db.execSQL(CREATE_TABLE_TEMP_DELETE_MESSAGES);
        db.execSQL(CREATE_TABLE_TEMP_STAR_MESSAGES);
        db.execSQL(CREATE_TABLE_CALL_LOGS);
        db.execSQL(CREATE_TABLE_TEMP_SEND_NEW_MESSAGE);
        db.execSQL(CREATE_TABLE_OFFLINE_EVENTS);
        db.execSQL(CREATE_UPLOAD_MAINTAIN);
        db.execSQL(CREATE_UPLOAD_FIRSTTIME_TABLE);

        db.execSQL(CREATE_USERPIC_MAINTAIN);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHAT_LIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHAT_LOCK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEMP_DELETE_MESSAGES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEMP_STAR_MESSAGES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CALL_LOGS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEMP_SEND_NEW_MESSAGE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFLINE_EVENTS);
        db.execSQL("DROP TABLE IF EXISTS " + FILE_UPLOAD_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + FILE_UPLOAD_FIRSTTIME_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + USERFILE_UPLOAD_TABLE);


        // Create tables again
        onCreate(db);
    }

    public void updateChatMessage(MessageItemChat item, String chatType) {

        String selectQuery = "SELECT " + COLUMN_OID + " FROM " + TABLE_MESSAGES + " WHERE " +
                COLUMN_MESSAGE_ID + "='" + item.getMessageId() + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);
        Log.d(TAG, "updateChatMessage2 tickMiss status: " + item.getDeliveryStatus());
        Log.d(TAG, "updateChatMessage2 tickMiss msgId: " + item.getMessageId());
        if (selectCur != null) {
            if (selectCur.getCount() > 0) {

                selectCur.close();
                updateMessage(item);
            } else {

                selectCur.close();
                insertNewMessage(item, chatType, 0L, null);
            }
        }
    }

    private void updateMessage(MessageItemChat item) {
        String jsonMessage = gson.toJson(item);
        ContentValues updateValues = new ContentValues();
        updateValues.put(COLUMN_MESSAGE_DATA, jsonMessage);
        updateValues.put(COLUMN_DELIVERY_STATUS, item.getDeliveryStatus());

        String recordId = item.getRecordId();
        if (recordId != null && !recordId.equals("")) {
            updateValues.put(COLUMN_RECORD_ID, item.getRecordId());
        }

        getDatabaseInstance().update(TABLE_MESSAGES, updateValues, COLUMN_MESSAGE_ID + "='" + item.getMessageId() + "'", null);
    }


    private void insertNewMessage(MessageItemChat message, String chatType, long updatedTs, String encryptionMsgChatId) {
        if (message == null) {
            MyLog.e(TAG, "insertNewMessage: message null.. check ");
            return;
        }
        MyLog.d(TAG, "duplicatetest insertNewMessage: ");
        String msgData = gson.toJson(message);
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
        boolean isBlockedMine = false;
        if (contactDB_sqlite.getBlockedMineStatus(message.getReceiverID(), false).equals(ContactDB_Sqlite.BLOCKED_STATUS)) {
            isBlockedMine = true;
        }
        message.setBlockedMsg(isBlockedMine);
        String receiverId = message.getReceiverID();
        String chatId = getChatId(receiverId, chatType);

        if (encryptionMsgChatId != null && !encryptionMsgChatId.isEmpty()) {
            chatId = encryptionMsgChatId;
        }
        Long ts = AppUtils.parseLong(message.getTS());
        if (updatedTs > 0) {
            ts = updatedTs;
        }

        ContentValues values = new ContentValues();
        values.put(COLUMN_MESSAGE_ID, message.getMessageId());
        values.put(COLUMN_RECEIVER_ID, message.getReceiverID());
        values.put(COLUMN_CHAT_ID, chatId);
        values.put(COLUMN_MESSAGE_DATA, msgData);
        values.put(COLUMN_DELIVERY_STATUS, message.getDeliveryStatus());
        values.put(COLUMN_CHAT_TYPE, chatType);
        values.put(COLUMN_TIME_STAMP, ts);
        values.put(COLUMN_STATUS, 1);

        String recordId = message.getRecordId();
        if (recordId != null && !recordId.equals("")) {
            values.put(COLUMN_RECORD_ID, message.getRecordId());
        }

        if (message.isSelf()) {
            values.put(COLUMN_IS_SELF, 1);
        } else {
            values.put(COLUMN_IS_SELF, 0);
        }
        // Inserting Row
        getDatabaseInstance().insert(TABLE_MESSAGES, null, values);
        updateChatList(message.getReceiverID(), msgData, chatType, message.getTS());
        Log.d(TAG, "groupmsgmiss insertNewMessage: ");
    }

    public ArrayList<MessageItemChat> selectAllChatMessages(String chatId, String chatType) {
        // Chat id referred by below things ----> single_chat(from-to), group_chat(from-to-g), secret_chat(from-to-secret)


        String query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_CHAT_ID + "='" + chatId
                /*+ "' AND " + COLUMN_CHAT_TYPE + "='" + chatType */ + "' ORDER BY 1 DESC";

        ArrayList<MessageItemChat> chats = new ArrayList<>();
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String data = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_DATA));
                MessageItemChat messageItemChat = gson.fromJson(data, MessageItemChat.class);
                chats.add(messageItemChat);
            }
            cursor.close();
        }
        return chats;
    }

    public int getMessageCount(String msgId) {


        String query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_MESSAGE_ID + "='" + msgId + "'";


        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            int count = cursor.getCount();
            cursor.close();
            return count;
        }
        return 0;
    }

    public int getChatListCount(String chatId) {
        try {
            Cursor mCount = getDatabaseInstance().rawQuery("select count(*) from " + TABLE_MESSAGES + " where " + COLUMN_CHAT_ID + "='" + chatId + "'", null);
            mCount.moveToFirst();
            int count = mCount.getCount();
            mCount.close();
            return count;

        } catch (Exception e) {
            MyLog.e(TAG, "getMessageCount: ", e);
        }

        return 0;
    }

    public ArrayList<MessageItemChat> selectAllMessagesWithLimit(String chatId, String chatType, String ts, int limit) {
        // Chat id referred by below things ----> single_chat(from-to), group_chat(from-to-g), secret_chat(from-to-secret)

        /*String query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_CHAT_ID + "='" + chatId
                + "' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "' ORDER BY 1 DESC";*/
        MyLog.d(TAG, "selectAllMessagesWithLimit: start");

        String query;
        long timeStamp;
        try {
            timeStamp = AppUtils.parseLong(ts);
        } catch (NumberFormatException e) {
            timeStamp = 0;
        }

        if (timeStamp == 0) {
            query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_CHAT_ID + "='" + chatId
                    + "' ORDER BY 1 DESC LIMIT " + limit;
        } else {
      /*      query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_CHAT_ID + "='" + chatId
                    + "' AND " + COLUMN_TIME_STAMP + "<" + timeStamp + " ORDER BY 1 DESC LIMIT " + limit;
    */

            query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_CHAT_ID + "='" + chatId
                    + "' ORDER BY 1 DESC LIMIT " + limit;

        }

        ArrayList<MessageItemChat> chats = new ArrayList<>();
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String data = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_DATA));
                MessageItemChat messageItemChat = gson.fromJson(data, MessageItemChat.class);
                chats.add(messageItemChat);
            }
            cursor.close();
        }
        MyLog.d(TAG, "selectAllMessagesWithLimit: end");
        return chats;
    }


    public MessageItemChat getFirstMsgTimeStamp(String chatType, String chatId) {
        String query = "SELECT * FROM " + TABLE_MESSAGES + " ORDER BY CAST(" + COLUMN_TIME_STAMP + " AS INTEGER) ASC LIMIT 1";
        MessageItemChat msgItem = new MessageItemChat();
        long ts = 0;
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {

            if (cursor.moveToNext()) {
                String msgData = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_DATA));
                ts = cursor.getLong(cursor.getColumnIndex(COLUMN_TIME_STAMP));
                MyLog.d(TAG, "getFirstMsgTimeStamp: ts: " + ts);
                msgItem = gson.fromJson(msgData, MessageItemChat.class);
            }
            cursor.close();
        }


        if (msgItem != null) {
            msgItem.setMessageType("" + MessageFactory.ENCRYPTION_INFO);
            msgItem.setMessageId(msgItem.getMessageId() + "_encryption");
            msgItem.setRecordId(msgItem.getRecordId() + "_encryption");
            if (ts == 0) {
                ts = System.currentTimeMillis();
                //  insertNewMessage(msgItem, chatType,ts,chatId);
                msgItem.setTS("" + ts);
            } else {
                ts = ts - 10;
                msgItem.setTS("" + ts);
            }

        }
        return msgItem;
    }


    // Starred msg fixed
    public ArrayList<MessageItemChat> selectAllMessagesWithLimit_again(String chatId, String chatType, String ts, int limit) {

        String query;
        long timeStamp;
        try {
            timeStamp = AppUtils.parseLong(ts);
        } catch (NumberFormatException e) {
            timeStamp = 0;
        }

        if (timeStamp == 0) {
            query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_CHAT_ID + "='" + chatId
                    + "' ORDER BY 1 DESC LIMIT " + limit;
        } else {
            query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_CHAT_ID + "='" + chatId
                    + "' AND " + COLUMN_TIME_STAMP + ">" + timeStamp + " ORDER BY 1 ASC LIMIT " + limit;
        }

        ArrayList<MessageItemChat> chats = new ArrayList<>();
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String data = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_DATA));
                MessageItemChat messageItemChat = gson.fromJson(data, MessageItemChat.class);
                chats.add(messageItemChat);
            }
            cursor.close();
        }
        return chats;
    }

    private void updateChatList(String receiverId, String msgData, String chatType, String ts) {
        Log.d(TAG, "updateChatList: groupmsgmiss");
        String selectQuery = "SELECT * FROM " + TABLE_CHAT_LIST + " WHERE " + COLUMN_CHAT_ID + " LIKE '" + mCurrentUserId
                + "%' AND " + COLUMN_RECEIVER_ID + "='" + receiverId + "' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        Long timeStamp = AppUtils.parseLong(ts);

        if (timeStamp == 0) {
            timeStamp = System.currentTimeMillis();
        }
        if (selectCur != null) {
            if (selectCur.moveToFirst()) {
                int rowId = selectCur.getInt(selectCur.getColumnIndex(COLUMN_OID));

                ContentValues updateValues = new ContentValues();
                updateValues.put(COLUMN_MESSAGE_DATA, msgData);
                updateValues.put(COLUMN_CLEAR_STATUS, MessageFactory.CHAT_STATUS_UNCLEARED);
                updateValues.put(COLUMN_TIME_STAMP, timeStamp);
                /*updateValues.put(COLUMN_CHAT_TYPE, chatType);
                updateValues.put(COLUMN_RECEIVER_ID, receiverId);
                updateValues.put(COLUMN_TIME_STAMP, ts);
                updateValues.put(COLUMN_STATUS, 1);*/

                getDatabaseInstance().update(TABLE_CHAT_LIST, updateValues, COLUMN_OID + "=" + rowId, null);
                selectCur.close();
            } else {
                selectCur.close();

                String chatId = getChatId(receiverId, chatType);

                ContentValues chatListValues = new ContentValues();
                chatListValues.put(COLUMN_RECEIVER_ID, receiverId);
                chatListValues.put(COLUMN_CHAT_ID, chatId);
                chatListValues.put(COLUMN_CHAT_TYPE, chatType);
                chatListValues.put(COLUMN_CLEAR_STATUS, MessageFactory.CHAT_STATUS_UNCLEARED);
                chatListValues.put(COLUMN_MESSAGE_DATA, msgData);
                chatListValues.put(COLUMN_TIME_STAMP, timeStamp);
                chatListValues.put(COLUMN_STATUS, 1);

                getDatabaseInstance().insert(TABLE_CHAT_LIST, null, chatListValues);
            }
        }
    }

    public int getChatClearedStatus(String receiverId, String chatType) {
        String selectQuery = "SELECT " + COLUMN_OID + "," + COLUMN_CLEAR_STATUS + " FROM " + TABLE_CHAT_LIST + " WHERE " +
                COLUMN_RECEIVER_ID + "='" + receiverId + "' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        int clearedStatus = MessageFactory.CHAT_STATUS_UNCLEARED;

        if (selectCur != null) {
            if (selectCur.moveToNext()) {
                int id = selectCur.getInt(selectCur.getColumnIndex(COLUMN_OID));
                clearedStatus = selectCur.getInt(selectCur.getColumnIndex(COLUMN_CLEAR_STATUS));
            }
            selectCur.close();
        }

        return clearedStatus;
    }

    public void updateChatListStatus(String receiverId, String chatType, int status) {

        String selectQuery = "SELECT " + COLUMN_OID + " FROM " + TABLE_CHAT_LIST + " WHERE " +
                COLUMN_RECEIVER_ID + "='" + receiverId + "' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.moveToFirst()) {
                int rowId = selectCur.getInt(selectCur.getColumnIndex(COLUMN_OID));
                selectCur.close();

                ContentValues updateValues = new ContentValues();
                updateValues.put(COLUMN_CLEAR_STATUS, status);

                getDatabaseInstance().update(TABLE_CHAT_LIST, updateValues, COLUMN_OID + "=" + rowId, null);
            } else {
                selectCur.close();
            }
        }
    }

    public ArrayList<MessageItemChat> selectChatList(String chatType) {
        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        String query = "SELECT * FROM " + TABLE_CHAT_LIST + " WHERE " + COLUMN_CHAT_ID + " LIKE '" + mCurrentUserId
                + "%' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";

        ArrayList<MessageItemChat> chats = new ArrayList<>();
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        try {
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String data = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_DATA));
                    MessageItemChat messageItemChat = gson.fromJson(data, MessageItemChat.class);
                    chats.add(messageItemChat);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "selectChatList: ", e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }

        }
        return chats;
    }

    public ArrayList<MessageItemChat> selectAllStarredMessages() {
        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        String query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_CHAT_ID + " LIKE '" + mCurrentUserId
                + "%' AND " + COLUMN_CHAT_TYPE + "!='" + MessageFactory.CHAT_TYPE_SECRET + "'";

        ArrayList<MessageItemChat> chats = new ArrayList<>();
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        try {
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String data = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_DATA));
                    MessageItemChat messageItemChat = gson.fromJson(data, MessageItemChat.class);
                    if (messageItemChat != null && messageItemChat.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED + "")) {
                        chats.add(messageItemChat);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "selectAllStarredMessages: ", e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return chats;
    }

    public MessageItemChat getMessageByRecordId(String recordId) {

        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        String query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_RECORD_ID + "='"
                + recordId + "'";

        MessageItemChat chat = null;
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            if (cursor.moveToNext()) {
                String data = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_DATA));
                chat = gson.fromJson(data, MessageItemChat.class);
            }
            cursor.close();
        }
        return chat;
    }

    public void updateSendNewMessage(OfflineRetryEventPojo pojo) {
        ContentValues offlineMsgValues = new ContentValues();
        offlineMsgValues.put(COLUMN_EVENT_ID, pojo.getEventId());
        offlineMsgValues.put(COLUMN_EVENT_NAME, pojo.getEventName());
        offlineMsgValues.put(COLUMN_EVENT_DATA, pojo.getEventObject().toString());
        offlineMsgValues.put(COLUMN_STATUS, 1);

        getDatabaseInstance().insert(TABLE_TEMP_SEND_NEW_MESSAGE, null, offlineMsgValues);
    }

    public ArrayList<OfflineRetryEventPojo> getSendNewMessage(String currentUserId) {
        String query = "SELECT * FROM " + TABLE_TEMP_SEND_NEW_MESSAGE + " WHERE " + COLUMN_EVENT_ID
                + " LIKE '" + currentUserId + "%'";

        ArrayList<OfflineRetryEventPojo> offlineMessages = new ArrayList<>();
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String eventId = cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_ID));
                String eventName = cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_NAME));
                String eventData = cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_DATA));

                try {
                    JSONObject object = new JSONObject(eventData);

                    OfflineRetryEventPojo pojo = new OfflineRetryEventPojo();
                    pojo.setEventId(eventId);
                    pojo.setEventName(eventName);
                    pojo.setEventObject(object);
                    offlineMessages.add(pojo);
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }
            cursor.close();
        }

        return offlineMessages;
    }

    public void deleteSendNewMessage(String msgId) {
        String query = "SELECT " + COLUMN_OID + " FROM " + TABLE_TEMP_SEND_NEW_MESSAGE + " WHERE "
                + COLUMN_EVENT_ID + "='" + msgId + "'";

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_TEMP_SEND_NEW_MESSAGE, COLUMN_OID + "=" + id, null);
            }
            cursor.close();
        }

    }

    public void updateOfflineEvents(OfflineRetryEventPojo pojo) {
        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        String eventId = mCurrentUserId + "-" + Calendar.getInstance().getTimeInMillis();

        ContentValues offlineEventValues = new ContentValues();
        offlineEventValues.put(COLUMN_EVENT_ID, eventId);
        offlineEventValues.put(COLUMN_EVENT_NAME, pojo.getEventName());
        offlineEventValues.put(COLUMN_EVENT_DATA, pojo.getEventObject().toString());
        offlineEventValues.put(COLUMN_STATUS, 1);

        getDatabaseInstance().insert(TABLE_OFFLINE_EVENTS, null, offlineEventValues);
    }

    public ArrayList<OfflineRetryEventPojo> getOfflineEvents(String currentUserId) {
        String query = "SELECT * FROM " + TABLE_OFFLINE_EVENTS + " WHERE " + COLUMN_EVENT_ID
                + " LIKE '" + currentUserId + "%'";

        ArrayList<OfflineRetryEventPojo> offlineEvents = new ArrayList<>();
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String eventId = cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_ID));
                String eventName = cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_NAME));
                String eventData = cursor.getString(cursor.getColumnIndex(COLUMN_EVENT_DATA));

                try {
                    JSONObject object = new JSONObject(eventData);

                    OfflineRetryEventPojo pojo = new OfflineRetryEventPojo();
                    pojo.setEventId(eventId);
                    pojo.setEventName(eventName);
                    pojo.setEventObject(object);
                    offlineEvents.add(pojo);
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }
            cursor.close();
        }

        return offlineEvents;
    }


    // Other functions
    public void updateChatLockData(String receiverId, String chatId, String status, String password, String chatType) {

        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();

        String selectQuery = "SELECT " + COLUMN_OID + " FROM " + TABLE_CHAT_LOCK + " WHERE " +
                COLUMN_SENDER_ID + "='" + mCurrentUserId + "' AND " + COLUMN_RECEIVER_ID
                + "='" + receiverId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.moveToNext()) {
                int id = selectCur.getInt(selectCur.getColumnIndex(COLUMN_OID));
                selectCur.close();

                ContentValues values = new ContentValues();
                values.put(COLUMN_PASSWORD, password);
                values.put(COLUMN_LOCK_STATUS, status);
                values.put(COLUMN_CHAT_TYPE, chatType);
                values.put(COLUMN_STATUS, 1);

                getDatabaseInstance().update(TABLE_CHAT_LOCK, values, COLUMN_OID + "=" + id, null);
            } else {
                selectCur.close();
                ContentValues values = new ContentValues();
                values.put(COLUMN_SENDER_ID, mCurrentUserId);
                values.put(COLUMN_RECEIVER_ID, receiverId);
                values.put(COLUMN_PASSWORD, password);
                values.put(COLUMN_CHAT_TYPE, chatType);
                values.put(COLUMN_LOCK_STATUS, status);
                values.put(COLUMN_STATUS, 1);

                getDatabaseInstance().insert(TABLE_CHAT_LOCK, null, values);
            }
        }

    }


    public ChatLockPojo getChatLockData(String receiverId, String chatType) {
        ChatLockPojo pojo = null;

        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();

        String query = "SELECT * FROM " + TABLE_CHAT_LOCK + " WHERE " + COLUMN_SENDER_ID + "='" + mCurrentUserId
                + "' AND " + COLUMN_RECEIVER_ID + "='" + receiverId + "' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            if (cursor.moveToNext()) {
                String password = cursor.getString(cursor.getColumnIndex(COLUMN_PASSWORD));
                String lockStatus = cursor.getString(cursor.getColumnIndex(COLUMN_LOCK_STATUS));

                pojo = new ChatLockPojo();
                pojo.setSenderId(mCurrentUserId);
                pojo.setReceiverId(receiverId);
                pojo.setPassword(password);
                pojo.setStatus(lockStatus);
            }
            cursor.close();
        }

        return pojo;
    }


    private String getChatId(String receiverId, String chatType) {
        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        String chatId = mCurrentUserId + "-" + receiverId;
        if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
            chatId = chatId + "-g";
        } else if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_SECRET)) {
            chatId = chatId + "-secret";
        }
        return chatId;
    }

    public void deleteChat(String chatId, String chatType) {
        String query = "SELECT " + COLUMN_OID + " FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_CHAT_ID + "='" + chatId
                + "' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_MESSAGES, COLUMN_OID + "=" + id, null);
            }
            cursor.close();
        }

        String chatListQuery = "SELECT " + COLUMN_OID + " FROM " + TABLE_CHAT_LIST + " WHERE " + COLUMN_CHAT_ID + "='"
                + chatId + "' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";

        Cursor listCursor = getDatabaseInstance().rawQuery(chatListQuery, null);
        if (listCursor != null) {
            if (listCursor.moveToNext()) {
                int id = listCursor.getInt(listCursor.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_CHAT_LIST, COLUMN_OID + "=" + id, null);
            }
            listCursor.close();
        }
    }

    public void deleteChatMessage(String chatId, String msgId, String chatType) {
        String query = "SELECT " + COLUMN_OID + " FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_MESSAGE_ID + "='" + msgId
                + "' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_MESSAGES, COLUMN_OID + "=" + id, null);
            }

            String topMsgQuery = "SELECT * FROM " + TABLE_MESSAGES + " WHERE "
                    + COLUMN_CHAT_ID + "='" + chatId + "' ORDER BY 1 DESC LIMIT 1";
            Cursor topMsgCursor = getDatabaseInstance().rawQuery(topMsgQuery, null);
            if (topMsgCursor.moveToNext()) {
                int id = topMsgCursor.getInt(topMsgCursor.getColumnIndex(COLUMN_OID));
                String msgData = topMsgCursor.getString(topMsgCursor.getColumnIndex(COLUMN_MESSAGE_DATA));
                MessageItemChat msgItem = gson.fromJson(msgData, MessageItemChat.class);
                updateChatList(msgItem.getReceiverID(), msgData, chatType, msgItem.getTS());
            } else {
                if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                    String groupId = chatId.split("-")[1];
                    createNewGroupChatList(groupId, chatId);
                } else {
                    deleteChat(chatId, chatType);
                }
            }
            topMsgCursor.close();

            cursor.close();
        }
    }


    public void clearUnStarredMessage(String chatId, String receiverId, String chatType) {
        String query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_CHAT_ID + " LIKE '"
                + chatId + "%' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";

        String lastMsg = null, lastMsgTS = null;

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                String msgData = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_DATA));
                MessageItemChat msgItem = gson.fromJson(msgData, MessageItemChat.class);
                if (msgItem.getStarredStatus().equals(MessageFactory.MESSAGE_UN_STARRED)) {
                    getDatabaseInstance().delete(TABLE_MESSAGES, COLUMN_OID + "=" + id, null);
                } else {
                    lastMsg = msgData;
                    lastMsgTS = msgItem.getTS();
                }
            }
            cursor.close();
        }

        if (lastMsg != null && !lastMsg.equals("")) {
            updateChatList(receiverId, lastMsg, chatType, lastMsgTS);
        }

    }

    public void clearAllGroupChatMessage(String chatId, String receiverId) {
        String query = "SELECT " + COLUMN_OID + " FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_CHAT_ID + " LIKE '"
                + chatId + "%'";

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_MESSAGES, COLUMN_OID + "=" + id, null);
            }
            cursor.close();
        }

        updateChatListStatus(receiverId, MessageFactory.CHAT_TYPE_GROUP, MessageFactory.CHAT_STATUS_CLEARED);
    }

    public void clearAllSingleChatMessage(String chatId, String receiverId) {
        String query = "SELECT " + COLUMN_OID + " FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_CHAT_ID + " LIKE '"
                + chatId + "%'";

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_MESSAGES, COLUMN_OID + "=" + id, null);
            }
            cursor.close();
        }

        updateChatListStatus(receiverId, MessageFactory.CHAT_TYPE_SINGLE, MessageFactory.CHAT_STATUS_CLEARED);
    }


    public void clearAllUnStarredSingleChatMessagewithTime(long mTimeStamp, String mMessageId, String chatType) {
        //Delete clear chat with unstarred ChatMessage Table
        String lastMsg = null, lastMsgTS = null;

        String query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_TIME_STAMP + "<="
                + getTimeStamp(mMessageId) + " AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";
        //  String query = "delete from " + TABLE_MESSAGES + " WHERE " + COLUMN_TIME_STAMP + "<=" + mTimeStamp + COLUMN_TIME_STAMP + "<=" + mTimeStamp;

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                String msgData = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_DATA));

                MessageItemChat msgItem = gson.fromJson(msgData, MessageItemChat.class);
                if (msgItem.getStarredStatus().equals(MessageFactory.MESSAGE_UN_STARRED)) {
                    getDatabaseInstance().delete(TABLE_MESSAGES, COLUMN_OID + "=" + id, null);
                } else {
                    lastMsg = msgData;
                    lastMsgTS = msgItem.getTS();
                }
            }
            cursor.close();
        }


        if (lastMsg != null && !lastMsg.equals("")) {
            MyLog.e("lastMsg", "lastMsg" + lastMsg);
            //    updateChatList(receiverId, lastMsg, chatType, lastMsgTS);
            String selectQuery = "SELECT * FROM " + TABLE_CHAT_LIST + " WHERE " + COLUMN_CHAT_ID + " LIKE '" + mCurrentUserId
                    + "%'AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";
            Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);
            Long timeStamp = AppUtils.parseLong(lastMsgTS);
            if (timeStamp == 0) {
                timeStamp = System.currentTimeMillis();
            }
            if (selectCur != null) {
                if (selectCur.moveToFirst()) {
                    MyLog.e("lastMsg", "moveToFirst" + lastMsg);
                    int rowId = selectCur.getInt(selectCur.getColumnIndex(COLUMN_OID));

                    ContentValues updateValues = new ContentValues();
                    updateValues.put(COLUMN_MESSAGE_DATA, lastMsg);
                    updateValues.put(COLUMN_CLEAR_STATUS, MessageFactory.CHAT_STATUS_UNCLEARED);
                    updateValues.put(COLUMN_TIME_STAMP, timeStamp);
                    updateValues.put(COLUMN_CHAT_TYPE, chatType);


                    getDatabaseInstance().update(TABLE_CHAT_LIST, updateValues, COLUMN_OID + "=" + rowId, null);
                    selectCur.close();
                } else {
                    selectCur.close();
                    MyLog.e("lastMsg", "INSERT" + lastMsg);

                    //   String chatId = getChatId(receiverId, chatType);

                    ContentValues chatListValues = new ContentValues();
                    //  chatListValues.put(COLUMN_RECEIVER_ID, receiverId);
                    //   chatListValues.put(COLUMN_CHAT_ID, chatId);
                    chatListValues.put(COLUMN_CHAT_TYPE, chatType);
                    chatListValues.put(COLUMN_CLEAR_STATUS, MessageFactory.CHAT_STATUS_UNCLEARED);
                    chatListValues.put(COLUMN_MESSAGE_DATA, lastMsg);
                    chatListValues.put(COLUMN_TIME_STAMP, timeStamp);
                    chatListValues.put(COLUMN_STATUS, 1);

                    getDatabaseInstance().insert(TABLE_CHAT_LIST, null, chatListValues);
                }
            }
        } else {
            MyLog.e("lastMsg", "lastMsg empty" + lastMsg);
//Clear TablechatList if no message is starred
//            String queryy = "SELECT * FROM " + TABLE_CHAT_LIST + " WHERE " + COLUMN_TIME_STAMP + "<=" + mTimeStamp;
            String queryy = "SELECT * FROM " + TABLE_CHAT_LIST + " WHERE " + COLUMN_CHAT_TYPE + "='" + MessageFactory.CHAT_TYPE_SINGLE + "'";

            Cursor cursorr = getDatabaseInstance().rawQuery(queryy, null);
            if (cursorr != null) {
                if (cursorr.moveToLast()) {
                    MyLog.e(TAG, "moveToLast" + mTimeStamp);
                    int id = cursorr.getInt(cursorr.getColumnIndex(COLUMN_OID));
                    String msgData = cursorr.getString(cursorr.getColumnIndex(COLUMN_MESSAGE_DATA));
                    MessageItemChat msgItem = gson.fromJson(msgData, MessageItemChat.class);
                    msgItem.setTextMessage("");
                    msgItem.setisclearchat(true);

                    MyLog.e(TAG, "LAST" + msgItem.getTS());
                    String modifiedData = gson.toJson(msgItem);
                    ContentValues updateValues = new ContentValues();
                    updateValues.put(COLUMN_MESSAGE_DATA, modifiedData);
                    getDatabaseInstance().update(TABLE_CHAT_LIST, updateValues, COLUMN_OID + "=" + id, null);

                }

            }

            cursorr.close();
        }

/*
//Clear TablechatList if no message is starred

        String queryy = "SELECT * FROM " + TABLE_CHAT_LIST + " WHERE " + COLUMN_CHAT_TYPE + "='" + MessageFactory.CHAT_TYPE_SINGLE + "'";
        ;
        Cursor cursorr = getDatabaseInstance().rawQuery(queryy, null);
        if (cursorr != null) {
            if (cursorr.moveToLast()) {
                MyLog.e(TAG, "TABLE_CHAT_LIST last" + mTimeStamp);
                int id = cursorr.getInt(cursorr.getColumnIndex(COLUMN_OID));
                String msgData = cursorr.getString(cursorr.getColumnIndex(COLUMN_MESSAGE_DATA));
                MessageItemChat msgItem = gson.fromJson(msgData, MessageItemChat.class);
                msgItem.setTextMessage("");
                msgItem.setisclearchat(true);

                MyLog.e(TAG, "TABLE_CHAT_LIST last" + msgItem.getTS());
                String modifiedData = gson.toJson(msgItem);
                ContentValues updateValues = new ContentValues();
                updateValues.put(COLUMN_MESSAGE_DATA, modifiedData);
                getDatabaseInstance().update(TABLE_CHAT_LIST, updateValues, COLUMN_OID + "=" + id, null);

            }

        }
        cursorr.close();*/

    }

    public String getTimeStamp(String mMessageId) {
        Cursor cursor = null;
        try {
            String query = "SELECT " + COLUMN_TIME_STAMP + " FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_MESSAGE_ID + "='" + mMessageId + "'";
            String result = "";
            //  String queryStudent = "SELECT students.color FROM students, comments WHERE comments.author = students.name;";
            cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor.moveToFirst()) {
                // result = cursor.getString(cursor.getColumnIndex("students.color"));
                result = "" + cursor.getString(0);
                return result;
            }

        } catch (Exception e) {
            Log.e(TAG, "getTimeStamp: ", e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return "";
    }

    public void clearAllSingleChatMessagewithTimeMessage(long mTimeStamp, String mMessageId) {
        //Delete clear chat with ChatMessage Table
        MyLog.e(TAG, "clearAllSingleChatMessagewithTime" + mTimeStamp);

        String lastMsg = null, lastMsgTS = null;

        String timeStampStr = getTimeStamp(mMessageId);
        if (timeStampStr == null || AppUtils.isEmpty(timeStampStr))
            return;

        String query = "delete from " + TABLE_MESSAGES + " WHERE " + COLUMN_TIME_STAMP + "<=" + getTimeStamp(mMessageId);

        Cursor cursore = getDatabaseInstance().rawQuery(query, null);
        if (cursore != null) {
            while (cursore.moveToNext()) {
                int idd = cursore.getInt(cursore.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_MESSAGES, COLUMN_OID + "=" + idd, null);
            }
            cursore.close();
        }
//Clear TablechatList if no message is starred

        String queryy = "SELECT * FROM " + TABLE_CHAT_LIST + " WHERE " + COLUMN_CHAT_TYPE + "='" + MessageFactory.CHAT_TYPE_SINGLE + "'";
        ;
        Cursor cursorr = getDatabaseInstance().rawQuery(queryy, null);
        if (cursorr != null) {
            if (cursorr.moveToLast()) {
                MyLog.e(TAG, "TABLE_CHAT_LIST last" + mTimeStamp);
                int id = cursorr.getInt(cursorr.getColumnIndex(COLUMN_OID));
                String msgData = cursorr.getString(cursorr.getColumnIndex(COLUMN_MESSAGE_DATA));
                MessageItemChat msgItem = gson.fromJson(msgData, MessageItemChat.class);
                msgItem.setTextMessage("");
                msgItem.setisclearchat(true);

                MyLog.e(TAG, "TABLE_CHAT_LIST last" + msgItem.getTS());
                String modifiedData = gson.toJson(msgItem);
                ContentValues updateValues = new ContentValues();
                updateValues.put(COLUMN_MESSAGE_DATA, modifiedData);
                getDatabaseInstance().update(TABLE_CHAT_LIST, updateValues, COLUMN_OID + "=" + id, null);

            }

        }
        cursorr.close();
    }

    public void clearAllUnStarredSingleChatMessagewithTime(long mTimeStamp, String chatType) {
        //Delete clear chat with unstarred ChatMessage Table
        String lastMsg = null, lastMsgTS = null;

        String query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_TIME_STAMP + "<="
                + mTimeStamp + " AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";
        //  String query = "delete from " + TABLE_MESSAGES + " WHERE " + COLUMN_TIME_STAMP + "<=" + mTimeStamp + COLUMN_TIME_STAMP + "<=" + mTimeStamp;

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                String msgData = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_DATA));

                MessageItemChat msgItem = gson.fromJson(msgData, MessageItemChat.class);
                if (msgItem.getStarredStatus().equals(MessageFactory.MESSAGE_UN_STARRED)) {
                    getDatabaseInstance().delete(TABLE_MESSAGES, COLUMN_OID + "=" + id, null);
                } else {
                    lastMsg = msgData;
                    lastMsgTS = msgItem.getTS();
                }
            }
            cursor.close();
        }


        if (lastMsg != null && !lastMsg.equals("")) {
            MyLog.e("lastMsg", "lastMsg" + lastMsg);
            //    updateChatList(receiverId, lastMsg, chatType, lastMsgTS);
            String selectQuery = "SELECT * FROM " + TABLE_CHAT_LIST + " WHERE " + COLUMN_CHAT_ID + " LIKE '" + mCurrentUserId
                    + "%'AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";
            Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);
            Long timeStamp = AppUtils.parseLong(lastMsgTS);
            if (timeStamp == 0) {
                timeStamp = System.currentTimeMillis();
            }
            if (selectCur != null) {
                if (selectCur.moveToFirst()) {
                    int rowId = selectCur.getInt(selectCur.getColumnIndex(COLUMN_OID));

                    ContentValues updateValues = new ContentValues();
                    updateValues.put(COLUMN_MESSAGE_DATA, lastMsg);
                    updateValues.put(COLUMN_CLEAR_STATUS, MessageFactory.CHAT_STATUS_UNCLEARED);
                    updateValues.put(COLUMN_TIME_STAMP, timeStamp);
                    updateValues.put(COLUMN_CHAT_TYPE, chatType);

                /*updateValues.put(COLUMN_CHAT_TYPE, chatType);
                updateValues.put(COLUMN_RECEIVER_ID, receiverId);
                updateValues.put(COLUMN_TIME_STAMP, ts);
                updateValues.put(COLUMN_STATUS, 1);*/

                    getDatabaseInstance().update(TABLE_CHAT_LIST, updateValues, COLUMN_OID + "=" + rowId, null);
                    selectCur.close();
                } else {
                    selectCur.close();

                    //   String chatId = getChatId(receiverId, chatType);

                    ContentValues chatListValues = new ContentValues();
                    //  chatListValues.put(COLUMN_RECEIVER_ID, receiverId);
                    //   chatListValues.put(COLUMN_CHAT_ID, chatId);
                    chatListValues.put(COLUMN_CHAT_TYPE, chatType);
                    chatListValues.put(COLUMN_CLEAR_STATUS, MessageFactory.CHAT_STATUS_UNCLEARED);
                    chatListValues.put(COLUMN_MESSAGE_DATA, lastMsg);
                    chatListValues.put(COLUMN_TIME_STAMP, timeStamp);
                    chatListValues.put(COLUMN_STATUS, 1);

                    getDatabaseInstance().insert(TABLE_CHAT_LIST, null, chatListValues);
                }
            }
        }
    }

    public void clearAllSingleChatMessagewithTime(long mTimeStamp) {
        //Delete clear chat with ChatMessage Table
        MyLog.e(TAG, "clearAllSingleChatMessagewithTime" + mTimeStamp);
        String query = "delete from " + TABLE_MESSAGES + " WHERE " + COLUMN_TIME_STAMP + "<=" + mTimeStamp;

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_MESSAGES, COLUMN_OID + "=" + id, null);
            }
            cursor.close();
        }


        String queryy = "SELECT * FROM " + TABLE_CHAT_LIST + " WHERE " + COLUMN_TIME_STAMP + "<=" + mTimeStamp;
        Cursor cursorr = getDatabaseInstance().rawQuery(queryy, null);
        if (cursorr != null) {
            if (cursorr.moveToLast()) {
                MyLog.e(TAG, "moveToLast" + mTimeStamp);
                int id = cursorr.getInt(cursorr.getColumnIndex(COLUMN_OID));
                String msgData = cursorr.getString(cursorr.getColumnIndex(COLUMN_MESSAGE_DATA));
                MessageItemChat msgItem = gson.fromJson(msgData, MessageItemChat.class);
                msgItem.setTextMessage("");
                MyLog.e(TAG, "LAST" + msgItem.getTS());
                String modifiedData = gson.toJson(msgItem);
                ContentValues updateValues = new ContentValues();
                updateValues.put(COLUMN_MESSAGE_DATA, modifiedData);
                getDatabaseInstance().update(TABLE_CHAT_LIST, updateValues, COLUMN_OID + "=" + id, null);

            }

        }
        cursorr.close();
    }

//Delete clear chat with ChatList Table
    //DELETE ChatList TimeStamp
    /*    String queryy = "delete from " + TABLE_CHAT_LIST + " WHERE " + COLUMN_TIME_STAMP + "<=" + mTimeStamp;
        Cursor cursorr = getDatabaseInstance().rawQuery(queryy, null);
        if (cursorr != null) {
            while (cursorr.moveToNext()) {
                int id = cursorr.getInt(cursor.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_CHAT_LIST, COLUMN_OID + "=" + id, null);
            }
            cursorr.close();
        }*/

    //  updateChatListStatus(receiverId, MessageFactory.CHAT_TYPE_SINGLE, MessageFactory.CHAT_STATUS_CLEARED);


    public void clearAllSingleChatMessageTimeStamp(long mTimeStamp, String mMessageId, String mChatId) {

      /*  //DELETE ChatMessage TimeStamp
        String query = "delete from " + TABLE_MESSAGES + " WHERE " + COLUMN_TIME_STAMP + "<=" + mTimeStamp;
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_MESSAGES, COLUMN_OID + "=" + id, null);
            }
            cursor.close();
        }
        //DELETE ChatList TimeStamp
        String queryy = "delete from " + TABLE_CHAT_LIST + " WHERE " + COLUMN_TIME_STAMP + "<=" + mTimeStamp;
        Cursor cursorr = getDatabaseInstance().rawQuery(queryy, null);
        if (cursorr != null) {
            while (cursorr.moveToNext()) {
                int id = cursorr.getInt(cursor.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_CHAT_LIST, COLUMN_OID + "=" + id, null);
            }
            cursorr.close();
        }*/


        //DELETE ChatMessage TimeStamp
        String timestamp = "";
        //Check Timestamp exists in Db
        String timestampStr = getTimeStamp(mMessageId);
        if (timestampStr != null && !AppUtils.isEmpty(timestampStr)) {

            String query = "delete from " + TABLE_MESSAGES + " WHERE " + COLUMN_TIME_STAMP + "<=" + getTimeStamp(mMessageId);
            Cursor cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                    timestamp = cursor.getString(cursor.getColumnIndex(COLUMN_TIME_STAMP));
                    getDatabaseInstance().delete(TABLE_MESSAGES, COLUMN_OID + "=" + id, null);

                }
                cursor.close();
            }

            String queryy = "SELECT * FROM " + TABLE_CHAT_LIST + " WHERE " + COLUMN_CHAT_TYPE + "='" + MessageFactory.CHAT_TYPE_SINGLE + "'";
            ;
            Cursor cursorr = getDatabaseInstance().rawQuery(queryy, null);
            if (cursorr != null) {
                if (cursorr.moveToNext()) {
                    MyLog.e(TAG, "TABLE_CHAT_LIST last" + mTimeStamp);
                    int id = cursorr.getInt(cursorr.getColumnIndex(COLUMN_OID));
                    String msgData = cursorr.getString(cursorr.getColumnIndex(COLUMN_MESSAGE_DATA));
                    MessageItemChat msgItem = gson.fromJson(msgData, MessageItemChat.class);

                    String[] arrIds = msgItem.getMessageId().split("-");
                    String chatId = arrIds[2];
                    if (Long.parseLong(chatId) <= mTimeStamp) {
                        getDatabaseInstance().delete(TABLE_CHAT_LIST, COLUMN_OID + "=" + id, null);
                    }
                    MyLog.e(TAG, "TABLE_CHAT_LIST last" + msgItem.getTS());

                }

            }
            cursorr.close();

        }
       /* //DELETE ChatList  TimeStamp

        String queryy = "delete from " + TABLE_CHAT_LIST + " WHERE " + COLUMN_TIME_STAMP + "==" + getTimeStamp(mMessageId);
        Cursor cursorr = getDatabaseInstance().rawQuery(queryy, null);
        if (cursorr != null) {
            while (cursorr.moveToNext()) {
                int idd = cursorr.getInt(cursorr.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_CHAT_LIST, COLUMN_OID + "=" + idd, null);
            }
            cursorr.close();
        }*/
    }

    public void clearAllSingleChatMessageTimeStamp(long mTimeStamp) {

        //DELETE ChatMessage TimeStamp
        String query = "delete from " + TABLE_MESSAGES + " WHERE " + COLUMN_TIME_STAMP + "<=" + mTimeStamp;
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_MESSAGES, COLUMN_OID + "=" + id, null);
            }
            cursor.close();
        }
        //DELETE ChatList TimeStamp
        String queryy = "delete from " + TABLE_CHAT_LIST + " WHERE " + COLUMN_TIME_STAMP + "<=" + mTimeStamp;
        Cursor cursorr = getDatabaseInstance().rawQuery(queryy, null);
        if (cursorr != null) {
            while (cursorr.moveToNext()) {
                int id = cursorr.getInt(cursor.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_CHAT_LIST, COLUMN_OID + "=" + id, null);
            }
            cursorr.close();
        }

    }

    public void updateTempDeletedMessage(String recordId, JSONObject deleteMsgData) {
        ContentValues deleteValues = new ContentValues();
        deleteValues.put(COLUMN_RECORD_ID, recordId);
        deleteValues.put(COLUMN_DELETE_MESSAGE_DATA, deleteMsgData.toString());
        deleteValues.put(COLUMN_STATUS, 1);

        getDatabaseInstance().insert(TABLE_TEMP_DELETE_MESSAGES, null, deleteValues);
    }

    public ArrayList<JSONObject> getAllTempDeletedMessage(String currentUserId) {
        String query = "SELECT " + COLUMN_DELETE_MESSAGE_DATA + " FROM " + TABLE_TEMP_DELETE_MESSAGES;
        Cursor cursor = null;
        ArrayList<JSONObject> deletedObjects = new ArrayList<>();
        try {
            cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String msgData = cursor.getString(cursor.getColumnIndex(COLUMN_DELETE_MESSAGE_DATA));
                    try {
                        JSONObject data = new JSONObject(msgData);
                        deletedObjects.add(data);
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                }
                cursor.close();
            }
        } catch (Exception exception) {
            if (cursor != null)
                cursor.close();
        } finally {
            // this gets called even if there is an exception somewhere above
            if (cursor != null)
                cursor.close();
        }
        return deletedObjects;
    }

    public void deleteTempDeletedMessage(String recordId, String chatType) {
        String query = "SELECT " + COLUMN_OID + " FROM " + TABLE_TEMP_DELETE_MESSAGES + " WHERE "
                + COLUMN_RECORD_ID + "='" + recordId + "' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_TEMP_DELETE_MESSAGES, COLUMN_OID + "=" + id, null);
            }
            cursor.close();
        }

    }

    public void updateStarredMessage(String msgId, String starStatus, String chatType) {
        String query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_MESSAGE_ID + "='" + msgId
                + "' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                String msgData = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_DATA));

                MessageItemChat msgItem = gson.fromJson(msgData, MessageItemChat.class);
                msgItem.setStarredStatus(starStatus);

                String modifiedData = gson.toJson(msgItem);
                ContentValues updateValues = new ContentValues();
                updateValues.put(COLUMN_MESSAGE_DATA, modifiedData);

                getDatabaseInstance().update(TABLE_MESSAGES, updateValues, COLUMN_OID + "=" + id, null);
            }
            cursor.close();
        }

    }

    public void updateTempStarredMessage(String recordId, JSONObject starMsgData) {
        ContentValues starValues = new ContentValues();
        starValues.put(COLUMN_RECORD_ID, recordId);
        starValues.put(COLUMN_STAR_MESSAGE_DATA, starMsgData.toString());
        starValues.put(COLUMN_STATUS, 1);

        getDatabaseInstance().insert(TABLE_TEMP_STAR_MESSAGES, null, starValues);
    }

    public ArrayList<JSONObject> getAllTempStarredMessage(String currentUserId) {
        String query = "SELECT " + COLUMN_STAR_MESSAGE_DATA + " FROM " + TABLE_TEMP_STAR_MESSAGES;
        Cursor cursor = null;
        ArrayList<JSONObject> starredObjects = new ArrayList<>();
        cursor = getDatabaseInstance().rawQuery(query, null);
        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    String msgData = cursor.getString(cursor.getColumnIndex(COLUMN_STAR_MESSAGE_DATA));
                    try {
                        JSONObject data = new JSONObject(msgData);
                        starredObjects.add(data);
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                }
                cursor.close();
            }
        } catch (Exception e) {

        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }


        return starredObjects;
    }

    public void deleteTempStarredMessage(String recordId, String chatType) {
        String query = "SELECT " + COLUMN_OID + " FROM " + TABLE_TEMP_DELETE_MESSAGES + " WHERE "
                + COLUMN_RECORD_ID + "='" + recordId + "' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                getDatabaseInstance().delete(TABLE_TEMP_DELETE_MESSAGES, COLUMN_OID + "=" + id, null);
            }
            cursor.close();
        }

    }

    public MessageItemChat getParticularMessage(String msgId) {
        String query = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_MESSAGE_ID + "='" + msgId + "'";

        MessageItemChat msgItem = null;
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            if (cursor.moveToNext()) {
                String msgData = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_DATA));

                msgItem = gson.fromJson(msgData, MessageItemChat.class);
            }
            cursor.close();
        }

        return msgItem;
    }


    public boolean isGroupId(String docId) {
        String query = "SELECT " + COLUMN_OID + " FROM " + TABLE_CHAT_LIST + " WHERE " + COLUMN_CHAT_ID + "='"
                + docId + "' AND " + COLUMN_CHAT_TYPE + "='" + MessageFactory.CHAT_TYPE_GROUP + "'";

        boolean isGroupChat = false;
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                isGroupChat = true;
            }
            cursor.close();
        }

        return isGroupChat;
    }


    // Calls methods
    public void updateCallLogs(CallItemChat callItem) {
        if (callItem == null || callItem.getCallId() == null)
            return;
        String selectQuery = "SELECT " + COLUMN_OID + " FROM " + TABLE_CALL_LOGS + " WHERE " +
                COLUMN_CALL_ID + "='" + callItem.getCallId() + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                updateCallEntry(callItem);
            } else {
                selectCur.close();
                insertNewCallEntry(callItem);
            }
        }
    }

    public void insertNewCallEntry(CallItemChat callItem) {
        String callData = gson.toJson(callItem);

        ContentValues values = new ContentValues();
        values.put(COLUMN_CALL_ID, callItem.getCallId());
        values.put(COLUMN_CALL_DATA, callData);
        values.put(COLUMN_STATUS, 1);

        // Inserting Row
        getDatabaseInstance().insert(TABLE_CALL_LOGS, null, values);
    }

    public void insertRejectCall(CallItemChat callItem) {
        String callData = gson.toJson(callItem);

        ContentValues values = new ContentValues();
        values.put(COLUMN_CALL_ID, callItem.getCallId());
        values.put(COLUMN_CALL_DATA, callData);
        values.put(COLUMN_STATUS, 5);

        // Inserting Row
        getDatabaseInstance().insert(TABLE_CALL_LOGS, null, values);
    }


    private void updateCallEntry(CallItemChat callItem) {
        String jsonMessage = gson.toJson(callItem);
        ContentValues updateValues = new ContentValues();
        updateValues.put(COLUMN_CALL_DATA, jsonMessage);

        getDatabaseInstance().update(TABLE_CALL_LOGS, updateValues, COLUMN_CALL_ID + "='" + callItem.getCallId() + "'", null);
    }

    public ArrayList<CallItemChat> selectAllCalls(String currentUserId) {
        String selectQuery = "SELECT " + COLUMN_CALL_DATA + " FROM " + TABLE_CALL_LOGS + " WHERE "
                + COLUMN_CALL_ID + " LIKE '" + currentUserId + "%'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        ArrayList<CallItemChat> callsList = new ArrayList<>();
        try {
            if (selectCur != null) {
                while (selectCur.moveToNext()) {
                    try {
                        String callData = selectCur.getString(selectCur.getColumnIndex(COLUMN_CALL_DATA));
                        CallItemChat callItemChat = gson.fromJson(callData, CallItemChat.class);
                        if (callItemChat != null) {
                            callsList.add(callItemChat);
                        }
                    } catch (Exception e) {
                        MyLog.e(TAG, "selectAllCalls: ", e);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "selectAllCalls: ", e);
        } finally {
            if (selectCur != null)
                selectCur.close();

        }
        return callsList;
    }

    public void updateCallStatus(String callId, int callStatus, String duration) {

        String selectQuery = "SELECT * FROM " + TABLE_CALL_LOGS + " WHERE " +
                COLUMN_CALL_ID + "='" + callId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.moveToNext()) {

                String callData = selectCur.getString(selectCur.getColumnIndex(COLUMN_CALL_DATA));
                CallItemChat callItem = gson.fromJson(callData, CallItemChat.class);
                if (!callItem.getCallStatus().equals(MessageFactory.CALL_STATUS_MISSED + "")
                        && !callItem.getCallStatus().equals(MessageFactory.CALL_STATUS_REJECTED + "")) {
                    callItem.setCallStatus(callStatus + "");
                    callItem.setCallDuration(duration);
                    updateCallLogs(callItem);
                }
            }
            selectCur.close();
        }
    }


    public void updateOutGoingCallStatus(String callId, int callStatus, String duration) {

        String selectQuery = "SELECT * FROM " + TABLE_CALL_LOGS + " WHERE " +
                COLUMN_CALL_ID + "='" + callId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.moveToNext()) {

                String callData = selectCur.getString(selectCur.getColumnIndex(COLUMN_CALL_DATA));
                CallItemChat callItem = gson.fromJson(callData, CallItemChat.class);
                callItem.setIsSelf(true);
                if (!callItem.getCallStatus().equals(MessageFactory.CALL_STATUS_MISSED + "")
                        && !callItem.getCallStatus().equals(MessageFactory.CALL_STATUS_REJECTED + "")) {
                    callItem.setCallStatus(callStatus + "");
                    callItem.setCallDuration(duration);
                    updateCallLogs(callItem);
                }
            }
            selectCur.close();
        }
    }

    public CallItemChat getCallStatus(String callId) {
        String selectQuery = "SELECT * FROM " + TABLE_CALL_LOGS + " WHERE " + COLUMN_CALL_ID
                + "='" + callId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        CallItemChat callItem = null;

        if (selectCur != null) {
            if (selectCur.moveToNext()) {
                String callData = selectCur.getString(selectCur.getColumnIndex(COLUMN_CALL_DATA));
                callItem = gson.fromJson(callData, CallItemChat.class);
            }
            selectCur.close();
        }
        return callItem;
    }

    public void deleteCallLog(String callId) {
        getDatabaseInstance().delete(TABLE_CALL_LOGS, COLUMN_CALL_ID + " = ?", new String[]{callId});
    }

    public void deleteAllCallLogs() {
        getDatabaseInstance().execSQL("DELETE FROM " + TABLE_CALL_LOGS);
    }

    public void deleteDatabase() {
        close();
        mContext.deleteDatabase(DB_NAME);
    }

    public void clearDatabase() {

        getDatabaseInstance().delete(TABLE_MESSAGES, null, null);
        getDatabaseInstance().delete(TABLE_CHAT_LIST, null, null);
        getDatabaseInstance().delete(TABLE_CHAT_LOCK, null, null);
        getDatabaseInstance().delete(TABLE_TEMP_DELETE_MESSAGES, null, null);
        getDatabaseInstance().delete(TABLE_TEMP_STAR_MESSAGES, null, null);
        getDatabaseInstance().delete(TABLE_CALL_LOGS, null, null);
        getDatabaseInstance().delete(TABLE_TEMP_SEND_NEW_MESSAGE, null, null);
        getDatabaseInstance().delete(TABLE_OFFLINE_EVENTS, null, null);

    }

    public void clearAllMsgs() {
        getDatabaseInstance().delete(TABLE_MESSAGES, null, null);
    }


    public MessageItemChat updateChatMessage(String docId, String msgId, String status,
                                             String deliverOrReadTS, boolean isSelfMsg) {
        Log.d(TAG, "updateChatMessageFrom db()" + status);
        String selectQuery = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " +
                COLUMN_MESSAGE_ID + "='" + msgId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        MessageItemChat msgItem = null;
        MyLog.d(TAG, "updateChatMessage1: tickMiss " + status);
        if (selectCur != null) {
            MyLog.d(TAG, "updateChatMessage1: tickMiss1 ");
            MyLog.d(TAG, "updateSingleMessageOfflineAcks " + selectCur.getColumnCount());
            if (selectCur.moveToNext()) {
                MyLog.d(TAG, "updateChatMessage1: tickMiss2 ");
                long timeStamp = selectCur.getLong(selectCur.getColumnIndex(COLUMN_TIME_STAMP));
                String msgData = selectCur.getString(selectCur.getColumnIndex(COLUMN_MESSAGE_DATA));
                String newMsgChatType = selectCur.getString(selectCur.getColumnIndex(COLUMN_CHAT_TYPE));
                msgItem = gson.fromJson(msgData, MessageItemChat.class);

                if (!msgItem.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_READ)
                        && status.equals(MessageFactory.DELIVERY_STATUS_DELIVERED)) {
                    if (msgItem.isBlockedMsg()) {
                        return msgItem;
                    }
                    msgItem.setDeliveryTime(deliverOrReadTS);
                    msgItem.setDeliveryStatus(status);
                    updateChatMessage(msgItem, newMsgChatType);
                }

                if (status.equals(MessageFactory.DELIVERY_STATUS_READ)) {
                    MyLog.d(TAG, "updateChatMessage1: tickMiss3 ");
                    msgItem.setReadTime(deliverOrReadTS);
                    msgItem.setDeliveryStatus(status);
                    if (msgItem.getDeliveryTime() == null || msgItem.getDeliveryTime().equals("0")) {
                        msgItem.setDeliveryTime(deliverOrReadTS);
                    }

                    if (msgItem.isSelf()) {
                        msgItem.setSecretMsgReadAt(deliverOrReadTS);
                    }
                    updateChatMessage(msgItem, newMsgChatType);
                }

                String msgQuery = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_TIME_STAMP
                        + "<" + timeStamp + " AND " + COLUMN_DELIVERY_STATUS + "<'" + status + "'"
                        + " AND " + COLUMN_CHAT_ID + "='" + docId + "'";
                Cursor msgCur = getDatabaseInstance().rawQuery(msgQuery, null);

                while (msgCur.moveToNext()) {
                    String uniqueChatMessageJson = msgCur.getString(msgCur.getColumnIndex(COLUMN_MESSAGE_DATA));
                    String chatType = msgCur.getString(msgCur.getColumnIndex(COLUMN_CHAT_TYPE));

                    if (status.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_DELIVERED)) {
                        MessageItemChat oldMsgItem = gson.fromJson(uniqueChatMessageJson, MessageItemChat.class);
                        if ((isSelfMsg == oldMsgItem.isSelf()) && oldMsgItem.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_SENT)) {
                            oldMsgItem.setDeliveryTime(deliverOrReadTS);
                            oldMsgItem.setDeliveryStatus(status);
                            updateChatMessage(oldMsgItem, chatType);
                        }
                    } else if (status.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ)) {
                        MessageItemChat oldMsgItem = gson.fromJson(uniqueChatMessageJson, MessageItemChat.class);

                        if (isSelfMsg && oldMsgItem.isSelf()) {
                            if (oldMsgItem.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_SENT)
                                    || oldMsgItem.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_DELIVERED)) {

                                // From sender message
                                if (oldMsgItem.getDeliveryTime() == null || oldMsgItem.getDeliveryTime().equals("0")) {
                                    oldMsgItem.setDeliveryTime(deliverOrReadTS);
                                }

                                oldMsgItem.setSecretMsgReadAt(deliverOrReadTS);
                                oldMsgItem.setReadTime(deliverOrReadTS);
                                oldMsgItem.setDeliveryStatus(status);
                                updateChatMessage(oldMsgItem, chatType);
                            }
                        } else if (!isSelfMsg && !oldMsgItem.isSelf()) {
                            // From receiver message
                            if (oldMsgItem.getDeliveryTime() == null || oldMsgItem.getDeliveryTime().equals("0")) {
                                oldMsgItem.setDeliveryTime(deliverOrReadTS);
                            }
                            oldMsgItem.setSecretMsgReadAt(deliverOrReadTS);
                            oldMsgItem.setReadTime(deliverOrReadTS);
                            oldMsgItem.setDeliveryStatus(status);
                            updateChatMessage(oldMsgItem, chatType);
                        }
                    }
                }
                msgCur.close();
            }
            selectCur.close();
        }

        return msgItem;
    }

    public MessageItemChat updateChatMessage(String docId, String msgId, String status, String recordId,
                                             String convId, String sentTS) {
        String selectQuery = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " +
                COLUMN_MESSAGE_ID + "='" + msgId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        MessageItemChat msgItem = null;

        if (selectCur != null) {

            if (selectCur.moveToNext()) {

                long timeStamp = selectCur.getLong(selectCur.getColumnIndex(COLUMN_TIME_STAMP));
                String msgData = selectCur.getString(selectCur.getColumnIndex(COLUMN_MESSAGE_DATA));
                String newMsgChatType = selectCur.getString(selectCur.getColumnIndex(COLUMN_CHAT_TYPE));
                msgItem = gson.fromJson(msgData, MessageItemChat.class);

                if (status.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_DELIVERED)) {
                    msgItem.setDeliveryTime(sentTS);
                } else if (status.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ)) {
                    msgItem.setReadTime(sentTS);
                } else if (status.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_SENT)) {
                    if (msgItem.getTS() != null && !msgItem.getTS().equals("")) {
                        sentTS = msgItem.getTS();
                    }
                    msgItem.setMsgSentAt(sentTS);
                }
                msgItem.setDeliveryStatus(status);
                if (recordId != null && !recordId.equals("")) {
                    msgItem.setRecordId(recordId);
                }
                if (convId != null && !convId.equals("")) {
                    msgItem.setConvId(convId);
                }
                msgItem.setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);

                String msgQuery = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_TIME_STAMP
                        + "<" + timeStamp + " AND " + COLUMN_DELIVERY_STATUS + "<'" + status + "'"
                        + " AND " + COLUMN_CHAT_ID + "='" + docId + "'";
                Cursor msgCur = getDatabaseInstance().rawQuery(msgQuery, null);

                updateChatMessage(msgItem, newMsgChatType);

                while (msgCur.moveToNext()) {
                    String uniqueChatMessageJson = msgCur.getString(msgCur.getColumnIndex(COLUMN_MESSAGE_DATA));
                    String chatType = msgCur.getString(msgCur.getColumnIndex(COLUMN_CHAT_TYPE));

                    if (status.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_DELIVERED)) {

                        MessageItemChat oldMsgItem = gson.fromJson(uniqueChatMessageJson, MessageItemChat.class);
                        if (oldMsgItem.isSelf() && oldMsgItem.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_SENT)) {
                            oldMsgItem.setDeliveryStatus(status);
                            oldMsgItem.setDeliveryTime(sentTS);
                            oldMsgItem.setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);
                            updateChatMessage(oldMsgItem, chatType);
                        }
                    } else if (status.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ)) {

                        MessageItemChat oldMsgItem = gson.fromJson(uniqueChatMessageJson, MessageItemChat.class);
                        if (oldMsgItem.isSelf()
                                && (oldMsgItem.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_SENT)
                                || oldMsgItem.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_DELIVERED))) {
                            oldMsgItem.setDeliveryStatus(status);
                            oldMsgItem.setReadTime(sentTS);
                            oldMsgItem.setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);
                            updateChatMessage(oldMsgItem, chatType);
                        }
                    }
                }
                msgCur.close();
            }
            selectCur.close();
        }

        return msgItem;
    }

    public MessageItemChat updateMessageDownloadStatus(String docId, String msgId, int downloadStatus, String localpath) {
        String selectQuery = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " +
                COLUMN_MESSAGE_ID + "='" + msgId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        MessageItemChat msgItem = null;

        if (selectCur != null) {
            if (selectCur.moveToNext()) {
                String msgData = selectCur.getString(selectCur.getColumnIndex(COLUMN_MESSAGE_DATA));
                String chatType = selectCur.getString(selectCur.getColumnIndex(COLUMN_CHAT_TYPE));
                msgItem = gson.fromJson(msgData, MessageItemChat.class);
                msgItem.setDownloadStatus(downloadStatus);
                msgItem.setUploadDownloadProgress(100);
                msgItem.setVideoPath(localpath);
                updateChatMessage(msgItem, chatType);
            }
            selectCur.close();
        }

        return msgItem;
    }

    public void initDownload(String msgId, int downloadStatus, String downloadPath, int downloadId) {
        String selectQuery = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " +
                COLUMN_MESSAGE_ID + "='" + msgId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        MessageItemChat msgItem = null;

        if (selectCur != null) {
            if (selectCur.moveToNext()) {
                String msgData = selectCur.getString(selectCur.getColumnIndex(COLUMN_MESSAGE_DATA));
                String chatType = selectCur.getString(selectCur.getColumnIndex(COLUMN_CHAT_TYPE));
                msgItem = gson.fromJson(msgData, MessageItemChat.class);
                msgItem.setDownloadStatus(downloadStatus);
                msgItem.setUploadDownloadProgress(0);
                msgItem.setDownloadingPath(downloadPath);
                msgItem.setDownloadId(downloadId);
                updateChatMessage(msgItem, chatType);
            }
            selectCur.close();
        }

    }


    public void updateGroupMessageStatus(String docId, String msgId, String status, String deliverOrReadTS,
                                         String ackUserId, String currentUserId) {
        String selectQuery = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " +
                COLUMN_MESSAGE_ID + "='" + msgId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        MessageItemChat msgItem = null;

        if (selectCur != null) {

            try {
                if (selectCur.moveToNext()) {

                    long timeStamp = selectCur.getLong(selectCur.getColumnIndex(COLUMN_TIME_STAMP));
                    String msgData = selectCur.getString(selectCur.getColumnIndex(COLUMN_MESSAGE_DATA));
                    String newMsgChatType = selectCur.getString(selectCur.getColumnIndex(COLUMN_CHAT_TYPE));
                    msgItem = gson.fromJson(msgData, MessageItemChat.class);

                    msgItem.setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);
                    if (status.equalsIgnoreCase(MessageFactory.GROUP_MSG_DELIVER_ACK)) {
                        if (msgItem.isSelf() && msgItem.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_SENT)) {
                            updateGroupMsgDeliverStatus(docId, ackUserId, msgItem, deliverOrReadTS);
                        } else if (!msgItem.isSelf() && ackUserId.equals(currentUserId)) {
                            msgItem.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_DELIVERED);
                            updateChatMessage(msgItem, MessageFactory.CHAT_TYPE_GROUP);
                        }
                    } else if (status.equalsIgnoreCase(MessageFactory.GROUP_MSG_READ_ACK)) {
                        if (msgItem.isSelf() && !msgItem.getDeliveryStatus().equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ)) {
                            updateGroupMsgReadStatus(docId, ackUserId, msgItem, deliverOrReadTS);
                        } else if (!msgItem.isSelf() && ackUserId.equals(currentUserId)) {
                            msgItem.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_READ);
                            updateChatMessage(msgItem, MessageFactory.CHAT_TYPE_GROUP);
                        }
                    }

                    String msgQuery = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " + COLUMN_TIME_STAMP
                            + "<" + timeStamp + " AND " + COLUMN_DELIVERY_STATUS + "<'" + status + "'"
                            + " AND " + COLUMN_CHAT_ID + "='" + docId + "'";
                    Cursor msgCur = getDatabaseInstance().rawQuery(msgQuery, null);

                    updateChatMessage(msgItem, newMsgChatType);

                    while (msgCur.moveToNext()) {
                        String uniqueChatMessageJson = msgCur.getString(msgCur.getColumnIndex(COLUMN_MESSAGE_DATA));

                        if (status.equalsIgnoreCase(MessageFactory.GROUP_MSG_DELIVER_ACK)) {
                            MessageItemChat oldMsgItem = gson.fromJson(uniqueChatMessageJson, MessageItemChat.class);
                            if (oldMsgItem.isSelf() && oldMsgItem.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_SENT)) {
                                updateGroupMsgDeliverStatus(docId, ackUserId, oldMsgItem, deliverOrReadTS);
                            } else if (!oldMsgItem.isSelf() && ackUserId.equals(currentUserId)) {
                                oldMsgItem.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_DELIVERED);
                                updateChatMessage(oldMsgItem, MessageFactory.CHAT_TYPE_GROUP);
                            }
                        } else if (status.equalsIgnoreCase(MessageFactory.GROUP_MSG_READ_ACK)) {
                            MessageItemChat oldMsgItem = gson.fromJson(uniqueChatMessageJson, MessageItemChat.class);
                            if (oldMsgItem.isSelf() && !oldMsgItem.getDeliveryStatus().equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ)) {
                                updateGroupMsgReadStatus(docId, ackUserId, oldMsgItem, deliverOrReadTS);
                            } else if (!oldMsgItem.isSelf() && ackUserId.equals(currentUserId)) {
                                oldMsgItem.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_READ);
                                updateChatMessage(oldMsgItem, MessageFactory.CHAT_TYPE_GROUP);
                            }
                        }
                    }
                    msgCur.close();
                }

            } catch (Exception e) {
                Log.e(TAG, "updateGroupMessageStatus: ", e);
            } finally {
                if (selectCur != null)
                    selectCur.close();
            }
        }

    }

    // update group message deliver status
    private void updateGroupMsgDeliverStatus(String docId, String ackUserId, MessageItemChat msgItem,
                                             String deliverOrReadTS) {

        String msgStatus = msgItem.getGroupMsgDeliverStatus();
        try {
            boolean isDeliveredAll = true;

            JSONObject msgDeliverObj = new JSONObject(msgStatus);
            JSONArray arrMembers = msgDeliverObj.getJSONArray("GroupMessageStatus");
            for (int i = 0; i < arrMembers.length(); i++) {
                JSONObject userObj = arrMembers.getJSONObject(i);
                String userId = userObj.getString("UserId");
                String deliverStatus = userObj.getString("DeliverStatus");

                if (userId.equals(ackUserId) && deliverStatus.equals(MessageFactory.DELIVERY_STATUS_SENT) &&
                        !deliverStatus.equals(MessageFactory.DELIVERY_STATUS_READ)) {
                    userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_DELIVERED);
                    userObj.put("DeliverTime", deliverOrReadTS);
                    arrMembers.put(i, userObj);
                    msgDeliverObj.put("GroupMessageStatus", arrMembers);
                    msgItem.setGroupMsgDeliverStatus(msgDeliverObj.toString());
                }

                deliverStatus = userObj.getString("DeliverStatus");
                int deliver = Integer.parseInt(deliverStatus);
                if (deliver < 2) {
                    isDeliveredAll = false;
                }
            }

            if (isDeliveredAll) {
                msgItem.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_DELIVERED);
            }
            updateChatMessage(msgItem, MessageFactory.CHAT_TYPE_GROUP);

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    // update group message read status
    private void updateGroupMsgReadStatus(String docId, String ackUserId, MessageItemChat msgItem,
                                          String deliverOrReadTS) {

        String msgStatus = msgItem.getGroupMsgDeliverStatus();
        try {
            JSONObject msgDeliverObj = new JSONObject(msgStatus);
            JSONArray arrMembers = msgDeliverObj.getJSONArray("GroupMessageStatus");

            boolean isReadAll = true;
            if (arrMembers != null && arrMembers.length() > 0) {
                for (int i = 0; i < arrMembers.length(); i++) {
                    JSONObject userObj = arrMembers.getJSONObject(i);
                    String userId = userObj.getString("UserId");
//                if(userId.equals(ackUserId) && msgItem.getCallStatus().equals(MessageFactory.DELIVERY_STATUS_SENT)) {
                    if (userId.equals(ackUserId)) {
                        userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_READ);
                        // for updating deliver time if not exists
                        if (userObj.getString("DeliverTime").equals("")) {
                            userObj.put("DeliverTime", deliverOrReadTS);
                        }
                        userObj.put("ReadTime", deliverOrReadTS);
                        arrMembers.put(i, userObj);
                        msgDeliverObj.put("GroupMessageStatus", arrMembers);
                        msgItem.setGroupMsgDeliverStatus(msgDeliverObj.toString());
                    }

                    String deliverStatus = userObj.getString("DeliverStatus");
                    if (!deliverStatus.equals(MessageFactory.DELIVERY_STATUS_READ)) {
                        isReadAll = false;
                    }
                }
            }
            if (isReadAll) {
                msgItem.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_READ);
            }
            updateChatMessage(msgItem, MessageFactory.CHAT_TYPE_GROUP);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void createNewGroupChatList(String groupId, String groupDocId) {
        MessageItemChat data = new MessageItemChat();
        data.setMessageType("");
        data.setMessageId(groupDocId.concat("-0"));
        data.setTextMessage("");
        data.setReceiverID(groupId);
        data.setTS("0");

        String msgData = gson.toJson(data);
        updateChatList(groupId, msgData, MessageFactory.CHAT_TYPE_GROUP, "0");
    }

    public ArrayList<MessageItemChat> selectAllSecretChatMessage(String docId, long serverTimeDiff) {
        String selectQuery = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " +
                COLUMN_CHAT_ID + "='" + docId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        long currentTime = Calendar.getInstance().getTimeInMillis() - serverTimeDiff;
        ArrayList<MessageItemChat> messageItemChats = new ArrayList<>();
        ArrayList<MessageItemChat> removeMsgItems = new ArrayList<>();

        while (selectCur.moveToNext()) {
            try {
                String msgData = selectCur.getString(selectCur.getColumnIndex(COLUMN_MESSAGE_DATA));
                MessageItemChat msgItem = gson.fromJson(msgData, MessageItemChat.class);
                if (!msgItem.isSelf() && msgItem.getDeliveryStatus().equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ)) {
                    String strReadAt = msgItem.getSecretMsgReadAt();
                    String strTimer = msgItem.getSecretTimer();

                    long readAt = AppUtils.parseLong(strReadAt);
                    long timer = AppUtils.parseLong(strTimer);
                    long timeDiff = currentTime - readAt;

                    if (timeDiff >= timer) {
                        removeMsgItems.add(msgItem);
                    }
                } else if (msgItem.isSelf() && !msgItem.getDeliveryStatus().equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_NOT_SENT)) {
                    String strSentAt = msgItem.getMsgSentAt();
                    String strTimer = msgItem.getSecretTimer();

                    long sentAt = AppUtils.parseLong(strSentAt);
                    long timer = AppUtils.parseLong(strTimer);
                    long timeDiff = currentTime - sentAt;

                    if (timeDiff >= timer) {
                        removeMsgItems.add(msgItem);
                    }
                }

                if (msgItem.isDate()) {
                    removeMsgItems.add(msgItem);
                }

                messageItemChats.add(msgItem);

            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }

        }

        selectCur.close();
        for (MessageItemChat removeItem : removeMsgItems) {
            deleteChatMessage(docId, removeItem.getMessageId(), MessageFactory.CHAT_TYPE_SECRET);
            messageItemChats.remove(removeItem);
        }

        return messageItemChats;
    }

    public void updateSecretMessageReadAt(String docId, String msgId, long readAt) {

        String selectQuery = "SELECT * FROM " + TABLE_MESSAGES + " WHERE " +
                COLUMN_MESSAGE_ID + "='" + msgId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.moveToNext()) {
                int id = selectCur.getInt(selectCur.getColumnIndex(COLUMN_OID));
                String msgData = selectCur.getString(selectCur.getColumnIndex(COLUMN_MESSAGE_DATA));

                MessageItemChat msgItem = gson.fromJson(msgData, MessageItemChat.class);
                if (msgItem != null) {
                    msgItem.setSecretMsgReadAt(readAt + "");
                    updateChatMessage(msgItem, MessageFactory.CHAT_TYPE_SECRET);
                }
            }
            selectCur.close();
        }
    }


    //-----------Delete Chat----------------------

    public void deleteSingleMessage(String groupAndMsgId, String msgId, String chatType, String msgType) {
        String query = "";
        //group msg
        if (msgId.contains("-g")) {
            query = "SELECT " + COLUMN_MESSAGE_DATA + "," + COLUMN_OID + " FROM " + TABLE_MESSAGES + " WHERE " +
                    COLUMN_MESSAGE_ID + " LIKE '" + "%" + groupAndMsgId + "%' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";
        }
        //single msg
        else {
            query = "SELECT " + COLUMN_MESSAGE_DATA + "," + COLUMN_OID + " FROM " + TABLE_MESSAGES + " WHERE " +
                    COLUMN_MESSAGE_ID + "='" + msgId + "' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";
        }
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            MyLog.d(TAG, "deleteSingleMessage: 1");
            if (cursor.moveToFirst()) {
                MyLog.d(TAG, "deleteSingleMessage: 2");
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                String chatData = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_DATA));

                MessageItemChat msgItem = gson.fromJson(chatData, MessageItemChat.class);
                switch (msgType) {
                    case "self":
                        msgItem.setMessageType(MessageFactory.DELETE_SELF + "");
                        msgItem.setIsSelf(true);
                        break;

                    case "other":
                        msgItem.setMessageType(MessageFactory.DELETE_OTHER + "");
                        msgItem.setIsSelf(false);
                        break;
                }

                ContentValues updateValues = new ContentValues();
                updateValues.put(COLUMN_MESSAGE_DATA, gson.toJson(msgItem));
                getDatabaseInstance().update(TABLE_MESSAGES, updateValues, COLUMN_OID + "=" + id, null);
                cursor.close();
            } else {
                cursor.close();
            }
        }
    }


    public void deleteChatListPage(String groupAndMsgId, String msgId, String chatType, String msgType) {
        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        String[] split = msgId.split("-");
        String query = "";
        //group msg

        query = "SELECT " + COLUMN_MESSAGE_DATA + "," + COLUMN_OID + " FROM " + TABLE_CHAT_LIST + " WHERE " + COLUMN_CHAT_ID + " LIKE '" + mCurrentUserId
                + "%' AND " + COLUMN_RECEIVER_ID + "='" + split[1] + "' AND " + COLUMN_CHAT_TYPE + "='" + chatType + "'";


        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            MyLog.d(TAG, "deleteSingleMessage: 1");
            if (cursor.moveToFirst()) {
                MyLog.d(TAG, "deleteSingleMessage: 2");
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_OID));
                String chatData = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_DATA));

                MessageItemChat msgItem = gson.fromJson(chatData, MessageItemChat.class);
                switch (msgType) {
                    case "self":
//                        msgItem.setMessageType("Delete Self");
                        msgItem.setMessageType(MessageFactory.DELETE_SELF + "");
                        msgItem.setIsSelf(true);
                        break;

                    case "other":
//                        msgItem.setMessageType("Delete Others");
                        msgItem.setMessageType(MessageFactory.DELETE_OTHER + "");
                        msgItem.setIsSelf(false);
                        break;
                }

                ContentValues updateValues = new ContentValues();
                updateValues.put(COLUMN_MESSAGE_DATA, gson.toJson(msgItem));
                getDatabaseInstance().update(TABLE_CHAT_LIST, updateValues, COLUMN_OID + "=" + id, null);
                cursor.close();
            } else {
                cursor.close();
            }
        }
    }


//--------------------------------------------File Upload Maintain--------------------------------------------------


    public void userpicinsertitem(String message_id, String upload_status, JSONObject object) {
        if (object == null) {
            MyLog.e(TAG, "insertNewMessage: message null.. check ");
            return;
        }
        String msgData = object.toString();


        ContentValues values = new ContentValues();
        values.put(USERFILE_UPLOAD_MESSAGE_ID, message_id);
        values.put(USERFILE_UPLOAD_STATUS, upload_status);
        values.put(USERFILE_UPLOAD_OBJECT, msgData);

        // Inserting Row
        getDatabaseInstance().insert(USERFILE_UPLOAD_TABLE, null, values);


    }

    public void userpicupdateMessage(String message_id, String upload_status, JSONObject object) {

        if (object == null)
            return;
        String jsonMessage = object.toString();
        if (jsonMessage != null) {


            ContentValues updateValues = new ContentValues();
            updateValues.put(USERFILE_UPLOAD_MESSAGE_ID, message_id);
            updateValues.put(USERFILE_UPLOAD_STATUS, upload_status);
            updateValues.put(USERFILE_UPLOAD_OBJECT, jsonMessage);
            getDatabaseInstance().update(USERFILE_UPLOAD_TABLE, updateValues, USERFILE_UPLOAD_MESSAGE_ID + "='" + message_id + "'", null);
        }
    }


    public void fileuploadinsertitem(String message_id, String upload_status, JSONObject object) {
        if (object == null) {
            MyLog.e(TAG, "insertNewMessage: message null.. check ");
            return;
        }
        String msgData = object.toString();

        ContentValues values = new ContentValues();
        values.put(FILE_UPLOAD_MESSAGE_ID, message_id);
        values.put(FILE_UPLOAD_STATUS, upload_status);
        values.put(FILE_UPLOAD_OBJECT, msgData);

        // Inserting Row
        getDatabaseInstance().insert(FILE_UPLOAD_TABLE, null, values);


    }


    public void fileuploadupdateMessage(String message_id, String upload_status, JSONObject object) {

        if (object == null)
            return;
        String jsonMessage = object.toString();
        if (jsonMessage != null) {


            ContentValues updateValues = new ContentValues();
            updateValues.put(FILE_UPLOAD_MESSAGE_ID, message_id);
            updateValues.put(FILE_UPLOAD_STATUS, upload_status);
            updateValues.put(FILE_UPLOAD_OBJECT, jsonMessage);
            getDatabaseInstance().update(FILE_UPLOAD_TABLE, updateValues, FILE_UPLOAD_MESSAGE_ID + "='" + message_id + "'", null);
        }
    }


    public void DeleteFileStatusUpdate(String message_id, String upload_status) {

        try {

            ContentValues updateValues = new ContentValues();
            updateValues.put(FILE_UPLOAD_MESSAGE_ID, message_id);
            updateValues.put(FILE_UPLOAD_STATUS, upload_status);

            getDatabaseInstance().update(FILE_UPLOAD_TABLE, updateValues, FILE_UPLOAD_MESSAGE_ID + "='" + message_id + "'", null);

        } catch (Exception e) {
            MyLog.e(TAG, "DeleteFileStatusUpdate: ", e);
        }

    }


    public JSONObject fileuploadobjectget(String message_id) {

        String query;
        JSONObject object = null;


        try {
            query = "SELECT * FROM " + FILE_UPLOAD_TABLE + " WHERE " + FILE_UPLOAD_MESSAGE_ID + "='" + message_id + "'";

            Cursor cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {

                    String data = cursor.getString(cursor.getColumnIndex(FILE_UPLOAD_OBJECT));


                    object = new JSONObject(data);

                }
                cursor.close();
            }


        } catch (Exception e) {

            MyLog.e(TAG, "fileuploadobjectget: ", e);
        }

        return object;
    }


    public String userpicfileuploadStatusget(String message_id) {

        String Uplod_Status = "";
        String query = "";
        try {

            query = "SELECT * FROM " + USERFILE_UPLOAD_TABLE + " WHERE " + USERFILE_UPLOAD_MESSAGE_ID + "='" + message_id + "'";

            Cursor cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {

                    Uplod_Status = cursor.getString(cursor.getColumnIndex(USERFILE_UPLOAD_STATUS));

                }
                cursor.close();
            }


        } catch (Exception e) {
            MyLog.e(TAG, "fileuploadStatusget: ", e);
        }

        return Uplod_Status;


    }

    public String fileuploadStatusget(String message_id) {

        String Uplod_Status = "";
        String query = "";
        try {

            query = "SELECT * FROM " + FILE_UPLOAD_TABLE + " WHERE " + FILE_UPLOAD_MESSAGE_ID + "='" + message_id + "'";

            Cursor cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {

                    Uplod_Status = cursor.getString(cursor.getColumnIndex(FILE_UPLOAD_STATUS));

                }
                cursor.close();
            }


        } catch (Exception e) {
            MyLog.e(TAG, "fileuploadStatusget: ", e);
        }

        return Uplod_Status;


    }

    public ArrayList<String> getpendingUserPicfileupload(String uploadingstatus) {

        ArrayList<String> array = new ArrayList<>();

        String dtata_object = "";
        String query = "";
        Cursor cursor = null;
        try {

            query = "SELECT * FROM " + USERFILE_UPLOAD_TABLE + " WHERE " + USERFILE_UPLOAD_STATUS + "='" + uploadingstatus + "'";

            cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {

                    dtata_object = cursor.getString(cursor.getColumnIndex(USERFILE_UPLOAD_OBJECT));

                    array.add(dtata_object);

                }
                cursor.close();
            }


        } catch (Exception e) {
            MyLog.e(TAG, "getpendingfileupload: ", e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return array;
    }


    public ArrayList<String> getpendingfileupload(String uploadingstatus) {

        ArrayList<String> array = new ArrayList<>();

        String dtata_object = "";
        String query = "";
        Cursor cursor = null;
        try {

            query = "SELECT * FROM " + FILE_UPLOAD_TABLE + " WHERE " + FILE_UPLOAD_STATUS + "='" + uploadingstatus + "'";

            cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {

                    dtata_object = cursor.getString(cursor.getColumnIndex(FILE_UPLOAD_OBJECT));

                    array.add(dtata_object);

                }
                cursor.close();
            }


        } catch (Exception e) {
            MyLog.e(TAG, "getpendingfileupload: ", e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return array;
    }


    //------------------------------------First Time Upload Files Maintain Insert Row--------------------------------------------


    public void FirsttimeFileUploadinsert(String message_id, String upload_status, JSONObject object) {
        if (object == null) {
            MyLog.e(TAG, "insertNewMessage: message null.. check ");
            return;
        }
        String msgData = object.toString();

        ContentValues values = new ContentValues();
        values.put(FILE_UPLOAD_FIRSTTIME_MESSAGE_ID, message_id);
        values.put(FILE_UPLOAD_FIRSTTIME_STATUS, upload_status);
        values.put(FILE_UPLOAD_FIRSTTIME_OBJECT, msgData);

        // Inserting Row
        getDatabaseInstance().insert(FILE_UPLOAD_FIRSTTIME_TABLE, null, values);


    }


    public JSONObject FirstTimeUploadObjectGet(String message_id) {

        String query;
        JSONObject object = null;
        JsonObject jobj = null;

        try {
            query = "SELECT * FROM " + FILE_UPLOAD_FIRSTTIME_TABLE + " WHERE " + FILE_UPLOAD_FIRSTTIME_MESSAGE_ID + "='" + message_id + "'";

            Cursor cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {

                    String data = cursor.getString(cursor.getColumnIndex(FILE_UPLOAD_FIRSTTIME_OBJECT));


                    object = new JSONObject(data);
                }
                cursor.close();
            }


        } catch (Exception e) {
            MyLog.e(TAG, "FirstTimeUploadObjectGet: ", e);
        }

        return object;
    }


}
