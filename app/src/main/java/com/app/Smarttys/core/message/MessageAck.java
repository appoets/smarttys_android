package com.app.Smarttys.core.message;

import android.content.Context;

import com.app.Smarttys.app.utils.MyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 *
 */
public class MessageAck extends BaseMessage implements Message {
    private static final String TAG = MessageAck.class.getSimpleName();

    public MessageAck(Context context) {
        super(context);
    }

    @Override
    public Object getMessageObject(String to, String doc_id, Boolean isSecretchat) {
        this.to = to;
        setId(from + "-" + to);
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("msgIds", new JSONArray(Arrays.asList(id)));
            object.put("doc_id", doc_id);
            object.put("status", "3");
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return object;

    }

    @Override
    public Object getGroupMessageObject(String to, String payload, String groupName) {
        return null;
    }

    public Object getMessageObject(String to, String doc_id, String status, String _id, boolean isSecretChat) {
        this.to = to;
        setId(from + "-" + to);
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("msgIds", new JSONArray(Arrays.asList(_id)));
            object.put("doc_id", doc_id);
            object.put("status", status);
            if (isSecretChat) {
                object.put("secret_type", "yes");
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return object;

    }

    public Object getMessageObject(String to, String doc_id, String status, String _id, boolean isSecretChat, String convId) {
        this.to = to;
        setId(from + "-" + to);
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("msgIds", new JSONArray(Arrays.asList(_id)));
            object.put("doc_id", doc_id);
            object.put("status", status);
            object.put("convId", convId);

            if (isSecretChat) {
                object.put("secret_type", "yes");
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return object;

    }


    public Object getStatusMessageObject(String to, String docId, String status, String _id) {
        this.to = to;
        setId(from + "-" + to);
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("msgIds", new JSONArray(Arrays.asList(_id)));
            object.put("doc_id", docId);
            object.put("status", status);
            object.put("type", "status");
        } catch (Exception e) {
            MyLog.e(TAG, "getStatusMessageObject: ", e);
        }
        return object;
    }

}
