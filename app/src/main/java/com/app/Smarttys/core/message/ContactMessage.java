package com.app.Smarttys.core.message;

import android.content.Context;

import com.app.Smarttys.app.utils.GroupInfoSession;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.model.GroupInfoPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.socket.SocketManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 11/2/2016.
 */
public class ContactMessage extends BaseMessage {
    private static final String TAG = "ContactMessage";
    private Context context;

    public ContactMessage(Context context) {
        super(context);
        this.context = context;
    }

    //createdTo, contact_name, createdTomsisdn
    public Object getMessageObject(String to, String payload, String contactSmarttyId,
                                   String contactName, String contactNumber, String contactDetails, boolean isSecretChat) {
        this.to = to;
        setId(from + "-" + to);
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("type", MessageFactory.contact);
            object.put("payload", payload);
            object.put("id", tsForServerEpoch);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);
            if (contactSmarttyId != null && !contactSmarttyId.equals("")) {
                object.put("createdTo", contactSmarttyId);
            }
            object.put("contact_name", contactName);
            object.put("createdTomsisdn", contactNumber);
            object.put("contactDetails", contactDetails);

            if (isSecretChat) {
//                setId(getId() + "-secret");
                object.put("chat_type", MessageFactory.CHAT_TYPE_SECRET);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return object;

    }

    //createdTo, contact_name, createdTomsisdn
    public Object getGroupMessageObject(String to, String payload, String groupName, String contactSmarttyId,
                                        String contactName, String contactNumber, String contactDetails) {
        this.to = to;
        setId(from + "-" + to + "-g");
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("type", MessageFactory.contact);
            object.put("payload", payload);
            object.put("id", tsForServerEpoch);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);
            object.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE);
            object.put("userName", groupName);
            if (contactSmarttyId != null && !contactSmarttyId.equals("")) {
                object.put("createdTo", contactSmarttyId);
            }
            object.put("contact_name", contactName);
            object.put("createdTomsisdn", contactNumber);
            object.put("contactDetails", contactDetails);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return object;
    }

    public MessageItemChat createMessageItem(boolean isSelf, String message, String status,
                                             String receiverUid, String senderName, String contactName,
                                             String contactNo, String contactSmarttyId, String contactDetail) {
        item = new MessageItemChat();
        item.setMessageId(getId() + "-" + tsForServerEpoch);
        item.setIsSelf(isSelf);
        item.setDeliveryStatus(status);
        item.setReceiverUid(receiverUid);
        item.setReceiverID(to);
        item.setMessageType("" + type);
//        item.setContactInfo(message);
        item.setSenderName(senderName);
        item.setTS(getShortTimeFormat());
        item.setContactName(contactName);
        item.setContactNumber(contactNo);
        item.setDetailedContacts(contactDetail);
        item.setContactSmarttyId(contactSmarttyId);

        if (getId().contains("-g")) {
            GroupInfoSession groupInfoSession = new GroupInfoSession(context);
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(getId());

            if (infoPojo != null) {
                String[] groupMembers = infoPojo.getGroupMembers().split(",");
                try {
                    JSONArray arrMembers = new JSONArray();
                    for (String member : groupMembers) {
                        JSONObject userObj = new JSONObject();
                        userObj.put("UserId", member);
                        userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_SENT);
                        if (from.equals(member)) {
                            userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_READ);
                        }
                        userObj.put("DeliverTime", "");
                        userObj.put("ReadTime", "");
                        arrMembers.put(userObj);
                    }
                    JSONObject deliverObj = new JSONObject();
                    deliverObj.put("GroupMessageStatus", arrMembers);
                    item.setGroupMsgDeliverStatus(deliverObj.toString());
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }
        }

        return item;
    }

}