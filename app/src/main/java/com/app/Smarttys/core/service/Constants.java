package com.app.Smarttys.core.service;

import android.content.Context;

import com.app.Smarttys.R;


/**
 * Created by Administrator on 10/6/2016.
 */
public class Constants {


    public static final String BASE_IP = "http://www.smarttysapp.com:8080";
    public static final String SOCKET_IP = BASE_IP + "/";
    public static final String SOCKET_URL_ = SOCKET_IP + "user";
    public static final String SOCKET_URL_CALL_ = SOCKET_IP + "message";
    public static final String BASE_URL = SOCKET_IP + "api/";
    public static final String IMAGE_BASE_URL = BASE_IP;
    public static final String DEVICE = "android";
    public static final String Contact_image_load = BASE_IP;
    public static final String USER_PROFILE_URL = SOCKET_IP + "uploads/users/";
    public static final String FAQ = BASE_IP + "/faq";
    public static final String pricvacy_policy = BASE_IP + "/privacypolicy";
    public static final String SETTINGSS = BASE_URL + "settings";
    public static final String REQUESTINVITE = BASE_URL + "requestinvite";
    public static final String VERIFY_NUMBER_REQUEST = BASE_URL + "Login";
    public static final String RESEND_INVITE_CODE_REQUEST = BASE_URL + "ResendInvitecode";
    public static final int STATUS_DELETE_REQUEST = 17;
    public static final String VERIFY_SMS_CODE = BASE_URL + "VerifyMsisdn";
    public static final String RESEND_SMS_OTP = BASE_URL + "ResendSms";
    public static final String RESEND_EMAIL_OTP = BASE_URL + "ResendEmailOtp";
    public static final String UPDATE_DATA = BASE_URL + "UpdateData";
    public static final String UPLOAD_IMAGE = BASE_URL + "UploadImage";
    public static final String UPDATE_STATUS = BASE_URL + "UpdateStatus";
    public static final String GET_SETTINGS = BASE_URL + "settings";
    public static final String UNREAD_MSG = "unread_msg";
    public static boolean isUserLeave, isRecentClicked, otherAppClicked;
    public static long recentAppsClickTime;
    public static boolean IS_FROM_SHARING_PAGE = false;
    public static boolean IS_FROM_THIRD_PARTY_APP = false;
    public static boolean IS_FROM_PASSWORD_PAGE = false;
    public static boolean HIDE_FILE = false;
    public static String callername;

    public static String getAppStoreLink(Context context) {
        return "Hey, check out " + context.getResources().getString(R.string.app_name)
                + " messenger to connect instantly. \nDownload: https://play.google.com/store/apps/details?id="
                + context.getPackageName();
    }
}