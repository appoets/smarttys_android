package com.app.Smarttys.core.service;


import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.provider.ContactsContract;

import androidx.annotation.Nullable;

import com.app.Smarttys.app.activity.SmarttyContactsService;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.ShortcutBadgeManager;
import com.app.Smarttys.core.message.OfflineMessageHandler;
import com.app.Smarttys.core.model.ContactsPojo;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.socket.SocketManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by CAS60 on 2/28/2017.
 */
public class ContactsSync extends Service {
    private static final String TAG = ContactsSync.class.getSimpleName();
    public static boolean isStarted;
    ShortcutBadgeManager contactDBtime;
    SocketManager.SocketCallBack callBack = new SocketManager.SocketCallBack() {

        @Override
        public void onSuccessListener(String eventName, Object... response) {
            ReceviceMessageEvent me = new ReceviceMessageEvent();
            me.setEventName(eventName);

            if (AppUtils.isEncryptionEnabled(ContactsSync.this)) {
                try {
                    if (response != null && !SocketManager.excludedList.contains(eventName)) {
                        MyLog.d(TAG, "invokeCallBack: event name" + eventName);
                        response[0] = SocketManager.getDecryptedMessage(ContactsSync.this, response[0].toString(), eventName);
                        String decrypted = response[0].toString();
                        MyLog.d(TAG, "onSuccessListener: " + decrypted);
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "onSuccessListener: ", e);
                }
            }
            me.setObjectsArray(response);
            if (eventName != null)
                switch (eventName) {
                    case SocketManager.EVENT_UPDATE_CONTACT: {
                        UpdateContactResponse.getInstance().updateContact(me, ContactsSync.this);
                    }
                    break;
                }
        }
    };
    private SocketManager manager;
    private boolean isContactLoading;
    private OfflineMessageHandler offlineMsgHandler;

    @Override
    public void onCreate() {
        isStarted = true;
        MyLog.e(TAG, "ContactsSync onCreate: ");
        contactDBtime = new ShortcutBadgeManager(this);
        if (manager == null) {
            manager = SocketManager.getInstance();
            manager.init(this, callBack);
        }
        // offlineMsgHandler = new OfflineMessageHandler(this);
        loadContacts();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final SendMessageEvent event) {
        String eventName = event.getEventName();
        Object eventObj = event.getMessageObject();
        MyLog.d(TAG, "slowTest onMessageEvent: " + eventName);


        if (manager.isConnected()) {

            manager.send(event.getMessageObject(), event.getEventName());
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    public void loadContacts() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                readContacts();
            }
        }).start();
    }

    public String getRawContactId(String contactId) {
        String res = "";
        Uri uri = ContactsContract.RawContacts.CONTENT_URI;
        String[] projection = new String[]{ContactsContract.RawContacts._ID};
        String selection = ContactsContract.RawContacts.CONTACT_ID + " = ?";
        String[] selectionArgs = new String[]{contactId};
        Cursor c = getContentResolver().query(uri, projection, selection, selectionArgs, null);

        if (c != null && c.moveToFirst()) {
            res = c.getString(c.getColumnIndex(ContactsContract.RawContacts._ID));
            c.close();
        }

        return res;
    }


    public String getDeletedContact() {
        String res = "";
        Uri uri = ContactsContract.RawContacts.CONTENT_URI;
        Uri dataUri = Uri.parse("content://" + ContactsContract.AUTHORITY + "/data");
        String[] projection = new String[]{ContactsContract.RawContacts._ID};
        String selection = ContactsContract.RawContacts.DELETED + " = ?";
        String[] selectionArgs = new String[]{"1"};
        Cursor cursor = getContentResolver().query(uri, projection, selection, selectionArgs, null);
        ContentResolver cr = getContentResolver();
/*
        if(c != null && c.moveToFirst())
        {
            res = c.getString(c.getColumnIndex(ContactsContract.RawContacts._ID));
            c.close();
        }
*/
        StringBuilder sb = new StringBuilder();
        if (cursor != null)
            while (cursor.moveToNext()) {
                // String data1= cursor.getString(cursor.getColumnIndex(ContactsContract.RawContacts.Data.DATA1));
                int deletedRawContactId = cursor.getInt(0);
                sb.append("rawId=" + deletedRawContactId).append("\n\t");
                Cursor dataCursor = cr.query(dataUri,
                        new String[]{"data1"},
                        "row_contact_id=" + deletedRawContactId,
                        null,
                        null);
                while (dataCursor.moveToNext()) {
                    String phone = dataCursor.getString(0);
                    sb.append(phone).append("\n");
                }
            }
        cursor.close();

        return res;
    }


    public void readContacts() {
        try {
            isContactLoading = true;

            ContentResolver cr = getContentResolver();
            JSONArray arrContacts = new JSONArray();
            HashMap<String, ContactsPojo> Contactdata = new HashMap<String, ContactsPojo>();


            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                    null, null, null);
            String phone = null;
            String phType = "";
            String name = "";
            String phNo = "";
            ContactsPojo contactsPojo = null;
            if (cur != null && cur.getCount() > 0) {

                while (cur.moveToNext()) {
                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                    name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                    if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {

                        Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                        + " = ?", new String[]{id}, null);
                        if (pCur != null) {
                            while (pCur.moveToNext()) {
                                phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                phType = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                                String type = "";

                                switch (getNumber(phType)) {
                                    case 1:
                                        type = "Home";
                                        break;
                                    case 2:
                                        type = "Mobile";
                                        break;
                                    case 3:
                                        type = "Work";
                                        break;
                                    case 4:
                                        type = "Work Fax";
                                        break;
                                    case 5:
                                        type = "Home Fax";
                                        break;
                                    case 6:
                                        type = "Pager";
                                        break;
                                    case 7:
                                        type = "Other";
                                        break;
                                    case 8:
                                        type = "Callback";
                                        break;
                                    default:
                                        type = "Custom";
                                        break;

                                }

                                try {
                                    if (phone != null && !phone.equals("") && !phone.isEmpty()) {
                                        phNo = phone.replace(" ", "").replace("(", "").replace(")", "").replace("-", "");
                                    }
                                    contactsPojo = new ContactsPojo();
                                    JSONObject contactObj = new JSONObject();
                                    contactObj.put("Phno", phNo);
                                    contactObj.put("Name", name);
                                    contactObj.put("Type", type);
                                    try {
                                        arrContacts.put(contactObj);
                                        contactsPojo.setNumber(phNo);
                                        contactsPojo.setName(name);
                                        contactsPojo.setType(type);
                                        Contactdata.put(phNo, contactsPojo);

                                        // data.add(contactsPojo);
                                    } catch (IllegalArgumentException e) {
                                        MyLog.e(TAG, "", e);
                                    }

                                } catch (JSONException e) {
                                    MyLog.e(TAG, "", e);
                                }
                            }
                            pCur.close();
                        }

                    }
                }

                cur.close();
            }
            if (SmarttyContactsService.contactEntries != null) {
                SmarttyContactsService.contactEntries.clear();
            }

            Set<Map.Entry<String, ContactsPojo>> set = Contactdata.entrySet();
            for (Map.Entry<String, ContactsPojo> entry : set) {
                SmarttyContactModel d = new SmarttyContactModel();
                d.setFirstName(entry.getValue().getName());
                //     Log.e("ContactSync","setNumberInDevice"+entry.getValue().getNumber());
                d.setNumberInDevice(entry.getValue().getNumber());
                d.setType(entry.getValue().getType());
                SmarttyContactsService.contactEntries.add(d);
            }

            SmarttyContactsService.contact = arrContacts.toString();
            SmarttyContactsService.contactLoadedAt = Calendar.getInstance().getTimeInMillis();
            try {
                Collections.sort(SmarttyContactsService.contactEntries, Getcontactname.nameAscComparator);
            } catch (Exception e) {
                MyLog.e(TAG, "readContacts: ", e);
            }
            contactDBtime.setfirsttimecontactSyncCompleted(true);
            //contactDBtime.setContactLastRefreshTime(System.currentTimeMillis());
            if (SocketManager.isConnected && SessionManager.getInstance(ContactsSync.this).getBackupRestored()) {
                //     Log.e(TAG, "SocketManager.isConnected: "+SocketManager.isConnected +"backuprestore"+SessionManager.getInstance(ContactsSync.this).getBackupRestored());
                SmarttyContactsService.startContactService(ContactsSync.this, true);
            }
            isContactLoading = false;


        } catch (Exception e) {
            MyLog.e(TAG, "readContacts: ", e);
        }
    }

    public int getNumber(String data) {
        try {
            if (data != null) {
                return Integer.parseInt(data);
            }
        } catch (NumberFormatException e) {
            MyLog.e(TAG, "", e);
        }
        return 0;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /*@Override
    protected void onHandleIntent(Intent intent) {
        String dataString = intent.getDataString();

        savedContactsMap = new HashMap<>();
       // loadContacts();
        getContentResolver().registerContentObserver(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, true, contentObserver);
    }*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        isStarted = false;
        //  getContentResolver().unregisterContentObserver(contentObserver);

//        Intent contactIntent = new Intent(ContactsSync.this, ContactsSync.class);
//        startService(contactIntent);
    }
}
