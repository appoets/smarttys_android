package com.app.Smarttys.core.model;

import java.io.Serializable;

/**
 * Created by CAS60 on 6/28/2017.
 */
public class MuteUserPojo implements Serializable {

    private String receiverId, chatType, secretType;

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public String getSecretType() {
        return secretType;
    }

    public void setSecretType(String secretType) {
        this.secretType = secretType;
    }
}
