package com.app.Smarttys.core.model;

/**
 * Created by CAS60 on 2/22/2017.
 */
public class MultiTextDialogPojo {

    private Integer imageResource;
    private String labelText;

    public Integer getImageResource() {
        return imageResource;
    }

    public void setImageResource(Integer imageResource) {
        this.imageResource = imageResource;
    }

    public String getLabelText() {
        return labelText;
    }

    public void setLabelText(String labelText) {
        this.labelText = labelText;
    }
}
