package com.app.Smarttys.core.model;

import java.io.Serializable;

/* This model is used for verifying the phone number and verifying the code sent via SMS*/
public class SCLoginModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 633956855520350887L;
    //{"message":"Verification Code Send to You Number","errNum":"0","code":"1143"}

    private String message;
    private String errNum;
    private String code;
    private String Name;
    private String Status;
    private String _id;
    private String loginCount;
    private String token;
    private String email;
    private String ProfilePic;
    private String first_name;
    private String last_name;
    private String PhNumber;

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhNumber() {
        return PhNumber;
    }

    public void setPhNumber(String phNumber) {
        PhNumber = phNumber;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String Email) {
        this.email = Email;
    }

    public String getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(String loginCount) {
        this.loginCount = loginCount;
    }

    public String getProfilePic() {
        return ProfilePic;
    }

    public void setProfilePic(String profilePic) {
        ProfilePic = profilePic;
    }
    //String phoneNumber = VerifyPhoneNumber.message;
    //private String message2 = "We will be verifying the phone number:\n"+phoneNumber+"\nIs this OK,or would you like to edit the number?";

    public String getMessage() {
        return message;
    }

    public void setMessage(String message2) {
        this.message = message;
    }

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
