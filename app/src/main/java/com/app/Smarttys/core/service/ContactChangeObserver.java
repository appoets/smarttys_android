package com.app.Smarttys.core.service;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.app.Smarttys.app.activity.SmarttyContactsService;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ContactChangeObserver extends ContentObserver {
    private static final String TAG = "ContactChangeObserver";
    private Context mContext;


    public ContactChangeObserver(Handler handler) {
        super(handler);
    }

    public void init(Context context) {
        mContext = context;
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange, uri);


        if (ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED) {
            loadUpdatedContacts();

        }

    }

    public void loadUpdatedContacts() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                readUpdatedContacts();
            }
        }).start();
    }

    public void readUpdatedContacts() {
        try {
            if (mContext == null)
                return;
            ContactDB_Sqlite contactDBSqlite = new ContactDB_Sqlite(mContext);
            ContentResolver cr = mContext.getContentResolver();
            Cursor cursor = mContext.getContentResolver().query(
                    ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP + " Desc");
            if (cursor.moveToNext()) {
                String id = cursor.getString(
                        cursor.getColumnIndex(ContactsContract.Contacts._ID));

                final String name = cursor.getString(
                        cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                final long lastUpdated = cursor.getLong(
                        cursor.getColumnIndex(ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP));
                long currentMillis = System.currentTimeMillis();
                long differenceMillis = currentMillis - lastUpdated;
                MyLog.d(TAG, "readUpdatedContacts: " + differenceMillis);
                Log.w("Contact ID", id);
                Log.w("Person Name", name);
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {

                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                    + " = ?", new String[]{id}, null);
                    if (pCur != null) {
                        while (pCur.moveToNext()) {
                            String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            String phType = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                            long savedRevision = SessionManager.getInstance(mContext).getContactSavedRevision();
                            phone = phone.replace(" ", "");
                            boolean isDeletedContact = isDeletedContact();
                            MyLog.d(TAG, "readUpdatedContacts: " + isDeletedContact);
                            //getDeletedContact();
                            String status = "";
                            boolean isDeleted = false;
                            if (differenceMillis > 20000) {
                                status = "deleted";
                                isDeleted = true;
                            }
                            status = contactDBSqlite.statusOfNumber(phone, name, savedRevision, isDeleted);

                            switch (status) {
                                case "new":
                                    sendUpdatedContactToServer(phone, name, "Mobile", "1");
                                    break;
                                case "edited":
                                    sendUpdatedContactToServer(phone, name, "Mobile", "2");
                                    break;
                                case "deleted":
                                    SmarttyContactsService.bindContactService(mContext, true);
                                    //getDeletedContact();
                                    //  sendUpdatedContactToServer(phone,name,"Mobile","0");
                                    break;
                            }
                            MyLog.d(TAG, "readUpdatedContacts: " + status);
                        }
                    }
                }
            } else {
                boolean isDeleted = isDeletedContact();
                MyLog.d(TAG, "readUpdatedContacts: " + isDeleted);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "readContacts: ", e);
        }
    }

    public boolean isDeletedContact() {
        boolean isDeletedContact = false;
        try {
            if (mContext == null)
                return false;
            String selection = ContactsContract.DeletedContacts.CONTACT_DELETED_TIMESTAMP + " > ?";
            String[] selectionArgs = new String[]{String.valueOf(System.currentTimeMillis() - 10000)};
            Cursor cursor = mContext.getContentResolver().query(ContactsContract.DeletedContacts.CONTENT_URI, null, selection, selectionArgs, null);
            if (cursor != null && cursor.getCount() > 0) {
                isDeletedContact = true;
            }
            cursor.close();
        } catch (Exception e) {
            MyLog.e(TAG, "isDeletedContact: ", e);
        }
        return isDeletedContact;
    }

    private void sendUpdatedContactToServer(final String phNo, final String name, final String type, final String status) {
        try {
            MyLog.d(TAG, "sendUpdatedContactToServer: ");
            if (mContext == null)
                return;

            if (SocketManager.getInstance().isConnected()) {
                MyLog.d(TAG, "sendUpdatedContactToServer: socket connected");
                JSONArray contactArray = new JSONArray();
                JSONObject contactObj = new JSONObject();
                contactObj.put("Phno", phNo);
                contactObj.put("Name", name);
                contactObj.put("Type", type);
                contactArray.put(contactObj);
                SendMessageEvent messageEvent = new SendMessageEvent();
                messageEvent.setEventName(SocketManager.EVENT_UPDATE_CONTACT);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("indexAt", "0");
                    jsonObject.put("msisdn", SessionManager.getInstance(mContext).getPhoneNumberOfCurrentUser());
                    jsonObject.put("Contacts", contactArray.toString());
                    jsonObject.put("status", status);
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }

                messageEvent.setMessageObject(jsonObject);
                EventBus.getDefault().post(messageEvent);
                MyLog.d(TAG, "sendUpdatedContactToServer: posted");
            } else {
                MyLog.d(TAG, "sendUpdatedContactToServer: not posted socket not connected");
            }
        } catch (Exception e) {
            MyLog.e(TAG, "sendUpdatedContactToServer: ", e);
        }
    }


}
