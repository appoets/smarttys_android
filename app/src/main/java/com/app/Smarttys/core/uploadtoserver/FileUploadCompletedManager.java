package com.app.Smarttys.core.uploadtoserver;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.Base64;

import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.AudioMessage;
import com.app.Smarttys.core.message.DocumentMessage;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.message.PictureMessage;
import com.app.Smarttys.core.message.VideoMessage;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;
import com.app.Smarttys.status.controller.SharedPrefUtil;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;


public class FileUploadCompletedManager {
    private static final String TAG = "FileUploadCompletedMana";
    private static FileUploadCompletedManager fileUploadCompletedManager = new FileUploadCompletedManager();
    private String mCurrentUserId;
    private FileUploadDownloadManager mFileUploadDownloadManager;
    private Context mContext;

    private FileUploadCompletedManager() {

    }

    public static FileUploadCompletedManager getInstance() {
        return fileUploadCompletedManager;
    }

    public void init(FileUploadDownloadManager fileUploadDownloadManager) {

        mFileUploadDownloadManager = fileUploadDownloadManager;
        mCurrentUserId = SessionManager.getInstance(mFileUploadDownloadManager.mContext).getCurrentUserID();
        mContext = mFileUploadDownloadManager.mContext;
    }


    public void fileUploadCompleted(JSONObject object) {
        mFileUploadDownloadManager.handler.removeCallbacksAndMessages(null);

        try {
            FileUploadDownloadManager.isFileUploading = false;
            int msgType = object.getInt("type");
            String msgId = "";
            if (object.has("id")) {
                msgId = object.getString("id");
                mFileUploadDownloadManager.removeUploadProgress(msgId);
            }
            switch (msgType) {

                case MessageFactory.picture:
                    imageCompleted(msgId, object);
                    break;

                case MessageFactory.audio:
                    audioCompleted(msgId, object);
                    break;

                case MessageFactory.video:
                    videoCompleted(msgId, object);
                    break;

                case MessageFactory.document:
                    documentCompleted(msgId, object);
                    break;

                case MessageFactory.user_profile_pic_update:
                    userProfileImageCompleted(object);
                    break;

                case MessageFactory.group_profile_pic_update:
                    groupProfileImageCompleted(object);
                    break;

            }


        } catch (Exception e) {
            MyLog.e(TAG, "uploadVideoChatFile: ", e);
        }
    }


    private void imageCompleted(String msgId, JSONObject object) {
        try {

            // MyLog.e(TAG, "filetest imageCompleted: sucesss"
            // +object);

            MyLog.d(TAG, "filetest uploadImageChatFile: sucesss");
            String receiverName = "";
            if (object.has("ReceiverName"))
                receiverName = object.getString("ReceiverName");
            String chatType = object.getString("chat_type");
            String caption = object.getString("payload");
            if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_STATUS)) {
                msgId = object.getString("toDocId");
            }
            int fileSize = object.getInt("size");
            String imgPath = object.getString("LocalPath");
            SendMessageEvent sendEvent = new SendMessageEvent();
            MyLog.d(TAG, "filetest uploadImageChatFile: sucesss 2");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imgPath, options);
            int imageHeight = options.outHeight;
            int imageWidth = options.outWidth;


            PictureMessage message = new PictureMessage(mContext);
            JSONObject msgObj = null;
            String fileName = object.getString("filename");
            if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_STATUS)) {
                if (AppUtils.isNetworkAvailable(mContext)) {
                    String docId = object.getString("toDocId");
                    CoreController.getStatusDB(mContext).updateStatusUploaded(docId);
                    msgObj = (JSONObject) message.getPictureMessageObject(caption, msgId, fileSize,
                            imageWidth, imageHeight, fileName, false);
                    msgObj.put("toDocId", docId);//we already save the docId in DB. so maintain same doc_id
                    String[] splitIds = docId.split("-");
                    String id = splitIds[1];
                    msgObj.put("id", id);
                    SharedPrefUtil.setStatusRunning(false);
                    sendEvent.setEventName(SocketManager.EVENT_STATUS);
                    MyLog.d(TAG, "StatusTest--> Success uploadImageChatFile: completed.. status event hit ###" + msgObj);
                }

            } else if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                sendEvent.setEventName(SocketManager.EVENT_GROUP);

                msgObj = (JSONObject) message.getGroupPictureMessageObject(caption, msgId, fileSize,
                        imageWidth, imageHeight, fileName, receiverName);
                MyLog.d(TAG, "imageCompleted directForward: " + msgObj);
            } else {
                MyLog.d(TAG, "filetest uploadImageChatFile: sucesss 3");
                String secretType = object.getString("secret_type");
                boolean isSecretChat = false;
                if (secretType.equalsIgnoreCase("yes")) {
                    isSecretChat = true;
                }

                msgObj = (JSONObject) message.getPictureMessageObject(caption, msgId, fileSize,
                        imageWidth, imageHeight, fileName, isSecretChat);
                sendEvent.setEventName(SocketManager.EVENT_MESSAGE);

                //System.out.println("ImageUploadTimes" + " " + "SuccessEVENTSENT");

                MyLog.e("CheckEventConnection", "Upload File Image Success" + "" + object);

            }

            sendEvent.setMessageObject(msgObj);
            EventBus.getDefault().post(sendEvent);
            MyLog.d(TAG, "filetest uploadImageChatFile: sucesss 4" + msgObj);
            MessageDbController dbController = CoreController.getDBInstance(mContext);
            JSONObject final_object = dbController.fileuploadobjectget(msgId);
            dbController.fileuploadupdateMessage(msgId, "completed", final_object);
            MyLog.d(TAG, "filetest uploadImageChatFile: sucesss 5 final " + final_object);
        } catch (Exception e) {
            MyLog.d(TAG, "filetest uploadImageChatFile: sucesss ", e);
        }
    }

    private void videoCompleted(String msgId, JSONObject object) {
        try {

            String chatType = object.getString("chat_type");
            String receiverName = object.getString("ReceiverName");
            String caption = object.getString("payload");
            String videoPath = object.getString("LocalPath");
            int originalFileSize = object.getInt("size");
            MediaMetadataRetriever mdr = new MediaMetadataRetriever();
            try {
                mdr.setDataSource(videoPath);
            } catch (RuntimeException e) {
            }

            int videoHeight = AppUtils.parseInt(mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
            int videoWidth = AppUtils.parseInt(mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
            String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            MyLog.d(TAG, "FileTest2 uploadCompressVideoFile: before compress bitmap ");
            Bitmap thumbBmp = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MINI_KIND);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            if (thumbBmp != null)
                thumbBmp.compress(Bitmap.CompressFormat.JPEG, 30, out);
//                Bitmap compressBmp = Compressor.getDefault(mContext).compressToBitmap(thumbBmp);
            byte[] thumbArray = out.toByteArray();
            out.close();
            String thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT);
            thumbData = thumbData.replace("\n", "");
            thumbData = thumbData.replace(" ", "");
            if (!thumbData.startsWith("data:image/jpeg;base64,")) {
                thumbData = "data:image/jpeg;base64," + thumbData.trim();
            }
            // Log.d(TAG, "FileTest2 uploadCompressVideoFile: after compress bitmap ");

            VideoMessage message = new VideoMessage(mFileUploadDownloadManager.mContext);
            JSONObject msgObj = null;

            String fileName = object.getString("filename");

            SendMessageEvent sendEvent = new SendMessageEvent();
            if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_STATUS)) {
                MyLog.d(TAG, "StatusTest--> final status upload uploadVideoChatFile: ");

                String statusDocId = "";
                try {
                    statusDocId = object.getString("toDocId");

                } catch (Exception e) {
                    MyLog.e(TAG, "uploadImageChatFile: ", e);
                }


                msgObj = (JSONObject) message.getVideoMessageObject(msgId, originalFileSize,
                        thumbData, videoWidth, videoHeight, duration, fileName, caption, false, true, statusDocId);
                msgObj.put("toDocId", statusDocId);//we already save the docId in DB. so maintain same doc_id
                MyLog.d(TAG, "uploadVideoChatFile: " + msgObj);
                String[] splitIds = statusDocId.split("-");
                String id = splitIds[1];
                msgObj.put("id", id);
                SharedPrefUtil.setStatusRunning(false);
                sendEvent.setEventName(SocketManager.EVENT_STATUS);
                //VIDEO FILE
                CoreController.getStatusDB(mFileUploadDownloadManager.mContext).updateStatusUploaded(statusDocId);
                //REMOVE FROM PREF
                mFileUploadDownloadManager.removeUploadProgress(statusDocId);

            } else if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                sendEvent.setEventName(SocketManager.EVENT_GROUP);

                msgObj = (JSONObject) message.getGroupVideoMessageObject(msgId, originalFileSize,
                        thumbData, videoWidth, videoHeight, duration, fileName, receiverName, caption);
            } else {
                String secretType = object.getString("secret_type");
                boolean isSecretChat = false;
                if (secretType.equalsIgnoreCase("yes")) {
                    isSecretChat = true;
                }

                msgObj = (JSONObject) message.getVideoMessageObject(msgId, originalFileSize,
                        thumbData, videoWidth, videoHeight, duration, fileName, caption, isSecretChat, false, "");
                sendEvent.setEventName(SocketManager.EVENT_MESSAGE);

            }

            sendEvent.setMessageObject(msgObj);
            EventBus.getDefault().post(sendEvent);

            MessageDbController dbController = CoreController.getDBInstance(mFileUploadDownloadManager.mContext);

            JSONObject final_object = dbController.fileuploadobjectget(msgId);
            dbController.fileuploadupdateMessage(msgId, "completed", final_object);
        } catch (Exception e) {
            MyLog.e(TAG, "videoCompleted: ", e);
        }
    }

    private void audioCompleted(String msgId, JSONObject object) {
        try {

            String duration = object.getString("Duration");
            String receiverName = object.getString("ReceiverName");
            String chatType = object.getString("chat_type");
            int audioFrom = object.getInt("audio_type");
            int fileSize = object.getInt("size");


            AudioMessage message = new AudioMessage(mContext);
            JSONObject msgObj;
            String fileName = object.getString("filename");

            SendMessageEvent sendEvent = new SendMessageEvent();

            if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                sendEvent.setEventName(SocketManager.EVENT_GROUP);

                msgObj = (JSONObject) message.getGroupAudioMessageObject(msgId, fileSize,
                        duration, fileName, receiverName, audioFrom);

            } else {
                String secretType = object.getString("secret_type");
                boolean isSecretChat = false;
                if (secretType.equalsIgnoreCase("yes")) {
                    isSecretChat = true;
                }

                msgObj = (JSONObject) message.getAudioMessageObject(msgId, fileSize,
                        duration, fileName, audioFrom, isSecretChat);
                sendEvent.setEventName(SocketManager.EVENT_MESSAGE);

                //System.out.println("ImageUploadTimes" + " " + "AudioSuccessEvent");
            }

            sendEvent.setMessageObject(msgObj);
            EventBus.getDefault().post(sendEvent);

            MessageDbController dbController = CoreController.getDBInstance(mContext);
            JSONObject final_object = dbController.fileuploadobjectget(msgId);
            dbController.fileuploadupdateMessage(msgId, "completed", final_object);

            MyLog.e("CheckEventConnection", "Upload File Audio Success" + "" + object);

        } catch (JSONException e) {
            MyLog.e(TAG, "uploadAudioChatFile: ", e);
        }
    }

    private void documentCompleted(String msgId, JSONObject object) {
        try {
            SendMessageEvent sendEvent = new SendMessageEvent();

            try {

                String receiverName = object.getString("ReceiverName");
                String chatType = object.getString("chat_type");
                int fileSize = object.getInt("size");
                String docPath = object.getString("LocalPath");

                DocumentMessage message = new DocumentMessage(mContext);
                JSONObject msgObj;
                String fileName = object.getString("filename");
                String[] fullPath = docPath.split("/");
                String originalFileName = fullPath[fullPath.length - 1];

                if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                    sendEvent.setEventName(SocketManager.EVENT_GROUP);

                    msgObj = (JSONObject) message.getGroupDocumentMessageObject(msgId, fileSize,
                            originalFileName, docPath, fileName, receiverName);

                } else {
                    String secretType = object.getString("secret_type");
                    boolean isSecretChat = false;
                    if (secretType.equalsIgnoreCase("yes")) {
                        isSecretChat = true;
                    }

                    msgObj = (JSONObject) message.getDocumentMessageObject(msgId, fileSize,
                            originalFileName, docPath, fileName, isSecretChat);
                    sendEvent.setEventName(SocketManager.EVENT_MESSAGE);
                }

                sendEvent.setMessageObject(msgObj);
                EventBus.getDefault().post(sendEvent);

                MessageDbController dbController = CoreController.getDBInstance(mContext);
                JSONObject final_object = dbController.fileuploadobjectget(msgId);
                dbController.fileuploadupdateMessage(msgId, "completed", final_object);

            } catch (JSONException e) {
                MyLog.e(TAG, "uploadDocumentChatFile: ", e);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "documentCompleted: ", e);
        }
    }

    private void groupProfileImageCompleted(JSONObject object) {

        try {
            SendMessageEvent imgEvent = new SendMessageEvent();
            imgEvent.setEventName(SocketManager.EVENT_IMAGE_UPLOAD);
            String imgName = object.getString("ImageName");
            JSONObject imgObject = new JSONObject();
//                    imgObject.put("file", imageEncoded);
            imgObject.put("from", mCurrentUserId);
            imgObject.put("type", "group");
            imgObject.put("ImageName", imgName);


            imgEvent.setMessageObject(imgObject);
            EventBus.getDefault().post(imgEvent);

        } catch (Exception e) {
            MyLog.e(TAG, "groupProfileImageCompleted: ", e);
        }
    }

    private void userProfileImageCompleted(JSONObject object) {
        SendMessageEvent imgEvent = new SendMessageEvent();
        imgEvent.setEventName(SocketManager.EVENT_IMAGE_UPLOAD);

        try {
            String imgName = object.getString("ImageName");
            JSONObject imgObject = new JSONObject();
//                    imgObject.put("file", imageEncoded);
            imgObject.put("from", mCurrentUserId);
            imgObject.put("type", "single");
            imgObject.put("ImageName", imgName);
            imgObject.put("removePhoto", "");

            imgEvent.setMessageObject(imgObject);
            EventBus.getDefault().post(imgEvent);

            MyLog.e("CheckEventConnection", "--------------Profile Picture Uploading SUCCESS-----------");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
