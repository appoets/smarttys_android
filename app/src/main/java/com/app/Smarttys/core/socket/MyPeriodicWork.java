package com.app.Smarttys.core.socket;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.MyLog;

public class MyPeriodicWork extends Worker {
    private static final String TAG = "MyPeriodicWork";
    private Context mContext;

    public MyPeriodicWork(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        mContext = context;
    }

    @Override
    public Result doWork() {
        Log.e(TAG, "doWork: Work is done.");

        if (!AppUtils.isMyServiceRunning(mContext, MessageService.class)) {
            MyLog.e("isMyServiceRunning", "MessageService started");

            AppUtils.startService(mContext, MessageService.class);

            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(new Intent(context, MessageService.class));
            } else {
                context.startService(new Intent(context, MessageService.class));
            }*/

        } else {
            MyLog.e("isMyServiceRunning", "MessageService running already");
          /*  if(!SocketManager.isConnected){
                MessageService.manager.connect();
            }*/

        }
        return Result.success();
    }
}
