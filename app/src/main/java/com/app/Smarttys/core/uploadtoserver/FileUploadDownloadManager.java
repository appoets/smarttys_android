package com.app.Smarttys.core.uploadtoserver;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StatFs;
import android.util.Log;

import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.socket.SocketManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import id.zelory.compressor.Compressor;

/**
 * Created by CAS60 on 1/27/2017.
 */
public class FileUploadDownloadManager {

    public static final boolean FETCH_DOWNLOAD_ENABLED = true;
    private static final String TAG = "FileUploadDownloadManag";
    private static final boolean isCompressEnabled = true;
    public static boolean isFileUploading = false;
    private final String FILE_MANAGER_PREF = "FileManagerPref";
    private final String FILE_UPLOAD_STATUS_PREF = "UploadFileStatusPref";
    private final String FILE_DOWNLOAD_STATUS_PREF = "DownloadFileStatusPref";
    private final String FILE_PAUSE_PREF = "FilePausePref";
    private final String KEY_UPLOAD_SIZE = "-uploadSize";
    private final String REMOVE_PHOTO = "RemovePhoto";
    private final String KEY_DOWNLOAD_SIZE = "-downloadSize";
    private final String FILE_DOWNLOAD_PAUSE_RESUME_PREF = "DownloadFilePauseResumePref";
    public Context mContext;
    public Handler handler = new Handler(Looper.getMainLooper());
    boolean suc = false;
    private String mCurrentUserId;
    private HashMap<String, Integer> uploadedItemsMap = new HashMap<>();
    private SharedPreferences filePref, uploadFileStatusPref, downloadFileStatusPref, pauseFilePref, downloadpauseresumePref;
    private SharedPreferences.Editor filePrefEditor, uploadFileStatusPrefEditor, downloadFileStatusPrefEditor, pauseFileEditor, DownloadPauseResumeEditor;
    private Gson gson;

    public FileUploadDownloadManager(Context mContext) {
        this.mContext = mContext;
        uploadedItemsMap = new HashMap<>();
        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
        filePref = mContext.getSharedPreferences(FILE_MANAGER_PREF, Context.MODE_PRIVATE);
        filePrefEditor = filePref.edit();
        uploadFileStatusPref = mContext.getSharedPreferences(FILE_UPLOAD_STATUS_PREF, Context.MODE_PRIVATE);
        uploadFileStatusPrefEditor = uploadFileStatusPref.edit();
        downloadFileStatusPref = mContext.getSharedPreferences(FILE_DOWNLOAD_STATUS_PREF, Context.MODE_PRIVATE);
        downloadFileStatusPrefEditor = downloadFileStatusPref.edit();
        pauseFilePref = mContext.getSharedPreferences(FILE_PAUSE_PREF, Context.MODE_PRIVATE);
        downloadpauseresumePref = mContext.getSharedPreferences(FILE_DOWNLOAD_PAUSE_RESUME_PREF, Context.MODE_PRIVATE);
    }

    public static String getFileExtnFromFile(File file) {
        String fileName = file.getName();
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return "." + fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    public static String getFileExtnFromPath(String fileName) {
        try {
            if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
                return "." + fileName.substring(fileName.lastIndexOf(".") + 1);
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }
    }

    //StorageDetails
    public static boolean externalMemoryAvailable() {
        return Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED);
    }

    public static String getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize;
        long availableBlocks;

        blockSize = stat.getBlockSizeLong();
        availableBlocks = stat.getAvailableBlocksLong();

        return formatSize(availableBlocks * blockSize);
    }

    public static String getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize;
        long totalBlocks;

        blockSize = stat.getBlockSizeLong();
        totalBlocks = stat.getBlockCountLong();

        return formatSize(totalBlocks * blockSize);
    }


    //-----------------------------------------------------------DownloadStatusPauseResume Maintain Pref-----------------------------------------

    public static String getAvailableExternalMemorySize() {
        if (externalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize;
            long availableBlocks;

            blockSize = stat.getBlockSizeLong();
            availableBlocks = stat.getAvailableBlocksLong();
            return formatSize(availableBlocks * blockSize);
        } else {
            return "";
        }
    }

    public static String getTotalExternalMemorySize() {
        if (externalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long totalBlocks;

            long blockSize;

            blockSize = stat.getBlockSizeLong();
            totalBlocks = stat.getBlockCountLong();

            return formatSize(totalBlocks * blockSize);
        } else {
            return "";
        }
    }

    public static String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
            suffix = " KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = " MB";
                size /= 1024;
            }
        } else {
            suffix = "  Bytes";
        }

        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        resultBuffer.append(suffix);
        return resultBuffer.toString();
    }

    public void setUploadProgress(String msgId, int uploadedSize, JSONObject uploadObj) {
        if (uploadObj.has("buffer")) {
            uploadObj.remove("buffer");
        }

        String uploadKey = msgId + KEY_UPLOAD_SIZE;

        filePrefEditor.putInt(uploadKey, uploadedSize);
        filePrefEditor.putString(msgId, uploadObj.toString());
        filePrefEditor.apply();

        if (uploadedSize > 0) {
            uploadFileStatusPrefEditor.putBoolean(msgId, true);
            uploadFileStatusPrefEditor.apply();
        }
    }

    //----------------------------------------------------------Close Download Maintain Method------------------------------------------

    public void setPauseFileStatus(String msgId, boolean isPaused) {
        if (pauseFilePref != null) {
            pauseFileEditor = pauseFilePref.edit();
            pauseFileEditor.putBoolean(msgId, isPaused);
            pauseFileEditor.apply();
        }
    }

    public void setPauseFileObject(String msgId, JSONObject jsonObject) {
        if (pauseFilePref != null) {
            pauseFileEditor = pauseFilePref.edit();
            pauseFileEditor.putString("pauseObject_" + msgId, gson.toJson(jsonObject));
            pauseFileEditor.apply();
        }
    }

    public JSONObject getPauseFileObject(String msgId) {
        if (pauseFilePref != null) {
            String pauseFileObject = pauseFilePref.getString("pauseObject_" + msgId, "");
            if (!pauseFileObject.isEmpty()) {
                return gson.fromJson(pauseFileObject, JSONObject.class);
            }
        }
        return null;
    }

    public boolean isFilePaused(String msgId) {
        if (pauseFilePref != null)
            return pauseFilePref.getBoolean(msgId, false);

        return false;
    }

    public void setDownloadResumePauseFileStatus(String msgId, boolean isPaused) {
        if (FETCH_DOWNLOAD_ENABLED) {
            if (isPaused) {
                FetchDownloadManager.getInstance().pauseFile(msgId);
            }
        }
        if (downloadpauseresumePref != null) {
            DownloadPauseResumeEditor = downloadpauseresumePref.edit();
            DownloadPauseResumeEditor.putBoolean("download_status_" + msgId, isPaused);
            DownloadPauseResumeEditor.apply();
        }
    }

    public void setDownloadResumePauseFileObject(String msgId, JSONObject jsonObject) {
        if (downloadpauseresumePref != null) {
            DownloadPauseResumeEditor = downloadpauseresumePref.edit();
            DownloadPauseResumeEditor.putString("downloadpauseObject_" + msgId, gson.toJson(jsonObject));
            DownloadPauseResumeEditor.apply();


        }
    }

    public JSONObject getDownloadResumePauseFileObject(String msgId) {

        try {

            if (downloadpauseresumePref != null) {
                String Key = "downloadpauseObject_" + msgId;

                String pauseFileObject = downloadpauseresumePref.getString(Key, "");

                if (!pauseFileObject.isEmpty()) {
                    return gson.fromJson(pauseFileObject, JSONObject.class);
                }
            }

        } catch (Exception e) {

            MyLog.e("CheckEventConnection", e.toString());
        }


        return null;
    }

    public boolean isDownloadFilePaused(String msgId) {

        try {

            if (downloadpauseresumePref != null)
                return downloadpauseresumePref.getBoolean("download_status_" + msgId, false);

        } catch (Exception e) {

            MyLog.e("CheckEventConnection", "Resume Download File Status" + "" + e.toString());
        }


        return false;
    }

    public int getUploadProgress(String msgId) {
        String uploadKey = msgId + KEY_UPLOAD_SIZE;

        if (filePref.contains(uploadKey)) {
            return filePref.getInt(uploadKey, 0);
        } else {
            return -1;
        }
    }

    public JSONObject getUploadProgressObject(String msgId) {
        JSONObject object = new JSONObject();
        if (filePref.contains(msgId)) {
            try {
                String msgObj = filePref.getString(msgId, "");
                object = new JSONObject(msgObj);
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        }
        return object;
    }

    public void removeUploadProgress(String msgId) {
        String uploadKey = msgId + KEY_UPLOAD_SIZE;
        filePrefEditor.remove(msgId);
        filePrefEditor.remove(uploadKey);
        filePrefEditor.apply();

        uploadFileStatusPrefEditor.remove(msgId);
        uploadFileStatusPrefEditor.apply();
    }

    public void setUploadConnectionOffline() {
        Map<String, ?> uploadMsgIds = uploadFileStatusPref.getAll();
        for (String key : uploadMsgIds.keySet()) {
            uploadFileStatusPrefEditor.putBoolean(key, false);
            uploadFileStatusPrefEditor.apply();
        }
    }

    public void setDownloadConnectionOffline() {
        Map<String, ?> downloadMsgIds = downloadpauseresumePref.getAll();
        for (String key : downloadMsgIds.keySet()) {

            try {

                DownloadPauseResumeEditor.putBoolean(key, false);
                DownloadPauseResumeEditor.apply();

            } catch (Exception e) {

            }

        }
    }

    public Boolean isRemovePhoto() {
        return filePref.getBoolean(REMOVE_PHOTO, false);
    }

    public void setRemovePhoto(boolean isRemove) {
        SharedPreferences.Editor prefEditor = filePref.edit();
        prefEditor.putBoolean(REMOVE_PHOTO, isRemove);
        prefEditor.apply();
    }

    public void setDownloadProgress(String msgId, int downloadedSize, JSONObject downloadObj) {
        downloadFileStatusPrefEditor.putBoolean(msgId, true);
        downloadFileStatusPrefEditor.apply();

        String downKey = msgId + KEY_DOWNLOAD_SIZE;
        filePrefEditor.putString(msgId, downloadObj.toString());
        filePrefEditor.putInt(downKey, downloadedSize);
        filePrefEditor.apply();
    }

    public int getDownloadProgress(String msgId) {
        String downKey = msgId + KEY_DOWNLOAD_SIZE;
        return filePref.getInt(downKey, 0);
       /* if (filePref.contains(downKey)) {

        } else {
            return -1;
        }*/
    }

  /*  public  void listen() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            ((CoreController)mContext)
                    .bus()
                    .toObservable()
                    .subscribe(new Consumer<Object>() {
                        @Override
                        public void accept(Object object) {
                            if (object instanceof EventTypeOne) {
                                Logger.d("Event Type One Received");
                            } else if (object instanceof EventTypeTwo) {
                                Logger.d("Event Type Two Received");
                            }
                        }
                    });
        }
    }*/

    public void removeOfflineDownloadStatusNotExist(String msgId) {
        downloadFileStatusPrefEditor.putBoolean(msgId, true);
        downloadFileStatusPrefEditor.apply();
    }

    public void removeDownloadProgress(String msgId) {
        String downKey = msgId + KEY_DOWNLOAD_SIZE;
        filePrefEditor.remove(msgId);
        filePrefEditor.remove(downKey);
        filePrefEditor.apply();

        downloadFileStatusPrefEditor.putBoolean(msgId, true);
        downloadFileStatusPrefEditor.apply();
    }

    public void uploadFile(EventBus eventBus, JSONObject object) {
        MessageDbController db = CoreController.getDBInstance(mContext);
        try {
            String msgId = object.getString("id");
            String chatType = "";
            if (object.has("chat_type")) {
                chatType = object.getString("chat_type");
            }
            if (chatType != null && !chatType.equals("status")) {
                db.fileuploadinsertitem(msgId, "uploading", object);
                db.FirsttimeFileUploadinsert(msgId, "firsttimestoredb", object);
            }

            // setUploadProgress(msgId, 1, object);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_NEW_FILE_MESSAGE);
        event.setMessageObject(object);
        eventBus.post(event);
    }

    public void startFileUpload(EventBus eventBus, JSONObject object) {
        try {
            isFileUploading = true;
            MyLog.d(TAG, "startFileUpload: <Status>");
            // MyLog.d(TAG, "startFileUpload: <Status> object"+object);

            int msgType = object.getInt("type");


            //  MyLog.e("CheckEventConnection", "MSG TYPE" + "" + msgType);

            switch (msgType) {

                case MessageFactory.picture:
                    uploadImageChatFile(eventBus, object);
                    break;

                case MessageFactory.audio:
                    uploadAudioChatFile(eventBus, object);
                    break;

                case MessageFactory.video:
                    VideoUploadFile(eventBus, object);
                    break;

                case MessageFactory.document:
                    uploadDocumentChatFile(eventBus, object);
                    break;

                case MessageFactory.user_profile_pic_update:
                    uploadUserProfilePic(eventBus, object);
                    //rxjavaupload(eventBus, object);
                    break;

                case MessageFactory.group_profile_pic_update:
                    uploadGroupProfilePic(eventBus, object);
                    break;
                case MessageFactory.text:
                    //TextStatusUpload(eventBus, object);
                    break;

            }

        } catch (Exception e) {
            MyLog.e(TAG, "startFileUpload: ", e);
        }
    }

    private void TextStatusUpload(EventBus eventBus, JSONObject object) {

        SendMessageEvent event = new SendMessageEvent();

        try {

            event.setEventName(SocketManager.EVENT_STATUS);
            Log.e("TextStatusUpload", "TextStatusUpload" + object);
            event.setMessageObject(object);
            eventBus.post(event);
            Log.e("CheckEventConnection", "Text Status Uploading..." + "" + object);

        } catch (Exception e) {
            Log.e(TAG, "", e);
        }

    }

    private void uploadGroupProfilePic(EventBus eventBus, JSONObject object) {
        try {
            int err = object.getInt("err");
            String from = object.getString("from");
            String imgName = object.getString("ImageName");
            String imgPath = object.getString("LocalPath");
            int type = object.getInt("type");
            int bufferAt = object.getInt("bufferAt");
            String uploadType = object.getString("uploadType");

            if (err == 0) {
                bufferAt += 1;
            }

            File file = new File(imgPath);
            File compressFile = Compressor.getDefault(mContext).compressToFile(file);

            byte[] bytesArray = new byte[(int) compressFile.length()];

            FileInputStream fis = new FileInputStream(compressFile);
            fis.read(bytesArray); //read file into bytes[]
            fis.close();

            int uploadedSize = 0;
            if (object.has("UploadedSize")) {
                uploadedSize = object.getInt("UploadedSize");
            }

            int originalFileSize = (int) compressFile.length();
            if (!object.has("size")) {
                object.put("size", originalFileSize);
            }

            if (uploadedSize < originalFileSize) {
                ByteSplit byteSplit = FileUploadUtils.divideArray(bytesArray, object, uploadedSize);
                byte[] divideArray = byteSplit.bytes;

                SendMessageEvent event = new SendMessageEvent();
                try {
                    object.put("buffer", divideArray);
                    object.put("bufferAt", bufferAt);
                    object.put("UploadedSize", uploadedSize + divideArray.length);

                    if (byteSplit.isFinalByte) {
                        object.put("FileEnd", 1);
                    } else {
                        object.put("FileEnd", 0);
                    }
/*
                    event.setEventName(SocketManager.EVENT_FILE_UPLOAD);
                    event.setMessageObject(object);
                    eventBus.post(event);
*/
                    checkDuplicateAndSend(object);

                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            } else {
                FileUploadCompletedManager.getInstance().init(this);
                FileUploadCompletedManager.getInstance().fileUploadCompleted(object);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "Cannot read file: " + e.toString());
        }
    }

    private void rxjavaupload(EventBus eventBus, JSONObject object) {
        /*  RxBus _rxBus= (RxBus) CoreController.getRxBusSingleton();

         *//* if (_rxBus.hasObservers()) {
            _rxBus.send(new RxBusDemoFragment.TapEvent());
        }*//*

        try {
            int err = object.getInt("err");
            String from = object.getString("from");
            String imgName = object.getString("ImageName");
            String imgPath = object.getString("LocalPath");
            int type = object.getInt("type");
            int bufferAt = 0;
            if (object.has("bufferAt"))
                bufferAt = object.getInt("bufferAt");
            String uploadType = object.getString("uploadType");


            if (err == 0) {
                bufferAt += 1;
            }

            File file = new File(imgPath);
            File compressFile = Compressor.getDefault(mContext).compressToFile(file);

            byte[] bytesArray = new byte[(int) compressFile.length()];
            MyLog.d(TAG, "uploadUserProfilePic: file length: " + file.length());
            MyLog.d(TAG, "uploadUserProfilePic: compressed file length: " + compressFile.length());
            MyLog.d(TAG, "uploadUserProfilePic: " + bytesArray.length);
            FileInputStream fis = new FileInputStream(compressFile);
            fis.read(bytesArray); //read file into bytes[]
            fis.close();

            int uploadedSize = 0;
            if (object.has("UploadedSize")) {
                uploadedSize = object.getInt("UploadedSize");
            }

            int originalFileSize = (int) compressFile.length();
            if (!object.has("size")) {
                object.put("size", originalFileSize);
            }

            if (uploadedSize < originalFileSize) {
                MyLog.d(TAG, "uploadUserProfilePic uploadedSize: " + uploadedSize);
                ByteSplit byteSplit = FileUploadUtils.divideArray(bytesArray, object, uploadedSize);
                byte[] divideArray = byteSplit.bytes;
                MyLog.d(TAG, "uploadUserProfilePic divided bytes len: " + divideArray.length);
                SendMessageEvent event = new SendMessageEvent();
                try {
                    JSONObject msgObj = new JSONObject();

                    msgObj.put("ImageName", imgName);
                    msgObj.put("buffer", divideArray);
                    msgObj.put("bufferAt", bufferAt);
                    msgObj.put("UploadedSize", uploadedSize + divideArray.length);
                    msgObj.put("LocalPath", imgPath);
                    msgObj.put("type", type);
                    msgObj.put("from", from);
                    msgObj.put("uploadType", uploadType);
                    msgObj.put("removePhoto", "");


                    if (byteSplit.isFinalByte) {
                        object.put("FileEnd", 1);
                    } else {
                        object.put("FileEnd", 0);
                    }

                    event.setEventName(SocketManager.EVENT_FILE_UPLOAD);
                    event.setMessageObject(msgObj);
                  //  eventBus.post(event);
                    if (_rxBus.hasObservers()) {
                        _rxBus.send(event);
                    }
                    MyLog.e("CheckEventConnection", "--------------Profile Picture Uploading-----------");
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            } else {
                MyLog.d(TAG, "uploadUserProfilePic: completed");
                FileUploadCompletedManager.getInstance().init(this);
                FileUploadCompletedManager.getInstance().fileUploadCompleted(object);

            }
        } catch (Exception e) {
            MyLog.e(TAG, "Can not read file: " + e.toString());
        }*/
    }

    private void uploadUserProfilePic(EventBus eventBus, JSONObject object) {
        try {
            Log.e(TAG, "uploadUserProfilePic" + object);
            int err = object.getInt("err");
            String from = object.getString("from");
            String imgName = object.getString("ImageName");
            String imgPath = object.getString("LocalPath");
            int type = object.getInt("type");
            int bufferAt = 0;
            if (object.has("bufferAt"))
                bufferAt = object.getInt("bufferAt");
            String uploadType = object.getString("uploadType");


            if (err == 0) {
                bufferAt += 1;
            }

            File file = new File(imgPath);
            File compressFile = Compressor.getDefault(mContext).compressToFile(file);

            byte[] bytesArray = new byte[(int) compressFile.length()];
            MyLog.d(TAG, "uploadUserProfilePic: file length: " + file.length());
            MyLog.d(TAG, "uploadUserProfilePic: compressed file length: " + compressFile.length());
            MyLog.d(TAG, "uploadUserProfilePic: " + bytesArray.length);
            FileInputStream fis = new FileInputStream(compressFile);
            fis.read(bytesArray); //read file into bytes[]
            fis.close();

            int uploadedSize = 0;
            if (object.has("UploadedSize")) {
                uploadedSize = object.getInt("UploadedSize");
            }

            int originalFileSize = (int) compressFile.length();
            if (!object.has("size")) {
                object.put("size", originalFileSize);
            }
            //  MyLog.d(TAG, "uploadUserProfilePic: completed" + object);

            if (uploadedSize < originalFileSize) {
                MyLog.d(TAG, "uploadUserProfilePic uploadedSize: " + uploadedSize);
                //     Log.e(TAG, "uploadUserProfilePic: uploading" + object);

                ByteSplit byteSplit = FileUploadUtils.divideArray(bytesArray, object, uploadedSize);
                byte[] divideArray = byteSplit.bytes;
                //     MyLog.d(TAG, "uploadUserProfilePic divided bytes len: " + divideArray.length);
                try {
                    JSONObject msgObj = new JSONObject();

                    msgObj.put("ImageName", imgName);
                    msgObj.put("buffer", divideArray);
                    msgObj.put("bufferAt", bufferAt);
                    msgObj.put("UploadedSize", uploadedSize + divideArray.length);
                    msgObj.put("LocalPath", imgPath);
                    msgObj.put("type", type);
                    msgObj.put("from", from);
                    msgObj.put("uploadType", uploadType);
                    msgObj.put("removePhoto", "");


                    if (byteSplit.isFinalByte) {
                        msgObj.put("FileEnd", 1);
                    } else {
                        msgObj.put("FileEnd", 0);
                    }


                    checkDuplicateAndSendPic(msgObj, object);

                    MyLog.e("CheckEventConnection", "--------------Profile Picture Uploading-----------");
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            } else {
                //  MyLog.d(TAG, "uploadUserProfilePic: completed"+object);

                Log.e(TAG, "uploadUserProfilePic: completed" + object);

                FileUploadCompletedManager.getInstance().init(this);
                FileUploadCompletedManager.getInstance().fileUploadCompleted(object);

                try {
                    String msgId = object.getString("ImageName");

                    MessageDbController dbController = CoreController.getDBInstance(mContext);
                    dbController.userpicupdateMessage(msgId, "completed", object);

                } catch (Exception e) {
                    MyLog.e(TAG, "uploadImageChatFile: ", e);
                }

            }
        } catch (Exception e) {
            MyLog.e(TAG, "Can not read file: " + e.toString());
        }
    }

    public void updatePropic(EventBus eventBus) {
        SendMessageEvent imgEvent = new SendMessageEvent();
        imgEvent.setEventName(SocketManager.EVENT_IMAGE_UPLOAD);

        try {
            JSONObject imgObject = new JSONObject();
//                    imgObject.put("file", imageEncoded);
            imgObject.put("from", mCurrentUserId);
            imgObject.put("type", "single");
            imgObject.put("ImageName", "");
            if (isRemovePhoto()) {
                imgObject.put("removePhoto", "yes");
            }

            imgEvent.setMessageObject(imgObject);
            eventBus.post(imgEvent);
        } catch (Exception ex) {
            MyLog.e(TAG, "updatePropic: ", ex);
        }
    }

    private void uploadImageChatFile(EventBus eventBus, JSONObject object) {

        try {
            //System.gc();
            MyLog.d(TAG, "filetest@ uploadImageChatFile: start()");
            int err = object.getInt("err");
            int bufferAt = object.getInt("bufferAt");
            String imgPath = object.getString("LocalPath");
            MyLog.e(TAG, "filetest@ uploadImageChatFile: start()" + object);

            if (object.has("thumbnail_data")) {
                object.remove("thumbnail_data");
            }

            int uploadedSize = 0;
            if (object.has("UploadedSize")) {
                uploadedSize = object.getInt("UploadedSize");
            }


            if (err == 0) {
                bufferAt += 1;
            }


            if (bufferAt == 0) {
                try {
                    String msgId = object.getString("toDocId");
                    CoreController.getDBInstance(mContext).deleteSendNewMessage(msgId);
                } catch (Exception e) {
                    MyLog.e(TAG, "uploadImageChatFile: ", e);
                }

            }
            MyLog.d(TAG, "uploadImageChatFile: before");
            File file = new File(imgPath);

            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize = 8;
            Bitmap compressBmp = BitmapFactory.decodeFile(file.getPath(), bmOptions);
            if (compressBmp == null) {
                String[] fullPath = file.getPath().split("/");
                String originalFileName = fullPath[fullPath.length - 1];
                String publicDirPath = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).getAbsolutePath();
                file = new File(publicDirPath + "/" + originalFileName);
            } /*else {

            }*/
            try {
                File compressedFile = Compressor.getDefault(mContext).compressToFile(file);
                if (compressedFile != null)
                    file = compressedFile;
            } catch (Exception e) {
                MyLog.d(TAG, "uploadImageChatFile: ", e);
            }
            MyLog.d(TAG, "uploadImageChatFile: after");

            byte[] bytesArray = new byte[(int) file.length()];

            FileInputStream fis = new FileInputStream(file);
            fis.read(bytesArray); //read file into bytes[]
            fis.close();
            int originalFileSize = 0;
            if (object.has("size")) {
                originalFileSize = object.getInt("size");
            } else {
                originalFileSize = (int) file.length();
                object.put("size", originalFileSize);
            }


            if (uploadedSize < originalFileSize) {
                ByteSplit byteSplit = FileUploadUtils.divideArray(bytesArray, object, uploadedSize);
                byte[] divideArray = byteSplit.bytes;
                if (bufferAt >= 0) {
                    try {
                        String msgId = object.getString("toDocId");
                        // setUploadProgress(msgId, bufferAt * divideArray[bufferAt].length, object);

                        MessageDbController dbController = CoreController.getDBInstance(mContext);
                        dbController.fileuploadupdateMessage(msgId, "uploading", object);


                    } catch (Exception e) {
                        MyLog.e(TAG, "uploadImageChatFile: ", e);
                    }
                } else {
                    bufferAt = 0;
                }


                try {
                    object.put("buffer", divideArray);
                    object.put("bufferAt", bufferAt);
                    object.put("UploadedSize", uploadedSize + divideArray.length);

                    if (byteSplit.isFinalByte) {
                        object.put("FileEnd", 1);
                    } else {
                        object.put("FileEnd", 0);
                    }
                    MyLog.d(TAG, "filetest@ uploadImageChatFile uploadSize: " + uploadedSize);
                    MyLog.d(TAG, "filetest@ uploadImageChatFile finalObj: " + object);
                    MyLog.d(TAG, "status_duplicate send: " + object);

                    checkDuplicateAndSend(object);
                    MyLog.e("CheckEventConnection", "Uploading Server Sended Pending Files" + "" + object);

                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            } else {
                FileUploadCompletedManager.getInstance().init(this);
                FileUploadCompletedManager.getInstance().fileUploadCompleted(object);

            }
        } catch (Exception e) {
            MyLog.d(TAG, "filetest uploadImageChatFile: sucesss ", e);
        }
    }

    private void checkDuplicateAndSendPic(JSONObject object, JSONObject mSendingObject) {
        SendMessageEvent event = new SendMessageEvent();
        String id = "";
        int uploadedSize = 0;
        Log.d(TAG, "checkDuplicateAndSend: " + uploadedItemsMap);
        try {
            if (object.has("id")) {
                id = object.getString("id");
            } else if (object.has("ImageName")) {
                id = object.getString("ImageName");
            }

            if (object.has("UploadedSize")) {
                uploadedSize = object.getInt("UploadedSize");
            }

            if (id != null && id.length() > 0 && uploadedItemsMap.containsKey(id)) {
                int oldUploadedSize = uploadedItemsMap.get(id);
                if (uploadedSize > 0 && oldUploadedSize > 0) {
                    if (oldUploadedSize == uploadedSize) {
                        Log.e(TAG, "duplicate skipped uploadedsize:" + uploadedSize);
                        return;
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "checkDuplicateAndSend: ", e);
        }
        if (id != null && id.length() > 0 && uploadedSize > 0) {
            uploadedItemsMap.put(id, uploadedSize);
        }
       /* try {
            String msgId = mSendingObject.getString("ImageName");
            MessageDbController dbController = CoreController.getDBInstance(mContext);
            dbController.userpicupdateMessage(msgId, "uploading", mSendingObject);
            Log.e(TAG, "uploading" + mSendingObject);


        } catch (Exception e) {
            MyLog.e(TAG, "uploadImageChatFile: ", e);
        }*/
        event.setEventName(SocketManager.EVENT_FILE_UPLOAD);
        event.setMessageObject(object);
        EventBus.getDefault().post(event);


    }


    //--------------------------------------------------Continous Video File Download------------------------------------------

    private void checkDuplicateAndSend(JSONObject object) {
        SendMessageEvent event = new SendMessageEvent();
        String id = "";
        int uploadedSize = 0;
        Log.d(TAG, "checkDuplicateAndSend: " + uploadedItemsMap);
        try {
            if (object.has("id")) {
                id = object.getString("id");
            } else if (object.has("ImageName")) {
                id = object.getString("ImageName");
            }

            if (object.has("UploadedSize")) {
                uploadedSize = object.getInt("UploadedSize");
            }

            if (id != null && id.length() > 0 && uploadedItemsMap.containsKey(id)) {
                int oldUploadedSize = uploadedItemsMap.get(id);
                if (uploadedSize > 0 && oldUploadedSize > 0) {
                    if (oldUploadedSize == uploadedSize) {
                        Log.d(TAG, "duplicate skipped uploadedsize:" + uploadedSize);
                        return;
                    }
                }

            }
        } catch (Exception e) {
            Log.e(TAG, "checkDuplicateAndSend: ", e);
        }
        if (id != null && id.length() > 0 && uploadedSize > 0) {
            uploadedItemsMap.put(id, uploadedSize);
        }
        event.setEventName(SocketManager.EVENT_FILE_UPLOAD);
        event.setMessageObject(object);
        EventBus.getDefault().post(event);
    }

    private void uploadAudioChatFile(EventBus eventBus, JSONObject object) {
        try {
            int err = object.getInt("err");
            String msgId = object.getString("id");
            String audioPath = object.getString("LocalPath");
            int bufferAt = object.getInt("bufferAt");

            if (err == 0) {
                bufferAt += 1;
            }
            if (bufferAt == 0) {
                CoreController.getDBInstance(mContext).deleteSendNewMessage(msgId);
            }

            File file = new File(audioPath);
            byte[] bytesArray = new byte[(int) file.length()];

            FileInputStream fis = new FileInputStream(file);
            fis.read(bytesArray); //read file into bytes[]
            fis.close();

            int originalFileSize = (int) file.length();
            if (!object.has("size")) {
                object.put("size", originalFileSize);
            }


            int uploadedSize = 0;
            if (object.has("UploadedSize")) {
                uploadedSize = object.getInt("UploadedSize");
            }
            if (uploadedSize < originalFileSize) {
                ByteSplit byteSplit = FileUploadUtils.divideArray(bytesArray, object, uploadedSize);
                byte[] divideArray = byteSplit.bytes;
                if (bufferAt >= 0) {
                    MessageDbController dbController = CoreController.getDBInstance(mContext);
                    dbController.fileuploadupdateMessage(msgId, "uploading", object);
                }

                try {
                    object.put("buffer", divideArray);
                    object.put("bufferAt", bufferAt);
                    object.put("UploadedSize", uploadedSize + divideArray.length);

                    if (byteSplit.isFinalByte) {
                        object.put("FileEnd", 1);
                    } else {
                        object.put("FileEnd", 0);
                    }

/*                    SendMessageEvent event = new SendMessageEvent();
                    event.setEventName(SocketManager.EVENT_FILE_UPLOAD);
                    event.setMessageObject(object);
                    eventBus.post(event);*/
                    checkDuplicateAndSend(object);

                    MyLog.e("CheckEventConnection", "Upload Audio File Uploading" + "" + object);

                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            } else {
                FileUploadCompletedManager.getInstance().init(this);
                FileUploadCompletedManager.getInstance().fileUploadCompleted(object);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "Can not read file: " + e.toString());
        }
    }

    private void VideoUploadFile(EventBus eventBus, JSONObject object) {
        try {
            String msgId = object.getString("id");
            MessageDbController dbController = CoreController.getDBInstance(mContext);
            String Upload_status = dbController.fileuploadStatusget(msgId);
            Log.e(TAG, "Upload_status" + Upload_status);
            if (Upload_status.equalsIgnoreCase("uploading")) {
                uploadCompressVideoFile(eventBus, object);
            } else if (Upload_status.equalsIgnoreCase("pause")) {
                dbController.fileuploadupdateMessage(msgId, "pause", object);
            }

            //Check video is sent from status then the uploadstatus is empty so send it ...
            if (AppUtils.isEmpty(Upload_status)) {
                uploadCompressVideoFile(eventBus, object);
            }

        } catch (Exception e) {
            MyLog.e(TAG, "uploadVideoChatFile: ", e);
        }

    }

    //Updated code for Video Uploading
    private void uploadCompressVideoFile(EventBus eventBus, JSONObject object) {


        try {

            int err = object.getInt("err");
            String msgId = object.getString("id");
            int bufferAt = object.getInt("bufferAt");

            String imgPath = object.getString("LocalPath");


            int uploadedSize = 0;
            if (object.has("UploadedSize")) {
                uploadedSize = object.getInt("UploadedSize");
            }

            if (object.has("thumbnail_data")) {
                object.remove("thumbnail_data");
            }

            if (err == 0) {
                bufferAt += 1;
            }
            File file = new File(imgPath);

            byte[] bytesArray = new byte[(int) file.length()];


            FileInputStream fis = new FileInputStream(file);
            fis.read(bytesArray); //read file into bytes[]
            fis.close();

            int originalFileSize = (int) file.length();
            if (!object.has("size")) {
                object.put("size", originalFileSize);
            }


            if (uploadedSize < originalFileSize) {
                ByteSplit byteSplit = FileUploadUtils.divideArray(bytesArray, object, uploadedSize);
                byte[] divideArray = byteSplit.bytes;


                if (bufferAt >= 0) {
                    MessageDbController dbController = CoreController.getDBInstance(mContext);
                    dbController.fileuploadupdateMessage(msgId, "uploading", object);
                    MyLog.d(TAG, "CheckEventConnection: UPLOADING OBJECT SET DB------------------ " + object);
                }

                try {
                    object.put("buffer", divideArray);
                    object.put("bufferAt", bufferAt);
                    object.put("UploadedSize", uploadedSize + divideArray.length);
                    if (byteSplit.isFinalByte) {
                        object.put("FileEnd", 1);
                    } else {
                        object.put("FileEnd", 0);
                    }

                    checkDuplicateAndSend(object);
/*                    SendMessageEvent event = new SendMessageEvent();
                    event.setEventName(SocketManager.EVENT_FILE_UPLOAD);
                    event.setMessageObject(object);
                    eventBus.post(event);*/

                } catch (Exception e) {
                    MyLog.e(TAG, "uploadVideoChatFile: ", e);
                }
            } else {

                FileUploadCompletedManager.getInstance().init(this);
                FileUploadCompletedManager.getInstance().fileUploadCompleted(object);


            }
        } catch (FileNotFoundException ex) {
            FileUploadCompletedManager.getInstance().fileUploadCompleted(object);
        } catch (Exception e) {
            MyLog.e(TAG, "uploadVideoChatFile: ", e);
            MyLog.d(TAG, "CheckEventConnection: ERROR---------------------------");
        }
    }

    private void uploadDocumentChatFile(EventBus eventBus, JSONObject object) {
        try {
            int err = object.getInt("err");
            int bufferAt = object.getInt("bufferAt");
            String docPath = object.getString("LocalPath");

            if (err == 0) {
                bufferAt += 1;
            }

            if (bufferAt == 0) {
                try {
                    String msgId = object.getString("toDocId");
                    CoreController.getDBInstance(mContext).deleteSendNewMessage(msgId);
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }

            }

            File file = new File(docPath);
            byte[] bytesArray = new byte[(int) file.length()];

            FileInputStream fis = new FileInputStream(file);
            fis.read(bytesArray); //read file into bytes[]
            fis.close();
            int originalFileSize = (int) file.length();
            if (!object.has("size")) {
                object.put("size", originalFileSize);
            }
            int uploadedSize = 0;
            if (object.has("UploadedSize")) {
                uploadedSize = object.getInt("UploadedSize");
            }

            if (uploadedSize < originalFileSize) {
                ByteSplit byteSplit = FileUploadUtils.divideArray(bytesArray, object, uploadedSize);
                byte[] divideArray = byteSplit.bytes;


                if (bufferAt >= 0) {

                    String msgId = object.getString("toDocId");

                    MessageDbController dbController = CoreController.getDBInstance(mContext);

                    dbController.fileuploadupdateMessage(msgId, "uploading", object);
                }
                SendMessageEvent event = new SendMessageEvent();
                try {
                    object.put("buffer", divideArray);
                    object.put("bufferAt", bufferAt);
                    object.put("UploadedSize", uploadedSize + divideArray.length);

                    if (byteSplit.isFinalByte) {
                        object.put("FileEnd", 1);
                    } else {
                        object.put("FileEnd", 0);
                    }
/*                    event.setEventName(SocketManager.EVENT_FILE_UPLOAD);
                    event.setMessageObject(object);
                    eventBus.post(event);*/
                    checkDuplicateAndSend(object);

                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            } else {
                FileUploadCompletedManager.getInstance().init(this);
                FileUploadCompletedManager.getInstance().fileUploadCompleted(object);

            }
        } catch (Exception e) {
            MyLog.e(TAG, "Can not read file: " + e);
        }
    }

    public void startFileDownload(EventBus eventBus, MessageItemChat msgItem, boolean isSecretChat) {

        String[] fullPath = msgItem.getChatFileServerPath().split("/");
        String thumbnail = fullPath[fullPath.length - 1];

        String id;
        String docId;
        String[] splitIds = msgItem.getMessageId().split("-");

        if (msgItem.getMessageId().contains("-g")) {
            id = splitIds[3];
            docId = mCurrentUserId + "-" + splitIds[1] + "-g";
        } else {
            id = splitIds[2];
            docId = mCurrentUserId + "-" + splitIds[1];
        }

        String dataSize = msgItem.getFileSize();
        File dir = new File(MessageFactory.BASE_STORAGE_PATH);
        if (!dir.exists()) {
            dir.mkdir();
        }

        String chatTypePath, publicDirPath = "";
        int msgType = Integer.parseInt(msgItem.getMessageType());
        File fileDir;
        MyLog.d(TAG, "startFileDownload: " + msgType);
        switch (msgType) {
            case MessageFactory.picture:
                chatTypePath = MessageFactory.IMAGE_STORAGE_PATH;
                fileDir = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), chatTypePath);
                publicDirPath = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).getAbsolutePath();
                break;

            case MessageFactory.document:
                chatTypePath = MessageFactory.DOCUMENT_STORAGE_PATH;
                fileDir = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOCUMENTS), chatTypePath);
                publicDirPath = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
                break;

            case MessageFactory.video:
                chatTypePath = MessageFactory.VIDEO_STORAGE_PATH;
                fileDir = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_MOVIES), chatTypePath);
                publicDirPath = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
                break;

            default:
                chatTypePath = MessageFactory.AUDIO_STORAGE_PATH;
                fileDir = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_MUSIC), chatTypePath);
                publicDirPath = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_MUSIC).getAbsolutePath();
                break;
        }

//        fileDir = new File(chatTypePath);
        if (!fileDir.exists()) {
            fileDir.mkdir();
        }
        File publicDir = new File(publicDirPath);
        if (!publicDir.exists()) {
            publicDir.mkdir();
        }

        File chatDir = new File(chatTypePath);
        if (!chatDir.exists()) {
            chatDir.mkdirs();
        }


        String localFileName = MessageFactory.getMessageFileName(msgType, id,
                FileUploadDownloadManager.getFileExtnFromPath(thumbnail));
        String filePath = chatTypePath + localFileName;

        if (thumbnail != null && !thumbnail.equals("")) {

            File file = new File(filePath);
            if (!file.exists()) {
                try {
                    try {
                        file.createNewFile();
                        //  file.delete();
                    } catch (Exception e) {
                        MyLog.d(TAG, "startFileDownload: file path error.. ");
                        try {
                            file = new File(publicDirPath, localFileName);

                            file.createNewFile();
                            filePath = publicDirPath + File.separator + localFileName;
                            MyLog.d(TAG, "startFileDownload: " + filePath);

                        } catch (Exception e1) {
                            MyLog.e(TAG, "startFileDownload 22: ", e1);
                        }
                    }
                    if (FETCH_DOWNLOAD_ENABLED) {
                        FetchDownloadManager.getInstance().startDownloadFile(msgItem.getMessageId(), msgItem.getChatFileServerPath(), filePath, this);
                    } else {
                        //Check it is web or Android
                        SendMessageEvent event = new SendMessageEvent();
                        JSONObject eventObj = new JSONObject();
                        eventObj.put("MsgId", docId + "-" + id);
                        if (isSecretChat) {
                            docId = docId + "-" + MessageFactory.CHAT_TYPE_SECRET;
                        }
                        //Check Doc Id is from or To or Admin'

                        eventObj.put("DocId", docId);
                        eventObj.put("ImageName", thumbnail);
                        eventObj.put("LocalPath", filePath);
                        eventObj.put("LocalFileName", localFileName);
                        eventObj.put("start", 0);
                        eventObj.put("filesize", dataSize);
                        eventObj.put("bytesRead", 0);
                   /* eventObj.put("UploadedSize", 0);
                    eventObj.put("TotalSize", msgItem.getFileSize());*/
                        MyLog.e(TAG, "startFileDownload" + eventObj);
                        event.setEventName(SocketManager.EVENT_FILE_DOWNLOAD);
                        event.setMessageObject(eventObj);
                        eventBus.post(event);

                        //----------------Set Pause File Object Set First time-----------------------

                        String Msg_id = docId + "-" + id;
                        setDownloadResumePauseFileObject(Msg_id, eventObj);
                    }

                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            } else {
                file.delete();
                startFileDownload(eventBus, msgItem, isSecretChat);
            }
        }
    }

    public void VideoDownloadProgress(EventBus eventBus, JSONObject object) {
        if (FETCH_DOWNLOAD_ENABLED) {

        } else {
            FileDownloadManager.getInstance().init(this, mContext);
            FileDownloadManager.getInstance().VideoDownloadProgress(eventBus, object);
        }
    }

    public void startServerRequestFileUpload(EventBus eventBus, JSONObject object) {
        try {
            int err = object.getInt("err");
            String imgName = object.getString("ImageName");
            String fileName = object.getString("filename");
            int msgType = Integer.parseInt(object.getString("type"));
            String requestUserId = object.getString("requestUser");
            int bufferAt = object.getInt("bufferAt");
            int fileEnd = object.getInt("FileEnd");
            String msgId = object.getString("msgId");
            String convId = object.getString("convId");
            String recordId = object.getString("recordId");

            if (requestUserId.equalsIgnoreCase(mCurrentUserId) && fileEnd == 0) {

                String localFileName = MessageFactory.getMessageFileName(msgType, msgId,
                        FileUploadDownloadManager.getFileExtnFromPath(imgName));

                JSONObject sendObj = new JSONObject();
                sendObj.put("from", mCurrentUserId);
                sendObj.put("requestUser", mCurrentUserId);
                sendObj.put("ImageName", imgName);
                sendObj.put("send", 1);
                sendObj.put("type", msgType);
                sendObj.put("msgId", msgId);
                sendObj.put("convId", convId);
                sendObj.put("recordId", recordId);

                if (msgType == MessageFactory.picture) {
                    localFileName = MessageFactory.IMAGE_STORAGE_PATH + localFileName;
                }

                File file = new File(localFileName);

                if (file.exists()) {
                    sendObj.put("fileExits", 1);
                    if (err == 0) {
                        bufferAt += 1;
                    }
                    MyLog.d(TAG, "startServerRequestFileUpload: before compress");
                    if (msgType == MessageFactory.picture) {
                        file = Compressor.getDefault(mContext).compressToFile(file);
                    }
                    MyLog.d(TAG, "startServerRequestFileUpload: after compress");

                    byte[] bytesArray = new byte[(int) file.length()];

                    try {
                        FileInputStream fis = new FileInputStream(file);
                        fis.read(bytesArray); //read file into bytes[]
                        fis.close();
                    } catch (IOException e) {
                        MyLog.e(TAG, "", e);
                    }

                    int originalFileSize = (int) file.length();
                    if (!object.has("size")) {
                        object.put("size", originalFileSize);
                    }
                    int uploadedSize = 0;
                    if (object.has("UploadedSize")) {
                        uploadedSize = object.getInt("UploadedSize");
                    }

                    if (uploadedSize < originalFileSize) {
                        ByteSplit byteSplit = FileUploadUtils.divideArray(bytesArray, object, uploadedSize);
                        byte[] divideArray = byteSplit.bytes;
                        try {
                            object.put("buffer", divideArray);
                            object.put("bufferAt", bufferAt);
                            object.put("UploadedSize", uploadedSize + divideArray.length);
                            sendObj.put("filename", fileName);
                            if (byteSplit.isFinalByte) {
                                object.put("FileEnd", 1);
                            } else {
                                object.put("FileEnd", 0);
                            }

                        } catch (JSONException e) {
                            MyLog.e(TAG, "", e);
                        }
                    }
                } else {
                    sendObj.put("fileExits", 0);
                }

                SendMessageEvent event = new SendMessageEvent();
                event.setEventName(SocketManager.EVENT_PHONE_DOWNLOAD);
                event.setMessageObject(sendObj);
                eventBus.post(event);
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void uploadUserPicPendingFiles(EventBus eventBus) {

        MessageDbController dbController = CoreController.getDBInstance(mContext);

        ArrayList arrayList = dbController.getpendingUserPicfileupload("uploading");

        MyLog.d("Bandwidth", "--------------------------PENDING UPLOAD SIZE-------------- " + " " + " " + arrayList.size());
        Log.d(TAG, "onClick: startFileUpload5 pending files" + arrayList);
        //Remove duplicate value from arraylist
        ArrayList<String> values = new ArrayList<String>();
        HashSet<String> hashSet = new HashSet<String>();
        hashSet.addAll(arrayList);
        arrayList.clear();
        arrayList.addAll(hashSet);

        Log.d(TAG, "onClick: startFileUpload5 pending files" + arrayList);

        for (int i = 0; i < arrayList.size(); i++) {
            String data = String.valueOf(arrayList.get(i));
            try {
                JSONObject object = new JSONObject(data);
                startFileUpload(eventBus, object);
                Log.e(TAG, "uploadUserPicPendingFiles" + object);
            } catch (JSONException e) {
                MyLog.e(TAG, "uploadPendingFiles: ", e);
            }
        }
    }

    public void uploadPendingFiles(EventBus eventBus) {

        MessageDbController dbController = CoreController.getDBInstance(mContext);

        ArrayList arrayList = dbController.getpendingfileupload("uploading");

        MyLog.d("Bandwidth", "--------------------------PENDING UPLOAD SIZE-------------- " + " " + " " + arrayList.size());
        Log.d(TAG, "onClick: startFileUpload5 pending files" + arrayList);
        for (int i = 0; i < arrayList.size(); i++) {

            String data = String.valueOf(arrayList.get(i));
            try {

                JSONObject object = new JSONObject(data);
                MyLog.d(TAG, "onClick: startFileUpload5 pending file obj= " + object);
                startFileUpload(eventBus, object);

            } catch (JSONException e) {
                MyLog.e(TAG, "uploadPendingFiles: ", e);
            }
        }
    }

    public void resumeFileUpload(EventBus eventBus, JSONObject object) {
        Log.d(TAG, "onClick: startFileUpload6 resume files");
        startFileUpload(eventBus, object);
    }

    public void downloadPendingFiles(EventBus eventBus) {

        Map<String, ?> pendingFiles = downloadpauseresumePref.getAll();


        for (String key : pendingFiles.keySet()) {
            Boolean downloadInProgress = false;
            String msgId = null;
            if (key.contains("download_status_")) {
                downloadInProgress = downloadpauseresumePref.getBoolean(key, false);
                msgId = key.replace("download_status_", "");
            }
            if (downloadInProgress != null) {
                if (!downloadInProgress) {
                    try {
                        JSONObject object = getDownloadResumePauseFileObject(msgId);
                        MyLog.e(TAG, "downloadPendingFiles" + object);
                        if (object != null) {
                            if (object.has("LocalPath")) {
                                File file = new File(object.getString("LocalPath"));
                                if (file.exists()) {
                                    if (object.has("id")) {
                                        setDownloadResumePauseFileObject(object.getString("id"), object);
                                        setDownloadResumePauseFileStatus(msgId, false);
                                        VideoDownloadProgress(EventBus.getDefault(), object);
                                        MyLog.e("CheckEventConnection", "Downlaod Pending Files" + "" + object);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        removeOfflineDownloadStatusNotExist(key);
                        MyLog.e(TAG, "", e);
                    }
                }


            }


        }
    }


}
