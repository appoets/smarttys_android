package com.app.Smarttys.core.message;

import android.content.Context;
import android.os.Handler;

import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.model.OfflineRetryEventPojo;
import com.app.Smarttys.core.socket.MessageService;
import com.app.Smarttys.core.socket.SocketManager;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by CAS60 on 6/2/2017.
 */
public class OfflineMessageHandler {

    private static final String TAG = "OfflineMessageHandler";
    private MessageService callBack;
    private Context mContext;
    private String mCurrentUserId;
    private boolean isOfflineInProgress;
    private ArrayList<String> offlineEventNames = new ArrayList<>();

    private int msgIndex;
    private Handler msgHandler = new Handler();
    private Runnable msgRunnable;

    public OfflineMessageHandler(MessageService messageService) {
        this.mContext = messageService;
        this.callBack = messageService;
        mCurrentUserId = SessionManager.getInstance(messageService).getCurrentUserID();
        setOfflineEventNames();
    }


    public boolean isOfflineMessageEvent(String eventName, Object message) {
        try {
            if (eventName != null && eventName.equalsIgnoreCase(SocketManager.EVENT_REPLY_MESSAGE) ||
                    eventName.equalsIgnoreCase(SocketManager.EVENT_MESSAGE) ||
                    eventName.equalsIgnoreCase(SocketManager.EVENT_NEW_FILE_MESSAGE)) {
                return true;
            } else if (eventName.equalsIgnoreCase(SocketManager.EVENT_GROUP)) {
                try {
                    JSONObject object = (JSONObject) message;
                    String actionType = object.getString("groupType");

                    return (actionType.equals(SocketManager.ACTION_EVENT_GROUP_MESSAGE) ||
                            actionType.equals(SocketManager.ACTION_EVENT_GROUP_REPlY_MESSAGE));
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            MyLog.e(TAG, "isOfflineMessageEvent: ", e);
        }
        return false;
    }

    private void setOfflineEventNames() {
        offlineEventNames.add(SocketManager.EVENT_IMAGE_UPLOAD);
//        offlineEventNames.add(SocketManager.EVENT_FILE_UPLOAD);
//        offlineEventNames.add(SocketManager.EVENT_FILE_DOWNLOAD);
//        offlineEventNames.add(SocketManager.EVENT_START_FILE_DOWNLOAD);
        offlineEventNames.add(SocketManager.EVENT_CHAT_LOCK_FROM_WEB);
    }

    public void storeOfflineEventData(String eventName, Object message) {
        if (isOfflineEvent(eventName)) {
            OfflineRetryEventPojo retryEventPojo = new OfflineRetryEventPojo();
            retryEventPojo.setEventName(eventName);
            retryEventPojo.setEventObject(message.toString());

            CoreController.getDBInstance(mContext).updateOfflineEvents(retryEventPojo);
        }
    }

    private boolean isOfflineEvent(String eventName) {
        return (offlineEventNames.contains(eventName));
    }

    public void sendOfflineMessages() {
        if (!isOfflineInProgress) {
            msgIndex = 0;
            performSend();
        }
    }

    private void performSend() {
        isOfflineInProgress = true;

        final ArrayList<OfflineRetryEventPojo> offlineStoredData = CoreController.getDBInstance(
                mContext).getOfflineEvents(mCurrentUserId);

        for (int i = 0; i < offlineStoredData.size(); i++) {
            OfflineRetryEventPojo data = offlineStoredData.get(i);
            callBack.sendOfflineMessage(data.getEventObject(), data.getEventName());
            MyLog.d("OfflineDataLoadTime", data.getEventObject().toString());
        }

        final ArrayList<OfflineRetryEventPojo> offlineMsgData = CoreController.getDBInstance(
                mContext).getSendNewMessage(mCurrentUserId);

        if (offlineMsgData.size() > 0) {
            msgRunnable = new Runnable() {
                @Override
                public void run() {
                    if (offlineMsgData.size() > msgIndex) {
                        OfflineRetryEventPojo pojo = offlineMsgData.get(msgIndex);
                        callBack.sendOfflineMessage(pojo.getEventObject(), pojo.getEventName());
                        msgIndex++;
                        if (msgIndex == offlineMsgData.size()) {
                            isOfflineInProgress = false;
                        } else {
                            msgHandler.postDelayed(this, 750);
                        }
                    } else {
                        isOfflineInProgress = false;
                    }
                }
            };
            msgHandler.postDelayed(msgRunnable, 750);
        } else {
            isOfflineInProgress = false;
        }


       /* for (int i = 0; i < offlineMsgData.size(); i++) {
            OfflineRetryEventPojo data = offlineMsgData.get(i);
            callBack.sendOfflineMessage(data.getEventObject(), data.getEventName());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                Log.e(TAG,"",e);
            }
            Log.d("OfflineDataLoadTime", data.getEventObject().toString());
        }*/
    }

    public void onServiceDestroy() {
        if (msgRunnable != null) {
            msgHandler.removeCallbacks(msgRunnable);
        }
    }

    public void sendOfflineStarredMessages() {
        MessageDbController db = CoreController.getDBInstance(mContext);
        ArrayList<JSONObject> starredList = db.getAllTempStarredMessage(mCurrentUserId);
        for (int i = 0; i < starredList.size(); i++) {
            callBack.sendOfflineMessage(starredList.get(i), SocketManager.EVENT_STAR_MESSAGE);
        }
    }

    public void sendOfflineDeletedMessages() {
        MessageDbController db = CoreController.getDBInstance(mContext);
        ArrayList<JSONObject> deletedList = db.getAllTempDeletedMessage(mCurrentUserId);
        for (int i = 0; i < deletedList.size(); i++) {
            callBack.sendOfflineMessage(deletedList.get(i), SocketManager.EVENT_REMOVE_MESSAGE);
        }
    }
}
