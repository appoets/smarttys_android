package com.app.Smarttys.core.connectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.status.controller.interfaces.MyInternetListener;

/**
 *
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    public static String TAG = NetworkChangeReceiver.class.getSimpleName();
    private static MyInternetListener myInternetListener;

    public static void setListener(MyInternetListener listener) {
        myInternetListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (isNetworkAvailable(context)) {
            MyLog.d(TAG, "Connected to a network");
            if (myInternetListener != null) {
                myInternetListener.networkAvailable(true);
            }
        } else {
            if (myInternetListener != null) {
                myInternetListener.networkAvailable(true);
            }
            MyLog.d(TAG, "Not Connected to a network");
        }
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
