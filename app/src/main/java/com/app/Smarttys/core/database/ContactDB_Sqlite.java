package com.app.Smarttys.core.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.GroupInfoSession;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.TimeStampUtils;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.model.GroupInfoPojo;
import com.app.Smarttys.core.model.MuteStatusPojo;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by user145 on 2/14/2018.
 */
public class ContactDB_Sqlite extends SQLiteOpenHelper {

    public static final String PRIVACY_STATUS_EVERYONE = "0";
    public static final String PRIVACY_STATUS_MY_CONTACTS = "1";
    public static final String PRIVACY_STATUS_NOBODY = "2";
    public static final String PRIVACY_TO_EVERYONE = "everyone";
    public static final String PRIVACY_TO_MY_CONTACTS = "mycontacts";
    public static final String PRIVACY_TO_NOBODY = "nobody";
    public static final String UN_BLOCKED_STATUS = "0";
    public static final String BLOCKED_STATUS = "1";
    public static final String REVISION_COUNT = "revisioncount";
    public final static String FIRSTNAME = "FirstName";
    public final static String AVATARIMAGEURL = "AvatarImageURL";
    public final static String MSISDN = "Msisdn";
    public static final String KEY_SECRET_TIMER_ID = "TimerId";
    public static final String KEY_SECRET_TIMER = "Timer";
    public static final String KEY_SECURITY_TOKEN = "security_token";
    public static final String KEY_SECRET_TIMER_MODE = "TimerMode";
    public static final String KEY_SECRET_TIMER_CREATED_BY = "TimerCreatedBy";
    private final static String DB_Name = "contacts_db";
    private final static int DB_Version = 6;
    private static final String CONTACT_SAVED_STATUS = "1";
    private static final String CONTACT_UNSAVED_STATUS = "0";
    //tablenames
    private final static String TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS = "smartty_CONTACT__USERSDETAILS_BLOCK_UNBLOCK_STATUS";
    private final static String TABLE_USERSDETAILS_MUTESTATUS = "smartty_CONTACT__USERSDETAILS_MUTESTATUS";
    private final static String TABLE_FREQUENT_CONTACTS = "smartty_CONTACT__FREQUENT_CONTACTS";
    private final static String TABLE_FREQUENT_GROUPS = "FREQUENT_GROUPS";
    private final static String TABLE_PHONE_CONTACTS = "smartty_CONTACT__FREQUENT_CONTACTS";
    //table for TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS fields
    private final static String NOINDEVICE = "NoInDevice";
    private final static String TYPE = "Type";
    private final static String ISSELECTED = "IsSelected";
    private final static String COUNTRYCODE = "CountryCode";
    private final static String ID = "_id";
    private final static String USERID = "UserID";
    private final static String STATUS = "status";
    private static final String TAG = "ContactDB_Sqlite";
    private final static String KEY_MY_CONTACT_STATUS = "MyContactStatus";
    private final static String KEY_LAST_SEEN_VISIBILITY = "LastSeenVisibility";
    private final static String KEY_PROFILE_PIC_VISIBILITY = "ProfilePicVisibility";
    private final static String KEY_PROFILE_STATUS_VISIBILITY = "ProfileStatusVisibility";
    //private final static String KEY_USER_DETAILS = "UserDetails";
    private final static String KEY_DP_UPDATED_TIME = "DpUpdatedTime";
    private final static String KEY_CONTACT_SAVED_REVISION = "ContactSavedRevision";
    private final static String KEY_BLOCKED_OPPONENET_DETAILS = "BlockedDetails";
    private final static String KEY_BLOCKED_MINE_DETAILS = "BlockedMineDetails";
    private final static String KEY_SECRET_TIMER_DETAILS = "SecretTimerDetails";
    private final static String KEY_NORMAL_CHAT = "NormalChat";
    private final static String KEY_SECRET_CHAT = "SecretChat";
    private final static String REVISION = "ContactSavedRevision";
    //table for TABLE_USERSDETAILS_MUTESTATUS fields
    private final static String KEY_TO_USERID = "ToUserID";
    private final static String KEY_CONVERSATION_ID = "ConversationID";
    private final static String TO_USERID = "touseruniqueid";
    private final static String CONVS_ID = "convsid";
    private final static String GROUP_ID = "groupId";
    private final static String KEY_DURATION = "Duration";
    private final static String KEY_TIME_STAMP = "TimeStamp";
    private final static String KEY_NOTIFY_STATUS = "NotifyStatus";
    private final static String KEY_EXPIRE_TS = "ExpireTS";
    private final Context context;
    private final String KEY_FREQUENTLY_CONTACTS = "FrequentContacts";


    String CREATE_TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS = "CREATE TABLE " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + "(" + ID
            + " INTEGER PRIMARY KEY ," + USERID + " TEXT UNIQUE,"
            + KEY_MY_CONTACT_STATUS + " TEXT,"
            + KEY_SECURITY_TOKEN + " TEXT,"
            + KEY_LAST_SEEN_VISIBILITY + " TEXT," + KEY_PROFILE_PIC_VISIBILITY + " TEXT," + KEY_PROFILE_STATUS_VISIBILITY + " TEXT,"
            + FIRSTNAME + " TEXT,"
            + STATUS + " TEXT,"
            + AVATARIMAGEURL + " TEXT,"
            + NOINDEVICE + " TEXT,"
            + MSISDN + " TEXT,"
            + TYPE + " TEXT,"
            + ISSELECTED + " TEXT,"
            + COUNTRYCODE + " TEXT,"
            + KEY_DP_UPDATED_TIME + " TEXT,"
            + KEY_CONTACT_SAVED_REVISION + " TEXT," + KEY_BLOCKED_OPPONENET_DETAILS + " TEXT," + KEY_BLOCKED_MINE_DETAILS + " TEXT,"
            + KEY_SECRET_TIMER_DETAILS + " TEXT)";

    String CREATE_TABLE_USERSDETAILS_MUTESTATUS = "CREATE TABLE " + TABLE_USERSDETAILS_MUTESTATUS + "(" + ID
            + " INTEGER PRIMARY KEY ," + USERID + " TEXT," + TO_USERID + " TEXT," + CONVS_ID + " TEXT," + KEY_TO_USERID + " TEXT,"
            + KEY_CONVERSATION_ID + " TEXT)";

    String CREATE_TABLE_FREQUENT_CONTACTS = "CREATE TABLE " + TABLE_FREQUENT_CONTACTS + "(" + ID
            + " INTEGER PRIMARY KEY ," + USERID + " TEXT," + CONVS_ID + " TEXT UNIQUE," + REVISION_COUNT + " INTEGER)";


    String CREATE_TABLE_FREQUENT_GROUPS = "CREATE TABLE " + TABLE_FREQUENT_GROUPS + "(" + ID
            + " INTEGER PRIMARY KEY ," + USERID + " TEXT," + GROUP_ID + " TEXT UNIQUE," + REVISION_COUNT + " INTEGER)";

    private Gson gson;
    private GsonBuilder gsonBuilder;

    private SQLiteDatabase mDatabaseInstance;

    public ContactDB_Sqlite(Context context) {
        super(context, DB_Name, null, DB_Version);
        this.context = context;
        gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    private synchronized SQLiteDatabase getDatabaseInstance() {
        if (mDatabaseInstance == null) {
            mDatabaseInstance = getWritableDatabase();
        }

        if (!mDatabaseInstance.isOpen()) {
            mDatabaseInstance = getWritableDatabase();
        }

        return mDatabaseInstance;
    }

    public synchronized void close() {
        if (mDatabaseInstance != null && mDatabaseInstance.isOpen()) {
            mDatabaseInstance.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS);
        db.execSQL(CREATE_TABLE_USERSDETAILS_MUTESTATUS);
        db.execSQL(CREATE_TABLE_FREQUENT_CONTACTS);
        db.execSQL(CREATE_TABLE_FREQUENT_GROUPS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERSDETAILS_MUTESTATUS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FREQUENT_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FREQUENT_GROUPS);

        onCreate(db);
    }

    public void deleteDatabase() {
        close();
        context.deleteDatabase(DB_Name);
    }

    public void clearDatabase() {
        getDatabaseInstance().delete(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, null, null);
        getDatabaseInstance().delete(TABLE_USERSDETAILS_MUTESTATUS, null, null);
        getDatabaseInstance().delete(TABLE_FREQUENT_CONTACTS, null, null);
        getDatabaseInstance().delete(TABLE_FREQUENT_GROUPS, null, null);
    }

    // update opponent user contact details //
    public void updateUserDetails(final String userId, SmarttyContactModel model) {
        String selectQuery = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                + userId + "'";

        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                updateOpponenet_UserDetails(userId, model);

            } else {
                selectCur.close();
                insertOpponenet_UserDetails(userId, model);
            }
        }
    }

    // insert opponent user contact details
    private void insertOpponenet_UserDetails(String userId, SmarttyContactModel model) {
        MyLog.d("OpponenetDetaiInsert", userId);
        try {
            getDatabaseInstance().insert(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, null, getUserData(model, userId));
        } catch (Exception e) {
            Log.e(TAG, "insertOpponenet_UserDetails: ", e);
        }
    }

    // update opponent user contact details
    private void updateOpponenet_UserDetails(String userId, SmarttyContactModel model) {
        try {
            MyLog.d("OpponenetDetailupdate", model.getFirstName() + "  ---- " + model.get_id());
            if (!AppUtils.isEmpty(userId)) {
                getDatabaseInstance().update(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, getUserData(model, model.get_id()), USERID + "='" + userId + "'", null);
            }
        } catch (Exception e) {
            Log.e(TAG, "updateOpponenet_UserDetails: ", e);
        }
    }

    private ContentValues getUserData(SmarttyContactModel model, String userId) {
        //Check typoe and number in device null dont pass it

        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(FIRSTNAME, model.getFirstName());
        values.put(STATUS, model.getStatus());
        values.put(AVATARIMAGEURL, model.getAvatarImageUrl());
        if (!AppUtils.isEmpty(model.getNumberInDevice())) {
            values.put(NOINDEVICE, model.getNumberInDevice());
        } else {
            values.put(NOINDEVICE, "");

        }
        values.put(MSISDN, model.getMsisdn());

        if (!AppUtils.isEmpty(model.getType())) {
            values.put(TYPE, model.getType());

        } else {
            values.put(TYPE, "");

        }
        values.put(ISSELECTED, "" + model.isSelected());
        values.put(COUNTRYCODE, model.getCountryCode());
        return values;
    }

    private SmarttyContactModel getContactModel(Cursor cursor) {
        SmarttyContactModel smarttyContactModel = new SmarttyContactModel();
        String userIdInDb = cursor.getString(cursor.getColumnIndex(USERID));
        if (userIdInDb != null) {

            smarttyContactModel.set_id(cursor.getString(cursor.getColumnIndex(USERID)));
            smarttyContactModel.setFirstName(cursor.getString(cursor.getColumnIndex(FIRSTNAME)));
            smarttyContactModel.setStatus(cursor.getString(cursor.getColumnIndex(STATUS)));
            smarttyContactModel.setAvatarImageUrl(cursor.getString(cursor.getColumnIndex(AVATARIMAGEURL)));
            smarttyContactModel.setNumberInDevice(cursor.getString(cursor.getColumnIndex(NOINDEVICE)));
            smarttyContactModel.setMsisdn(cursor.getString(cursor.getColumnIndex(MSISDN)));
            if (cursor.getString(cursor.getColumnIndex(TYPE)) != null) {
                smarttyContactModel.setType(cursor.getString(cursor.getColumnIndex(TYPE)));
            }
            if (cursor.getString(cursor.getColumnIndex(ISSELECTED)) != null) {
                if (cursor.getString(cursor.getColumnIndex(ISSELECTED)).equals("true")) {
                    smarttyContactModel.setSelected(true);
                } else {
                    smarttyContactModel.setSelected(false);
                }
            }
            smarttyContactModel.setCountryCode(cursor.getString(cursor.getColumnIndex(COUNTRYCODE)));

        }
        return smarttyContactModel;
    }

    // get opponent user contact details
    public SmarttyContactModel getUserOpponenetDetails(final String userId) {

        String query = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                + userId + "'";

        SmarttyContactModel smarttyContactModel = new SmarttyContactModel();

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                try {
                    MyLog.d(TAG, "getUserOpponenetDetails: performance");
                    smarttyContactModel = getContactModel(cursor);

                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            }
            cursor.close();
        }

        return smarttyContactModel;
    }


    public String statusOfNumber(final String phoneNumber, final String name, final long savedRevision, final boolean isDeleted) {

        String status = "new";

        String query = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS;


        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                try {


                    String numberInDevice = cursor.getString(cursor.getColumnIndex(MSISDN));
                    if (numberInDevice != null) {
                        String nameOfUser = cursor.getString(cursor.getColumnIndex(FIRSTNAME));
                        long revisionNumber = 0;
                        String revsionvalue = cursor.getString(cursor.getColumnIndex(REVISION));
                        if (revsionvalue != null && !revsionvalue.isEmpty()) {
                            revisionNumber = Long.parseLong(revsionvalue);

                        }
                        if (revisionNumber >= savedRevision) {
                            /*if (numberInDevice.contains(phoneNumber) && nameOfUser.equals(name)) {
                                status = "deleted";
                                deleteNumber(numberInDevice,savedRevision);
                                break;
                            } else*/
                            if (numberInDevice.contains(phoneNumber) && !nameOfUser.equals(name)) {
                                status = "edited";
                                updateName(numberInDevice, name, savedRevision);
                                break;

                            }
                        }


                    }

                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            }
            cursor.close();
        }

        if (isDeleted)
            status = "deleted";
        return status;
    }


    public void updateName(final String phoneNumber, final String name, final long savedRevision) {

        String query = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS;

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                try {

                    String userId = cursor.getString(cursor.getColumnIndex(USERID));


                    String numberInDevice = cursor.getString(cursor.getColumnIndex(MSISDN));
                    long revisionNumber = 0;
                    String revsionvalue = cursor.getString(cursor.getColumnIndex(REVISION));
                    if (revsionvalue != null && !revsionvalue.isEmpty()) {
                        revisionNumber = Long.parseLong(revsionvalue);
                    }

                    if (revisionNumber >= savedRevision) {
                        if (numberInDevice.contains(phoneNumber)) {
                            ContentValues values = new ContentValues();
                            values.put(USERID, userId);
                            values.put(FIRSTNAME, name);
                            getDatabaseInstance().update(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, values, USERID + "='" + userId + "'", null);
                            break;
                        }

                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            }
            cursor.close();
        }

    }

    // update opponent user saved status
    public void updateSavedRevision(final String userId, long revision) {
        String selectQuery = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                + userId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        //System.out.print(".....Ramtesting...updateSavedRevision." + String.valueOf(selectCur.getCount()));

        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                updateOpponenet_UserDetails_savedRevision(userId, revision);
            } else {
                selectCur.close();
                insertOpponenet_UserDetails_savedRevision(userId, revision);
            }
        }
    }

    private void insertOpponenet_UserDetails_savedRevision(String userId, long revision) {
        try {
            ContentValues values = new ContentValues();
            values.put(USERID, userId);
            values.put(REVISION, revision);

            getDatabaseInstance().insert(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, null, values);
        } catch (Exception e) {
            Log.e(TAG, "insertOpponenet_UserDetails_savedRevision: ", e);
        }
    }

    private void updateOpponenet_UserDetails_savedRevision(String userId, long revision) {
        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(REVISION, revision);

        getDatabaseInstance().update(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, values, USERID + "='" + userId + "'", null);
    }

    // get opponent user saved status
    public long getOpponenet_UserDetails_savedRevision(final String userId) {
        long value = 0;
        String query = "SELECT " + REVISION + " FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                + userId + "'";

        Cursor cursor = getDatabaseInstance().rawQuery(query, null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                String revsionvalue = cursor.getString(cursor.getColumnIndex(REVISION));
                if (revsionvalue == null) {
                    value = 0;
                } else {
                    value = Long.parseLong(revsionvalue);
                }

            }
            cursor.close();
        }

        return value;
    }

    // update current user in opponent user's contacts list status
    public void updateMyContactStatus(final String userId, String status) {
        String selectQuery = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                + userId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                update_MyContactStatus(userId, status);
            } else {
                selectCur.close();
                insert_MyContactStatus(userId, status);
            }
        }
    }


    public void updateSecurityToken_(final String userId, String securityToken) {
        String selectQuery = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                + userId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                updateSecurityToken(userId, securityToken);
            } else {
                selectCur.close();
                insertSecurityToken(userId, securityToken);
            }
        }
    }

    private void insert_MyContactStatus(String userId, String status) {

        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(KEY_MY_CONTACT_STATUS, status);

        getDatabaseInstance().insertWithOnConflict(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    private void update_MyContactStatus(String userId, String status) {
        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(KEY_MY_CONTACT_STATUS, status);

        getDatabaseInstance().update(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, values, USERID + "='" + userId + "'", null);

    }


    private void insertSecurityToken(String userId, String securityToken) {

        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(KEY_SECURITY_TOKEN, securityToken);

        getDatabaseInstance().insert(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, null, values);
    }

    private void updateSecurityToken(String userId, String securityToken) {
        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(KEY_SECURITY_TOKEN, securityToken);

        getDatabaseInstance().update(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, values, USERID + "='" + userId + "'", null);

    }

    // get current user in opponent user's contacts list status
    public String getMyContactStatus(String userId) {
        String result = getSingleData(userId, KEY_MY_CONTACT_STATUS);
        if (result != null && !result.isEmpty())
            return result;

        return CONTACT_UNSAVED_STATUS;
    }


    public String getSecurityToken(String userId) {
        String result = getSingleData(userId, KEY_SECURITY_TOKEN);
        if (result != null && !result.isEmpty())
            return result;
        return "";
    }


    public void updateLastSeenVisibility(final String userId, String visibleTo) {
        String selectQuery = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                + userId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);
        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                update_LastSeenVisibility(userId, visibleTo);
            } else {
                selectCur.close();
                insert_LastSeenVisibility(userId, visibleTo);
            }
        }
    }

    private void insert_LastSeenVisibility(String userId, String visibleTo) {
        try {
            ContentValues values = new ContentValues();
            values.put(USERID, userId);
            values.put(KEY_LAST_SEEN_VISIBILITY, visibleTo);

            getDatabaseInstance().insert(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, null, values);
        } catch (Exception e) {
            Log.e(TAG, "insert_LastSeenVisibility: ", e);
        }
    }

    private void update_LastSeenVisibility(String userId, String visibleTo) {
        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(KEY_LAST_SEEN_VISIBILITY, visibleTo);

        getDatabaseInstance().update(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, values, USERID + "='" + userId + "'", null);

    }

    // get opponent user's last seen visibility to others
    public String getLastSeenVisibility(String userId) {

        String result = getSingleData(userId, KEY_LAST_SEEN_VISIBILITY);
        if (result != null && !result.isEmpty())
            return result;
        return PRIVACY_STATUS_EVERYONE;
    }

    // update opponent user's profile picture visibility to others
    public void updateProfilePicVisibility(final String userId, String visibleTo) {
        String selectQuery = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                + userId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                update_ProfilePicVisibility(userId, visibleTo);
            } else {
                selectCur.close();
                insert_ProfilePicVisibility(userId, visibleTo);
            }
        }
    }

    private void insert_ProfilePicVisibility(String userId, String visibleTo) {
        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(KEY_PROFILE_PIC_VISIBILITY, visibleTo);

        getDatabaseInstance().insert(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, null, values);
    }

    private void update_ProfilePicVisibility(String userId, String visibleTo) {
        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(KEY_PROFILE_PIC_VISIBILITY, visibleTo);

        getDatabaseInstance().update(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, values, USERID + "='" + userId + "'", null);

    }

    // get opponent user's profile picture visibility to others
    public String getProfilePicVisibility(String userId) {
        String result = getSingleData(userId, KEY_PROFILE_PIC_VISIBILITY);
        if (result != null && !result.isEmpty())
            return result;
        return PRIVACY_STATUS_EVERYONE;
    }

    // update opponent user's profile status visibility to others
    public void updateProfileStatusVisibility(final String userId, String visibleTo) {

        String selectQuery = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                + userId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                update_ProfileStatusVisibility(userId, visibleTo);
            } else {
                selectCur.close();
                insert_ProfileStatusVisibility(userId, visibleTo);
            }
        }
    }

    private void insert_ProfileStatusVisibility(String userId, String visibleTo) {
        try {
            ContentValues values = new ContentValues();
            values.put(USERID, userId);
            values.put(KEY_PROFILE_STATUS_VISIBILITY, visibleTo);

            getDatabaseInstance().insert(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, null, values);
        } catch (Exception e) {
            Log.e(TAG, "insert_ProfileStatusVisibility: ", e);
        }
    }

    private void update_ProfileStatusVisibility(String userId, String visibleTo) {
        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(KEY_PROFILE_STATUS_VISIBILITY, visibleTo);
        getDatabaseInstance().update(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, values, USERID + "='" + userId + "'", null);
    }

    // get opponent user's profile status visibility to others
    public String getProfileStatusVisibility(String userId) {
        String result = getSingleData(userId, KEY_PROFILE_STATUS_VISIBILITY);
        if (result != null && !result.isEmpty())
            return result;
        return PRIVACY_STATUS_EVERYONE;
    }

    //have to work form here
    public void updateBlockedStatus(final String userId, String status, boolean isSecretChat) {

        String selectQuery = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                + userId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                update_User_block_unblock_status(userId, status, isSecretChat);
            } else {
                selectCur.close();
                insert_User_block_unblock_status(userId, status, isSecretChat);
            }
        }
    }

    private void update_User_block_unblock_status(String userId, String status, boolean isSecretChat) {
        final JSONObject blockObj = new JSONObject();
        try {

            if (isSecretChat) {
                blockObj.put(KEY_SECRET_CHAT, status);
            } else {
                blockObj.put(KEY_NORMAL_CHAT, status);
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(KEY_BLOCKED_OPPONENET_DETAILS, blockObj.toString());

        getDatabaseInstance().update(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, values, USERID + "='" + userId + "'", null);
    }

    private void insert_User_block_unblock_status(String userId, String status, boolean isSecretChat) {

        final JSONObject blockObj = new JSONObject();
        try {
            if (isSecretChat) {
                blockObj.put(KEY_SECRET_CHAT, status);
            } else {
                blockObj.put(KEY_NORMAL_CHAT, status);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(KEY_BLOCKED_OPPONENET_DETAILS, blockObj.toString());

        getDatabaseInstance().insert(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, null, values);
    }

    // get opponent user contact details
    public String getBlockedStatus(String userId, boolean isSecretChat) {
        try {
            String result = getSingleData(userId, KEY_BLOCKED_OPPONENET_DETAILS);
            if (result != null && !result.isEmpty()) {
                JSONObject blockObj = new JSONObject(result);
                if (isSecretChat && blockObj.has(KEY_SECRET_CHAT)) {
                    return blockObj.getString(KEY_SECRET_CHAT);
                } else if (!isSecretChat && blockObj.has(KEY_NORMAL_CHAT)) {
                    return blockObj.getString(KEY_NORMAL_CHAT);
                }
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        return UN_BLOCKED_STATUS;
    }


    // update current user is blocked status by opponent user
    public void updateBlockedMineStatus(final String userId, String status, boolean isSecretChat) {
        String selectQuery = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                + userId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                update_BlockedMineStatus(userId, status, isSecretChat);
            } else {
                selectCur.close();
                insert_BlockedMineStatus(userId, status, isSecretChat);

            }
        }
    }

    private void insert_BlockedMineStatus(String userId, String status, boolean isSecretChat) {
        final JSONObject blockObj = new JSONObject();
        try {
            if (isSecretChat) {
                blockObj.put(KEY_SECRET_CHAT, status);
            } else {
                blockObj.put(KEY_NORMAL_CHAT, status);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(KEY_BLOCKED_MINE_DETAILS, blockObj.toString());

        getDatabaseInstance().insert(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, null, values);
    }

    private void update_BlockedMineStatus(String userId, String status, boolean isSecretChat) {
        final JSONObject blockObj = new JSONObject();
        try {

            if (isSecretChat) {
                blockObj.put(KEY_SECRET_CHAT, status);
            } else {
                blockObj.put(KEY_NORMAL_CHAT, status);
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(KEY_BLOCKED_MINE_DETAILS, blockObj.toString());

        getDatabaseInstance().update(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, values, USERID + "='" + userId + "'", null);
    }

    // get current user blocked status
    public String getBlockedMineStatus(String userId, boolean isSecretChat) {
        try {
            String result = getSingleData(userId, KEY_BLOCKED_MINE_DETAILS);
            if (result != null && !result.isEmpty()) {
                JSONObject blockObj = new JSONObject(result);
                if (isSecretChat && blockObj.has(KEY_SECRET_CHAT)) {
                    return blockObj.getString(KEY_SECRET_CHAT);
                } else if (!isSecretChat && blockObj.has(KEY_NORMAL_CHAT)) {
                    return blockObj.getString(KEY_NORMAL_CHAT);
                }
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        return UN_BLOCKED_STATUS;
    }

    // update opponent user's profile image updated time
    public void updateDpUpdatedTime(final String userId, String timeStamp) {
        String selectQuery = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                + userId + "'";
        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);
        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                update_DpUpdatedTime(userId, timeStamp);
            } else {
                selectCur.close();
                insert_DpUpdatedTime(userId, timeStamp);
            }
        }
    }

    private void insert_DpUpdatedTime(String userId, String timeStamp) {
        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(KEY_DP_UPDATED_TIME, timeStamp);

        getDatabaseInstance().insert(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, null, values);
    }

    private void update_DpUpdatedTime(String userId, String timeStamp) {
        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(KEY_DP_UPDATED_TIME, timeStamp);

        getDatabaseInstance().update(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, values, USERID + "='" + userId + "'", null);
    }

    // get opponent user's profile image updated time
    public String getDpUpdatedTime(String userId) {
        String result = getSingleData(userId, KEY_DP_UPDATED_TIME);
        if (result != null && !result.isEmpty())
            return result;
        return "0";
    }

    // update secret message expiration time
    public void updateSecretMessageTimer(final String userId, String timer, String createdBy, String msgId) {
        Cursor selectCur = null;
        try {
            String selectQuery = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                    + userId + "'";
            selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

            if (selectCur != null) {
                if (selectCur.getCount() > 0) {
                    update_SecretMessageTimer(userId, timer, createdBy, msgId);
                } else {
                    insert_SecretMessageTimer(userId, timer, createdBy, msgId);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "updateSecretMessageTimer: ", e);
        } finally {
            if (selectCur != null) {
                selectCur.close();
            }

        }
    }

    private void update_SecretMessageTimer(String userId, String timer, String createdBy, String msgId) {

        try {
            boolean needUpdate = true;
            String result = getSingleData(userId, KEY_SECRET_TIMER_DETAILS);
            if (result != null && !result.isEmpty()) {
                try {
                    JSONObject timerObj = new JSONObject(result);
                    String prevCreatedBy = timerObj.getString(KEY_SECRET_TIMER_CREATED_BY);
                    String prevTimer = timerObj.getString(KEY_SECRET_TIMER);

                    if (prevCreatedBy.equalsIgnoreCase(createdBy) && prevTimer.equalsIgnoreCase(timer)) {
                        needUpdate = false;
                    }
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }

            if (needUpdate) {

                try {
                    JSONObject timerObj = new JSONObject();
                    timerObj.put(KEY_SECRET_TIMER_CREATED_BY, createdBy);
                    timerObj.put(KEY_SECRET_TIMER, timer);
                    timerObj.put(KEY_SECRET_TIMER_ID, msgId);

                    ContentValues values = new ContentValues();
                    values.put(USERID, userId);
                    values.put(KEY_SECRET_TIMER_DETAILS, timerObj.toString());

                    getDatabaseInstance().update(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, values, USERID + "='" + userId + "'", null);

                } catch (Exception e) {
                    MyLog.d("ContactsDBError", e.getMessage() + "");
                    MyLog.e(TAG, "", e);
                }
            }
        } catch (Exception e) {
        }
    }

    private void insert_SecretMessageTimer(String userId, String timer, String createdBy, String msgId) {
        try {
            JSONObject timerObj = new JSONObject();
            timerObj.put(KEY_SECRET_TIMER_CREATED_BY, createdBy);
            timerObj.put(KEY_SECRET_TIMER, timer);
            timerObj.put(KEY_SECRET_TIMER_ID, msgId);

            ContentValues values = new ContentValues();
            values.put(USERID, userId);
            values.put(KEY_SECRET_TIMER_DETAILS, timerObj.toString());

            getDatabaseInstance().insert(TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS, null, values);

        } catch (Exception e) {

        }
    }

    // get secret message expiration time
    public String getSecretMessageTimer(String userId) {

        String result = getSingleData(userId, KEY_SECRET_TIMER_DETAILS);
        if (result != null && !result.isEmpty()) {
            JSONObject timerObj = null;
            try {
                timerObj = new JSONObject(result);
                return timerObj.toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void updateMuteStatus(String currentUserId, String toUserId, String convId, int status, String duration, String notifyStatus, boolean isSecretChat) {

        String selectQuery = "SELECT * FROM " + TABLE_USERSDETAILS_MUTESTATUS + " WHERE " +
                TO_USERID + "='" + toUserId + "' AND " + CONVS_ID + "='" + convId + "'";

        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                update_MuteStatus_UserID_Conv_ID(currentUserId, toUserId, convId, status, duration, notifyStatus, isSecretChat);

            } else {
                selectCur.close();
                insert_MuteStatus_UserID_Conv_ID(currentUserId, toUserId, convId, status, duration, notifyStatus, isSecretChat);

            }
        }

    }

    private void update_MuteStatus_UserID_Conv_ID(String currentUserId, String toUserId, String convId,
                                                  int status, String duration, String notifyStatus, boolean isSecretChat) {
        final JSONObject valObj = new JSONObject();
        final JSONObject muteObj = new JSONObject();
        try {
            Date currentDate = Calendar.getInstance().getTime();
            muteObj.put(KEY_DURATION, duration);
            muteObj.put(KEY_TIME_STAMP, currentDate.getTime());

            if (status == 1) {
                if (duration.equalsIgnoreCase("8 Hours")) {
                    Date date = TimeStampUtils.addHour(currentDate, 8);
                    muteObj.put(KEY_EXPIRE_TS, date.getTime());
                } else if (duration.equalsIgnoreCase("1 Week")) {
                    Date date = TimeStampUtils.addDay(currentDate, 7);
                    muteObj.put(KEY_EXPIRE_TS, date.getTime());
                } else if (duration.equalsIgnoreCase("1 Year")) {
                    Date date = TimeStampUtils.addYear(currentDate, 1);
                    muteObj.put(KEY_EXPIRE_TS, date.getTime());
                }
            }

            if (notifyStatus == null || notifyStatus.equals("")) {
                muteObj.put(KEY_NOTIFY_STATUS, "0");
            } else {
                muteObj.put(KEY_NOTIFY_STATUS, notifyStatus);
            }

            if (isSecretChat) {
                muteObj.put(KEY_SECRET_CHAT, status);

                if (toUserId != null && !toUserId.equals("")) {
                    valObj.put(KEY_SECRET_CHAT, muteObj);
                }
            } else {
                muteObj.put(KEY_NORMAL_CHAT, status);

                if (toUserId != null && !toUserId.equals("")) {
                    valObj.put(KEY_NORMAL_CHAT, muteObj);
                }
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        JSONObject json_valobj = new JSONObject();
        JSONObject json_muteObj = new JSONObject();
        try {
            if (toUserId != null && !toUserId.trim().equals("")) {
                json_valobj.put(toUserId, valObj);
            }
            if (convId != null && !convId.trim().equals("")) {
                json_muteObj.put(convId, muteObj);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        ContentValues values = new ContentValues();
        values.put(USERID, currentUserId);
        values.put(TO_USERID, toUserId);
        values.put(CONVS_ID, convId);
        values.put(KEY_TO_USERID, json_valobj.toString());
        values.put(KEY_CONVERSATION_ID, json_muteObj.toString());

        getDatabaseInstance().update(TABLE_USERSDETAILS_MUTESTATUS, values, TO_USERID + "='" + toUserId + "'" + " AND " + CONVS_ID + "='" + convId + "'", null);
    }

    private void insert_MuteStatus_UserID_Conv_ID(String currentUserId, String toUserId, String convId,
                                                  int status, String duration, String notifyStatus, boolean isSecretChat) {
        final JSONObject valObj = new JSONObject();
        final JSONObject muteObj = new JSONObject();
        try {
            Date currentDate = Calendar.getInstance().getTime();
            muteObj.put(KEY_DURATION, duration);
            muteObj.put(KEY_TIME_STAMP, currentDate.getTime());

            if (status == 1) {
                if (duration.equalsIgnoreCase("8 Hours")) {
                    Date date = TimeStampUtils.addHour(currentDate, 8);
                    muteObj.put(KEY_EXPIRE_TS, date.getTime());
                } else if (duration.equalsIgnoreCase("1 Week")) {
                    Date date = TimeStampUtils.addDay(currentDate, 7);
                    muteObj.put(KEY_EXPIRE_TS, date.getTime());
                } else if (duration.equalsIgnoreCase("1 Year")) {
                    Date date = TimeStampUtils.addYear(currentDate, 1);
                    muteObj.put(KEY_EXPIRE_TS, date.getTime());
                }
            }

            if (notifyStatus == null || notifyStatus.equals("")) {
                muteObj.put(KEY_NOTIFY_STATUS, "0");
            } else {
                muteObj.put(KEY_NOTIFY_STATUS, notifyStatus);
            }

            if (isSecretChat) {
                muteObj.put(KEY_SECRET_CHAT, status);

                if (toUserId != null && !toUserId.equals("")) {
                    valObj.put(KEY_SECRET_CHAT, muteObj);
                }
            } else {
                muteObj.put(KEY_NORMAL_CHAT, status);

                if (toUserId != null && !toUserId.equals("")) {
                    valObj.put(KEY_NORMAL_CHAT, muteObj);
                }
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        JSONObject json_valobj = new JSONObject();
        JSONObject json_muteObj = new JSONObject();
        try {
            if (toUserId != null && !toUserId.trim().equals("")) {
                json_valobj.put(toUserId, valObj);
            }
            if (convId != null && !convId.trim().equals("")) {
                json_muteObj.put(convId, muteObj);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        ContentValues values = new ContentValues();
        values.put(USERID, currentUserId);
        values.put(TO_USERID, toUserId);
        values.put(CONVS_ID, convId);
        values.put(KEY_TO_USERID, json_valobj.toString());
        values.put(KEY_CONVERSATION_ID, json_muteObj.toString());

        getDatabaseInstance().insert(TABLE_USERSDETAILS_MUTESTATUS, null, values);
    }

    // get opponent user contact details
    public MuteStatusPojo getMuteStatus(String currentUserId, String toUserId, String convId, boolean isSecretChat) {

        MuteStatusPojo muteData_top = null;
        JSONObject toUserIdObj = new JSONObject();
        JSONObject convIdObj = new JSONObject();
        String selectQuery;

        if (toUserId != null) {
            selectQuery = "SELECT * FROM " + TABLE_USERSDETAILS_MUTESTATUS + " WHERE " +
                    TO_USERID + "='" + toUserId + "' AND " + CONVS_ID + "='" + convId + "'";
        } else {
            selectQuery = "SELECT * FROM " + TABLE_USERSDETAILS_MUTESTATUS + " WHERE " +
                    CONVS_ID + "='" + convId + "'";
        }

        Cursor cursor = getDatabaseInstance().rawQuery(selectQuery, null);

        if (cursor != null) {
            try {
                while (cursor.moveToNext()) {

                    String convsJson = cursor.getString(cursor.getColumnIndex(KEY_CONVERSATION_ID));
                    if (toUserId != null && !toUserId.trim().equals("")) {
                        String touserJson = cursor.getString(cursor.getColumnIndex(KEY_TO_USERID));

                        try {
                            JSONObject tempObj = new JSONObject(touserJson);
                            if (isSecretChat) {
                                toUserIdObj = tempObj.getJSONObject(toUserId).getJSONObject(KEY_SECRET_CHAT);
                            } else {
                                toUserIdObj = tempObj.getJSONObject(toUserId).getJSONObject(KEY_NORMAL_CHAT);
                            }
                        } catch (JSONException e) {
                            MyLog.e(TAG, "", e);
                        }
                    }

                    try {
                        convIdObj = new JSONObject(convsJson);
                        convIdObj = convIdObj.getJSONObject(convId);

                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                }
            } catch (Exception e) {
                MyLog.e(TAG, "getMuteStatus: ", e);
            }
            cursor.close();
        } else {
            return null;
        }

        // check last updated mute status either by userid or convid
        long toUserIdTS = 0, convIdTS = 0;
        if (toUserIdObj.has(KEY_TIME_STAMP)) {
            try {
                toUserIdTS = toUserIdObj.getLong(KEY_TIME_STAMP);
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        }

        if (convIdObj.has(KEY_TIME_STAMP)) {
            try {
                convIdTS = convIdObj.getLong(KEY_TIME_STAMP);
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        }

        MuteStatusPojo muteData = null;
        JSONObject muteObj;
        if (toUserIdTS >= convIdTS) {
            muteObj = toUserIdObj;
        } else {
            muteObj = convIdObj;
        }

        try {
            Long ts = 0L;
            if (muteObj.has(KEY_TIME_STAMP))
                ts = muteObj.getLong(KEY_TIME_STAMP);
            String duration = "";
            if (muteObj.has(KEY_DURATION))
                duration = muteObj.getString(KEY_DURATION);
            String notifyStatus = "";
            if (muteObj.has(KEY_NOTIFY_STATUS))
                notifyStatus = muteObj.getString(KEY_NOTIFY_STATUS);

            Long expireTS = 0L;
            if (muteObj.has(KEY_EXPIRE_TS)) {
                expireTS = muteObj.getLong(KEY_EXPIRE_TS);
            }

            String muteStatus = "";
            if (isSecretChat) {
                if (muteObj.has(KEY_SECRET_CHAT))
                    muteStatus = muteObj.getString(KEY_SECRET_CHAT);
            } else {
                if (muteObj.has(KEY_NORMAL_CHAT))
                    muteStatus = muteObj.getString(KEY_NORMAL_CHAT);
            }

            muteData = new MuteStatusPojo();
            muteData.setTs(ts);
            muteData.setDuration(duration);
            muteData.setNotifyStatus(notifyStatus);
            muteData.setMuteStatus(muteStatus);
            muteData.setExpireTs(expireTS);

            muteData_top = muteData;
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        return muteData_top;
    }


    public String getSingleData(String userId, String key) {
        String result = "";
        Cursor cursor = null;
        try {
            String query = "SELECT " + key + " FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                    + userId + "'";
            cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor.moveToNext()) {
                result = cursor.getString(cursor.getColumnIndex(key));
            }
        } catch (Exception e) {
            MyLog.e(TAG, "getSingleData: ", e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return result;
    }

    public String getNameByMobile(String msisdn) {
        Cursor cursor = null;
        String result = "";
        try {
            String query = "SELECT " + FIRSTNAME + " FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + MSISDN + "='"
                    + msisdn + "'";
            cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor.moveToNext()) {
                result = cursor.getString(cursor.getColumnIndex(FIRSTNAME));
            }
        } catch (Exception e) {
            MyLog.e(TAG, "getSingleData: ", e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return result;
    }

    public String getNameByUserId(String userId) {
        Cursor cursor = null;
        String result = "";
        try {
            String query = "SELECT " + FIRSTNAME + " FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                    + userId + "'";
            cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor.moveToNext()) {
                result = cursor.getString(cursor.getColumnIndex(FIRSTNAME));
            }
        } catch (Exception e) {
            MyLog.e(TAG, "getSingleData: ", e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return result;
    }


    public boolean isUserAvailableInDB(String userId) {
        Cursor cursor = null;
        try {
            String query = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS + " WHERE " + USERID + "='"
                    + userId + "'";
            cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            } else {
                cursor.close();
            }
        } catch (Exception e) {
            if (cursor != null)
                cursor.close();
            MyLog.e(TAG, "getSingleData: ", e);
        }
        return false;
    }

    //have to work from here for sqlite DB
    public ArrayList<SmarttyContactModel> getSavedSmarttyContacts() {
        ArrayList<SmarttyContactModel> contactsList = new ArrayList<>();
        long savedRevision = SessionManager.getInstance(context).getContactSavedRevision();
        Cursor cursor = getDatabaseInstance().rawQuery("select * from smartty_CONTACT__USERSDETAILS_BLOCK_UNBLOCK_STATUS", null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                String revsiosn = cursor.getString(cursor.getColumnIndex(KEY_CONTACT_SAVED_REVISION));
                if (revsiosn != null) {
                    long contctSaveRevision = Long.parseLong(cursor.getString(cursor.getColumnIndex(KEY_CONTACT_SAVED_REVISION)));
                    if (contctSaveRevision >= savedRevision) {
                        try {
                            SmarttyContactModel smarttyContactModel = getContactModel(cursor);
                            smarttyContactModel.setSelected(false);
                            if (smarttyContactModel.getFirstName() != null && !smarttyContactModel.getFirstName().isEmpty())
                                contactsList.add(smarttyContactModel);
                        } catch (Exception e) {
                            MyLog.e(TAG, "", e);
                        }
                    }

                }
                cursor.moveToNext();
            }
        }
        return contactsList;
    }

    public ArrayList<SmarttyContactModel> getAllSmarttyContacts() {
        ArrayList<SmarttyContactModel> contactsList = new ArrayList<>();
        try {

            String query = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS;

            Cursor cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {

                    try {
                        SmarttyContactModel smarttyContactModel = getContactModel(cursor);
                        smarttyContactModel.setSelected(false);

                        contactsList.add(smarttyContactModel);
                    } catch (Exception e) {
                        MyLog.e(TAG, "", e);
                    }
                    cursor.moveToNext();
                }
            }
        } catch (Exception e) {
        }
        return contactsList;
    }

    public void updateFrequentContact(String userId, String convId, String timeStamp) {
        String selectQuery = "SELECT * FROM " + TABLE_FREQUENT_CONTACTS + " WHERE " +
                CONVS_ID + "='" + convId + "'";

        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                update_FrequentContact(userId, convId, timeStamp);
            } else {
                selectCur.close();
                insert_FrequentContact(userId, convId, timeStamp);
            }
        }
    }

    public void updateFrequentGroups(String userId, String groupId, String timeStamp) {
        String selectQuery = "SELECT * FROM " + TABLE_FREQUENT_GROUPS + " WHERE " +
                GROUP_ID + "='" + groupId + "'";

        Cursor selectCur = getDatabaseInstance().rawQuery(selectQuery, null);

        if (selectCur != null) {
            if (selectCur.getCount() > 0) {
                selectCur.close();
                update_FrequentGroup(userId, groupId, timeStamp);
            } else {
                selectCur.close();
                insert_FrequentGroups(userId, groupId, timeStamp);
            }
        }
    }

    private void insert_FrequentContact(String userId, String convId, String timeStamp) {
        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(CONVS_ID, convId);
        values.put(REVISION_COUNT, 1);
        getDatabaseInstance().insert(TABLE_FREQUENT_CONTACTS, null, values);
    }

    private void insert_FrequentGroups(String userId, String groupId, String timeStamp) {
        ContentValues values = new ContentValues();
        values.put(USERID, userId);
        values.put(GROUP_ID, groupId);
        values.put(REVISION_COUNT, 1);
        getDatabaseInstance().insert(TABLE_FREQUENT_GROUPS, null, values);
    }

    public void update_FrequentContact(String userId, String convId, String timeStamp) {
        String query = "SELECT * FROM " + TABLE_FREQUENT_CONTACTS + " WHERE " +
                CONVS_ID + "='" + convId + "'";
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int count_revision = cursor.getInt(cursor.getColumnIndex(REVISION_COUNT));
                ContentValues values = new ContentValues();
                values.put(USERID, userId);
                values.put(CONVS_ID, convId);
                values.put(REVISION_COUNT, count_revision + 1);
                getDatabaseInstance().update(TABLE_FREQUENT_CONTACTS, values, CONVS_ID + "='" + convId + "'", null);
            }
            cursor.close();
        }
    }

    public void update_FrequentGroup(String userId, String groupId, String timeStamp) {
        String query = "SELECT * FROM " + TABLE_FREQUENT_GROUPS + " WHERE " +
                GROUP_ID + "='" + groupId + "'";
        Cursor cursor = getDatabaseInstance().rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int count_revision = cursor.getInt(cursor.getColumnIndex(REVISION_COUNT));

                ContentValues values = new ContentValues();
                values.put(USERID, userId);
                values.put(GROUP_ID, groupId);
                values.put(REVISION_COUNT, count_revision + 1);

                getDatabaseInstance().update(TABLE_FREQUENT_GROUPS, values, GROUP_ID + "='" + groupId + "'", null);
            }
            cursor.close();
        }
    }

    public List<SmarttyContactModel> getFrequentContacts(Context context, String userId) {
        UserInfoSession userInfoSession = new UserInfoSession(context);
        List<SmarttyContactModel> dataList = new ArrayList<>();
        Cursor cursor = getDatabaseInstance().rawQuery("SELECT * FROM smartty_CONTACT__FREQUENT_CONTACTS where UserID = '" + userId + "'  order by revisioncount desc", null);
        System.out.println("print qry: " + cursor);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String conv = cursor.getString(cursor.getColumnIndex(CONVS_ID));
                Log.d(TAG, "getFrequentContacts: conv " + conv);
                if (conv != null) {
                    String userIds = userInfoSession.getReceiverIdByConvId(conv);
                    Log.d(TAG, "getFrequentContacts: userIds: " + userIds);
                    if (userIds != null && !userIds.equals("") && getBlockedStatus(userIds, false).equals("0")) {
                        SmarttyContactModel data = getUserOpponenetDetails(userIds);
                        dataList.add(data);
                    }
                    if (dataList.size() > 2) {
                        break;
                    }
                }
            }
            cursor.close();
        }
        return dataList;
    }

    public List<SmarttyContactModel> getFrequentGroups(Context context, String userId) {
        Getcontactname getcontactname = new Getcontactname(context);
        List<SmarttyContactModel> dataList = new ArrayList<>();
        Cursor cursor = getDatabaseInstance().rawQuery("SELECT * FROM " + TABLE_FREQUENT_GROUPS + " where UserID = '" + userId + "'  order by revisioncount desc", null);
        GroupInfoSession groupInfoSession = new GroupInfoSession(context);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String groupId = cursor.getString(cursor.getColumnIndex(GROUP_ID));
                Log.d(TAG, "getFrequentContacts: conv " + groupId);
                if (groupId != null) {
                    String docIdd = userId + "-" + groupId + "-g";
                    GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docIdd);
                    SmarttyContactModel vSmarttyContactModel = new SmarttyContactModel();
                    if (infoPojo != null && infoPojo.getGroupName() != null) {
                        StringBuilder sb = new StringBuilder();

                        vSmarttyContactModel.set_id(groupId);
                        vSmarttyContactModel.setType("");
                        vSmarttyContactModel.setAvatarImageUrl(infoPojo.getAvatarPath());
                        vSmarttyContactModel.setFirstName(infoPojo.getGroupName());
                        vSmarttyContactModel.setGroup(true);
                        String memername = "";
                        if (infoPojo.getGroupMembers() != null) {
                            String[] contacts = infoPojo.getGroupMembers().split(",");

                            for (int i = 0; i < contacts.length; i++) {
                                if (!contacts[i].equalsIgnoreCase(userId)) {
                                    String msisdn = getSingleData(contacts[i], ContactDB_Sqlite.MSISDN);
                                    if (msisdn != null) {
                                        memername = getcontactname.getSendername(contacts[i], msisdn);
                                        sb.append(memername);
                                        if (contacts.length - 1 != i) {
                                            sb.append(", ");
                                        }
                                    }
                                } else {
                                    memername = "You";
                                    sb.append(memername);
                                    if (contacts.length - 1 != i) {
                                        sb.append(", ");
                                    }
                                }
                            }
                            vSmarttyContactModel.setStatus(String.valueOf(sb));
                        }
                        dataList.add(vSmarttyContactModel);
                    }
                }
            }
            cursor.close();
        }
        return dataList;
    }

    public int getContactsCount() {
        try {
            String query = "SELECT * FROM " + TABLE_USERSDETAILS_BLOCK_UNBLOCK_STATUS;
            return getItemsCount(query);
        } catch (Exception e) {
            MyLog.e(TAG, "isMutedUser: ", e);
        }
        return 0;
    }

    private int getItemsCount(String query) {
        Cursor cursor = null;
        try {
            cursor = getDatabaseInstance().rawQuery(query, null);
            return cursor.getCount();
        } catch (Exception e) {
            MyLog.e(TAG, "getItemsCount: ", e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return 0;
    }
}