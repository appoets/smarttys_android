package com.app.Smarttys.core.socket;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.app.Smarttys.app.utils.MyLog;

public class TemporaryForegroundService extends Service {

    static TemporaryForegroundService instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        if (startService(new Intent(this, PermanentBackgroundService.class)) == null)
            throw new RuntimeException("Couldn't find " + PermanentBackgroundService.class.getSimpleName());
        MyLog.e("onCreate", "TemporaryForegroundService");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        instance = null;
        MyLog.e("onDestroy", "TemporaryForegroundService");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
