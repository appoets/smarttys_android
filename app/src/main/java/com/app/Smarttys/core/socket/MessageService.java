package com.app.Smarttys.core.socket;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.app.Smarttys.R;
import com.app.Smarttys.app.activity.ChatPageActivity;
import com.app.Smarttys.app.activity.Chatbackup;
import com.app.Smarttys.app.activity.SecretChatViewActivity;
import com.app.Smarttys.app.activity.SmarttyContactsService;
import com.app.Smarttys.app.calls.CallAck;
import com.app.Smarttys.app.calls.CallDisconnect;
import com.app.Smarttys.app.calls.CallMessage;
import com.app.Smarttys.app.calls.CallsActivity;
import com.app.Smarttys.app.calls.IncomingCallActivity;
import com.app.Smarttys.app.model.FirstTimeGroupRefreshed;
import com.app.Smarttys.app.model.GroupInviteBraodCast;
import com.app.Smarttys.app.model.GroupMemberFetched;
import com.app.Smarttys.app.model.NewGroupDetails_Model;
import com.app.Smarttys.app.model.StatusRefresh;
import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.AutoDownLoadUtils;
import com.app.Smarttys.app.utils.CryptLibDecryption;
import com.app.Smarttys.app.utils.DateChangeBroadcastReceiver;
import com.app.Smarttys.app.utils.Getcontactname;
import com.app.Smarttys.app.utils.GroupInfoSession;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.app.utils.SharedPreference;
import com.app.Smarttys.app.utils.StringCryptUtils;
import com.app.Smarttys.app.utils.UserInfoSession;
import com.app.Smarttys.core.CoreActivity;
import com.app.Smarttys.core.CoreController;
import com.app.Smarttys.core.Session;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.ShortcutBadgeManager;
import com.app.Smarttys.core.database.ContactDB_Sqlite;
import com.app.Smarttys.core.database.DatabaseClassForDB;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.ChangeSetController;
import com.app.Smarttys.core.message.GroupEventInfoMessage;
import com.app.Smarttys.core.message.IncomingMessage;
import com.app.Smarttys.core.message.MessageAck;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.message.MesssageObjectReceiver;
import com.app.Smarttys.core.message.OfflineMessageHandler;
import com.app.Smarttys.core.model.CallItemChat;
import com.app.Smarttys.core.model.ChatLockPojo;
import com.app.Smarttys.core.model.GroupInfoPojo;
import com.app.Smarttys.core.model.GroupMembersPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.model.OfflineRetryEventPojo;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.app.Smarttys.core.model.SmarttyContactModel;
import com.app.Smarttys.core.service.Constants;
import com.app.Smarttys.core.service.UpdateContactResponse;
import com.app.Smarttys.core.uploadtoserver.FileUploadDownloadManager;
import com.app.Smarttys.eventnotejob.DemoSyncJob;
import com.app.Smarttys.status.controller.StatusUploadUtil;
import com.app.Smarttys.status.controller.StatusUtil;
import com.app.Smarttys.status.model.StatusDB;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import io.socket.client.Socket;
import me.leolin.shortcutbadger.ShortcutBadger;


/**
 *
 */
public class MessageService extends Service implements EventCallBack {

    public final static String SENDMESAGGE = "passMessage";
    public static final long MIN_GET_OFFLINE_MESSAGES_TIME = 10 * 1000; // 60 seconds
    public static final String BACK_UP_NEVER = "Never";
    public static final String BACK_UP_ONLY_I_TAP = "Only when I tap backup";
    public static final String BACK_UP_DAILY = "Daily";
    public static final String BACK_UP_WEEKLY = "Weekly";
    public static final String BACK_UP_MONTHLY = "Monthly";
    //Reply Notification
    public static final String NOTIFICATION_REPLY = "NotificationReply";
    public static final String KEY_INTENT_MORE = "keyintentmore";
    public static final int REQUEST_CODE_MORE = 100;
    public static final String KEY_INTENT_REPLY = "replyNotification";
    public static final int REQUEST_CODE_NOTIFICATION_REPLY = 190;
    private static final String TAG = MessageService.class.getSimpleName();
    public static SocketManager manager;
    static public MessageService service;
    public static boolean dataCleared = false;
    public static HashMap<String, Long> receivedUserDetailsMap = new HashMap<>();
    public LinkedList<String> messagegroupid = new LinkedList<>();
    public LinkedList<String> messageIds = new LinkedList<>();
    public LinkedList<String> messageSendIds = new LinkedList<>();
    public LinkedList<String> mMessageIdIncomingCall = new LinkedList<>();
    public LinkedList<String> mWriteAudio = new LinkedList<>();
    Getcontactname getcontactname;
    DatabaseClassForDB statusDB;
    Context mcontext;
    private long sessionFailedTimeStamp = 0;
    private ActiveSocketDispatcher dispatcher;
    private MesssageObjectReceiver objectReceiver;
    private FileUploadDownloadManager uploadDownloadManager;
    private boolean wifiEnabled = false;
    private String uniqueCurrentID;
    private IncomingMessage incomingMsg;
    private String connected;
    private Session session;
    private UserInfoSession userInfoSession;
    private GroupInfoSession groupInfoSession;
    private SessionManager sessionManager;
    private OfflineMessageHandler offlineMsgHandler;
    private boolean isFileUploadCalled;
    private Handler incomCallBroadcastHandler;
    private Runnable incomCallBroadcastRunnable;
    SocketManager.SocketCallBack callBack = new SocketManager.SocketCallBack() {
        @Override
        public void onSuccessListener(String eventName, Object... response) {
            ReceviceMessageEvent me = new ReceviceMessageEvent();
            me.setEventName(eventName);

            if (AppUtils.isEncryptionEnabled(mcontext)) {
                try {
                    if (response != null && !SocketManager.excludedList.contains(eventName)) {
                        //         MyLog.e(TAG, "invokeCallBack: event name" + eventName);
                        response[0] = SocketManager.getDecryptedMessage(mcontext, response[0].toString(), eventName);
                        // String decrypted = response[0].toString();
                        //        MyLog.e(TAG, "onSuccessListener: " + decrypted);
                        /*String responsee = response[0].toString();
                        String encryptionTokenHashKey = SessionManager.getInstance(mcontext).getSecurityTokenHash();
                        String messageAfterDecrypt = AESCrypt.decrypt(encryptionTokenHashKey, responsee);
                        response[0] = messageAfterDecrypt;*/
                        MyLog.e(TAG, "response[0]" + response[0] + "eventName" + eventName);
                    }
                } catch (Exception e) {
                    //       MyLog.e(TAG, "onSuccessListener: ", e);
                }
            }
            try {
                //for re-login
                if (response != null && response.length > 0) {
                    String decrypted = response[0].toString();
                    long currentTime = System.currentTimeMillis();
                    long diffMillis = currentTime - sessionFailedTimeStamp;

                    if (diffMillis > 5000) {
                        if ((decrypted.contains("Authentication Failed"))) {
                            if (sessionManager.getlogin()) {
                                createUser();
                                sessionFailedTimeStamp = System.currentTimeMillis();

                                if ((decrypted.contains("Authentication Failed"))) {
                                    SessionManager.getInstance(mcontext).logoutUser(true);
                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(mcontext, "Your session expired or You may login in another device!\nPlease login again", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        } else if (decrypted.contains("invalid token")) {
                            //fetchSecretKeys();
                            performLogout();
                        }
                    }


                }
            } catch (Exception e) {
                MyLog.e(TAG, "onSuccessListener: ", e);
            }
            me.setObjectsArray(response);
            if (eventName != null && !eventName.equals("sc_change_online_status") && !eventName.equals("sc_get_offline_status"))
                //    MyLog.d(TAG, "onSuccessListener: " + eventName);
                //MyLog.d("Event_response"+" "+" "+eventName+" "+" "+response.toString());
                //    MyLog.e(TAG, "allevents MessageService eventname: " + eventName);

                switch (eventName) {
                    case Socket.EVENT_CONNECT: {
                        MyLog.e(TAG, "EVENT_CONNECT");
                        if (sessionManager.getlogin()) {
                            onSocketConnect();
                        }
                        if (!manager.isConnected()) {
                            manager.connect();
                        }
                    }
                    break;
                    case SocketManager.EVENT_GET_OFFLINE_GROUPMESSAGE: {
                        //   MyLog.e(TAG, "EVENT_GET_OFFLINE_GROUPMESSAGE" + response[0].toString());
                        handleGroupResponse(response);
                    }
                    break;
                    case Socket.EVENT_DISCONNECT: {
                        MyLog.e(TAG, "EVENT_DISCONNECT");
                        onSocketDisconnect();

                    }
                    break;

                    case SocketManager.EVENT_STATUS_RESPONSE:
                        MyLog.d(TAG, "onSuccessListener: EVENT_STATUS_RESPONSE");
                        StatusUtil.myStatusSuccessResponse(MessageService.this, me);
                        StatusUploadUtil.getInstance().checkAndUploadStatus(MessageService.this);
                        break;
//Check whether the Media status response is calling or not recevier
                    case SocketManager.EVENT_STATUS:
                        MyLog.e(TAG, "onSuccessListener: EVENT_STATUS");
                        //      StatusUtil.myStatusSuccessResponse(MessageService.this, me);
                        //     StatusUploadUtil.getInstance().checkAndUploadStatus(MessageService.this);

                        break;

                    case SocketManager.EVENT_STATUS_MUTE:
                        StatusUtil.muteUnMuteResponse(MessageService.this, me);
                        break;
                    case SocketManager.EVENT_TO_DELETECHAT:

                        try {
                            Object[] array = me.getObjectsArray();
                            if (array == null || array.length == 0)
                                return;
                            JSONObject objects = new JSONObject(array[0].toString());
                            MyLog.e(TAG, "EVENT_TO_DELETECHAT " + objects);
                            String star_status = null;
                            String opponent = null;
                            boolean mStar = false;
                            String notifyType = objects.getString("notifyType");
                            String convId = objects.getString("convId");
                            String lastId = objects.getString("lastId");

                            //Delete chat in  oppenent
                            if (objects.has("star_status")) {
                                star_status = objects.getString("star_status");
                            }
                            //iF IS IS MASS CHAT FOR RECEIVER CLEAR  OR DELETE CHAT
                            if (objects.has("opponent")) {
                                opponent = objects.getString("opponent");
                            }
                            if (objects.has("star_status")) {
                                if (Integer.parseInt(star_status) == 1) {
                                    mStar = true;
                                }
                            }
                            if (notifyType.equals("delete")) {
                                JSONObject object = new JSONObject();
                                object.put("convId", convId);
                                object.put("lastId", lastId);
                                object.put("from", uniqueCurrentID);
                                object.put("type", MessageFactory.CHAT_TYPE_SINGLE);
                                Log.e("DELETE_OPPONENT", "DELETE_OPPONENT" + object);
                                SendMessageEvent event = new SendMessageEvent();
                                event.setEventName(SocketManager.DELETE_OPPONENT);
                                event.setMessageObject(object);
                                EventBus.getDefault().post(event);

                                //delete  my chat for mass chat
                                //Clear data for single chat
                                MessageDbController db = CoreController.getDBInstance(mcontext);
                                long mLong = Long.parseLong(lastId);

                                // lastId=lastId+2000;
                                MyLog.e(TAG, "clearAllSingleChatMessageTimeStamp" + mLong);
                                String mMessageId = uniqueCurrentID + "-" + opponent + "-" + lastId;
                                MyLog.e(TAG, "mMessageId" + mMessageId);
                                MyLog.e(TAG, "mchatId 5d15e1e8e87e7f685173dad4-5d15e0e0e87e7f685173dac4");
                                String[] arrIds = mMessageId.split("-");
                                String chatId = arrIds[0] + "-" + arrIds[1];
                                MyLog.e(TAG, "chatId" + chatId);
                                db.clearAllSingleChatMessageTimeStamp(mLong, mMessageId, chatId);

                                //db.clearAllUnStarredSingleChatMessagewithTime(mLong, mMessageId, MessageFactory.CHAT_TYPE_SINGLE);


                            } else if (notifyType.equals("clear")) {
                                JSONObject object = new JSONObject();
                                object.put("convId", convId);
                                object.put("lastId", lastId);
                                if (objects.has("star_status")) {
                                    object.put("star_status", star_status);
                                }
                                object.put("from", uniqueCurrentID);
                                object.put("type", MessageFactory.CHAT_TYPE_SINGLE);
                                Log.e("DELETE_OPPONENT", "DELETE_OPPONENT" + object);
                                SendMessageEvent event = new SendMessageEvent();
                                event.setEventName(SocketManager.EVENT_CLEAR_CHAT_OPPENENT);
                                event.setMessageObject(object);
                                EventBus.getDefault().post(event);

                                //clear my chat for mass chat

                                String docid = uniqueCurrentID + "-" + opponent + "-" + lastId;
                                Log.e("EVENT_CLEAR_CHAT_OPPENENT", "docid" + docid);

//clear chat items
                                MessageDbController db = CoreController.getDBInstance(mcontext);
                                long mLong = 0L;
                                //    mLong=Long.parseLong(lastId)+2550;
                                mLong = Long.parseLong(lastId);


                                if (mStar) {
                                    //Check timestamp  and delete all values below it
                                    //     Toast.makeText(mcontext, getString(R.string.chatclear), Toast.LENGTH_SHORT).show();
                                    showclearToast();
                                    db.clearAllUnStarredSingleChatMessagewithTime(mLong, docid, MessageFactory.CHAT_TYPE_SINGLE);
                                    //   db.clearUnStarredMessage(docId, receiverId, MessageFactory.CHAT_TYPE_SINGLE);
                                } else {
                                    //    db.clearAllSingleChatMessage(docId, receiverId);
                                    //working with time stamp
                                    //   db.clearAllSingleChatMessagewithTime((mLong),messageId);
                                    //   Toast.makeText(mcontext, getString(R.string.chatclear), Toast.LENGTH_SHORT).show();
                                    showclearToast();
                                    db.clearAllSingleChatMessagewithTimeMessage((mLong), docid);

                                }

                            }


                        } catch (Exception e) {
                            MyLog.e(TAG, "myStatusUpdate: ", e);
                        }
                        //  StatusUtil.muteUnMuteResponse(MessageService.this, me);
                        break;
                    case SocketManager.DELETE_OPPONENT:
                        try {
                            Object[] array = me.getObjectsArray();
                            if (array == null || array.length == 0)
                                return;
                            JSONObject objects = new JSONObject(array[0].toString());
                            MyLog.e(TAG, "objects### DELETE_OPPONENT: " + objects);


                        } catch (Exception e) {
                            MyLog.e(TAG, "myStatusUpdate: ", e);
                        }
                        // StatusUtil.muteUnMuteResponse(MessageService.this, me);
                        break;
                    case SocketManager.EVENT_MESSAGE: {
                        if (AppUtils.isEncryptionEnabled(mcontext)) {
                            //Decrypt my message
                            try {
                                JSONObject object = null;
                                if (response != null) {
                                    object = new JSONObject(response[0].toString());
                                    MyLog.e(TAG, "EVENT_MESSAGE: " + response[0].toString());

                                    loadMessage(object);
                                }

                            } catch (Exception e) {
                                MyLog.e(TAG, "onSuccessListener: ", e);
                            }
                        } else {
                            MyLog.e(TAG, "EVENT_MESSAGE: " + response[0].toString());

                            JSONObject object = (JSONObject) response[0];
                            loadMessage(object);
                        }
                    }
                    break;

                    case SocketManager.EVENT_GET_MESSAGE: {
                        storeInDataBase(response);
                    }
                    break;

                    case SocketManager.EVENT_GROUP: {
                        handleGroupResponse(response);
                    }
                    break;

                    case SocketManager.EVENT_NEW_FILE_MESSAGE: {
                        try {
                            MyLog.d(TAG, "onSuccessListener: uploadVideoChatFile");
                            JSONObject object = new JSONObject(response[0].toString());
                            Log.d(TAG, "onClick: startFileUpload9");
                            uploadDownloadManager.startFileUpload(EventBus.getDefault(), object);
                        } catch (Exception e) {
                            MyLog.e(TAG, "", e);
                        }
                    }
                    break;

                    case SocketManager.EVENT_FILE_RECEIVED: {
                        try {

                            MyLog.d(TAG, "filetest app/received onSuccessListener: ");
                            JSONObject object = new JSONObject(response[0].toString());
                            Log.e(TAG, "filetest app/received onSuccessListener: " + object);



                            /*try {
                                String msgId = mSendingObject.getString("ImageName");
                                MessageDbController dbController = CoreController.getDBInstance(mContext);
                                dbController.userpicupdateMessage(msgId, "uploading", mSendingObject);
                                Log.e(TAG, "uploading" + mSendingObject);


                            } catch (Exception e) {
                                MyLog.e(TAG, "uploadImageChatFile: ", e);
                            }*/
                            if (object.has("id")) {

                                String msgId = object.getString("id");


                                MessageDbController dbController = CoreController.getDBInstance(mcontext);

                                String Upload_status = dbController.fileuploadStatusget(msgId);


                                if (Upload_status.equalsIgnoreCase("uploading")) {

                                    Log.d(TAG, "onClick: startFileUpload10 file received ");
                                    uploadDownloadManager.startFileUpload(EventBus.getDefault(), object);

                                } else if (Upload_status.equalsIgnoreCase("pause")) {

                                    dbController.fileuploadupdateMessage(msgId, "pause", object);


                                } else if (Upload_status.equalsIgnoreCase("")) {
                                    Log.d(TAG, "onClick: startFileUpload11 file received");
                                    uploadDownloadManager.startFileUpload(EventBus.getDefault(), object);

                                }

                            }/* else if (object.has("ImageName")) {
                                Log.e(TAG, "filetest app/received ImageName: " + object);


                                try {
                                    String msgId = object.getString("ImageName");
                                    MessageDbController dbController = CoreController.getDBInstance(mcontext);
                                    String Upload_status = dbController.userpicfileuploadStatusget(msgId);
                                    Log.e(TAG, "filetest app/received ImageName: " + Upload_status);


                                    if (Upload_status.equalsIgnoreCase("uploading")) {
                                        Log.d(TAG, "onClick: startFileUpload10 file received ");
                                        uploadDownloadManager.startFileUpload(EventBus.getDefault(), object);


                                        try {
                                            dbController.userpicupdateMessage(msgId, "uploading", object);
                                            Log.e(TAG, "uploading saved" + object);
                                        } catch (Exception e) {
                                            MyLog.e(TAG, "uploadImageChatFile: ", e);
                                        }
                                    }
                                    //  dbController.userpicupdateMessage(msgId, "uploading", mSendingObject);
                                    // Log.e(TAG, "uploading" + mSendingObject);


                                } catch (Exception e) {
                                    MyLog.e(TAG, "uploadImageChatFile: ", e);
                                }
                            }*/ else {

                                Log.d(TAG, "onClick: startFileUpload12 file received");
                                uploadDownloadManager.startFileUpload(EventBus.getDefault(), object);

                            }


                        } catch (Exception e) {
                            MyLog.e(TAG, "onSuccessListener: ", e);
                        }
                    }
                    break;

                    case SocketManager.EVENT_STAR_MESSAGE: {
                        loadStarredMessage(response);
                    }
                    break;

                    case SocketManager.EVENT_REMOVE_MESSAGE: {
//                   loadDeleteMessage(response);
                    }
                    break;

                    case SocketManager.EVENT_START_FILE_DOWNLOAD: {
                        try {
                            JSONObject jsonObject = (JSONObject) response[1];
                            writeBufferToFile(jsonObject);
                        } catch (Exception e) {
                            MyLog.e(TAG, "", e);
                        }
                    }
                    break;

                    case SocketManager.EVENT_MESSAGE_RES: {
                        loadMessageRes(response);
                    }
                    break;

                    case SocketManager.EVENT_MESSAGE_STATUS_UPDATE: {
                        Log.d(TAG, "onSuccessListener: tickMiss");
                        loadMessageStatusUpdate(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_STATUS_UPDATE:
                        StatusUtil.statusAckByOthers(MessageService.this, me, uniqueCurrentID);
                        break;

                    case SocketManager.EVENT_STATUS_DELETE:
                        //    Toast.makeText(getActivity(), "Status remove", Toast.LENGTH_SHORT).show();
                        MyLog.d(TAG, "onMessageEvent: EVENT_STATUS_DELETE");
                        StatusUtil.afterStatusDelete(me, MessageService.this);
                        break;


                    case SocketManager.EVENT_MESSAGE_ACK: {
                        loadMessageAckMessage(response[0].toString());
                    }
                    break;


                    case SocketManager.EVENT_GET_MOBILE_SETTINGS: {
                        try {
                            JSONObject object = new JSONObject(response[0].toString());
                            performSettingsConnect(object);
                        } catch (Exception e) {
                            MyLog.e(TAG, "", e);
                        }
                    }
                    break;

                    case SocketManager.EVENT_GET_USER_DETAILS: {
                        //saveUserDetails(response[0].toString());
                        //new saveUserDetails(response[0].toString()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        EventBus.getDefault().post(new GroupMemberFetched());
                        // new saveUserDetails(response[0].toString()).execute();
                        saveUserDetailsOnBackground(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_UPDATE_MOBILE_LOGIN_NOTIFICATION: {
                        loadDeviceLoginMessage(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_CHECK_MOBILE_LOGIN_KEY: {
                        loadCheckLoginKey(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_PHONE_DOWNLOAD: {
                        loadPhoneDownloadMessage(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_PHONE_DATA_RECEIVED: {
                        loadPhoneDataReceivedMessage(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_CHANGE_PROFILE_STATUS: {
                        loadProfileStatusChangeMessage(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_IMAGE_UPLOAD: {
                        Log.e(TAG, "EVENT_IMAGE_UPLOAD");
                        loadProfilePicChangeMessage(response[0].toString());
                    }

                    case SocketManager.EVENT_GET_ACKSTATUS: {
                        Log.e(TAG, "EVENT_GET_ACKSTATUS" + response[0].toString());
                        // loadProfilePicChangeMessage(response[0].toString());
                        //StatusUtil.statusAckByOthersOffline(this, event, mCurrentUserId);
                    }

                    break;

                    case SocketManager.EVENT_CHANGE_USER_NAME: {
                        loadProfileNameChangeMessage(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_GROUP_DETAILS: {
                        loadGroupDetails(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_MOBILE_TO_WEB_LOGOUT: {
                        //  performLogout(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_VIEW_CHAT: {
                        changeMsgViewedStatus(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_DELETE_CHAT: {
                        boolean isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
                        if (isMassChat) {
                            clearnotify();
                        }

                        loadDeleteChat(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_CLEAR_CHAT: {
                        //    boolean isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
                        //    if (isMassChat) {
                        clearnotify();
                        //   }
                        loadClearChat(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_ARCHIVE_UNARCHIVE: {
                        loadarchive(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_GET_GROUP_LIST: {
                        final String responseStr = response[0].toString();

                        performBackgroundGroupInfo(responseStr);
                    }
                    break;

                    case SocketManager.EVENT_GET_MESSAGE_INFO: {
                        loadMessageOfflineAcks(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_PRIVACY_SETTINGS: {
                        loadPrivacySetting(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_USER_CREATED: {
                        if (AppUtils.isEncryptionEnabled(mcontext)) {
                            onUserConnectedToServer(response[0].toString());
                        }
                        boolean isLoginKeySent = !sessionManager.isLoginKeySent();
                        if (sessionManager != null && isLoginKeySent) {
                            attachDeviceWithAccount();
                        }

                    }
                    break;

                    case SocketManager.EVENT_GET_SERVER_TIME: {
                        loadServerTimeMessage(response[0].toString());
                    }
                    case SocketManager.EVENT_USER_AUTHENTICATED: {

                        //if (AppUtils.isEncryptionEnabled(mcontext)) {

                        String securityToken = SessionManager.getInstance(getApplicationContext()).getSecurityToken();
                        MyLog.e(TAG, "offlinegrouptest securityToken" + securityToken);

                        if (AppUtils.isEmpty(securityToken)) {


                            fetchSecretKeys();
                        }

                        fetchOfflineMsgsAndHistory();


                        //}
                    }
                    break;
                    case SocketManager.EVENT_GET_SECRET_KEYS: {
                        changesecuritytoken(response[0].toString());
                    }
                    case SocketManager.EVENT_CAHNGE_ONLINE_STATUS: {
                        loadOnlineStatus(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_DELETE_ACCOUNT: {
                        loadDeleteAccountMessage(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_CLEAR_DB:
                        clearDB(MessageService.this);
                        break;

                    case SocketManager.EVENT_CLEAR_ALL_MESSAGES:
                        clearAllMsgs(MessageService.this);
                        break;

                    case SocketManager.EVENT_REMOVE_MOBILE_LOGIN_NOTIFICATION: {
                        loadUnSetDeviceTokenMessage(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_TO_CONV_SETTING: {
                        loadToConvSettings(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_MUTE: {
                        loadmute(response[0].toString());
                    }
                    break;


                    case SocketManager.EVENT_STATUSDELETE: {
                        loadofflinestatus(response[0].toString());
                    }
                    case SocketManager.EVENT_MARKREAD: {
                        loadmark(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_BLOCK_USER: {
                        updateBlockUserStatus(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_CHAT_LOCK: {
                        loadChatLockResponse(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_CHANGE_EMAIL: {
                        responseforeventchangemail(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_RECOVERY_EMAIL: {
                        saveRecoveryEmail(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_RECOVERY_PHONE: {
                        responseforeventRecoveryPhone(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_VERIFY_EMAIL: {
                        responseforVerifyemail(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_UPDATE_GOOGLE_DRIVE_SETTINGS: {
                        loadBackUpUpdateMessage(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_GET_APP_SETTINGS: {
                        loadAppSettingsData(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_GET_UPLOADED_FILE_SIZE: {
                        resumeFileUpload(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_GET_CONV_ID: {
                        saveConvId(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_GET_SETTINGS: {
                        loadAdminSettingsData(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_GET_ADMIN_SETTINGS: {
                        loadAdminSettingsData(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_GET_CONTACTS: {
                        session.setFavContacts(response[0].toString());
                    }
                    break;
                    case SocketManager.EVENT_UPDATE_CONTACT: {
                        UpdateContactResponse.getInstance().updateContact(me, MessageService.this);
                    }
                    break;
                    case SocketManager.EVENT_CHAT_LOCK_FROM_WEB: {
                        MyLog.d("Chatlock_from web", response[0].toString());
                    }
                    break;
                    case SocketManager.EVENT_CALL: {

                        Log.e(TAG, "EVENT_CALL sc_call" + response[0].toString());

                        loadIncomingCallData(response[0].toString());
                    }
                    break;
                    case SocketManager.EVENT_CALL_STATUS: {
                        loadCallStatusFromOpponentUser(response[0].toString());
                    }
                    break;
                    case SocketManager.EVENT_GET_OFFLINE_CALLS: {
                        loadOfflineCalls(response[0].toString());
                    }
                    break;
                    case SocketManager.EVENT_REMOVE_CALLS: {
                        loadRemoveCallLog(response[0].toString());
                    }
                    break;
                    case SocketManager.EVENT_REMOVE_ALL_CALLS: {
                        loadRemoveAllCallLogs(response[0].toString());
                    }
                    break;
                    case SocketManager.EVENT_CONV_SETTINGS: {
                        loadConvSettingsMessage(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_GET_MESSAGE_DETAILS: {
                        loadReplyMessageDetails(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_REMOVED_ACCOUNT_BY_ADMIN: {
                        loadRemovedAccountMessage(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_CALL_STATUS_RESONSE: {
                        MyLog.d(TAG, "" + response[0].toString());
                    }
                    break;


                    //-------Delete Chat-----------------------
                    case SocketManager.EVENT_DELETE_MESSAGE: {
                        deleteSingleMessage(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_SINGLE_OFFLINE_MSG: {
                        getSingleOffline(response[0].toString());
                    }
                    break;


                    case SocketManager.EVENT_CHANGE_SECURITY_CODE:
                        updateSecurityCode(response[0].toString());
                        break;

                    case SocketManager.EVENT_NEW_ROOM_CONNECTION:
                        updateSecurityCode(response[0].toString());
                        break;


                    case SocketManager.EVENT_ADMIN_TRACK_START:
                        SessionManager.getInstance(mcontext).setAdminLocationRequested(true);
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (CoreController.gpsTracker != null) {
                                    Location location = CoreController.gpsTracker.getLocation();
                                    CoreController.gpsTracker.sendLocation(location);
                                }
                            }
                        });
                        break;


                    case SocketManager.EVENT_ADMIN_TRACK_STOP:
                        SessionManager.getInstance(mcontext).setAdminLocationRequested(false);
                        break;

                }


            dispatcher.addwork(me);
        }


    };
    private DateChangeBroadcastReceiver mDateReceiver;

    public static boolean isStarted() {
        return service != null;
    }

    public static ActivityManager.RunningServiceInfo getRunningServiceInfo(Class serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return service;
            }
        }
        return null;
    }

    public static void clearDB(Context context) {
        MessageDbController db = CoreController.getDBInstance(context);

        //EXIT ALL GROUPS
        List<MessageItemChat> groupChats = new ArrayList<>();
        groupChats = db.selectChatList(MessageFactory.CHAT_TYPE_GROUP);
        for (MessageItemChat messageItemChat : groupChats) {
            String[] arrIds = messageItemChat.getMessageId().split("-");
            String groupId = arrIds[1];
            // NewChatListFragment.performGroupExit(groupId);
        }
        //CLEAR ALL CHATS
        db.clearDatabase();
        //CLEAR ALL CONTACTS
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
        contactDB_sqlite.clearDatabase();
        //AppUtils.startService(context, ContactsSync.class);

        //CLEAR ALL STATUS
        StatusDB statusDB = new StatusDB(context);
        statusDB.clearDatabase();
        SharedPreference.getInstance().clearSharedPreference(context);
        //ChangeSetController.setChangeStatus("0");
    }

    public static void clearAllMsgs(Context context) {
        MessageDbController db = CoreController.getDBInstance(context);
        //CLEAR ALL CHATS
        db.clearAllMsgs();
    }

    public static void removeUser(String groupId) {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_REMOVE_USER);
        JSONObject object = new JSONObject();
        try {
            object.put("_id", groupId);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        messageEvent.setMessageObject(object);
        EventBus.getDefault().post(messageEvent);
    }

    public static void getStatusHistory(String currentUserId) {
        MyLog.d(TAG, "getStatusHistory: " + currentUserId);
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_STATUS_OFFLINE);
        JSONObject object = new JSONObject();
        try {
            object.put("msg_to", currentUserId);
        } catch (JSONException e) {
            MyLog.e(TAG, "getStatusHistory: ", e);
        }
        messageEvent.setMessageObject(object);
        EventBus.getDefault().post(messageEvent);
    }

    public static void sendGroupACK(String GroupID, String msgId, String uniqueCurrentID) {
        SendMessageEvent groupMsgEvent = new SendMessageEvent();
        groupMsgEvent.setEventName(SocketManager.EVENT_GROUP);
        try {
            if (GroupID == null || GroupID.isEmpty())
                return;
            JSONObject object = new JSONObject();
            object.put("from", uniqueCurrentID);
            object.put("groupType", SocketManager.ACTION_ACK_GROUP_MESSAGE);
            object.put("groupId", GroupID);
            object.put("status", "1");
            object.put("msgId", msgId);
            groupMsgEvent.setMessageObject(object);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        EventBus.getDefault().post(groupMsgEvent);
    }

    public void onMessageEvent(final SendMessageEvent event) {
        String eventName = event.getEventName();
        Object eventObj = event.getMessageObject();


        // Store always messages in offline mode, remove from offline message list after getting message response
        if (offlineMsgHandler.isOfflineMessageEvent(eventName, eventObj)) {
            try {
                JSONObject object = (JSONObject) eventObj;
                String msgId = object.getString("toDocId");

                OfflineRetryEventPojo pojo = new OfflineRetryEventPojo();
                pojo.setEventId(msgId);
                pojo.setEventName(eventName);
                Log.d(TAG, "onMessageEvent: offlinemsgmiss");
                if (messageSendIds.contains(eventName + msgId)) {
                    Log.d(TAG, "onMessageEvent: offlinemsgmiss return");
                    return;
                }
                messageSendIds.add(eventName + msgId);
                pojo.setEventObject(eventObj.toString());

                CoreController.getDBInstance(MessageService.this).updateSendNewMessage(pojo);
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
        }

        if (eventName.equals(SocketManager.EVENT_GROUP)) {
            try {
                //for broadcast feature
                Log.d(TAG, "onMessageEvent: offlinemsgmiss setbraodcast");
                event.getMessageObject().put("is_broadcast", 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (manager != null && manager.isConnected()) {
            MyLog.d(TAG, "filetest onMessageEvent: socket connected");
            manager.send(event.getMessageObject(), event.getEventName());

        } else {
            MyLog.d(TAG, "filetest onMessageEvent: socket not connected");
            if (manager == null) {
                manager = SocketManager.getInstance();
                manager.init(MessageService.this, null);
                manager.setCallBack(callBack);
            }
            if (manager != null && !manager.isConnected()) {
                manager.connect();
            } else {
                MyLog.d(TAG, "manager null");

            }
            MyLog.d(TAG, "manager" + !manager.isConnected());

            manager.send(event.getMessageObject(), event.getEventName());
            offlineMsgHandler.storeOfflineEventData(event.getEventName(), event.getMessageObject());
            MyLog.d("SocketDisconnect----------" + "Post Comments");

        }
    }

/*
    public static boolean checkSocketConnected() {

        return manager.isConnected();
    }
*/

    private boolean isBlockedUser(String to) {
        try {
            ContactDB_Sqlite contactDB_sqlite = new ContactDB_Sqlite(this);
            boolean isBlockedInSecret = contactDB_sqlite.getBlockedStatus(to, true).equals("1");
            boolean isBlockedInNormal = contactDB_sqlite.getBlockedStatus(to, false).equals("1");
            if (isBlockedInNormal || isBlockedInSecret)
                return true;
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        MyLog.d(TAG, "onStartCommand: servicetest");
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        MyLog.d(TAG, "onBind: servicetest");
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MyLog.e(TAG, "filetest@ servicetest eonCreate");
        sessionManager = SessionManager.getInstance(MessageService.this);
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        NotificationUtil.getInstance().init(this);
        CoreController.setCallback(this);
        if (uniqueCurrentID == null || uniqueCurrentID.equalsIgnoreCase("")) {
            MyLog.d(TAG, "Service Stoped" + uniqueCurrentID);
            stopSelf();
        }

        //Check background service is running
        MyLog.e(TAG, "onCreate" + "register");

        //if already register unregister old one
        /*if (EventBus.getDefault().isRegistered(this)) {
            MyLog.d(TAG, "onCreate: servicetest eventbus already registered");
            EventBus.getDefault().unregister(this);
        }
        */

        registerDateTimeChangeReceiver();

        if (manager == null) {
            manager = SocketManager.getInstance();
            manager.init(MessageService.this, null);
        }
        if (!manager.isConnected()) {
            manager.connect();
        }
        manager.setCallBack(callBack);

        objectReceiver = new MesssageObjectReceiver(MessageService.this);
        offlineMsgHandler = new OfflineMessageHandler(MessageService.this);
        service = this;
        mcontext = MessageService.this;


        dispatcher = new ActiveSocketDispatcher();
        incomCallBroadcastHandler = new Handler(Looper.getMainLooper());
        if (!manager.isConnected()) {
            manager.connect();
        }
        getcontactname = new Getcontactname(MessageService.this);
        session = new Session(MessageService.this);
        uploadDownloadManager = new FileUploadDownloadManager(MessageService.this);
        statusDB = new DatabaseClassForDB(this);

        incomingMsg = new IncomingMessage(MessageService.this);
        incomingMsg.setCallback(MessageService.this);

        // For getting db instance if not created
        CoreController.getDBInstance(MessageService.this);


        userInfoSession = new UserInfoSession(MessageService.this);
        groupInfoSession = new GroupInfoSession(MessageService.this);
        objectReceiver.setEventBusObject(EventBus.getDefault());

    }

    private void registerDateTimeChangeReceiver() {
        mDateReceiver = new DateChangeBroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                super.onReceive(context, intent);
                getServerTime();
            }
        };
        IntentFilter timeFilter = new IntentFilter();
//        timeFilter.addAction(Intent.ACTION_TIME_TICK);
        timeFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        timeFilter.addAction(Intent.ACTION_TIME_CHANGED);
        timeFilter.addAction(Intent.ACTION_DATE_CHANGED);
        try {
            registerReceiver(mDateReceiver, timeFilter);
        } catch (Exception e) {
            Log.d(TAG, "registerDateTimeChangeReceiver: ", e);
        }
    }

    private void attachDeviceWithAccount() {

        String deviceId = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

        String loginKey = sessionManager.getLoginKey();
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        try {
            JSONObject msgObj = new JSONObject();
            msgObj.put("from", uniqueCurrentID);
            msgObj.put("DeviceId", deviceId);
            msgObj.put("login_key", loginKey);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_UPDATE_MOBILE_LOGIN_NOTIFICATION);
            event.setMessageObject(msgObj);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void onSocketConnect() {
        //    if (!Constants.IS_ENCRYPTION_ENABLED) {
        boolean isLoginKeySent = sessionManager.isLoginKeySent();
        MyLog.e(TAG, "isLoginKeySent" + isLoginKeySent);
        boolean isValidDevice = sessionManager.isValidDevice();
        MyLog.e(TAG, "isValidDevice" + isValidDevice);
        /*if (sessionManager != null && !isLoginKeySent && !isValidDevice) {
            attachDeviceWithAccount();
        }*/


        if (sessionManager != null && sessionManager.getlogin() && sessionManager.isLoginKeySent() && sessionManager.isValidDevice()) {
            validateDeviceWithAccount();
        }
        //  }
        //  if (!Constants.IS_ENCRYPTION_ENABLED) {
        //Check if device token is notempty

        String securityToken = SessionManager.getInstance(getApplicationContext()).getSecurityToken();

        if (sessionManager != null && sessionManager.isValidDevice()) {
            createUser();
        }
        //      }
        session.setconnect_disconnectevent(true);

    }

    private void onSocketDisconnect() {
        try {
            if (session == null) {
                session = new Session(MessageService.this);
            }
            isFileUploadCalled = false;
            FileUploadDownloadManager.isFileUploading = false;
            session.setconnect_disconnectevent(false);
            sessionManager.setSocketDisconnectedTS(Calendar.getInstance().getTimeInMillis());
            setUploadFileConnectionOffline();
        } catch (Exception e) {
            MyLog.e(TAG, "onSocketDisconnect: ", e);
        }

    }

    private void performBackgroundGroupInfo(final String responseStr) {
        final Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    loadGroupListDetails(responseStr);
                } catch (Exception e) {
                    Log.e(TAG, "run: ", e);
                }
            }
        };
        t.start();

    }

    private void changesecuritytoken(String data) {

        try {
            //     MyLog.d(TAG, "EVENT_GET_SECRET_KEYS: ");
            //   String data = response[0].toString();
            JSONObject object = new JSONObject(data);
            String publicKey = "", privateKey = "";
            if (object.has("public_key")) {
                publicKey = object.getString("public_key");
                privateKey = object.getString("private_key");
                SessionManager.getInstance(this).setPublicEncryptionKey(publicKey);
                SessionManager.getInstance(this).setPrivateEncryptionKey(privateKey);
            }


        } catch (Exception e) {
            //    MyLog.e(TAG, "onSuccessListener: ", e);
        }
        //   MyLog.e(TAG, "EVENT_GET_SECRET_KEYS");
    }

    private void updateSecurityCode(String data) {
        try {
            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
            String userId = "";
            String userToken = "";
            JSONObject object = new JSONObject(data);
            // from this event EVENT_CHANGE_SECURITY_CODE use "to"
            if (object.has("to"))
                userId = object.getString("to");
                //from this event EVENT_NEW_ROOM_CONNECTION use "convId"
            else
                userId = object.getString("convId");
            userToken = object.getString("security_code");
            if (userId != null && !userId.isEmpty() && userToken != null && !userToken.isEmpty())
                contactDB_sqlite.updateSecurityToken_(userId, userToken);

        } catch (Exception e) {
            MyLog.e(TAG, "updateSecurityCode: ", e);
        }
    }

    private void onUserConnectedToServer(String data) {

        MyLog.d("onUserConnectedToServer", "OfflineMsgTest data:" + data);


        String Chat_type = "";
        try {

            String Update_Option = "";
            //   MyLog.e("getMessageHistory", "String" + data);
            String securityToken = SessionManager.getInstance(mcontext).getSecurityTokenHash();
            String messageAfterDecrypt = CryptLibDecryption.decrypt(securityToken, data);
            //  MyLog.e(TAG, "<<<getMessageHistory: after decrypt: " + messageAfterDecrypt);
            //    final JSONObject object = new JSONObject(messageAfterDecrypt);
            try {
                JSONObject object = new JSONObject(messageAfterDecrypt);
                if (object.has("last_clear_timestamp")) {
                    String latestClearedTimeStamp = object.getString("last_clear_timestamp");
                    String prevClearedTimeStamp = sessionManager.getLastClearTimeStampForMsgs();
                    if (!prevClearedTimeStamp.equals(latestClearedTimeStamp)) {
                        sessionManager.setLastClearTimeStampForMsgs(latestClearedTimeStamp);
                        clearAllMsgs(this);
                        dataCleared = true;
                    }
                }
            } catch (Exception e) {
                MyLog.e(TAG, "onUserConnectedToServer: ", e);
            }
            try {
                JSONObject objects = new JSONObject(messageAfterDecrypt);

                if (objects.has("chat_type")) {

                    Chat_type = objects.getString("chat_type");
                    MyLog.d(TAG, "onUserConnectedToServer: OfflineMsgTest chatType : " + Chat_type);
                    if (Chat_type.equalsIgnoreCase("single")) {

                        if (objects.has("is_new_update")) {

                            Update_Option = objects.getString("is_new_update");
                            if (Update_Option.equalsIgnoreCase("1")) {

                                Intent i = new Intent();
                                i.setAction("com.setupdate.dialog");
                                sendBroadcast(i);

                            }
                        }

                        FileUploadDownloadManager.isFileUploading = false;
                        fetchOfflineMsgsAndHistory();
                    } else {

                    }
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "onUserConnectedToServer: OfflineMsgTest ", e);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "onUserConnectedToServer: OfflineMsgTest ", e);
        }
    }

    private void loadReplyMessageDetails(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);
            String err = object.getString("err");

            if (err.equals("0")) {
                JSONObject dataObj = object.getJSONObject("data");
                String from = dataObj.getString("from");
                String to = dataObj.getString("to");
                String requestMsgId = dataObj.getString("requestMsgId");
                String chatType = dataObj.getString("type");

                String docId = from + "-" + to;
                if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                    docId = docId + "-g";
                } else {
                    String secretType = dataObj.getString("secret_type");
                    if (secretType.equalsIgnoreCase("yes")) {
                        docId = docId + "-secret";
                    }
                }

                MessageDbController dbController = CoreController.getDBInstance(this);
                MessageItemChat msgItem = dbController.getParticularMessage(requestMsgId);
                if (msgItem != null) {
                    JSONObject replyObj = object.getJSONObject("chatDetails");
                    msgItem = incomingMsg.getReplyDetailsByObject(object, replyObj, msgItem);
                    if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_SINGLE) && docId.contains("-secret")) {
                        chatType = MessageFactory.CHAT_TYPE_SECRET;
                    }
                    dbController.updateChatMessage(msgItem, chatType);
                }
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadMessageAckMessage(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);
            MyLog.e(TAG, "loadMessageAckMessage" + object);
            //cHECK eRROR CODE {"err":1,"msg":"Invalid Convid Id"}
            if (object.has("err")) {
                String err = object.getString("err");
                if (Integer.valueOf(err) == 1) {
                    return;
                }
            }
            String from = object.getString("from");
            if (from.equalsIgnoreCase(uniqueCurrentID)) {
                String to = object.getString("to");
                String chatId = object.getString("doc_id");
                String status = object.getString("status");
                String timeStamp = "" + System.currentTimeMillis();
                if (object.has("currenttime"))
                    timeStamp = object.getString("currenttime");
                String secretType = object.getString("secret_type");

                String docId = from + "-" + to;
                try {
                    String[] splitIds = chatId.split("-");
                    String id = splitIds[2];
                    String msgId = docId + "-" + id;

                    if (secretType.equalsIgnoreCase("yes")) {
                        docId = docId + "-secret";
                    }

                    MessageDbController db = CoreController.getDBInstance(this);
                    Log.d(TAG, "updateChatMessageFrom1 ");
                    db.updateChatMessage(docId, msgId, status, timeStamp, false);
                } catch (Exception e) {
                    MyLog.e(TAG, "loadMessageAckMessage: ", e);
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void updateBlockUserStatus(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();


        try {
            JSONObject object = new JSONObject(data);

            String status = object.getString("status");
            String from = object.getString("from");
            String to = object.getString("to");

            boolean isSecretChat = false;
            if (object.has("secret_type")) {
                String secretType = object.getString("secret_type");
                if (secretType.equalsIgnoreCase("yes")) {
                    isSecretChat = true;
                }
            }

            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
            if (uniqueCurrentID.equalsIgnoreCase(from)) {
                contactDB_sqlite.updateBlockedStatus(to, status, isSecretChat);

            } else if (uniqueCurrentID.equalsIgnoreCase(to)) {
                // update current user blocked from opponent user
                contactDB_sqlite.updateBlockedMineStatus(from, status, isSecretChat);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadRemoveCallLog(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();


        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");

            if (from.equalsIgnoreCase(uniqueCurrentID)) {
                MessageDbController db = CoreController.getDBInstance(this);

                JSONArray arrCalls = object.getJSONArray("recordIds");
                for (int i = 0; i < arrCalls.length(); i++) {
                    String callId = arrCalls.getJSONObject(i).getString("docId");
                    db.deleteCallLog(callId);
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadRemoveAllCallLogs(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");

            if (from.equalsIgnoreCase(uniqueCurrentID)) {
                MessageDbController db = CoreController.getDBInstance(this);
                db.deleteAllCallLogs();
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadAdminSettingsData(String data) {
        try {
            JSONObject object = new JSONObject(data);
            Log.e(TAG, "loadAdminSettingsData" + object);
            if (object.has("contactus_email_address")) {
                String contactUsEmailId = object.getString("contactus_email_address");
                sessionManager.setContactUsEMailId(contactUsEmailId);
            }

            if (object.has("chat_lock")) {
                String chatLock = object.getString("chat_lock");
                sessionManager.setLockChatEnabled(chatLock);
            }

            if (object.has("secret_chat")) {
                String secretChat = object.getString("secret_chat");
                sessionManager.setSecretChatEnabled(secretChat);
            }

            if (object.has("file_size")) {
                String fileSize = object.getString("file_size");
                sessionManager.setFileUploadMaxSize(fileSize);
            }
            if (object.has("is_encryption_available")) {
                String is_encryption_available = object.getString("is_encryption_available");
                if (Integer.parseInt(is_encryption_available) == 1) {

                    SessionManager.getInstance(mcontext).setIsEncryptionEnabled(true);
                } else if (Integer.parseInt(is_encryption_available) == 0) {

                    SessionManager.getInstance(mcontext).setIsEncryptionEnabled(false);
                }
            } else {
                SessionManager.getInstance(mcontext).setIsEncryptionEnabled(false);
            }
            if (object.has("file_count")) {
                String fileCount = object.getString("file_count");
                sessionManager.setFileUploadMaxCount(fileCount);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "loadAdminSettingsData: ", e);
            MyLog.e(TAG, "loadAdminSettingsData: json data : " + data);
        }
    }

    private void saveConvId(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);
            String err = object.getString("err");

            if (err.equals("0")) {
                String from = object.getString("from");
                String convId = object.getString("convId");

                if (from.equalsIgnoreCase(uniqueCurrentID) && !convId.equals("")) {
                    String to = object.getString("to");

                    String docId = from + "-" + to;

                    if (object.has("chat_type")) {
                        String chatType = object.getString("chat_type");
                        if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_SECRET)) {
                            docId = docId + "-secret";
                        }
                    }

                    userInfoSession.updateChatConvId(docId, to, convId);
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void resumeFileUpload(String data) {

        try {
            MyLog.d(TAG, "resumeFileUpload: uploadVideoChatFile");

            JSONObject object = new JSONObject(data);

            String msgId = object.getString("id");

            MessageDbController dbController = CoreController.getDBInstance(mcontext);

            dbController.fileuploadupdateMessage(msgId, "uploading", object);

            uploadDownloadManager.resumeFileUpload(EventBus.getDefault(), object);


        } catch (Exception e) {

        }

    }

    private void retryFileUpload() {
        if (!isFileUploadCalled && !FileUploadDownloadManager.isFileUploading) {
            long disconnectedTS = sessionManager.getSocketDisconnectedTS();
            long disconnectedTimeDiff = Calendar.getInstance().getTimeInMillis() - disconnectedTS;
            long delay = 1000;
            Log.d(TAG, "startFileUpload retryfileupload diff: " + disconnectedTimeDiff);
            if (disconnectedTimeDiff < 30000) {
                delay = 3000;
            }

            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    uploadDownloadManager.uploadPendingFiles(EventBus.getDefault());
                    uploadDownloadManager.downloadPendingFiles(EventBus.getDefault());
                    uploadDownloadManager.uploadUserPicPendingFiles(EventBus.getDefault());

                    offlineMsgHandler.sendOfflineStarredMessages();
                    offlineMsgHandler.sendOfflineDeletedMessages();
                    offlineMsgHandler.sendOfflineMessages();
                    isFileUploadCalled = false;
                }
            }, delay);
        }
        isFileUploadCalled = true;
    }

    public boolean isRunning(Context ctx) {
        ActivityManager activityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (ctx.getPackageName().equalsIgnoreCase(task.baseActivity.getPackageName()))
                return true;
        }

        return false;
    }

    private void setUploadFileConnectionOffline() {
        try {
            // Setting up socket connection destroy for re-try upload and download.
            uploadDownloadManager.setUploadConnectionOffline();
            uploadDownloadManager.setDownloadConnectionOffline();
            //cHECK aCTIVITY IS ALIVE
            if (isRunning(mcontext)) {
                if (!manager.isConnected()) {
                    manager.connect();
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "setUploadFileConnectionOffline: ", e);
        }
    }

    private void loadAppSettingsData(String data) {
        MyLog.e(TAG, "loadAppSettingsData" + data);
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        String from = null;
        try {
            JSONObject object = new JSONObject(data);

            //  String from = object.getString("from");
            if (object.has("from")) {

                from = object.getString("from");
            }
            if (from != null)
                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    String strTime = object.getString("server_time");

                    if (object.has("email")) {
                        String email = object.getString("email");
                        sessionManager.setUserEmailId(email);
                    }

                    if (object.has("recovery_email")) {
                        String recoveryEmail = object.getString("recovery_email");
                        sessionManager.setRecoveryEMailId(recoveryEmail);
                    }

                    if (object.has("recovery_phone")) {
                        String recoveryPhone = object.getString("recovery_phone");
                        sessionManager.setRecoveryPhoneNo(recoveryPhone);
                    }

                    if (object.has("verify_email")) {
                        String verifyStatus = object.getString("verify_email");
                        sessionManager.setChatLockEmailIdVerifyStatus(verifyStatus);
                    }

                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);

                    JSONArray arrBlockedChats = object.getJSONArray("blockedChat");
                    for (int i = 0; i < arrBlockedChats.length(); i++) {
                        JSONObject valObj = arrBlockedChats.getJSONObject(i);

                        String toUserId = valObj.getString("toUserId");
                        String convId = valObj.getString("convId");
                        String toUserMsisdn = valObj.getString("toUserMsisdn");
                        String blockedStatus = valObj.getString("is_blocked");

                        String docId = uniqueCurrentID + "-" + toUserId;
                        boolean isSecretChat = false;

                        if (valObj.has("secret_type")) {
                            String secretType = valObj.getString("secret_type");
                            if (secretType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_SECRET)) {
                                docId = docId + "-secret";
                                isSecretChat = true;
                            }
                        }

                        userInfoSession.updateChatConvId(docId, toUserId, convId);
                        userInfoSession.updateUserMsisdn(toUserId, toUserMsisdn);

                        getUserDetails(toUserId);

                        if (blockedStatus.equals("1")) {
                            contactDB_sqlite.updateBlockedStatus(toUserId, blockedStatus, isSecretChat);
                        }

                    }

                    JSONArray arrLockedChats = object.getJSONArray("ChatLock");
                    MessageDbController db = CoreController.getDBInstance(this);

                    for (int i = 0; i < arrLockedChats.length(); i++) {
                        JSONObject valObj = arrLockedChats.getJSONObject(i);

                        String chatType = valObj.getString("chat_type");
                        String convId = valObj.getString("convId");
                        String password = valObj.getString("password");

                        String docId;
                        String toUserId;

                        if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_SINGLE)) {
                            chatType = MessageFactory.CHAT_TYPE_SINGLE;
                            String toUserMsisdn = valObj.getString("toUserMsisdn");
                            toUserId = valObj.getString("toUserId");

                            docId = uniqueCurrentID + "-" + toUserId;
                            userInfoSession.updateChatConvId(docId, toUserId, convId);
                            userInfoSession.updateUserMsisdn(toUserId, toUserMsisdn);
                        } else {
                            toUserId = convId;
                            chatType = MessageFactory.CHAT_TYPE_GROUP;
                            docId = uniqueCurrentID + "-" + convId + "-g";
                        }

                        db.updateChatLockData(toUserId, docId, "1", password, chatType);
                    }
                    if (object.has("secretChat")) {
                        JSONArray arrSecretChat = object.getJSONArray("secretChat");

                        for (int i = 0; i < arrSecretChat.length(); i++) {
                            JSONObject valObj = arrSecretChat.getJSONObject(i);

                            String toUserId = valObj.getString("toUserId");
                            String toUserMsisdn = valObj.getString("toUserMsisdn");
                            String convId = valObj.getString("convId");
                            String msgId = uniqueCurrentID.concat("-").concat(toUserId).concat("-").concat(
                                    Calendar.getInstance().getTimeInMillis() + "");
                            try {
                                String timer = valObj.getString("incognito_timer");
                                String timerMode = valObj.getString("incognito_timer_mode");
                                String createdBy = valObj.getString("incognito_user");

                                contactDB_sqlite.updateSecretMessageTimer(toUserId, timer, createdBy, msgId);
//                        contactsDB.updateSecretMessageTimer(toUserId, timer, createdBy, msgId);
                            } catch (JSONException e) {
                                MyLog.e(TAG, "", e);
                            }

                            String docId = uniqueCurrentID + "-" + toUserId + "-" + MessageFactory.CHAT_TYPE_SECRET;
                            userInfoSession.updateChatConvId(docId, toUserId, convId);
                            userInfoSession.updateUserMsisdn(toUserId, toUserMsisdn);
                        }
                    }
                    if (object.has("clearchatNotify")) {
                        JSONArray arrSecretChat = object.getJSONArray("clearchatNotify");
                        for (int i = 0; i < arrSecretChat.length(); i++) {
                            JSONObject valObj = arrSecretChat.getJSONObject(i);
                            String mNotifyType = valObj.getString("notifyType");
                            String convId = valObj.getString("convId");
                            String lastId = valObj.getString("lastId");
                            String _id = valObj.getString("_id");
                            String opponent = valObj.getString("opponent");

                            if (mNotifyType.equalsIgnoreCase("delete")) {
                                String mMessageId = uniqueCurrentID + "-" + opponent + "-" + lastId;
                                MyLog.e(TAG, "mMessageId" + mMessageId);
                                String[] arrIds = mMessageId.split("-");
                                String chatId = arrIds[0] + "-" + arrIds[1];
                                MyLog.e(TAG, "chatId" + chatId);
                                long mLong = 0L;
                                mLong = Long.parseLong(lastId);
                                db.clearAllSingleChatMessageTimeStamp(mLong, mMessageId, chatId);
                            } else {

                            }
                        }
                    }
                    Long serverTime = Long.parseLong(strTime);
                    MyLog.e(TAG, "serverTime" + serverTime);
                    Long deviceTime = Calendar.getInstance().getTimeInMillis();
                    MyLog.e(TAG, "deviceTime" + deviceTime);
                    Long timeDiff = deviceTime - serverTime;
                    MyLog.e(TAG, "timeDiff" + timeDiff);
                    sessionManager.setServerTimeDifference(serverTime, timeDiff);
                    sessionManager.setIsAppSettingsReceived(true);
                }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void checkDrivePush() {
        if (!Chatbackup.isChatBackUpPage) {
            String backUpDuration = sessionManager.getBackUpDuration();
            long currentTime = Calendar.getInstance().getTimeInMillis() / 1000; // convert to seconds
            long lastBackUpAt = sessionManager.getBackUpTS() / 1000; // convert to seconds
            long backUpTimer = 0;  // For avoid auto back-up (never, only user tap) options

            switch (backUpDuration) {
                case BACK_UP_DAILY:
                    backUpTimer = 24 * 60 * 60;
                    break;

                case BACK_UP_WEEKLY:
                    backUpTimer = 7 * 24 * 60 * 60;
                    break;

                case BACK_UP_MONTHLY:
                    backUpTimer = 30 * 24 * 60 * 60;
                    break;
            }

            long backUpTimeDiff = currentTime - lastBackUpAt;

            if (backUpTimer > 0 && backUpTimeDiff > backUpTimer) {
                uploadToDrive();
            }

        }
    }

    private void uploadToDrive() {
        boolean canStart = false;
        long currentTime = Calendar.getInstance().getTimeInMillis();

        Intent svcIntent = new Intent(MessageService.this, GDBackUpService.class);

        if (!GDBackUpService.isServiceStarted) {
            canStart = true;
        } else {
            long gdSvcStartedAt = sessionManager.getBackUpServiceStartedAt();
            long diff = currentTime - gdSvcStartedAt;

            if (diff > 15 * 60 * 1000) { // stop service if backup take more than 15 minutes
                stopService(svcIntent);
                canStart = true;
            }
        }

        if (canStart) {
            // Marshmallow+
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
                Intent restartServiceIntent = new Intent(getApplicationContext(), GDBackUpService.class);
                restartServiceIntent.setPackage(getPackageName());
                PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
                AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                if (alarmService != null) {
                    alarmService.set(
                            AlarmManager.ELAPSED_REALTIME,
                            SystemClock.elapsedRealtime() + 500,
                            restartServicePendingIntent);
                }
            } else {
                //below Marshmallow{
                startService(svcIntent);
            }
        }
    }

    public void clearnotify() {
        //   if (!isRunning(mcontext)) {
        NotificationUtil.clearNotificationData();
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.cancelAll();
        ShortcutBadgeManager shortcutBadgeManager = new ShortcutBadgeManager(mcontext);
        shortcutBadgeManager.clearBadgeCount();
        try {
            ShortcutBadger.applyCount(mcontext, 0);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
        //   }
    }

    private void loadBackUpUpdateMessage(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");

            if (from.equalsIgnoreCase(uniqueCurrentID)) {

                JSONObject driveObj = object.getJSONObject("drive_settings");

                if (driveObj.length() > 0) {
                    long fileSize = driveObj.getLong("FileSize");
                    long createdTs = driveObj.getLong("CreatedTs");
                    String backUpGmailId = driveObj.getString("BackUpGmailId");
                    String backUpOver = driveObj.getString("BackUpOver");
                    String backUpDuration = driveObj.getString("BackUpDuration");

                    if (driveObj.has("FileName") && driveObj.has("DriveId")) {
                        String fileName = driveObj.getString("FileName");
                        String driveId = driveObj.getString("DriveId");
                        sessionManager.setBackUpDriveFileName(fileName);
                        sessionManager.setBackUpDriveFileId(driveId);
                    }

                    sessionManager.setBackUpMailAccount(backUpGmailId);
                    sessionManager.setBackUpSize(fileSize);
                    sessionManager.setBackUpTS(createdTs);
                    sessionManager.setBackUpOver(backUpOver);
                    sessionManager.setBackUpDuration(backUpDuration);
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadofflinestatus(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject objects = new JSONObject(data);
            String mErr = "";

            Log.e(TAG, "loadofflinestatus" + objects);
            if (objects.has("err")) {
                mErr = objects.getString("err");
                if (Integer.valueOf(mErr) == 0) {
                    if (objects.has("status")) {
                        JSONArray array = objects.optJSONArray("status");
                        for (int i = 0; i < array.length(); i++) {

                            try {
                                JSONObject msgObj = array.getJSONObject(i);
                                String doc_id = msgObj.getString("doc_id");
                                CoreController.getStatusDB(mcontext).deleteStatus(doc_id);
                            } catch (Exception e) {

                            }
                        }
                    }
                }

            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadmark(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        try {
            JSONObject objects = new JSONObject(data);
            Log.e(TAG, "loadmark" + objects);
            String from = "", convId = "", type = "";
            int status = 0;
            if (objects.has("from")) {
                from = objects.getString("from");
            }

            if (objects.has("convId")) {
                convId = objects.getString("convId");
            }

            if (objects.has("type")) {
                type = objects.getString("type");
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadmute(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        MyLog.d("MuteResponse_1", data);
        try {
            JSONObject objects = new JSONObject(data);
            String from = objects.getString("from");

            if (from.equalsIgnoreCase(uniqueCurrentID)) {
                String type = objects.getString("type");

                String option = "";
                int status = objects.getInt("status");
                if (objects.has("option")) {
                    option = objects.getString("option");
                }

                String notifyStatus = "";
                if (objects.has("notify_status")) {
                    notifyStatus = objects.getString("notify_status");
                }

                String to = "";
                String secretType = "no";

                if (type.equalsIgnoreCase("single")) {
                    String convId = "";
                    if (objects.has("convId")) {
                        convId = objects.getString("convId");
                    }

                    if (objects.has("secret_type")) {
                        secretType = objects.getString("secret_type");
                    } else {
                        secretType = "no";
                    }
                    boolean isSecretChat = secretType.equalsIgnoreCase("yes");

                    if (objects.has("to")) {
                        to = objects.getString("to");
                    } else if (!userInfoSession.getReceiverIdByConvId(convId).equals("")) {
                        to = userInfoSession.getReceiverIdByConvId(convId);
                    } else {
                        getToUserIdByConvId(convId, secretType);
                    }
                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
                    contactDB_sqlite.updateMuteStatus(uniqueCurrentID, to, convId, status, option, notifyStatus, isSecretChat);
                } else {
                    String convId = objects.getString("convId");
                    to = convId;
                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
                    contactDB_sqlite.updateMuteStatus(uniqueCurrentID, null, convId, status, option, notifyStatus, false);
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void getToUserIdByConvId(String convId, String secretType) {
        try {
            JSONObject object = new JSONObject();
            object.put("from", uniqueCurrentID);
            object.put("convId", convId);
            object.put("secret_type", secretType);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_CONV_SETTINGS);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadConvSettingsMessage(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");
            if (from.equalsIgnoreCase(uniqueCurrentID)) {
                String to = object.getString("to_user_id");
                String convId = object.getString("convId");
                String secretType = object.getString("secret_type");

                String docId = from + "-" + to;
                if (secretType.equalsIgnoreCase("yes")) {
                    docId = docId + "-secret";
                }

                userInfoSession.updateChatConvId(docId, to, convId);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void responseforVerifyemail(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();


        try {
            JSONObject objects = new JSONObject(data);
            String err = objects.getString("err");
            if (err.equals("0")) {
                String from = objects.getString("from");

                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    String verifyStatus = objects.getString("verify_email");
                    sessionManager.setChatLockEmailIdVerifyStatus(verifyStatus);
                }
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void responseforeventchangemail(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject objects = new JSONObject(data);
            String err = objects.getString("err");
            if (err.equals("0")) {
                String from = objects.getString("from");
                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    String email = objects.getString("email");
                    sessionManager.setUserEmailId(email);
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void saveRecoveryEmail(String data) {
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        try {
            JSONObject objects = new JSONObject(data);
            String err = objects.getString("err");
            if (err.equals("0")) {
                String from = objects.getString("from");
                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    String recoveryEmail = objects.getString("recovery_email");
                    SessionManager.getInstance(this).setRecoveryEMailId(recoveryEmail);
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }

    private void responseforeventRecoveryPhone(String data) {
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        try {
            JSONObject objects = new JSONObject(data);
            String err = objects.getString("err");
            if (err.equals("0")) {
                String from = objects.getString("from");
                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    String recoveryPhone = objects.getString("recovery_phone");
                    sessionManager.setRecoveryPhoneNo(recoveryPhone);
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadChatLockResponse(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject objects = new JSONObject(data);

            String err = objects.getString("err");
            String message = objects.getString("msg");
            String mode = objects.getString("mode");
            // TODO: 7/14/2017 get locked chat user ids on login
//            if (err.equals("0")) {

            String from = objects.getString("from");
            String password = objects.getString("password");
            String convId = objects.getString("convId");
            String status = objects.getString("status");
            String type = objects.getString("type");

            MessageDbController db = CoreController.getDBInstance(this);
            String receiverId = userInfoSession.getReceiverIdByConvId(convId);
            String docId, chatType;
            if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                chatType = MessageFactory.CHAT_TYPE_GROUP;
                docId = from.concat("-").concat(convId).concat("-g");
                receiverId = convId;
            } else {
                chatType = MessageFactory.CHAT_TYPE_SINGLE;
                docId = from.concat("-").concat(receiverId);
            }

            if (mode.equalsIgnoreCase("phone")) {
                password = objects.getString("mobile_password");
            } else {
                password = getEncryptPwd(password, convId);

                if (status.equalsIgnoreCase("1")) {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("from", from);
                        object.put("type", type);
                        object.put("convId", convId);
                        object.put("mode", "phone");
                        object.put("mobile_password", password);

                        SendMessageEvent postEvent = new SendMessageEvent();
                        postEvent.setEventName(SocketManager.EVENT_CHAT_LOCK_FROM_WEB);
                        postEvent.setMessageObject(object);
                        EventBus.getDefault().post(postEvent);
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                }
            }

            db.updateChatLockData(receiverId, docId, status, password, chatType);

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void loadToConvSettings(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);
            if (object.has("from")) {

                String from = object.getString("from");

                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    String status = object.getString("is_blocked");
                    String to = object.getString("to");

                    if (object.has("privacy")) {
                        updateToUserPrivacySettings(to, object);
                    }

                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
                    contactDB_sqlite.updateBlockedMineStatus(to, status, false);
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadUnSetDeviceTokenMessage(String data) {
        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");

            if (from.equalsIgnoreCase(uniqueCurrentID)) {
                notifyLogoutToServer();
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadDeleteAccountMessage(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");

            if (from.equalsIgnoreCase(uniqueCurrentID)) {
                performLogout();
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadRemovedAccountMessage(String data) {


        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);
            String userId = object.getString("_id");
            if (userId.equalsIgnoreCase(uniqueCurrentID)) {
                performLogout();
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void notifyLogoutToServer() {
        try {
            JSONObject logoutObj = new JSONObject();
            logoutObj.put("from", uniqueCurrentID);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_MOBILE_TO_WEB_LOGOUT);
            event.setMessageObject(logoutObj);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadOnlineStatus(String data) {
        MyLog.e(TAG, "loadOnlineStatus" + data);
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        try {
            JSONObject jsonObject = new JSONObject(data);
            String id = (String) jsonObject.get("_id");
            String status = String.valueOf(jsonObject.get("Status"));
            if (id.equalsIgnoreCase(uniqueCurrentID) && status.equals("0") && sessionManager.isScreenActivated()) {
                ChangeSetController.setChangeStatus("1");
            }
        } catch (JSONException e) {

        }
    }

    private void loadServerTimeMessage(String data) {
        try {
            MyLog.e("loadServerTimeMessage", "loadServerTimeMessage" + data);
//            JSONObject object = new JSONObject(data);
            //Check wthether it is string or jsonobject
            Object json = null;
            try {
                json = new JSONTokener(data).nextValue();
                if (json instanceof String) {
                    MyLog.e("loadServerTimeMessage", "String" + data);
                    MyLog.e("getMessageHistory", "String" + data);
                    String securityToken = SessionManager.getInstance(mcontext).getSecurityTokenHash();
                    String messageAfterDecrypt = CryptLibDecryption.decrypt(securityToken, data);
                    MyLog.e(TAG, "<<<getMessageHistory: after decrypt: " + messageAfterDecrypt);
                    JSONObject object = new JSONObject(messageAfterDecrypt);
                    String from = object.getString("from");
                    MyLog.e(TAG, "<<<getMessageHistory: " + from);

                    if (from.equalsIgnoreCase(uniqueCurrentID)) {
                        String strTime = object.getString("server_time");
                        MyLog.e(TAG, "serverTime" + strTime);

                        Long serverTime = Long.parseLong(strTime);

                        MyLog.e(TAG, "serverTime" + serverTime);
                        Long deviceTime = Calendar.getInstance().getTimeInMillis();

                        MyLog.e(TAG, "serverTime" + deviceTime);
                        Long timeDiff = deviceTime - serverTime;

                        MyLog.e(TAG, "serverTime" + timeDiff);
                        //  sessionManager.setServerTimeDifference(serverTime, timeDiff);
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadPrivacySetting(String event) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(event);
            String userid = object.getString("from");

            String statusVisibility = "";
            if (object.has("status")) {
                statusVisibility = object.getString("status");
            }

            String lastSeenVisibility = "";
            if (object.has("last_seen")) {
                lastSeenVisibility = object.getString("last_seen");
            }

            String profilePicVisibility = "";
            if (object.has("profile_photo")) {
                profilePicVisibility = object.getString("profile_photo");
            }

            if (userid.equalsIgnoreCase(uniqueCurrentID)) {
                sessionManager.setProfilePicVisibleTo(profilePicVisibility);
                sessionManager.setProfileStatusVisibleTo(statusVisibility);
                sessionManager.setLastSeenVisibleTo(lastSeenVisibility);
            } else {
                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(MessageService.this);
                String mineContactsStatus = "0";
                JSONArray contactUserList = object.getJSONArray("contactUserList");
                if (contactUserList != null && contactUserList.toString().contains(uniqueCurrentID)) {
                    mineContactsStatus = "1";
                }
                //new Db sqlite
                contactDB_sqlite.updateMyContactStatus(userid, mineContactsStatus);
                contactDB_sqlite.updateLastSeenVisibility(userid, getPrivacyStatus(lastSeenVisibility));
                contactDB_sqlite.updateProfilePicVisibility(userid, getPrivacyStatus(profilePicVisibility));
                contactDB_sqlite.updateProfileStatusVisibility(userid, getPrivacyStatus(statusVisibility));
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

    }

    private String getPrivacyStatus(String visibleTo) {
        switch (visibleTo.toLowerCase()) {

            case ContactDB_Sqlite.PRIVACY_TO_MY_CONTACTS:
                return ContactDB_Sqlite.PRIVACY_STATUS_MY_CONTACTS;

            case ContactDB_Sqlite.PRIVACY_TO_NOBODY:
                return ContactDB_Sqlite.PRIVACY_STATUS_NOBODY;

            default:
                return ContactDB_Sqlite.PRIVACY_STATUS_EVERYONE;
        }
    }

    private void fetchOfflineMsgsAndHistory() {
        boolean isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
        if (isMassChat) {
            getAppSettings();
        }
        getOfflineDeletedStatus();

        if (!sessionManager.isAppSettingsReceived()) {
            getAppSettings();
        }

        MyLog.d(TAG, "fetchOfflineMsgsAndHistory: 1 offlinegrouptest");
        if (sessionManager.isInitialGetGroupList()) {
            MyLog.d(TAG, "fetchOfflineMsgsAndHistory 2: offlinegrouptest OfflineMsgTest");

            SmarttyContactsService.bindContactService(this, true);
            getGroupList();
            checkDrivePush();

            //getSettings();
        } else {
            List<String> liveGroupIds = groupInfoSession.getGroupIdList();
            for (String groupId : liveGroupIds) {
                createGroup(groupId);
            }
        }
        retryFileUpload();
        getMessageHistory();
    }

    private void getMessageHistory() {
        try {
            // Avoid multiple hits in off-line message to server
            Calendar calendar = Calendar.getInstance();
            long currentTime = calendar.getTimeInMillis();
            long timeDiff = currentTime - session.getLastOfflineHistoryAt();
            MyLog.d(TAG, "getMessageHistory: offlinegrouptest " + timeDiff);
            if (timeDiff > MIN_GET_OFFLINE_MESSAGES_TIME) {
                MyLog.d(TAG, "getMessageHistory: offlinegrouptest min time diff satisfied");
                getStatusHistory(uniqueCurrentID);
                getHistory();
                getGroupHistory();
                getofflineGroupHistory();
                session.setLastOfflineHistoryAt(currentTime);
            } else {
                getStatusHistory(uniqueCurrentID);
            }

            //----------Delete Chat---------------
            sendSingleOffline();
            sendGroupOffline();


        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadMessageOfflineAcks(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();


        try {
            JSONObject object = new JSONObject(data);
            String err = object.getString("err");

            if (err.equals("0")) {

                String chatType = object.getString("type");
                JSONArray arrMessages = object.getJSONArray("messageDetails");
                if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_SINGLE)) {
                    //updateSingleMessageOfflineAcks(arrMessages);
                } else if (chatType.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                    //updateGroupMessageOfflineAcks(arrMessages);
                }
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }

    private void updateGroupMessageOfflineAcks(JSONArray arrMessages) {

        try {
            MessageDbController db = CoreController.getDBInstance(this);

            for (int i = 0; i < arrMessages.length(); i++) {
                try {
                    JSONObject msgObj = arrMessages.getJSONObject(i);

                    String from = msgObj.getString("from");
                    if (from.equalsIgnoreCase(uniqueCurrentID)) {
                        String msgId = msgObj.getString("msgId");
                        String chatId = msgObj.getString("doc_id");
                        String timeStamp = msgObj.getString("timestamp");

                        JSONArray arrMsgStatus = msgObj.getJSONArray("message_status");

                        for (int j = 0; j < arrMsgStatus.length(); j++) {
                            JSONObject statusObj = arrMsgStatus.getJSONObject(j);
                            String ackUserId = statusObj.getString("id");
                            String deliverStatus = statusObj.getString("status");
                            String deliverTime = statusObj.getString("time_to_deliever");
                            String readTime = statusObj.getString("time_to_seen");

                            String groupId = chatId.split("-")[1];
                            String docId = from.concat("-").concat(groupId).concat("-g");

                            if (deliverStatus.equalsIgnoreCase(MessageFactory.GROUP_MSG_READ_ACK)) {
                                db.updateGroupMessageStatus(docId, chatId, MessageFactory.GROUP_MSG_DELIVER_ACK,
                                        deliverTime, ackUserId, uniqueCurrentID);
                                db.updateGroupMessageStatus(docId, chatId, deliverStatus, readTime,
                                        ackUserId, uniqueCurrentID);
                            } else {
                                synchronized (this) {
                                    db.updateGroupMessageStatus(docId, chatId, deliverStatus, deliverTime,
                                            ackUserId, uniqueCurrentID);
                                }

                            }
                        }
                    }

                } catch (Exception e) {
                    MyLog.e(TAG, "updateGroupMessageOfflineAcks: ", e);
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

    }

    private void updateSingleMessageOfflineAcks(JSONArray arrMessages) {
        MessageDbController db = CoreController.getDBInstance(this);
        Log.d(TAG, "updateSingleMessageOfflineAcks: " + arrMessages);
        String deliverStatus = "";
        try {
            for (int i = 0; i < arrMessages.length(); i++) {
                JSONObject msgObj = arrMessages.getJSONObject(i);
                String deliverTime = null;
                String from = msgObj.getString("from");
                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    String to = msgObj.getString("to");
                    String msgId = msgObj.getString("msgId");
                    String chatId = msgObj.getString("doc_id");
                    String timeStamp = msgObj.getString("timestamp");
                    if (msgObj.has("time_to_deliever")) {
                        deliverStatus = msgObj.getString("message_status");
                    }
                    //Timetodeliver null
                    if (msgObj.has("time_to_deliever")) {
                        deliverTime = msgObj.getString("time_to_deliever");
                    }
                    String readTime = msgObj.getString("time_to_seen");
                    String docId = from.concat("-").concat(to);
                    if (deliverStatus.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ)) {
                        Log.d(TAG, "updateChatMessageFrom2 ");
                        db.updateChatMessage(docId, chatId, deliverStatus, deliverTime, true);
                        Log.d(TAG, "updateChatMessageFrom3 ");
                        db.updateChatMessage(docId, chatId, deliverStatus, readTime, true);
                    } else {
                        Log.d(TAG, "updateChatMessageFrom4 ");
                        db.updateChatMessage(docId, chatId, deliverStatus, deliverTime, true);
                    }
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadarchive(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);

            try {
                String from = object.getString("from");
                String convId = object.getString("convId");
                String type = object.getString("type");
                int status = object.getInt("status");

                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    String receiverId;
                    if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                        receiverId = convId;
                    } else {
                        receiverId = userInfoSession.getReceiverIdByConvId(convId);
                    }

                    if (!receiverId.equals("")) {
                        String docId = from.concat("-").concat(receiverId);
                        if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                            docId = docId.concat("-g");
                            if (status == 1) {
                                if (!session.getarchivegroup(uniqueCurrentID + "-" + receiverId + "-g")) {
                                    session.putarchivegroup(uniqueCurrentID + "-" + receiverId + "-g");
                                }
                            } else {
                                if (session.getarchivegroup(uniqueCurrentID + "-" + receiverId + "-g")) {
                                    session.removearchivegroup(uniqueCurrentID + "-" + receiverId + "-g");
                                }

                            }
                        } else {
                            if (status == 1) {
                                if (!session.getarchive(uniqueCurrentID + "-" + receiverId)) {
                                    session.putarchive(uniqueCurrentID + "-" + receiverId);
                                }

                            } else {
                                if (session.getarchive(uniqueCurrentID + "-" + receiverId)) {
                                    session.removearchive(uniqueCurrentID + "-" + receiverId);
                                }

                            }
                        }


                    }
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadClearChat(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);
            MyLog.e(TAG, "loadClearChat" + object);
            try {
                int star_status;
                Boolean starred = false;
                String from = object.getString("from");
                String convId = object.getString("convId");
                String type = object.getString("type");
                String lastId = "";
                String messageId = "";
                if (object.has("star_status")) {
                    star_status = object.getInt("star_status");
                    starred = star_status != 0;
                }


                if (object.has("lastId")) {
                    lastId = object.getString("lastId");
                }
                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    Log.e("from", "from equals" + from + "uniqueCurrentID" + uniqueCurrentID);

                    String receiverId;
                    if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                        receiverId = convId;
                    } else {
                        receiverId = userInfoSession.getReceiverIdByConvId(convId);
                    }

                    if (!receiverId.equals("")) {
                        String docId = from.concat("-").concat(receiverId);
                        if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                            docId = docId.concat("-g");
                        }
                        if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                            MessageDbController db = CoreController.getDBInstance(this);
                            if (starred) {
                                db.clearUnStarredMessage(docId, receiverId, MessageFactory.CHAT_TYPE_GROUP);
                            } else {
                                db.clearAllGroupChatMessage(docId, receiverId);
                            }
                        } else {
                            //   boolean isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
                            //  if (isMassChat) {
                            MessageDbController db = CoreController.getDBInstance(this);
                            long mLong = 0L;
                            //    mLong=Long.parseLong(lastId)+2550;
                            mLong = Long.parseLong(lastId);
                            if (object.has("MessageId")) {
                                messageId = object.getString("MessageId");
                                Log.e("messageId", "messageId" + messageId);
                                if (from.equalsIgnoreCase(uniqueCurrentID)) {

                                    if (starred) {
                                        //Check timestamp  and delete all values below it
                                        Log.e("equalsIgnoreCase", "equalsIgnoreCase" + messageId);
                                        //     Toast.makeText(mcontext, getString(R.string.chatclear), Toast.LENGTH_SHORT).show();
                                        showclearToast();
                                        db.clearAllUnStarredSingleChatMessagewithTime(mLong, messageId, MessageFactory.CHAT_TYPE_SINGLE);
                                        //   db.clearUnStarredMessage(docId, receiverId, MessageFactory.CHAT_TYPE_SINGLE);
                                    } else {
                                        //    db.clearAllSingleChatMessage(docId, receiverId);
                                        //working with time stamp
                                        //   db.clearAllSingleChatMessagewithTime((mLong),messageId);
                                        showclearToast();
                                        db.clearAllSingleChatMessagewithTimeMessage((mLong), messageId);

                                    }
                                } else {
                                    Log.e("not equalsIgnoreCase", "not equalsIgnoreCase" + messageId);

                                }

                            }
                        }
                    }
                } else {
                    Log.e("from", "not  equals" + from + "uniqueCurrentID" + uniqueCurrentID);
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void showclearToast() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mcontext, getString(R.string.chatclear), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void loadDeleteChat(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        MyLog.e(TAG, "loadDeleteChat" + data);
        try {
            JSONObject object = new JSONObject(data);
            MyLog.e(TAG, "loadDeleteChat" + object);
            try {
                String mMessageId = null;
                String from = object.getString("from");
                String convId = object.getString("convId");
                String type = object.getString("type");
                String lastId = object.getString("lastId");
                if (object.has("MessageId")) {
                    mMessageId = object.getString("MessageId");
                }

                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    String receiverId;
                    String chatType = MessageFactory.CHAT_TYPE_SINGLE;
                    if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                        receiverId = convId;
                        chatType = MessageFactory.CHAT_TYPE_GROUP;
                    } else {
                        receiverId = userInfoSession.getReceiverIdByConvId(convId);
                    }
                    boolean isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
                    if (isMassChat) {
                        //Add 2000
                        long mLong = 0L;
                        MyLog.e(TAG, "isMassChat" + isMassChat);
                        if (!receiverId.equals("")) {
                            String docId = from.concat("-").concat(receiverId);
                            if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                                docId = docId.concat("-g");
                            } else {
                                receiverId = userInfoSession.getReceiverIdByConvId(convId);
                                MyLog.e("receiverId", "receiverId" + receiverId);
                            }

                            //Clear data for single chat
                            MessageDbController db = CoreController.getDBInstance(this);
                            mLong = Long.parseLong(lastId);

                            // lastId=lastId+2000;
                            if (object.has("MessageId")) {
                                MyLog.e(TAG, "clearAllSingleChatMessageTimeStamp" + mLong);
                                MyLog.e(TAG, "mMessageId" + mMessageId);

                                String[] arrIds = mMessageId.split("-");
                                String chatId = arrIds[0] + "-" + arrIds[1];
                                MyLog.e(TAG, "chatId" + chatId);
                                db.clearAllSingleChatMessageTimeStamp(mLong, mMessageId, chatId);
                                MyLog.e(TAG, "clearAllSingleChatMessageTimeStamp");
                            }

                            ShortcutBadgeManager shortcutBadgeManager = new ShortcutBadgeManager(mcontext);
                            shortcutBadgeManager.clearBadgeCount();
                            try {
                                ShortcutBadger.applyCount(mcontext, 0);
                            } catch (Exception e) {
                                MyLog.e(TAG, "", e);
                            }
                        } else {
                            Log.e(TAG, "not receiverId" + receiverId);
                        }
                    } else {
                        MyLog.e(TAG, "isMassChat" + isMassChat);
                        if (!receiverId.equals("")) {
                            String docId = from.concat("-").concat(receiverId);
                            if (type.equalsIgnoreCase(MessageFactory.CHAT_TYPE_GROUP)) {
                                docId = docId.concat("-g");
                            }
                            MessageDbController db = CoreController.getDBInstance(this);
                            db.deleteChat(docId, chatType);
                        }
                    }

                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void changeMsgViewedStatus(String data) {
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");
            String mode = object.getString("mode");

            if (from.equalsIgnoreCase(uniqueCurrentID) && mode.equalsIgnoreCase("web")) {
                String to = object.getString("to");
                String chatType = object.getString("type");
                String convId = object.getString("convId");
                String id = object.getString("id");

                ShortcutBadgeManager shortcutBadgeMgnr = new ShortcutBadgeManager(MessageService.this);
                shortcutBadgeMgnr.removeMessageCount(convId, id);
                int totalCount = shortcutBadgeMgnr.getTotalCount();

                session.Removemark(to);

                // Badge working if supported devices
                try {
                    ShortcutBadger.applyCount(MessageService.this, totalCount);
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }

    private void fetchSecretKeys() {
        try {
            JSONObject fetchKeysObj = new JSONObject();
            fetchKeysObj.put("userId", SessionManager.getInstance(this).getCurrentUserID());
            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GET_SECRET_KEYS);
            event.setMessageObject(fetchKeysObj);
            EventBus.getDefault().post(event);
        } catch (Exception e) {
        }
    }

    private void performLogout() {
        MessageDbController db = CoreController.getDBInstance(this);
        db.deleteDatabase();

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        contactDB_sqlite.deleteDatabase();

        notifyLogoutToServer();
        ChangeSetController.setChangeStatus("0");
        SessionManager.getInstance(MessageService.this).logoutUser(false);
    }

    private void loadProfileNameChangeMessage(String response) {
        Log.e(TAG, "loadProfileNameChangeMessage" + response);
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();


        try {
            JSONObject object = new JSONObject(response);
            String err = object.getString("err");

            if (err.equals("0")) {
                String from = object.getString("from");

                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    String name = object.getString("name");

                    SessionManager.getInstance(MessageService.this).setnameOfCurrentUser(name);

                    if (getResources().getString(R.string.app_name).equalsIgnoreCase("TrueMobile")) {

                        String email = object.getString("email");
                        SessionManager.getInstance(MessageService.this).setEmail(email);

                    }
                }
            } else if (Integer.parseInt(err) == 1) {
                createUser();
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void loadMessageStatusUpdate(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        Log.e(TAG, "loadMessageStatusUpdate: " + data);
        try {
            JSONObject jsonObject = new JSONObject(data);
            String from = jsonObject.getString("from");
            String to = jsonObject.getString("to");
            String msgIds = jsonObject.getString("msgIds");
            String chatId = jsonObject.getString("doc_id");
            String status = jsonObject.getString("status");
            String secretType = jsonObject.getString("secret_type");
            String timeStamp = jsonObject.optString("currenttime");//todo currenttime issue

            //todo - check this timeStamp need or not
            if (timeStamp == null || timeStamp.isEmpty()) {
                timeStamp = "" + System.currentTimeMillis();
            }
            MessageDbController db = CoreController.getDBInstance(this);
            String docId = to + "-" + from;
            if (secretType.equalsIgnoreCase("yes")) {
                docId = docId + "-" + MessageFactory.CHAT_TYPE_SECRET;
            }
            if (status.equalsIgnoreCase(MessageFactory.DELIVERY_STATUS_READ)) {
                Log.d(TAG, "loadMessageStatusUpdate: tickMiss read");
                if (SessionManager.getInstance(MessageService.this).canSendReadReceipt()) {
                    Log.d(TAG, "loadMessageStatusUpdate: tickMiss2 read2");
                    Log.d(TAG, "updateChatMessageFrom5 ");
                    db.updateChatMessage(docId, chatId, status, timeStamp, true);
                }
            } else {
                Log.d(TAG, "updateChatMessageFrom6 ");
                db.updateChatMessage(docId, chatId, status, timeStamp, true);
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadProfileStatusChangeMessage(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();


        try {
            JSONObject object = new JSONObject(data);

            String err = object.getString("err");

            if (err.equalsIgnoreCase("0")) {
                String from = object.getString("from");

                String status = object.getString("status");

                byte[] decodeStatus = Base64.decode(status, Base64.DEFAULT);
                status = new String(decodeStatus, StandardCharsets.UTF_8);

                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    sessionManager.setcurrentUserstatus(status);
                    if (!statusDB.isAlreadyInsertedStatus(status)) {
                        statusDB.insetData(status);
                    }
                } else {
                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
                    contactDB_sqlite.updateMyContactStatus(from, status);
                    EventBus.getDefault().post(new StatusRefresh(from, status));
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadProfilePicChangeMessage(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        CoreActivity.hidepDialog();

        try {
            JSONObject objects = new JSONObject(data);
            String err = objects.getString("err");

            if (err.equalsIgnoreCase("0")) {
                String message = objects.getString("message");
                String from = objects.getString("from");
                String type = objects.getString("type");

                if (from.equalsIgnoreCase(uniqueCurrentID)
                        && type.equalsIgnoreCase("single")) {
                    if (objects.has("removePhoto")) {
                        String removePhoto = objects.getString("removePhoto");
                        if (removePhoto.equalsIgnoreCase("yes")) {
                            sessionManager.setUserProfilePic("");

                        } else {
                            String fileName = objects.getString("file");
                            String path = "";
                            if (fileName == null || fileName.isEmpty()) {
                                String imageName = objects.getString("ImageName");
                                path = Constants.USER_PROFILE_URL + imageName + "?id=" + Calendar.getInstance().getTimeInMillis();
                            } else {
                                path = objects.getString("file") + "?id=" + Calendar.getInstance().getTimeInMillis();
                            }
                            sessionManager.setUserProfilePic(AppUtils.getValidProfilePath(path));
                        }
                        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);

                        if (objects.has("profile_changed_time")) {
                            boolean isAvaialble = contactDB_sqlite.isUserAvailableInDB(from);
                            if (isAvaialble)
                                contactDB_sqlite.updateDpUpdatedTime(from, objects.getString("profile_changed_time"));
                        }

                    } else {
                        String path = objects.getString("file") + "?id=" + Calendar.getInstance().getTimeInMillis();
                        sessionManager.setUserProfilePic(AppUtils.getValidProfilePath(path));
                    }
                } else if (type.equalsIgnoreCase("single")) {
                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);


                    if (objects.has("profile_changed_time")) {
                        boolean isUserAvailable = contactDB_sqlite.isUserAvailableInDB(from);
                        if (isUserAvailable)
                            contactDB_sqlite.updateDpUpdatedTime(from, objects.getString("profile_changed_time"));
                    }
                }
            } else if (err.equalsIgnoreCase("0")) {

                Toast.makeText(this, R.string.imagefailed, Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }

    private void loadPhoneDataReceivedMessage(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);

            int err = object.getInt("err");
            if (err == 0) {
                MyLog.d("UploadProgress", data);
                uploadDownloadManager.startServerRequestFileUpload(EventBus.getDefault(), object);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadPhoneDownloadMessage(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);

            int err = object.getInt("err");
            if (err == 0) {
                object.put("bufferAt", -1); // For index from 0
                uploadDownloadManager.startServerRequestFileUpload(EventBus.getDefault(), object);
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void validateDeviceWithAccount() {
        try {
            MyLog.d(TAG, "multidevice validateDeviceWithAccount: ");
            JSONObject msgObj = new JSONObject();
            msgObj.put("from", uniqueCurrentID);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_CHECK_MOBILE_LOGIN_KEY);
            event.setMessageObject(msgObj);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "multidevice", e);
        }
    }

    private void loadCheckLoginKey(String data) {
        MyLog.e(TAG, "loadCheckLoginKey" + data);
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);

            String err = object.getString("err");
            if (err.equalsIgnoreCase("0")) {
                String msg = object.getString("msg");

                JSONObject apiObj = object.getJSONObject("apiMobileKeys");
                String deviceId = apiObj.getString("DeviceId");
                String loginKey = apiObj.getString("login_key");
                String timeStamp = apiObj.getString("timestamp");

                String deviceLoginKey = sessionManager.getLoginKey();
                MyLog.e(TAG, "loadCheckLoginKey" + loginKey);
                MyLog.e(TAG, "loadCheckLoginKey" + deviceLoginKey);

                if (!loginKey.equalsIgnoreCase(deviceLoginKey)) {
                    SharedPreference.getInstance().saveBool(mcontext, "validdevice", false);

                    sessionManager.setIsValidDevice(false);
                } else {
                    SharedPreference.getInstance().saveBool(mcontext, "validdevice", true);

                    getServerTime();
                }

            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void getServerTime() {
        try {
            JSONObject timeObj = new JSONObject();
            timeObj.put("from", uniqueCurrentID);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GET_SERVER_TIME);
            event.setMessageObject(timeObj);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadDeviceLoginMessage(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);

            String err = object.getString("err");
            if (err.equalsIgnoreCase("0")) {
                String msg = object.getString("msg");

                JSONObject apiObj = object.getJSONObject("apiMobileKeys");
                String deviceId = apiObj.getString("DeviceId");
                String loginKey = apiObj.getString("login_key");
                String timeStamp = apiObj.getString("timestamp");

                String settingsDeviceId = Settings.Secure.getString(getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                if (settingsDeviceId.equalsIgnoreCase(deviceId)) {
                    sessionManager.setLoginKeySent(true);
                    sessionManager.setIsValidDevice(true);
                    sessionManager.setLoginKey(loginKey);
                    getServerTime();
                } else {
                    sessionManager.setIsValidDevice(false);
                }
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void saveUserDetails(String data) {

        try {
            JSONObject object = new JSONObject(data);
            String userId = object.getString("id");
            String avatar = object.getString("avatar");
            String msisdn = object.getString("msisdn");
            String name = "";
            String status = "";
            if (object.has("Name")) {
                name = object.getString("Name");
            }
            if (object.has("Status")) {
                status = object.getString("Status");
                try {
                    byte[] nameBuffer = Base64.decode(status, Base64.DEFAULT);
                    status = new String(nameBuffer, StandardCharsets.UTF_8);
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            } else {
                status = getResources().getString(R.string.default_user_status);
            }

            String dpChangedTS = "0";
            if (object.has("profile_changed_time")) {
                dpChangedTS = object.getString("profile_changed_time");
            }

            if (userId.equalsIgnoreCase(uniqueCurrentID)) {
                JSONObject objPrivacy = object.getJSONObject("privacy");

                if (objPrivacy.has("status")) {
                    String statusVisibleTo = objPrivacy.getString("status");
                    sessionManager.setProfileStatusVisibleTo(statusVisibleTo);
                }

                if (objPrivacy.has("last_seen")) {
                    String lastSeenVisibleTo = objPrivacy.getString("last_seen");
                    sessionManager.setLastSeenVisibleTo(lastSeenVisibleTo);
                }

                if (objPrivacy.has("profile_photo")) {
                    String profilePhotoVisibleTo = objPrivacy.getString("profile_photo");
                    sessionManager.setProfilePicVisibleTo(profilePhotoVisibleTo);
                }

                sessionManager.setUserDetailsReceived(true);
            } else {
                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
                SmarttyContactModel contact = contactDB_sqlite.getUserOpponenetDetails(userId);
                if (contact == null) {
                    contact = new SmarttyContactModel();
                }
                contact.set_id(userId);
                contact.setStatus(status);
                contact.setAvatarImageUrl(avatar);
                contact.setMsisdn(msisdn);

                if (sessionManager.getContactSavedRevision() > contactDB_sqlite.getOpponenet_UserDetails_savedRevision(userId)) {
                    contact.setFirstName(name);
                }

                contactDB_sqlite.updateUserDetails(userId, contact);
                contactDB_sqlite.updateDpUpdatedTime(userId, dpChangedTS);

                updateToUserPrivacySettings(userId, object);

                if (receivedUserDetailsMap == null) {
                    receivedUserDetailsMap = new HashMap<>();
                }
                receivedUserDetailsMap.put(userId, Calendar.getInstance().getTimeInMillis());
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }

    private void saveUserDetailsOnBackground(final String data) {
        new Thread() {
            @Override
            public void run() {
                saveUserDetails(data);
            }
        }.start();

    }

    private void updateToUserPrivacySettings(String userId, JSONObject object) {
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);

        try {
            JSONObject objPrivacy = object.getJSONObject("privacy");

            if (objPrivacy.has("status")) {
                String statusVisibleTo = objPrivacy.getString("status").toLowerCase();
                switch (statusVisibleTo) {

                    case ContactDB_Sqlite.PRIVACY_TO_NOBODY:
                        contactDB_sqlite.updateProfileStatusVisibility(userId, ContactDB_Sqlite.PRIVACY_STATUS_NOBODY);
                        break;

                    case ContactDB_Sqlite.PRIVACY_TO_MY_CONTACTS:
                        contactDB_sqlite.updateProfileStatusVisibility(userId, ContactDB_Sqlite.PRIVACY_STATUS_MY_CONTACTS);
                        break;

                    default:
                        contactDB_sqlite.updateProfileStatusVisibility(userId, ContactDB_Sqlite.PRIVACY_STATUS_EVERYONE);
                        break;
                }
            }

            if (objPrivacy.has("last_seen")) {
                String lastSeenVisibleTo = objPrivacy.getString("last_seen");
                switch (lastSeenVisibleTo) {

                    case ContactDB_Sqlite.PRIVACY_TO_NOBODY:
                        contactDB_sqlite.updateLastSeenVisibility(userId, ContactDB_Sqlite.PRIVACY_STATUS_NOBODY);
                        break;

                    case ContactDB_Sqlite.PRIVACY_TO_MY_CONTACTS:
                        contactDB_sqlite.updateLastSeenVisibility(userId, ContactDB_Sqlite.PRIVACY_STATUS_MY_CONTACTS);
                        break;

                    default:
                        contactDB_sqlite.updateLastSeenVisibility(userId, ContactDB_Sqlite.PRIVACY_STATUS_EVERYONE);
                        break;
                }
            }

            if (objPrivacy.has("profile_photo")) {
                String profilePhotoVisibleTo = objPrivacy.getString("profile_photo");
                switch (profilePhotoVisibleTo) {

                    case ContactDB_Sqlite.PRIVACY_TO_NOBODY:
                        contactDB_sqlite.updateProfilePicVisibility(userId, ContactDB_Sqlite.PRIVACY_STATUS_NOBODY);
                        break;

                    case ContactDB_Sqlite.PRIVACY_TO_MY_CONTACTS:
                        contactDB_sqlite.updateProfilePicVisibility(userId, ContactDB_Sqlite.PRIVACY_STATUS_MY_CONTACTS);
                        break;

                    default:
                        contactDB_sqlite.updateProfilePicVisibility(userId, ContactDB_Sqlite.PRIVACY_STATUS_EVERYONE);
                        break;
                }
            }

            if (object.has("is_contact_user")) {
                String myContactStatus = object.getString("is_contact_user");
                contactDB_sqlite.updateMyContactStatus(userId, myContactStatus);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void performSettingsConnect(JSONObject object) {
        MyLog.d("GetmobileCalled", object.toString());

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            String from = object.getString("requestUser");

            if (from.equalsIgnoreCase(uniqueCurrentID)) {

                int send = object.getInt("send");

                if (send == 0) {
                    object.put("send", 1);

                    if (IncomingCallActivity.isStarted || CallMessage.isAlreadyCallClick) {
                        object.put("call_connect", MessageFactory.CALL_IN_RINGING);
                    } else if (!IncomingCallActivity.isStarted && !CallsActivity.isStarted) {
                        object.put("call_connect", MessageFactory.CALL_IN_FREE);
                    } else {
                        object.put("call_connect", MessageFactory.CALL_IN_WAITING);
                    }

                    SendMessageEvent event = new SendMessageEvent();
                    event.setEventName(SocketManager.EVENT_GET_MOBILE_SETTINGS);
                    event.setMessageObject(object);
                    EventBus.getDefault().post(event);
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadMessageRes(Object[] respose) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            if (respose == null)
                return;
            JSONObject objects = new JSONObject(respose[0].toString());
            MyLog.e(TAG, "loadMessageRes" + objects);
            int errorState = objects.getInt("err");
            if (errorState == 0) {
                int delivery = (int) objects.get("deliver");
                String id = objects.getString("id");
                MyLog.e("loadMessageRes", "loadMessageRes" + objects);

                JSONObject msgData = objects.getJSONObject("data");
                String recordId = msgData.getString("recordId");
                String convId = msgData.getString("convId");
                String fromUserId = msgData.getString("from");
                String toUserId = msgData.getString("to");
                String type = msgData.getString("type");
                String timeStamp = msgData.getString("timestamp");

                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(MessageService.this);

                if (type.equalsIgnoreCase(MessageFactory.text + "")) {
                    String message = msgData.getString("message");
                    long messagelength = session.getsentmessagelength() + message.length();
                    session.putsentmessagelength(messagelength);
                    int sentmesagecount = session.getsentmessagecount();
                    sentmesagecount = sentmesagecount + 1;
                    session.putsentmessagecount(sentmesagecount);
                } else if (type.equalsIgnoreCase(MessageFactory.picture + "") || type.equalsIgnoreCase(MessageFactory.audio + "") || type.equalsIgnoreCase(MessageFactory.video + "")) {
                    long filesize = Long.parseLong(msgData.getString("filesize"));
                    filesize = session.getsentmedialength() + filesize;
                    session.putsentmedialength(filesize);
                }

                sessionManager.setIsFirstMessage(toUserId, true);

                MessageDbController db = CoreController.getDBInstance(this);

                String secretType = msgData.getString("secret_type");
                String chat_id = (String) objects.get("doc_id");
                String[] ids = chat_id.split("-");
                String doc_id = ids[0] + "-" + ids[1];

                String chatType = MessageFactory.CHAT_TYPE_SINGLE;
                if (secretType.equalsIgnoreCase("yes")) {
                    doc_id = doc_id.concat("-").concat(MessageFactory.CHAT_TYPE_SECRET);
                    chatType = MessageFactory.CHAT_TYPE_SECRET;
                } else {
                    contactDB_sqlite.updateFrequentContact(uniqueCurrentID, convId, timeStamp);
                }
                //Check chat type is single update msg id


                MessageItemChat item = db.updateChatMessage(doc_id, chat_id, "" + delivery,
                        recordId, convId, timeStamp);


                db.deleteSendNewMessage(chat_id);

                if (!userInfoSession.hasChatConvId(doc_id)) {
                    userInfoSession.updateChatConvId(doc_id, ids[1], convId);
                }

                // Add message to local DB from Web
                if (item == null && fromUserId.equalsIgnoreCase(uniqueCurrentID)) {
                    item = incomingMsg.loadSingleMessageFromWeb(objects);
                    if (item != null) {
                        db.updateChatMessage(item, chatType);
                    }
                } else {

                }
            } /*else if (errorState == 1) {
                SessionManager.getInstance(MessageService.this).logoutUser();
            }*/

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void setGroupAck(JSONObject object) {
        try {
            String msgId = object.getString("id");
            String groupId = object.getString("groupId");
            sendGroupACK(groupId, msgId, uniqueCurrentID);
        } catch (Exception e) {
            Log.e(TAG, "setGroupAck: ", e);
        }
    }
/*

    // Handle Group Event response
    private void handleGroupResponse(Object[] response) {
        MyLog.e(TAG,"handleGroupResponse"+response);
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject objects = new JSONObject(response[0].toString());
          //  MyLog.e(TAG,"handleGroupResponse"+objects);
//Form JSONARRAY
            String groupAction = objects.getString("groupType");

            String error = objects.getString("err");

            if (error.equalsIgnoreCase("0")) {


                if (objects.has("from")) {
                    String from = objects.getString("from");
                    if (!from.equalsIgnoreCase(uniqueCurrentID)) {
                        //getUserDetails(from);
                    }
                }

                if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EVENT_GROUP_MESSAGE)) {

                    storeGroupMsgInDataBase(response);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_JOIN_NEW_GROUP)) {
                    storeGroupMsgInDataBase(response);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_NEW_GROUP)) {
                    addNewGroupInDataBase(response);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_ACK_GROUP_MESSAGE)) {
                    updateGroupMsgStatus(response);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EXIT_GROUP)) {
                    performExitGroup(response);
                } else if (groupAction.equals(SocketManager.ACTION_CHANGE_GROUP_NAME)) {
                    performChangeGroupName(response);
                } else if (groupAction.equals(SocketManager.ACTION_CHANGE_GROUP_DP)) {
                    performGroupChangeDp(response);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_DELETE_GROUP_MEMBER)) {
                    performDeleteMemberMessage(response);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_MAKE_GROUP_ADMIN)) {
                    performAddAdminMessage(response);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_ADD_GROUP_MEMBER)) {
                    performAddMemberMessage(response);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EVENT_GROUP_MSG_DELETE)) {
                    deleteGroupMessage(response);
                } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EVENT_GROUP_OFFLINE)) {
              //  MyLog.e(TAG,"ACTION_EVENT_GROUP_OFFLINE")
                    getGroupOffline(response);
                }

            } else if (error.equalsIgnoreCase("1")) {

                String message = objects.getString("msg");

                MyLog.d("CheckEventConnection" + " " + message);

                String messageid = objects.getString("doc_id");

                MessageDbController db = CoreController.getDBInstance(mcontext);

                JSONObject ob = db.FirstTimeUploadObjectGet(messageid);

            */
/*    if(ob!=null && ob.length()>0){

                    uploadDownloadManager.startFileUpload(EventBus.getDefault(),ob);
                }*//*


            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }
*/

    private void storeGroupMsgInDataBase(JSONObject objects, String mType) {

        try {

            //   JSONObject objects = //new JSONObject(response[0].toString());
            if (Integer.parseInt(mType) == 23) {
                setGroupAck(objects);
                return;
            }
            String to;
            if (objects.has("to")) {
                to = objects.getString("to");
            } else {
                to = objects.getString("groupId");
            }
            Log.d(TAG, "storeGroupMsgInDataBase{}: ");
            if (objects.has("id")) {
                String msgId = objects.getString("id");
                if (messageIds.contains(msgId)) {
                    return;
                }
                messageIds.add(msgId);
                Log.d(TAG, "storeGroupMsgInDataBasemsgid{}: " + msgId);
            }
            String docId = uniqueCurrentID.concat("-").concat(to).concat("-g");
            objectReceiver.storeGroupMsgInDataBase(objects, mType);
            if (objects.has("payload")) {

                String stat = "", pwd = "";
                ChatLockPojo lockPojo = getChatLockdetailfromDB(docId, MessageFactory.CHAT_TYPE_GROUP);
                if (sessionManager.getLockChatEnabled().equals("1") && lockPojo != null) {
                    stat = lockPojo.getStatus();
                    pwd = lockPojo.getPassword();
                }

                if (!stat.equalsIgnoreCase("1")) {
                    if (!ChatPageActivity.isChatPage) {
                        boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                        if (is_gossip) {

                            if (Build.VERSION.SDK_INT >= 27) {
                                NotificationUtil.getInstance().CustomshowNotificationWithReply(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, false);
                            } else {
                                NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, false);

                            }
                        } else {
                            NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, false);

                        }

                    } else {
                        if (!session.gettoid().equalsIgnoreCase(docId)) {

                            boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                            if (is_gossip) {
                                if (Build.VERSION.SDK_INT >= 27) {

                                    NotificationUtil.getInstance().CustomshowNotificationWithReply(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, false);
                                } else {

                                    NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, false);
                                }
                            } else {
                                NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, false);
                            }
                        }

                    }
                } else if (stat.equals("1")) {
                    boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                    if (is_gossip) {
                        if (Build.VERSION.SDK_INT >= 27) {

                            NotificationUtil.getInstance().CustomshowNotificationWithReply(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, true);
                        } else {
                            NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, true);
                        }
                    } else {
                        NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, true);
                    }
                }
                //-------------Delete Chat----------------
                int group_type, is_deleted;
                String new_msgId, groupId;
                if (objects.has("groupType")) {
                    group_type = objects.getInt("groupType");
                    if (group_type == 9) {
                        if (objects.has("is_deleted_everyone")) {
                            is_deleted = objects.getInt("is_deleted_everyone");
                            if (is_deleted == 1) {
                                try {
                                    MessageDbController db = CoreController.getDBInstance(this);
                                    String fromId = objects.getString("from");

                                    if (!fromId.equalsIgnoreCase(uniqueCurrentID)) {
                                        String chat_id = (String) objects.get("toDocOId");
                                        String[] ids = chat_id.split("-");

                                        groupId = objects.getString("groupId");
                                        new_msgId = uniqueCurrentID + "-g-" + ids[1] + "-g-" + ids[3];
                                        String groupAndMsgId = ids[1] + "-g-" + ids[3];
                                        if (ChatPageActivity.Chat_Activity == null) {
                                            db.deleteSingleMessage(groupAndMsgId, new_msgId, "group", "other");
                                            db.deleteChatListPage(groupAndMsgId, new_msgId, "group", "other");
                                        } else if (!ChatPageActivity.Activity_GroupId.equalsIgnoreCase(groupId)) {
                                            db.deleteSingleMessage(groupAndMsgId, new_msgId, "group", "other");
                                            db.deleteChatListPage(groupAndMsgId, new_msgId, "group", "other");
                                        }

                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    }
                }


            } else {
                //we have another event for raad group notification - so no need use it
                if (CoreController.isRaad) {
                    NotificationUtil.getInstance().init(this);
                    NotificationUtil.getInstance().newGroupNotification(this, objects);
                }
            }

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    // Store Group message to local DB
    private void storeGroupMsgInDataBase(Object[] response) {

        try {
            JSONObject objects = new JSONObject(response[0].toString());
            if (Integer.parseInt(objects.optString("type")) == 23) {
                /*String groupID = objects.getString("groupId");
                String chat_id = (String) objects.get("toDocId");
                String[] ids = chat_id.split("-");
                String msgId = uniqueCurrentID + "-g-" + ids[1] + "-g-" + ids[3];
                sendGroupACK(groupID,"1",msgId);*/
                String groupID = objects.optString("groupId");
                String id = objects.optString("id");
                sendGroupACK(groupID, id, uniqueCurrentID);
                return;
            }
            String to;
            if (objects.has("to")) {
                to = objects.getString("to");
            } else {
                to = objects.getString("groupId");
            }
            Log.d(TAG, "storeGroupMsgInDataBase[]: ");
            if (objects.has("id")) {
                String msgId = objects.getString("id");
                if (messageIds.contains(msgId)) {
                    return;
                }
                Log.d(TAG, "storeGroupMsgInDataBasemsgid[]: " + msgId);
                messageIds.add(msgId);
            }
            String docId = uniqueCurrentID.concat("-").concat(to).concat("-g");
            objectReceiver.storeGroupMsgInDataBase(response);
            if (objects.has("payload")) {

                String stat = "", pwd = "";
                ChatLockPojo lockPojo = getChatLockdetailfromDB(docId, MessageFactory.CHAT_TYPE_GROUP);
                if (sessionManager.getLockChatEnabled().equals("1") && lockPojo != null) {
                    stat = lockPojo.getStatus();
                    pwd = lockPojo.getPassword();
                }

                if (!stat.equalsIgnoreCase("1")) {
                    if (!ChatPageActivity.isChatPage) {
                        boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                        if (is_gossip) {
                            if (Build.VERSION.SDK_INT >= 27) {


                                NotificationUtil.getInstance().CustomshowNotificationWithReply(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, false);
                            } else {
                                NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, false);
                            }
                        } else {
                            NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, false);
                        }
                    } else {
                        if (!session.gettoid().equalsIgnoreCase(docId)) {
                            boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                            if (is_gossip) {
                                if (Build.VERSION.SDK_INT >= 27) {

                                    NotificationUtil.getInstance().CustomshowNotificationWithReply(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, false);
                                } else {
                                    NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, false);
                                }
                            } else {
                                NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, false);

                            }
                        }

                    }
                } else if (stat.equalsIgnoreCase("1")) {
                    //   NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, true);
                    boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                    if (is_gossip) {
                        if (Build.VERSION.SDK_INT >= 27) {


                            NotificationUtil.getInstance().CustomshowNotificationWithReply(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, true);
                        } else {
                            NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, true);

                        }
                    } else {
                        NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), false, false, true);
                    }

                }

                //-------------Delete Chat----------------
                int group_type, is_deleted;
                String new_msgId, groupId;
                if (objects.has("groupType")) {
                    group_type = objects.getInt("groupType");
                    if (group_type == 9) {
                        if (objects.has("is_deleted_everyone")) {
                            is_deleted = objects.getInt("is_deleted_everyone");
                            if (is_deleted == 1) {
                                try {
                                    MessageDbController db = CoreController.getDBInstance(this);
                                    String fromId = objects.getString("from");

                                    if (!fromId.equalsIgnoreCase(uniqueCurrentID)) {
                                        String chat_id = (String) objects.get("toDocOId");
                                        String[] ids = chat_id.split("-");

                                        groupId = objects.getString("groupId");
                                        new_msgId = uniqueCurrentID + "-g-" + ids[1] + "-g-" + ids[3];
                                        String groupAndMsgId = ids[1] + "-g-" + ids[3];
                                        if (ChatPageActivity.Chat_Activity == null) {
                                            db.deleteSingleMessage(groupAndMsgId, new_msgId, "group", "other");
                                            db.deleteChatListPage(groupAndMsgId, new_msgId, "group", "other");
                                        } else if (!ChatPageActivity.Activity_GroupId.equalsIgnoreCase(groupId)) {
                                            db.deleteSingleMessage(groupAndMsgId, new_msgId, "group", "other");
                                            db.deleteChatListPage(groupAndMsgId, new_msgId, "group", "other");
                                        }

                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    }
                }


            } else {
                //we have another event for raad group notification - so no need use it
                if (CoreController.isRaad) {
                    NotificationUtil.getInstance().init(this);
                    NotificationUtil.getInstance().newGroupNotification(this, objects);
                }
            }

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void logLargeString(String str) {
        if (str.length() > 3000) {
            MyLog.d("handleGroupResponse", str.substring(0, 3000));
            //logLargeString(str.substring(3000));
        } else {
            MyLog.d("handleGroupResponse", str); // continuation
        }
    }

    private void handleGroupResponse(Object[] response) {
        //  MyLog.e(TAG,"handleGroupResponse"+response);

        //Object json = response;
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject objects = new JSONObject(response[0].toString());
            MyLog.e(TAG, "handleGroupResponse" + objects);
//Form JSONARRAY
            try {
                if (objects.has("groupId")) {
                    String groupId = objects.getString("groupId");
                    ContactDB_Sqlite contactDB_sqlite = new ContactDB_Sqlite(mcontext);
                    contactDB_sqlite.updateFrequentGroups(uniqueCurrentID, groupId, "" + System.currentTimeMillis());
                }
            } catch (Exception e) {
                Log.e(TAG, "handleGroupResponse: ", e);
            }
            if (objects.has("result")) {
                JSONArray mResults = objects.optJSONArray("result");
                //   MyLog.e(TAG, "handleGroupResponse" + mResults);
                for (int i = 0; i < mResults.length(); i++) {

                    if (mResults.getJSONObject(i).has("type")) {
                        String type = mResults.getJSONObject(i).getString("type");//old one mResults.getJSONObject(i).getString("groupType")
                        if (mResults.getJSONObject(i).getString("groupType").equalsIgnoreCase(SocketManager.ACTION_EVENT_GROUP_MESSAGE)) {
//                            String from = objects.getString("from");
                            //   if (!from.equalsIgnoreCase(uniqueCurrentID)) {
                            //getUserDetails(from);
                            storeGroupMsgInDataBase(mResults.getJSONObject(i), type);
                            //   }
                        } else if (mResults.getJSONObject(i).getString("groupType").equalsIgnoreCase(SocketManager.ACTION_JOIN_NEW_GROUP)) {
                            storeGroupMsgInDataBase(mResults.getJSONObject(i), type);
                        }
                    }

                }
            }
            if (objects.has("groupType")) {
                String groupAction = objects.getString("groupType");

                String error = objects.getString("err");

                if (error.equalsIgnoreCase("0")) {


                    if (objects.has("from")) {
                        String from = objects.getString("from");
                        if (!from.equalsIgnoreCase(uniqueCurrentID)) {
                            //getUserDetails(from);
                        }
                    }

                    if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EVENT_GROUP_MESSAGE)) {
                        Log.e(TAG, "response" + response);
                        String from = objects.getString("from");
                        //    if (!from.equalsIgnoreCase(uniqueCurrentID)) {
                        //getUserDetails(from);
                        storeGroupMsgInDataBase(response);
                        //    }
                        //  storeGroupMsgInDataBase(response);
                    } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_JOIN_NEW_GROUP)) {
                        storeGroupMsgInDataBase(response);
                    } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_NEW_GROUP)) {
                        addNewGroupInDataBase(response);
                    } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_ACK_GROUP_MESSAGE)) {
                        updateGroupMsgStatus(response);
                    } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EXIT_GROUP)) {
                        performExitGroup(response);
                    } else if (groupAction.equals(SocketManager.ACTION_CHANGE_GROUP_NAME)) {
                        performChangeGroupName(response);
                    } else if (groupAction.equals(SocketManager.ACTION_CHANGE_GROUP_DP)) {
                        performGroupChangeDp(response);
                    } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_DELETE_GROUP_MEMBER)) {
                        performDeleteMemberMessage(response);
                    } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_MAKE_GROUP_ADMIN)) {
                        performAddAdminMessage(response);
                    } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_ADD_GROUP_MEMBER)) {
                        performAddMemberMessage(response);
                    } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EVENT_GROUP_MSG_DELETE)) {
                        deleteGroupMessage(response);
                    } else if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EVENT_GROUP_OFFLINE)) {
                        //  MyLog.e(TAG,"ACTION_EVENT_GROUP_OFFLINE")
                        getGroupOffline(response);
                    }

                } else if (error.equalsIgnoreCase("1")) {
                    Log.e(TAG, "objects" + objects);
                    String message = objects.optString("msg");

                    MyLog.d("CheckEventConnection" + " " + message);

                    //String messageid = objects.getString("doc_id");

                    // MessageDbController db = CoreController.getDBInstance(mcontext);

                    // JSONObject ob = db.FirstTimeUploadObjectGet(messageid);

            /*    if(ob!=null && ob.length()>0){

                    uploadDownloadManager.startFileUpload(EventBus.getDefault(),ob);
                }*/

                }
            }


        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }

    private void performAddAdminMessage(Object[] response) {
        try {
            JSONObject objects = new JSONObject(response[0].toString());
            objectReceiver.loadMakeAdminMessage(objects);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void performAddMemberMessage(Object[] response) {
        try {
            JSONObject objects = new JSONObject(response[0].toString());
            objectReceiver.loadAddMemberMessage(objects);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void performDeleteMemberMessage(Object[] response) {
        try {
            JSONObject objects = new JSONObject(response[0].toString());
            objectReceiver.loadDeleteMemberMessage(objects);

            try {
                String groupId = objects.getString("groupId");
                String removeId = objects.getString("removeId");

                if (removeId.equalsIgnoreCase(uniqueCurrentID)) {
                    removeUser(groupId);
                }

            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void performGroupChangeDp(Object[] response) {
        try {
            JSONObject objects = new JSONObject(response[0].toString());
            objectReceiver.loadGroupDpChangeMessage(objects);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

    }

    private void performChangeGroupName(Object[] response) {
        try {
            JSONObject objects = new JSONObject(response[0].toString());
//            String groupId = objects.getString("groupId");

            objectReceiver.loadChangeGroupNameMessage(objects);

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void performExitGroup(Object[] response) {
        try {
            JSONObject objects = new JSONObject(response[0].toString());
            String groupId = objects.getString("groupId");
            removeUser(groupId);
            MesssageObjectReceiver.deleteInvitedGroup(groupId, CoreController.mcontext);
            objectReceiver.loadExitMessage(objects);

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void updateGroupMsgStatus(Object[] response) {
        try {
            JSONObject objects = new JSONObject(response[0].toString());

            objectReceiver.updateGroupMsgStatus(objects);

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void addNewGroupInDataBase(Object[] response) {
        try {
            JSONObject objects = new JSONObject(response[0].toString());
            String mMessageId = objects.optString("id");
            if (messagegroupid.contains(mMessageId)) {
                return;
            }
            String from = objects.optString("from");
            if (CoreController.isRaad && from.equals(uniqueCurrentID)) {
                String groupId = objects.getString("groupId");
                if (groupId != null) {
                    NewGroupDetails_Model model = CoreController.getmNewGroup_db(CoreController.mcontext).getSingleData(groupId);
                    if (model != null)
                        addNewGroupInDb(model);
                }
                MesssageObjectReceiver.deleteInvitedGroup(groupId, CoreController.mcontext);
            }
            messagegroupid.add(mMessageId);
            objectReceiver.addNewGroupData(objects);

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    //raad
    public void addNewGroupInDb(NewGroupDetails_Model modelTest) {
       /* JSONObject object = new JSONObject();
        try {
            object.put("err", modelTest.getErr());
            object.put("id", modelTest.getId());
            object.put("createdBy", modelTest.getCreatedBy());
            object.put("groupId", modelTest.getGroupId());
            object.put("profilePic", modelTest.getProfilePic());
            object.put("groupName", modelTest.getGroupName());
            object.put("groupMembers", modelTest.getGroupMembers());
            object.put("admin", modelTest.getAdmin());
            object.put("timeStamp", modelTest.getTimestamp());
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        try {
            approveGroup(new JSONObject(modelTest.getJsonObj()));
        } catch (JSONException e) {
            MyLog.e(TAG, "onClick: ", e);
        }

        //for broadcast to hide home page icon
        EventBus.getDefault().post(new GroupInviteBraodCast());
    }

    public void denyGroup(String groupId) {
        try {
            MessageService.removeUser(groupId);
            performGroupExit(groupId);
            //NewGroupDetails_Model model=CoreController.getmNewGroup_db(CoreController.mcontext).getSingleData(groupId);
            //if(model!=null)
            //createGroupInLocalDB(new JSONObject(model.getJsonObj()));

        } catch (Exception e) {
            e.printStackTrace();
        }
        //for broadcast to hide home page icon
        EventBus.getDefault().post(new GroupInviteBraodCast());
    }

    public void performGroupExit(String mGroupId) {
        String mCurrentUserId = SessionManager.getInstance(CoreController.mcontext).getCurrentUserID();
        long ts = Calendar.getInstance().getTimeInMillis();
        String msgId = mCurrentUserId + "-" + mGroupId + "-g-" + ts;

        try {
            JSONObject exitObject = new JSONObject();
            exitObject.put("groupType", SocketManager.ACTION_EXIT_GROUP);
            exitObject.put("from", mCurrentUserId);
            exitObject.put("groupId", mGroupId);
            exitObject.put("id", ts);
            exitObject.put("toDocId", msgId);

            SendMessageEvent exitGroupEvent = new SendMessageEvent();
            exitGroupEvent.setEventName(SocketManager.EVENT_GROUP);
            exitGroupEvent.setMessageObject(exitObject);
            EventBus.getDefault().post(exitGroupEvent);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void createGroupInLocalDB(JSONObject object) {
        try {
            String mCurrentUserId = SessionManager.getInstance(CoreController.mcontext).getCurrentUserID();
            String err = object.getString("err");
            if (err.equals("0")) {
                String msgId = object.getString("id");
                String createdBy = object.getString("createdBy");
                String groupId = object.getString("groupId");
                //String groupAvatar = object.getString("profilePic");
                String groupName = object.getString("groupName");
                String groupMembers = object.getString("groupMembers");
                //String admin = object.getString("admin");
                String timeStamp = object.getString("timeStamp");


                String docId = mCurrentUserId + "-" + groupId + "-g";
                GroupEventInfoMessage createdMsg = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, CoreController.mcontext);
                MessageItemChat createdMsgItem = createdMsg.createMessageItem(MessageFactory.join_new_group, false,
                        null, MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, createdBy, null);
                String createdTS = String.valueOf(Long.parseLong(timeStamp) - 10);
                createdMsgItem.setTS(createdTS);
                String createdMsgId = String.valueOf(Long.parseLong(msgId) - 10);
                createdMsgItem.setMessageId(docId.concat("-").concat(createdMsgId));

                MessageDbController db = CoreController.getDBInstance(CoreController.mcontext);
                db.updateChatMessage(createdMsgItem, MessageFactory.CHAT_TYPE_GROUP);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "createGroupInLocalDB: ", e);
        }
    }

    private void loadDeleteMessage(Object[] data) {

        try {
            JSONObject objects = new JSONObject(data[0].toString());
            int errorState = objects.getInt("err");
            if (errorState == 0) {
                String fromId = objects.getString("from");

                if (fromId.equalsIgnoreCase(uniqueCurrentID)) {
                    String recordId = objects.getString("recordId");
                    String chat_id = objects.getString("doc_id");
                    String deleteStatus = objects.getString("status");

                    String[] ids = chat_id.split("-");

                    String docId;
                    String msgId, chatType = MessageFactory.CHAT_TYPE_SINGLE;
                    if (chat_id.contains("-g-")) {
                        docId = fromId + "-" + ids[1] + "-g";
                        msgId = docId + "-" + ids[3];
                        chatType = MessageFactory.CHAT_TYPE_GROUP;
                    } else {
                        if (fromId.equalsIgnoreCase(ids[0])) {
                            docId = ids[0] + "-" + ids[1];
                        } else {
                            docId = ids[1] + "-" + ids[0];
                        }
                        msgId = docId + "-" + ids[2];
                    }

                    if (deleteStatus.equalsIgnoreCase("1")) {
                        MessageDbController db = CoreController.getDBInstance(this);
                        db.deleteChatMessage(docId, msgId, chatType);
                        db.deleteTempDeletedMessage(recordId, chatType);
                    }
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void loadStarredMessage(Object[] data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject objects = new JSONObject(data[0].toString());
            int errorState = objects.getInt("err");
            if (errorState == 0) {
                String fromId = objects.getString("from");
                if (fromId.equalsIgnoreCase(uniqueCurrentID)) {
                    String chat_id = (String) objects.get("doc_id");
                    String[] ids = chat_id.split("-");

                    String docId;
                    String chatType = MessageFactory.CHAT_TYPE_SINGLE;
                    if (fromId.equalsIgnoreCase(ids[0])) {
                        docId = ids[0] + "-" + ids[1];
                    } else {
                        docId = ids[1] + "-" + ids[0];
                    }

                    if (chat_id.contains("-g-")) {
                        docId = docId + "-g";
                        chatType = MessageFactory.CHAT_TYPE_GROUP;
                    }

                    String starred = objects.getString("status");
                    /*String msgId;
                    if(chat_id.contains("-g-")) {
                        msgId = docId + "-" + ids[3];
                    } else {
                        msgId = docId + "-" + ids[2];
                    }*/
                    String recordId = objects.getString("recordId");

                    MessageDbController db = CoreController.getDBInstance(this);
                    db.updateStarredMessage(chat_id, starred, chatType);
                    db.deleteTempStarredMessage(uniqueCurrentID, recordId);
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void storeInDataBase(Object[] response) {
        try {
            MyLog.d(TAG, "storeInDataBase: OfflineMsgTest " + response[0]);
            JSONObject object = new JSONObject(response[0].toString());

            if (object.has("result")) {
                MyLog.d(TAG, "storeInDataBase: OfflineMsgTest has result array");
                JSONArray array = object.getJSONArray("result");
                for (int i = 0; i < array.length(); i++) {
                    MyLog.d(TAG, "storeInDataBase: OfflineMsgTest for loop");
                    JSONObject offlineMsg = array.getJSONObject(i);
                    MyLog.d(TAG, "storeInDataBase: OfflineMsgTest " + offlineMsg);
                    loadMessage(offlineMsg);
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "OfflineMsgTest", e);
            //for Non-Encryption apps
            try {
                JSONObject object = (JSONObject) response[0];
                loadMessage(object);
            } catch (Exception ex) {
                MyLog.e(TAG, "storeInDataBase: OfflineMsgTest ", ex);
            }
        }
    }

    private void getSettings() {
        SendMessageEvent settingsEvent = new SendMessageEvent();

        settingsEvent.setMessageObject(new JSONObject());
        settingsEvent.setEventName(SocketManager.EVENT_GET_SETTINGS);
        EventBus.getDefault().post(settingsEvent);
    }

    /*  private void createUser() {
          if (!sessionManager.isAppSettingsReceived()) {
              getAppSettings();
          }

          SendMessageEvent messageEvent = new SendMessageEvent();
          messageEvent.setEventName(SocketManager.EVENT_CREATE_USER);
          JSONObject object = new JSONObject();
          try {
              object.put("_id", uniqueCurrentID);
              object.put("mode", "phone");
              object.put("chat_type", "single");
              if (AppUtils.isEncryptionEnabled(mcontext)) {
                  String securityToken = SessionManager.getInstance(getApplicationContext()).getSecurityToken();
                  MyLog.d("createUser", "securityToken: " + securityToken);
                  object.put("token", securityToken);
                  object.put("device", Constants.DEVICE);
              }
          } catch (JSONException e) {
              MyLog.e(TAG, "", e);
          }
          messageEvent.setMessageObject(object);
          EventBus.getDefault().post(messageEvent);


          if (sessionManager.isInitialGetGroupList()) {
              getGroupList();
          } else {
              List<String> liveGroupIds = groupInfoSession.getGroupIdList();
              for (String groupId : liveGroupIds) {
                  createGroup(groupId);
              }
          }
      }*/
    private void createUser() {
        String securityToken = SessionManager.getInstance(getApplicationContext()).getSecurityToken();

        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_CREATE_USER);
        JSONObject object = new JSONObject();
        try {
            object.put("_id", uniqueCurrentID);
            object.put("mode", "phone");
            object.put("chat_type", "single");
            object.put("device", Constants.DEVICE);

            MyLog.d("createUser", "securityToken: " + securityToken);
            object.put("token", securityToken);
            //object.put("device", Constants.DEVICE);
            MyLog.e(TAG, "createUser" + object);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        messageEvent.setMessageObject(object);
        //check if token is empty dont call it
        if (AppUtils.isEncryptionEnabled(mcontext)) {
            if (!AppUtils.isEmptyString(securityToken)) {
                EventBus.getDefault().post(messageEvent);
            } else {
                //if security token not available logout user
                performLogout();
            }
        } else {
            EventBus.getDefault().post(messageEvent);
        }

    }

    private void getAppSettings() {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_GET_APP_SETTINGS);

        try {
            JSONObject object = new JSONObject();
            object.put("from", uniqueCurrentID);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void getOfflineDeletedStatus() {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_STATUSDELETE);

        try {
            JSONObject object = new JSONObject();
            object.put("from", uniqueCurrentID);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void getGroupList() {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        MyLog.d(TAG, "getGroupList: OfflineMsgTest userId: " + uniqueCurrentID);
        if (uniqueCurrentID != null && !uniqueCurrentID.equalsIgnoreCase("")) {

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GET_GROUP_LIST);

            try {
                JSONObject object = new JSONObject();
                object.put("from", uniqueCurrentID);
                event.setMessageObject(object);
                EventBus.getDefault().post(event);
                MyLog.e(TAG, "app/getGroupList OfflineMsgTest" + object);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

    }

    private void loadGroupListDetails(String data) {

        uniqueCurrentID = SessionManager.getInstance(CoreController.mcontext).getCurrentUserID();
        if (groupInfoSession == null)
            groupInfoSession = new GroupInfoSession(CoreController.mcontext);
        MyLog.e("loadGroupListDetails", "GroupListResponse OfflineMsgTest" + "" + data);

        try {
            JSONObject object = new JSONObject(data);
            MessageDbController db = CoreController.getDBInstance(this);

            JSONArray arrGroups = object.getJSONArray("GroupDetails");
            for (int i = 0; i < arrGroups.length(); i++) {
                JSONObject valObj = arrGroups.getJSONObject(i);
                String groupId = valObj.getString("groupId");
                String isDeleted = valObj.getString("isDeleted");
                MyLog.d(TAG, "loadGroupListDetails: OfflineMsgTest groupId " + groupId);
                if (isDeleted.equalsIgnoreCase("0")) {
                    String groupDocId = uniqueCurrentID.concat("-").concat(groupId).concat("-g");

                    boolean hasInfo = groupInfoSession.hasGroupInfo(groupDocId);
                    MyLog.d(TAG, "loadGroupListDetails hasInfo: " + hasInfo);
                    if (!hasInfo) {
                        db.createNewGroupChatList(groupId, groupDocId);
                        getGroupDetails(groupId);
                    } else {
                        MyLog.d(TAG, "loadGroupListDetails: not hasInfo");
                        GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(groupDocId);
                        if (infoPojo.getGroupContactNames() == null) {
                            getGroupDetails(groupId);
                        }
                    }

                    createGroup(groupId);
                }
            }
            SessionManager.getInstance(CoreController.mcontext).setIsInitialGetGroupList();
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        EventBus.getDefault().post(new FirstTimeGroupRefreshed());
    }

    public void getGroupDetails(String groupId) {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_GROUP_DETAILS);

        try {
            JSONObject object = new JSONObject();
            object.put("from", uniqueCurrentID);
            object.put("convId", groupId);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void createGroup(String groupId) {
        //Log.d(TAG, "createGroup: "+groupId);
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_CREATE_USER);
        JSONObject object = new JSONObject();
        try {
            object.put("_id", groupId);
            object.put("mode", "phone");
            object.put("chat_type", "group");
            object.put("device", Constants.DEVICE);
            // if (AppUtils.isEncryptionEnabled(mcontext)) {
            if (mcontext == null) {
                mcontext = CoreController.mcontext;
            }
            String securityToken = SessionManager.getInstance(mcontext).getSecurityToken();
            MyLog.d("createUser", "securityToken: " + securityToken);
            object.put("token", securityToken);
            //object.put("device", Constants.DEVICE);
            //  }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        messageEvent.setMessageObject(object);
        EventBus.getDefault().post(messageEvent);

    }

    private void getHistory() {
        MyLog.d(TAG, "getHistory: OfflineMsgTest");
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_GET_MESSAGE);
        JSONObject object = new JSONObject();
        try {
            object.put("msg_to", uniqueCurrentID);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        messageEvent.setMessageObject(object);
        EventBus.getDefault().post(messageEvent);

        // Getting offline call history
        SendMessageEvent callEvent = new SendMessageEvent();
        callEvent.setEventName(SocketManager.EVENT_GET_OFFLINE_CALLS);
        JSONObject callObj = new JSONObject();
        try {
            callObj.put("to", uniqueCurrentID);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        callEvent.setMessageObject(callObj);
        EventBus.getDefault().post(callEvent);
    }

    public void getGroupHistory() {
        SendMessageEvent groupMsgEvent = new SendMessageEvent();
        groupMsgEvent.setEventName(SocketManager.EVENT_GROUP);
        try {
            JSONObject object = new JSONObject();
            object.put("groupType", SocketManager.ACTION_GET_GROUP_CHAT_HISTORY);
            object.put("from", uniqueCurrentID);
            groupMsgEvent.setMessageObject(object);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        EventBus.getDefault().post(groupMsgEvent);
    }

    public void getofflineGroupHistory() {
        SendMessageEvent groupMsgEvent = new SendMessageEvent();
        groupMsgEvent.setEventName(SocketManager.EVENT_GET_OFFLINE_GROUPMESSAGE);
        try {
            JSONObject object = new JSONObject();
            object.put("groupType", SocketManager.ACTION_GET_GROUP_CHAT_HISTORY);
            object.put("from", uniqueCurrentID);
            groupMsgEvent.setMessageObject(object);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        EventBus.getDefault().post(groupMsgEvent);
    }


    private void changeBadgeCount(String convId, String msgId) {
        ShortcutBadgeManager shortcutBadgeMgnr = new ShortcutBadgeManager(MessageService.this);
        shortcutBadgeMgnr.putMessageCount(convId, msgId);
        int totalCount = shortcutBadgeMgnr.getTotalCount();

        try {
            ShortcutBadger.applyCount(MessageService.this, totalCount);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    private String decodeString(String encoded) {
        byte[] dataDec = Base64.decode(encoded, Base64.DEFAULT);
        String decodedString = "";
        try {
            decodedString = new String(dataDec, StandardCharsets.UTF_8);
        } finally {
            return decodedString;
        }
    }

    public void loadMessage(JSONObject objects) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        MyLog.e(TAG, "loadMessage OfflineMsgTest: " + objects);
        try {
            MessageItemChat item = null;
            int normal_Offline = 0;
            String type = objects.getString("type");
//Check type 21 and check it is answered

            MyLog.d(TAG, "loadMessage: " + type);
            if (objects.has("is_deleted_everyone")) {
                normal_Offline = objects.getInt("is_deleted_everyone");
            }

            if (type.equalsIgnoreCase("" + MessageFactory.timer_change)) {
                loadSecretTimerChangeMessage(objects);
            } else {

                item = incomingMsg.loadSingleMessage(objects);
                String from = objects.getString("from");
                Log.e("isBlockedUser(from)", "isBlockedUser(from)" + isBlockedUser(from));
                if (isBlockedUser(from))
                    return;
                String to = objects.getString("to");
                String secretType = objects.getString("secret_type");
                String msgId = objects.getString("msgId");


                //String status = objects.getString("status");
                //String message_status = objects.getString("message_status");


              /*  if (objects.has("call_status")) {
                    String status = objects.getString("call_status");
                    if (Integer.parseInt(status) == MessageFactory.CALL_STATUS_REJECTED) {
                        if (!uniqueCurrentID.equals(from)) {
                            //Dont save Missed call if rejected in oppenet side
                            return;
                        }
                    }
                }*/
                MyLog.d(TAG, "loadMessageMS messageIds: " + messageIds);
                if (messageIds.contains(msgId)) {
                    return;
                }
                MyLog.d(TAG, "loadMessageMS msgId: " + msgId);
                messageIds.add(msgId);
//                String id = objects.getString("id");
                String convId = objects.getString("convId");
                String stat = "", pwd = "";
                String doc_id;
                if (objects.has("docId")) {
                    doc_id = objects.getString("docId");
                } else {
                    doc_id = objects.getString("doc_id");
                }
                String uniqueID = to + "-" + from;
                boolean isSecretMsg = false;
                if (secretType.equalsIgnoreCase("yes")) {
                    isSecretMsg = true;
                    sendAckToServer(from, doc_id, "" + msgId, true, convId);
                } else {
                    sendAckToServer(from, doc_id, "" + msgId, false, convId);
                }
                if (Integer.parseInt(type) == 21) {
                    if (!uniqueCurrentID.equals(from)) {
                        boolean isAnsweredToUser = SharedPreference.getInstance().getBool(this, "isAnsweredToUser");
                        if (objects.has("call_status")) {
                            String status = objects.getString("call_status");
                            if (Integer.parseInt(status) != 5) {
                                if (!isAnsweredToUser) {
                                    return;
                                }
                            }
                        }
                    }
                }
                if (Integer.parseInt(objects.optString("type")) == 23) {
                    String groupID = objects.optString("groupId");
                    String id = objects.optString("id");
                    sendGroupACK(groupID, id, uniqueCurrentID);
                    return;
                }


                getUserDetails(from);


                MessageDbController db = CoreController.getDBInstance(this);
                item.setMessageId(uniqueID + "-" + msgId);
                item.setIsSelf(false);
                item.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_DELIVERED);

                String chatType = MessageFactory.CHAT_TYPE_SINGLE;
                if (secretType.equalsIgnoreCase("yes")) {
                    chatType = MessageFactory.CHAT_TYPE_SECRET;
                    uniqueID = uniqueID + "-" + MessageFactory.CHAT_TYPE_SECRET;
                    String timer = objects.getString("incognito_timer");
                    String timerCreatedBy = objects.getString("incognito_user");
                    item.setSecretTimer(timer);
                    item.setSecretTimeCreatedBy(timerCreatedBy);


                    //Check secret chat is blocked

                    MyLog.e("isBlockedUser(from)", "isBlockedUser(from)" + isBlockedUser(from));

                   /* if (isBlockedUser(from)){
                        return ;
                    }
*/
                    //new dbsqlite
                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
                    contactDB_sqlite.updateSecretMessageTimer(to, timer, timerCreatedBy, item.getMessageId());
                }

                if (session.getarchivecount() != 0) {
                    if (session.getarchive(uniqueCurrentID + "-" + from))
                        session.removearchive(uniqueCurrentID + "-" + from);
                }

                if (objects.has("filename")) {
                    item.setChatFileServerPath(objects.getString("filename"));
                }

                db.updateChatMessage(item, chatType);
                changeBadgeCount(convId, msgId);

                ChatLockPojo lockPojo = getChatLockdetailfromDB(uniqueID, chatType);
                if (sessionManager.getLockChatEnabled().equals("1") && lockPojo != null) {
                    stat = lockPojo.getStatus();
                    pwd = lockPojo.getPassword();
                }

                if (type.equalsIgnoreCase("" + MessageFactory.missed_call)) {
                    //  showMissedCallNotification(from, item.getSenderMsisdn());
                }

                if (!ChatPageActivity.isChatPage && !SecretChatViewActivity.isSecretChatPage && !from.equalsIgnoreCase(uniqueCurrentID)) {
                    if (isSecretMsg) {
                        //    NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);
                        boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                        if (is_gossip) {
                            if (Build.VERSION.SDK_INT >= 27) {

                                NotificationUtil.getInstance().CustomshowNotificationWithReply(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);
                            } else {
                                NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);

                            }
                        } else {
                            NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);
                        }
                    } else if (!stat.equals("1")) {
                        boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                        if (is_gossip) {
                            if (Build.VERSION.SDK_INT >= 27) {

                                NotificationUtil.getInstance().CustomshowNotificationWithReply(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);
                            } else {
                                NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);

                            }
                        } else {
                            NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);
                        }

//                        NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);
                    } else if (stat.equals("1")) {
                        boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                        if (is_gossip) {
                            if (Build.VERSION.SDK_INT >= 27) {


                                NotificationUtil.getInstance().CustomshowNotificationWithReply(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, true);
                            } else {
                                NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, true);

                            }
                        } else {
                            NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, true);
                        }

                    }
                } else if (SecretChatViewActivity.isSecretChatPage) {
                    if (!from.equalsIgnoreCase(uniqueCurrentID)) {
                        if (!session.gettoid().equalsIgnoreCase(uniqueID)) {
//                            NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);
                            boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                            if (is_gossip) {
                                if (Build.VERSION.SDK_INT >= 27) {

                                    NotificationUtil.getInstance().CustomshowNotificationWithReply(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);
                                } else {
                                    NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);

                                }
                            } else {
                                NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);
                            }

                        }
                    }
                } else {
                    if (!from.equalsIgnoreCase(uniqueCurrentID)) {
                        if (!stat.equals("1")) {
                            if (!session.gettoid().equalsIgnoreCase(uniqueID)) {
//                                NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);

                                boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                                if (is_gossip) {
                                    if (Build.VERSION.SDK_INT >= 27) {

                                        NotificationUtil.getInstance().CustomshowNotificationWithReply(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);
                                    } else {
                                        NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);

                                    }
                                } else {
                                    NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, false);
                                }

                            }
                        } else if (stat.equals("1")) {
//                            NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, true);
                            boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                            if (is_gossip) {
                                if (Build.VERSION.SDK_INT >= 27) {

                                    NotificationUtil.getInstance().CustomshowNotificationWithReply(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, true);
                                } else {
                                    NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, true);

                                }
                            } else {
                                NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, objects, objects.getString("from"), objects.getString("type"), true, isSecretMsg, true);
                            }
                        }
                    }
                }
                if (!userInfoSession.hasChatConvId(uniqueID)) {
                    userInfoSession.updateChatConvId(uniqueID, from, convId);
                }

                AutoDownLoadUtils.getInstance().checkAndAutoDownload(this, item);


                //-------------Delete Chat-----------------

                if (normal_Offline == 1) {
                    String new_docId, new_msgId, new_type, new_recId, new_convId;
                    try {
                        String fromId = objects.getString("from");

                        if (!fromId.equalsIgnoreCase(uniqueCurrentID)) {
                            String chat_id = (String) objects.get("docId");
                            String[] ids = chat_id.split("-");

                            new_type = objects.getString("chat_type");
//                            new_recId = objects.getString("recordId");
//                            new_convId = objects.getString("convId");

                            if (new_type.equalsIgnoreCase("single")) {
                                new_docId = ids[1] + "-" + ids[0];
                                new_msgId = new_docId + "-" + ids[2];
                                db.deleteSingleMessage(new_docId, new_msgId, "single", "other");
                                db.deleteChatListPage(new_docId, new_msgId, "single", "other");
                            } else {
                                new_docId = ids[1] + "-g-" + ids[0];
                                new_msgId = uniqueCurrentID + "-g-" + ids[1] + "-g-" + ids[3];
                                String groupAndMsgId = ids[1] + "-g-" + ids[3];

                                db.deleteSingleMessage(groupAndMsgId, new_msgId, "group", "other");
                                db.deleteChatListPage(groupAndMsgId, new_msgId, "group", "other");
                            }


                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }


            }

        } catch (Exception e) {
            /*try{
                if(objects.has("message") ){
                    String errorMsg=objects.getString("message");
                    if(errorMsg.contains("Access is denied")){
                        createUser();
                    }
                }
            }
            catch (Exception ex){
                Log.e(TAG, "loadMessage: ",ex );
            }*/
            MyLog.e(TAG, "", e);
        }
    }

    private void loadSecretTimerChangeMessage(JSONObject objects) {
        try {
            String secretType = objects.getString("secret_type");
            if (secretType.equalsIgnoreCase("yes")) {
                String from = objects.getString("from");
                String to = objects.getString("to");
                String convId = objects.getString("convId");
                String recordId = objects.getString("recordId");
                String timer = objects.getString("incognito_timer");
                String timerMode = objects.getString("incognito_timer_mode");
                String toUserMsgId;
                if (objects.has("doc_id")) {
                    toUserMsgId = objects.getString("doc_id");
                } else {
                    toUserMsgId = objects.getString("docId");
                }
                String msgId = objects.getString("id");
                String timeStamp = objects.getString("timestamp");
                String fromMsisdn = objects.getString("ContactMsisdn");

                MessageItemChat item = new MessageItemChat();
                String docId;
                if (from.equalsIgnoreCase(uniqueCurrentID)) {
                    docId = from + "-" + to;
                    item.setReceiverID(to);
                } else {
                    docId = to + "-" + from;
                    item.setReceiverID(from);
                    sendAckToServer(from, toUserMsgId, msgId, true, convId);
                }

                item.setIsSelf(false);
                item.setIsDate(true);
                item.setConvId(convId);
                item.setRecordId(recordId);
                item.setSecretTimer(timer);
                item.setSecretTimeCreatedBy(from);
                item.setSecretTimerMode(timerMode);
                item.setMessageId(docId + "-" + msgId);
                item.setTS(timeStamp);
                item.setSenderMsisdn(fromMsisdn);
                item.setMessageType(MessageFactory.timer_change + "");
                item.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_DELIVERED);

                MessageDbController db = CoreController.getDBInstance(this);
                docId = docId + "-" + MessageFactory.CHAT_TYPE_SECRET;
                db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SECRET);


                //new dbsqlite
                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
                contactDB_sqlite.updateSecretMessageTimer(from, timer, uniqueCurrentID, item.getMessageId());

                if (!userInfoSession.hasChatConvId(docId)) {
                    userInfoSession.updateChatConvId(docId, from + "-" + MessageFactory.CHAT_TYPE_SECRET, convId);
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }


    public void getUserDetails(String userId) {
        try {
            JSONObject eventObj = new JSONObject();
            eventObj.put("userId", userId);
            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GET_USER_DETAILS);
            event.setMessageObject(eventObj);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void writeBufferToFile(JSONObject object) {

        try {
            int start = 0;
            MyLog.d(TAG, "FileDownloadManager>>:  receiveObj " + object);
            JSONObject Main_object = object;
            String fileName = object.getString("ImageName");
            String localPath = object.getString("LocalPath");
//            String localFileName = object.getString("LocalFileName");
            String end = object.getString("end");
            int bytesRead = object.getInt("bytesRead");
            int fileSize = object.getInt("filesize");
            String msgId = object.getString("MsgId");
            String Start = object.getString("start");

            if (Integer.valueOf(Start) == 0) {
                mWriteAudio.add(Start);

                //mWriteAudio.clear();

            }

            MyLog.d(TAG, "writeBufferToFile downloadtest bytesread: " + bytesRead);

            if (object.get("bufferData") instanceof byte[]) {

                byte[] buffer = (byte[]) object.get("bufferData");

                int len = buffer.length;

                MyLog.e("downloadtest", "Buffer Length" + "" + len);
                if (!mWriteAudio.contains(Start)) {
                    if (mWriteAudio.size() > 0)
                        Start = mWriteAudio.get(mWriteAudio.size() - 1);
                    start = Integer.parseInt(Start);
                } else {
                    start = Integer.parseInt(Start);
                }
                //    start = Integer.parseInt(Start);

                final int progress = (start * 100) / fileSize;

                MyLog.e("downloadtest", "Buffer Progress" + "" + progress);

                File file = new File(localPath);
                if (!file.exists()) {
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        MyLog.e(TAG, "", e);

                        MyLog.e("CheckEventConnection", "File Create Error");
                    }
                } else {

                    MyLog.e("CheckEventConnection", "File is Exist");
                }

                FileOutputStream outputStream = new FileOutputStream(file, true);
                outputStream.write(buffer);
                outputStream.close();

                uploadDownloadManager.VideoDownloadProgress(EventBus.getDefault(), Main_object);

                object.remove("bufferData");
                if (uploadDownloadManager.getDownloadProgress(msgId) < 0) {
                    String docId = object.getString("DocId");
                    MessageDbController db = CoreController.getDBInstance(this);
                    db.updateMessageDownloadStatus(docId, msgId, MessageFactory.DOWNLOAD_STATUS_DOWNLOADING, localPath);
                }
                uploadDownloadManager.setDownloadProgress(msgId, bytesRead, object);

                if (end.equalsIgnoreCase("1")) {
                    MyLog.d(TAG, "writeBufferToFile: downloadtest download completed");
                    String docId = object.getString("DocId");
                    uploadDownloadManager.removeDownloadProgress(msgId);
                    MessageDbController db = CoreController.getDBInstance(this);
                    db.updateMessageDownloadStatus(docId, msgId, MessageFactory.DOWNLOAD_STATUS_COMPLETED, localPath);
                }


            }
        } catch (IOException e) {
            MyLog.e(TAG, "", e);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }

    private void sendAckToServer(String to, String doc_id, String id, boolean isSecretChat, String convId) {
        //Check if it is blocked message or not
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_MESSAGE_ACK);
        MessageAck ack = (MessageAck) MessageFactory.getMessage(MessageFactory.message_ack, (this));
        messageEvent.setMessageObject((JSONObject) ack.getMessageObject(to, doc_id,
                MessageFactory.DELIVERY_STATUS_DELIVERED, id, isSecretChat, convId));
        EventBus.getDefault().post(messageEvent);
    }

    private void sendCallAckToServer(JSONObject callObj, CallItemChat callItem) {
        try {
            String callId;
            if (callObj.has("docId")) {
                callId = callObj.getString("docId");
            } else {
                callId = callObj.getString("doc_id");
            }
            SendMessageEvent messageEvent = new SendMessageEvent();
            messageEvent.setEventName(SocketManager.EVENT_CALL_ACK);
            CallAck ack = (CallAck) MessageFactory.getMessage(MessageFactory.call_ack, (this));
            messageEvent.setMessageObject((JSONObject) ack.getMessageObject(callItem.getOpponentUserId(),
                    callId, MessageFactory.DELIVERY_STATUS_DELIVERED, callItem.getId()));
            EventBus.getDefault().post(messageEvent);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    @Override
    public void onDestroy() {
        sessionManager = SessionManager.getInstance(MessageService.this);
        MyLog.d(TAG, "onDestroy: servicetest");
        if (sessionManager.getlogin()) {
            MyLog.e(TAG, "onDestroy" + "unregister");
            //  EventBus.getDefault().unregister(this);
            offlineMsgHandler.onServiceDestroy();
            service = null;
            manager.disconnect();
            if (incomCallBroadcastRunnable != null) {
                incomCallBroadcastHandler.removeCallbacks(incomCallBroadcastRunnable);
            }
            try {
                unregisterReceiver(mDateReceiver);
                //  StopService();
            } catch (IllegalArgumentException e) {
                if (e.getMessage().contains("Receiver not registered")) {
                    // Ignore this exception. It's a known bug and is exactly what is desired.
                    //Log.w(TAG,"Tried to unregister the receiver when it's not registered");
                } else {
                    // unexpected, re-throw
                    throw e;
                }
            }
          /*  Intent broadcastIntent = new Intent("com.restart.service");
            sendBroadcast(broadcastIntent);*/
        }
        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        MyLog.e(TAG, "onTaskRemoved " + "servicetest");
        DemoSyncJob.scheduleJob();
        WorkerManager();
        super.onTaskRemoved(rootIntent);
    }

    public void WorkerManager() {
        Constraints constraints = new Constraints.Builder()
                .setRequiresBatteryNotLow(true)
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .setRequiresCharging(true)
                .setRequiresStorageNotLow(true)
                .build();
        Data.Builder data = new Data.Builder();
        data.putString("SyncMaster", "SyncMaster");


        PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest.Builder(
                MyPeriodicWork.class, 15, TimeUnit.MINUTES)
                .addTag("Sync")
                .setInputData(data.build())
                .setConstraints(constraints)
                .build();

        WorkManager.getInstance().enqueue(periodicWorkRequest);

    }

    private void loadGroupDetails(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        try {
            JSONObject objects = new JSONObject(data);

            String displayName = objects.getString("DisplayName");
            String displayPic = objects.getString("GroupIcon");
            String isAdmin = objects.getString("isAdmin");
            String groupId = objects.getString("_id");

            String adminMembers = "";
            List<GroupMembersPojo> savedMembersList = new ArrayList<>();
            List<GroupMembersPojo> unsavedMembersList = new ArrayList<>();
            List<GroupMembersPojo> allMembersList = new ArrayList<>();
            GroupMembersPojo currentUserPojo = null;

            JSONArray arrMembers = objects.getJSONArray("GroupUsers");
            for (int i = 0; i < arrMembers.length(); i++) {
                JSONObject userObj = arrMembers.getJSONObject(i);
                String userId = userObj.getString("id");
                String active = userObj.getString("active");
                // String isDeleted = userObj.getString("isDeleted");
                String msisdn = userObj.getString("msisdn");
                //  String phNumber = userObj.getString("PhNumber");
                String status = userObj.getString("Status");
                // String userDp = userObj.getString("avatar");
                String adminUser = userObj.getString("isAdmin");
                String isPending = userObj.optString("isPending");
                // String savedContactName = userObj.getString("ContactName");
                // String isExitsContact = userObj.getString("isExitsContact");
                // String contactName = getcontactname.getSendername(userId, msisdn);
                if (uniqueCurrentID.equalsIgnoreCase(userId)) {
                    //  contactName = "You";
                } else {
                    getUserDetails(userId);
                }

                String savedName = getcontactname.getSendername(userId, msisdn);
                GroupMembersPojo membersPojo = new GroupMembersPojo();
                membersPojo.setUserId(userId);
                membersPojo.setIsPending(isPending);
                membersPojo.setContactName(savedName);
                membersPojo.setMsisdn(msisdn);

                if (userId.equalsIgnoreCase(uniqueCurrentID)) {
                    currentUserPojo = membersPojo;
                } else {
                    if (msisdn.equalsIgnoreCase(savedName)) {
                        unsavedMembersList.add(membersPojo);
                    } else {
                        savedMembersList.add(membersPojo);
                    }
                }

                if (adminUser.equals("1")) {
                    adminMembers = adminMembers.concat(userId).concat(",");
                }
            }

            if (currentUserPojo != null) {
                allMembersList.addAll(savedMembersList);
                allMembersList.addAll(unsavedMembersList);
                allMembersList.add(currentUserPojo);

                String groupContactNames = "";
                String groupMembers = "";
                for (int i = 0; i < allMembersList.size(); i++) {
                    GroupMembersPojo pojo = allMembersList.get(i);
                    groupContactNames = groupContactNames.concat(pojo.getContactName());
                    groupMembers = groupMembers.concat(pojo.getUserId());
                    if ((arrMembers.length() - 1) != i) {
                        groupContactNames = groupContactNames.concat(", ");
                        groupMembers = groupMembers.concat(",");
                    }
                }

                String docId = uniqueCurrentID.concat("-").concat(groupId).concat("-g");
                GroupInfoSession groupInfoSession = new GroupInfoSession(MessageService.this);
                boolean hasInfo = groupInfoSession.hasGroupInfo(docId);
                GroupInfoPojo groupData;
                if (!hasInfo) {
                    groupData = new GroupInfoPojo();
                } else {
                    groupData = groupInfoSession.getGroupInfo(docId);
                }
                if (groupData != null) {
                    groupData.setGroupId(groupId);
                    groupData.setGroupName(displayName);
                    groupData.setIsAdminUser(isAdmin);
                    groupData.setAvatarPath(displayPic);
                    groupData.setLiveGroup(true);
                    groupData.setGroupMembers(groupMembers);
                    groupData.setAdminMembers(adminMembers);
                    groupData.setGroupContactNames(groupContactNames);
                    groupInfoSession.updateGroupInfo(docId, groupData);
                }
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }

    public void sendOfflineMessage(Object message, String eventName) {

        try {
            JSONObject ob = new JSONObject(message.toString());

            if (ob.has("id")) {

                MessageDbController dbController = CoreController.getDBInstance(mcontext);

                String Message_id = ob.getString("id");

                JSONObject object = dbController.fileuploadobjectget(Message_id);


                if (object != null) {

                    if (object.has("start")) {

                        int start = object.getInt("start");

                        if (start == 0) {

                            manager.send(message, eventName);

                        }
                    }


                } else {
                    if (eventName.equals(SocketManager.EVENT_GROUP)) {
                        try {
                            //for broadcast feature
                            Log.d(TAG, "onMessageEvent: offlinemsgmiss setbraodcast");
                            if (message instanceof JSONObject) {
                                JSONObject jsonObject = (JSONObject) message;
                                jsonObject.put("is_broadcast", 0);
                                message = jsonObject;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    manager.send(message, eventName);

                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public ChatLockPojo getChatLockdetailfromDB(String documentID, String chatType) {
        MessageDbController dbController = CoreController.getDBInstance(this);
        String convId = userInfoSession.getChatConvId(documentID);
        String receiverId = userInfoSession.getReceiverIdByConvId(convId);
        ChatLockPojo pojo = dbController.getChatLockData(receiverId, chatType);
        return pojo;
    }

    public boolean isActivityRunning(Context ctx) {
        ActivityManager activityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (ctx.getPackageName().equalsIgnoreCase(task.baseActivity.getPackageName()))
                return true;
        }

        return false;
    }

    private void loadIncomingCallData(String data) {


        MyLog.e(TAG, "loadIncomingCallData" + data);
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            String recordId = "";
            final JSONObject callObj = new JSONObject(data);
            String from = callObj.getString("from");
            String id = callObj.getString("id");


            if (mMessageIdIncomingCall.contains(id)) {
                return;
            }
            mMessageIdIncomingCall.add(id);
            MyLog.e(TAG, "loadIncomingCallData CallsActivity.isStarted" + CallsActivity.isStarted + "IncomingCallActivity.isStarted" + IncomingCallActivity.isStarted);
            String reconnecting = "";
            final String to = callObj.getString("to");
            String callStatus = callObj.getString("call_status");
            final String Room_id = "" + callObj.optString("roomid");
            recordId = callObj.getString("recordId");
            if (to.equalsIgnoreCase(uniqueCurrentID) && callStatus.equals(MessageFactory.CALL_STATUS_CALLING + "")) {
                final CallItemChat callItem = incomingMsg.loadIncomingCall(callObj);
                MessageDbController db = CoreController.getDBInstance(this);
                db.updateCallLogs(callItem);
                boolean isVideoCall = false;
                if (callItem.getCallType().equals(MessageFactory.video_call + "")) {
                    isVideoCall = true;
                }
                if (!CallsActivity.isStarted && !IncomingCallActivity.isStarted) {
                    if (callObj.has("reconnecting")) {
                        reconnecting = callObj.getString("reconnecting");

                        if (reconnecting.equals("true")) {
                            final String ts = callObj.getString("timestamp");


                            String[] splitIds = callItem.getCallId().split("-");
                            String mId = splitIds[2];
                            String callDocId = to + "-" + callItem.getOpponentUserId() + "-" + mId;
                            String type = "" + MessageFactory.audio_call;
                            if (isVideoCall)
                                type = "" + MessageFactory.video_call;

                            JSONObject object = CallMessage.getCallStatusObject(uniqueCurrentID, callItem.getOpponentUserId(),
                                    mId, callDocId, recordId, MessageFactory.CALL_STATUS_ANSWERED, type);


                            SendMessageEvent event = new SendMessageEvent();

                            event.setEventName(SocketManager.EVENT_CALL_STATUS);
                            event.setMessageObject(object);
                            EventBus.getDefault().post(event);


                            final boolean finalIsVideoCall = isVideoCall;

                           /* CallsActivity.mActivity.finish();

                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    CallMessage.openCallScreen(mcontext, callItem.getOpponentUserId(), to, callItem.getCallId(),
                                            Room_id, "", callItem.getOpponentUserMsisdn(), MessageFactory.CALL_IN_FREE + "",
                                            finalIsVideoCall, false, ts, "true");

                                    // finish();
                                }
                            }, 1000);*/


                        } else {
                            String ts = callObj.getString("timestamp");
                            Intent intent = new Intent(this, IncomingCallActivity.class);
                            intent.putExtra(IncomingCallActivity.EXTRA_DOC_ID, callItem.getCallId());
                            intent.putExtra(IncomingCallActivity.EXTRA_FROM_USER_ID, callItem.getOpponentUserId());
                            intent.putExtra(IncomingCallActivity.EXTRA_TO_USER_ID, to);
                            intent.putExtra(IncomingCallActivity.EXTRA_FROM_USER_MSISDN, callItem.getOpponentUserMsisdn());
                            intent.putExtra(IncomingCallActivity.EXTRA_CALL_ROOM_ID, Room_id);
                            intent.putExtra(IncomingCallActivity.EXTRA_CALL_RECORD_ID, recordId);
                            intent.putExtra(IncomingCallActivity.EXTRA_CALL_TYPE, isVideoCall);
                            intent.putExtra(IncomingCallActivity.EXTRA_CALL_TIME_STAMP, ts);
                            intent.putExtra("mRecordId", recordId);


                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            if (callItem.getOpponentUserId().equalsIgnoreCase(CallsActivity.opponentUserId)) {
                                sendIncomingCallBroadcast(intent);
                            }
                            String type = "" + MessageFactory.audio_call;
                            if (isVideoCall)
                                type = "" + MessageFactory.video_call;
                            // Ack to call sender(reverse order)
                            JSONObject ackObj = CallMessage.getCallStatusObject(to, callItem.getOpponentUserId(),
                                    callItem.getId(), callItem.getCallId(), callItem.getRecordId(), MessageFactory.CALL_STATUS_ARRIVED, type);
                            final SendMessageEvent event = new SendMessageEvent();
                            MyLog.e(TAG, "EVENT_CALL_STATUS" + ackObj);
                            event.setEventName(SocketManager.EVENT_CALL_STATUS);
                            event.setMessageObject(ackObj);

                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {


                                    Log.e(TAG, "loadIncomingCallData hANDLER");
                                    EventBus.getDefault().post(event);

                                    sendCallAckToServer(callObj, callItem);
                                }
                            }, 2000);
                        }
                    } else {
                        //
                        String ts = callObj.getString("timestamp");
                        Intent intent = new Intent(this, IncomingCallActivity.class);
                        intent.putExtra(IncomingCallActivity.EXTRA_DOC_ID, callItem.getCallId());
                        intent.putExtra(IncomingCallActivity.EXTRA_FROM_USER_ID, callItem.getOpponentUserId());
                        intent.putExtra(IncomingCallActivity.EXTRA_TO_USER_ID, to);
                        intent.putExtra(IncomingCallActivity.EXTRA_FROM_USER_MSISDN, callItem.getOpponentUserMsisdn());
                        intent.putExtra(IncomingCallActivity.EXTRA_CALL_ROOM_ID, Room_id);
                        intent.putExtra(IncomingCallActivity.EXTRA_CALL_RECORD_ID, recordId);
                        intent.putExtra(IncomingCallActivity.EXTRA_CALL_TYPE, isVideoCall);
                        intent.putExtra(IncomingCallActivity.EXTRA_CALL_TIME_STAMP, ts);

                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        if (callItem.getOpponentUserId().equalsIgnoreCase(CallsActivity.opponentUserId)) {
                            sendIncomingCallBroadcast(intent);
                        }
                        String type = "" + MessageFactory.audio_call;
                        if (isVideoCall)
                            type = "" + MessageFactory.video_call;
                        // Ack to call sender(reverse order)
                        JSONObject ackObj = CallMessage.getCallStatusObject(to, callItem.getOpponentUserId(),
                                callItem.getId(), callItem.getCallId(), callItem.getRecordId(), MessageFactory.CALL_STATUS_ARRIVED, type);
                        final SendMessageEvent event = new SendMessageEvent();
                        MyLog.e(TAG, "EVENT_CALL_STATUS" + ackObj);
                        event.setEventName(SocketManager.EVENT_CALL_STATUS);
                        event.setMessageObject(ackObj);

                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {


                                Log.e(TAG, "loadIncomingCallData hANDLER");
                                EventBus.getDefault().post(event);

                                sendCallAckToServer(callObj, callItem);
                            }
                        }, 2000);
                    }


                } else {
                    if (callObj.has("reconnecting")) {


                        reconnecting = callObj.getString("reconnecting");


                        if (reconnecting.equals("true")) {
                            final String ts = callObj.getString("timestamp");


                            String[] splitIds = callItem.getCallId().split("-");
                            String mId = splitIds[2];
                            String callDocId = to + "-" + callItem.getOpponentUserId() + "-" + mId;
                            String type = "" + MessageFactory.audio_call;
                            if (isVideoCall)
                                type = "" + MessageFactory.video_call;

                            JSONObject object = CallMessage.getCallStatusObject(uniqueCurrentID, callItem.getOpponentUserId(),
                                    mId, callDocId, recordId, MessageFactory.CALL_STATUS_ANSWERED, type);


                            SendMessageEvent event = new SendMessageEvent();

                            event.setEventName(SocketManager.EVENT_CALL_STATUS);
                            event.setMessageObject(object);
                            EventBus.getDefault().post(event);


                            final boolean finalIsVideoCall = isVideoCall;
                           /* CallsActivity.mActivity.finish();
                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    CallMessage.openCallScreen(mcontext, callItem.getOpponentUserId(), to, callItem.getCallId(),
                                            Room_id, "", callItem.getOpponentUserMsisdn(), MessageFactory.CALL_IN_FREE + "",
                                            finalIsVideoCall, false, ts, "true");

                                    // finish();
                                }
                            }, 1000);*/


                        }
                    }
                }
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void reconnectingtrue() {

    }

    private void Passvalue() {
        Log.e(TAG, "Passvalue");
        Intent intent = new Intent();
        intent.setAction(SENDMESAGGE);
        intent.putExtra("message", "Calling");
        sendBroadcast(intent);
    }

    private void loadCallStatusFromOpponentUser(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        Log.e(TAG, "loadCallOpponent" + "data" + data);

        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");
            String to = object.getString("to");
            String id = object.getString("id");

            String recordId = object.getString("recordId");
            String status = object.getString("call_status");
            String toDocId = object.getString("toDocId");
            if (status.equals(MessageFactory.CALL_STATUS_ARRIVED + "")) {
                CallMessage.arrivedCallId = recordId;
            }
            if (!uniqueCurrentID.equals(from)) {
                //Incoming call data save in db if rejected
                try {
                    MessageDbController db = CoreController.getDBInstance(this);
                    CallItemChat callItemChat = new CallItemChat();

                    //Check call status 6 and answered dont update for user

                    if (Integer.parseInt(status) == 6) {
                        SharedPreference.getInstance().saveBool(mcontext, "callongoing", false);
                        Passvalue();

                        if (!uniqueCurrentID.equals(from)) {
                            Log.e(TAG, "loadCallStatusFromOpponentUser: " + uniqueCurrentID);

                            CallsActivity.canEndCall = true;
                            Log.e(TAG, "loadCallStatusFromOpponentUser: " + uniqueCurrentID);

                            Log.e(TAG, "isStarted: " + !IncomingCallActivity.isStarted);
                            Log.e(TAG, "isStarted: " + !CallsActivity.isStarted);

                            //     if (!IncomingCallActivity.isStarted && !CallsActivity.isStarted) {
                            EventBus.getDefault().post(new CallDisconnect());
                        } else {
                            Log.e(TAG, "loadCallStatusFromOpponentUser: not equals " + uniqueCurrentID);

                        }
                    } else if (Integer.parseInt(status) == 5) {
                        SharedPreference.getInstance().saveBool(mcontext, "callongoing", false);
                        Passvalue();
                        if (!uniqueCurrentID.equals(from)) {
                            Log.e(TAG, "loadCallStatusFromOpponentUser: " + uniqueCurrentID);
                            CallsActivity.canEndCall = true;

                            CallDisconnect event = new CallDisconnect();
                            event.setEventName("false");

                            EventBus.getDefault().post(event);
                        } else {
                            Log.e(TAG, "loadCallStatusFromOpponentUser: not equals " + uniqueCurrentID);

                        }
                        callItemChat = incomingMsg.loadOutgoingCallOppenent(object);
                        db.updateCallLogs(callItemChat);
                    } else {
                        //Disconnect call for sender and receiver if not connected
                        callItemChat = incomingMsg.loadOutgoingCallOppenent(object);
                        db.updateCallLogs(callItemChat);
                    }
                } catch (Exception ex) {
                    MyLog.e(TAG, "loadCallStatusFromOpponentUser: ", ex);
                }
            }
            if (Integer.parseInt(status) == 5) {

                SharedPreference.getInstance().saveBool(mcontext, "callongoing", false);
                Passvalue();
                MessageDbController db = CoreController.getDBInstance(this);
                if (!uniqueCurrentID.equals(from)) {
                    //check call is missed or attended
                    boolean isAnsweredToUser = SharedPreference.getInstance().getBool(this, "isAnsweredToUser");
                    if (!isAnsweredToUser) {
                        CallItemChat callItem = incomingMsg.loadfromOfflineOutgoingCall(object);
                        db.updateCallLogs(callItem);
                    } else {
                        SharedPreference.getInstance().saveBool(mcontext, "callongoing", false);
                        Passvalue();
                        CallItemChat callItem = incomingMsg.loadOfflineCall(object);
                        db.updateCallLogs(callItem);
                    }
                }
            }
            //Check it is user and call status is ringing
         /*   if (uniqueCurrentID.equalsIgnoreCase(to)) {
                MyLog.e("loadCallOpponent", "data" + uniqueCurrentID + "to" + to + "equals");
                MyLog.e("loadCallOpponent", "status" + status);

                //Check call status is ringing
                if (Integer.parseInt(status) == 1) {
                    Passvalue(1);
                } else if (Integer.parseInt(status) == 2) {
                    //Check call status is Ended or Answered
                    Passvalue(2);
                }
            } else {
                MyLog.e("loadCallOpponent", "data" + uniqueCurrentID + "to" + to + "not equals");

            }
*/
        } catch (JSONException e) {
            Log.e(TAG, "loadCallOpponent eXCEPTION", e);
        }
    }

    private void loadOfflineCalls(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(data);
            String to = object.getString("to");

            if (to.equalsIgnoreCase(uniqueCurrentID)) {
                CallItemChat callItem = incomingMsg.loadOfflineCall(object);
                MessageDbController db = CoreController.getDBInstance(this);
                db.updateCallLogs(callItem);

                String callId;
                if (object.has("docId")) {
                    callId = object.getString("docId");
                } else {
                    callId = object.getString("doc_id");
                }

                sendCallAckToServer(object, callItem);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private String getEncryptPwd(String password, String convID) {
        try {
            String iv = getString(R.string.app_name);
            StringCryptUtils cryptLib = new StringCryptUtils();
            return cryptLib.encrypt(password, convID, iv);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
            return null;
        }
    }

    public void getReplyMessageDetails(String toUserId, String convId, String recordId, String chatType,
                                       String secretType, String msgId) {
        try {
            JSONObject object = new JSONObject();
            object.put("from", uniqueCurrentID);
            object.put("to", toUserId);
            object.put("convId", convId);
            object.put("recordId", recordId);
            object.put("requestMsgId", msgId);
            object.put("type", chatType);
            object.put("secret_type", secretType);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GET_MESSAGE_DETAILS);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void sendIncomingCallBroadcast(final Intent dataIntent) {

        incomCallBroadcastRunnable = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setAction(getPackageName() + ".incoming_call");
                intent.putExtras(dataIntent.getExtras());
                sendBroadcast(intent);
            }
        };
        incomCallBroadcastHandler.postDelayed(incomCallBroadcastRunnable, 4000);
    }

    private void deleteSingleMessage(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject objects = new JSONObject(data);
            int errorState = objects.getInt("err");
            if (errorState == 0) {
                String fromId = objects.getString("from");

                if (!fromId.equalsIgnoreCase(uniqueCurrentID)) {
//                    String deleteStatus = objects.getString("status");
                    String chat_id = (String) objects.get("doc_id");
                    String[] ids = chat_id.split("-");

                    String docId, msgId, type, recId, lastMsg_Status, convId;

//                    type = objects.getString("type");
//                    recId = objects.getString("recordId");
//                    convId = objects.getString("convId");

                    docId = ids[1] + "-" + ids[0];
                    msgId = docId + "-" + ids[2];

//                    if (deleteStatus.equalsIgnoreCase("1")) {
                    MessageDbController db = CoreController.getDBInstance(this);
                    if (ChatPageActivity.Chat_Activity == null) {
                        db.deleteSingleMessage(docId, msgId, "single", "other");
                        db.deleteChatListPage(docId, msgId, "single", "other");
                    } else {
                        if (!ChatPageActivity.Chat_to.equalsIgnoreCase(fromId)) {
                            db.deleteSingleMessage(docId, msgId, "single", "other");
                            db.deleteChatListPage(docId, msgId, "single", "other");
                        }
                    }
                    sendSingleACK(ids[0], msgId, "" + msgId, false);
//                    }
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void deleteGroupMessage(Object[] response) {
        try {
            JSONObject objects = new JSONObject(response[0].toString());
            int groupType = objects.getInt("groupType");
            String groupID = objects.getString("groupId");
            if (groupType == 19) {
                String fromId = objects.getString("from");

                if (!fromId.equalsIgnoreCase(uniqueCurrentID)) {
//                    String deleteStatus = objects.getString("status");
                    String chat_id = (String) objects.get("doc_id");
                    String[] ids = chat_id.split("-");

                    String docId, msgId, type, recId, lastMsg_Status, convId;

//                    type = objects.getString("type");
//                    recId = objects.getString("recordId");
//                    convId = objects.getString("convId");

                    docId = ids[1] + "-g-" + ids[0];
                    msgId = uniqueCurrentID + "-g-" + ids[1] + "-g-" + ids[3];

                    String groupAndMsgId = ids[1] + "-g-" + ids[3];

                    MessageDbController db = CoreController.getDBInstance(this);
                    if (ChatPageActivity.Chat_Activity == null) {
                        db.deleteSingleMessage(groupAndMsgId, chat_id, "group", "other");
                        db.deleteChatListPage(groupAndMsgId, chat_id, "group", "other");
                    } else if (!ChatPageActivity.Activity_GroupId.equalsIgnoreCase(groupID)) {
                        db.deleteSingleMessage(groupAndMsgId, chat_id, "group", "other");
                        db.deleteChatListPage(groupAndMsgId, chat_id, "group", "other");
                    }
                    sendGroupACK(groupID, msgId, uniqueCurrentID);
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void sendSingleACK(String to, String doc_id, String id, boolean isSecretChat) {
        SendMessageEvent groupMsgEvent = new SendMessageEvent();
        groupMsgEvent.setEventName(SocketManager.EVENT_SINGLE_ACK);
        try {
            String[] ids = doc_id.split("-");
            JSONObject object = new JSONObject();
            object.put("from", uniqueCurrentID);
            object.put("msgIds", new JSONArray(Arrays.asList(ids[2])));
            object.put("doc_id", doc_id);
            object.put("status", "2");
            object.put("to", to);
            object.put("secret_type", "");
            groupMsgEvent.setMessageObject(object);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        EventBus.getDefault().post(groupMsgEvent);
    }

    private void sendSingleOffline() {
        SendMessageEvent groupMsgEvent = new SendMessageEvent();
        groupMsgEvent.setEventName(SocketManager.EVENT_SINGLE_OFFLINE_MSG);
        try {
            JSONObject object = new JSONObject();
            object.put("msg_to", uniqueCurrentID);
            groupMsgEvent.setMessageObject(object);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        EventBus.getDefault().post(groupMsgEvent);
    }

    private void getSingleOffline(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject objects = new JSONObject(data);
            if (objects.has("result")) {
                JSONArray mResults = objects.optJSONArray("result");
                for (int i = 0; i < mResults.length(); i++)
                    storeDeleteMsg(mResults.getJSONObject(i));
            } else {
                storeDeleteMsg(objects);
            }

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void storeDeleteMsg(JSONObject objects) {
        try {
            int is_everyone = objects.optInt("is_deleted_everyone");
            if (is_everyone == 1) {
                String fromId = objects.getString("from");
                if (fromId != null && !fromId.equalsIgnoreCase(uniqueCurrentID)) {
                    String chat_id = objects.getString("docId");
                    String[] ids = chat_id.split("-");
                    String docId, msgId, chat_type, recId, lastMsg_Status, convId, deleteStatus;
                    String to = objects.getString("to");

                    chat_type = objects.getString("chat_type");
//                    recId = objects.getString("recordId");
//                    convId = objects.getString("convId");
//                    deleteStatus = objects.getString("is_deleted_everyone");

                    docId = ids[1] + "-" + ids[0];
                    msgId = docId + "-" + ids[2];

                    MessageDbController db = CoreController.getDBInstance(this);
                    //Default code and Issue Message Deleted Automatically
                    /*   if (ChatPageActivity.Chat_Activity == null) {

                        db.deleteSingleMessage(docId, msgId, chat_type, "other");
                        db.deleteChatListPage(docId, msgId, chat_type, "other");
                    } else {

                        if (!ChatPageActivity.Chat_to.equalsIgnoreCase(fromId)) {
                            db.deleteSingleMessage(docId, msgId, chat_type, "other");
                            db.deleteChatListPage(docId, msgId, chat_type, "other");
                        }
                    }*/
                    if (ChatPageActivity.Chat_Activity == null) {
                        //Check whether the group id is same
                        //Check from id equal to to id
                        if (!uniqueCurrentID.equalsIgnoreCase(to.trim())) {
                            if (ChatPageActivity.Chat_to != null) {
                                if (!ChatPageActivity.Chat_to.equalsIgnoreCase(fromId.trim())) {
                                    db.deleteSingleMessage(docId, msgId, chat_type, "other");
                                    db.deleteChatListPage(docId, msgId, chat_type, "other");
                                }
                            }
                        }

                    } else {
                        if (!uniqueCurrentID.equalsIgnoreCase(to.trim())) {

                            if (ChatPageActivity.Chat_to != null) {
                                if (!ChatPageActivity.Chat_to.equalsIgnoreCase(fromId.trim())) {
                                    //Check whether the group id is same
                                    db.deleteSingleMessage(docId, msgId, chat_type, "other");
                                    db.deleteChatListPage(docId, msgId, chat_type, "other");
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "storeDeleteMsg: ", e);
        }
    }

    private void sendGroupOffline() {
        SendMessageEvent groupMsgEvent = new SendMessageEvent();
        groupMsgEvent.setEventName(SocketManager.EVENT_GROUP);
        try {
            JSONObject object = new JSONObject();
            object.put("from", uniqueCurrentID);
            object.put("groupType", SocketManager.ACTION_EVENT_GROUP_OFFLINE);
            groupMsgEvent.setMessageObject(object);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        EventBus.getDefault().post(groupMsgEvent);
    }

    private void getGroupOffline(Object[] response) {
        try {
            JSONObject objects = new JSONObject(response[0].toString());
            String status = objects.getString("err");
            String fromID, toDocId, type, msgId, groupId;
            if (status.equalsIgnoreCase("0")) {
                int groupType = objects.getInt("groupType");
                if (groupType == 20) {
                    int is_deleted_everyone = objects.getInt("is_deleted_everyone");
                    if (is_deleted_everyone == 1) {
                        fromID = objects.getString("from");
                        toDocId = objects.getString("toDocId");
                        type = objects.getString("groupName");
                        String to = objects.getString("to");

                        String[] ids = toDocId.split("-");

                        String groupAndMsgId = ids[1] + "-g-" + ids[3];

                        msgId = uniqueCurrentID + "-g-" + ids[1] + "-g-" + ids[3];
                        groupId = objects.getString("groupId");

                        if (fromID != null && !fromID.equalsIgnoreCase(uniqueCurrentID)) {
                            MessageDbController db = CoreController.getDBInstance(this);
//Default functionality
                         /*   if (ChatPageActivity.Chat_Activity == null) {
                                db.deleteSingleMessage(groupAndMsgId, toDocId, "group", "other");
                                db.deleteChatListPage(groupAndMsgId, toDocId, "group", "other");
                            } else if (ChatPageActivity.Activity_GroupId != null && !ChatPageActivity.Activity_GroupId.equalsIgnoreCase(groupId)) {
                                db.deleteSingleMessage(groupAndMsgId, toDocId, "group", "other");
                                db.deleteChatListPage(groupAndMsgId, toDocId, "group", "other");
                            }*/
                            if (ChatPageActivity.Chat_Activity == null) {
                                //Check whether the group id is same
                                //Check from id equal to to id
                                if (!uniqueCurrentID.equalsIgnoreCase(to.trim())) {
                                    if (ChatPageActivity.Chat_to != null) {
                                        if (!ChatPageActivity.Chat_to.equalsIgnoreCase(fromID.trim())) {
                                            db.deleteSingleMessage(groupAndMsgId, toDocId, "group", "other");
                                            db.deleteChatListPage(groupAndMsgId, toDocId, "group", "other");
                                        }
                                    }
                                }

                            } else {
                                if (!uniqueCurrentID.equalsIgnoreCase(to.trim())) {

                                    if (ChatPageActivity.Chat_to != null) {
                                        if (!ChatPageActivity.Chat_to.equalsIgnoreCase(fromID.trim())) {
                                            //Check whether the group id is same
                                            db.deleteSingleMessage(groupAndMsgId, toDocId, "group", "other");
                                            db.deleteChatListPage(groupAndMsgId, toDocId, "group", "other");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            MyLog.e(TAG, "getGroupOffline: ", e);
        }
    }

    public void approveGroup(JSONObject object) {
        if (objectReceiver == null)
            objectReceiver = new MesssageObjectReceiver(this);
        objectReceiver.loadNewGroupMessage(object);
    }

    private class saveUserDetails extends AsyncTask<String, Void, String> {
        String datagolbal;

        public saveUserDetails(String data) {
            datagolbal = data;
        }

        @Override
        protected String doInBackground(String... params) {
            saveUserDetails(datagolbal);
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            //UI thread
        }
    }

    public class ActiveSocketDispatcher {


        private BlockingQueue<Runnable> dispatchQueue
                = new LinkedBlockingQueue<Runnable>();

        public ActiveSocketDispatcher() {
            Thread mThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            dispatchQueue.take().run();
                        } catch (InterruptedException e) {
                            MyLog.e(TAG, "", e);
                        }
                    }
                }
            });
            mThread.start();
        }

        private void addwork(final Object packet) {
            try {
                dispatchQueue.put(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().post(packet);
                    }
                });
            } catch (Exception e) {
            }
        }

    }


}
