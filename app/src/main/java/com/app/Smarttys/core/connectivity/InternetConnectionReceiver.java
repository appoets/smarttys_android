package com.app.Smarttys.core.connectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.socket.MessageService;
import com.app.Smarttys.core.socket.SocketManager;

/**
 *
 */
public class InternetConnectionReceiver extends BroadcastReceiver {

    public static String TAG = InternetConnectionReceiver.class.getSimpleName();


    @Override
    public void onReceive(Context context, Intent intent) {
        if (AppUtils.isNetworkAvailable(context)) {
            MyLog.e(TAG, "Connected to a network");
            if (!AppUtils.isMyServiceRunning(context, MessageService.class)) {
                AppUtils.startService(context, MessageService.class);
            } else if (!SocketManager.getInstance().isConnected()) {
                SocketManager.getInstance().connect();
            }
        } else {
            MyLog.e(TAG, "disConnected to a network");
        }
    }


}
