package com.app.Smarttys.core.message;

import android.content.Context;

import com.app.Smarttys.app.utils.GroupInfoSession;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.model.GroupInfoPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.socket.SocketManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CAS60 on 1/31/2017.
 */
public class WebLinkMessage extends BaseMessage implements Message {
    private static final String TAG = "WebLinkMessage";
    private Context context;

    public WebLinkMessage(Context context) {
        super(context);
        setType(MessageFactory.web_link);
        this.context = context;
    }

    @Override
    public Object getMessageObject(String to, String payload, Boolean isSecretChat) {
        this.to = to;
        setId(from + "-" + to);
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("type", MessageFactory.web_link);
            object.put("payload", payload);
            object.put("id", tsForServerEpoch);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);

            if (isSecretChat) {
//                setId(getId() + "-secret");
                object.put("chat_type", MessageFactory.CHAT_TYPE_SECRET);
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return object;
    }

    @Override
    public Object getGroupMessageObject(String to, String payload, String groupName) {
        this.to = to;
        setId(from + "-" + to + "-g");
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("type", MessageFactory.web_link);
            object.put("payload", payload);
            object.put("id", tsForServerEpoch);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);
            object.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE);
            object.put("userName", groupName);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return object;
    }

    public Object getWebLinkObject(JSONObject msgObj, String webLink, String webLinkTitle,
                                   String webLinkDesc, String webLinkImgUrl, String webLinkThumb) {

        JSONObject linkObj = new JSONObject();
        try {
            linkObj.put("title", webLinkTitle);
            linkObj.put("url", webLink);
            linkObj.put("description", webLinkDesc);
            /*linkObj.put("image", webLinkImgUrl);
            linkObj.put("thumbnail_data", webLinkThumb);*/

            msgObj.put("metaDetails", linkObj);

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return msgObj;
    }

    public MessageItemChat createMessageItem(boolean isSelf, String message, String status, String receiverUid,
                                             String senderName, String webLink, String webLinkTitle, String webLinkDesc,
                                             String webLinkImgUrl, String webLinkThumb) {
        item = new MessageItemChat();
        item.setMessageId(getId() + "-" + tsForServerEpoch);
        item.setIsSelf(isSelf);
        item.setStarredStatus(MessageFactory.MESSAGE_UN_STARRED);
        item.setTextMessage(message);
        item.setDeliveryStatus(status);
        item.setReceiverUid(receiverUid);
        item.setMessageType("" + type);
        item.setSenderName(senderName);
        item.setTS(getShortTimeFormat());
        item.setWebLink(webLink);
        item.setReceiverID(to);
        item.setWebLinkTitle(webLinkTitle);
        item.setWebLinkDesc(webLinkDesc);
        item.setWebLinkImgUrl(webLinkImgUrl);
        item.setWebLinkImgThumb(webLinkThumb);

        if (getId().contains("-g")) {
            GroupInfoSession groupInfoSession = new GroupInfoSession(context);
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(getId());

            if (infoPojo != null) {
                String[] groupMembers = infoPojo.getGroupMembers().split(",");
                try {
                    JSONArray arrMembers = new JSONArray();
                    for (String member : groupMembers) {
                        JSONObject userObj = new JSONObject();
                        userObj.put("UserId", member);
                        userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_SENT);
                        if (from.equals(member)) {
                            userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_READ);
                        }
                        userObj.put("DeliverTime", "");
                        userObj.put("ReadTime", "");
                        arrMembers.put(userObj);
                    }
                    JSONObject deliverObj = new JSONObject();
                    deliverObj.put("GroupMessageStatus", arrMembers);
                    item.setGroupMsgDeliverStatus(deliverObj.toString());
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }
        }

        return item;
    }
}
