package com.app.Smarttys.core.socket;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.Smarttys.R;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.database.MessageDbController;
import com.app.Smarttys.core.message.BackUpMessage;
import com.app.Smarttys.core.message.MessageFactory;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.model.SendMessageEvent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.MetadataChangeSet;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by CAS60 on 5/11/2017.
 */
public class GDBackUpService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "GDBackUpService";
    public static boolean isServiceStarted;
    private SessionManager sessionManager;
    private String mChatDbName, mCurrentUserId, mBackUpOver;
    private String mDriveFileName;
    private DriveId mDriveId;
    private long mDriveBackUpFileSize;
    private String mBackUpDuration;
    private String mBackupGmailId;
    private GoogleApiClient mGoogleApiClient;

    @Override
    public void onCreate() {
        super.onCreate();

        isServiceStarted = true;
        mChatDbName = MessageDbController.DB_NAME;
        EventBus.getDefault().register(GDBackUpService.this);

        sessionManager = SessionManager.getInstance(GDBackUpService.this);
        mCurrentUserId = sessionManager.getCurrentUserID();
        mBackUpOver = sessionManager.getBackUpOver();
        mBackUpDuration = sessionManager.getBackUpDuration();
        mBackupGmailId = sessionManager.getBackMailAccount();

        sessionManager.setBackUpServiceStartedAt();

        if (!mBackupGmailId.equals("")) {
            connectGoogleApiClient();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void performBackup() {
        boolean autoBackUpEnabled = !mBackUpDuration.equalsIgnoreCase(MessageService.BACK_UP_NEVER)
                && !mBackUpDuration.equalsIgnoreCase(MessageService.BACK_UP_ONLY_I_TAP);
        if (autoBackUpEnabled) {
            File dbDir = new File(getFilesDir() + "/" + mChatDbName + ".cblite2");
            String[] allFiles = dbDir.list();

            File backUpFolder = new File(MessageFactory.DATABASE_PATH);
            if (!backUpFolder.exists()) {
                backUpFolder.mkdirs();
            }
            String backUpFilePath = backUpFolder + "/" + getResources().getString(R.string.app_name) + "_db.zip";

            zip(dbDir, allFiles, backUpFilePath);
        }
    }

    public void zip(File dbDir, String[] _files, String zipFileName) {
        try {
            BufferedInputStream origin = null;
            File zipFile = new File(zipFileName);
            if (zipFile.exists()) {
                zipFile.delete();
                zipFile.createNewFile();
            }
            FileOutputStream dest = new FileOutputStream(zipFileName);
            BufferedOutputStream outputStream = new BufferedOutputStream(dest);
            ZipOutputStream out = new ZipOutputStream(outputStream);
            byte data[] = new byte[1024];

            for (int i = 0; i < _files.length; i++) {
                Log.v("Compress", "Adding: " + _files[i]);
                boolean isDirectory = new File(dbDir + "/" + _files[i]).isDirectory();
                if (!isDirectory) {
                    FileInputStream fi = new FileInputStream(dbDir + "/" + _files[i]);
                    origin = new BufferedInputStream(fi, 1024);

                    ZipEntry entry = new ZipEntry(_files[i]);
                    out.putNextEntry(entry);
                    int count;

                    while ((count = origin.read(data, 0, 1024)) != -1) {
                        out.write(data, 0, count);
                    }
                    origin.close();
                    fi.close();
                }
            }

            out.close();
            outputStream.close();
            dest.close();
            uploadToServer();
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void uploadToServer() {
        File dbDir = new File(MessageFactory.DATABASE_PATH);
        String backUpFileName = getResources().getString(R.string.app_name) + "_db.zip";
        String filePath = dbDir + "/" + backUpFileName;

        File backUpFile = new File(filePath);
        if (backUpFile.exists()) {
            mDriveBackUpFileSize = backUpFile.length();
            mDriveFileName = mCurrentUserId + "/" + backUpFileName;

            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                saveToDrive(Drive.DriveApi.getRootFolder(mGoogleApiClient), mDriveFileName,
                        "*/*", backUpFile);
            }

        }
    }

    private void saveToDrive(final DriveFolder pFldr, final String titl, final String mime, final java.io.File file) {
        if (mGoogleApiClient != null && pFldr != null && titl != null && mime != null && file != null)
            try {
                // create content from file
                Drive.DriveApi.newDriveContents(mGoogleApiClient).setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {
                    @Override
                    public void onResult(DriveApi.DriveContentsResult driveContentsResult) {
                        DriveContents cont = driveContentsResult != null && driveContentsResult.getStatus().isSuccess() ?
                                driveContentsResult.getDriveContents() : null;

                        // write file to content, chunk by chunk
                        if (cont != null) try {
                            OutputStream oos = cont.getOutputStream();
                            if (oos != null) try {
                                InputStream is = new FileInputStream(file);
                                byte[] buf = new byte[4096];
                                int c;
                                while ((c = is.read(buf, 0, buf.length)) > 0) {
                                    oos.write(buf, 0, c);
                                    oos.flush();
                                }
                                is.close();
                            } finally {
                                oos.close();
                            }

                            // content's COOL, create metadata
                            MetadataChangeSet meta = new MetadataChangeSet.Builder()
                                    .setTitle(titl).setMimeType(mime).build();

                            // now create file on GooDrive
                            pFldr.createFile(mGoogleApiClient, meta, cont).setResultCallback(new ResultCallback<DriveFolder.DriveFileResult>() {
                                @Override
                                public void onResult(DriveFolder.DriveFileResult driveFileResult) {
                                    if (driveFileResult != null && driveFileResult.getStatus().isSuccess()) {
                                        DriveFile dFil = driveFileResult.getStatus().isSuccess() ?
                                                driveFileResult.getDriveFile() : null;
                                        if (dFil != null) {
                                            // BINGO , file uploaded
                                            dFil.getMetadata(mGoogleApiClient).setResultCallback(new ResultCallback<DriveResource.MetadataResult>() {
                                                @Override
                                                public void onResult(DriveResource.MetadataResult metadataResult) {
                                                    if (metadataResult != null && metadataResult.getStatus().isSuccess()) {
                                                        mDriveFileName = metadataResult.getMetadata().getTitle();
                                                        mDriveId = metadataResult.getMetadata().getDriveId();

                                                        updateDataToServer(Calendar.getInstance().getTimeInMillis());
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        } catch (Exception e) {
                            MyLog.e(TAG, "", e);
                        }
                    }
                });
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
    }

    private void updateDataToServer(long backUpTime) {
        BackUpMessage message = new BackUpMessage();
        JSONObject object = message.getBackUpMessageObject(mCurrentUserId, mDriveBackUpFileSize, mDriveFileName, mDriveId,
                backUpTime, mBackUpDuration, mBackupGmailId, mBackUpOver);

        SendMessageEvent backupEvent = new SendMessageEvent();
        backupEvent.setEventName(SocketManager.EVENT_UPDATE_GOOGLE_DRIVE_SETTINGS);
        backupEvent.setMessageObject(object);
        EventBus.getDefault().post(backupEvent);
    }

    private void connectGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_FILE)
                .addScope(Drive.SCOPE_APPFOLDER)
                .setAccountName(mBackupGmailId)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mGoogleApiClient.connect();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_UPDATE_GOOGLE_DRIVE_SETTINGS)) {
            String data = event.getObjectsArray()[0].toString();

            try {
                JSONObject object = new JSONObject(data);
                String from = object.getString("from");

                if (from.equalsIgnoreCase(mCurrentUserId)) {
                    stopSelf();
                }
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        performBackup();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        stopSelf();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        isServiceStarted = false;
        EventBus.getDefault().unregister(GDBackUpService.this);
    }
}
