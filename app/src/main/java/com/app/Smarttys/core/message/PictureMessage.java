package com.app.Smarttys.core.message;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;

import com.app.Smarttys.app.model.CreatePost_Model;
import com.app.Smarttys.app.utils.GroupInfoSession;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.model.GroupInfoPojo;
import com.app.Smarttys.core.model.MessageItemChat;
import com.app.Smarttys.core.socket.SocketManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import id.zelory.compressor.Compressor;

/**
 * Created by Administrator on 11/2/2016.
 */
public class PictureMessage extends BaseMessage implements Message {
    private static final String TAG = "PictureMessage";
    private Context context;

    public PictureMessage(Context context) {
        super(context);
        setType(MessageFactory.picture);
        this.context = context;
    }

    @Override
    public Object getMessageObject(String to, String payload, Boolean isSecretChat) {
        this.to = to;
        setId(from + "-" + to);
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("type", MessageFactory.picture);
            object.put("payload", payload);
            object.put("id", tsForServerEpoch);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);

            if (isSecretChat) {
//                setId(getId() + "-secret");
                object.put("chat_type", MessageFactory.CHAT_TYPE_SECRET);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "getMessageObject: ", e);
        }
        return object;
    }

    @Override
    public Object getGroupMessageObject(String to, String payload, String groupName) {
        this.to = to;
        setId(from + "-" + to + "-g");
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("type", MessageFactory.picture);
            if (payload.contains(TextMessage.TAG_KEY)) {
                object.put("payload", payload);
                object.put("is_tag_applied", "1");
            } else {
                object.put("payload", payload);
                object.put("is_tag_applied", "0");
            }
            object.put("id", tsForServerEpoch);
            object.put("convId", to);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);
            object.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE);
            object.put("userName", groupName);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return object;
    }

    public Object createStatusTextObject(String Status_color, String Font_type, String caption, String docId, String category) {

        JSONObject uploadObj = new JSONObject();
        try {
            Log.d(TAG, "createStatusImageObject: id: " + getId());
            uploadObj.put("err", 0);
            uploadObj.put("ImageName", "");
            uploadObj.put("from", from);
            uploadObj.put("bufferAt", ""); // for get first index is 0.
            uploadObj.put("LocalPath", "");
            uploadObj.put("type", MessageFactory.text);
            uploadObj.put("uploadType", "status");
            uploadObj.put("chat_type", MessageFactory.CHAT_TYPE_STATUS);
            uploadObj.put("id", from + "-" + tsForServerEpoch);
            uploadObj.put("toDocId", docId);
            uploadObj.put("payload", caption);
            uploadObj.put("theme_color", Status_color);
            uploadObj.put("theme_font", Font_type);
            uploadObj.put("theme_category", category);
            Log.e(TAG, "createStatusTextObject" + uploadObj);
        } catch (JSONException e) {
            Log.e(TAG, "createStatusImageObject: ", e);
        }
        return uploadObj;
    }


    public MessageItemChat createMessageItem(boolean isSelf, String caption, String imgPath, String status,
                                             String receiverUid, String senderName, int imageWidth, int imageHeight) {
        item = new MessageItemChat();
        item.setMessageId(getId() + "-" + tsForServerEpoch);
        item.setIsSelf(isSelf);
        item.setTextMessage(caption);
        item.setImagePath(imgPath);
        item.setChatFileLocalPath(imgPath);
        item.setDeliveryStatus(status);
        item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_COMPLETED);
        item.setReceiverUid(receiverUid);
        item.setMessageType("" + type);
        item.setSenderName(senderName);
        item.setReceiverID(to);
        item.setTS(getShortTimeFormat());
        item.setFileBufferAt(0);
        item.setChatFileWidth(String.valueOf(imageWidth));
        item.setChatFileHeight(String.valueOf(imageHeight));

        try {
            File file = new File(imgPath);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            Bitmap compressBmp = Compressor.getDefault(context).compressToBitmap(file);
            compressBmp = Bitmap.createScaledBitmap(compressBmp, 100, 100, false);
            compressBmp.compress(Bitmap.CompressFormat.JPEG, 30, out);
            byte[] thumbArray = out.toByteArray();
            out.close();

            String thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT);
            item.setThumbnailData(thumbData);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

        if (getId().contains("-g")) {
            GroupInfoSession groupInfoSession = new GroupInfoSession(context);
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(getId());

            if (infoPojo != null) {
                String[] groupMembers = infoPojo.getGroupMembers().split(",");
                try {
                    JSONArray arrMembers = new JSONArray();
                    for (String member : groupMembers) {
                        JSONObject userObj = new JSONObject();
                        userObj.put("UserId", member);
                        userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_SENT);
                        if (from.equals(member)) {
                            userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_READ);
                        }
                        userObj.put("DeliverTime", "");
                        userObj.put("ReadTime", "");
                        arrMembers.put(userObj);
                    }
                    JSONObject deliverObj = new JSONObject();
                    deliverObj.put("GroupMessageStatus", arrMembers);
                    item.setGroupMsgDeliverStatus(deliverObj.toString());
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }
        }

        return item;
    }

    public Object getPictureMessageObject(String caption, String msgId, int fileSize, int imageWidth,
                                          int imageHeight, String serverFilePath, boolean isSecretChat) {
        JSONObject msgObj = new JSONObject();
        try {
            String[] splitIds = msgId.split("-");
            String fromUserId = splitIds[0];
            String toUserId = splitIds[1];
            String picMsgId;
            if (msgId.contains("-g")) {
                picMsgId = splitIds[3];
            } else {
                try {
                    picMsgId = splitIds[2];
                } catch (Exception e) {
                    //for status upload it only have xxxxxxxx-yyyy
                    picMsgId = splitIds[1];
                }
            }

            msgObj.put("from", fromUserId);
            msgObj.put("to", toUserId);
            msgObj.put("type", MessageFactory.picture);
            msgObj.put("payload", caption);
            msgObj.put("id", picMsgId);
            msgObj.put("toDocId", msgId);
            msgObj.put("filesize", fileSize);
            msgObj.put("width", imageWidth);
            msgObj.put("height", imageHeight);
            msgObj.put("thumbnail", serverFilePath);

            if (isSecretChat) {
                msgObj.put("chat_type", MessageFactory.CHAT_TYPE_SECRET);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return msgObj;
    }

    public Object getGroupPictureMessageObject(String caption, String msgId, int fileSize, int imageWidth,
                                               int imageHeight, String serverFilePath, String groupName) {
        JSONObject msgObj = new JSONObject();
        try {
            String[] splitIds = msgId.split("-");
            String fromUserId = splitIds[0];
            String toUserId = splitIds[1];
            String picMsgId;
            if (msgId.contains("-g")) {
                picMsgId = splitIds[3];
            } else {
                picMsgId = splitIds[2];
            }
            msgObj.put("from", fromUserId);
            msgObj.put("to", toUserId);
            msgObj.put("type", MessageFactory.picture);

            if (caption.contains(TextMessage.TAG_KEY)) {
                msgObj.put("payload", caption);
                msgObj.put("is_tag_applied", "1");
            } else {
                msgObj.put("payload", caption);
                msgObj.put("is_tag_applied", "0");
            }
            //msgObj.put("payload", caption);

            msgObj.put("id", picMsgId);
            msgObj.put("toDocId", msgId);
            msgObj.put("filesize", fileSize);
            msgObj.put("width", imageWidth);
            msgObj.put("height", imageHeight);
            msgObj.put("thumbnail", serverFilePath);
            msgObj.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE);
            msgObj.put("userName", groupName);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return msgObj;
    }

    public Object createImageUploadObject(String msgId, String docId, String fileName, String imgPath, String receiverName,
                                          String caption, String chatType, boolean isSecretChat) {
        JSONObject uploadObj = new JSONObject();

        try {
            uploadObj.put("err", 0);
            uploadObj.put("ImageName", fileName);
            uploadObj.put("id", msgId);
            uploadObj.put("docId", docId);
            uploadObj.put("from", docId.split("-")[0]);
            uploadObj.put("to", docId.split("-")[1]);
            uploadObj.put("toDocId", msgId);
            uploadObj.put("bufferAt", -1); // for get first index is 0.
            uploadObj.put("LocalPath", imgPath);
            uploadObj.put("type", MessageFactory.picture);
            uploadObj.put("ReceiverName", receiverName);
            uploadObj.put("mode", "phone");
            uploadObj.put("payload", caption);
            uploadObj.put("chat_type", chatType);
            uploadObj.put("start", 0);
            uploadObj.put("FileEnd", 0);

            if (isSecretChat) {
                uploadObj.put("secret_type", "yes");
            } else {
                uploadObj.put("secret_type", "no");
            }

            File file = new File(imgPath);

            try {
                file = Compressor.getDefault(context).compressToFile(file);
            } catch (Exception e) {
                MyLog.e(TAG, "createImageUploadObject: ", e);
            }

            uploadObj.put("size", file.length());
            uploadObj.put("original_filename", file.getName());

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            Bitmap compressBmp = Compressor.getDefault(context).compressToBitmap(file);
            int height = compressBmp.getHeight();
            int width = compressBmp.getWidth();
            compressBmp = Bitmap.createScaledBitmap(compressBmp, 100, 100, false);
            compressBmp.compress(Bitmap.CompressFormat.JPEG, 30, out);
            byte[] thumbArray = out.toByteArray();

            try {
                out.close();
            } catch (IOException e) {
                MyLog.e(TAG, "", e);
            }

            String thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT);
            if (!thumbData.startsWith("data:image/jpeg;base64,")) {
                thumbData = "data:image/jpeg;base64," + thumbData;
            }

            uploadObj.put("thumbnail_data", thumbData);
            uploadObj.put("height", height);
            uploadObj.put("width", width);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

        return uploadObj;
    }

    public Object createImageDownloadObject(String msgId, String docId, String imgServerPath,
                                            String imgLocalPath, String localFileName, String dataSize) {
        JSONObject downloadObj = new JSONObject();

        try {
            downloadObj.put("DocId", docId);
            downloadObj.put("MsgId", msgId);
            downloadObj.put("ImageName", imgServerPath);
            downloadObj.put("LocalPath", imgLocalPath);
            downloadObj.put("LocalFileName", localFileName);
            downloadObj.put("FileSize", dataSize);
            downloadObj.put("bytesRead", 0);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

        return downloadObj;
    }

    public Object createUserProfileImageObject(String fileName, String imgPath) {

        JSONObject uploadObj = new JSONObject();
        try {
            uploadObj.put("err", 0);
            uploadObj.put("ImageName", fileName);
            uploadObj.put("from", from);
            uploadObj.put("bufferAt", -1); // for get first index is 0.
            uploadObj.put("LocalPath", imgPath);
            uploadObj.put("type", MessageFactory.user_profile_pic_update);
            uploadObj.put("uploadType", "single");
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return uploadObj;
    }

    public Object createStatusImageObject(String fileName, String fileExtension, String imgPath, String caption, String docId) {

        JSONObject uploadObj = new JSONObject();
        try {
            MyLog.d(TAG, "createStatusImageObject: id: " + getId());
            uploadObj.put("err", 0);
            uploadObj.put("ImageName", fileName + "-" + tsForServerEpoch + fileExtension);
            uploadObj.put("from", from);
            uploadObj.put("bufferAt", -1); // for get first index is 0.
            uploadObj.put("LocalPath", imgPath);
            uploadObj.put("type", MessageFactory.picture);
            uploadObj.put("uploadType", "status");
            uploadObj.put("chat_type", MessageFactory.CHAT_TYPE_STATUS);
            uploadObj.put("id", from + "-" + tsForServerEpoch);
            uploadObj.put("toDocId", docId);
            uploadObj.put("payload", caption);
        } catch (JSONException e) {
            MyLog.e(TAG, "createStatusImageObject: ", e);
        }
        return uploadObj;
    }

    public Object createGroupProfileImageObject(String fileName, String imgPath) {

        JSONObject uploadObj = new JSONObject();
        try {
            uploadObj.put("err", 0);
            uploadObj.put("ImageName", fileName);
            uploadObj.put("from", from);
            uploadObj.put("bufferAt", -1); // for get first index is 0.
            uploadObj.put("LocalPath", imgPath);
            uploadObj.put("type", MessageFactory.group_profile_pic_update);
            uploadObj.put("uploadType", "group");
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        return uploadObj;
    }


    //---------------------Celebrity Post--------------------

    public Object createCelebrityImageObject(String imageName, String imagePath, String id, String docId) {
        JSONObject uploadObj = new JSONObject();
        try {
            MyLog.d(TAG, "createStatusImageObject: id: " + getId());
            uploadObj.put("err", 0);
            uploadObj.put("ImageName", imageName);
            uploadObj.put("from", from);
            uploadObj.put("bufferAt", -1); // for get first index is 0.
            uploadObj.put("LocalPath", imagePath);
            uploadObj.put("type", MessageFactory.picture);
            uploadObj.put("uploadType", MessageFactory.CHAT_TYPE_CELEBRITY);
            uploadObj.put("chat_type", MessageFactory.CHAT_TYPE_CELEBRITY);
            uploadObj.put("id", id);
            uploadObj.put("toDocId", docId);
            uploadObj.put("payload", "");
        } catch (JSONException e) {
            MyLog.e(TAG, "createStatusImageObject: ", e);
        }
        return uploadObj;
    }


    public Object createCelebrityVideoThumbImageObject(String imageName, String imagePath, String id, String docId) {
        JSONObject uploadObj = new JSONObject();
        try {
            MyLog.d(TAG, "createStatusImageObject: id: " + getId());
            uploadObj.put("err", 0);
            uploadObj.put("ImageName", imageName);
            uploadObj.put("from", from);
            uploadObj.put("bufferAt", -1); // for get first index is 0.
            uploadObj.put("LocalPath", imagePath);
            uploadObj.put("type", MessageFactory.picture);
            uploadObj.put("uploadType", MessageFactory.CHAT_TYPE_CELEBRITY_VIDEO_THUMB);
            uploadObj.put("chat_type", MessageFactory.CHAT_TYPE_CELEBRITY_VIDEO_THUMB);
            uploadObj.put("id", id);
            uploadObj.put("toDocId", docId);
            uploadObj.put("payload", "");
        } catch (JSONException e) {
            MyLog.e(TAG, "createStatusImageObject: ", e);
        }
        return uploadObj;
    }


    public Object createPostObject(List<CreatePost_Model> data, String payLoad) {
        JSONObject msgObj = new JSONObject();
        try {
            if (data.size() > 0) {
                JSONArray array = new JSONArray();
                for (int i = 0; i < data.size(); i++) {
                    JSONObject object = new JSONObject();
                    object.put("id", data.get(i).getId());
                    object.put("toDocId", data.get(i).getDocId());
                    object.put("filesize", data.get(i).getFileSize());
                    object.put("type", data.get(i).getType());
                    object.put("thumbnail", data.get(i).getFileName());
                    object.put("width", data.get(i).getFileWidth());
                    object.put("height", data.get(i).getFileHeight());
                    object.put("original_filename", data.get(i).getOriginal_FileName());
                    array.put(object);
                }
                msgObj.put("from", from);
                msgObj.put("id", tsForServerEpoch);
                msgObj.put("toDocId", from + "-" + tsForServerEpoch);
                msgObj.put("payload", payLoad);
                msgObj.put("uploads", array);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return msgObj;
    }


    public Object editPostObject(List<CreatePost_Model> data, String payLoad) {
        JSONObject msgObj = new JSONObject();
        try {
            if (data.size() > 0) {
                JSONArray array = new JSONArray();
                for (int i = 0; i < data.size(); i++) {
                    JSONObject object = new JSONObject();
                    object.put("id", data.get(i).getId());
                    object.put("toDocId", data.get(i).getDocId());
                    object.put("filesize", data.get(i).getFileSize());
                    object.put("type", data.get(i).getType());
                    object.put("thumbnail", data.get(i).getFileName());
                    object.put("width", data.get(i).getFileWidth());
                    object.put("height", data.get(i).getFileHeight());
                    object.put("original_filename", data.get(i).getOriginal_FileName());
                    object.put("edit_status", data.get(i).getStatus());
                    if (data.get(i).getStatus().equalsIgnoreCase("delete")) {
                        object.put("_id", data.get(i).getUniquechild_id());
                    }
                    array.put(object);
                }
                msgObj.put("from", from);
                msgObj.put("recordId", data.get(0).getRecord_ID());
                msgObj.put("collection_name", data.get(0).getCollections_Name());
                msgObj.put("payload", payLoad);
                msgObj.put("uploads", array);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return msgObj;
    }

}
