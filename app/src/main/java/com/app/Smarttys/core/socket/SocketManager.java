package com.app.Smarttys.core.socket;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.app.Smarttys.app.utils.AppUtils;
import com.app.Smarttys.app.utils.CryptLib;
import com.app.Smarttys.app.utils.CryptLibDecryption;
import com.app.Smarttys.app.utils.MyLog;
import com.app.Smarttys.core.SessionManager;
import com.app.Smarttys.core.model.ReceviceMessageEvent;
import com.app.Smarttys.core.service.Constants;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.OkHttpClient;

public class SocketManager {

    public static final String EVENT_RECONNECTINTIMATE = "sc_call_reconnect_intimate";
    public static final String EVENT_CREATE_USER = "create_user";
    public static final String EVENT_USER_CREATED = "usercreated";
    public static final String EVENT_REMOVE_USER = "remove_user";
    public static final String EVENT_DELETE_ACCOUNT = "sc_delete_account";
    public static final String EVENT_MESSAGE_RES = "sc_message_response";
    public static final String EVENT_MESSAGE = "sc_message";
    public static final String EVENT_STATUS = "sc_media_status";
    public static final String EVENT_STATUS_RESPONSE = "sc_media_status_response";
    public static final String EVENT_STATUS_UPDATE = "sc_media_message_status_update";
    public static final String EVENT_STATUS_ACK = "sc_media_status_ack";
    public static final String EVENT_STATUS_OFFLINE = "sc_get_offline_status";
    public static final String EVENT_STATUS_DELETE = "sc_remove_media_status";
    public static final String EVENT_STATUS_MUTE = "sc_mute_status";
    public static final String EVENT_STATUS_FETCH_ALL = "sc_get_media_status";
    public static final String EVENT_STATUS_PRIVACY = "sc_media_status_privacy";
    public static final String EVENT_QR_DATA_RES = "qrdataresponsCe";
    public static final String EVENT_GROUP = "group";
    public static final String EVENT_REPLY_MESSAGE = "ReplyMessage";
    public static final String EVENT_GET_MESSAGE = "sc_get_offline_messages";
    public static final String EVENT_CALL_RECONNECT = "sc_call_reconnect_hold";
    public static final String EVENT_GET_ACKSTATUS = "sc_get_status_ack";
    public static final String EVENT_GET_OFFLINE_GROUPMESSAGE = "sc_group_offline_message";
    public static final String EVENT_MESSAGE_STATUS_UPDATE = "sc_message_status_update";
    public static final String EVENT_MESSAGE_STATUS_BROADCAST_UPDATE = "sc_broadcast_message_ack";//TODO ADD THIS EVENT (AND APPLY SAME AS EVENT_MESSAGE_STATUS_UPDATE)
    public static final String EVENT_MESSAGE_ACK = "sc_message_ack";
    public static final String EVENT_GET_CURRENT_TIME_STATUS = "getCurrentTimeStatus";
    public static final String EVENT_CLEAR_CHAT = "sc_clear_chat";
    public static final String EVENT_CLEAR_DB = "sc_clear_user_db";
    public static final String EVENT_ARCHIVE_UNARCHIVE = "sc_archived_chat";
    public static final String EVENT_CAHNGE_ONLINE_STATUS = "sc_change_online_status";
    public static final String EVENT_CHANGE_ST = "sc_change_status";
    //DELETE CHAT
    public static final String EVENT_TYPING = "sc_typing";
    public static final String EVENT_ADD_CONTACT = "add contact";
    public static final String EVENT_GET_FAVORITE = "GetPhoneContact";
    public static final String EVENT_GET_CONTACTS = "sc_contacts";
    public static final String EVENT_QR_DATA = "qrdata";
    public static final String EVENT_IMAGE_UPLOAD = "uploadImage";
    public static final String EVENT_GROUP_DETAILS = "getGroupDetails";
    public static final String EVENT_MOBILE_TO_WEB_LOGOUT = "mobileToWebLogout";
    public static final String EVENT_STAR_MESSAGE = "StarMessage";
    public static final String EVENT_REMOVE_MESSAGE = "RemoveMessage";
    public static final String EVENT_DELETE_CHAT = "sc_delete_chat";
    //Delete chat receiver
    public static final String EVENT_TO_DELETECHAT = "sc_to_delete_chat";
    public static final String DELETE_OPPONENT = "sc_delete_chat_opponenet";
    public static final String EVENT_CLEAR_CHAT_OPPENENT = "sc_clear_chat_opponenet";
    public static final String EVENT_CHANGE_USER_NAME = "changeName";
    public static final String EVENT_FILE_UPLOAD = "app/fileUpload";
    public static final String EVENT_FILE_RECEIVED = "app/received";
    public static final String EVENT_FILE_DOWNLOAD = "app/fileDownload";
    public static final String EVENT_START_FILE_DOWNLOAD = "app/download";
    public static final String EVENT_GET_USER_DETAILS = "sc_get_user_Details";
    public static final String EVENT_UPDATE_MOBILE_LOGIN_NOTIFICATION = "updateMobilePushNotificationKey";
    public static final String EVENT_REMOVE_MOBILE_LOGIN_NOTIFICATION = "unsetMobilePushNotificationKey";
    public static final String EVENT_CHECK_MOBILE_LOGIN_KEY = "checkMobileLoginKey";
    public static final String EVENT_BLOCK_USER = "sc_block_user";
    public static final String EVENT_MUTE = "sc_mute_chat";
    public static final String EVENT_MARKREAD = "sc_marked_chat";
    public static final String EVENT_PRIVACY_SETTINGS = "sc_privacy_settings";
    public static final String EVENT_PHONE_DOWNLOAD = "app/phoneDownload";
    public static final String EVENT_REPORT_SPAM_USER = "sc_report_spam_user";
    public static final String EVENT_VIEW_CHAT = "sc_view_chat";
    public static final String EVENT_NEW_FILE_MESSAGE = "sc_new_message";
    public static final String EVENT_CHANGE_PROFILE_STATUS = "changeProfileStatus";
    public static final String EVENT_GET_MESSAGE_INFO = "app/getMessageInfo";
    public static final String EVENT_GET_SERVER_TIME = "sc_get_server_time";
    public static final String EVENT_CHANGE_SECRET_MSG_TIMER = "sc_change_timer";
    public static final String EVENT_TO_CONV_SETTING = "sc_to_conv_settings";
    public static final String EVENT_CHANGE_EMAIL = "sc_change_email";
    public static final String EVENT_RECOVERY_EMAIL = "sc_change_recovery_email";
    public static final String EVENT_RECOVERY_PHONE = "sc_change_recovery_phone";
    public static final String EVENT_UPDATE_GOOGLE_DRIVE_SETTINGS = "sc_drive_settings";
    public static final String EVENT_GET_GOOGLE_DRIVE_SETTINGS = "sc_get_drive_settings";
    public static final String EVENT_CHAT_LOCK = "sc_chat_lock";
    public static final String EVENT_CHAT_LOCK_FROM_WEB = "sc_set_mobile_password_chat_lock";
    public static final String EVENT_VERIFY_EMAIL = "sc_verify_email";
    //TODO NEED TO INTEGRATE WITH WEB SERVICE
    public static final String EVENT_RATING = "sc_rating";
    public static final String EVENT_GET_SETTINGS = "sc_settings";
    public static final String EVENT_GET_CONV_ID = "getMobileConvId";
    public static final String EVENT_CALL = "sc_call";
    public static final String EVENT_CALL_RESPONSE = "sc_call_response";
    public static final String EVENT_CALL_ACK = "sc_call_ack";
    public static final String EVENT_CALL_STATUS = "sc_call_status";
    public static final String EVENT_CALL_STATUS_RESONSE = "sc_call_status_response";
    public static final String EVENT_REMOVE_CALLS = "RemoveCalls";
    public static final String EVENT_REMOVE_ALL_CALLS = "RemoveAllCalls";
    public static final String EVENT_RETRY_CALL_CONNECT = "sc_call_retry";
    public static final String EVENT_DISCONNECT_CALL = "disconnect_call";
    public static final String EVENT_SKIP_BACKUP_MESSAGES = "sc_skipbackup_messages";
    public static final String EVENT_GET_MESSAGE_DETAILS = "sc_get_message_details";
    public static final String EVENT_PING = "pingEvent";//TESTING PURPOSE -> FOR AVOID SOCKET DISCONNECT
    public static final String EVENT_STATUSDELETE = "sc_get_offline_deleted_status";//Status delete PURPOSE
    //------------Delete Chat---------------------------------------
    public static final String EVENT_DELETE_MESSAGE = "sc_remove_message_everyone";
    public static final String EVENT_SINGLE_OFFLINE_MSG = "sc_get_offline_deleted_messages";
    public static final String ACTION_EVENT_GROUP_OFFLINE = "20";
    public static final String ACTION_EVENT_GROUP_MSG_DELETE = "19";
    //------------------------------turn server call events ---------------------------
    //TURN SERVER CODE
    public static final String EVENT_TURN_MESSAGE_FROM_CALLER = "sc_webrtc_turn_message_from_caller";
    public static final String EVENT_TURN_MESSAGE = "sc_webrtc_turn_message";
    public static final String EVENT_GET_SECRET_KEYS = "sc_get_secret_keys";
    public static final String EVENT_USER_AUTHENTICATED = "userauthenticated";
    public static final String EVENT_UPDATE_CONTACT = "sc_update_phone_contact";
    public static final String EVENT_UPDATE_INFO = "sc_update_new_changes";
    //-------------------------- RAAD ADMIN TRACKING----------------
    public static final String EVENT_ADMIN_TRACK_START = "sc_get_location_admin";
    public static final String EVENT_ADMIN_TRACK_STOP = "sc_stop_location_admin";
    public static final String EVENT_TEST_ENCRYPT = "PublicPrivateKeyEncryption";
    public static final String EVENT_TEST_DECRYPT = "PublicPrivateKeyDecryption";
    public static final String ACTION_NEW_GROUP = "1";
    public static final String ACTION_CHANGE_GROUP_DP = "2";
    public static final String ACTION_DELETE_GROUP_MEMBER = "4";
    public static final String ACTION_ADD_GROUP_MEMBER = "5";
    public static final String ACTION_CHANGE_GROUP_NAME = "6";
    public static final String ACTION_MAKE_GROUP_ADMIN = "7";
    public static final String ACTION_EXIT_GROUP = "8";
    public static final String ACTION_EVENT_GROUP_MESSAGE = "9";
    public static final String ACTION_JOIN_NEW_GROUP = "10";
    public static final String ACTION_ACK_GROUP_EVENTS = "11";
    public static final String ACTION_ACK_GROUP_MESSAGE = "12";
    public static final String ACTION_GET_OFFLINE_CREATED_GROUPS = "14";
    public static final String ACTION_EVENT_GROUP_REPlY_MESSAGE = "18";
    public static final long RESPONSE_TIMEOUT = 45000;  // 45 seconds
    public static final long CONTACT_REFRESH_TIMEOUT = 90000; // 90 seconds
    static final String EVENT_CLEAR_ALL_MESSAGES = "sc_clear_single_user_chat";
    static final String EVENT_NEW_ROOM_CONNECTION = "sc_new_room_connection";
    static final String EVENT_GET_GROUP_LIST = "app/getGroupList";
    static final String EVENT_GET_MOBILE_SETTINGS = "GetMobileSettings";
    static final String EVENT_PHONE_DATA_RECEIVED = "app/phoneDataReceived";


    //---------------------------Old User Update email password Event-----------------
    static final String EVENT_GET_APP_SETTINGS = "sc_app_settings";
    static final String EVENT_GET_UPLOADED_FILE_SIZE = "app/getFilesizeInBytes";
    static final String EVENT_GET_ADMIN_SETTINGS = "changeAdminSettings";
    public static final List<String> excludedList = new ArrayList<String>() {
        {
            add(EVENT_CREATE_USER);
            add(EVENT_USER_CREATED);
            add(Socket.EVENT_CONNECT);
            add(Socket.EVENT_DISCONNECT);
            add(EVENT_GET_ADMIN_SETTINGS);
            add(EVENT_DISCONNECT_CALL);
            add(EVENT_QR_DATA);
            add(EVENT_GET_SERVER_TIME);
            add(EVENT_FILE_UPLOAD);
            add(EVENT_GET_SETTINGS);
            add(EVENT_TURN_MESSAGE_FROM_CALLER);
        }
    };
    static final String EVENT_GET_OFFLINE_CALLS = "sc_get_offline_calls";
    static final String EVENT_CONV_SETTINGS = "sc_conv_settings";
    static final String EVENT_REMOVED_ACCOUNT_BY_ADMIN = "RemovedByAdmin";
    static final String EVENT_SINGLE_ACK = "sc_deleted_message_ack";
    static final String ACTION_EVENT_GROUP_ACK = "21";
    static final String ACTION_EVENT_GROUP_ACK_NEW = "12";
    //Encryption
    static final String EVENT_CHANGE_SECURITY_CODE = "sc_change_new_security_code";
    static final String ACTION_GET_GROUP_CHAT_HISTORY = "13";
    private static final String EVENT_NEW_MESSAGE = "new message";
    private static final String EVENT_USER_JOINED = "user joined";
    private static final String EVENT_HEARTBEAT = "Heartbeat";
    private static final String EVENT_STOP_TYPING = "stop typing";
    private static final String EVENT_UPDATE_ONLINE_STATUS = "UpdateOnlineStatus";
    private static final String EVENT_BROADCAST_PROFILE = "broadCastProfile";
    private static final String EVENT_CREATE_ROOM = "create room";
    private static final String ROOM_STRING = "room";
    private static final String TAG = SocketManager.class.getSimpleName();
    public static SocketCallBack callBack;
    public static boolean isConnected;
    private static SocketManager mSocketManager = new SocketManager();
    private static String[] scMessageJsonKeys = new String[]{
            "payload"
    };
    private static final ArrayList<String> scMessageJsonKeysList = new ArrayList<>(Arrays.asList(scMessageJsonKeys));
    /*    private static String [] replyMessageJsonKeys =new String[]{
                "toDocId","id","message","payload","link_details","thumbnail","original_filename","contact_name","contact_details","reply_details","contactDetails"
        };*/
    private static String[] replyMessageJsonKeys = new String[]{
            "payload"
    };
    private static final ArrayList<String> replyMessageJsonKeysList = new ArrayList<>(Arrays.asList(replyMessageJsonKeys));
    private static String[] scMessageResponseJsonKeys = new String[]{
            "payload", "message"
    };
    private static final ArrayList<String> scMessageResponseJsonKeysList = new ArrayList<>(Arrays.asList(scMessageResponseJsonKeys));
    private SocketCallBack messageServiceCallBack;
    private String ip = Constants.SOCKET_URL_;
    private String callIP = Constants.SOCKET_URL_CALL_;

/*    private void getSocketIp() {
        try {
            if (mSocket != null) {
                mSocket.close();
                mSocket.off();
                mSocket = null;
            }

            *//*OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .build();
            IO.setDefaultOkHttpCallFactory(okHttpClient);
            IO.setDefaultOkHttpWebSocketFactory(okHttpClient);*//*

            IO.Options options = new IO.Options();
            // options.transports=new String[]{ip};
            options.reconnectionAttempts = Integer.MAX_VALUE;
            options.reconnectionDelay = 0;
            options.secure = false;
            options.reconnection = true;
            options.timeout = 60000;//60 seconds
            //options.timeout = 72000000;
            //options.transports = new String[]{"polling", "websocket"};

            mSocket = IO.socket(ip, options);
            mSocket.io().reconnection(true);
            mCallSocket=IO.socket(callIP,options);
            mCallSocket.io().reconnection(true);
            //Toast.makeText(context, "New Socket Connection Created ", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            MyLog.e(TAG, "getSocketIp: ", e);
        }
    }*/
    private Context context;
    private Socket mSocket;
    private Socket mCallSocket;
    private Emitter.Listener onPongEvent = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.e("CheckEventConnection", "PONG>>");
            // MyLog.e(TAG,"registerd"+ EventBus.getDefault().isRegistered(MessageService.class));

        }
    };
    private Emitter.Listener onPingEvent = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.e("CheckEventConnection", "PING>>");
            //  MyLog.e(TAG,"registerd"+ EventBus.getDefault().isRegistered(MessageService.class));
        }
    };

    //----------------------PONG EVENT---------------------------------------
    private Emitter.Listener privacysettings = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "privacysettings ");
            invokeCallBack(EVENT_PRIVACY_SETTINGS, args);
        }
    };

    //-----------------------------PING EVENT-------------------------------
    private Emitter.Listener rating = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //    MyLog.d("SOCKET MANAGER", "rating " );
            invokeCallBack(EVENT_RATING, args);
        }
    };
    private Emitter.Listener Mute_Unmute = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "muteunmute ");
            invokeCallBack(EVENT_MUTE, args);
        }
    };
    private Emitter.Listener MarkRead_Unread = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "markreadunread ");
            invokeCallBack(EVENT_MARKREAD, args);
        }
    };
    private Emitter.Listener archive_unarchive = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "clearchat ");
            invokeCallBack(EVENT_ARCHIVE_UNARCHIVE, args);
        }
    };
    private Emitter.Listener clearchat = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "clearchat ");
            invokeCallBack(EVENT_CLEAR_CHAT, args);
        }
    };
    private Emitter.Listener clearDB = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "clearDB ");
            invokeCallBack(EVENT_CLEAR_DB, args);
        }
    };
    private Emitter.Listener clearAllMsgs = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "clearDB ");
            invokeCallBack(EVENT_CLEAR_ALL_MESSAGES, args);
        }
    };
    private Emitter.Listener ReplyMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "ReplyMessage ");
            invokeCallBack(EVENT_REPLY_MESSAGE, args);
        }
    };
    private Emitter.Listener stopTypingListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "heartBeatListener ");
            invokeCallBack(EVENT_STOP_TYPING, args);
        }
    };
    private Emitter.Listener heartBeatListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "heartBeatListener ");
            invokeCallBack(EVENT_HEARTBEAT, args);
        }
    };
    private Emitter.Listener messageResponseListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "messageResponseListener");
            invokeCallBack(EVENT_MESSAGE_RES, args);
        }
    };
    private Emitter.Listener messageListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "messageListener");
            invokeCallBack(EVENT_MESSAGE, args);
        }
    };
    private Emitter.Listener statusListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "messageListener");
            invokeCallBack(EVENT_STATUS, args);
        }
    };
    private Emitter.Listener statusResponseListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "messageListener");
            invokeCallBack(EVENT_STATUS_RESPONSE, args);
        }
    };
    private Emitter.Listener statusUpdateListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "messageListener");
            invokeCallBack(EVENT_STATUS_UPDATE, args);
        }
    };
    private Emitter.Listener statusAckListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "messageListener");
            invokeCallBack(EVENT_STATUS_ACK, args);
        }
    };
    private Emitter.Listener callReconnectStatusListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "EVENT_CALL_RECONNECT");
            invokeCallBack(EVENT_CALL_RECONNECT, args);
        }
    };
    private Emitter.Listener callReconnectInitimateListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "EVENT_RECONNECTINTIMATE");
            invokeCallBack(EVENT_RECONNECTINTIMATE, args);
        }
    };
    private Emitter.Listener statusOfflineListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "messageListener");
            invokeCallBack(EVENT_STATUS_OFFLINE, args);
        }
    };
    private Emitter.Listener statusRemoveListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "messageListener");
            invokeCallBack(EVENT_STATUS_DELETE, args);
        }
    };
    private Emitter.Listener statusMuteListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "messageListener");
            invokeCallBack(EVENT_STATUS_MUTE, args);
        }
    };
    private Emitter.Listener statusFetchAllListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "messageListener");
            invokeCallBack(EVENT_STATUS_FETCH_ALL, args);
        }
    };
    private Emitter.Listener statusPrivacyListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "messageListener");
            invokeCallBack(EVENT_STATUS_PRIVACY, args);
        }
    };
    private Emitter.Listener groupListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "groupListener");
            invokeCallBack(EVENT_GROUP, args);
        }
    };
    private Emitter.Listener getMessageListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "getMessageListener");
            invokeCallBack(EVENT_GET_MESSAGE, args);
        }
    };
    private Emitter.Listener getStatusAckListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "getMessageListener");
            invokeCallBack(EVENT_GET_ACKSTATUS, args);
        }
    };
    private Emitter.Listener getofflineMessageListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "getMessageListener");
            invokeCallBack(EVENT_GET_OFFLINE_GROUPMESSAGE, args);
        }
    };
    private Emitter.Listener messageStatusListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "messageStatusListener");
            invokeCallBack(EVENT_MESSAGE_STATUS_UPDATE, args);
        }
    };
    private Emitter.Listener messageAckListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "messageAckListener");
            invokeCallBack(EVENT_MESSAGE_ACK, args);
        }
    };
    private Emitter.Listener getCurrentTimeStatusListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "getCurrentTimeStatusListener");
            invokeCallBack(EVENT_GET_CURRENT_TIME_STATUS, args);
        }
    };
    private Emitter.Listener changeOnlineStatusListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //      MyLog.d("SOCKET MANAGER", "changeOnlineStatusListener" );
            invokeCallBack(EVENT_CAHNGE_ONLINE_STATUS, args);
        }
    };
    private Emitter.Listener updateOnlineStatusListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "updateOnlineStatusListener");
            invokeCallBack(EVENT_UPDATE_ONLINE_STATUS, args);
        }
    };
    private Emitter.Listener changeStateListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "changeStateListener");
            invokeCallBack(EVENT_CHANGE_ST, args);
        }
    };
    private Emitter.Listener typingListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "typingListener");
            invokeCallBack(EVENT_TYPING, args);
        }
    };
    private Emitter.Listener broadCastProfileListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "broadCastProfileListener");
            invokeCallBack(EVENT_BROADCAST_PROFILE, args);
        }
    };
    private Emitter.Listener addUserListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "addUser");
            invokeCallBack(EVENT_CREATE_USER, args);
        }
    };
    private Emitter.Listener userCreatedListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "User Created");
            invokeCallBack(EVENT_USER_CREATED, args);
        }
    };
    private Emitter.Listener createRoomListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "createRoomListener");
            invokeCallBack(EVENT_CREATE_ROOM, args);
        }
    };
    private Emitter.Listener newRoomConnectionListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "newRoom");
            invokeCallBack(EVENT_NEW_ROOM_CONNECTION, args);
        }
    };
    private Emitter.Listener userJoinedListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "userJoinedListener");
            invokeCallBack(EVENT_USER_JOINED, args);
        }
    };
    private Emitter.Listener addContact = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "addContact");
            invokeCallBack(EVENT_ADD_CONTACT, args);
        }
    };
    private Emitter.Listener getFavouriteListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "getFavouriteListener");
            invokeCallBack(EVENT_GET_FAVORITE, args);
        }
    };
    private Emitter.Listener getContacts = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //    MyLog.d("SOCKET MANAGER", "getContacts" );
            invokeCallBack(EVENT_GET_CONTACTS, args);
        }
    };
    private Emitter.Listener qrDataListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //  MyLog.d("SOCKET MANAGER", "QR_Data_Listener " );
            invokeCallBack(EVENT_QR_DATA, args);
        }
    };
    private Emitter.Listener imageUploadListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //      MyLog.d("SOCKET MANAGER", "Upload Image Listener " );
            invokeCallBack(EVENT_IMAGE_UPLOAD, args);
        }
    };
    private Emitter.Listener groupDetailsListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //MyLog.d("SOCKET MANAGER", "Group Details Listener " );
            invokeCallBack(EVENT_GROUP_DETAILS, args);
        }
    };
    private Emitter.Listener removeUserListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // MyLog.d("SOCKET MANAGER", "Group Details Listener " );
            invokeCallBack(EVENT_REMOVE_USER, args);
        }
    };
    private Emitter.Listener deleteAccountListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // MyLog.d("SOCKET MANAGER", "Delete account Listener " );
            invokeCallBack(EVENT_DELETE_ACCOUNT, args);
        }
    };
    private Emitter.Listener Logoutlistener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //  MyLog.d("SOCKET MANAGER", "logout " );
            invokeCallBack(EVENT_MOBILE_TO_WEB_LOGOUT, args);
        }
    };
    private Emitter.Listener BlockusermessageListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //    MyLog.d("SOCKET MANAGER", "blockuser " );
            invokeCallBack(EVENT_BLOCK_USER, args);
        }
    };
    private Emitter.Listener starredMessageListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //      MyLog.d("SOCKET MANAGER", "starNextLine" );
            invokeCallBack(EVENT_STAR_MESSAGE, args);
        }
    };
    private Emitter.Listener removeMsgListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //        MyLog.d("SOCKET MANAGER", "Remove Msg" );
            invokeCallBack(EVENT_REMOVE_MESSAGE, args);
        }
    };
    private Emitter.Listener deleteChatListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //          MyLog.d("SOCKET MANAGER", "Delete Msg" );
            invokeCallBack(EVENT_DELETE_CHAT, args);
        }
    };
    private Emitter.Listener todeleteChatListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //          MyLog.d("SOCKET MANAGER", "Delete Msg" );
            invokeCallBack(EVENT_TO_DELETECHAT, args);
        }
    };
    private Emitter.Listener deleteopponentListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //          MyLog.d("SOCKET MANAGER", "Delete Msg" );
            invokeCallBack(DELETE_OPPONENT, args);
        }
    };
    private Emitter.Listener clearchatopponentListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //          MyLog.d("SOCKET MANAGER", "Delete Msg" );
            invokeCallBack(EVENT_CLEAR_CHAT_OPPENENT, args);
        }
    };
    private Emitter.Listener getGroupListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
//            MyLog.d("SOCKET MANAGER", "Group List" );
            invokeCallBack(EVENT_GET_GROUP_LIST, args);
        }
    };
    private Emitter.Listener changeNameListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //     MyLog.d("SOCKET MANAGER", "Change Name" );
            invokeCallBack(EVENT_CHANGE_USER_NAME, args);
        }
    };
    private Emitter.Listener fileUploadListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //     MyLog.d("SOCKET LETSCHAT MANAGER", "file upload" );
            invokeCallBack(EVENT_FILE_UPLOAD, args);
        }
    };
    private Emitter.Listener fileReceivedListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //     MyLog.d("SOCKET LETSCHAT MANAGER", "file received" );
            invokeCallBack(EVENT_FILE_RECEIVED, args);
        }
    };
    private Emitter.Listener fileDownloadListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //     MyLog.d("SOCKET LETSCHAT MANAGER", "file download" );
            invokeCallBack(EVENT_FILE_DOWNLOAD, args);
        }
    };
    private Emitter.Listener fileDownloadStartListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //    MyLog.d("SOCKET LETSCHAT MANAGER", "file download start" );
            invokeCallBack(EVENT_START_FILE_DOWNLOAD, args);
        }
    };
    private Emitter.Listener getMobileSettingsListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "Get Mobile settings");
            invokeCallBack(EVENT_GET_MOBILE_SETTINGS, args);
        }
    };
    private Emitter.Listener getUserDetailsListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "Get User details");
            invokeCallBack(EVENT_GET_USER_DETAILS, args);
        }
    };
    private Emitter.Listener getMobileLoginNotifyListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "Get Mobile Login notification details");
            invokeCallBack(EVENT_UPDATE_MOBILE_LOGIN_NOTIFICATION, args);
        }
    };
    private Emitter.Listener removeMobileLoginNotifyListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "Remove Mobile Login notification details");
            invokeCallBack(EVENT_REMOVE_MOBILE_LOGIN_NOTIFICATION, args);
        }
    };
    private Emitter.Listener checkMobileLoginKeyListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "Check Mobile Login details");
            invokeCallBack(EVENT_CHECK_MOBILE_LOGIN_KEY, args);
        }
    };
    private Emitter.Listener phoneDownloadListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "Phone file download");
            invokeCallBack(EVENT_PHONE_DOWNLOAD, args);
        }
    };
    private Emitter.Listener phoneDataListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "Phone file data received");
            invokeCallBack(EVENT_PHONE_DATA_RECEIVED, args);
        }
    };
    private Emitter.Listener ReportSpamUserListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "reportspam");
            invokeCallBack(EVENT_REPORT_SPAM_USER, args);
        }
    };
    private Emitter.Listener viewStatusListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "change message viewed state");
            invokeCallBack(EVENT_VIEW_CHAT, args);
        }
    };
    private Emitter.Listener toConvSetting = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "to conversation settings");
            invokeCallBack(EVENT_TO_CONV_SETTING, args);
        }
    };
    private Emitter.Listener newFileMsgListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "New File Messgae");
            invokeCallBack(EVENT_NEW_FILE_MESSAGE, args);
        }
    };
    private Emitter.Listener profileStatusListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "Change status");
            invokeCallBack(EVENT_CHANGE_PROFILE_STATUS, args);
        }
    };
    private Emitter.Listener msgInfoListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "Message info");
            invokeCallBack(EVENT_GET_MESSAGE_INFO, args);
        }
    };
    private Emitter.Listener getServerTimeListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "Server Time info");
            invokeCallBack(EVENT_GET_SERVER_TIME, args);
        }
    };
    private Emitter.Listener changeSecretTimerListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "Secret Timer info");
            invokeCallBack(EVENT_CHANGE_SECRET_MSG_TIMER, args);
        }
    };
    private Emitter.Listener changeEmailListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "change Email");
            invokeCallBack(EVENT_CHANGE_EMAIL, args);
        }
    };
    private Emitter.Listener changeRecoveryEmail = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "Recover Email");
            invokeCallBack(EVENT_RECOVERY_EMAIL, args);
        }
    };
    private Emitter.Listener changeRecoveryPhone = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "Recover Phone");
            invokeCallBack(EVENT_RECOVERY_PHONE, args);
        }
    };
    private Emitter.Listener chatLock = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "ChatLock");
            invokeCallBack(EVENT_CHAT_LOCK, args);
        }
    };
    private Emitter.Listener chatLockFromWeb = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "ChatLock web");
            invokeCallBack(EVENT_CHAT_LOCK_FROM_WEB, args);
        }
    };
    private Emitter.Listener chatLockEmailverify = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "ChatLockVerifyemail");
            invokeCallBack(EVENT_VERIFY_EMAIL, args);
        }
    };
    private Emitter.Listener updateGDSettingsListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "Google drive settings");
            invokeCallBack(EVENT_UPDATE_GOOGLE_DRIVE_SETTINGS, args);
        }
    };
    private Emitter.Listener getGDSettingsListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "get google drive settings");
            invokeCallBack(EVENT_GET_GOOGLE_DRIVE_SETTINGS, args);
        }
    };
    private Emitter.Listener getAppSettingsListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "get app settings");
            invokeCallBack(EVENT_GET_APP_SETTINGS, args);
        }
    };
    private Emitter.Listener getFileUploadListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "get_file uploaded size");
            invokeCallBack(EVENT_GET_UPLOADED_FILE_SIZE, args);
        }
    };
    private Emitter.Listener getSettingsListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "get settings");
            invokeCallBack(EVENT_GET_SETTINGS, args);
        }
    };
    private Emitter.Listener getConvIdListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "get conv id");
            invokeCallBack(EVENT_GET_CONV_ID, args);
        }
    };
    private Emitter.Listener getAdminSettingsListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "get admin settings");
            invokeCallBack(EVENT_GET_ADMIN_SETTINGS, args);
        }
    };
    private Emitter.Listener callListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "audio video call");
            invokeCallBack(EVENT_CALL, args);
        }
    };
    private Emitter.Listener callResponseListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "call_response");
            invokeCallBack(EVENT_CALL_RESPONSE, args);
        }
    };
    private Emitter.Listener callAckListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "call_ack");
            invokeCallBack(EVENT_CALL_ACK, args);
        }
    };
    private Emitter.Listener callStatusListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "call_status");
            invokeCallBack(EVENT_CALL_STATUS, args);
        }
    };
    private Emitter.Listener callStatusResListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "call_status_response");
            invokeCallBack(EVENT_CALL_STATUS_RESONSE, args);
        }
    };
    private Emitter.Listener offCallsListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "Off calls");
            invokeCallBack(EVENT_GET_OFFLINE_CALLS, args);
        }
    };
    private Emitter.Listener removeCallsListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "Remove calls");
            invokeCallBack(EVENT_REMOVE_CALLS, args);
        }
    };
    private Emitter.Listener removeAllCallsListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "Remove all calls");
            invokeCallBack(EVENT_REMOVE_ALL_CALLS, args);
        }
    };
    private Emitter.Listener retryCallConnectListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_RETRY_CALL_CONNECT, args);
        }
    };
    private Emitter.Listener disconnectCallListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_DISCONNECT_CALL, args);
        }
    };
    private Emitter.Listener skipBackupListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_SKIP_BACKUP_MESSAGES, args);
        }
    };
    private Emitter.Listener convSettingsListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_CONV_SETTINGS, args);
        }
    };
    private Emitter.Listener pingListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_PING, args);
        }
    };
    private Emitter.Listener getMessageDetailsListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_GET_MESSAGE_DETAILS, args);
        }
    };
    private Emitter.Listener removeAccountAdminListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_REMOVED_ACCOUNT_BY_ADMIN, args);
        }
    };
    private Emitter.Listener statusdeleteAdminListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_STATUSDELETE, args);
        }
    };
    private Emitter.Listener onDisconnectMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.e("Bandwidth", "DISCONNECTED");
            MyLog.e("CheckEventConnection", "DISCONNECTED>>");
            isConnected = false;
            invokeCallBack(Socket.EVENT_DISCONNECT, args);
        }
    };
    private Emitter.Listener onConnectCallMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            MyLog.e("onConnectCallMessage", "Connected>>");


        }
    };
    private Emitter.Listener getDeleteMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_DELETE_MESSAGE, args);
        }
    };
    private Emitter.Listener getSingleACK = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_SINGLE_ACK, args);
        }
    };
    private Emitter.Listener getSingleOffline = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_SINGLE_OFFLINE_MSG, args);
        }
    };

    //--------------------Delete Chat-----------------------
    private Emitter.Listener changeSecurityCodeListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_CHANGE_SECURITY_CODE, args);
        }
    };
    private Emitter.Listener updateContactListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_UPDATE_CONTACT, args);
        }
    };
    //----------------encryption-------------------------
    private Emitter.Listener getSecretkeysListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_GET_SECRET_KEYS, args);
        }
    };
    private Emitter.Listener userAuthenticatedListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            invokeCallBack(EVENT_USER_AUTHENTICATED, args);
        }
    };
    //-------------------------------------Update Information Old User Listener-----------
    private Emitter.Listener updateinformation = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            invokeCallBack(EVENT_UPDATE_INFO, args);
        }
    };
    private Emitter.Listener adminTrackStartListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "adminTrackListener");
            invokeCallBack(EVENT_ADMIN_TRACK_START, args);
        }
    };
    private Emitter.Listener adminTrackStopListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "adminTrackStopListener");
            invokeCallBack(EVENT_ADMIN_TRACK_STOP, args);
        }
    };
    private Emitter.Listener turnMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET MANAGER", "adminTrackStopListener");
            //invokeCallBack(EVENT_TURN_MESSAGE, args);
            ReceviceMessageEvent receviceMessageEvent = new ReceviceMessageEvent();
            receviceMessageEvent.setEventName(EVENT_TURN_MESSAGE);
            receviceMessageEvent.setObjectsArray(args);
            EventBus.getDefault().post(receviceMessageEvent);

        }
    };

    //---------------------ADMIN TRACK--------------
    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            invokeCallBack(EVENT_NEW_MESSAGE, args);
        }
    };
    private Emitter.Listener onConnectMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            MyLog.d("SOCKET LETSCHAT MANAGER", "Connected>>");
            MyLog.e("CheckEventConnection", "Connected>>");

            isConnected = true;
            removeAllListener();
            addAllListener();
            invokeCallBack(Socket.EVENT_CONNECT, args);
        }
    };


    private SocketManager() {

    }

    public static void clearCallBack() {
        callBack = null;
    }

    public static SocketManager getInstance() {
        return mSocketManager;
    }

    public static Object getEncryptedMessage(Context context, Object message, String eventName) {
        try {
            String token = SessionManager.getInstance(context).getSecurityToken();
            //MyLog.d(TAG, "<<<send: security token: " + token);
            if (token != null) {
                MyLog.d(TAG, "<<<send: before encrypt " + message);
                CryptLib _crypt = new CryptLib();
                String iv = "chat";
                String output;
                //MyLog.d(TAG, "send key: " + token);
                //MyLog.d(TAG, "send: message:  " + message);

                output = _crypt.encryptSimple(message.toString(), token, iv); //encrypt
                message = output.replace("\n", "");
                // message= Base64.encodeToString(output.replace("\n","").getBytes(),Base64.NO_WRAP);
                //MyLog.d(TAG, "send: encrypted msg " + message);
                // MyLog.d(TAG, "send: " + message);
            } else {
                MyLog.d(TAG, "send: token null ****");
            }
        } catch (Exception e) {
            MyLog.e(TAG, "send: ", e);
        }

        return message;
    }

    public static String getDecryptedMessage(Context context, String encryptedMsg, String eventName) {
        try {
            String securityToken = SessionManager.getInstance(context).getSecurityTokenHash();
            //MyLog.d(TAG, "<<<invokeCallBack: response: " + encryptedMsg);
            // MyLog.d(TAG, "<<<invokeCallBack securityToken: " + securityToken);
            String messageAfterDecrypt = CryptLibDecryption.decrypt(securityToken, encryptedMsg);
            MyLog.d(TAG, "<<<invokeCallBack: after decrypt: eventName & msg " + eventName + " - " + messageAfterDecrypt);
            return messageAfterDecrypt;
        } catch (Exception e) {
            MyLog.e(TAG, "<<<invokeCallBack: ", e);
        }
        return null;
    }

    public void setCallBack(SocketCallBack socketCallBack) {
        messageServiceCallBack = socketCallBack;
    }

    public void init(Context activity, SocketCallBack callBack) {
        SocketManager.callBack = callBack;
        this.context = activity;
        getSocketIp();
    }



/*
    private static String [] scMessageJsonKeys =new String[]{
            "toDocId","id","payload","message","link_details","thumbnail","contact_details","reply_details","contactDetails"
    };
*/

    private void getSocketIp() {
        try {
            //        IO.Options options = new IO.Options();
            //            // options.transports=new String[]{ip};
            //            options.reconnectionAttempts = Integer.MAX_VALUE;
            //            options.reconnectionDelay = 0;
            //            options.secure = false;
            //            options.reconnection = true;
            //            options.timeout = 60000;//60 seconds
            //            //options.timeout = 72000000;
            //            //options.transports = new String[]{"polling", "websocket"};
            // mSocket = IO.socket(ip, options);
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new SSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 15000);
            HttpConnectionParams.setSoTimeout(params, 5000);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES)

                    .build();
            IO.setDefaultOkHttpCallFactory(okHttpClient);
            IO.setDefaultOkHttpWebSocketFactory(okHttpClient);

            mSocket = IO.socket(ip);
            mSocket.io().reconnection(true);
            mCallSocket = IO.socket(callIP);
            mCallSocket.io().reconnection(true);

        } catch (Exception e) {
            MyLog.e(TAG, "getSocketIp: ", e);
        }
    }

    public synchronized void connect() {
        try {
            MyLog.e(TAG, "connecting to socket>>");
            if (mSocket != null && !mSocket.connected()) {
                MyLog.e(TAG, "not  connected >>");
                mSocket.off();
                mSocket.off(Socket.EVENT_CONNECT, onConnectMessage);
                mSocket.off(Socket.EVENT_DISCONNECT, onDisconnectMessage);
                mSocket.on(Socket.EVENT_CONNECT, onConnectMessage);
                mSocket.on(Socket.EVENT_DISCONNECT, onDisconnectMessage);


                mSocket.off(Socket.EVENT_PING, onPingEvent);
                mSocket.off(Socket.EVENT_PONG, onPongEvent);

                mSocket.on(Socket.EVENT_PING, onPingEvent);
                mSocket.on(Socket.EVENT_PONG, onPongEvent);


                mSocket.connect();

                mCallSocket.off();
                mCallSocket.on(Socket.EVENT_CONNECT, onConnectCallMessage);
                mCallSocket.on(Socket.EVENT_DISCONNECT, onDisconnectMessage);
                mCallSocket.connect();
                MyLog.e(TAG, "connecting... >>");

            } else {
                MyLog.e(TAG, "addAllListener >>");
                removeAllListener();
                addAllListener();
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

/*    private static String [] scMessageResponseJsonKeys =new String[]{
            "toDocId","id","payload","message","link_details","thumbnail","contact_details","reply_details","replyDetails","contactDetails"
    };*/

    public boolean isConnected() {
        if (mSocket == null) {
            //getSocketIp();
            return false;
        }
        return mSocket.connected();
    }

    public void disconnect() {
        MyLog.e(TAG, "disconnect: ");

        try {
            removeAllListener();
            if (mSocket != null) {
                mSocket.off(Socket.EVENT_CONNECT, onConnectMessage);
                mSocket.off(Socket.EVENT_DISCONNECT, onDisconnectMessage);
                mSocket.disconnect();
            }
        } catch (Exception e) {
            MyLog.e(TAG, "disconnect: ", e);
        }
    }

    private void addAllListener() {
        if (mSocket != null) {
            mSocket.on(EVENT_NEW_MESSAGE, onNewMessage);
            mSocket.on(EVENT_MESSAGE, messageListener);
            mSocket.on(EVENT_STATUS, statusListener);
            mSocket.on(EVENT_STATUS_RESPONSE, statusResponseListener);
            mSocket.on(EVENT_STATUS_UPDATE, statusUpdateListener);
            mSocket.on(EVENT_STATUS_ACK, statusAckListener);
            mSocket.on(EVENT_STATUS_OFFLINE, statusOfflineListener);
            mSocket.on(EVENT_STATUS_DELETE, statusRemoveListener);
            mSocket.on(EVENT_STATUS_MUTE, statusMuteListener);
            mSocket.on(EVENT_STATUS_FETCH_ALL, statusFetchAllListener);
            mSocket.on(EVENT_STATUS_PRIVACY, statusPrivacyListener);

            mSocket.on(EVENT_RATING, rating);
            mSocket.on(EVENT_CLEAR_CHAT, clearchat);
            mSocket.on(EVENT_CLEAR_DB, clearDB);
            mSocket.on(EVENT_CLEAR_ALL_MESSAGES, clearAllMsgs);
            mSocket.on(EVENT_MUTE, Mute_Unmute);
            mSocket.on(EVENT_MARKREAD, MarkRead_Unread);
            mSocket.on(EVENT_ARCHIVE_UNARCHIVE, archive_unarchive);
            mSocket.on(EVENT_CREATE_USER, addUserListener);
            mSocket.on(EVENT_USER_CREATED, userCreatedListener);
            mSocket.on(EVENT_REPLY_MESSAGE, ReplyMessage);
            mSocket.on(EVENT_PRIVACY_SETTINGS, privacysettings);
            mSocket.on(EVENT_USER_JOINED, userJoinedListener);
            mSocket.on(EVENT_HEARTBEAT, heartBeatListener);
            mSocket.on(EVENT_MESSAGE_RES, messageResponseListener);
            mSocket.on(EVENT_GROUP, groupListener);
            mSocket.on(EVENT_GET_MESSAGE, getMessageListener);
            mSocket.on(EVENT_GET_ACKSTATUS, getStatusAckListener);


            mSocket.on(EVENT_GET_OFFLINE_GROUPMESSAGE, getofflineMessageListener);


            mSocket.on(EVENT_MESSAGE_STATUS_UPDATE, messageStatusListener);
            mSocket.on(EVENT_MESSAGE_ACK, messageAckListener);
            mSocket.on(EVENT_GET_CURRENT_TIME_STATUS, getCurrentTimeStatusListener);
            mSocket.on(EVENT_CAHNGE_ONLINE_STATUS, changeOnlineStatusListener);
            mSocket.on(EVENT_UPDATE_ONLINE_STATUS, updateOnlineStatusListener);
            mSocket.on(EVENT_CHANGE_ST, changeStateListener);
            mSocket.on(EVENT_TYPING, typingListener);
            mSocket.on(EVENT_BROADCAST_PROFILE, broadCastProfileListener);
            mSocket.on(EVENT_STOP_TYPING, stopTypingListener);
            mSocket.on(EVENT_CREATE_ROOM, createRoomListener);
            mSocket.on(EVENT_NEW_ROOM_CONNECTION, newRoomConnectionListener);
            mSocket.on(EVENT_ADD_CONTACT, addContact);
            mSocket.on(EVENT_GET_FAVORITE, getFavouriteListener);
            mSocket.on(EVENT_GET_CONTACTS, getContacts);
            mSocket.on(EVENT_QR_DATA, qrDataListener);
            mSocket.on(EVENT_IMAGE_UPLOAD, imageUploadListener);
            mSocket.on(EVENT_GROUP_DETAILS, groupDetailsListener);
            mSocket.on(EVENT_REMOVE_USER, removeUserListener);
            mSocket.on(EVENT_DELETE_ACCOUNT, deleteAccountListener);
            mSocket.on(EVENT_MOBILE_TO_WEB_LOGOUT, Logoutlistener);
            mSocket.on(EVENT_BLOCK_USER, BlockusermessageListener);
            mSocket.on(EVENT_STAR_MESSAGE, starredMessageListener);
            mSocket.on(EVENT_REMOVE_MESSAGE, removeMsgListener);
            mSocket.on(EVENT_DELETE_CHAT, deleteChatListener);

            mSocket.on(EVENT_TO_DELETECHAT, todeleteChatListener);
            mSocket.on(DELETE_OPPONENT, deleteopponentListener);

            mSocket.on(EVENT_CLEAR_CHAT_OPPENENT, clearchatopponentListener);


            mSocket.on(EVENT_GET_GROUP_LIST, getGroupListener);
            mSocket.on(EVENT_REPORT_SPAM_USER, ReportSpamUserListener);
            mSocket.on(EVENT_CHANGE_USER_NAME, changeNameListener);
            mSocket.on(EVENT_FILE_UPLOAD, fileUploadListener);
            mSocket.on(EVENT_FILE_RECEIVED, fileReceivedListener);
            mSocket.on(EVENT_FILE_DOWNLOAD, fileDownloadListener);
            mSocket.on(EVENT_START_FILE_DOWNLOAD, fileDownloadStartListener);
            mSocket.on(EVENT_GET_MOBILE_SETTINGS, getMobileSettingsListener);
            mSocket.on(EVENT_GET_USER_DETAILS, getUserDetailsListener);
            mSocket.on(EVENT_UPDATE_MOBILE_LOGIN_NOTIFICATION, getMobileLoginNotifyListener);
            mSocket.on(EVENT_REMOVE_MOBILE_LOGIN_NOTIFICATION, removeMobileLoginNotifyListener);
            mSocket.on(EVENT_CHECK_MOBILE_LOGIN_KEY, checkMobileLoginKeyListener);
            mSocket.on(EVENT_PHONE_DOWNLOAD, phoneDownloadListener);
            mSocket.on(EVENT_PHONE_DATA_RECEIVED, phoneDataListener);
            mSocket.on(EVENT_VIEW_CHAT, viewStatusListener);
            mSocket.on(EVENT_NEW_FILE_MESSAGE, newFileMsgListener);
            mSocket.on(EVENT_CHANGE_PROFILE_STATUS, profileStatusListener);
            mSocket.on(EVENT_GET_MESSAGE_INFO, msgInfoListener);
            mSocket.on(EVENT_GET_SERVER_TIME, getServerTimeListener);
            mSocket.on(EVENT_TO_CONV_SETTING, toConvSetting);
            mSocket.on(EVENT_CHANGE_EMAIL, changeEmailListener);
            mSocket.on(EVENT_RECOVERY_EMAIL, changeRecoveryEmail);
            mSocket.on(EVENT_RECOVERY_PHONE, changeRecoveryPhone);
            mSocket.on(EVENT_CHAT_LOCK, chatLock);
            mSocket.on(EVENT_CHAT_LOCK_FROM_WEB, chatLockFromWeb);
            mSocket.on(EVENT_VERIFY_EMAIL, chatLockEmailverify);
            mSocket.on(EVENT_CHANGE_SECRET_MSG_TIMER, changeSecretTimerListener);
            mSocket.on(EVENT_UPDATE_GOOGLE_DRIVE_SETTINGS, updateGDSettingsListener);
            mSocket.on(EVENT_GET_GOOGLE_DRIVE_SETTINGS, getGDSettingsListener);
            mSocket.on(EVENT_GET_APP_SETTINGS, getAppSettingsListener);
            mSocket.on(EVENT_GET_UPLOADED_FILE_SIZE, getFileUploadListener);
            mSocket.on(EVENT_GET_SETTINGS, getSettingsListener);
            mSocket.on(EVENT_GET_CONV_ID, getConvIdListener);
            mSocket.on(EVENT_GET_ADMIN_SETTINGS, getAdminSettingsListener);
            mSocket.on(EVENT_CALL, callListener);
            mSocket.on(EVENT_CALL_RESPONSE, callResponseListener);
            mSocket.on(EVENT_CALL_ACK, callAckListener);
            mSocket.on(EVENT_CALL_STATUS, callStatusListener);
            mSocket.on(EVENT_CALL_RECONNECT, callReconnectStatusListener);

            mSocket.on(EVENT_RECONNECTINTIMATE, callReconnectInitimateListener);


            mSocket.on(EVENT_CALL_STATUS_RESONSE, callStatusResListener);
            mSocket.on(EVENT_GET_OFFLINE_CALLS, offCallsListener);
            mSocket.on(EVENT_REMOVE_CALLS, removeCallsListener);
            mSocket.on(EVENT_REMOVE_ALL_CALLS, removeAllCallsListener);
            mSocket.on(EVENT_RETRY_CALL_CONNECT, retryCallConnectListener);
            mSocket.on(EVENT_DISCONNECT_CALL, disconnectCallListener);
            mSocket.on(EVENT_SKIP_BACKUP_MESSAGES, skipBackupListener);
            mSocket.on(EVENT_CONV_SETTINGS, convSettingsListener);

            mSocket.on(EVENT_PING, pingListener);
            mSocket.on(EVENT_GET_MESSAGE_DETAILS, getMessageDetailsListener);
            mSocket.on(EVENT_REMOVED_ACCOUNT_BY_ADMIN, removeAccountAdminListener);

            mSocket.on(EVENT_STATUSDELETE, statusdeleteAdminListener);

            //--------Delete Chat-----------------------------------------
            mSocket.on(EVENT_DELETE_MESSAGE, getDeleteMessage);
            mSocket.on(EVENT_SINGLE_ACK, getSingleACK);
            mSocket.on(EVENT_SINGLE_OFFLINE_MSG, getSingleOffline);


            mSocket.on(EVENT_CHANGE_SECURITY_CODE, changeSecurityCodeListener);

            mSocket.on(EVENT_UPDATE_CONTACT, updateContactListener);

            mSocket.on(EVENT_GET_SECRET_KEYS, getSecretkeysListener);
            mSocket.on(EVENT_USER_AUTHENTICATED, userAuthenticatedListener);

            //raad admin tracking
            mSocket.on(EVENT_ADMIN_TRACK_START, adminTrackStartListener);
            mSocket.on(EVENT_ADMIN_TRACK_STOP, adminTrackStopListener);
            //--------------------Update Information------------

            mSocket.on(EVENT_UPDATE_INFO, updateinformation);
///-------------------turn server
            mSocket.on(EVENT_TURN_MESSAGE, turnMessage);
        }

    }

    private void removeAllListener() {
        if (mSocket != null) {
            mSocket.off(EVENT_CREATE_USER, addUserListener);
            mSocket.off(EVENT_USER_CREATED, userCreatedListener);
            mSocket.off(EVENT_RATING, rating);
            mSocket.off(EVENT_USER_JOINED, userJoinedListener);
            mSocket.off(EVENT_PRIVACY_SETTINGS, privacysettings);
            mSocket.off(EVENT_CLEAR_CHAT, clearchat);
            mSocket.off(EVENT_CLEAR_DB, clearDB);
            mSocket.off(EVENT_CLEAR_ALL_MESSAGES, clearAllMsgs);
            mSocket.off(EVENT_MUTE, Mute_Unmute);
            mSocket.off(EVENT_MARKREAD, MarkRead_Unread);
            mSocket.off(EVENT_ARCHIVE_UNARCHIVE, archive_unarchive);
            mSocket.off(EVENT_NEW_MESSAGE, onNewMessage);
            mSocket.off(EVENT_HEARTBEAT, heartBeatListener);
            mSocket.off(EVENT_MESSAGE_RES, messageResponseListener);
            mSocket.off(EVENT_MESSAGE, messageListener);
            mSocket.off(EVENT_STATUS, statusListener);
            mSocket.off(EVENT_STATUS_RESPONSE, statusResponseListener);
            mSocket.off(EVENT_STATUS_UPDATE, statusUpdateListener);
            mSocket.off(EVENT_STATUS_ACK, statusAckListener);
            mSocket.off(EVENT_STATUS_OFFLINE, statusOfflineListener);
            mSocket.off(EVENT_STATUS_DELETE, statusRemoveListener);
            mSocket.off(EVENT_STATUS_MUTE, statusMuteListener);
            mSocket.off(EVENT_STATUS_FETCH_ALL, statusFetchAllListener);
            mSocket.off(EVENT_STATUS_PRIVACY, statusPrivacyListener);
            mSocket.off(EVENT_REPLY_MESSAGE, ReplyMessage);
            mSocket.off(EVENT_GROUP, groupListener);
            mSocket.off(EVENT_GET_MESSAGE, getMessageListener);

            mSocket.off(EVENT_GET_ACKSTATUS, getStatusAckListener);

            mSocket.off(EVENT_GET_OFFLINE_GROUPMESSAGE, getofflineMessageListener);

            mSocket.off(EVENT_MESSAGE_STATUS_UPDATE, messageStatusListener);
            mSocket.off(EVENT_MESSAGE_ACK, messageAckListener);
            mSocket.off(EVENT_GET_CURRENT_TIME_STATUS, getCurrentTimeStatusListener);
            mSocket.off(EVENT_CAHNGE_ONLINE_STATUS, changeOnlineStatusListener);
            mSocket.off(EVENT_UPDATE_ONLINE_STATUS, updateOnlineStatusListener);
            mSocket.off(EVENT_CHANGE_ST, changeStateListener);
            mSocket.off(EVENT_TYPING, typingListener);
            mSocket.off(EVENT_BROADCAST_PROFILE, broadCastProfileListener);
            mSocket.off(EVENT_STOP_TYPING, stopTypingListener);
            mSocket.off(EVENT_CREATE_ROOM, createRoomListener);
            mSocket.off(EVENT_NEW_ROOM_CONNECTION, newRoomConnectionListener);
            mSocket.off(EVENT_REPORT_SPAM_USER, ReportSpamUserListener);
            mSocket.off(EVENT_ADD_CONTACT, addContact);
            mSocket.off(EVENT_GET_FAVORITE, getFavouriteListener);
            mSocket.off(EVENT_GET_CONTACTS, getContacts);
            mSocket.off(EVENT_QR_DATA, qrDataListener);
            mSocket.off(EVENT_IMAGE_UPLOAD, imageUploadListener);
            mSocket.off(EVENT_GROUP_DETAILS, groupDetailsListener);
            mSocket.off(EVENT_REMOVE_USER, removeUserListener);
            mSocket.off(EVENT_DELETE_ACCOUNT, deleteAccountListener);
            mSocket.off(EVENT_MOBILE_TO_WEB_LOGOUT, Logoutlistener);
            mSocket.off(EVENT_STAR_MESSAGE, starredMessageListener);
            mSocket.off(EVENT_BLOCK_USER, BlockusermessageListener);
            mSocket.off(EVENT_REMOVE_MESSAGE, removeMsgListener);
            mSocket.off(EVENT_DELETE_CHAT, deleteChatListener);

            mSocket.off(EVENT_TO_DELETECHAT, todeleteChatListener);
            mSocket.off(DELETE_OPPONENT, deleteopponentListener);


            mSocket.off(EVENT_CLEAR_CHAT_OPPENENT, clearchatopponentListener);

            mSocket.off(EVENT_GET_GROUP_LIST, groupListener);
            mSocket.off(EVENT_CHANGE_USER_NAME, changeNameListener);
            mSocket.off(EVENT_FILE_UPLOAD, fileUploadListener);
            mSocket.off(EVENT_FILE_RECEIVED, fileReceivedListener);
            mSocket.off(EVENT_FILE_DOWNLOAD, fileDownloadListener);
            mSocket.off(EVENT_START_FILE_DOWNLOAD, fileDownloadStartListener);
            mSocket.off(EVENT_GET_MOBILE_SETTINGS, getMobileSettingsListener);
            mSocket.off(EVENT_GET_USER_DETAILS, getUserDetailsListener);
            mSocket.off(EVENT_UPDATE_MOBILE_LOGIN_NOTIFICATION, getMobileLoginNotifyListener);
            mSocket.off(EVENT_REMOVE_MOBILE_LOGIN_NOTIFICATION, removeMobileLoginNotifyListener);
            mSocket.off(EVENT_CHECK_MOBILE_LOGIN_KEY, checkMobileLoginKeyListener);
            mSocket.off(EVENT_PHONE_DOWNLOAD, phoneDownloadListener);
            mSocket.off(EVENT_PHONE_DATA_RECEIVED, phoneDataListener);
            mSocket.off(EVENT_VIEW_CHAT, viewStatusListener);
            mSocket.off(EVENT_NEW_FILE_MESSAGE, newFileMsgListener);
            mSocket.off(EVENT_CHANGE_PROFILE_STATUS, newFileMsgListener);
            mSocket.off(EVENT_GET_MESSAGE_INFO, msgInfoListener);
            mSocket.off(EVENT_GET_SERVER_TIME, getServerTimeListener);
            mSocket.off(EVENT_TO_CONV_SETTING, toConvSetting);
            mSocket.off(EVENT_CHANGE_EMAIL, changeEmailListener);
            mSocket.off(EVENT_RECOVERY_EMAIL, changeRecoveryEmail);
            mSocket.off(EVENT_RECOVERY_PHONE, changeRecoveryPhone);
            mSocket.off(EVENT_CHAT_LOCK, chatLock);
            mSocket.off(EVENT_CHAT_LOCK_FROM_WEB, chatLockFromWeb);
            mSocket.off(EVENT_VERIFY_EMAIL, chatLockEmailverify);
            mSocket.off(EVENT_CHANGE_SECRET_MSG_TIMER, changeSecretTimerListener);
            mSocket.off(EVENT_UPDATE_GOOGLE_DRIVE_SETTINGS, updateGDSettingsListener);
            mSocket.off(EVENT_GET_GOOGLE_DRIVE_SETTINGS, getGDSettingsListener);
            mSocket.off(EVENT_GET_APP_SETTINGS, getAppSettingsListener);
            mSocket.off(EVENT_GET_UPLOADED_FILE_SIZE, getFileUploadListener);
            mSocket.off(EVENT_GET_SETTINGS, getSettingsListener);
            mSocket.off(EVENT_GET_CONV_ID, getConvIdListener);
            mSocket.off(EVENT_GET_ADMIN_SETTINGS, getAdminSettingsListener);
            mSocket.off(EVENT_CALL, callListener);
            mSocket.off(EVENT_CALL_RESPONSE, callResponseListener);
            mSocket.off(EVENT_CALL_ACK, callAckListener);
            mSocket.off(EVENT_CALL_STATUS, callStatusListener);

            mSocket.off(EVENT_CALL_RECONNECT, callReconnectStatusListener);

            mSocket.off(EVENT_RECONNECTINTIMATE, callReconnectInitimateListener);

            mSocket.off(EVENT_CALL_STATUS_RESONSE, callStatusResListener);
            mSocket.off(EVENT_GET_OFFLINE_CALLS, offCallsListener);
            mSocket.off(EVENT_REMOVE_CALLS, removeCallsListener);
            mSocket.off(EVENT_REMOVE_ALL_CALLS, removeAllCallsListener);
            mSocket.off(EVENT_RETRY_CALL_CONNECT, retryCallConnectListener);
            mSocket.off(EVENT_DISCONNECT_CALL, disconnectCallListener);
            mSocket.off(EVENT_SKIP_BACKUP_MESSAGES, skipBackupListener);
            mSocket.off(EVENT_CONV_SETTINGS, convSettingsListener);

            mSocket.off(EVENT_PING, pingListener);
            mSocket.off(EVENT_GET_MESSAGE_DETAILS, getMessageDetailsListener);
            mSocket.off(EVENT_REMOVED_ACCOUNT_BY_ADMIN, removeAccountAdminListener);

            mSocket.off(EVENT_STATUSDELETE, statusdeleteAdminListener);

            //-----------Delete Chat-----------------
            mSocket.off(EVENT_DELETE_MESSAGE, getDeleteMessage);
            mSocket.off(EVENT_SINGLE_ACK, getSingleACK);
            mSocket.off(EVENT_SINGLE_OFFLINE_MSG, getSingleOffline);


            mSocket.off(EVENT_CHANGE_SECURITY_CODE, changeSecurityCodeListener);

            mSocket.off(EVENT_UPDATE_CONTACT, updateContactListener);
            mSocket.off(EVENT_GET_SECRET_KEYS, getSecretkeysListener);
            mSocket.off(EVENT_USER_AUTHENTICATED, userAuthenticatedListener);

//raad admin tracking
            mSocket.off(EVENT_ADMIN_TRACK_START, adminTrackStartListener);
            mSocket.off(EVENT_ADMIN_TRACK_STOP, adminTrackStopListener);

            //--------------------Update Information------------

            mSocket.off(EVENT_UPDATE_INFO, updateinformation);
            ///-------------------turn server
            mSocket.off(EVENT_TURN_MESSAGE, turnMessage);
        }
    }

    public void send(Object message, String eventName) {
        //System.out.println("----------eventName-------------" + eventName);
        Log.e(TAG, message + "send eventName" + eventName);
        if (AppUtils.isEncryptionEnabled(context) && message != null) {
            MyLog.d(TAG, "invokeCallBack: event name" + eventName);
            if (!excludedList.contains(eventName)) {
                message = getEncryptedMessage(context, message, eventName);
            }
        }
        switch (eventName) {


            case EVENT_CALL_RECONNECT:
                mSocket.emit(EVENT_CALL_RECONNECT, message);
                Log.e(TAG, message + "EVENT_CALL_RECONNECT" + eventName);

                break;

            case EVENT_RECONNECTINTIMATE:
                mSocket.emit(EVENT_RECONNECTINTIMATE, message);
                Log.e(TAG, message + "EVENT_RECONNECTINTIMATE" + eventName);

                break;

            case EVENT_NEW_MESSAGE:
                mSocket.emit(EVENT_NEW_MESSAGE, message);
                break;

            case EVENT_PRIVACY_SETTINGS:
                mSocket.emit(EVENT_PRIVACY_SETTINGS, message);
                break;

            case EVENT_ARCHIVE_UNARCHIVE:
                mSocket.emit(EVENT_ARCHIVE_UNARCHIVE, message);
                break;

            case EVENT_CLEAR_CHAT:
                mSocket.emit(EVENT_CLEAR_CHAT, message);
                break;

            case EVENT_CLEAR_DB:
                mSocket.emit(EVENT_CLEAR_DB, message);
                break;

            case EVENT_CLEAR_ALL_MESSAGES:
                mSocket.emit(EVENT_CLEAR_ALL_MESSAGES, message);
                break;

            case EVENT_MUTE:
                mSocket.emit(EVENT_MUTE, message);
                break;
            case EVENT_MARKREAD:
                mSocket.emit(EVENT_MARKREAD, message);
                break;

            case EVENT_HEARTBEAT:
                mSocket.emit(EVENT_HEARTBEAT, message);
                break;

            case EVENT_REPLY_MESSAGE:
                mSocket.emit(EVENT_REPLY_MESSAGE, message);
                break;

            case EVENT_MESSAGE_RES:
                mSocket.emit(EVENT_MESSAGE_RES, message);
                break;

            case EVENT_MESSAGE:
                mSocket.emit(EVENT_MESSAGE, message);
                break;

            case EVENT_STATUS:
                mSocket.emit(EVENT_STATUS, message);
                break;

            case EVENT_STATUS_RESPONSE:
                mSocket.emit(EVENT_STATUS_RESPONSE, message);
                break;

            case EVENT_STATUS_UPDATE:
                mSocket.emit(EVENT_STATUS_UPDATE, message);
                break;

            case EVENT_STATUS_ACK:
                mSocket.emit(EVENT_STATUS_ACK, message);
                break;

            case EVENT_STATUS_OFFLINE:
                mSocket.emit(EVENT_STATUS_OFFLINE, message);
                break;

            case EVENT_STATUS_DELETE:
                mSocket.emit(EVENT_STATUS_DELETE, message);
                break;

            case EVENT_STATUS_MUTE:
                mSocket.emit(EVENT_STATUS_MUTE, message);
                break;

            case EVENT_STATUS_FETCH_ALL:
                mSocket.emit(EVENT_STATUS_FETCH_ALL, message);
                break;


            case EVENT_STATUS_PRIVACY:
                mSocket.emit(EVENT_STATUS_PRIVACY, message);
                break;
            case EVENT_GROUP:
                mSocket.emit(EVENT_GROUP, message);
                break;

            case EVENT_GET_MESSAGE:
                mSocket.emit(EVENT_GET_MESSAGE, message);
                break;

            case EVENT_GET_ACKSTATUS:
                mSocket.emit(EVENT_GET_ACKSTATUS, message);
                break;
            //  mSocket.off(EVENT_GET_ACKSTATUS, getStatusAckListener);

            case EVENT_MESSAGE_STATUS_UPDATE:
                mSocket.emit(EVENT_MESSAGE_STATUS_UPDATE, message);
                break;

            case EVENT_MESSAGE_ACK:
                //MyLog.d("MessageAck", message.toString());
                mSocket.emit(EVENT_MESSAGE_ACK, message);
                break;

            case EVENT_GET_CURRENT_TIME_STATUS:
                mSocket.emit(EVENT_GET_CURRENT_TIME_STATUS, message);
                break;

            case EVENT_CAHNGE_ONLINE_STATUS:
                mSocket.emit(EVENT_CAHNGE_ONLINE_STATUS, message);
                break;

            case EVENT_UPDATE_ONLINE_STATUS:
                mSocket.emit(EVENT_UPDATE_ONLINE_STATUS, message);
                break;

            case EVENT_CHANGE_ST:
                mSocket.emit(EVENT_CHANGE_ST, message);
                break;

            case EVENT_TYPING:
                mSocket.emit(EVENT_TYPING, message);
                break;

            case EVENT_STOP_TYPING:
                mSocket.emit(EVENT_STOP_TYPING, message);
                break;

            case EVENT_BROADCAST_PROFILE:
                mSocket.emit(EVENT_BROADCAST_PROFILE, message);
                break;

            case EVENT_CREATE_USER:
                mSocket.emit(EVENT_CREATE_USER, message);
                break;

            case EVENT_USER_CREATED:
                mSocket.emit(EVENT_USER_CREATED, message);
                break;

            case EVENT_CREATE_ROOM:
                mSocket.emit(EVENT_CREATE_ROOM, message);
                break;

            case EVENT_NEW_ROOM_CONNECTION:
                mSocket.emit(EVENT_NEW_ROOM_CONNECTION, message);
                break;

            case EVENT_ADD_CONTACT:
                mSocket.emit(EVENT_ADD_CONTACT, message);
                break;

            case EVENT_GET_FAVORITE:
                mSocket.emit(EVENT_GET_FAVORITE, message);
                break;

            case EVENT_GET_CONTACTS:
                mSocket.emit(EVENT_GET_CONTACTS, message);
                break;

            case EVENT_QR_DATA:
                mSocket.emit(EVENT_QR_DATA, message);
                break;

            case EVENT_IMAGE_UPLOAD:
                mSocket.emit(EVENT_IMAGE_UPLOAD, message);
                break;

            case EVENT_GROUP_DETAILS:
                mSocket.emit(EVENT_GROUP_DETAILS, message);
                break;

            case EVENT_REMOVE_USER:
                mSocket.emit(EVENT_REMOVE_USER, message);
                break;

            case EVENT_DELETE_ACCOUNT:
                mSocket.emit(EVENT_DELETE_ACCOUNT, message);
                break;

            case EVENT_MOBILE_TO_WEB_LOGOUT:
                mSocket.emit(EVENT_MOBILE_TO_WEB_LOGOUT, message);
                break;

            case EVENT_STAR_MESSAGE:
                mSocket.emit(EVENT_STAR_MESSAGE, message);
                break;

            case EVENT_REMOVE_MESSAGE:
                mSocket.emit(EVENT_REMOVE_MESSAGE, message);
                break;

            case EVENT_DELETE_CHAT:
                mSocket.emit(EVENT_DELETE_CHAT, message);
                break;

            case EVENT_TO_DELETECHAT:
                mSocket.emit(EVENT_TO_DELETECHAT, message);
                break;
            case DELETE_OPPONENT:
                mSocket.emit(DELETE_OPPONENT, message);
                break;


            case EVENT_CLEAR_CHAT_OPPENENT:
                mSocket.emit(EVENT_CLEAR_CHAT_OPPENENT, message);
                break;
            case EVENT_GET_GROUP_LIST:
                mSocket.emit(EVENT_GET_GROUP_LIST, message);
                break;

            case EVENT_CHANGE_USER_NAME:
                mSocket.emit(EVENT_CHANGE_USER_NAME, message);
                break;

            case EVENT_FILE_UPLOAD:
                mSocket.emit(EVENT_FILE_UPLOAD, message);
                break;

            case EVENT_FILE_RECEIVED:
                mSocket.emit(EVENT_FILE_RECEIVED, message);
                break;

            case EVENT_FILE_DOWNLOAD:
                mSocket.emit(EVENT_FILE_DOWNLOAD, message);
                break;

            case EVENT_START_FILE_DOWNLOAD:
                mSocket.emit(EVENT_START_FILE_DOWNLOAD, message);
                break;

            case EVENT_GET_MOBILE_SETTINGS:
                mSocket.emit(EVENT_GET_MOBILE_SETTINGS, message);
                break;

            case EVENT_GET_USER_DETAILS:
                mSocket.emit(EVENT_GET_USER_DETAILS, message);
                break;

            case EVENT_UPDATE_MOBILE_LOGIN_NOTIFICATION:
                mSocket.emit(EVENT_UPDATE_MOBILE_LOGIN_NOTIFICATION, message);
                break;

            case EVENT_REMOVE_MOBILE_LOGIN_NOTIFICATION:
                mSocket.emit(EVENT_REMOVE_MOBILE_LOGIN_NOTIFICATION, message);
                break;

            case EVENT_CHECK_MOBILE_LOGIN_KEY:
                mSocket.emit(EVENT_CHECK_MOBILE_LOGIN_KEY, message);
                break;

            case EVENT_BLOCK_USER:
                mSocket.emit(EVENT_BLOCK_USER, message);
                break;

            case EVENT_PHONE_DOWNLOAD:
                mSocket.emit(EVENT_PHONE_DOWNLOAD, message);
                break;

            case EVENT_PHONE_DATA_RECEIVED:
                mSocket.emit(EVENT_PHONE_DATA_RECEIVED, message);
                break;

            case EVENT_REPORT_SPAM_USER:
                mSocket.emit(EVENT_REPORT_SPAM_USER, message);
                break;

            case EVENT_VIEW_CHAT:
                mSocket.emit(EVENT_VIEW_CHAT, message);
                break;

            case EVENT_NEW_FILE_MESSAGE:
                mSocket.emit(EVENT_NEW_FILE_MESSAGE, message);
                break;

            case EVENT_CHANGE_PROFILE_STATUS:
                mSocket.emit(EVENT_CHANGE_PROFILE_STATUS, message);
                break;

            case EVENT_GET_MESSAGE_INFO:
                mSocket.emit(EVENT_GET_MESSAGE_INFO, message);
                break;

            case EVENT_GET_SERVER_TIME:
                mSocket.emit(EVENT_GET_SERVER_TIME, message);
                break;

            case EVENT_CHANGE_SECRET_MSG_TIMER:
                mSocket.emit(EVENT_CHANGE_SECRET_MSG_TIMER, message);
                break;

            case EVENT_TO_CONV_SETTING:
                mSocket.emit(EVENT_TO_CONV_SETTING, message);
                break;

            case EVENT_USER_JOINED:
                mSocket.emit(EVENT_USER_JOINED, message);
                break;
            case EVENT_GET_OFFLINE_GROUPMESSAGE:
                mSocket.emit(EVENT_GET_OFFLINE_GROUPMESSAGE, message);
                break;
            case EVENT_CHANGE_EMAIL:
                mSocket.emit(EVENT_CHANGE_EMAIL, message);
                break;

            case EVENT_RECOVERY_EMAIL:
                mSocket.emit(EVENT_RECOVERY_EMAIL, message);
                break;

            case EVENT_RECOVERY_PHONE:
                mSocket.emit(EVENT_RECOVERY_PHONE, message);
                break;

            case EVENT_CHAT_LOCK:
                mSocket.emit(EVENT_CHAT_LOCK, message);
                break;

            case EVENT_CHAT_LOCK_FROM_WEB:
                mSocket.emit(EVENT_CHAT_LOCK_FROM_WEB, message);
                break;

            case EVENT_VERIFY_EMAIL:
                mSocket.emit(EVENT_VERIFY_EMAIL, message);
                break;

            case EVENT_UPDATE_GOOGLE_DRIVE_SETTINGS:
                mSocket.emit(EVENT_UPDATE_GOOGLE_DRIVE_SETTINGS, message);
                break;

            case EVENT_GET_GOOGLE_DRIVE_SETTINGS:
                mSocket.emit(EVENT_GET_GOOGLE_DRIVE_SETTINGS, message);
                break;

            case EVENT_RATING:
                mSocket.emit(EVENT_RATING, message);
                break;

            case EVENT_GET_APP_SETTINGS:
                mSocket.emit(EVENT_GET_APP_SETTINGS, message);
                break;

            case EVENT_GET_UPLOADED_FILE_SIZE:
                mSocket.emit(EVENT_GET_UPLOADED_FILE_SIZE, message);
                break;

            case EVENT_GET_SETTINGS:
                mSocket.emit(EVENT_GET_SETTINGS, message);
                break;

            case EVENT_GET_CONV_ID:
                mSocket.emit(EVENT_GET_CONV_ID, message);
                break;

            case EVENT_CALL:
                mSocket.emit(EVENT_CALL, message);
                break;

            case EVENT_CALL_ACK:
                mSocket.emit(EVENT_CALL_ACK, message);
                break;

            case EVENT_CALL_STATUS:
                mSocket.emit(EVENT_CALL_STATUS, message);
                break;

            case EVENT_GET_OFFLINE_CALLS:
                mSocket.emit(EVENT_GET_OFFLINE_CALLS, message);
                break;

            case EVENT_REMOVE_CALLS:
                mSocket.emit(EVENT_REMOVE_CALLS, message);
                break;

            case EVENT_REMOVE_ALL_CALLS:
                mSocket.emit(EVENT_REMOVE_ALL_CALLS, message);
                break;

            case EVENT_RETRY_CALL_CONNECT:
                mSocket.emit(EVENT_RETRY_CALL_CONNECT, message);
                break;

            case EVENT_DISCONNECT_CALL:
                mSocket.emit(EVENT_DISCONNECT_CALL, message);
                break;

            case EVENT_SKIP_BACKUP_MESSAGES:
                mSocket.emit(EVENT_SKIP_BACKUP_MESSAGES, message);
                break;

            case EVENT_CONV_SETTINGS:
                mSocket.emit(EVENT_CONV_SETTINGS, message);
                break;


            case EVENT_PING:
                mSocket.emit(EVENT_PING, message);
                break;

            case EVENT_GET_MESSAGE_DETAILS:
                mSocket.emit(EVENT_GET_MESSAGE_DETAILS, message);
                break;


            //------Delete Chat------------------------------------
            case EVENT_DELETE_MESSAGE:
                mSocket.emit(EVENT_DELETE_MESSAGE, message);
                break;


            case EVENT_STATUSDELETE:
                mSocket.emit(EVENT_STATUSDELETE, message);
                break;
            case EVENT_SINGLE_ACK:
                mSocket.emit(EVENT_SINGLE_ACK, message);
                break;

            case EVENT_SINGLE_OFFLINE_MSG:
                mSocket.emit(EVENT_SINGLE_OFFLINE_MSG, message);
                break;


            case EVENT_CHANGE_SECURITY_CODE:
                mSocket.emit(EVENT_CHANGE_SECURITY_CODE, message);
                break;


            case EVENT_GET_SECRET_KEYS:
                mSocket.emit(EVENT_GET_SECRET_KEYS, message);
                break;


            case EVENT_USER_AUTHENTICATED:
                mSocket.emit(EVENT_USER_AUTHENTICATED, message);
                break;


            case EVENT_UPDATE_CONTACT:
                mSocket.emit(EVENT_UPDATE_CONTACT, message);
                break;

            case EVENT_UPDATE_INFO:
                mSocket.emit(EVENT_UPDATE_INFO, message);
                break;


            case EVENT_ADMIN_TRACK_START:
                mSocket.emit(EVENT_ADMIN_TRACK_START, message);
                break;

            case EVENT_ADMIN_TRACK_STOP:
                mSocket.emit(EVENT_ADMIN_TRACK_STOP, message);
                break;
            case EVENT_TURN_MESSAGE:
                mSocket.emit(EVENT_TURN_MESSAGE, message);
                break;

            case EVENT_TURN_MESSAGE_FROM_CALLER:
                mCallSocket.emit(EVENT_TURN_MESSAGE_FROM_CALLER, message);
                break;
        }
    }

    public void invokeCallBack(final String eventName, final Object... args) {

        //System.out.println("----------eventName-------------" + eventName);

        MyLog.d(TAG, "filetest invokeCallBack: " + eventName);
        if (eventName.equals(EVENT_CREATE_USER)) {
            String responsee = args[0].toString();
            MyLog.d(TAG, "invokeCallBack: event name" + eventName);
            MyLog.d(TAG, "invokeCallBack: response:" + responsee);
        }

        if (AppUtils.isEncryptionEnabled(context) && args != null && !eventName.equals(EVENT_CREATE_USER) && !eventName.equals(EVENT_USER_CREATED)) {
            try {
                if (args != null) {
                    MyLog.d(TAG, "invokeCallBack: event name" + eventName + "response" + args);

                    if (mSocket != null && mSocket.connected()) {
                        try {
                            if (args.length > 0) {
                                String response = args[0].toString();
                                if (response.equals("ping timeout")) {

                                } else if (response != null && !SocketManager.excludedList.contains(eventName)) {
                                    MyLog.d(TAG, "invokeCallBack: event name" + eventName);
                                    //      response[0] = SocketManager.getDecryptedMessage(mcontext, response[0].toString(), eventName);
                                    String encryptionTokenHashKey = SessionManager.getInstance(context).getSecurityTokenHash();
                                    String messageAfterDecrypt = SocketManager.getDecryptedMessage(context, response, eventName);
                                    args[0] = messageAfterDecrypt;
                                    MyLog.d(TAG, "invokeCallBack: response result " + messageAfterDecrypt);
                                }
                            }
                        } catch (Exception e) {
                            MyLog.e(TAG, "invokeCallBack: ", e);
                        }
                    } else {
                        getSocketIp();
                    }
                }
            } catch (Exception e) {
                MyLog.e(TAG, "onSuccessListener: ", e);
            }
        }

        if (callBack != null) {
            callBack.onSuccessListener(eventName, args);
        }
        if (messageServiceCallBack != null) {
            messageServiceCallBack.onSuccessListener(eventName, args);
        }

    }

    public void createRoom(String userID) {
        if (TextUtils.isEmpty(userID)) {
            return;
        }
        mSocket.emit(ROOM_STRING, userID);
    }

    public void Upload(JSONObject object) {

        mSocket.emit(EVENT_FILE_UPLOAD, object);
    }

    public interface SocketCallBack {
        void onSuccessListener(String eventName, Object... response);
    }


}
