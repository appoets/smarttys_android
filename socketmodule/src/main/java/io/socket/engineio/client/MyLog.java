package io.socket.engineio.client;

import android.util.Log;


public class MyLog {

    private static final boolean LOG_ENABLED = true;

    public static void d(String TAG, String msg) {
        if (LOG_ENABLED)
            Log.d(TAG, "" + msg);
    }

    public static void e(String TAG, String msg, Throwable throwable) {
        if (LOG_ENABLED)
            Log.e(TAG, "" + msg, throwable);
    }

    public static void e(String TAG, String msg) {
        if (LOG_ENABLED)
            Log.e(TAG, "" + msg);
    }

    public static void d(String msg) {
        if (LOG_ENABLED)
            Log.d("MyLog", "" + msg);
    }
}
